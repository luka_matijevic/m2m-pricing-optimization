<!-- 
* Name: AgentView
* Description: To present Case view and charts to Agents.
* Author: Ritesh Saxena
* Version: 1.0
* Created: 09-Oct-2014
-->
<apex:page standardController="Case" extensions="AgentViewController" showHeader="false" sidebar="false" id="pageId">
    <apex:includeScript value="{!URLFOR($Resource.jQueryPackage, 'jQueryPackage/media/js/jquery-1.10.2.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.jQueryPackage, 'jQueryPackage/media/js/jquery.dataTables.js')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.jQueryPackage, 'jQueryPackage/media/css/demo_table_jui.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.jQueryPackage, 'jQueryPackage/media/css/jquery-ui.css')}"/>
    <apex:includeScript value="/support/console/28.0/integration.js"/>

    <apex:form id="formId">
        <apex:pageMessages id="pageMsg"/>
        <apex:actionFunction name="applyOnChangeFilters" action="{!updateAllCaseView}" rerender="pgBlock2" status="fetchStatus"/>
        <apex:actionFunction action="{!viewCaseDetails}" name="selectCase" oncomplete="" rerender="pageMsg, pgBlock4" status="fetchStatus">
            <apex:param name="firstParam" value="" assignTo="{!caseId}"/>
        </apex:actionFunction>     
        <apex:actionFunction action="{!updateAllCaseView}" name="renderOpenCaseData" oncomplete="" rerender="pageMsg, pgBlock2" status="fetchStatus">
            <apex:param name="firstParam" value="" assignTo="{!selectedPriority}"/>
        </apex:actionFunction> 

        <apex:actionStatus startText="In progress..." id="fetchStatus">
            <!-- Displaying loading status-->
            <apex:facet name="start">
                <div>
                    <div class="loadingBackground" />
                    <div class="loadingPanel">
                       <img src=" /img/loading32.gif"/>
                    </div>
                    
                </div>
            </apex:facet>
        </apex:actionStatus>
        
        <div style="float: left; width: 98%; height:98%; padding:1%;" id="div1">
            <div style="float:left;width: 100%; height:100%; min-height:65vh; border:2px solid; border-radius:8px" id="div2">
                <div style="float: left; width: 100%;" id="div21">
                    <div style="width:46%; height:365px; margin:2% 1% 1% 2%;float:left; border:2px solid; border-radius:8px; min-height:47vh; overflow: auto;" id="div211">
                        <apex:pageBlock id="pgBlock" mode="maindetail" title="My Assigned Tickets" rendered="true">
                            <script>
                                var JQ = jQuery.noConflict();
                                    JQ(document).ready(function(){          
                                    JQ("[class$=datatable]").dataTable({
                                    /* "aTargets": [0, 1] sets No-Sort to first, second column of dataTable*/
                                    "aoColumnDefs": [{ "bSortable": false, "aTargets": [0, 1]}],
                                    /*"order": [[ 2, "asc" ]], */
                                    "bJQueryUI": true,
                                    "info": true,
                                    "bLengthChange": false,
                                    "sPaginationType": "full_numbers",
                                    "jQueryUI": false,
                                    "aLengthMenu": [30, 50, 100, 200],
                                    "iDisplayLength": 5 ,
                                    });
                                    });  
                            </script>                       
                            <apex:pageBlockSection title="" collapsible="false" columns="1" id="pbSec1">
                                <apex:commandButton value="Refresh All Open Case View" action="{!initiatePage}" rerender="pgBlock2, pgBlock3, pgBlock4, pageMsg" status="fetchStatus"/>
                                <apex:dataTable value="{!myOpenCaseList}" var="myCase" styleClass="display tftable datatable" >
                                    <apex:column headerValue="View">
                                        <apex:outputLink onclick="caseDetailView('{!myCase.id}'); return false;">View</apex:outputLink>   
                                    </apex:column>
                                    <apex:column headerValue="Edit">
                                        <apex:outputLink onclick="redirectToCase('{!myCase.Id}', null); return false;">Edit</apex:outputLink>   
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.Case.fields.Type.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!myCase.Type}"/>     
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="Account Name"/>
                                        </apex:facet>
                                        <apex:outputField value="{!myCase.Account.Name}"/>     
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.Case.fields.CaseNumber.Label}"/>
                                        </apex:facet>
                                        <apex:outputText value="{!myCase.CaseNumber}"/>      
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.Case.fields.Subject.Label}"/>
                                        </apex:facet>
                                        <apex:outputText value="{!myCase.Subject}"/>
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.Case.fields.SLA_Breach_Status__c.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!myCase.SLA_Breach_Status__c}"/>     
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.Case.fields.Has_new_Email__c.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!myCase.Has_new_Email__c}"/>     
                                    </apex:column>
                                    
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.Case.fields.CreatedDate.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!myCase.CreatedDate}"/>     
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.Case.fields.Status.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!myCase.Status}"/>     
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.Case.fields.Priority__c.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!myCase.Priority__c}"/>     
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.Case.fields.Origin.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!myCase.Origin}"/>     
                                    </apex:column>
                                </apex:dataTable>
                            </apex:pageBlockSection>
                        </apex:pageBlock>
                    </div>          
                    <div style="width:46%; height:365px; margin:2% 1% 1% 2%;float:left; border:2px solid; border-radius:8px; min-height:47vh; overflow: auto;" id="div212">
                        <apex:pageBlock id="pgBlock2" mode="maindetail" title="{!allOpenCaseLabel}" rendered="true">
                            <script>
                                /* jQuery for datTable pagination, sorting and search */
                                var JQ1 = jQuery.noConflict();
                                JQ1(document).ready(function(){          
                                JQ1("[class$=datatable1]").dataTable({
                                /* "aTargets": [0,1,2] sets No-Sort to first, second and third column of dataTable*/
                                "aoColumnDefs": [{ "bSortable": false, "aTargets": [0,1,2]}],
                                /* To set default sort on column*/
                                /*"order": [[ 3, "asc" ]],*/
                                "bJQueryUI": true,
                                "info": true,
                                "bLengthChange": false,
                                "sPaginationType": "full_numbers",
                                "jQueryUI": false,
                                "aLengthMenu": [30, 50, 100, 200],
                                "iDisplayLength": 5 ,
                                });
                                });
                            </script>
                            <apex:pageBlockSection title="" collapsible="false" columns="1" id="pbSec2">
                                <apex:selectList size="1" value="{!selectedPriority}" onchange="applyOnChangeFilters()" title="Select" style="float:left;">
                                    <apex:selectOptions value="{!selectList}" />
                                </apex:selectList>
                                <apex:dataTable value="{!openCaseList}" var="openCase" styleClass="display tftable datatable1">  
                                    <apex:column headerValue="View">
                                        <apex:outputLink onclick="caseDetailView('{!openCase.Id}'); return false;">View</apex:outputLink>   
                                    </apex:column>
                                    <apex:column headerValue="Edit">
                                        <apex:outputLink onclick="redirectToCase('{!openCase.Id}', null); return false;">Edit</apex:outputLink>   
                                    </apex:column>
                                    <apex:column headerValue="Assign">
                                        <apex:outputLink onclick="redirectToCase('{!openCase.Id}', 'assign'); return false;">Assign</apex:outputLink>   
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.Case.fields.Type.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!openCase.Type}"/>     
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="Account Name"/>
                                        </apex:facet>
                                        <apex:outputField value="{!openCase.Account.Name}"/>     
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.Case.fields.CaseNumber.Label}"/>
                                        </apex:facet>
                                        <apex:outputText value="{!openCase.CaseNumber}"/>      
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.Case.fields.Subject.Label}"/>
                                        </apex:facet>
                                        <apex:outputText value="{!openCase.Subject}"/>
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.Case.fields.SLA_Breach_Status__c.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!openCase.SLA_Breach_Status__c}"/>     
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.Case.fields.Has_new_Email__c.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!openCase.Has_new_Email__c}"/>     
                                    </apex:column>
                                    
                                     <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.Case.fields.CreatedDate.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!openCase.CreatedDate}"/>     
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.Case.fields.Status.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!openCase.Status}"/>     
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.Case.fields.Priority__c.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!openCase.Priority__c}"/>     
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.Case.fields.Origin.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!openCase.Origin}"/>     
                                    </apex:column>
                                </apex:datatable>                               
                            </apex:pageBlockSection>
                        </apex:pageBlock>
                    </div>
                </div>
        
                <div style="float: left; width: 100%;" id="div22">
                    <div style="width:46%;height:425px; margin:1% 1% 1% 2%;float:left; border:2px solid; border-radius:8px; min-height:60vh; overflow: auto;    overflow-y: visible;" id="div221">
                        <apex:pageBlock id="pgBlock3" mode="maindetail" title="Open Tickets Chart" rendered="true">                              
                            <apex:pageBlockSection title="" collapsible="false" columns="3" id="pbSec3">  
                                <div id="div2211">
                                    <apex:chart height="300" width="350" data="{!pieData}" resizable="true" rendered="{!pieData.size>0}">
                                        <apex:pieSeries dataField="data" labelField="name" colorSet="{!chartColor}" id="pieChartId">
                                            <apex:chartTips rendererFn="renderChartData" id="chartTipId"/>
                                        </apex:pieSeries>
                                        <apex:legend position="right"/>
                                        <div style="width:70%; padding-left:20px; border:1px solid; border-radius:5px; text-align:left;"><li><b>Prio 1</b>-response time is 4 hour</li><li><b>Prio 2</b>-response time is 8 hour</li><li><b>Prio 3</b>-response time is 24 hour</li>
                                        </div>
                                    </apex:chart>
                                    <apex:outputPanel style="font-size:14px;" rendered="{!IF(pieData.size>0, false, true)}"><br/><br/><b>No Data Available! </b></apex:outputPanel>
                                    
                                </div>                              
                            </apex:pageBlockSection>  
                            <apex:pageBlockButtons location="bottom" style="text-align:centre">
                                <apex:commandButton value="Refresh All Open Case View" action="{!initiatePage}" rerender="pgBlock2, pgBlock3, pgBlock4, pageMsg" status="fetchStatus"/>
                            </apex:pageBlockButtons>                            
                        </apex:pageBlock> 
                    </div>          
                    <div style="width:46%; height:425px; margin:1% 1% 1% 2%; float:left; border:2px solid; border-radius:8px; min-height:60vh; overflow: auto;" id="div222">
                        <apex:pageBlock id="pgBlock4" mode="maindetail" title="{!IF(caseNum!='', 'Ticket email/comments history: ' + caseNum, 'Ticket email/comments history')}" rendered="true">
                            <script>
                                /* jQuery for datTable pagination, sorting and search */
                                var JQ2 = jQuery.noConflict();
                                JQ2(document).ready(function(){          
                                JQ2("[class$=datatable2]").dataTable({
                                "bAutoWidth": false,
                                /*overrides defaults and sets width for each column of dataTable */
                                "aoColumns": [
                                { "sWidth": "180px" }, 
                                { "sWidth": "80px" }, 
                                { "sWidth": "270px" }, 
                                { "sWidth": "150px" },
                                { "sWidth": "100px" }
                                ],
                                "bJQueryUI": true,
                                "info": true,
                                "bFilter": false,
                                "bLengthChange": false,
                                "sPaginationType": "full_numbers",
                                "jQueryUI": true,
                                "aLengthMenu": [30, 50, 100, 200],
                                "iDisplayLength": 3
                                });
                                });
                                
                                /* jQuery for datTable pagination, sorting and search */
                                var JQ3 = jQuery.noConflict();
                                JQ3(document).ready(function(){          
                                JQ3("[class$=datatable3]").dataTable({
                                "bAutoWidth": false,
                                /*overrides defaults and sets width for each column of dataTable */
                                "aoColumns": [
                                { "sWidth": "200px" }, 
                                { "sWidth": "150px" }, 
                                { "sWidth": "150px" }, 
                                { "sWidth": "150px" },
                                ],
                                "bJQueryUI": true,
                                "info": true,
                                "bFilter": false,
                                "bLengthChange": false,
                                "sPaginationType": "full_numbers",
                                "jQueryUI": true,
                                "aLengthMenu": [30, 50, 100, 200],
                                "iDisplayLength": 3
                                });
                                });
                            </script>
                            <style>
                                table.display2 {
                                    margin: 0 auto;
                                    width: 100%;
                                    clear: both;
                                    border-collapse: collapse;
                                    table-layout: fixed; 
                                    word-wrap:break-word; 
                                }
                            </style>
                            
                            <apex:pageBlockSection title="Emails" collapsible="true" id="pbSec4">
                                <apex:dataTable value="{!caseEmailList}" var="caseEmail" styleClass="display2 display tftable datatable2">
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.EmailMessage.fields.Subject.Label}"/>
                                        </apex:facet>
                                        <apex:outputText value="{!caseEmail.Subject}"/>      
                                    </apex:column>
                                    
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.EmailMessage.fields.Status.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!caseEmail.Status}"/>     
                                    </apex:column>
                                    <apex:column width="50px">
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.EmailMessage.fields.TextBody.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!caseEmail.TextBody}"/>     
                                    </apex:column>
                                    <!--
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.EmailMessage.fields.FromName.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!caseEmail.FromName}"/>     
                                    </apex:column> -->
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.EmailMessage.fields.FromAddress.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!caseEmail.FromAddress}"/>     
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.EmailMessage.fields.CreatedDate.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!caseEmail.CreatedDate}"/>     
                                    </apex:column>                                                                     
                                </apex:dataTable>
                            </apex:pageBlockSection> 
                            
                            <apex:pageBlockSection title="Case Comments" collapsible="true" id="pbSec5">
                            
                                <apex:dataTable value="{!commentList}" var="caseCom" styleClass="display2 display tftable datatable3">
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.CaseComment.fields.CommentBody.Label}"/>
                                        </apex:facet>
                                        <apex:outputText value="{!caseCom.CommentBody}"/>      
                                    </apex:column>   
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.CaseComment.fields.IsPublished.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!caseCom.IsPublished}"/>     
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.CaseComment.fields.CreatedDate.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!caseCom.CreatedDate}"/>     
                                    </apex:column>  
                                    <apex:column >
                                        <apex:facet name="header">
                                                <apex:outputText value="{!$ObjectType.CaseComment.fields.LastModifiedDate.Label}"/>
                                        </apex:facet>
                                        <apex:outputField value="{!caseCom.LastModifiedDate}"/>     
                                    </apex:column>                              
                                </apex:dataTable>
                            </apex:pageBlockSection>    
                        </apex:pageBlock>
                    </div>
                </div>
            </div>
        </div>
        
    </apex:form>
    
    <script type="text/javascript">
        
        function caseDetailView(caseId) {
            selectCase(caseId);
        }
        
        function renderChartData(klass, item) {
            var m = item.storeItem;   
            this.setTitle(m.get('name') + ': ' + m.get('data'));  
            // to capture onClick on pieChart and execute action. //
            window.onclick = function(e) {
                if(e.target.localName=='path'){
                    renderOpenCaseData(m.get('name'));
                }
            };
        }
        
        // Redirects to case detail page based on check isInConsole 
        function redirectToCase(caseId, action) {
            var caseURL='';
            if(action=='assign')
                caseURL = '/'+caseId+'/a?retURL=%2F'+caseId;
            else {
                caseURL = '/'+caseId+'?retURL=%2F'+caseId;
            }
            if (sforce.console.isInConsole()) {
                sforce.console.openPrimaryTab(null, caseURL, true, 'Case Details', openSuccess, 'Case Edit');
            } 
            else { 
                window.open(caseURL, '_blank');
            } 
        
            var openSuccess = function openSuccess(result) {
                //Report whether we succeeded in opening the subtab
                if (result.success == true) {
                } else {
                }
            };
        }
    
    </script>
    
    <style type="text/css">
        /* adds styling to table header and data*/
        .tftable {border-width: 1px;border-color: #ccc;border-collapse: collapse;border-spacing:0; overflow:auto;}
        .tftable th {font-weight:bold;font-family:Arial,Helvetica,sans-serif;font-size:12px;background-color:#EEEEEE;border-width: 1px;padding: 5px;border-style: solid;border-color: #a9a9a9;text-align:center;}
        .tftable tr {background-color:#fff;}
        .tftable td {font-family:Arial,Helvetica,sans-serif;font-size:12px;border-width: 1px;padding: 3px;border-style: solid;border-color: #EEEEEE;}
        .tftable tr:hover {background-color:#EBF4FA;}

        /* to stop wrapping of pageBlock table headings.*/
        .pbTitle {
            white-space: nowrap
        }
        
        /* to align dataTable filter to left*/
        .dataTables_filter {
           float: left;
           text-align: left;
           font-weight: bold;
        }
        
        /* to align dataTable pagination attributes to left*/
        .dataTables_paginate {
            float: left;
            text-align: left;
        }
        
        /* loads the inProgress image and grey out the background*/
        .loadingBackground {
           /* Background color */
            background-color:black;
            opacity: 0.10;
            filter: alpha(opacity = 10);
        
            /* Dimensions */
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            z-index: 999;
            position: fixed;
        }
        .loadingPanel {
            /* Dimensions */
            left: 48%;
            top: 48%;
            height: 50px;
            z-index: 999;
            position: fixed;
        }

    </style>

</apex:page>