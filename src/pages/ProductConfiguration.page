<apex:page doctype="html-5.0" standardcontroller="cscfga__Product_Basket__c" extensions="cscfga.UISupport"
	showHeader="true" sidebar="true" title="Edit Product Configuration" action="{!checkRuntimeVersion}">

	<cscfga:FixUIStyles />
	
	<apex:stylesheet value="{!$Resource.cscfga__Configuration_css}"/>
	<apex:stylesheet value="{!URLFOR($Resource.cscfga__select2,'select2.min.css')}" />
	<apex:stylesheet value="{!URLFOR($Resource.cscfga__Indicator,'css/indicator.css')}"></apex:stylesheet>
	<div id="indicatorContainer" style="top: 0;bottom: 0;right: 0;left: 0;position: absolute; display:none;">{!$Label.cscfga__embscr_Loading}</div>
	<c:CSSE_GenericFlatui ></c:CSSE_GenericFlatui>
	<!--<c:ScreenList />-->
	
	<div id="CSValidationMessageBox" class="message warningM3">
		<table border="0" cellpadding="0" cellspacing="0" class="messageTable" style="padding:0px;margin:0px;">
			<tr valign="top">
				<td>
					<img alt="WARNING" class="msgIcon" src="/s.gif" title="WARNING" />
				</td>
				<td class="messageCell">
					<div class="messageText">
						<h4>Warning:</h4>
						<span id="CSValidationMessage"></span>
					</div>
				</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>

	<div id="CSInfoMessageBox" class="message infoM3">
		<table border="0" cellpadding="0" cellspacing="0" class="messageTable" style="padding:0px;margin:0px;">
			<tr valign="top">
				<td>
					<img alt="INFO" class="msgIcon" src="/s.gif" title="INFO" />
				</td>
				<td class="messageCell">
					<div class="messageText">
						<div id="CSInfoMessage"></div>
					</div>
				</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>

	<!--
	################################################
	# Dummy input date field to enable hand-rolled
	# date pickers to be built dynamically. Adding
	# this control forces SF to build the DatePicker
	# table at the end of the page containing the
	# content for the datepicker div.
	################################################
	-->
	<div id="configurationContainer" />

	<script type="text/javascript" src="{!URLFOR($Resource.cscfga__jshashtable)}" />
	<script type="text/javascript" src="{!URLFOR($Resource.cscfga__jQuery_min)}"></script>
	<script type="text/javascript" src="{!URLFOR($Resource.cscfga__jquery_numberformatter)}"></script>
	<script type="text/javascript" src="{!URLFOR($Resource.cscfga__Underscore)}"></script>
	<script type="text/javascript" src="{!URLFOR($Resource.cscfga__select2,'select2.min.js')}"></script>
	<script type="text/javascript" src="{!URLFOR($Resource.cscfga__datejs,'date-en-GB.min.js')}"></script>
	<!--<script type="text/javascript" src="{!URLFOR($Resource.cscfga__datejs,'date-' & userLocale & '.min.js')}" />-->
	<script type="text/javascript" src="{!URLFOR($Resource.cscfga__cs_online)}"></script>
	<script type="text/javascript" src="{!URLFOR($Resource.cscfga__cs_js)}/require.js"/>  
	<script type="text/javascript"> require.config({baseUrl: '{!JSENCODE(URLFOR($Resource.cscfga__cs_js))}'});</script>
		

	<script type="text/javascript">
		var params = {};
		var UserContext = UserContext || {};
			UserContext["dateFormat"] = "dd.MM.yyyy.";
			UserContext["language"] = "en_US";
			UserContext["locale"] = "hr_HR";
		if (jQuery.browser.msie) {
			jQuery(document).ready(function() {
				jQuery('body').append('<div id="popupOverlay"><div id="lookupContainer"></div></div>');
			});
		}

		jQuery(document).ready(function() {
			window.setTimeout(clearSafariRequireBug, 50);
		});

		/*
		 * Safari 9.x issue: jam require() will fail on first call
		 * unaffected browsers should log 'Require initialiseation not needed...' and continue as normal
		 * affected browsers should log 'Require initialised...' and continue as normal
		 */
		function clearSafariRequireBug() {
			try {
				require(['./src/cs-full'], function() {
					CS.App.Components.Repository.addComponent('configurator', 'MultipleRelatedProduct', jQuery('#CS\\.MultipleRelatedProduct__tpl')[0]);
					CS.App.Components.Repository.addComponent('configurator', 'MultiSelectLookup', jQuery('#CS\\.MultiSelectLookup__tpl')[0]);

					console.log('Require initialisation not needed (Safari 9.x fix)');
				});
			} catch (e) {
				console.log('Require initialised (Safari 9.x fix)');
			}
		}

		/*
		 * Returns true if we are in Classic Salesforce view,
		 * or false if we are in either Lightning or Salesforce1 mobile
		 */
		function isClassicSalesforce() {
			return (typeof sforce == 'undefined') || !sforce || !sforce.one;
		}

		var params = {
			basketId: '{!JSENCODE(basketId)}',
			configId: '{!JSENCODE(configId)}',
			definitionId: '{!JSENCODE(definitionId)}',
			linkedId: '{!JSENCODE(linkedId)}',
			retURL: '{!JSENCODE(retURL)}',
			packageSlotId: '{!JSENCODE(packageSlotId)}',
			isDtp: '{!JSENCODE(isDtp)}',
			screenflow: '{!JSENCODE(screenFlowName)}'
		};

		require(['./src/cs-full'], function() {
			CS.App.Components.Repository.addComponent('configurator', 'MultipleRelatedProduct', jQuery('#CS\\.MultipleRelatedProduct__tpl')[0]);
			CS.App.Components.Repository.addComponent('configurator', 'MultiSelectLookup', jQuery('#CS\\.MultiSelectLookup__tpl')[0]);

			var delegate = {
				getLinkedObjectId: function() {
					return '{!JSENCODE(linkedId)}';
				},

				loadConfiguration: function(id, definitionId, callback) {
					Visualforce.remoting.Manager.invokeAction(
						'{!$RemoteAction.UISupport.getProductConfiguration}',
						id,
						function(result, event) {
							try {
								var configData = JSON.parse(result);
								callback(definitionId, configData, '{!$RemoteAction.UISupport.loadLookupRecord}');
							} catch (e) {
								CS.Log.error('Could not load product configuration id ' + id + ': ' + e.message);
							}
						},
						{escape: false}
					);
				},

				loadProductTemplateHtml: function(id, screenFlowName, callback) {
				if (typeof screenFlowName === 'function') {
					callback = screenFlowName;
					screenFlowName = '';
				}
				var json = Visualforce.remoting.Manager.invokeAction(
					'{!$RemoteAction.UISupport.getProductTemplate}',
					id, screenFlowName, template,
					function(result, event) {
						if (event.status) {
							callback(JSON.parse(result).html);
						} else {
							callback(new Error(event.message));
						}
					},
					{escape: false}
				);
			},

			loadDefinition: function(id, callback) {
				var json = Visualforce.remoting.Manager.invokeAction(
					'{!$RemoteAction.UISupport.getProductModel}',
					id,
					function(result, event) {
						if (event.status) {
							callback(JSON.parse(result));
						} else {
							customErrorHandler(event.message);
						}
					},
					{escape: false}
				);
			},

				loadLinkedObjectProperties: function(linkedObjectId, productDefinitionId, callback) {
					var json = Visualforce.remoting.Manager.invokeAction(
						'{!$RemoteAction.UISupport.getLinkedObjectProperties}',
						linkedObjectId, productDefinitionId,
						function(result, event) {
							callback(JSON.parse(result));
						},
						{escape: false}
					);
				},

				lookupQuery: {!$RemoteAction.UISupport.lookupQuery},

				loadRelatedProductSelectionData: function(params, callback) {
					var json = Visualforce.remoting.Manager.invokeAction(
						'{!$RemoteAction.UISupport.getRelatedProductSelectionData}',
						params,
						function(result, event) {
							callback(result);
						},
						{escape: false}
					);
				},

				loadLookupRecord: function(params, callback) {
					var json = Visualforce.remoting.Manager.invokeAction(
						'{!$RemoteAction.UISupport.loadLookupRecord}',
						params,
						function(result, event) {
							callback(result);
						},
						{escape: false}
					);
				},

				loadCustomLookupReferencedAttributes: function(productDefinitionId, callback) {
					var json = Visualforce.remoting.Manager.invokeAction(
						'{!$RemoteAction.UISupport.loadCustomLookupReferencedAttributes}',
						productDefinitionId,
						function(result, event) {
							callback(JSON.parse(result));
						},
						{escape: false}
					);
				},

				getSelectListLookup: function(params, callback) {
					var json = Visualforce.remoting.Manager.invokeAction(
						'{!$RemoteAction.UISupport.getSelectListLookup}',
						JSON.stringify(params), '', '', '',
						function(result, event) {
							callback(result, event);
						},
						{escape: false}
					);
				},

				storeConfiguration: function(payload, callback) {
					Visualforce.remoting.Manager.invokeAction(
						'{!$RemoteAction.UISupport.storeConfigs}',
						payload,
						function(result, event) {
							if (event.status) {
								result._success = true;
							} else {
								if (!result) {
									result = {};
								}
								result._success = false;
								result._message = event.message;
							}
							callback(result, function redirect() {
								window.location.href = CS.params.retURL;
							});
						}
					);
				}
			},
			template = 'CustomScreenTemplate';

			CS.Settings = {
				"configAllowsLiveReload" : true,
				"isProfilingEnabled" : false,
				"isWarnEnabled" : true,
				"isInfoEnabled" : true,
				"isDebugEnabled" : false,
				"isEntryLoggingEnabled" : false,
				"isSoqlLoggingEnabled" : false,
				"isScreenLoggingEnabled" : false,
				"isBrowserLoggingEnabled" : true,
				"cascadeDeleteDisabled" : false,
				"isLoadingOverlayEnabled" : true,
				"isDocumentLoggingEnabled" : false,
				"displayTCVColumn" : true,
				"displayRecurringInvoiceColumn" : false,
				"copyOppOwnerToConfigs" : true,
				"showRateLineItemsInSeparateColumn" : false,
				"showListPriceAndDiscountAmount" : true,
				"isMultiLingualSupportEnabled" : false,
				"enableDynamicLookupQueries" : true,
				"indicatorTimeout" : 1000
			};

			CS.Labels = {
				"selectlistlookup_Please_select_value" : "{!$Label.cscfga__selectlistlookup_Please_select_value}",
				"selectlistlookup_Please_enter_search" : "{!$Label.cscfga__selectlistlookup_Please_enter_search}"
			};

			params.linkedId = CS.Util.stripIdFromUrl(params.linkedId);

			//AF temp
			CS.params = params;

			// set user language for number/date formatter
			if (typeof CS !== 'undefined' && CS['DataConverter'] instanceof Object) {
				CS.DataConverter.userLanguage = '{!userLanguage}';
			}

			launchConfigurator(CS, delegate, params, function() {
				// no op
				setTimeout(initializeWidgets, 400);
				var date = CS.todayFormatted.split('/');
				CS.todayFormatted2 = date[1] + '/' + date[0] + '/' + date[2];
			});
		});

		var finishWaitData = {
			elapsedTime: 0,
			tick: 100,
			timeout: 5000
		};
		function finish() {
			var _this = this;
			var _arguments = arguments;
			if (typeof CS.rulesTimer !== 'undefined') {
				finishWaitData.elapsedTime += finishWaitData.tick;
				if (finishWaitData.elapsedTime > finishWaitData.timeout) {
					CS.Log.warn('finish() method timed out while waiting for rules to execute, continuing anyway...');
				} else {
					CS.Log.debug('[' + finishWaitData.elapsedTime + '] Waiting for rules to finish... ');
					setTimeout(function() { finish.apply(_this, _arguments); }, finishWaitData.tick);
					return;
				}
			}

			finishWaitData.elapsedTime = 0;
			var validationResult = CS.Service.validateCurrentConfig(true);
			var configurationStatus = CS.getConfigurationProperty('', 'status');

			if (configurationStatus === '{!CONFIGURATION_STATUS_INVALID}' || !(validationResult.isValid)) {
				updateFinishButtonUI(this);

				// There are validation errors within the configuration. The configuration can still be saved but the basket cannot be synchronised with an Opportunity until the errors have been corrected.
				CS.markConfigurationInvalid('{!$Label.cscfga__configuration_There_are_validation_errors}');
			} else {
				saveConfiguration(this);
			}
		}

		function saveConfiguration(buttonElement) {
			var basketSpec = {
				Id: params.basketId,
				linkedId: params.linkedId,
				packageSlotId: params.packageSlotId
			};
			if (typeof buttonElement === 'undefined') {
				buttonElement = this;
			}

			CS.Log.info('Persisting configuration...');
			CS.Service.persistConfiguration(basketSpec, (function(p){
				return function(result, redirectCallback) {
					if (result._success) {
						CS.Log.info('Configuration persisted');
						window.location.assign(buildAfterFinishUrl(p, result));
					} else {
						CS.markConfigurationInvalid(result._message);
						updateFinishButtonUI(buttonElement, true);
					}
				}
			})(params)
			);
		}

		function displayButtons() {
			var pp = CS.Util.configuratorPrefix;
			var buttonsHTML = '';
			var currentScreenIdx = CS.Service.getCurrentScreenIndex();
			var ref = CS.Service.getCurrentConfigRef();
			var currentConfig = CS.Service.config[ref].config;

			var rootRef = ref;
			if (typeof ref === 'string' && ref.indexOf('_') > 0) {
				var refParts = ref.split(/_/);
				if (refParts.length > 0 && !isNaN(refParts[refParts.length - 1])) {
					refParts[refParts.length - 1] = 0;
					rootRef = refParts.join('_');
				}
			}
			var numScreens = CS.Service.config[ref].screens.length;

			var productIndex = CS.Service.getProductIndex(currentConfig[pp + 'Product_Definition__c']);
			var productId = CS.Service.getCurrentProductId();
			var productDefinition = productIndex.productsById[productId];

			if (currentScreenIdx > 0) {
				buttonsHTML += '<button class="btn btn-primary" data-cs-group="Previous" onclick="clickAction(this, displayScreen, ' + (currentScreenIdx - 1) + ')">Previous</button>';
			}

			if (currentScreenIdx < numScreens - 1) {
				buttonsHTML += '<button class="btn btn-primary" data-cs-group="Next" onclick="clickAction(this, displayScreen, ' + (currentScreenIdx + 1) + ')">Next</button>';
			}

			if (ref === '') {
				buttonsHTML += '<button class="btn btn-primary" data-cs-group="Cancel" onclick="clickAction(this, cancel, buildCancelUrl(params))">Cancel</button>';
				buttonsHTML += '<button class="btn btn-primary" data-cs-group="Finish" onclick="clickAction(this, finish)">Finish</button>';
				if (productDefinition[pp + 'Allow_progress_from_incomplete_screens__c']) {
					buttonsHTML += '<button class="hidden btn btn-primary" data-cs-group="ForceFinish" onclick="clickAction(this, saveConfiguration)">Save without Validation</button>';
					buttonsHTML += '<button class="hidden btn btn-primary" data-cs-group="ForceFinish" onclick="clickAction(this, finish)">Validate and Save</button>';
				}
			} else {
				buttonsHTML += '<button class="btn btn-primary" data-cs-group="Cancel" onclick="clickAction(this, cancelRelated)">Cancel</button>';
				buttonsHTML += '<button class="btn btn-primary" data-cs-group="Finish" onclick="clickAction(this, cont)">Continue</button>';
				if (productDefinition[pp + 'Allow_progress_from_incomplete_screens__c']) {
					buttonsHTML += '<button class="hidden btn btn-primary" data-cs-group="ForceFinish" onclick="clickAction(this, saveRelated)">Save without Validation</button>';
					buttonsHTML += '<button class="hidden btn btn-primary" data-cs-group="ForceFinish" onclick="clickAction(this, cont)">Validate and Save</button>';
				}
			}


			jQuery('.CS_configButtons').html(buttonsHTML);
		}

	</script>

	<script type="text/html" id="CS.MultipleRelatedProduct__tpl">
	<%  var disabled = definition.Max__c && relatedProducts.length >= definition.Max__c ? 'disabled="disabled"' : '',
			prod,
			attRef,
			rowClass;
		var attr = anchor.attr;
		var isReadOnly = attr[CS.Util.configuratorPrefix + 'Is_Read_Only__c'];
		var isActive = attr[CS.Util.configuratorPrefix + 'is_active__c'];
		var isRequired = attr[CS.Util.configuratorPrefix + 'Is_Required__c'];
	%>
		<div class="apexp">
			<div class="individualPalette">
				<div class="bootstrap-iso">
					<div class="panel panel-success">
						<div class="panel-heading clearfix">
							<h4 class="panel-title pull-left">
								<span><%= definition.Name %></span>
								<% if (isRequired) { %><span class="required">(Required)</span><% } %>
								<% if (isReadOnly) { %><span class="readOnly">(Read Only)</span><% } %>
							</h4>
							<div class="CS_configButtons text-center">
								<button class= "btn btn-primary" <%= disabled %>  data-cs-control="<%= anchor.reference %>" data-cs-ref="<%= anchor.reference %>" data-cs-action="addRelatedProduct" data-cs-type="add" data-role="none">New <%= definition.Name %></button>
							</div>
						</div>
						<div class="pbBody">
	<% if (relatedProducts.length > 0) { %>
		<table class="table table-striped table-hover lato" data-cs-binding="<%= anchor.reference %>" data-cs-control="<%= anchor.reference %>" data-cs-type="list">
			<thead class="rich-table-thead">
				<tr class="headerRow">
					<th class="headerRow" style="width: 6em;">Action</th>
					<th class="headerRow" style="width: 6em;">Status</th>
					<th class="headerRow">Name</th>
	<%  for (var i = 0; i < cols.length; i++) {
			var spec = colSpecs[cols[i]]; %>
				<th class="headerRow">
					<%= spec.header %>
				</th>
	<%  } %>
				</tr>
			</thead>
	<%  for (var i = 0; i < relatedProducts.length; i++) {
			prod = relatedProducts[i];
			rowClass = 'dataRow ' + (i/2 == Math.floor(i/2) ? 'even ' : 'odd ') + (i == 0 ? 'first ' : '') + (i >= relatedProducts.length - 1 ? 'last' : '');
	%>
			<tr data-cs-ref="<%= prod.reference %>">
				<td class="dataCell">
				<% if (isActive && ! isReadOnly) { %>
					<span data-cs-action="editRelatedProduct" data-cs-ref="<%= prod.reference %>">Edit</span> |
					<span data-cs-action="removeRelatedProduct" data-cs-ref="<%= prod.reference %>">Del</span>
				<% } %>
				</td>
				<td class="dataCell"><%= prod.config[CS.Util.configuratorPrefix + 'Configuration_Status__c'] %></td>
				<td class="dataCell"><span data-cs-action="editRelatedProduct" data-cs-ref="<%= prod.reference %>"><%= prod.config.Name %></span></td>
	<%	  for (var j = 0; j < cols.length; j++) {
				if (colSpecs[cols[j]].ref != undefined) {
					attRef = prod.reference + ':' + colSpecs[cols[j]].ref;
				}
				if (attRef != undefined) {
	%>
				<td><%= CS.getAttributeDisplayValue(attRef) %></td>
	<%		  }
			} %>
			</tr>
	<%  } %>
		</table>
	<% } else { %>
		<table class="table" data-cs-control="<%= anchor.reference %>" data-cs-type="list">
			<tr class="dataRow even first last">
				<td class="dataCell">
					No items to display
				</td>
			</tr>
		</table>
	<% } %>
					</div>
						<div class="pbFooter secondaryPalette">
							<div class="bg"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</script>
<apex:form style="width: 0; height: 0; position: absolute; left: -1000px;">
	<!--
		This inputField initializes the SFDC DatePicker JS widget.
		The form is hidden due to recent changes in SFDC VF rendering engine
		which started displaying this inputField (it was previously hidden due
		to referencing the read-only SObject field)
	-->
	<apex:inputField value="{!cscfga__Product_Basket__c.CreatedDate}"/>
</apex:form>

<!-- related product selection template START -->
<div id="rpDialogContainer" style="display: none">

		<div class="bPageTitle">
			<div class="ptBody">
				<div class="content">
					<img src="/s.gif" alt="Rule" class="pageTitleIcon" title="Select Product" />
					<h1 class="pageType">Select Product</h1>
					<h2 class="pageDescription">Select Product</h2>
					<div class="blank">&nbsp;</div>
				</div>
			</div>
		</div>

		<div id="" style="display: block">
			<div class="apexp">
				<div class="Custom24Block">
					<div id="rpdBreadcrumb" class=""></div>
					<br />
				</div>
			</div>
		</div>

		<div class="apexp">
			<div class="individualPalette">
				<div class="Custom24Block">
					<div class="bPageBlock brandSecondaryBrd apexDefaultPageBlock secondaryPalette">
						<div class="pbHeader">
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td class="pbTitle"><h2 class="mainTitle">{!$Label.cscfga__selcat_Select_Category}</h2></td>
								</tr>
							</table>
						</div>
						<div class="pbBody">

							<table class="list" data-cs-binding="" data-cs-control="" data-cs-type="list" cellpadding="0" cellspacing="0">
								<thead class="rich-table-thead">
									<tr class="headerRow">
										<th class="headerRow" width="50%">{!$Label.cscfga__selprodcat_Category_Name}</th>
										<th class="headerRow" width="50%">{!$Label.cscfga__selprodcat_Description}</th>
									</tr>
								</thead>
								<tbody id="rpSelectCategoryList">
								</tbody>
							</table>

						</div>
						<div class="pbFooter secondaryPalette">
							<div class="bg"></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="apexp">
			<div class="individualPalette">
				<div class="Custom24Block">
					<div class="bPageBlock brandSecondaryBrd apexDefaultPageBlock secondaryPalette">
						<div class="pbHeader">
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td class="pbTitle"><h2 class="mainTitle">Select Product</h2></td>
								</tr>
							</table>
						</div>
						<div class="pbBody">
							<table class="list" data-cs-binding="" data-cs-control="" data-cs-type="list" cellspacing="0" cellpadding="0">
								<thead class="rich-table-thead">
									<tr class="headerRow">
										<th class="headerRow" width="50%">{!$Label.cscfga__selcat_Product_Name}</th>
										<th class="headerRow" width="50%">{!$Label.cscfga__selcat_prod_Description}</th>
									</tr>
								</thead>
								<tbody id="rpSelectProductDefinitionList">
								</tbody>
							</table>
						</div>
						<div class="pbFooter secondaryPalette">
							<div class="bg"></div>
						</div>
					</div>
				</div>
				<div id="" class="">
					<input id="rpCancelButton" type="button" value="{!$Label.cscfga__selcat_Cancel}" class="pbButtonb CS_configButtons"></input>
				</div>
			</div>
		</div>

</div>
<!-- related product selection template END -->
<apex:stylesheet value="{!URLFOR($Resource.FlatUI,'FlatUI/css/flat-ui-iso.css')}"></apex:stylesheet>
<c:SlidingPanel />


<!-- Loading Pick A Date -->
<apex:includeScript value="{!URLFOR($Resource.pickadate,'pickadate.js-3.5.6/picker.js')}"/>
<apex:includeScript value="{!URLFOR($Resource.pickadate,'pickadate.js-3.5.6/picker.date.js')}"/>
<apex:includeScript value="{!URLFOR($Resource.pickadate,'pickadate.js-3.5.6/picker.time.js')}"/>
<apex:includeScript value="{!URLFOR($Resource.pickadate,'pickadate.js-3.5.6/legacy.js')}"/>
<apex:stylesheet value="{!URLFOR($Resource.pickadate,'pickadate.js-3.5.6/themes/classic.css')}"/>
<apex:stylesheet value="{!URLFOR($Resource.pickadate,'pickadate.js-3.5.6/themes/classic.date.css')}"/>

<style type="text/css">
	.lookupInput img.deleteIcon, .lookupInput div.deleteIcon {
		background-image: url({!URLFOR($Resource.cscfga__delete_icon)});
	}
	label.requiredOn {
		font-weight: bold;
	}
	label.requiredOn:after {
		color: #e32;
		content: ' *';
		display:inline;
	}
	.lato tr td {
		font-family: Lato;
	}
	p.attributeErrorMessage {
		margin: 0;
		margin-top: 3px;
		color: #C00;
		position: relative;
		right: -40%;
	}
	.form-control.form-control-date {
		width: inherit; 
		display:inline-block;
	}
	
	.form-control.form-control-date[readOnly] {
		color: #34495e;
		background-color: #fff;
		opacity: 1;
		border-color: #bdc3c7;;
	}
	
	.form-control.form-control-lookup {
		width: inherit; 
		display:inline-block;
	}
	
	.form-control.form-control-lookup[readOnly] {
		color: #34495e;
		background-color: #fff;
		opacity: 1;
		border-color: #bdc3c7;;
	}
	
	.form-control.form-control-select-lookup {
		padding: 0px;
	}
	
	.select2-container.select2-allowclear {
		width: 100%;
		height: 100%;
	}
	
	.select2-choice {
		width: 100% !important;
		height: 100% !important;
		padding: 0px !important;
		background-image: none !important;
		background-color: #fff !important;
	}
	
	.select2-chosen {
		padding:  8px 12px;
	}
	
	.select2-search-choice-close {
		top: 12px !important;
	}
	
	.select2-arrow {
		border-width: 6px 3px !important;
		right: 4px !important;
		top: 16px !important;
		width: auto !important;
		border-left: 3px solid rgba(0, 0, 0, 0) !important;
		border-radius: 0px !important;
		background: #fff !important;
		background-image: none !important;
	}
	</style>

</apex:page>