<apex:page controller="TerminationConsoleNewController" applyBodyTag="false" standardStylesheets="false" showHeader="false" sidebar="false" title="Termination Confirmation Letter" renderAs="pdf">
    <head>
        <style type="text/css" media="print">
            @page {
                size: A4;
                
                @top-center {
                    content: element(header-section);
                }
    
                @bottom-left {
                    content: element(footer-section);
                }
                padding: 20px 0 80px;
            }
            
            /*.main-container {*/
            /*    padding: 20px 0 80px;*/
            /*}*/
            .header-section {
                position: running(header-section);
                padding-bottom: 10px;
            }
            
            .footer-section {
                display: block;
                position: running(footer-section);
                padding-top: 10px;
                margin-top: -80px;
                font-size: 9px;
                border-top: 1px solid #999;
            }
            
            .pagenumber:before {
                content: counter(page);
            }
            
            .pagecount:before {
                content: counter(pages);
            }
            
            div.page-break {
                page-break-inside:avoid; 
                page-break-after:auto;
            }
            
            table.page-break {
                page-break-inside:auto;
            }
            
        </style>
    </head>
    
    <div class="main-container">
        <div class="header-section">
            <div><p style="text-align:right;">Seite <span class="pagenumber"/> von <span class="pagecount"/></p></div>
        </div>
        <div class="second-section">
            <div class="len-text" style="float:left;">
                <div>
                    <p style="font-size: 12px; padding:0; ">
                        <apex:outputText value="Telefónica Germany GmbH & Co. OHG Business M2M Service 90345 Nürnberg"/>
                    </p>
                </div>
                <div style="margin-left:15px; margin-top:50px;">
                    <apex:outputText value="{!acc.Name}"/>
                </div>
                <div style="margin-left:15px;">
                    <apex:outputText value="{!acc.BillingState}"/>
                </div>
                <div style="margin-left:15px;">
                    <apex:outputText value="{!acc.BillingStreet}"/>
                </div>
                <div style="margin-left:15px;">
                    <apex:outputText value="{!acc.BillingPostalCode}"/>&nbsp;&nbsp;
                    <apex:outputText value="{!acc.BillingCity}"/>
                </div>
            </div>
            <div class="right-section" style="float:right; font-family: 'Helvatica'; font-size: 14px;">
                <div style="margin-top: 5px;">
                    M2M Service von Telefónica
                </div>
                <div style="margin-top: 15px;">
                    t +49 (0)40 51900 6600
                    <br/> e service@m2m.telefonica.de
                </div>
                <div style="margin-top: 15px;">
                    www.m2m.telefonica.de
                </div>
                <div style="margin-top: 15px;">
                    <apex:outputText value="Rahmenvertragsnummer: {!frameContract}"/>
                </div>
                <div style="margin-top: 15px;">
                    <apex:outputText value="Ticket-Nummer: {!ticketCase.CaseNumber}"/>
                </div>
            </div>
        </div>
        <div class="date-section" style="margin-top:150px;">
            <div style="float: left; margin-left:15px; margin-top:20px;">
                <p><apex:outputText value="{!terminationDate}"/></p>
            </div>
        </div>
        <div>
            <div style="margin-top: 70px;">
                <span style="margin-left: -85px;"><strong>Kündigung Ihrer M2M-Anschlüsse</strong></span>
            </div>
        </div>
        <div>
            <div style=" margin-top: 15px; margin-left:15px;">
                Sehr geehrte Damen und Herren,
            </div>
        </div>
        <div>
            <div style=" margin-top: 15px; margin-left:15px;">
                vielen Dank für Ihr Schreiben vom {!dateOfReceipt}.
            </div>
        </div>
        <div>
            <div style=" margin-top: 15px; margin-left:15px;">
                Wir bestätigen Ihnen den Eingang Ihres Kündigungsschreibens. Die betroffenen Telefónica M2M-Anschlüsse und den jeweiligen Kündigungstermin haben wir in der Anlage zu diesem Schreiben für Sie aufgeführt.
            </div>
        </div>
        <div>
            <div style=" margin-top: 15px; margin-left:15px;">
                Bis zu diesem Zeitpunkt werden alle Leistungen wie gewohnt berechnet. Deshalb erhalten Sie auch nach dem Vertragsende noch Rechnungen. Die Aufstellung der Kosten in der letzten Rechnung erfolgt entsprechend dem tatsächlich abgerechneten Zeitraum.
            </div>
        </div>
        <div>
            <div style=" margin-top: 15px; margin-left:15px;">
                Das Vertrauen unserer Kunden liegt uns am Herzen und so würden wir gerne mehr über Ihre Kündigungsentscheidung erfahren. Was hat Ihnen bei Telefónica gefallen und was nicht? Was war der ausschlaggebende Grund für Ihre Kündigung? Bitte rufen Sie uns an und schildern Sie uns Ihre ganz persönlichen Erfahrungen mit Telefónica.
            </div>
        </div>
        <div>
            <div style=" margin-top: 15px;margin-left:15px;">
                Selbstverständlich würden wir uns freuen, wenn Sie Ihre Kündigung noch einmal überdenken und wir Sie auch zukünftig für unsere Produkte und Services begeistern können. Gerne unterbreiten wir Ihnen auf Anfrage ein individuelles Angebot zur Vertragsverlängerung. Sie haben für ein individuelles günstiges Angebot ab dem Vertragsende weitere vier Wochen Zeit die Verträge mit allen Services weiter zu führen.
            </div>
        </div>
        <div>
            <div style=" margin-top: 15px; margin-left:15px;">
                Wir würden uns freuen, Sie zu einem späteren Zeitpunkt wieder als Kunden bei uns begrüßen zu dürfen.
            </div>
        </div>
        <div>
            <div style=" margin-top: 15px; margin-left:15px;">
                Aktuelle Informationen, Tipps und Services für Telefónica M2M-Geschäftskunden finden Sie auf
                <br/><a href="www.m2m.telefonica.de">www.m2m.telefonica.de</a> .
            </div>
        </div>
        <div>
            <div style=" margin-top: 15px;margin-left:15px;">
                Mit freundlichen Grüßen
            </div>
        </div>
        <div>
            <div style=" margin-top: 15px; margin-left:15px; ">
                Ihr M2M Team von Telefónica
            </div>
        </div>
        <div class="footer-section" style="text-align:center;">
            <apex:outputText style="font-size: 14px;" value="Telefónica Germany GmbH & Co. OHG Georg-Brauchle-Ring 23-25 80992 München Deutschland www.o2.de"/><br/><br/>
            UST-IdNr. DE 811 889 638. Amtsgericht München HRA 70343. Gesellschafter: Telefónica Germany Management GmbH, Sitz in München, Amtsgericht München HRB 109061. <br/>
            Geschäftsführer: Thorsten Dirks, Cayetano Carbajo Martín, Rachel Empey, Markus Haas, Alfons Lösing, Jesús Pérez de Uriguen, Andreas Pfisterer, Peter Rampling, <br/>
            Telefónica Deutschland Holding AG, Sitz in München, Amtsgericht München HRB 201055. Vorstand: Thorsten Dirks (Vorsitzender), Rachel Empey, Markus Haas. <br/>
            Vorsitzende des Aufsichtsrates der Telefónica Deutschland Holding AG: Eva Castillo Sanz.
        </div>
        <div class="" style="page-break-before:always;">
            <div class="name-area">
                <p><strong>Aufstellung der M2M-Anschlüsse mit Kündigungsterminen</strong></p>
            </div>
        </div>
        <div>            
            <apex:repeat value="{!TerminatedSIMs}" var="tSIM">
                <apex:dataTable value="{!tSIM}" var="sim" style="border: 1px solid #cfcfcf; text-align: center; width: 100%;" styleClass="page-break" border="1px">
                    <div class="page-break">
                        <apex:column headerValue="Rahmenvertragsnummer" value="{!sim.fc}" style="border: 1px solid #cfcfcf;"/>
                        <apex:column headerValue="SIM-Karten Nummer" value="{!sim.icc}" style="border: 1px solid #cfcfcf;"/>
                        <apex:column headerValue="Rufnummer" value="{!sim.msisdn}" style="border: 1px solid #cfcfcf;"/>
                        <apex:column headerValue="Aktivierungsdatum" value="{!sim.activationDate}" style="border: 1px solid #cfcfcf;"/>
                        <apex:column headerValue="Vertragsende" value="{!sim.terminationDate}" style="border: 1px solid #cfcfcf;"/>
                    </div>
                </apex:dataTable>
            </apex:repeat>
            
        </div>
    </div>
</apex:page>