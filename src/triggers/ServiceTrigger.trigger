trigger ServiceTrigger on csord__Service__c (after delete, after insert, after undelete, 
	after update, before delete, before insert, before update) {
	No_Trigger__c notriggers = No_Trigger__c.getInstance(UserInfo.getUserId());
	if (!notriggers.Flag__c) {
		//Logic removed to OrderTriggerHandler_AddBoniCheck
		//TriggerHandler.execute(new ServiceTriggerDelegate());
	}
}