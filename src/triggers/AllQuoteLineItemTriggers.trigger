trigger AllQuoteLineItemTriggers on QuoteLineItem (after insert) {

	No_Trigger__c notrigger = No_Trigger__c.getInstance(UserInfo.getUserId());
    if (notrigger == null || !notrigger.Flag__c) 
    {
    	if (trigger.isAfter && trigger.isInsert) {
	    	QuoteLineItemTriggerHandler.handleAfterInsert(trigger.newMap);
	    }
    }

}