trigger ProductBundleTrigger on cscfga__Product_Bundle__c (after delete, after insert, after undelete,
after update, before delete, before insert, before update) {
	No_Trigger__c notriggers = No_Trigger__c.getInstance(UserInfo.getUserId());
	system.debug(notriggers);
	if (!notriggers.Flag__c) {
		TriggerHandler.execute(new ProductBundleTriggerDelegate());
		TriggerHandler.execute(new ProductBundleTriggerDelegate_TSC());
	}
}