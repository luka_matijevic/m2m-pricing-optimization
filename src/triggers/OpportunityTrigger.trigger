trigger OpportunityTrigger on Opportunity (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	No_Trigger__c notriggers = No_Trigger__c.getInstance(UserInfo.getUserId());
	if (!notriggers.Flag__c) {
		if(!SystemUpdateFlags.NoOpportunityTrigger){
			TriggerHandler.execute(new OpportunityTriggerDelegate());
		}
	}	
}