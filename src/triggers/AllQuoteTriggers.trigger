trigger AllQuoteTriggers on Quote (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	
	No_Trigger__c notrigger = No_Trigger__c.getInstance(UserInfo.getUserId());
    if (notrigger == null || !notrigger.Flag__c) 
    {
    	if (trigger.isBefore && trigger.isInsert) {
	    	QuoteTriggerHandler.handleBeforeInsert(trigger.new);
	    }
    }
}