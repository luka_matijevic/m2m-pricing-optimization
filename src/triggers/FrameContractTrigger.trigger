/*
 * Trigger on Frame Contract Object
 */
trigger FrameContractTrigger on Frame_Contract__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) 
{
  No_Trigger__c notriggers = No_Trigger__c.getInstance(UserInfo.getUserId());
  if (!notriggers.Flag__c)
  {
    TriggerHandler.execute(new FrameContractTriggerDelegate());
  }
}