/*
* Trigger on Billing Account to execute all events
* Events: after delete, after insert, after undelete, after update, before delete, before insert, before update
*/
trigger ContactTrigger on Contact (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

    No_Trigger__c notriggers = No_Trigger__c.getInstance(UserInfo.getUserId());
    if (!notriggers.Flag__c) {
        // Handler class execution
        TriggerHandler.execute(new ContactTriggerDelegate());
    }
}