trigger AllOpportunityLineItemTriggers on OpportunityLineItem (before insert) 
{
	No_Trigger__c notrigger = No_Trigger__c.getInstance(UserInfo.getUserId());
    if (notrigger == null || !notrigger.Flag__c) 
    {
    	if (trigger.isBefore && trigger.isInsert) {
	    	OpportunityLineItemTriggerHandler.handleBeforeInsert(trigger.new);
	    }
    }
}