trigger StornoTrigger on Storno__c (before insert, after insert, before update, after update, before delete, after delete) {
No_Trigger__c notriggers = No_Trigger__c.getInstance(UserInfo.getUserId());
    if (!notriggers.Flag__c) {
        TriggerHandler.execute(new StornoTriggerDelegate());
    }
}