//------------------------------------------------------------------------------
// Interpretation of account to customer for OFI integration
//------------------------------------------------------------------------------
public with sharing class CustomerRow implements IRow
{
	//--------------------------------------------------------------------------
	// Private members
	//--------------------------------------------------------------------------
	private List<String> cells;

	//--------------------------------------------------------------------------
	// Constants
	//--------------------------------------------------------------------------
	private static final Map<String, String> paymentMethodMap = new Map<String, String>
	{
		'Direct Debit' => 'Lastschrift',
		'Bank Transfer' => 'Überweisung'
	};

	private static final Map<String, String> englishCountryMap = new Map<String, String>
	{
		'Afghanistan' => 'AF',
		'Aland Islands' => 'AX',
		'Albania' => 'AL',
		'Algeria' => 'DZ',
		'American Samoa' => 'AS',
		'Andorra' => 'AD',
		'Angola' => 'AO',
		'Anguilla' => 'AI',
		'Antarctica' => 'AQ',
		'Antigua and Barbuda' => 'AG',
		'Argentina' => 'AR',
		'Armenia' => 'AM',
		'Aruba' => 'AW',
		'Australia' => 'AU',
		'Austria' => 'AT',
		'Azerbaijan' => 'AZ',
		'Bahamas' => 'BS',
		'Bahrain' => 'BH',
		'Bangladesh' => 'BD',
		'Barbados' => 'BB',
		'Belarus' => 'BY',
		'Belgium' => 'BE',
		'Belize' => 'BZ',
		'Benin' => 'BJ',
		'Bermuda' => 'BM',
		'Bhutan' => 'BT',
		'Bolivia, Plurinational State of' => 'BO',
		'Bonaire, Sint Eustatius and Saba' => 'BQ',
		'Bosnia and Herzegovina' => 'BA',
		'Botswana' => 'BW',
		'Bouvet Island' => 'BV',
		'Brazil' => 'BR',
		'British Indian Ocean Territory' => 'IO',
		'Brunei Darussalam' => 'BN',
		'Bulgaria' => 'BG',
		'Burkina Faso' => 'BF',
		'Burundi' => 'BI',
		'Cambodia' => 'KH',
		'Cameroon' => 'CM',
		'Canada' => 'CA',
		'Cape Verde' => 'CV',
		'Cayman Islands' => 'KY',
		'Central African Republic' => 'CF',
		'Chad' => 'TD',
		'Chile' => 'CL',
		'China' => 'CN',
		'Christmas Island' => 'CX',
		'Cocos (Keeling) Islands' => 'CC',
		'Colombia' => 'CO',
		'Comoros' => 'KM',
		'Congo' => 'CG',
		'Congo, the Democratic Republic of the' => 'CD',
		'Cook Islands' => 'CK',
		'Costa Rica' => 'CR',
		'Cote d\'Ivoire' => 'CI',
		'Croatia' => 'HR',
		'Cuba' => 'CU',
		'Curaçao' => 'CW',
		'Cyprus' => 'CY',
		'Czech Republic' => 'CZ',
		'Denmark' => 'DK',
		'Djibouti' => 'DJ',
		'Dominica' => 'DM',
		'Dominican Republic' => 'DO',
		'Ecuador' => 'EC',
		'Egypt' => 'EG',
		'El Salvador' => 'SV',
		'Equatorial Guinea' => 'GQ',
		'Eritrea' => 'ER',
		'Estonia' => 'EE',
		'Ethiopia' => 'ET',
		'Falkland Islands (Malvinas)' => 'FK',
		'Faroe Islands' => 'FO',
		'Fiji' => 'FJ',
		'Finland' => 'FI',
		'France' => 'FR',
		'French Guiana' => 'GF',
		'French Polynesia' => 'PF',
		'French Southern Territories' => 'TF',
		'Gabon' => 'GA',
		'Gambia' => 'GM',
		'Georgia' => 'GE',
		'Germany' => 'DE',
		'Ghana' => 'GH',
		'Gibraltar' => 'GI',
		'Greece' => 'GR',
		'Greenland' => 'GL',
		'Grenada' => 'GD',
		'Guadeloupe' => 'GP',
		'Guam' => 'GU',
		'Guatemala' => 'GT',
		'Guernsey' => 'GG',
		'Guinea' => 'GN',
		'Guinea-Bissau' => 'GW',
		'Guyana' => 'GY',
		'Haiti' => 'HT',
		'Heard Island and McDonald Islands' => 'HM',
		'Holy See (Vatican City State)' => 'VA',
		'Honduras' => 'HN',
		'Hong Kong' => 'HK',
		'Hungary' => 'HU',
		'Iceland' => 'IS',
		'India' => 'IN',
		'Indonesia' => 'ID',
		'Iran, Islamic Republic of' => 'IR',
		'Iraq' => 'IQ',
		'Ireland' => 'IE',
		'Isle of Man' => 'IM',
		'Israel' => 'IL',
		'Italy' => 'IT',
		'Jamaica' => 'JM',
		'Japan' => 'JP',
		'Jersey' => 'JE',
		'Jordan' => 'JO',
		'Kazakhstan' => 'KZ',
		'Kenya' => 'KE',
		'Kiribati' => 'KI',
		'Korea, Democratic People\'s Republic of' => 'KP',
		'Korea, Republic of' => 'KR',
		'Kuwait' => 'KW',
		'Kyrgyzstan' => 'KG',
		'Lao People\'s Democratic Republic' => 'LA',
		'Latvia' => 'LV',
		'Lebanon' => 'LB',
		'Lesotho' => 'LS',
		'Liberia' => 'LR',
		'Libya' => 'LY',
		'Liechtenstein' => 'LI',
		'Lithuania' => 'LT',
		'Luxembourg' => 'LU',
		'Macao' => 'MO',
		'Macedonia, the former Yugoslav Republic of' => 'MK',
		'Madagascar' => 'MG',
		'Malawi' => 'MW',
		'Malaysia' => 'MY',
		'Maldives' => 'MV',
		'Mali' => 'ML',
		'Malta' => 'MT',
		'Marshall Islands' => 'MH',
		'Martinique' => 'MQ',
		'Mauritania' => 'MR',
		'Mauritius' => 'MU',
		'Mayotte' => 'YT',
		'Mexico' => 'MX',
		'Micronesia, Federated States of' => 'FM',
		'Moldova, Republic of' => 'MD',
		'Monaco' => 'MC',
		'Mongolia' => 'MN',
		'Montenegro' => 'ME',
		'Montserrat' => 'MS',
		'Morocco' => 'MA',
		'Mozambique' => 'MZ',
		'Myanmar' => 'MM',
		'Namibia' => 'NA',
		'Nauru' => 'NR',
		'Nepal' => 'NP',
		'Netherlands' => 'NL',
		'New Caledonia' => 'NC',
		'New Zealand' => 'NZ',
		'Nicaragua' => 'NI',
		'Niger' => 'NE',
		'Nigeria' => 'NG',
		'Niue' => 'NU',
		'Norfolk Island' => 'NF',
		'Northern Mariana Islands' => 'MP',
		'Norway' => 'NO',
		'Oman' => 'OM',
		'Pakistan' => 'PK',
		'Palau' => 'PW',
		'Palestine, State of' => 'PS',
		'Panama' => 'PA',
		'Papua New Guinea' => 'PG',
		'Paraguay' => 'PY',
		'Peru' => 'PE',
		'Philippines' => 'PH',
		'Pitcairn' => 'PN',
		'Poland' => 'PL',
		'Portugal' => 'PT',
		'Puerto Rico' => 'PR',
		'Qatar' => 'QA',
		'Réunion' => 'RE',
		'Romania' => 'RO',
		'Russian Federation' => 'RU',
		'Rwanda' => 'RW',
		'Saint Barthélemy' => 'BL',
		'Saint Helena, Ascension and Tristan da Cunha' => 'SH',
		'Saint Kitts and Nevis' => 'KN',
		'Saint Lucia' => 'LC',
		'Saint Martin (French part)' => 'MF',
		'Saint Pierre and Miquelon' => 'PM',
		'Saint Vincent and the Grenadines' => 'VC',
		'Samoa' => 'WS',
		'San Marino' => 'SM',
		'Sao Tome and Principe' => 'ST',
		'Saudi Arabia' => 'SA',
		'Senegal' => 'SN',
		'Serbia' => 'RS',
		'Seychelles' => 'SC',
		'Sierra Leone' => 'SL',
		'Singapore' => 'SG',
		'Sint Maarten (Dutch part)' => 'SX',
		'Slovakia' => 'SK',
		'Slovenia' => 'SI',
		'Solomon Islands' => 'SB',
		'Somalia' => 'SO',
		'South Africa' => 'ZA',
		'South Georgia and the South Sandwich Islands' => 'GS',
		'South Sudan' => 'SS',
		'Spain' => 'ES',
		'Sri Lanka' => 'LK',
		'Sudan' => 'SD',
		'Suriname' => 'SR',
		'Svalbard and Jan Mayen' => 'SJ',
		'Swaziland' => 'SZ',
		'Sweden' => 'SE',
		'Switzerland' => 'CH',
		'Syrian Arab Republic' => 'SY',
		'Taiwan, Province of China' => 'TW',
		'Tajikistan' => 'TJ',
		'Tanzania, United Republic of' => 'TZ',
		'Thailand' => 'TH',
		'Timor-Leste' => 'TL',
		'Togo' => 'TG',
		'Tokelau' => 'TK',
		'Tonga' => 'TO',
		'Trinidad and Tobago' => 'TT',
		'Tunisia' => 'TN',
		'Turkey' => 'TR',
		'Turkmenistan' => 'TM',
		'Turks and Caicos Islands' => 'TC',
		'Tuvalu' => 'TV',
		'Uganda' => 'UG',
		'Ukraine' => 'UA',
		'United Arab Emirates' => 'AE',
		'United Kingdom' => 'GB',
		'United States' => 'US',
		'United States Minor Outlying Islands' => 'UM',
		'Uruguay' => 'UY',
		'Uzbekistan' => 'UZ',
		'Vanuatu' => 'VU',
		'Venezuela, Bolivarian Republic of' => 'VE',
		'Viet Nam' => 'VN',
		'Virgin Islands, British' => 'VG',
		'Virgin Islands, U.S.' => 'VI',
		'Wallis and Futuna' => 'WF',
		'Western Sahara' => 'EH',
		'Yemen' => 'YE',
		'Zambia' => 'ZM',
		'Zimbabwe' => 'ZW'
	};

	private static final Map<String, String> germanCountryMap = new Map<String, String>
	{
		'Andorra' => 'AD',
		'Vereinigte Emirate' => 'AE',
		'Afghanistan' => 'AF',
		'Antigua und Barbuda' => 'AG',
		'Anguilla' => 'AI',
		'Albanien' => 'AL',
		'Armenien' => 'AM',
		'Niederländische Antillen' => 'AN',
		'Angola' => 'AO',
		'Antarktis' => 'AQ',
		'Argentinien' => 'AR',
		'Amerikanisch-Samoa' => 'AS',
		'Österreich' => 'AT',
		'Australien' => 'AU',
		'Aruba' => 'AW',
		'Azerbaidjan' => 'AZ',
		'Bosnien und Herzegowina' => 'BA',
		'Barbados' => 'BB',
		'Bangladesch' => 'BD',
		'Belgien' => 'BE',
		'Burkina Faso' => 'BF',
		'Bulgarien' => 'BG',
		'Bahrain' => 'BH',
		'Burundi' => 'BI',
		'Benin' => 'BJ',
		'Bermuda' => 'BM',
		'Brunei Darussalam' => 'BN',
		'Bolivien' => 'BO',
		'Brasilien' => 'BR',
		'Bahamas' => 'BS',
		'Bhutan' => 'BT',
		'Bouvet Island' => 'BV',
		'Botswana' => 'BW',
		'Belo-Rußland' => 'BY',
		'Belize' => 'BZ',
		'Kanada' => 'CA',
		'Cocos (Keeling) Inseln' => 'CC',
		'Zentralafrikanische Republik' => 'CF',
		'Kongo' => 'CG',
		'Schweiz' => 'CH',
		'Elfenbeinküste' => 'CI',
		'Cook Islands' => 'CK',
		'Chile' => 'CL',
		'Kamerun' => 'CM',
		'China' => 'CN',
		'Kolumbien' => 'CO',
		'Costa Rica' => 'CR',
		'Kuba' => 'CU',
		'Cap Verde' => 'CV',
		'Weihnachtsinseln' => 'CX',
		'Zypern' => 'CY',
		'Tschechische Republik' => 'CZ',
		'Deutschland' => 'DE',
		'Germany' => 'DE',
		'Dschibuti' => 'DJ',
		'Dänemark' => 'DK',
		'Dominica' => 'DM',
		'Dominikanische Republik' => 'DO',
		'Algerien' => 'DZ',
		'Ecuador' => 'EC',
		'Estland' => 'EE',
		'Ägypten' => 'EG',
		'Westl. Sahara' => 'EH',
		'Eritrea' => 'ER',
		'Spanien' => 'ES',
		'Äthiopien' => 'ET',
		'Finnland' => 'FI',
		'Fidschi' => 'FJ',
		'Falklandinseln (Malvinas)' => 'FK',
		'Mikronesien (Staatenföderation)' => 'FM',
		'Färöer Inseln' => 'FO',
		'Frankreich' => 'FR',
		'Frankreich (Paris)' => 'FX',
		'Gabun' => 'GA',
		'Großbritannien' => 'GB',
		'United Kingdom' => 'GB',
		'Great Britain' => 'GB',
		'Grenada' => 'GD',
		'Georgien' => 'GE',
		'Französisch-Guyana' => 'GF',
		'Ghana' => 'GH',
		'Gibraltar' => 'GI',
		'Grönland' => 'GL',
		'Gambia' => 'GM',
		'Guinea' => 'GN',
		'Guadeloupe' => 'GP',
		'Äquator-Guinea' => 'GQ',
		'Griechenland' => 'GR',
		'Südgeorgien und South Sandwich Island' => 'GS',
		'Guatemala' => 'GT',
		'Guam' => 'GU',
		'Guinea-Bissau' => 'GW',
		'Guyana' => 'GY',
		'Hongkong' => 'HK',
		'Heard Island und McDonald Islands' => 'HM',
		'Honduras' => 'HN',
		'Kroatien' => 'HR',
		'Croatia' => 'HR',
		'Haiti' => 'HT',
		'Ungarn' => 'HU',
		'Indonesien' => 'ID',
		'Irland' => 'IE',
		'Israel' => 'IL',
		'Indien' => 'IN',
		'Indischer Ozean, Britische Gebiete' => 'IO',
		'Irak' => 'IQ',
		'Iran (Islamische Republik)' => 'IR',
		'Island' => 'IS',
		'Italien' => 'IT',
		'Jamaika' => 'JM',
		'Jordanien' => 'JO',
		'Japan' => 'JP',
		'Kenia' => 'KE',
		'Kirgisistan' => 'KG',
		'Kambodscha' => 'KH',
		'Kiribati' => 'KI',
		'Komoren' => 'KM',
		'St. Kitts und Nevis' => 'KN',
		'Korea (Volksrepublik)' => 'KP',
		'Korea (Republik)' => 'KR',
		'Kuwait' => 'KW',
		'Kaiman Inseln' => 'KY',
		'Kasachstan' => 'KZ',
		'Laos Volksdemokratische Republik' => 'LA',
		'Libanon' => 'LB',
		'Saint Lucia' => 'LC',
		'Liechtenstein' => 'LI',
		'Sri Lanka' => 'LK',
		'Liberien' => 'LR',
		'Lesotho' => 'LS',
		'Lateinamerika' => 'LX',
		'Luxemburg' => 'LU',
		'Lettland' => 'LV',
		'Litauen' => 'LT',
		'Libyan Arab Jamahiriya' => 'LY',
		'Marokko' => 'MA',
		'Monaco' => 'MC',
		'Moldavische Republik' => 'MD',
		'Madagaskar' => 'MG',
		'Marshall Islands' => 'MH',
		'Makedonien' => 'MK',
		'Mali' => 'ML',
		'Myanmar' => 'MM',
		'Mongolei' => 'MN',
		'Macau' => 'MO',
		'Martinique' => 'MQ',
		'Mauritanien' => 'MR',
		'Montserrat' => 'MS',
		'Malta' => 'MT',
		'Mauritius' => 'MU',
		'Malediven' => 'MV',
		'Malawi' => 'MW',
		'Mexiko' => 'MX',
		'Malaysia' => 'MY',
		'Mozambique' => 'MZ',
		'Namibia' => 'NA',
		'Neukaledonien' => 'NC',
		'Niger' => 'NE',
		'Norfolk Inseln' => 'NF',
		'Nigeria' => 'NG',
		'Nicaragua' => 'NI',
		'Niederlande' => 'NL',
		'Norwegen' => 'NO',
		'Nepal' => 'NP',
		'Nauru' => 'NR',
		'Niue' => 'NU',
		'Neuseeland' => 'NZ',
		'Oman' => 'OM',
		'Panama' => 'PA',
		'Peru' => 'PE',
		'Französisch-Polynesien' => 'PF',
		'Papua Neu Guinea' => 'PG',
		'Philippinen' => 'PH',
		'Pakistan' => 'PK',
		'Polen' => 'PL',
		'Saint Pierre und Miquelon' => 'PM',
		'Pitcairn' => 'PN',
		'Puerto Rico' => 'PR',
		'Portugal' => 'PT',
		'Palau' => 'PW',
		'Paraguay' => 'PY',
		'Qatar' => 'QA',
		'Reunion' => 'RE',
		'Rumänien' => 'RO',
		'Russische Föderation' => 'RU',
		'Ruanda' => 'RW',
		'Saudi Arabien' => 'SA',
		'Solomon Islands' => 'SB',
		'Seychellen' => 'SC',
		'Sudan' => 'SD',
		'Schweden' => 'SE',
		'Singapur' => 'SG',
		'Sankt Helena' => 'SH',
		'Slowenien' => 'SI',
		'Svalbard und Jan Mayen' => 'SJ',
		'Slowakei' => 'SK',
		'Sierra Leone' => 'SL',
		'San Marino' => 'SM',
		'Senegal' => 'SN',
		'Somalia' => 'SO',
		'Surinam' => 'SR',
		'Sao Tome und Principe' => 'ST',
		'El Salvador' => 'SV',
		'Syrisch-Arabische Republik' => 'SY',
		'Swaziland' => 'SZ',
		'Turks und Caicos' => 'TC',
		'Tschad' => 'TD',
		'French Southern Territories' => 'TF',
		'Togo' => 'TG',
		'Thailand' => 'TH',
		'Tadschikistan' => 'TJ',
		'Tokelau' => 'TK',
		'Turkmenistan' => 'TM',
		'Tunesien' => 'TN',
		'Tonga' => 'TO',
		'Ost-Timor' => 'TP',
		'Türkei' => 'TR',
		'Trinidad und Tobago' => 'TT',
		'Tuvalu' => 'TV',
		'Taiwan, Chinesische Republik' => 'TW',
		'Tansania' => 'TZ',
		'Ukraine' => 'UA',
		'Uganda' => 'UG',
		'United States Minor Outlying Islands' => 'UM',
		'Vereinigte Staaten' => 'US',
		'Uruguay' => 'UY',
		'Usbekistan' => 'UZ',
		'Vatikan-Staat' => 'VA',
		'Saint Vincent und Grenadines' => 'VC',
		'Venezuela' => 'VE',
		'Virgin Islands (Britisch)' => 'VG',
		'Virgin Islands (US)' => 'VI',
		'Vietnam' => 'VN',
		'Vanuatu' => 'VU',
		'Wallis und Futuna Islands' => 'WF',
		'Samoa' => 'WS',
		'Jemen' => 'YE',
		'Mayotte' => 'YT',
		'Kroatien/Serbien/Slowenien/Bosnien' => 'YU',
		'Südafrika' => 'ZA',
		'Sambia' => 'ZM',
		'Zaire' => 'ZR',
		'Zimbabwe' => 'ZW'
	};

	private static final Map<String, String> languageMap = new Map<String, String>
	{
		'English' => 'E',
		'German' => 'D'
	};

	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	public CustomerRow(Billing_Account__c billingAccount)
	{
		cells = new List<String>();
		cells.add(toString(billingAccount.Account_Number__c));
		cells.add(null);
		cells.add(billingAccount.Name);
		if (billingAccount.Override_Account_Address__c)
		{
			//
			// Billing account address
			//
			cells.add(billingAccount.Billing_Street__c);
			cells.add(billingAccount.Billing_City__c);
			cells.add(billingAccount.Billing_Postcode__c);
			cells.add(toCountry(billingAccount.Billing_Country__c));
		}
		else
		{
			//
			// Account address
			//
			cells.add(billingAccount.Account_Street__c);
			cells.add(billingAccount.Account_City__c);
			cells.add(billingAccount.Account_Postcode__c);
			cells.add(toCountry(billingAccount.Account_Country__c));
		}
		cells.add(toLanguage(billingAccount.Billing_Language__c));
		cells.add(billingAccount.Payer__r.Phone);
		cells.add(billingAccount.Payer__r.Phone__c);
		cells.add(null);
		cells.add(billingAccount.Payer__r.Fax);
		cells.add(billingAccount.Payer__r.Company_Email__c);
		cells.add(billingAccount.Payer__r.Value_Added_Tax_ID__c);
		cells.add(billingAccount.Bank_Identifier__c);
		cells.add(toString(billingAccount.BIC__c));
		cells.add(billingAccount.Bank_Name__c);
		cells.add(billingAccount.Bank_Address__c);
		cells.add(billingAccount.Payment_Term__c);
		cells.add(toPaymentMethod(billingAccount.Payment_Method__c));
		cells.add(null);
		cells.add(billingAccount.Payer__r.Sales_Representative__c);
		cells.add(null);
	}

	//--------------------------------------------------------------------------
	// Get cells
	//--------------------------------------------------------------------------
	public List<String> getCells()
	{
		return cells;
	}

	//--------------------------------------------------------------------------
	// Convert decimal to string
	//--------------------------------------------------------------------------
	private static String toString(Decimal num)
	{
		if (num != null)
		{
			return String.valueOf(num);
		}
		else
		{
			return null;
		}
	}

	//--------------------------------------------------------------------------
	// Convert country
	//--------------------------------------------------------------------------
	private static String toCountry(String country)
	{
		String returnValue = englishCountryMap.get(country);
		if (returnValue == null)
		{
			returnValue = germanCountryMap.get(country);
		}
		return returnValue;
	}

	//--------------------------------------------------------------------------
	// Convert language
	//--------------------------------------------------------------------------
	private static String toLanguage(String language)
	{
		return languageMap.get(language);
	}

	//--------------------------------------------------------------------------
	// Convert payment method
	//--------------------------------------------------------------------------
	private static String toPaymentMethod(String paymentMethod)
	{
		return paymentMethodMap.get(paymentMethod);
	}
}