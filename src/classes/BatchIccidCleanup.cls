global class BatchIccidCleanup implements Database.Batchable<sObject> {
	
	private static final String query = 'Select Id, ICCID__c from Stock__c';
	private static final Pattern numberPattern = Pattern.compile('(\\d+)');  

	global BatchIccidCleanup() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		for (Stock__c sim : (List<Stock__c>)scope) {
			Matcher m = numberPattern.matcher(sim.ICCID__c);
			m.find();
			if (m.groupCount() > 0) {
				String iccid = m.group(0); 
				if (iccid.length() == 19 || iccid.length() == 20) {
					sim.ICCID__c = iccid;
				}
			}
		}
		update scope;
	}
	
	global void finish(Database.BatchableContext BC) {
		// Mortal Kombat: "Finish him!"		
	}
}