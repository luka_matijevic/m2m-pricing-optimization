global with sharing class FillDailyBillItemObject implements Database.Batchable<sObject>, Database.stateful {

	//according to "Rechnung pro Product.xlsx" document
	private static final String FINAL_PREBILL_FILE_LINE_FORMAT = '{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36}\n';
	private static final String DOT_DATE_FORMAT = 'dd.MM.yyyy';	 
	private Date billingDate;
	private String csvInstallments = '';
	private String csvOneOff = ''; 

	//T-12918 changes
	private Integer countAllPrebill=0;
	private Integer countRatePaymentPrebill=0;
	private Double sumNetAmountAll=0.00;
	private Double sumNetAmountRatePmnt=0.00;
	private Integer dataRowsRatePmnt = 0;
	private Integer dataRowsAll = 0;

	public FillDailyBillItemObject(Date billingDate){
		this.billingDate = billingDate;
	}

	global List<sObject> start(Database.BatchableContext BC) {
	

		return [
			SELECT 
				Id,
				Name,Invoice_Number__c,Daily_Bill__c,
				Transaction_Date__c,
				Billing_Account__r.Name,
				Billing_Account__r.Payer__c,
				Billing_Account__r.Payment_Term__c,
				Billing_Account__r.OFI_ID__c,
				Billing_Account__r.Account_Country__c,
				Billing_Rate_Model__c,
				//Invoice_Number__c,
				Total_Net_Amount__c,
				Total_Tax_Amount__c,
				Total_Gross_Amount__c,
				Currency_Code__c,
				Internal_Notes__c,
				(SELECT Id,
					Name,
					Line_Item_Quantity__c,
					Line_Item_Amount__c,
					Net_Amount__c,
					Tax_Amount__c,
					Gross_Amount__c,
					Tax_Code__c,
					Konto__c,
					KoSt__c,
					Prod__c,
					Unique_Identifier__c,
					I_CO__c,
					Projekt__c,
					Line_Item_Description__c,
					Order_Line_Item__r.csord__Is_Recurring__c,
					Order_Line_Item__r.Article_Info_Set__r.One_Time_Fee_Billing_Description__c,
					Order_Line_Item__r.Article_Info_Set__r.Recurring_Fee_Billing_Description__c,
					Order_Line_Item__r.csord__Order__r.Order_Request__c,
					Order_Line_Item__r.csord__Order__r.Order_Request__r.Name,
					Service_Line_Item__r.csord__Is_Recurring__c,
					Service_Line_Item__r.Article_Info_Set__r.One_Time_Fee_Billing_Description__c,
					Service_Line_Item__r.Article_Info_Set__r.Recurring_Fee_Billing_Description__c,
					Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c,
					Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Identification__c,
					Service_Line_Item__r.csord__Order_Request__r.Name,
					Is_Geotab__c,
					M2M_Order_Request__c,
					M2M_Order_Request_Name__c
				FROM 
					Prebill_Line_Items__r) 
			FROM 
				Prebill__c 
			WHERE 
				Daily_Bill__r.Date__c = :billingDate
		];
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope)  {
		
		//System.debug(FinalPrebillFileCSVGenerator.class + ' execute method started with batch size of ' + scope.size());

		// get all prebills with prebill line items that will be included in this daily bill and add them to the resulting csv string
		List<Prebill__c> billCyclePrebills = (List<Prebill__c>) scope;

		Set<Id> billingAccIdSet = new Set<Id>();
		Set<Id> pBundleSet = new Set<Id>();
		Map<Id, Id> billAccIdOrdReqIdMap = new Map<Id, Id>();
		Map<Id, Decimal> pBundleToContractTermMap = new Map<Id, Decimal>();

		for(Prebill__c prebillObj : billCyclePrebills){
			if(null!=prebillObj.Billing_Account__c){
				billingAccIdSet.add(prebillObj.Billing_Account__c);
			}
		}

		for(Order_Request__c oReq : [Select Id, Billing_Account__c, Frame_Contract__c, Frame_Contract__r.Product_Bundle__c from Order_Request__c where Billing_Account__c IN: billingAccIdSet]){
			
			billAccIdOrdReqIdMap.put(oReq.Billing_Account__c, oReq.Frame_Contract__r.Product_Bundle__c);
			pBundleSet.add(oReq.Frame_Contract__r.Product_Bundle__c);
		}

		for(cscfga__Product_Configuration__c pConf : [Select id, cscfga__Contract_Term__c, cscfga__Contract_Term_Period__c, cscfga__Product_Bundle__c from cscfga__Product_Configuration__c where cscfga__Product_Bundle__c IN: pBundleSet]){
				pBundleToContractTermMap.put(pConf.cscfga__Product_Bundle__c, pConf.cscfga__Contract_Term__c);
		}
		
		for (Prebill__c prebill : billCyclePrebills) {
			
			String country = prebill.Billing_Account__r.Account_Country__c;
			String taxString = getCountry(country) == 'EU' ? 'NStbEU' : 'SONSTNSTB'; //T-17278 - Change for non EU countries.
			
			for (Prebill_Line_Item__c pbli : prebill.Prebill_Line_Items__r) {
				
				String description = '';
				Decimal contractTerm = 0.0;
				
				if (LineItems.isFromOrderLineItem(pbli)) {
					if (pbli.Order_Line_Item__r.Article_Info_Set__r == null) {
						description = pbli.Line_Item_Description__c;
					} else if (pbli.Order_Line_Item__r.csord__Is_Recurring__c) {
						description = pbli.Order_Line_Item__r.Article_Info_Set__r.Recurring_Fee_Billing_Description__c;
					} else {
						description = pbli.Order_Line_Item__r.Article_Info_Set__r.One_Time_Fee_Billing_Description__c;
					}

					//25.2.2017. P. Matkovic
					//Added M2M Order Request number in the end of every daily bill item generated from order line items or service line items
					if (!String.isEmpty(pbli.M2M_Order_Request_Name__c)){
						description = description + ' ' + pbli.M2M_Order_Request_Name__c;
					}
				} else if (LineItems.isFromServiceLineItem(pbli)) {
					if (pbli.Service_Line_Item__r.Article_Info_Set__r == null) {
						description = pbli.Line_Item_Description__c;
					} else if (pbli.Service_Line_Item__r.csord__Is_Recurring__c) {
						description = pbli.Service_Line_Item__r.Article_Info_Set__r.Recurring_Fee_Billing_Description__c;
					} else {
						description = pbli.Service_Line_Item__r.Article_Info_Set__r.One_Time_Fee_Billing_Description__c;
					}

					//25.2.2017. P. Matkovic
					//Added M2M Order Request number in the end of every daily bill item generated from order line items or service line items
					if (!String.isEmpty(pbli.M2M_Order_Request_Name__c)){
						description = description + ' ' + pbli.M2M_Order_Request_Name__c; 
					}
				} else {
					description = pbli.Line_Item_Description__c;
				}

				/*
				if (pbli.Is_Geotab__c){
					String[] orderRequestNumberFromPbliArray = pbli.Line_Item_Description__c.split('Geotab Purchase Order Number:');
					if (orderRequestNumberFromPbliArray != null && orderRequestNumberFromPbliArray.size() > 1){
						description = description + ':' + orderRequestNumberFromPbliArray[1];
					}
				}*/
			
				if (BillingRateModels.billingRateModel(prebill.Billing_Rate_Model__c) == BillingRateModel.INSTALLMENTS) {
					contractTerm = billAccIdOrdReqIdMap.get(prebill.Billing_Account__c)==null?0.0: 
						(pBundleToContractTermMap.get(billAccIdOrdReqIdMap.get(prebill.Billing_Account__c))==null?0.0:
						pBundleToContractTermMap.get(billAccIdOrdReqIdMap.get(prebill.Billing_Account__c)));
				}   
				
				Daily_Bill_Item__c dbi = new Daily_Bill_Item__c();
				dbi.Rechnungsnummer__c=prebill.Invoice_Number__c;
				dbi.VO_Nr__c = prebill.Billing_Account__r.OFI_ID__c;
				dbi.Art__c=6;
				dbi.Menge__c=pbli.Line_Item_Quantity__c;
				dbi.Betrag__c=pbli.Line_Item_Amount__c;
				dbi.Einzel_Nettobetrag__c=pbli.Net_Amount__c;
				dbi.Einzel_Taxbetrag__c=pbli.Tax_Amount__c;
				dbi.Einzel_Bruttobetrag__c=pbli.Gross_Amount__c;
				dbi.Gesamt_Nettobetrag__c=prebill.Total_Net_Amount__c;
				dbi.Gesamt_Taxbetrag__c=prebill.Total_Tax_Amount__c;
				dbi.Gesamt_Bruttobetrag__c=prebill.Total_Gross_Amount__c;
				dbi.Waehrung__c='EUR';
				dbi.Steuersatz__c= CSV.ofi(pbli.Tax_Code__c) == '0%' ? 0 : 19;
				dbi.Konto__c=pbli.Konto__c;
				dbi.KoSt__c=pbli.KoSt__c;
				dbi.Prod__c=pbli.Prod__c;
				dbi.Unique_Identifier__c=pbli.Unique_Identifier__c;
				dbi.I_CO__c=pbli.I_CO__c;
				dbi.Projekt__c=CSV.ofi(OfiUtils.isProjektZero(pbli) ? '' : pbli.Projekt__c);
				dbi.Zahlungsbedingung__c='30 Tage netto.';
				dbi.Anweisung__c=CSV.escape('Abrechnungszeitraum ' + DateUtils.formatDate(billingDate.addMonths(-1), DOT_DATE_FORMAT) + ' bis ' + DateUtils.formatDate(billingDate.addDays(-1), DOT_DATE_FORMAT));
				dbi.Daily_Bill__c=prebill.Daily_Bill__c;
				dbi.M2M_Order_Request__c = LineItems.getM2MOrderRequest(pbli);
				dbi.Beschreibung__c = LineItems.isConnectivitySubscription(pbli) ? description + ' - ' + LineItems.returnM2MOrderRequestName(pbli) : description;
				//12.2.2017. Petar Matkovic added lookup to billing account
				dbi.Billing_Account__c = prebill.Billing_Account__c;
				//27.2.2017. Petar Markovic added Id of origin prebill line item in Daily bill item
				dbi.Origin_Prebill_Line_Item_Id__c = String.valueOf(pbli.Id);
				insert dbi;    
			    
			}
		
		}
		
	}
	
	global void finish(Database.BatchableContext BC){

           Database.executeBatch(new FinalPrebillFileCSVGenerator(this.billingDate));
	}

	private String dailyBillName(Date billingDate, BillingRateModel rateModel) {
		
		String name = 'M2M_SF_INVOICE';
		if (rateModel == BillingRateModel.ONEOFF) {
			name += '_';
		} else if (rateModel == BillingRateModel.INSTALLMENTS) {
			name += '_RATE_PAYMENT_';
		}
		name += Datetime.newInstance(billingdate, Time.newInstance(0, 0, 0, 0)).format('ddMMYYYY');
		name += '_1.csv';
		return name;
	}

	private String ofiHeaderRow() {
		return String.format(FINAL_PREBILL_FILE_LINE_FORMAT,
			new List<String>{
				'LINETYPE',
				'RE-Nr.'.escapeCsv(),
				'RE-Datum',
				'Buchungsdatum',
				'VO-Nr.',
				'Beschreibung',
				'Art',
				'Menge',
				'Betrag',
				'Einzel-Nettobetrag',
				'Einzel-Taxbetrag',
				'Einzel-Bruttobetrag',
				'Gesamt-Nettobetrag',
				'Gesamt-Taxbetrag',
				'Gesamt-Bruttobetrag',
				'Waehrung',
				'Steuersatz',
				'Konto',
				'KoSt',
				'Prod',
				'Unique Identifier',
				'I/CO',
				'Projekt',
				'Zahlungsbedingung',
				'BA-Nr',
				'CurConvRate',
				'Anweisung',
				'Fakturierungsregeln',
				'Buchungsregeln',
				'Vom_Datum',
				'Adresse',
				'Bestellnummer',
				'RATE_PLAN_MONTH',
				'RATE_PLAN_START_DATE',
				'RATE_PLAN_AMOUNT',
				'RATE_PLAN_MISC',
				'Order request'
			});
	}
	
	private String prependOfiHeader(String text) {
		return (text == null) ? ofiHeaderRow() : ofiHeaderRow() + text;
	}
	
	private static String getCountry(String country) {
		
	
		System.debug('Country: ' + country);
		
		//T-17278 - Change for non EU countries - custom setting
		List<EU_Countries__c> euCountries = [SELECT Name FROM EU_Countries__c WHERE Name=:country];
		if (euCountries.size() == 0)
		{
			return 'Non-EU';
		}
		else
		{
			return 'EU';
		}
	}
    
}