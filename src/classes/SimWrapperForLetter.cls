public class SimWrapperForLetter{
    public String fc {get; set;}
    public String icc {get; set;}
    public String msisdn {get; set;}
    public String terminationDate {get; set;}
    public String activationDate {get; set;}
    
    public SimWrapperForLetter(Stock__c sim, String germanDate){
        this.fc = sim.BIC_Framework_Contract_No__c;
        this.icc = sim.ICCID__c;
        this.msisdn = sim.MSISDN__c;
        this.terminationDate = germanDate;
        if (sim.Commercial_Date__c != null)
            this.activationDate = convertDateToGermanFormat(Date.valueOf(sim.Commercial_Date__c));
        else
            this.activationDate = convertDateToGermanFormat(Date.valueOf(sim.Activation_Date__c));
    }

    public static final Map<Integer, String> germanMonthNames = new Map<Integer, String>{1 => 'Januar', 2 => 'Februar', 3 => 'März', 4 => 'April', 5 => 'Mai', 6 => 'Juni', 7 => 'Juli', 8 => 'August', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Dezember'};
    
    public String convertDateToGermanFormat(Date reqDate){
        Integer day, month, year;
        String germanDate = '';
        try{
            if(reqDate != null){
                day = reqDate.day();
                month = reqDate.month();
                year = reqDate.year();
                if(!germanMonthNames.isEmpty() && germanMonthNames.containsKey(month)){
                    germanDate = day + '. ' + germanMonthNames.get(month) + ' ' + year;
                }
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
        return germanDate;
    }
}