global class ErrorStatusInboundProcessor implements csam_t1.InboundMessageProcessor {

	/**
	* @description Accepts messages with URL path ends with <em>error</em>
	* @param incomingMessage message to be processed
	* @return Boolean returns true if message URL path ends with <em>error</em>
	*/
	public Boolean isMessageProcessible(csam_t1__Incoming_Message__c incomingMessage) {
		return isLegacyErrorPath(incomingMessage.csam_t1__Incoming_URL_Path__c) || isErrorPath(incomingMessage.csam_t1__Incoming_URL_Path__c);
	}

	/**
	* @description Changes Incoming Message record status to <em>Integration Error</em>
	* @param incomingMessage message to be processed with corresponding payload attachment
	* @param oldToNewIdsMap map of old-to-new ids used when inserting in chunks to pass the references between messages
	*/
	public void processMessage(csam_t1__Incoming_Message__c incomingMessage, Map<Id, Id> oldToNewIdsMap) {
		
		// this should best be split between various error inbound processors
		
		String urlPath = incomingMessage.csam_t1__Incoming_URL_Path__c;
				
		incomingMessage.csam_t1__Status__c = 'Integration Error';
		incomingMessage.csam_t1__Processed_At__c = System.now();
		update incomingMessage;

		EmailHandler emailHandler = new EmailHandler();

		if (isLegacyErrorPath(urlPath)) {
			
			SM2MErrorMessageParser errorParser = new SM2MErrorMessageParser(incomingMessage);
			String status = 'error';
	
			if (errorParser.hasUnrecoverableException()) {
				status = errorParser.getUnrecoverableException();
			} else if (errorParser.hasSM2MError()) {
				status = errorParser.getSM2MError();
			}
			
			InboundProcessorHelper.updateStatuses(incomingMessage, status);

			csam_t1__Outgoing_Message__c outMsg = InboundProcessorHelper.outgoingMessage(incomingMessage);
			
			if (outMsg.csam_t1__URL_File__c.contains('subscriptions/sim-status')) {
				emailHandler.send(new IntegrationAlarms.SimStatus(incomingMessage));
			}
			
		} else if (isErrorPath(urlPath, '/prebills/retrieve')) {
			
			InboundProcessorHelper.updateStatuses(incomingMessage, 'Billing error');
			emailHandler.send(new IntegrationAlarms.PrebillRetrieve(incomingMessage));
		} else if (isErrorPath(urlPath, '/dailybills/upload')) {
			InboundProcessorHelper.updateStatuses(incomingMessage, 'Daily Bill upload error');
		}
	}
	
	private Boolean isLegacyErrorPath(String incomingUrlPath) {
		return incomingUrlPath.endsWith('/error');
	}
	
	private Boolean isErrorPath(String incomingUrlPath) {
		return incomingUrlPath.contains('/error/');
	}
	
	private Boolean isErrorPath(String incomingUrlPath, String ending) {
		String part = ending.startsWith('/') ? ending : '/' + ending;
		return isErrorPath(incomingUrlPath) && incomingUrlPath.endsWith(part);
	}
}