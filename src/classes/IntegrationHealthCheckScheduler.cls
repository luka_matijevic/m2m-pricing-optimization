global class IntegrationHealthCheckScheduler implements Schedulable {
	// Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
	public static final String CRON_EXP = '0 0 * * * ? *';
	
	global static String scheduleIt() {
		return System.schedule('Integration Health Check', CRON_EXP, new IntegrationHealthCheckScheduler());
	}
	
	global void execute(SchedulableContext sc)	{
	
		List<Job_Alarm__c> alarms = Job_Alarm__c.getAll().values();

		DateTime lastHour = (DateTime.now()).addHours(-1);
		Set<String> lastHourJobs = jobsSince(lastHour);
		
		for (Job_Alarm__c alarm : alarms) {
			if (alarm.Active__c && !String.isEmpty(alarm.Email__c) && !lastHourJobs.contains(alarm.Job_Name__c)) {
				Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
				//email.setToAddresses(new String[] { alarm.Email__c });
				//26.5.2016. Petar Matkovic added multiple emails splitted with character ';'
				String[] emailAddresses = alarm.Email__c.split(';');
				email.setToAddresses(emailAddresses);
				email.setSubject('Alarm for ' + alarm.Name);
				email.setPlainTextBody('The automatic schedule for ' + alarm.Name + ' has not been fired in the last hour. Please check the scheduled jobs.');

				Messaging.SendEmailResult[] result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
			}
		}
	}
	
	private String trimTime(String jobName) {
		return jobName.split('_')[0];
	}

	private Set<String> jobsSince(Datetime dt) {
		List<CronTrigger> cronTriggers = [Select Id, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType from CronTrigger where CreatedDate > :dt];
		
		Set<String> jobsSince = new Set<String>();
		for (CronTrigger ct : cronTriggers) {
			jobsSince.add(trimTime(ct.CronJobDetail.Name));
		}
		return jobsSince;
	}
}