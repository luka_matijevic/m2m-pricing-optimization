global class MsisdnIterable implements iterable<MSISDN__c>{ 
    
	long rangeStart;
    long rangeEnd;
    Id rangeID;
    public MsisdnIterable(long rangeStart, long rangeEnd, Id rangeID)
   	{
   		this.rangeStart = rangeStart;
   		this.rangeEnd = rangeEnd;
        this.rangeID = rangeID;
   	}
    
    global Iterator<MSISDN__c> Iterator(){
        return new MsisdnIterator(rangeStart, rangeEnd, rangeID);
    }

}