@isTest
public class IoTOppController_Test {

    
  
    public static testMethod void NewIOTConnect(){
        
        list<EU_Countries__c> countries = Test_Util.createCountries();
	    insert countries;
        
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers; 
        
        
        Account account = new Account(
        	Name = 'Test3',
            Type = 'Business'
        	);
        	
        insert account;
        
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1),
            Description = 'Description must have at least 20 characters',
			AccountId = account.Id
		);
		
		insert opp;
        
        cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(
			cscfga__Synchronised_with_Opportunity__c = false,
			cscfga__Opportunity__c = opp.Id
		);
		
		insert productBundle;
        
        list<cscfga__Product_Definition__c> productDefinitionLst = new list<cscfga__Product_Definition__c>();
        
        
        cscfga__Product_Definition__c productDefinition = Test_Util.createProdDefination('IoT Connect','Test IoT Connect');
		productDefinitionLst.add(productDefinition);
        
        insert productDefinitionLst;
        
        list<cscfga__Attribute_Definition__c> attrDefinationsLst = Test_Util.attrDefinations(new list<id>{productDefinition.id});
		insert attrDefinationsLst;
        
        cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c(
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinition.Id,
			name = 'IoT Connect'
		);
		
		
		insert productConfiguration;
        
        Article_Info_Set__c articleInfoSet = new Article_Info_Set__c(name ='Article Info set',
		                                                             Article_Type__c='SIM',
		                                                             Global_Local__c='Global',
		                                                             Status__c='Active');
		insert articleInfoSet;
        
        map<string,cscfga__Attribute_Definition__c> attrDefinations = Test_Util.attrDefinations(productDefinition.Id);
        list<cscfga__Attribute__c> attLst = new list<cscfga__Attribute__c>();
        attLst.add(Test_Util.createAttrDef('Article Info Set',productConfiguration.id,articleInfoSet.id,attrDefinations.get('Article Info Set').id ));
        attLst.add(Test_Util.createAttrDef('Tariffa',productConfiguration.id,'tariffId',attrDefinations.get('Tariffa').id ));
        attLst.add(Test_Util.createAttrDef('Pack price',productConfiguration.id,'10',attrDefinations.get('Pack price').id ));
        attLst.add(Test_Util.createAttrDef('Activation fee',productConfiguration.id,'10',attrDefinations.get('Activation fee').id ));
        attLst.add(Test_Util.createAttrDef('Data per sim',productConfiguration.id,'5',attrDefinations.get('Data per sim').id ));
        attLst.add(Test_Util.createAttrDef('Discount value',productConfiguration.id,'1',attrDefinations.get('Discount value').id ));
        attLst.add(Test_Util.createAttrDef('Contract Term',productConfiguration.id,'24',attrDefinations.get('Contract Term').id ));
        attLst.add(Test_Util.createAttrDef('Dynamic pool',productConfiguration.id,'Yes',attrDefinations.get('Dynamic pool').id ));
        attLst.add(Test_Util.createAttrDef('Connect Plus',productConfiguration.id,'Yes',attrDefinations.get('Connect Plus').id ));
        attLst.add(Test_Util.createAttrDef('LTE Option',productConfiguration.id,'Yes',attrDefinations.get('LTE Option').id ));
        attLst.add(Test_Util.createAttrDef('Quantity',productConfiguration.id,'100',attrDefinations.get('Quantity').id ));
        attLst.add(Test_Util.createAttrDef('Discount',productConfiguration.id,'0',attrDefinations.get('Discount').id ));
       
        insert attLst; 
        notriggers.Flag__c = false;
        update notriggers;
        Test.startTest();
            PageReference pageRefr = Page.IoTOpportunity;
            pageRefr.getParameters().put('oppId',opp.Id);
            pageRefr.getParameters().put('accId',account.Id); 
            
        	test.setCurrentPage(pageRefr);
            IoTOppController ctrl = new IoTOppController (new ApexPages.StandardController(new cscfga__Product_Bundle__c()));
        	ctrl.init();
       	test.stopTest();
         
    }
    
    public static testMethod void editIOTConnect1(){
        
        list<EU_Countries__c> countries = Test_Util.createCountries();
	    insert countries;
        
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers; 
        
        
        Account account = new Account(
        	Name = 'Test3',
            Type = 'Business',
            Billing_Cycle__c = '1',
            VAT__c = 14
        	);
        	
        insert account;
        
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1),
            Description = 'Description must have at least 20 characters',
			AccountId = account.Id
		);
		insert (new Frame_Contract__c(opportunity__c = opp.id,Customer__c = account.id));
		insert opp;
        
        cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(
			cscfga__Synchronised_with_Opportunity__c = false,
			cscfga__Opportunity__c = opp.Id
		);
		
		insert productBundle;
        
        list<cscfga__Product_Definition__c> productDefinitionLst = new list<cscfga__Product_Definition__c>();
        
        
        cscfga__Product_Definition__c productDefinition = Test_Util.createProdDefination('IoT Connect','Test IoT Connect');
		productDefinitionLst.add(productDefinition);
        
        insert productDefinitionLst;
        
        list<cscfga__Attribute_Definition__c> attrDefinationsLst = Test_Util.attrDefinations(new list<id>{productDefinition.id});
		insert attrDefinationsLst;
        
        cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c(
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinition.Id,
			name = 'IoT Connect'
		);
		
		
		insert productConfiguration;
        Id simRecordtypeId = Schema.SObjectType.Article_Info_Set__c.getRecordTypeInfosByName().get('SIM').getRecordTypeId();
        Article_Info_Set__c articleInfoSet = new Article_Info_Set__c(name ='Article Info set',
		                                                             Article_Type__c='SIM',
		                                                             Global_Local__c='Global',
		                                                             Status__c='Active',
                                                                    recordTypeId= simRecordtypeId);
		insert articleInfoSet;
        
        map<string,cscfga__Attribute_Definition__c> attrDefinations = Test_Util.attrDefinations(productDefinition.Id);
        list<cscfga__Attribute__c> attLst = new list<cscfga__Attribute__c>();
        attLst.add(Test_Util.createAttrDef('Article Info Set',productConfiguration.id,articleInfoSet.id,attrDefinations.get('Article Info Set').id ));
        attLst.add(Test_Util.createAttrDef('Tariffa',productConfiguration.id,'tariffId',attrDefinations.get('Tariffa').id ));
        attLst.add(Test_Util.createAttrDef('Pack price',productConfiguration.id,'10',attrDefinations.get('Pack price').id ));
        attLst.add(Test_Util.createAttrDef('Activation fee',productConfiguration.id,'10',attrDefinations.get('Activation fee').id ));
        attLst.add(Test_Util.createAttrDef('Data per sim',productConfiguration.id,'5',attrDefinations.get('Data per sim').id ));
        attLst.add(Test_Util.createAttrDef('Discount value',productConfiguration.id,'1',attrDefinations.get('Discount value').id ));
        attLst.add(Test_Util.createAttrDef('Contract Term',productConfiguration.id,'24',attrDefinations.get('Contract Term').id ));
        attLst.add(Test_Util.createAttrDef('Dynamic pool',productConfiguration.id,'Yes',attrDefinations.get('Dynamic pool').id ));
        attLst.add(Test_Util.createAttrDef('Connect Plus',productConfiguration.id,'Yes',attrDefinations.get('Connect Plus').id ));
        attLst.add(Test_Util.createAttrDef('LTE Option',productConfiguration.id,'Yes',attrDefinations.get('LTE Option').id ));
        attLst.add(Test_Util.createAttrDef('Quantity',productConfiguration.id,'100',attrDefinations.get('Quantity').id ));
        attLst.add(Test_Util.createAttrDef('Discount',productConfiguration.id,'0',attrDefinations.get('Discount').id ));
       
        insert attLst; 
        notriggers.Flag__c = false;
        update notriggers;
        Test.startTest();
            PageReference pageRefr = Page.IoTOpportunity;
            pageRefr.getParameters().put('oppId',opp.Id);
            pageRefr.getParameters().put('accId',account.Id); 
            pageRefr.getParameters().put('id',productBundle.Id); 
        	test.setCurrentPage(pageRefr);
            IoTOppController ctrl = new IoTOppController (new ApexPages.StandardController(productBundle));
        	ctrl.init();
        	ctrl.approvalStatusRequired();
        	ctrl.quicksaveBundle();

       	test.stopTest(); 
         
    }
    
    public static testMethod void editIOTConnect2(){
        
        list<EU_Countries__c> countries = Test_Util.createCountries();
	    insert countries;
        
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers; 
        
        
        Account account = new Account(
        	Name = 'Test3',
            Type = 'Business',
            Billing_Cycle__c = '1',
            VAT__c = 14
        	);
        	
        insert account;
        
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1),
            Description = 'Description must have at least 20 characters',
			AccountId = account.Id
		);
		insert (new Frame_Contract__c(opportunity__c = opp.id,Customer__c = account.id));
		insert opp;
        
        cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(
			cscfga__Synchronised_with_Opportunity__c = false,
			cscfga__Opportunity__c = opp.Id
		);
		
		insert productBundle;
        
        list<cscfga__Product_Definition__c> productDefinitionLst = new list<cscfga__Product_Definition__c>();
        
        
        cscfga__Product_Definition__c productDefinition = Test_Util.createProdDefination('IoT Connect','Test IoT Connect');
		productDefinitionLst.add(productDefinition);
        
        insert productDefinitionLst;
        
        list<cscfga__Attribute_Definition__c> attrDefinationsLst = Test_Util.attrDefinations(new list<id>{productDefinition.id});
		insert attrDefinationsLst;
        
        cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c(
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinition.Id,
			name = 'IoT Connect'
		);
		
		
		insert productConfiguration;
        Id simRecordtypeId = Schema.SObjectType.Article_Info_Set__c.getRecordTypeInfosByName().get('SIM').getRecordTypeId();
        Article_Info_Set__c articleInfoSet = new Article_Info_Set__c(name ='Article Info set',
		                                                             Article_Type__c='SIM',
		                                                             Global_Local__c='Global',
		                                                             Status__c='Active',
                                                                    recordTypeId= simRecordtypeId);
		insert articleInfoSet;
        
        map<string,cscfga__Attribute_Definition__c> attrDefinations = Test_Util.attrDefinations(productDefinition.Id);
        list<cscfga__Attribute__c> attLst = new list<cscfga__Attribute__c>();
        attLst.add(Test_Util.createAttrDef('Article Info Set',productConfiguration.id,articleInfoSet.id,attrDefinations.get('Article Info Set').id ));
        attLst.add(Test_Util.createAttrDef('Tariffa',productConfiguration.id,'tariffId',attrDefinations.get('Tariffa').id ));
        attLst.add(Test_Util.createAttrDef('Pack price',productConfiguration.id,'10',attrDefinations.get('Pack price').id ));
        attLst.add(Test_Util.createAttrDef('Activation fee',productConfiguration.id,'10',attrDefinations.get('Activation fee').id ));
        attLst.add(Test_Util.createAttrDef('Data per sim',productConfiguration.id,'5',attrDefinations.get('Data per sim').id ));
        attLst.add(Test_Util.createAttrDef('Discount value',productConfiguration.id,'1',attrDefinations.get('Discount value').id ));
        attLst.add(Test_Util.createAttrDef('Contract Term',productConfiguration.id,'24',attrDefinations.get('Contract Term').id ));
        attLst.add(Test_Util.createAttrDef('Dynamic pool',productConfiguration.id,'Yes',attrDefinations.get('Dynamic pool').id ));
        attLst.add(Test_Util.createAttrDef('Connect Plus',productConfiguration.id,'Yes',attrDefinations.get('Connect Plus').id ));
        attLst.add(Test_Util.createAttrDef('LTE Option',productConfiguration.id,'Yes',attrDefinations.get('LTE Option').id ));
        attLst.add(Test_Util.createAttrDef('Quantity',productConfiguration.id,'100',attrDefinations.get('Quantity').id ));
        attLst.add(Test_Util.createAttrDef('Discount',productConfiguration.id,'0',attrDefinations.get('Discount').id ));
       
        insert attLst; 
        notriggers.Flag__c = false;
        update notriggers;
        Test.startTest();
            PageReference pageRefr = Page.IoTOpportunity;
            pageRefr.getParameters().put('oppId',opp.Id);
            pageRefr.getParameters().put('accId',account.Id); 
            pageRefr.getParameters().put('id',productBundle.Id); 
        	test.setCurrentPage(pageRefr);
            IoTOppController ctrl = new IoTOppController (new ApexPages.StandardController(productBundle));
        	ctrl.init();
        	
            try{
                //ctrl.saveBundle();
                ctrl.saveBundleAndCreateContract();    	
            }catch(exception e){
                
            }
            
        	ctrl.saveBundle();
       	test.stopTest(); 
         
    }
    
    public static testMethod void budnleApproval1(){
        
        list<EU_Countries__c> countries = Test_Util.createCountries();
	    insert countries;
        
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers; 
        
        
        Account account = new Account(
        	Name = 'Test3',
            Type = 'Business',
            Billing_Cycle__c = '1',
            VAT__c = 14
        	);
        	
        insert account;
        
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1),
            Description = 'Description must have at least 20 characters',
			AccountId = account.Id
		);
		insert (new Frame_Contract__c(opportunity__c = opp.id,Customer__c = account.id));
		insert opp;
        
        cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(
			cscfga__Synchronised_with_Opportunity__c = false,
			cscfga__Opportunity__c = opp.Id,
            IOT_Approval_Status__c = 'Required'
		);
		
		insert productBundle;
        
        list<cscfga__Product_Definition__c> productDefinitionLst = new list<cscfga__Product_Definition__c>();
        
        
        cscfga__Product_Definition__c productDefinition = Test_Util.createProdDefination('IoT Connect','Test IoT Connect');
		productDefinitionLst.add(productDefinition);
        
        insert productDefinitionLst;
        
        list<cscfga__Attribute_Definition__c> attrDefinationsLst = Test_Util.attrDefinations(new list<id>{productDefinition.id});
		insert attrDefinationsLst;
        
        cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c(
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinition.Id,
			name = 'IoT Connect'
		);
		
		
		insert productConfiguration;
        Id simRecordtypeId = Schema.SObjectType.Article_Info_Set__c.getRecordTypeInfosByName().get('SIM').getRecordTypeId();
        Article_Info_Set__c articleInfoSet = new Article_Info_Set__c(name ='Article Info set',
		                                                             Article_Type__c='SIM',
		                                                             Global_Local__c='Global',
		                                                             Status__c='Active',
                                                                    recordTypeId= simRecordtypeId);
		insert articleInfoSet;
        
        map<string,cscfga__Attribute_Definition__c> attrDefinations = Test_Util.attrDefinations(productDefinition.Id);
        list<cscfga__Attribute__c> attLst = new list<cscfga__Attribute__c>();
        attLst.add(Test_Util.createAttrDef('Article Info Set',productConfiguration.id,articleInfoSet.id,attrDefinations.get('Article Info Set').id ));
        attLst.add(Test_Util.createAttrDef('Tariffa',productConfiguration.id,'tariffId',attrDefinations.get('Tariffa').id ));
        attLst.add(Test_Util.createAttrDef('Pack price',productConfiguration.id,'10',attrDefinations.get('Pack price').id ));
        attLst.add(Test_Util.createAttrDef('Activation fee',productConfiguration.id,'10',attrDefinations.get('Activation fee').id ));
        attLst.add(Test_Util.createAttrDef('Data per sim',productConfiguration.id,'5',attrDefinations.get('Data per sim').id ));
        attLst.add(Test_Util.createAttrDef('Discount value',productConfiguration.id,'1',attrDefinations.get('Discount value').id ));
        attLst.add(Test_Util.createAttrDef('Contract Term',productConfiguration.id,'24',attrDefinations.get('Contract Term').id ));
        attLst.add(Test_Util.createAttrDef('Dynamic pool',productConfiguration.id,'Yes',attrDefinations.get('Dynamic pool').id ));
        attLst.add(Test_Util.createAttrDef('Connect Plus',productConfiguration.id,'Yes',attrDefinations.get('Connect Plus').id ));
        attLst.add(Test_Util.createAttrDef('LTE Option',productConfiguration.id,'Yes',attrDefinations.get('LTE Option').id ));
        attLst.add(Test_Util.createAttrDef('Quantity',productConfiguration.id,'100',attrDefinations.get('Quantity').id ));
        attLst.add(Test_Util.createAttrDef('Discount',productConfiguration.id,'0',attrDefinations.get('Discount').id ));
        
        insert attLst; 
        notriggers.Flag__c = false;
    
        update notriggers;
      
        Test.startTest();
          /*  PageReference pageRefr = Page.IoTOpportunity;
            pageRefr.getParameters().put('oppId',opp.Id);
            pageRefr.getParameters().put('accId',account.Id); 
            pageRefr.getParameters().put('id',productBundle.Id);
  
        	test.setCurrentPage(pageRefr);
            
            IoTOppController ctrl = new IoTOppController (new ApexPages.StandardController(productBundle));
        	ctrl.init(); //puca
            
        	ctrl.approvalStatusRequired(); 
        	ctrl.quicksaveBundle();
        	ctrl.updateDiscount();
        	ctrl.submitRecordApproval();
        	ctrl.init();
        	ctrl.approveBundle();*/
             
 Test.stopTest();
      
         
    }
    public static testMethod void budnleApproval2(){
        
        list<EU_Countries__c> countries = Test_Util.createCountries();
	    insert countries;
        
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers; 
        
        
        Account account = new Account(
        	Name = 'Test3',
            Type = 'Business',
            Billing_Cycle__c = '1',
            VAT__c = 14
        	);
        	
        insert account;
        
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1),
            Description = 'Description must have at least 20 characters',
			AccountId = account.Id
		);
		insert (new Frame_Contract__c(opportunity__c = opp.id,Customer__c = account.id));
		insert opp;
        
        cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(
			cscfga__Synchronised_with_Opportunity__c = false,
			cscfga__Opportunity__c = opp.Id,
            IOT_Approval_Status__c = 'Required'
		);
		
		insert productBundle;
        
        list<cscfga__Product_Definition__c> productDefinitionLst = new list<cscfga__Product_Definition__c>();
        
        
        cscfga__Product_Definition__c productDefinition = Test_Util.createProdDefination('IoT Connect','Test IoT Connect');
		productDefinitionLst.add(productDefinition);
        
        insert productDefinitionLst;
        
        list<cscfga__Attribute_Definition__c> attrDefinationsLst = Test_Util.attrDefinations(new list<id>{productDefinition.id});
		insert attrDefinationsLst;
        
        cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c(
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinition.Id,
			name = 'IoT Connect'
		);
		
		
		insert productConfiguration;
        Id simRecordtypeId = Schema.SObjectType.Article_Info_Set__c.getRecordTypeInfosByName().get('SIM').getRecordTypeId();
        Article_Info_Set__c articleInfoSet = new Article_Info_Set__c(name ='Article Info set',
		                                                             Article_Type__c='SIM',
		                                                             Global_Local__c='Global',
		                                                             Status__c='Active',
                                                                    recordTypeId= simRecordtypeId);
		insert articleInfoSet;
        
        map<string,cscfga__Attribute_Definition__c> attrDefinations = Test_Util.attrDefinations(productDefinition.Id);
        list<cscfga__Attribute__c> attLst = new list<cscfga__Attribute__c>();
        attLst.add(Test_Util.createAttrDef('Article Info Set',productConfiguration.id,articleInfoSet.id,attrDefinations.get('Article Info Set').id ));
        attLst.add(Test_Util.createAttrDef('Tariffa',productConfiguration.id,'tariffId',attrDefinations.get('Tariffa').id ));
        attLst.add(Test_Util.createAttrDef('Pack price',productConfiguration.id,'10',attrDefinations.get('Pack price').id ));
        attLst.add(Test_Util.createAttrDef('Activation fee',productConfiguration.id,'10',attrDefinations.get('Activation fee').id ));
        attLst.add(Test_Util.createAttrDef('Data per sim',productConfiguration.id,'5',attrDefinations.get('Data per sim').id ));
        attLst.add(Test_Util.createAttrDef('Discount value',productConfiguration.id,'1',attrDefinations.get('Discount value').id ));
        attLst.add(Test_Util.createAttrDef('Contract Term',productConfiguration.id,'24',attrDefinations.get('Contract Term').id ));
        attLst.add(Test_Util.createAttrDef('Dynamic pool',productConfiguration.id,'Yes',attrDefinations.get('Dynamic pool').id ));
        attLst.add(Test_Util.createAttrDef('Connect Plus',productConfiguration.id,'Yes',attrDefinations.get('Connect Plus').id ));
        attLst.add(Test_Util.createAttrDef('LTE Option',productConfiguration.id,'Yes',attrDefinations.get('LTE Option').id ));
        attLst.add(Test_Util.createAttrDef('Quantity',productConfiguration.id,'100',attrDefinations.get('Quantity').id ));
        attLst.add(Test_Util.createAttrDef('Discount',productConfiguration.id,'0',attrDefinations.get('Discount').id ));
       
        insert attLst; 
        notriggers.Flag__c = false;
        update notriggers;
        Test.startTest();
            PageReference pageRefr = Page.IoTOpportunity;
            pageRefr.getParameters().put('oppId',opp.Id);
            pageRefr.getParameters().put('accId',account.Id); 
            pageRefr.getParameters().put('id',productBundle.Id); 
        	test.setCurrentPage(pageRefr);
            IoTOppController ctrl = new IoTOppController (new ApexPages.StandardController(productBundle));
        	/*ctrl.init();
        	ctrl.approvalStatusRequired();
        	ctrl.quicksaveBundle();
        	ctrl.updateDiscount();
        	ctrl.submitRecordApproval();
        	ctrl.init();
        	ctrl.rejectBundle();
        	ctrl.cancelBundle();
            */
       	test.stopTest(); 
         
    }
}