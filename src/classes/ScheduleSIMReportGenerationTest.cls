@isTest
private class ScheduleSIMReportGenerationTest {

    static testMethod void basicTest() {
        
        Integer year = Date.today().year() + 1;
        String CRON_EXP = '0 0 0 1 1 ? ' + year;  
        
        Test.startTest();
        String jobId = System.schedule('SIMReportGeneration schedule test', CRON_EXP, new ScheduleSIMReportGeneration());
        Test.stopTest();
        
        CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression); 
        // System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals(year+'-01-01 00:00:00', String.valueOf(ct.NextFireTime));
    }
}