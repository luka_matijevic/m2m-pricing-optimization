public without sharing class ServiceFee implements IServiceFee
{
	//--------------------------------------------------------------------------
	// Private members
	//--------------------------------------------------------------------------
	Service_Fee__c sf;

	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	public ServiceFee(Service_Fee__c sf)
	{
		this.sf = sf;
	}

	//--------------------------------------------------------------------------
	// Getters / setters
	//--------------------------------------------------------------------------
	public Service_Fee__c getServiceFee()
	{
		return sf;
	}

	public String getChargedPer()
	{
		return sf.Charged_Per__c;
	}

	public void setChargedPer(String chargedPer)
	{
		sf.Charged_Per__c = chargedPer;
	}

	public Boolean getShowOnQuote()
	{
		return sf.Show_on_Quote__c;
	}

	public void setShowOnQuote(Boolean showOnQuote)
	{
		sf.Show_on_Quote__c = showOnQuote;
	}

	public String getName()
	{
		return sf.Name;
	}

	public void setName(String name)
	{
		sf.Name = name;
	}

	public Decimal getOneTimeBaseFee()
	{
		return sf.One_Time_Base_Fee__c;
	}

	public void setOneTimeBaseFee(Decimal oneTimeBaseFee)
	{
		sf.One_Time_Base_Fee__c = oneTimeBaseFee;
	}

	public Decimal getOneTimeTargetFee()
	{
		return sf.One_Time_Target_Fee__c;
	}

	public void setOneTimeTargetFee(Decimal oneTimeTargetFee)
	{
		sf.One_Time_Target_Fee__c = oneTimeTargetFee;
	}

	public Decimal getRecurringBaseFee()
	{
		return sf.Recurring_Base_Fee__c;
	}

	public void setRecurringBaseFee(Decimal recurringBaseFee)
	{
		sf.Recurring_Base_Fee__c = recurringBaseFee;
	}

	public Decimal getRecurringTargetFee()
	{
		return sf.Recurring_Target_Fee__c;
	}

	public void setRecurringTargetFee(Decimal recurringTargetFee)
	{
		sf.Recurring_Target_Fee__c = recurringTargetFee;
	}

	public Boolean getCanDiscount()
	{
		return sf.Can_Discount__c;
	}

	public void setCanDiscount(Boolean canDiscount)
	{
		sf.Can_Discount__c = canDiscount;
	}

	public Boolean getIsTargetFeeDiscounted()
	{
		return (isOneTimeTargetFeeDiscounted() || isRecurringTargetFeeDiscounted());
	}

	public Boolean isOneTimeTargetFeeDiscounted()
	{
		return sf.One_Time_Target_Fee__c != sf.One_Time_Base_Fee__c;
	}

	public Boolean isRecurringTargetFeeDiscounted()
	{
		return sf.Recurring_Target_Fee__c != sf.Recurring_Base_Fee__c;
	}

	//--------------------------------------------------------------------------
	// Clone service fee
	//--------------------------------------------------------------------------
	public IServiceFee deepClone()
	{
		return new ServiceFee(sf.clone());
	}

	//--------------------------------------------------------------------------
	// Compares two service fees
	//--------------------------------------------------------------------------
	public Boolean equals(IServiceFee serviceFee)
	{
		//
		// Compare Service_Fee__c
		//
		if (getServiceFee().Show_on_Quote__c != serviceFee.getServiceFee().Show_on_Quote__c
			|| getServiceFee().One_Time_Target_Fee__c != serviceFee.getServiceFee().One_Time_Target_Fee__c
			|| getServiceFee().Recurring_Target_Fee__c != serviceFee.getServiceFee().Recurring_Target_Fee__c)
		{
			return false;
		}
		return equalsWithoutConfiguration(serviceFee);
	}

	//--------------------------------------------------------------------------
	// Compares two service fees without configuration changes
	//--------------------------------------------------------------------------
	public Boolean equalsWithoutConfiguration(IServiceFee serviceFee)
	{
		//
		// Compare Service_Fee__c without configuration fields
		//
		if (getServiceFee().Name != serviceFee.getServiceFee().Name
			|| getServiceFee().Can_Discount__c != serviceFee.getServiceFee().Can_Discount__c
			|| getServiceFee().Charged_Per__c != serviceFee.getServiceFee().Charged_Per__c
			|| getServiceFee().Description_DE__c != serviceFee.getServiceFee().Description_DE__c
			|| getServiceFee().Description_ENG__c != serviceFee.getServiceFee().Description_ENG__c
			|| getServiceFee().One_Time_Base_Fee__c != serviceFee.getServiceFee().One_Time_Base_Fee__c
			|| getServiceFee().Recurring_Base_Fee__c != serviceFee.getServiceFee().Recurring_Base_Fee__c
			|| getServiceFee().Sequence_No__c != serviceFee.getServiceFee().Sequence_No__c)
		{
			return false;
		}
		return true;
	}
}