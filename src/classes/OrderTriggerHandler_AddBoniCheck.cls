/**
 * This class is used as a trigger handler delegate that creates a boni check record after the order has been put to status Boni Check Requested
 * */
public with sharing class OrderTriggerHandler_AddBoniCheck extends TriggerHandler.DelegateBase {

    public static final string SENT_TO_BONI_STATUS = 'Sent to Boni';

	List<Boni_Check__c> bonisToInsert = new List<Boni_Check__c>();
	Map<Id, Boni_Check__c> orderBoniMap = new Map<Id, Boni_Check__c>();
	Map<Id, Id> orderProductBundleMap = new Map<Id, Id>();
    map<id, Account> accountMap = new map<id, Account>();
    List<Third_Upload_File__c> tufsToUpdate = new List<third_upload_file__c>();
    List<Stock__c> simsToUpdate = new List<Stock__c>();
    List<Opportunity> oppList = new List<Opportunity>();
    Map<Id, String> oppOwnerEmailMap = new Map<Id, String>();
    List<Frame_Contract__c> fcList = new List<Frame_Contract__c>();
    Map<Id, String> fcOwnerEmailMap = new Map<Id, String>();
    /*Set<Id> baIds = new Set<Id>();
    Billing_Account__c ba;
    Profile salesSupportProfile, firstLevelAgent, secondAndThirdLevelAgents, systemAdminProfile;
    Boolean updateBillingAccount = false;
    Boolean submitForApproval = false;
    String pkkForApproval = '';*/
    List<Id> orderRequestsToGenerateForm = new List<Id>();
    List<M2M_Order_Request_Subscription_Group_Jun__c> orderRequestSubscriptionGroupList = new List<M2M_Order_Request_Subscription_Group_Jun__c>();
	//--------------------------------------------------------------------------
	// Prepare before
	//--------------------------------------------------------------------------
	public override void prepareBefore()
	{
		List<Id> orderReqIds = new List<Id>();
		
		/*
	    	While creating/updating m2m order request if user checks "Use account Address", then below code will get
    		Account shipping address and copies to Order request
		*/
		
		if(trigger.isInsert || trigger.isUpdate){
		    set<id> accIds = new set<id>();
		    for(Order_Request__c ord : (list<Order_Request__c>) trigger.new){
		        if(ord.Use_Account_Address__c && ord.Account__c != null){
		            accIds.add(ord.Account__c);
		        }
		        //baIds.add(ord.Billing_Account__c);
		    }
		    if(accIds.size() > 0){
		        accountMap = new map<id, Account>([select id, shippingStreet,shippingCity,shippingPostalCode,shippingCountry from Account where id In:accIds]);
		        for(Order_Request__c ord : (list<Order_Request__c>) trigger.new){
    		        if(ord.Use_Account_Address__c && ord.Account__c != null && accountMap.get(ord.Account__c) != null){
    		            if(accountMap.get(ord.Account__c).shippingStreet != null){
    		                ord.Shipping_Street__c = accountMap.get(ord.Account__c).shippingStreet;
    		            }
    		            
    		            if(accountMap.get(ord.Account__c).shippingCity != null){
    		                ord.Shipping_City__c = accountMap.get(ord.Account__c).shippingCity;
    		            }
    		            
    		            if(accountMap.get(ord.Account__c).shippingPostalCode != null){
    		                ord.Shipping_Postal_Code__c = accountMap.get(ord.Account__c).shippingPostalCode;
    		            }
    		            
    		            if(accountMap.get(ord.Account__c).shippingCountry != null){
    		                ord.Shipping_Country__c = accountMap.get(ord.Account__c).shippingCountry;
    		            }
    		        }
    		    }
		    }
		}
		
		/*List<Billing_Account__c> listBa = [SELECT Id, Name, PKK__c, PKK_Approval__c FROM Billing_Account__c WHERE Id in :baIds];
		
		if(listBa != null && listBa.size() > 0) {
		    ba = listBa[0];
		}
		
		List<Profile> profileList = new List<Profile>([SELECT id, name FROM profile WHERE Name IN ('M2M Sales Support', '1st Level - DSC - READ permissions', 'M2M Service 2nd Level', 'System Administrator')]);
        if(profileList.size() > 0) {
            for(Profile prf : profileList) {
                if(prf.Name.equalsIgnoreCase('M2M Sales Support')) {
                    salesSupportProfile = prf;
                } else if(prf.Name.equalsIgnoreCase('1st Level - DSC - READ permissions')) {
                    firstLevelAgent = prf;
                } else if(prf.Name.equalsIgnoreCase('M2M Service 2nd Level')) {
                    secondAndThirdLevelAgents = prf;
                } else if(prf.Name.equalsIgnoreCase('System Administrator')) {
                    systemAdminProfile = prf;
                }
            }
        }*/
		
		if (Trigger.isUpdate)
		{

			Map<Id, Id> orderFrameIdMap = new Map<Id, Id>();
			Map<Id, Id> orderOpportunityIdMap = new Map<Id, Id>();
			//
			// All order requests that needs boni check
			//
			for (sObject obj : trigger.new)
			{
				Order_Request__c newItem = (Order_Request__c)obj;
				Order_Request__c oldItem = (Order_Request__c)trigger.oldMap.get(newItem.Id);
                if (readyForBoniCheck(newItem, oldItem))
				{
					orderReqIds.add(newItem.id);
					orderFrameIdMap.put(newItem.Id, newItem.Frame_Contract__c);
				}

				orderOpportunityIdMap.put(newItem.Id, newItem.Opportunity__c);
			}

            System.debug('=== OrderTriggerHandler_AddBoniCheck.prepareBefore() - orderReqIds.size()=' + orderReqIds.size());

			Map<Id,Frame_Contract__c> frameContracts = new Map<Id,Frame_Contract__c> (
					[ SELECT Opportunity__c, Product_Bundle__c
					  FROM Frame_Contract__c
					  WHERE Id IN :orderFrameIdMap.values() ]);

			for (Id orderId : orderFrameIdMap.keySet())
			{
				orderProductBundleMap.put(orderId, frameContracts.get(orderFrameIdMap.get(orderId)).Product_Bundle__c);
			}

			oppList = [select id, owner.email from opportunity where id in :orderOpportunityIdMap.values()];
			for (Opportunity opp : oppList){
				oppOwnerEmailMap.put(opp.Id, opp.Owner.Email);
			}

			fcList = [select id, owner.email from Frame_Contract__c where id in : orderFrameIdMap.values()];
			for (Frame_Contract__c fc : fcList){
				fcOwnerEmailMap.put(fc.Id, fc.Owner.Email);
			}
		}
	}

	public override void prepareAfter(){
		//for all trial tufs and sims in those tufs, update order request to new commercial
		//PM 15.2.2017. Commented out - BAU T-016
		
		if((trigger.isInsert || trigger.isUpdate) && trigger.new.size() > 0){
			Order_Request__c ordReq = (Order_Request__c)trigger.new[0];
			/*if (ordReq != null && ordReq.Frame_Contract_Type__c != null && ordReq.Frame_Contract_Type__c == 'Commercial'){
				if (ordReq.Account__c != null){
					Id accId = ordReq.Account__c;
					tufsToUpdate = getTrialTufsForAccount(accId);
					List<Id> tufIds = new List<Id>();
					for (third_upload_file__c tuf : tufsToUpdate){
						tuf.Order_Request__c = ordReq.Id;
						tufIds.add(tuf.Id);
						//simsToUpdate.addAll(getTufSims(tuf.Id));
					}
					simsToUpdate.addAll(getTufSims(tufIds));

					for (Stock__c sim : simsToUpdate){
						sim.Order_Request__c = ordReq.Id;
						sim.Trial__c = false;
					}
				}
			}*/

			//create order request form
			//get order products and prepare to send mail for APN
			List<Order_Request__c> orderReqsWithFc = [SELECT Id, Frame_Contract__r.Name from Order_request__c where id in :trigger.new];
			for (Order_Request__c oreq : orderReqsWithFc){
				if (oreq.Frame_Contract__r.Name == 'FC - 4991'){
					orderRequestsToGenerateForm.add(oreq.Id);
				}
			}

			//get order request billing accounts and create appropriate subscription group junction objects for each order request
			if (orderRequestSubscriptionGroupList == null){
				orderRequestSubscriptionGroupList = new List<M2M_Order_Request_Subscription_Group_Jun__c>();
			}
			Map<Id, Id> orderRequestBillingAccountMap = new Map<Id, Id>();
			for (Order_Request__c oreq : (List<Order_Request__c>)trigger.new){
				if (oreq.Billing_Account__c != null){
					orderRequestBillingAccountMap.put(oreq.Billing_Account__c, oreq.Id);
				}
			}

			List<M2M_Order_Request_Subscription_Group_Jun__c> subGroupListAlreadyCreated = [SELECT id, Billing_Account_Subscription_Group__c,
																					Billing_Account_Subscription_Group__r.Id
																					FROM M2M_Order_Request_Subscription_Group_Jun__c
																			WHERE M2M_Order_Request__c in :orderRequestBillingAccountMap.values()];
			Set<Id> subscriptionGrpsUsed = new Set<Id>();
			if (subGroupListAlreadyCreated != null && subGroupListAlreadyCreated.size() > 0){
				for (M2M_Order_Request_Subscription_Group_Jun__c tmpJun : subGroupListAlreadyCreated){
					subscriptionGrpsUsed.add(tmpJun.Billing_Account_Subscription_Group__r.Id);
				}
			}

			List<Billing_Account_Subscription_Group__c> subGroupList = new List<Billing_Account_Subscription_Group__c>();
			if (orderRequestBillingAccountMap != null && orderRequestBillingAccountMap.size() > 0){
				subGroupList = [SELECT Id, Name, Billing_Account__c, SM2M_Subscription_Group_Name__c 
								FROM Billing_Account_Subscription_Group__c
								WHERE Billing_Account__c in :orderRequestBillingAccountMap.keySet()
								AND ID not in :subscriptionGrpsUsed];
			}

			if (subGroupList != null && subGroupList.size() > 0){
				for(Billing_Account_Subscription_Group__c subGrp : subGroupList){
					if (orderRequestBillingAccountMap.containsKey(subGrp.Billing_Account__c)){
						M2M_Order_Request_Subscription_Group_Jun__c ordReqSubGrp = new M2M_Order_Request_Subscription_Group_Jun__c();
						ordReqSubGrp.Billing_Account_Subscription_Group__c = subGrp.Id;
						ordReqSubGrp.M2M_Order_Request__c = orderRequestBillingAccountMap.get(subGrp.Billing_Account__c);
						orderRequestSubscriptionGroupList.add(ordReqSubGrp);
					}
				}
			}

			System.debug(orderRequestSubscriptionGroupList);
		}
	}

//--------------------------------------------------------------------------
	// Before update
	//--------------------------------------------------------------------------
	public override void beforeInsert(sObject o) {
	    Order_Request__c newItem = (Order_Request__c)o;
	    
	    /*if(ba != null && ba.PKK__c != null) {
		    newItem.PKK__c = ba.PKK__c;
		}*/
	}
	
	//--------------------------------------------------------------------------
	// Before update
	//--------------------------------------------------------------------------
	public override void beforeUpdate(sObject old, sObject o)
	{

		Order_Request__c newItem = (Order_Request__c)o;
		Order_Request__c oldItem = (Order_Request__c)old;

        System.debug('=== OrderTriggerHandler_AddBoniCheck.beforeUpdate newItem.Boni_Check__c = ' + newItem.Boni_Check__c + ' ===');
        System.debug('=== OrderTriggerHandler_AddBoniCheck.beforeUpdate() (newItem/olditem).Status__c = ' + newItem.Status__c + '/' + oldItem.Status__c + ' ===');
        if (readyForBoniCheck(newItem, oldItem))
		{
            System.debug('=== OrderTriggerHandler_AddBoniCheck.beforeUpdate() - createBoniCheck() ===');
			Boni_Check__c boni = createBoniCheck(newItem);
			bonisToInsert.add(boni);
			orderBoniMap.put(newItem.Id, boni);
		}
		
		if(newItem.Status__c != oldItem.Status__c && newItem.Status__c.equalsIgnoreCase('Closed')) {
		    newItem.Order_close_date__c = System.now().date();
		}
		
		/*Id usersProfileId = UserInfo.getProfileId();
		updateBillingAccount = false;
        submitForApproval = false;
		
		if (oldItem.PKK__c != newItem.PKK__c) {
		    system.debug('usersProfileId - ' + usersProfileId + ' salesSupportProfile.Id - ' + salesSupportProfile.Id + ' firstLevelAgent.Id - ' + 
		        firstLevelAgent.Id + ' systemAdminProfile.Id - ' + systemAdminProfile.Id + ' secondAndThirdLevelAgents.Id - ' + secondAndThirdLevelAgents.Id);
		        
		    if(usersProfileId == salesSupportProfile.Id) {
		        if(newItem.PKK_Changed__c) {
		            newItem.addError('PKK is already changed and M2M Sales Support profile is not allowed to change it anymore!');
		        } else {
		            newItem.PKK_Changed__c = true;
		            ba.PKK__c = newItem.PKK__c;
		            ba.PKK_Approval__c = newItem.PKK__c;
		            updateBillingAccount = true;
		        }
		    } else if(usersProfileId == firstLevelAgent.Id) {
		        if(newItem.PKK_Changed__c) {
    		        submitForApproval = true;
    		        pkkForApproval = newItem.PKK__c;
    		        newItem.PKK__c = oldItem.PKK__c;
		        } else {
		            newItem.PKK_Changed__c = true;
    	            ba.PKK__c = newItem.PKK__c;
    	            ba.PKK_Approval__c = newItem.PKK__c;
    	            updateBillingAccount = true;
		        }
		    } else if(usersProfileId == secondAndThirdLevelAgents.Id) {
		        // change allowed
		        newItem.PKK_Changed__c = true;
	            ba.PKK__c = newItem.PKK__c;
	            ba.PKK_Approval__c = newItem.PKK__c;
	            updateBillingAccount = true;
		    } else {
		        newItem.PKK_Changed__c = true;
	            ba.PKK__c = newItem.PKK__c;
	            ba.PKK_Approval__c = newItem.PKK__c;
	            updateBillingAccount = true;
		    }
		}*/
	}
    
    /*private void submitApproval() {
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitted for approval. Please approve or reject the PKK change.');
        req.setObjectId(ba.Id);
        // submit the approval request for processing
        Approval.ProcessResult result = Approval.process(req);
    }*/

	//--------------------------------------------------------------------------
	// Creates boni check
	//--------------------------------------------------------------------------
	private Boni_Check__c createBoniCheck(Order_Request__c m2mOrder)
	{
		Boni_Check__c boniToCreate = new Boni_Check__c();

        System.debug('=== OrderTriggerHandler_AddBoniCheck.createBoniCheck() - m2mOrder.name=' + m2mOrder.Name + ' ===');
        System.debug('=== boniToCreate.Product_Bundle__c=' + orderProductBundleMap.get(m2mOrder.Id));

		OrderRequestDetail ord = new OrderRequestDetail(m2mOrder.Id);
		ProductOrder[] productOrders = ord.join().getProductOrders();

		//
		// One times
		//
		Decimal totalFixedSIM = 0;
		Decimal totalFixedHW = 0;
		Decimal totalFixedVAS = 0;

		//
		// Recurring
		//
		Decimal totalVariableSIM = 0;
		Decimal totalVariableHW = 0;
		Decimal totalVariableVAS = 0;

		//
		// Quantities
		//
		Decimal quantitySIM = 0;
		Decimal quantityHW = 0;
		Decimal quantityVAS = 0;

		//
		// Comments
		//
		String commentsSIM = '';
		String commentsHW = '';
		String commentsVAS = '';

		//
		// Go through current order request
		//
		for (ProductOrder po : productOrders)
		{
			for (ProductOrder.ProductConfiguration pc : po.productConfigurations)
			{
				Decimal oneTimeFee = sum(OrderRequestDetail.filter(pc.lineItems, new OrderRequestDetail.FilterOneTime()));
				Decimal recurringFee = sum(OrderRequestDetail.filter(pc.lineItems, new OrderRequestDetail.FilterRecurring()));
				if (pc.aisRecordType == 'SIM')
				{
					totalFixedSIM += oneTimeFee;
					totalVariableSIM += recurringFee;
					quantitySIM += po.quantity;
					commentsSIM += po.comment + '\n';

				}
				else if (pc.aisRecordType == 'Hardware')
				{
					totalFixedHW += oneTimeFee;
					totalVariableHW += recurringFee;
					quantityHW += po.quantity;
					commentsHW += po.comment + '\n';
				}
				else if (pc.aisRecordType == 'Support & Service')
				{
					totalFixedVAS += oneTimeFee;
					totalVariableVAS += recurringFee;
					quantityVAS += po.quantity;
					commentsVAS += po.comment + '\n';
				}
				else
				{
					System.debug('******** Product configuration is not part of any type');
				}
			}
		}

		boniToCreate.Name = 'CCR for ' + m2mOrder.name;
		boniToCreate.Requisition_Note__c = 'Credit Check Request for the customer order ' + m2mOrder.name;
		boniToCreate.Legitimate_Interest__c = true;
		boniToCreate.Premium_Services_0900__c = true;
		boniToCreate.International_Roaming_Voice__c = true;
		boniToCreate.Status_of_CC__c = BoniCheckController.BONICHECK_STATUS_SUBMITTED;
		boniToCreate.Account__c = m2mOrder.Account__c;
		boniToCreate.Comments__c = m2mOrder.Business_Model_Description__c;
		boniToCreate.Product_Bundle__c = orderProductBundleMap.get(m2mOrder.Id);

		boniToCreate.Number_Requested_SMS__c = quantitySIM;
		boniToCreate.Requested_Units_HW__c = quantityHW;
		boniToCreate.Number_Requested_VAS__c = quantityVAS;

		boniToCreate.Comment_Fix_SMS__c = commentsSIM;
		boniToCreate.Comment_Fix_HW__c = commentsHW;
		boniToCreate.Comment_Fix_VAS__c = commentsVAS;

		boniToCreate.Fix_Revenue_SMS__c = div(totalFixedSIM, quantitySIM);
		boniToCreate.Fix_Revenue_HW__c = div(totalFixedHW, quantityHW);
		boniToCreate.Fix_Revenue_VAS__c = div(totalFixedVAS, quantityVAS);

		boniToCreate.Variable_Revenue_SMS__c = div(totalVariableSIM, quantitySIM);
		boniToCreate.Variable_Revenue_HW__c = div(totalVariableHW, quantityHW);
		boniToCreate.Variable_Revenue_VAS__c = div(totalVariableVAS, quantityVAS);

		//27.6.2016. Petar Matkovic:
		//Added email to boni check field sales email
		String IoTFcId = 'a1Bb0000005Sxi2';
		if (m2mOrder.Frame_Contract__c == IoTFcId){
			//IoT; Email from opportunity Owner
			boniToCreate.Sales_Email__c = oppOwnerEmailMap.get(m2mOrder.Opportunity__c);

		}
		else{
			//email from frame contract creator
			boniToCreate.Sales_Email__c = fcOwnerEmailMap.get(m2mOrder.Frame_Contract__c);
		}
		

		return boniToCreate;
	}

	//--------------------------------------------------------------------------
	// Finish
	//--------------------------------------------------------------------------
	public override void finish()
	{
		//
		// Insert boni checks
		//
		insert bonisToInsert;
		//
		// Connect them with order requests
		//
		if (trigger.newMap != null)
		{
			for(Id orderId : orderBoniMap.keySet() )
			{
				Order_Request__c item = (Order_Request__c)trigger.newMap.get(orderId);
				item.Boni_Check__c = orderBoniMap.get(orderId).Id;
			}
		}

		//update trial tufs and their sims
		No_Trigger__c notrigger = null;
    	boolean noTriggerUser = false;
		// Set trigger to inactive
		List<No_Trigger__c> noList = [Select Id, SetupOwnerId, Flag__c from No_Trigger__c where SetupOwnerId =: UserInfo.getUserId() limit 1];
		if (noList.isEmpty()) {
			noTriggerUser=true;
			notrigger = new No_Trigger__c();
			notrigger.Flag__c = true;
			notrigger.SetupOwnerId = UserInfo.getUserId();
			upsert notrigger;
		} else {
			noList[0].Flag__c = true;
			upsert noList[0];
			notrigger = noList[0];
		}
		upsert tufsToUpdate;
		upsert simsToUpdate;

		notrigger.Flag__c = false;
		upsert notrigger; 
		if (noTriggerUser) {
			delete notrigger;
		}

		//insert order request subscription group junction object records
		if (orderRequestSubscriptionGroupList != null && orderRequestSubscriptionGroupList.size() > 0){
			upsert orderRequestSubscriptionGroupList;
		}
		
		/*if(submitForApproval) {
		    ba.PKK_Approval__c = pkkForApproval;
		    update ba;
		    submitApproval();
		}
		
		if(updateBillingAccount)
		    update ba;*/

		//Generate Order-Forms
		/*if (orderRequestsToGenerateForm != null && orderRequestsToGenerateForm.size()>0){
			for (Id oreqId : orderRequestsToGenerateForm){
				CongaComposerCommunicator.addNoteToOrderRequest(oreqId);
				CongaComposerCommunicator.generateOrderForm(UserInfo.getSessionId(), UserInfo.getOrganizationId(), oreqId);
			}
		}*/
	}

	//--------------------------------------------------------------------------
	// Sum line items
	//--------------------------------------------------------------------------
	private static Decimal sum(ProductOrder.LineItem[] lineItems)
	{
		Decimal accumulate = 0;
		for (ProductOrder.LineItem li : lineItems)
		{
			accumulate += li.price;
		}
		return accumulate;
	}

	//--------------------------------------------------------------------------
	// Divides two numbers
	//--------------------------------------------------------------------------
	private static Decimal div(Decimal a, Decimal b)
	{
		return b == 0 ? 0 : a / b;
	}

	//--------------------------------------------------------------------------
	// Check if we are ready for boni check
	//--------------------------------------------------------------------------
	private static Boolean readyForBoniCheck(Order_Request__c newItem, Order_Request__c oldItem)
	{
		return newItem.Boni_Check__c == null
				&& newItem.Status__c != oldItem.Status__c
				&& newItem.Status__c == SENT_TO_BONI_STATUS;
	}

	private List<third_upload_file__c> getTrialTufsForAccount(Id acc){
		List<third_upload_file__c> trialTufs = new List<third_upload_file__c>(
									[select id, Order_Request__c
									from third_upload_file__c 
									where account__c = :acc and 
									Order_request__r.Frame_Contract_Type__c = 'Trial']);

		return trialTufs;
	}

	private List<Stock__c> getTufSims(Id tufId){
		List<Stock__c> sims = new List<Stock__c>(
			[select id, trial__c, Order_Request__c
			from Stock__c
			where third_upload_file__c = :tufId
			]);

		return sims;
	}

	private List<Stock__c> getTufSims(List<Id> tufIds){
		List<Stock__c> sims = new List<Stock__c>(
			[select id, trial__c, Order_Request__c
			from Stock__c
			where third_upload_file__c in :tufIds
			]);

		return sims;
	}
}