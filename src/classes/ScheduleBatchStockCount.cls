/*
* Description: Schedule class for BatchStockCount class (batch class to count the total number of Stock records for StockServiceV1 processing & update in a CustomSetting field).
* Author: Ritesh Saxena
* Date: 25/08/2014
*/
public class ScheduleBatchStockCount implements Schedulable
{
    public void execute(SchedulableContext sc) {
        // keeping batch size 2000 as batch execution performs only record count.
        Database.executeBatch(new BatchStockCount(), 2000);
    }
}