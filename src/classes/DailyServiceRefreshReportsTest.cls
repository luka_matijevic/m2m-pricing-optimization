@isTest
private class DailyServiceRefreshReportsTest
{
	@isTest(SeeAllData='true')
	public static void testBatch1()
	{
		List<Report> reports = [SELECT Id FROM Report LIMIT 2];
		List<DailyServiceReportIds__c> reportIds = new List<DailyServiceReportIds__c>();
		Integer i = 0;
		
		Test.startTest();	
		for(Report r : reports){
			
			reportIds.add(new DailyServiceReportIds__c(Name ='Test'+ i, Report_ID__c = String.valueOf(r.Id)));
			i++;
		}

		insert reportIds;

			DailyServiceRefreshReports dd = new DailyServiceRefreshReports('Daily');
			Database.executeBatch(dd);
			
		Test.stopTest();
	
	}

		@isTest(SeeAllData='true')
	public static void testBatch2()
	{
		List<Report> reports = [SELECT Id FROM Report LIMIT 2];
		List<DailyServiceReportIds__c> reportIds = new List<DailyServiceReportIds__c>();
		Integer i = 0;
		
		Test.startTest();	
		for(Report r : reports){
			
			reportIds.add(new DailyServiceReportIds__c(Name ='Test'+ i, Report_ID__c = String.valueOf(r.Id)));
			i++;
		}

		insert reportIds;

			DailyServiceRefreshReports dd = new DailyServiceRefreshReports('Weekly');
			Database.executeBatch(dd);
			
		Test.stopTest();
	
	}
		@isTest(SeeAllData='true')
	public static void testBatch3()
	{
		List<Report> reports = [SELECT Id FROM Report LIMIT 2];
		List<DailyServiceReportIds__c> reportIds = new List<DailyServiceReportIds__c>();
		Integer i = 0;
		
		Test.startTest();	
		for(Report r : reports){
			
			reportIds.add(new DailyServiceReportIds__c(Name ='Test'+ i, Report_ID__c = String.valueOf(r.Id)));
			i++;
		}

		insert reportIds;

			DailyServiceRefreshReports dd = new DailyServiceRefreshReports('Monthly');
			Database.executeBatch(dd);
			
		Test.stopTest();
	
	}
}