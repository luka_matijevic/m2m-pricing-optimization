/*
* BillingAcountTriggerDelegate extends Trigger handler to execute calls based on events.
*/
public with sharing class BillingAcountTriggerDelegate extends TriggerHandler.DelegateBase {

    Map<Id, DateTime> payerIdCommercialDateMap = new Map<Id, DateTime>();
    Account parentAccount;
    Profile salesSupportProfile, firstLevelAgent, secondAndThirdLevelAgents, systemAdminProfile;
    Boolean submitForApproval = false;
    String pkkForApproval = '';
    Billing_Account__c ba;
    List<Order_Request__c> baChildOrderRequest;
    //
    
    public override void prepareBefore() {
        // do any preparation here – bulk loading of data etc
        List<Billing_account__c> baList = (List<Billing_account__c>)Trigger.new;
        if (Trigger.isInsert) {     
        	 
        	 AggregateResult[] ar = [select SM2M_Type__c, max(Billing_account_reference__c) bar from Billing_account__c group by SM2M_Type__c];
        	 integer i = 0;
        	 integer globalNumber = 0;
        	 integer localNumber = 0;
        	 while (i<ar.size()){
        	 	
        	 	if (ar[i].get('SM2M_Type__c') == 'Global') globalNumber = null == ar[i].get('bar') ? 0 : Integer.valueOf(String.valueOf(ar[i].get('bar')).substring(4));
        	 	if (ar[i].get('SM2M_Type__c') == 'Local')  localNumber = null == ar[i].get('bar') ? 0 : Integer.valueOf(String.valueOf(ar[i].get('bar')).substring(4));
        	 i++;
        	 }        	 
        	 for (Billing_account__c ba : baList){
        	 	if (ba.SM2M_Type__c == 'Global') {
        	 		globalNumber++;
        	 		ba.Billing_account_reference__c = 'BAG-'+String.valueOf(globalNumber).leftPad(5).replace(' ', '0');
        	 	}
        	 	else if (ba.SM2M_Type__c == 'Local') {
        	 		localNumber++;
        	 		ba.Billing_account_reference__c = 'BAL-'+String.valueOf(localNumber).leftPad(5).replace(' ', '0');
        	 	}
        	 		
        	 }
        	 
        	
        }
        //billable can be choosen to "yes" only if a related commercial fc exists on that account
        /*
        if (Trigger.isInsert || Trigger.isUpdate) {
        	List<Id> accountList = new List<Id>();
        	for (Billing_account__c ba : baList){
        		if (ba.billable__c) {
        			accountList.add(ba.payer__c);
         		}        			
        	}
        	system.debug('accountList: ' + accountList);
        	List<Frame_Contract__c> fcList = new List<Frame_Contract__c>
        		([select id, Customer__c, recordtype.name from Frame_Contract__c 
        		where Customer__c in :accountList and  (recordtype.name like '%Commercial%')]);
        		
        	Map<Id, Frame_Contract__c>	fcMap = new Map<Id, Frame_Contract__c>();
        	for (Frame_Contract__c fc:fcList) fcMap.put(fc.Customer__c, fc);
        	
        	for (Billing_account__c ba : baList){
        		if (ba.billable__c && null == fcMap.get(ba.payer__c))
        			ba.addError('There is no related commercial frame contract for this customer.');
        	}	
        }  
        */
        
        if (Trigger.isInsert) {
            for (SObject obj : Trigger.new) {
                Billing_Account__c ba = (Billing_Account__c) obj;
                parentAccount = [SELECT ID, Local__c, Global__c FROM Account WHERE Id = :ba.Payer__c];
            }
        } else if (Trigger.isUpdate) {
            for (SObject obj : Trigger.new) {
                Billing_Account__c ba = (Billing_Account__c) obj;
                parentAccount = [SELECT ID, Local__c, Global__c FROM Account WHERE Id = :ba.Payer__c];
                if (ba.Date_Report_Start__c==null && (ba.EDIFACT__c==true || ba.Online_Bill_Report_Delivery__c==true || ba.EVN_Report__c==true)){
                    ba.Date_Report_Start__c=date.today();
                }
            }
        }
        
        List<Profile> profileList = new List<Profile>([SELECT id, name FROM profile WHERE Name IN ('M2M Sales Support', '1st Level - DSC - READ permissions', 'M2M Service 2nd Level', 'System Administrator')]);
        if(profileList.size() > 0) {
            for(Profile prf : profileList) {
                if(prf.Name.equalsIgnoreCase('M2M Sales Support')) {
                    salesSupportProfile = prf;
                } else if(prf.Name.equalsIgnoreCase('1st Level - DSC - READ permissions')) {
                    firstLevelAgent = prf;
                } else if(prf.Name.equalsIgnoreCase('M2M Service 2nd Level')) {
                    secondAndThirdLevelAgents = prf;
                } else if(prf.Name.equalsIgnoreCase('System Administrator')) {
                    systemAdminProfile = prf;
                }
            }
        }
        
    }

    public override void prepareAfter() {
        // do any preparation here – bulk loading of data etc
    }

    public override void beforeInsert(sObject o) {
        Billing_Account__c ba = (Billing_Account__c) o;
        if(parentAccount != null) {
            if(ba.SM2M_Type__c != null) {
                if(ba.SM2M_Type__c.equalsIgnoreCase('Local') && !parentAccount.Local__c) {
                    ba.SM2M_Type__c.addError('Check the SM2M Type field again. It can\'t be Local since its Payer isn\'t Local!');
                }
                
                if(ba.SM2M_Type__c.equalsIgnoreCase('Global') && !parentAccount.Global__c) {
                    ba.SM2M_Type__c.addError('Check the SM2M Type field again. It can\'t be Global since its Payer isn\'t Global!');
                }
            }
        }
        
        if(ba.PKK__c != null) {
            ba.PKK_Approval__c = ba.PKK__c;
        }
    }

    public override void beforeUpdate(sObject old, sObject o) {
        Billing_Account__c baOld = (Billing_Account__c) old;
        Billing_Account__c baNew = (Billing_Account__c) o;
        if(parentAccount != null) {
            
            if(baNew.SM2M_Type__c != null) {
                if(baOld.SM2M_Type__c != baNew.SM2M_Type__c) {
                    if(baNew.SM2M_Type__c.equalsIgnoreCase('Local') && !parentAccount.Local__c) {
                        baNew.SM2M_Type__c.addError('Check the SM2M Type field again. It can\'t be Local since its Payer isn\'t Local!');
                    }
                    
                    if(baNew.SM2M_Type__c.equalsIgnoreCase('Global') && !parentAccount.Global__c) {
                        baNew.SM2M_Type__c.addError('Check the SM2M Type field again. It can\'t be Global since its Payer isn\'t Global!');
                    }
                }
            }
        }
        
        Id usersProfileId = UserInfo.getProfileId();
        submitForApproval = false;
		
		if (baOld.PKK__c != baNew.PKK__c) {
		    if(usersProfileId == salesSupportProfile.Id) {
		        if(baNew.PKK_Changed__c) {
		            baNew.addError('PKK is already changed and M2M Sales Support profile is not allowed to change it anymore!');
		        } else {
		            baNew.PKK_Changed__c = true;
		            ba = baNew;
                    //Update WI users
                   System.enqueueJob(new queueableWIUserUpdate(String.valueOf(baNew.id)));
                
		        }
		    } else if(usersProfileId == firstLevelAgent.Id) {
		        if(!Approval.isLocked(baNew.Id)) {
    		        if(baNew.PKK_Changed__c) {
        		        submitForApproval = true;
        		        baNew.PKK_Approval__c = baNew.PKK__c;
        		        baNew.PKK__c = baOld.PKK__c;
        		        ba = baNew;
    		        } else {
    		            baNew.PKK_Changed__c = true;
    		            ba = baNew;
        	            ba.PKK_Approval__c = baNew.PKK__c;
                        //Update WI users
                        System.enqueueJob(new queueableWIUserUpdate(String.valueOf(baNew.id)));
                
    		        }
		        }
		    } else if(usersProfileId == secondAndThirdLevelAgents.Id || usersProfileId == systemAdminProfile.Id) {
		        // change allowed
		        baNew.PKK_Changed__c = true;
	            ba = baNew;
	            baNew.PKK_Approval__c = baNew.PKK__c;
                //Update WI users
                System.enqueueJob(new queueableWIUserUpdate(String.valueOf(baNew.id)));
                               
		    } else {
		        baNew.PKK_Changed__c = true;
	            ba = baNew;
	            baNew.PKK_Approval__c = baNew.PKK__c;
                //Update WI users
               System.enqueueJob(new queueableWIUserUpdate(String.valueOf(baNew.id)));
                
		    }
		}
    }

    public override void beforeDelete(sObject o) {
        // Apply before delete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterInsert(sObject o) {
        // Apply before delete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterUpdate(sObject old, sObject o) {
        Billing_Account__c oldBA = (Billing_Account__c) old;
        Billing_Account__c newBA = (Billing_Account__c) o;
        if((oldBA.Billable__c != newBA.Billable__c && newBA.Billable__c && !newBA.Trial__c) 
            || (oldBA.Trial__c != newBA.Trial__c && newBA.Billable__c && !newBA.Trial__c)){
            payerIdCommercialDateMap.put(newBA.Payer__c, newBA.LastModifiedDate);
        }
    }

    public override void afterDelete(sObject o) {
        // Apply after delete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterUndelete(sObject o) {
        // Apply after undelete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    private void submitApproval(Billing_Account__c ba) {
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitted for approval. Please approve or reject the PKK change.');
        req.setObjectId(ba.Id);
        // submit the approval request for processing
        Approval.ProcessResult result = Approval.process(req);
    }

    public override void finish() {
        try{
            List<Stock__c> stockUpdateList = new List<Stock__c>();
            if(!payerIdCommercialDateMap.isEmpty()){
                /*
                for(Stock__c st : [Select Id, Account__c, Commercial_Date__c from Stock__c where Account__c IN: payerIdCommercialDateMap.keySet()]){
                    if(null!=payerIdCommercialDateMap.get(st.Account__c)){
                        st.Commercial_Date__c = payerIdCommercialDateMap.get(st.Account__c);
                        stockUpdateList.add(st);
                        
                    }               
                }
				*/
                UpdateStockCommercialDateBatch updateSIMBatch = new UpdateStockCommercialDateBatch(payerIdCommercialDateMap);
                database.executeBatch(updateSIMBatch);
                
            }

            if(!stockUpdateList.isEmpty()){
                update stockUpdateList;                
            }

        }catch(Exception e){
            system.debug('>>>>Exception in Billing Account trigger handler finish: ' + e.getMessage());
        }
        
        if(submitForApproval) {
    	    submitApproval(ba);
    	}
    }
    
	
    
}