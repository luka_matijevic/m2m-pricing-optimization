public with sharing class CalloutIntegration
{
    /* create HTTP request */
	public static HttpRequest createRequest(string endPoint, string httpMethod, string body, integer timeout)
	{
		HttpRequest req = new HttpRequest();
		
		req.setEndpoint(endPoint);
		req.setHeader('content-type', 'application/json');
        req.setHeader('Authorization', getBasicAuthHeader('logbookTef','logbookTef_1'));
		req.setmethod(httpMethod);
		req.setbody(body);
		req.setTimeout(timeout * 1000);
		return req;
	}
	
	/* http call of request */
	public static HttpResponse executeRequest(string endPoint, string httpMethod, string body, integer timeout)
	{
	    // create JSON
		HttpRequest req = createRequest(endPoint, httpMethod, body, timeout);
		Http httpCall = new Http();
		HttpResponse resp = httpCall.send(req);
		return resp;
	}
	
    public static String getBasicAuthHeader(String username, String password) {
        String authHeader = '';
        authHeader = (username != null && password != null) ? 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(username + ':' + password)) : '';
        return authHeader;
    }
    
    /* create HTTP request - BLOB*/
	public static HttpRequest createFileRequest(String endPoint, String httpMethod, string fileName, Blob body, Integer timeout)
    {
        String boundary = '----------------------------741e90d31eff';
        String header = '--' + boundary + '\nContent-Disposition: form-data; name="file"; filename="' + fileName + '";\nContent-Type: application/octet-stream';
          
        String footer = '--' + boundary + '--';             
        String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header + '\r\n\r\n'));
        while(headerEncoded.endsWith('='))
        {
            header += ' ';
            headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header + '\r\n\r\n'));
        }
        
        //System.debug(LoggingLevel.DEBUG, 'EncodingUtil ' + EncodingUtil);
        System.debug(LoggingLevel.DEBUG, 'endPoint ' + endPoint + ' httpMethod ' + httpMethod + ' body ' + body + ' fileName ' + fileName);
        
        String bodyEncoded = EncodingUtil.base64Encode(body);
        Blob bodyBlob = null;
        String last4Bytes = bodyEncoded.substring(bodyEncoded.length() - 4, bodyEncoded.length());
 
        if(last4Bytes.endsWith('==')) {
            last4Bytes = last4Bytes.substring(0,2) + '0K';
            bodyEncoded = bodyEncoded.substring(0, bodyEncoded.length() - 4) + last4Bytes;          
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded + footerEncoded);
        } else if(last4Bytes.endsWith('=')) {
            last4Bytes = last4Bytes.substring(0, 3) + 'N';
            bodyEncoded = bodyEncoded.substring(0, bodyEncoded.length() - 4) + last4Bytes;          
            footer = '\n' + footer;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded + bodyEncoded + footerEncoded);              
        } else {
            footer = '\r\n' + footer;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded + bodyEncoded + footerEncoded);  
        }
 
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', getBasicAuthHeader('m2mbic', Label.CustomerBillAuthPassword));
        req.setHeader('Content-Type','multipart/form-data; boundary=' + boundary);
        req.setMethod('POST');
        req.setEndpoint(endPoint);
        req.setBodyAsBlob(bodyBlob);
        req.setTimeout(timeout * 1000);
        return req;
    }
    
    /* create HTTP request - BLOB*/
	public static HttpRequest createFileRequestCSV(String endPoint, String httpMethod, string fileName, Blob body, Integer timeout)
    {
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', getBasicAuthHeader('m2mbic', Label.CustomerBillAuthPassword));
        req.setHeader('Content-Type','text/csv');
        req.setMethod('POST');
        req.setEndpoint(endPoint);
        req.setBodyAsBlob(body);
        req.setTimeout(timeout * 1000);
        return req;
    }
    
    @Future(callout=true)
    public static void executeFileRequest(String endPoint, String httpMethod, String fileName, Blob body, Integer timeout)
    {
        HttpRequest req = createFileRequest(endPoint, httpMethod, fileName, body, timeout);
        Http httpCall = new Http();
        HttpResponse resp = httpCall.send(req);
        System.debug(LoggingLevel.DEBUG, 'resp ' + resp);
        //return resp;
    }
    
    @Future(callout=true)
    public static void executeFileRequestCSV(String endPoint, String httpMethod, String fileName, Blob body, Integer timeout)
    {
        HttpRequest req = createFileRequestCSV(endPoint, httpMethod, fileName, body, timeout);
        Http httpCall = new Http();
        HttpResponse resp = httpCall.send(req);
        System.debug(LoggingLevel.DEBUG, 'resp ' + resp);
        //return resp;
    }
}