/*
* Description: Controller class for Case Escalation Page. Holds logic to send escalation messages to manager
* Author: Simo Brtan
* Version: 1.0
*/
public with sharing class CaseEscalationController {

   private Case caseObject;
   private String subject;
   private String body;

   public String escalationMessage {get;set;}
   public String escalationAnswerMessage {get;set;}

   private String escalationQueue = 'Case_Escalation_Queue';
   private String templateApiName = 'SUPPORT_Ticket_ESCALATION';
   private String orgWideEmailId = 'Service Team';
   private String assigneeName;

   	public CaseEscalationController (ApexPages.StandardController cont){

   		this.caseObject = (Case) cont.getRecord();
   		if(caseObject == null){
   			caseObject = new Case();
   		}
   		this.subject = 'Ticket escalation for ticket number: ' + caseObject.CaseNumber;
   	}


   /*
    * Method to display Case details i.e. case comments and email history in Case email history/comments section
    */
    public PageReference  escalateCase(){
		// TODO save data from escalation message field. And send a message to escalation queue and assign a case to a escalation queue
 		caseObject = [Select id, ownerId, caseNumber, account.Name, Owner.Name from Case where id =: caseObject.Id];

       	caseObject.Escalation_message__c = this.escalationMessage;
      	caseObject.Escalation_Agent__c = this.caseObject.ownerid;
      	this.assigneeName = caseObject.Owner.Name;
 		processForEscalation();
		caseObject.Status = 'Escalated';
		update caseObject;

		PageReference pref = new ApexPages.StandardController(caseObject).view();
		return pref;

    }

    private void processForEscalation(){

     	// sets the escalation Queue as owner
		List<Group> escalationQueue = [Select g.Name, g.LastModifiedDate, g.Id, g.DoesSendEmailToMembers, g.DeveloperName, g.Email From Group g where g.DeveloperName =: escalationQueue];
 		List<RecordType> escalationRecType = [select id from RecordType where DeveloperName  = 'Escalated_case'];
		Id queueId;
		String toAddress;
 		if(escalationQueue != null){
			queueId = (Id)escalationQueue.get(0).Id;
			toAddress = escalationQueue.get(0).Email;
			if(toAddress == null){	}
				//TODO alternative solution
 		} else {
 			System.debug(logginglevel.error,'NO Escalation QUEUE');
  		}

 		if(escalationRecType != null){
 			caseObject.recordtypeid = escalationRecType.get(0).Id;
 		} else {
 			System.debug(logginglevel.error,'NO Escalation RECORD TYPE');
 		}

 		caseObject.OwnerId = queueId;

		CaseComment escalationComment = new CaseComment();
		escalationComment.CommentBody = caseObject.escalation_message__c;
		escalationComment.IsPublished = TRUE;
		escalationComment.ParentId = caseObject.Id;

 		insert escalationComment;

 	 	String[] addresses = new String[]{toAddress};


		String body = '\n *** Ticket Escalation ***'+
					  '\n Company: ' + this.caseObject.Account.Name +
					  '\n Ticket: ' +  this.caseObject.CaseNumber   +
					  '\n Ticket Agent: ' + this.assigneeName +
					  '\n Escalation message : ' + this.caseObject.escalation_message__c ;


		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
 		email.setToAddresses(addresses);
	  	email.setPlainTextBody(body);
	  	email.setWhatId(caseObject.Id);
	  	email.setSenderDisplayName(assigneeName);
 		// Sends the email
 		Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});

   	}

   /**
    * Method to display Case details i.e. case comments and email history in Case email history/comments section
    */
    public PageReference  escalationAnswer(){
		// TODO save data from escalation message field. And send a message to escalation queue and assign a case to a escalation queue
 		caseObject = [Select id, ownerId, caseNumber, account.Name, Escalation_Agent__c  from Case where id =: caseObject.Id];

      	caseObject.escalation_answer__c = this.escalationAnswerMessage;
      	caseObject.ownerId = caseObject.Escalation_Agent__c;

      	CaseComment escalationComment = new CaseComment();
		escalationComment.CommentBody = caseObject.Escalation_answer__c;
		escalationComment.IsPublished = TRUE;
		escalationComment.ParentId = caseObject.Id;
		insert escalationComment;


      	List<RecordType> standardRecType = [select id from RecordType where DeveloperName  = 'Standard_Case'];
      	caseObject.recordtypeid = standardRecType.get(0).Id;
		caseObject.Status = 'In Progress';
		update caseObject;

		PageReference pref = new ApexPages.StandardController(caseObject).view();
		return pref;

    }


}