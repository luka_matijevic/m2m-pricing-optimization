public class CaseNCaseComments {
    public Case Cs;
    public list<CaseComment> caseCmnt;
    
    public CaseNCaseComments(Case c, list<CaseComment> cm){
        if(c != null){
            cs = c;
        }
        if( cm != null){
            caseCmnt = cm;
        }
    }
}