public class AdditionalUtils {

	//--------------------------------------------------------------------------
    // compares two big numbers stored as string
	// returns -1 when n1<n2; 0 when n1=n2; 1 when n1>n2
    //--------------------------------------------------------------------------
	public static Integer compareBigNumbers(String n1, String n2)
	{
		if (null!=n1 && null!=n2 && n1!='' && n2!=''){
		List<String> n1List = new List<String>();
		List<String> n2List = new List<String>();
		n1List = n1.split('');
		n2List = n2.split('');
		//first element is empty
		n1List.remove(0);
		n2List.remove(0);
		//extract first character and determine the sign
		if (n1List[0]=='-') { 
			n1List[1] ='-' + n1List[1];
			n1List.remove(0);
		}
		if (n2List[0]=='-') { 
			n2List[1] = '-' + n2List[1];
			n2List.remove(0);
		}

		if (n1List.size() > n2List.size()) {return 1;}
		else if (n1List.size() < n2List.size()) {return -1;}
		else
		{
			for (Integer i = 0; i < n1List.size(); i++)
			{
				if (Integer.valueOf(n1List[i]) > Integer.valueOf(n2List[i])) { return 1; break;}
				else if (Integer.valueOf(n1List[i]) < Integer.valueOf(n2List[i])) { return -1; break;}
			}
		}
		return 0;
	}
	else return 0;
	}
}