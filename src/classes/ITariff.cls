//------------------------------------------------------------------------------
// Interface for tariff container
//------------------------------------------------------------------------------
public interface ITariff
{
	//--------------------------------------------------------------------------
	// Get Tariff__c
	//--------------------------------------------------------------------------
	Tariff__c getTariff();

	//--------------------------------------------------------------------------
	// Get all tariff options sorted by type and sequence
	//--------------------------------------------------------------------------
	List<ITariffOption> getTariffOptions();

	//--------------------------------------------------------------------------
	// Get included tariff options sorted by sequence
	//--------------------------------------------------------------------------
	List<ITariffOption> getIncludedTariffOptions();

	//--------------------------------------------------------------------------
	// Get additional tariff options sorted by sequence
	//--------------------------------------------------------------------------
	List<ITariffOption> getAdditionalTariffOptions();

	//--------------------------------------------------------------------------
	// Get supplementary service tariff options sorted by sequence
	//--------------------------------------------------------------------------
	List<ITariffOption> getSupplementaryServiceTariffOptions();

	//--------------------------------------------------------------------------
	// Clone tariff with all selected tariff options and configurations
	//--------------------------------------------------------------------------
	ITariff deepClone();

	//--------------------------------------------------------------------------
	// Compare two tariffs
	//--------------------------------------------------------------------------
	Boolean equals(ITariff tariff);

	//--------------------------------------------------------------------------
	// Compare two tariffs without quantity
	//--------------------------------------------------------------------------
	Boolean equalsWithoutQuantity(ITariff tariff);

	//--------------------------------------------------------------------------
	// Merges tariff options
	// Tariff options that are missing in this tariff are added
	// and unselected
	//--------------------------------------------------------------------------
	void deepMerge(ITariff tariff);

	//--------------------------------------------------------------------------
	// Copy quantities from destination tariff
	//--------------------------------------------------------------------------
	void copyQuantities(ITariff tariff);

	//--------------------------------------------------------------------------
	// Store tariff to database
	//--------------------------------------------------------------------------
	Id store();
	Id storeUpdate();

	//--------------------------------------------------------------------------
	// Store tariff using insert helper for storing bulk after
	//--------------------------------------------------------------------------
	void store(IInsertHelper insertHelper);

	//--------------------------------------------------------------------------
	// Prepare child tariff before storing
	//--------------------------------------------------------------------------
	void prepareChild(
			ITariff parentTariff,
			Id accountId,
			String accountName,
			String contractTerm);
}