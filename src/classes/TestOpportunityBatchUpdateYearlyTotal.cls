@isTest
private class TestOpportunityBatchUpdateYearlyTotal {

	static testMethod void testBatchUpdating() {

		Date today = Date.today();

		Account account = new Account(
			Name = 'Test',
			Type = 'Business'
		);

		insert account;

		Double monthlyRevenue = 2.0;

		List<Opportunity> opportunities = new List<Opportunity>();

		opportunities.add(new Opportunity(
			Name = 'Test-1',
			AccountId = account.Id,
			CloseDate = Date.newInstance(today.year(), 1, 1),
			StageName = 'Closed Won',
			Win_Reason__c = 'Price',
			Contract_Term_Per_Subscription__c = 12,
			Monthly_Revenue_Per_Service__c = monthlyRevenue,
			Description = 'Test Description 20 characters'
		));

		opportunities.add(new Opportunity(
			Name = 'Test-2',
			AccountId = account.Id,
			CloseDate = Date.newInstance(today.year(), 1, 1),
			StageName = 'Closed Won',
			Win_Reason__c = 'Price',
			Contract_Term_Per_Subscription__c = 12,
			Monthly_Revenue_Per_Service__c = monthlyRevenue,
			Description = 'Test Description 20 characters'
		));

		insert opportunities;

		List<cscfga__Product_Bundle__c> bundles = new List<cscfga__Product_Bundle__c>();

		bundles.add(new cscfga__Product_Bundle__c(
			Name = 'Test 1',
			cscfga__Synchronised_with_Opportunity__c = true,
			cscfga__Opportunity__c = opportunities.get(0).Id
		));

		bundles.add(new cscfga__Product_Bundle__c(
			Name = 'Test 2',
			cscfga__Synchronised_with_Opportunity__c = true,
			cscfga__Opportunity__c = opportunities.get(1).Id
		));

		insert bundles;

		List<Unit_Schedule__c> schedules = new List<Unit_Schedule__c>();

		Integer monthlyAmount = 10;

		schedules.add(new Unit_Schedule__c(
			Opportunity__c = opportunities[0].Id,
			Year__c = String.valueOf(today.year()),
			VAS_Units_January__c 	= monthlyAmount,
			VAS_Units_February__c 	= monthlyAmount,
			VAS_Units_March__c 		= monthlyAmount,
			VAS_Units_April__c		= monthlyAmount,
			VAS_Units_May__c 		= monthlyAmount,
			VAS_Units_June__c 		= monthlyAmount,
			VAS_Units_July__c 		= monthlyAmount,
			VAS_Units_August__c 	= monthlyAmount,
			VAS_Units_September__c 	= monthlyAmount,
			VAS_Units_October__c 	= monthlyAmount,
			VAS_Units_November__c 	= monthlyAmount,
			VAS_Units_December__c 	= monthlyAmount
		));

		schedules.add(new Unit_Schedule__c(
			Opportunity__c = opportunities[0].Id,
			Year__c = String.valueOf(today.addYears(-1).year()),
			VAS_Units_November__c = monthlyAmount
		));

		schedules.add(new Unit_Schedule__c(
			Opportunity__c = opportunities[1].Id,
			Year__c = String.valueOf(today.year()),
			VAS_Units_January__c 	= monthlyAmount,
			VAS_Units_February__c 	= monthlyAmount,
			VAS_Units_March__c 		= monthlyAmount,
			VAS_Units_April__c		= monthlyAmount,
			VAS_Units_May__c 		= monthlyAmount,
			VAS_Units_June__c 		= monthlyAmount,
			VAS_Units_July__c 		= monthlyAmount,
			VAS_Units_August__c 	= monthlyAmount,
			VAS_Units_September__c 	= monthlyAmount,
			VAS_Units_October__c 	= monthlyAmount,
			VAS_Units_November__c 	= monthlyAmount,
			VAS_Units_December__c 	= monthlyAmount
		));

		schedules.add(new Unit_Schedule__c(
			Opportunity__c = opportunities[1].Id,
			Year__c = String.valueOf(today.addYears(-1).year()),
			VAS_Units_November__c = monthlyAmount
		));

		insert schedules;

		for (Integer i = 0; i < opportunities.size(); i++) {
			opportunities[i].Total_Connectivity_Rev_in_current_year__c = 0.0;
			opportunities[i].Total_Service_Revenue_in_current_year__c = 0.0;
		}

		update opportunities;

		double expectedRevenue = 0.0;
		// adding values for current year's Unit_Schedule__c
		for (integer i = 0; i < 12; i++) {
			expectedRevenue += (12 - i) * monthlyAmount * monthlyRevenue;
		}

		// add value for last year's Unit_Schedule__c (10 months in current year)
		expectedRevenue += 10 * monthlyAmount * monthlyRevenue;

		System.debug('*** expectedRevenue ' + expectedRevenue);


		Test.startTest();

		OpportunityBatchUpdateYearlyTotal batch = new OpportunityBatchUpdateYearlyTotal();
		Database.executeBatch(batch);

		Test.stopTest();

		List<Opportunity> changedOpportunities = [SELECT Id, Total_Connectivity_Rev_in_current_year__c, Total_Service_Revenue_in_current_year__c FROM Opportunity];

		for (Opportunity opp : changedOpportunities) {
			System.assertEquals(expectedRevenue, opp.Total_Service_Revenue_in_current_year__c);
		}
	}
}