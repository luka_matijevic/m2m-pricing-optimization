public class CustomerMonthlyBillController {

 
    transient List<Customer_Monthly_Bill__c> cmbList = new List<Customer_Monthly_Bill__c>();
   
    private List<SelectOption> monthList = new List<SelectOption>();
    public List<SelectOption> getMonthList(){
        return monthList;
    }
    public String monthSelected {get;set;}
    
    public CustomerMonthlyBillController(){
        //populate months
        List<AggregateResult> monthAR = [select Billing_Date__c from Customer_Monthly_Bill__c group by Billing_Date__c order by Billing_Date__c desc];
        for (AggregateResult ar : monthAR){
            monthList.add(new SelectOption(String.valueOf(ar.get('Billing_Date__c')), ((Date) ar.get('Billing_Date__c')).format()));
        }
        if (monthList!=null && monthList.size()>0) monthSelected = monthList.get(0).getValue();
        refresh();
    }

	
    
    
    public List<Customer_Monthly_Bill__c> getcmbList(){
        return this.cmbList;
    }
 	
    public PageReference refresh(){
        Date d = Date.valueOf(monthSelected);
        cmbList = [select 
                   Billing_Account__r.Name, 
                   Billing_Account__r.Payer__r.Name, 
                   Archived__c, 
                   Invoice_Number__c,
                   Billing_Account_OFI_ID__c,
                   PDF_Bill_Link__c,
                   EDIFACT__c,
                   EVN_EDIFACT__c,
                   EVN_Report_CSV__c,
                   EVN_Report_PDF__c,
                   Sim_Card_Summary__c,
                   Billing_Account__r.SM2M_Type__c,
                   Billing_Account__r.Online_Bill_Report_Delivery__c
                   from Customer_Monthly_Bill__c
                   where Billing_Date__c = :d
                   order by Billing_Date__c desc limit 10000];
        return null;
    }

    
}