@isTest
public class ActivationPlanTriggerDelegateTest {
	private static testMethod void testActivationPlan() {
       	Account account = new Account(
        	Name = 'Test3',
            Type = 'Business'
        	);
        	
        insert account;
        
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1),
			AccountId = account.Id
		);
		
		insert opp;
		
		cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(
			cscfga__Synchronised_with_Opportunity__c = false,
			cscfga__Opportunity__c = opp.Id
		);
		
		insert productBundle;
        
        Activation_plan__c aPlan = new Activation_Plan__c(
            Year__c = '2015', 
            Opportunity__c = opp.Id, 
            Connectivity_Sims_January__c = 1,
            Connectivity_Sims_February__c = 1,
            Connectivity_Sims_March__c = 1,
            Connectivity_Sims_April__c = 1,
            Connectivity_Sims_May__c = 1,
            Connectivity_Sims_June__c = 1,
            Connectivity_Sims_July__c = 1,
            Connectivity_Sims_August__c = 1,
            Connectivity_Sims_September__c = 1,
            Connectivity_Sims_October__c = 1,
            Connectivity_Sims_November__c = 1,
            Connectivity_Sims_December__c = 1,
        	Product_bundle__c = productBundle.Id);
        
        insert aPlan;
        
        /*Luka - code coverage fix*/
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
      	User u = new User(Alias = '123sta', Email='standarduser@testorg.com', 
      	EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      	LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles',
      	UserName='123o2spin-salesforce@syzygy.de');
      	
      	System.runAs(u){
      		aPlan.Connectivity_Sims_January__c=2;
      		update aPlan;
            System.assertEquals(aPlan.Connectivity_Sims_January__c, 2);
      	}
	}
}