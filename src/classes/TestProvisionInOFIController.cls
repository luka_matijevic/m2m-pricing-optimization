@isTest
private class TestProvisionInOFIController
{
	static testMethod void testProvisionInOFI()
	{
		//
		// Create test data
		//
				Account accPayer = new Account(
			Name = 'Test account', 
			BillingCity = 'Bremen', 
			BillingCountry = 'Germany', 
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße', 
			Type = 'Customer', 

			VAT__c = 1,
            Global__c = true
			
		);
		insert accPayer;
        
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1)
			, recordtypeid = Cache.getRecordTypeId('Opportunity.Commercial_Global'),
			Description = 'Test description 20 characters'
		);
		insert opp;		
		 Contact con = Test_Util.createContact(accPayer.id);
		
        Frame_Contract__c fc = new Frame_Contract__c();
        fc.contact__c = con.id;
        fc.customer__c = accPayer.id;
        fc.opportunity__c = opp.id;
         insert fc;      
        
        	
        
		Billing_Account__c billingAccount = new Billing_Account__c(
			Payer__c = accPayer.Id, 
			Trial__c=false, 
			Billable__c=false,
			VAT__c = '1'
            , Payment_Term__c = '30 days',
            Name = 'Name',
		Billing_Street__c = 'Street',
		Billing_City__c = 'City',
		Billing_Postcode__c = 'Postal',
            Billing_country__c = 'Country',		
		Payment_Method__c = 'method',
            Use_account_address__c = false
		);
		insert billingAccount;
		//
		// Custom settings
		//
		Email_Settings__c emailSettings = new Email_Settings__c(
				Name = 'Provision_In_OFI',
				Email__c = 'test@m2m.com',
				Subject__c = 'Test OFI',
				Body__c = 'Test OFI');
		insert emailSettings;

		//
		// Remove mandatory field
		//


		//
		// Run controller
		//
		Test.startTest();
		ProvisionInOFIController provisionInOFIController =
				new ProvisionInOFIController(new ApexPages.StandardController(billingAccount));
		provisionInOFIController.provision();
		PageReference pr = provisionInOFIController.gotoBillingAccount();
		Test.stopTest();
		//
		// Tests
		//
		billingAccount =
				[ SELECT OFI_Provisioning_Status__c
				  FROM Billing_Account__c
				  WHERE Id = :billingAccount.Id ];
		System.assertEquals('Pending', billingAccount.OFI_Provisioning_Status__c);
		System.assertNotEquals(null, pr);
		System.assertEquals(null, provisionInOFIController.errorMessages);
	}

	static testMethod void testProvisionInOFIWithoutMandarotyFields()
	{
		//
		// Create test data
		//
				Account accPayer = new Account(
			Name = 'Test account', 
			BillingCity = 'Bremen', 
			BillingCountry = 'Germany', 
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße', 
			Type = 'Customer', 

			VAT__c = 1,
            Global__c = true
			
		);
		insert accPayer;
        
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1)
			, recordtypeid = Cache.getRecordTypeId('Opportunity.Commercial_Global'),
			Description = 'Test description 20 characters'
		);
		insert opp;		
		 Contact con = Test_Util.createContact(accPayer.id);
		
        Frame_Contract__c fc = new Frame_Contract__c();
        fc.contact__c = con.id;
        fc.customer__c = accPayer.id;
        fc.opportunity__c = opp.id;
         insert fc;      
        
		Billing_Account__c billingAccount = new Billing_Account__c(
			Payer__c = accPayer.Id, 
			Trial__c=false, 
			Billable__c=false,
			VAT__c = '1'
		);
		insert billingAccount;
		//
		// Custom settings
		//
		Email_Settings__c emailSettings = new Email_Settings__c(
				Name = 'Provision_In_OFI',
				Email__c = 'test@m2m.com',
				Subject__c = 'Test OFI',
				Body__c = 'Test OFI');
		insert emailSettings;

		//
		// Remove mandatory field
		//
		
		billingAccount.Payment_Term__c = null;
		update billingAccount;

		//
		// Run controller
		//
		Test.startTest();
		ProvisionInOFIController provisionInOFIController =
				new ProvisionInOFIController(new ApexPages.StandardController(billingAccount));
		provisionInOFIController.provision();
		PageReference pr = provisionInOFIController.gotoBillingAccount();
		Test.stopTest();
		//
		// Tests
		//
		Billing_Account__c[] billingAccounts =
				[ SELECT OFI_Provisioning_Status__c
				  FROM Billing_Account__c
				  WHERE Id = :billingAccount.Id ];
		System.assertEquals(null, billingAccount.OFI_Provisioning_Status__c);
		System.assertNotEquals(null, pr);
		System.assertNotEquals(null, provisionInOFIController.errorMessages);
		System.assert(provisionInOFIController.errorMessages.size() > 0);
	}
}