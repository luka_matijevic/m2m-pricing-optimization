public class PrebillLineItemDistributor {
	
	private Map<Id, Map<BillingRateModel, Prebill__c>> billingAccountPrebills = new Map<Id, Map<BillingRateModel, Prebill__c>>();
	private Map<Id, sObject> lineItemsMap = new Map<Id, sObject>();
	private Daily_Bill__c dailyBill;
	
	private Integer newPrebillsCount = 0;
	private Integer newPrebillLineItemsCount = 0;
	
	private Date billingDate;
	
	/**
	 * Constructor
	 */ 
	public PrebillLineItemDistributor(Date billingDate) {
		this.billingDate = billingDate;
		for (Prebill__c prebill : selectPrebills(billingDate)) {
			addPrebill(prebill);
		}
		this.dailyBill = [
			Select
				Id, Name, Date__c
			from
				Daily_Bill__c
			where
				Date__c = :billingDate
			limit 1
		];
	}
	
	/**
	 * Returns the number of newly inserted prebills
	 */ 
	public Integer getNewPrebillsCount() {
		return Integer.valueOf(this.newPrebillsCount);
	}
	
	
	public Daily_Bill__c getDailyBill(Date billingDate) {
		return this.dailyBill; 
	}

	/**
	 * Returns the number of newly inserted prebill line items
	 */ 
	public Integer getNewPrebillLineItemsCount() {
		return Integer.valueOf(this.newPrebillLineItemsCount);
	}
	
	/**
	 * Stores information about the service or order line item to be used
	 * for linking parents
	 */ 
	public void addLineItem(csord__Order_Line_Item__c oli) {
		lineItemsMap.put(oli.Id, oli);
	}
	
	/**
	 * Stores information about the service or order line item to be used
	 * for linking parents
	 */ 
	public void addLineItem(csord__Service_Line_Item__c sli) {
		lineItemsMap.put(sli.Id, sli);
	}
	
	/**
	 * Tries to add a prebill to the appropriate billing account-billing rate model combination
	 */ 
	public Boolean addPrebill(Prebill__c prebill) {
		if (this.billingAccountPrebills.get(prebill.Billing_Account__c) == null) {
			this.billingAccountPrebills.put(prebill.Billing_Account__c, new Map<BillingRateModel, Prebill__c>{BillingRateModels.billingRateModel(prebill.Billing_Rate_Model__c) => prebill});
			return true;
		} else if (this.billingAccountPrebills.get(prebill.Billing_Account__c).get(BillingRateModels.billingRateModel(prebill.Billing_Rate_Model__c)) == null) {
			this.billingAccountPrebills.get(prebill.Billing_Account__c).put(BillingRateModels.billingRateModel(prebill.Billing_Rate_Model__c), prebill);
			return true;
		}
		return false;
	}
	
	/**
	 * Reads the billing account and billing rate model from the Order line item and returns
	 * the Prebill at that location if it exists, null otherwise.
	 */ 	
	public Prebill__c findPrebill(csord__Order_Line_Item__c oli) {
	    system.debug(oli);
	    system.debug(LineItems.billingAccountId(oli));
	    system.debug(this.billingAccountPrebills);
	    if (oli.billing_account_id__c!=null) {
	        return this.billingAccountPrebills.get(LineItems.billingAccountId(oli)).get(BillingRateModel.ONEOFF);
	    }
		if (this.billingAccountPrebills.get(LineItems.billingAccountId(oli)) == null) {
			return null;
		} else {
			return this.billingAccountPrebills.get(LineItems.billingAccountId(oli)).get(BillingRateModels.billingRateModel(oli));
		}
	}

	/**
	 * Reads the billing account and billing rate model from the Service line item and returns
	 * the Prebill at that location if it exists, null otherwise.
	 */	
	public Prebill__c findPrebill(csord__Service_Line_Item__c sli) {
		if (this.billingAccountPrebills.get(LineItems.billingAccountId(sli)) == null) {
			return null;
		} else {
			return this.billingAccountPrebills.get(LineItems.billingAccountId(sli)).get(BillingRateModels.billingRateModel(sli));
		} 
	}
	
	/**
	 * Tries to locate the originating order or service line item that resulted in creation of a
	 * prebill line item, and then tries to locate the appropriate Prebill for the (billing account
	 * -billing rate model) combination.
	 */ 
	private Prebill__c findPrebill(Prebill_Line_Item__c pbli) {
		system.debug('**IP** finding prebill for '+ JSON.serializePretty(pbli));
		sObject lineItem = findLineItem(pbli);
		system.debug('**IP** lineItem '+ JSON.serializePretty(lineItem));
		if (lineItem instanceOf csord__Order_Line_Item__c) {
			return findPrebill((csord__Order_Line_Item__c) lineItem);
		} else if (lineItem instanceOf csord__Service_Line_Item__c) {
			return findPrebill((csord__Service_Line_Item__c) lineItem);
		}
		return null;
	}
	
	/**
	 * Finds prebill line items without prebills and generates appropriate parent prebills for them from
	 * order or service line items that prebill line items originated from.
	 * After that:
	 * - generated prebills are inserted 
	 * - orphaned prebill items are linked to appropriate generated prebill parents 
	 * - all prebill line items are inserted
	 */ 
	public void linkParentsAndInsert(List<Prebill_Line_Item__c> prebillLineItems) {
		
		List<Prebill__c> newPrebills = new List<Prebill__c>();
		List<Prebill_Line_Item__c> orphanPrebillLineItems = new List<Prebill_Line_Item__c>();
		
		for (Prebill_Line_Item__c pbli : prebillLineItems) {
			if (isOrphan(pbli)) {
				orphanPrebillLineItems.add(pbli);
				sObject lineItem = findLineItem(pbli);
				
				Prebill__c prebill = findPrebill(pbli);
				system.debug('**IP** returned prebill: '+ JSON.serializePretty(prebill)); 
				if (prebill == null) {
					
					if (lineItem instanceof csord__Order_Line_Item__c) {
						prebill = PrebillHelper.buildPrebill((csord__Order_Line_Item__c) lineItem, this.dailyBill);
					} else if (lineItem instanceof csord__Service_Line_Item__c) {
						prebill = PrebillHelper.buildPrebill((csord__Service_Line_Item__c) lineItem, this.dailyBill);
					}
					addPrebill(prebill);
					newPrebills.add(prebill);
				}
			}
		}
		
		system.debug(newPrebills);
		
		insert newPrebills;
		newPrebillsCount += newPrebills.size();
		
		system.debug(prebillLineItems);
		
		for (Prebill_Line_Item__c pbli : orphanPrebillLineItems) {
			Prebill__c parentPrebill = findPrebill(pbli);
			pbli.Prebill__c = parentPrebill.Id; 
		}
		
		system.debug(prebillLineItems);
		
		insert prebillLineItems;
		newPrebillLineItemsCount += prebillLineItems.size();
	}
	
	/**
	 * Tries to find the order or service line item that the given prebill line item
	 * originated from
	 */ 
	public sObject findLineItem(Prebill_Line_Item__c pbli) {
		return lineItemsMap.get(lineItemId(pbli));
	}
	
	/**
	 * 
	 */ 
	private Id lineItemId(Prebill_Line_Item__c pbli) {
		if (pbli.Order_Line_Item__c != null) {
			return pbli.Order_Line_Item__c;
		} else if (pbli.Service_Line_Item__c != null) {
			return pbli.Service_Line_Item__c;
		} else {
			return null;
		}
	}
	
	/**
	 * Returns whether a prebill line item has a parent prebill
	 */ 
	private Boolean isOrphan(Prebill_Line_Item__c pbli) {
		return pbli.Prebill__c == null;
	}
	
	/**
	 * Returns prebills from  
	 */ 
	private List<Prebill__c> selectPrebills(Date billingDate) {
		return [
			SELECT 
				Id, Billing_Account__c, Billing_Rate_Model__c
			FROM 
				Prebill__c
			WHERE
				Daily_Bill__r.Date__c = :billingDate
		];
	}
}