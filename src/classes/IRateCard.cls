public interface IRateCard
{
	//--------------------------------------------------------------------------
	// Getters / setters
	//--------------------------------------------------------------------------
	Rate_Card__c getRateCard();
	
	List<IRateCardCountryManagement> getRateCardCountriesManagement();
	
	List<SelectOption> getAvailableCountries();
	
	String getRateCardId();

	Boolean getShowOnQuote();

	void setShowOnQuote(Boolean showOnQuote);

	String getName();

	void setName(String name);

	String getClockRate();

	void setClockRate(String clockRate);

	Decimal getBaseUsageFee();

	void setBaseUsageFee(Decimal buf);

	Decimal getTargetUsageFee();

	void setTargetUsageFee(Decimal tuf);

	Decimal getExpectedUsage();

	void setExpectedUsage(Decimal expectedUsage);

	Boolean getCanDiscount();

	void setCanDiscount(Boolean canDiscount);

	Boolean getIsTargetFeeDiscounted();

    Decimal getEditedUsageFee();

	void setEditedUsageFee(Decimal buf);

	//--------------------------------------------------------------------------
	// Clone rate card
	//--------------------------------------------------------------------------
	IRateCard deepClone();

	//--------------------------------------------------------------------------
	// Compares two rate cards
	//--------------------------------------------------------------------------
	Boolean equals(IRateCard rateCard);

	//--------------------------------------------------------------------------
	// Compare two rate cards without configuration changes
	//--------------------------------------------------------------------------
	Boolean equalsWithoutConfiguration(IRateCard rateCard);
}