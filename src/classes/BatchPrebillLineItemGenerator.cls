global class BatchPrebillLineItemGenerator implements Database.Batchable<sObject>, Database.stateful {

    private BillingProcessor billingProcessor;
    private Date billingDate;

    public BatchPrebillLineItemGenerator(Date billingDate) {
        this.billingDate = billingDate;
        this.billingProcessor = new BillingProcessor(billingDate);
    }

    /**
     * The start method should gather:
     * - order line items which are not billed
     * - service line items which are active
     * Those items are to be iterated upon in the execute method.
     */
    global List<sObject> start(Database.BatchableContext BC) {

        System.debug(BatchPrebillLineItemGenerator.class + ' scheduled job started, with billing date set to: ' + billingDate + ', and transaction date set to: ' + Date.today());

        // collect service line items and order line items that need to be billed (processing on line item level provides an accurate estimation for batch size)
        List<sObject> orderLineItems = getOrderLineItemsForUpdate(billingDate, billingProcessor.getBillingAccounts());
        List<sObject> discountLineItems = getDiscountLineItemsForUpdate(billingDate, billingProcessor.getBillingAccounts());
        List<sObject> serviceLineItems = getServiceLineItemsForUpdate(billingDate, billingProcessor.getBillingAccounts());

        List<sObject> aggregatedLineItems = new List<sObject>();
        aggregatedLineItems.addAll(orderLineItems);
        aggregatedLineItems.addAll(discountLineItems);
        aggregatedLineItems.addAll(serviceLineItems);

        return aggregatedLineItems;
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {

        system.debug(BatchPrebillLineItemGenerator.class + ' execute method started with batch size of ' + scope.size());

        for (sObject obj : scope) {
            if (obj instanceof csord__Order_Line_Item__c) {
                billingProcessor.queue((csord__Order_Line_Item__c) obj);
            } else if (obj instanceof csord__Service_Line_Item__c) {
                billingProcessor.queue((csord__Service_Line_Item__c) obj);
            }
        }
        billingProcessor.processQueued();
    }

    global void finish(Database.BatchableContext BC) {

        System.debug(BatchPrebillLineItemGenerator.class + ' finish method started');
        System.debug(BatchPrebillLineItemGenerator.class + ' Total new Prebill records: ' + billingProcessor.getNewPrebillsCount());
        System.debug(BatchPrebillLineItemGenerator.class + ' Total new Prebill Line Item records: ' + billingProcessor.getNewPrebillLineItemsCount());

        // Create billing status notification email
        Messaging.SingleEmailMessage email = createReportEmail(billingProcessor);
        Messaging.SendEmailResult result = BillingNotifier.sendEmail(email);

        // call the FinalPrebillFileCSVGenerator
        Database.executeBatch(new FillDailyBillItemObject(this.billingDate),1);
    }

    private Messaging.SingleEmailMessage createReportEmail(BillingProcessor billingProcessor) {

        String subject = 'Daily Bill for billing cycle date ' + billingProcessor.getBillingDate();
        String body;

        if (billingProcessor.hasFailedBillingAccounts()) {
            subject += ' failed!';
            body = 'Number of processed billing accounts:' + billingProcessor.getBillingAccounts().size() + '\n';
            body += '\n\n';
            body += 'Number of prebill line items generated: ' + billingProcessor.getNewPrebillLineItemsCount() + '\n';
            body += 'Number of failed items (order and service line items): ' + billingProcessor.getFailedBillingAccounts().size() + '\n';
            body += '\n\n';
            body += 'You can check the billing accounts for failed items using the following links:\n';
            for (Billing_Account__c billingAccount : billingProcessor.getFailedBillingAccounts()) {
                body += URL.getSalesforceBaseUrl().toExternalForm() + '/' + billingAccount.Id + '\n';
            }
        } else {
            subject += ' was successful';
            body = 'Number of processed billing accounts:' + billingProcessor.getBillingAccounts().size() + '\n';
            body += '\n\n';
            body += 'Total of prebill line items generated: ' + billingProcessor.getNewPrebillLineItemsCount() + '\n';
        }

        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setSubject(subject);
        email.setPlainTextBody(body);

        return email;
    }

    /**
     * get all order line items of billing accounts selected above, that haven't been billed for yet for this billing cycle (check both the is_billed field and if prebill line item exist for a ordli)
     * all order line items should be one-time charges
     * lock for update - if second lock is attempted, just throw an exception so no items are processed
     */
    private List<csord__Order_Line_Item__c> getOrderLineItemsForUpdate(Date billingDate, List<Billing_Account__c> billingAccounts) {
        Date firstDayOfMonth = Date.newInstance(billingDate.year(), billingDate.month(), 1);
        return [
            SELECT
                Id, Name, csord__Order__r.Order_Request__r.Billing_Account__c, csord__Total_Price__c,
                Quantity__c, Journal_Mapping__c, Billing_Rate_Model__c, billing_Account_id__c,
                csord__Order__r.Order_Request__r.Name, csord__Order__r.Order_Request__r.Id,
                csord__Order__r.Order_Request__r.RecordTypeId
            FROM
                csord__Order_Line_Item__c
            WHERE
                Is_Billed__c = false
            AND
                csord__Order__r.Order_Request__r.Billing_Account__c IN :billingAccounts
            AND 
                csord__Order_Line_Item__c.CreatedDate < :firstDayOfMonth
            AND
                Id NOT IN (
                    SELECT
                        Order_Line_Item__c
                    FROM
                        Prebill_Line_Item__c
                    WHERE
                        Prebill__r.Billing_Account__c IN :billingAccounts
                    AND
                        Prebill__r.Daily_Bill__r.Date__c = :billingDate
                )
            FOR UPDATE
        ];
    }

    private List<csord__Order_Line_Item__c> getDiscountLineItemsForUpdate(Date billingDate, List<Billing_Account__c> billingAccounts) {
        Date firstDayOfMonth = Date.newInstance(billingDate.year(), billingDate.month(), 1);
        
        List<csord__Order_Line_Item__c> retList = new List<csord__Order_Line_Item__c>();
        List<Billing_Account_Discount__c> billAccDiscList = [SELECT id, amount__c, Billing_Account__c,Billing_Account_Subsc_Group__r.Service_Pack_Name__c,Discount_Type__c,Date_Start__c,Date_end__c from Billing_Account_Discount__c where Billing_Account__c in :billingAccounts and active__c=true];
        
       // List<Prebill_Line_Item__c> list = [SELECT id from Prebill_Line_Item__c where Billing_Account__c in :billingAccounts];
        
        for (Billing_Account_Discount__c bada: billAccDiscList) {

            csord__Order_Line_Item__c cl = new csord__Order_Line_Item__c();
            /*
                24.1.2017. P. Matkovic:
                Added condition in query "prebill__r.daily_bill__r.date__c = :billingDate"
                to return prebill line items for month we are creating bills.
                Before this query would return prebill line items for all months and code is accessing first one (position [0])
            */
            List<Prebill_Line_Item__c> pbli = [SELECT id,Line_Item_Amount__c,Line_Item_Description__c,prebill__r.billing_Account__c,prebill__r.Daily_Bill__r.date__c 
                                                from Prebill_Line_Item__c 
                                                where  prebill__r.billing_Account__c = :bada.Billing_Account__c and 
                                                        prebill__r.daily_bill__r.date__c = :billingDate and
                                                        line_item_subscription_group_name__c=:bada.Billing_Account_Subsc_Group__r.Service_Pack_Name__c and 
                                                        line_item_discount_type__c=:bada.Discount_Type__c];
            system.debug(pbli);
            system.debug(bada.Billing_Account__c);
            system.debug(bada.Billing_Account_Subsc_Group__r.Service_Pack_Name__c);
            system.debug(bada.Discount_Type__c);            
                


            if (!pbli.isEmpty()) {
            String from_date ='';
            decimal factor=1;
            if(bada.Date_Start__c.month()==pbli[0].prebill__r.Daily_Bill__r.date__c.addMonths(-1).month() && bada.Date_Start__c.year()==pbli[0].prebill__r.Daily_Bill__r.date__c.addMonths(-1).year())
            {
                from_date=bada.Date_Start__c.format();
                factor=decimal.valueOf(Date.DaysinMonth(bada.Date_Start__c.year(),bada.Date_Start__c.month()) - bada.Date_Start__c.day() + 1)/decimal.valueOf(Date.DaysinMonth(bada.Date_Start__c.year(),bada.Date_Start__c.month()));
            }
            else from_date=pbli[0].prebill__r.Daily_Bill__r.date__c.addMonths(-1).toStartOfMonth().format();
            
            String end_date ='';
            if(bada.Date_End__c.month()==pbli[0].prebill__r.Daily_Bill__r.date__c.addMonths(-1).month() && bada.Date_End__c.year()==pbli[0].prebill__r.Daily_Bill__r.date__c.addMonths(-1).year())
            {
                end_date=bada.Date_End__c.format();
                factor=decimal.valueOf(bada.Date_End__c.day())/decimal.valueOf(Date.DaysinMonth(bada.Date_End__c.year(),bada.Date_End__c.month()));
            }
            else end_date=pbli[0].prebill__r.Daily_Bill__r.date__c.adddays(-1).format();
            
            cl.csord__Total_Price__c=(-1)*pbli[0].Line_Item_Amount__c*(bada.amount__c/100)*factor;
            cl.csord__Line_Description__c=pbli[0].Line_Item_Description__c + ' - ' + bada.amount__c + '% Rabatt ('+from_date+'-'+end_date+')';
            cl.Quantity__c=1;
            cl.Journal_Mapping__c='a1Cb0000000KYtm';
            cl.name='Rabatt';
            //cl.csord__Order__c='a1P7E0000002nKS';
            cl.billing_account_id__c = pbli[0].prebill__r.billing_Account__c;
            
            retList.add(cl);
                
            }
        }
        return retList;
        
    }



    /**
    * get all service line items of billing accounts selected above, that haven't been billed for yet for this billing cycle
    * (check both the is_billed field and if prebill line item(s) exist for a sli)
    * add a month after deactivation to charge the client for the last (partial) billing cycle too
    */
    private List<csord__Service_Line_Item__c> getServiceLineItemsForUpdate(Date billingDate, List<Billing_Account__c> billingAccounts) {
        Date firstDayOfMonth = Date.newInstance(billingDate.year(), billingDate.month(), 1);
        return [
            SELECT
                Id, Name,
                csord__Service__r.csord__Subscription__r.Billing_Account__c,
                csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__r.Name,
                csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__r.RecordTypeId,
                csord__Service__r.csord__Activation_Date__c,
                csord__Service__r.csord__Deactivation_Date__c,
                csord__Service__r.Billing_Rate_Model__c,
                csord__Is_Recurring__c, Journal_Mapping__c, Quantity__c, Total_Price__c,
                csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__r.Id
            FROM
                csord__Service_Line_Item__c
            WHERE
                Is_Billed__c = false
            AND
                csord__Service__r.csord__Deactivation_Date__c > :billingDate.addMonths(-1)
            AND
                csord__Service__r.csord__Subscription__r.Billing_Account__c in :billingAccounts
            AND 
                csord__Service_Line_Item__c.csord__Service__r.csord__Activation_Date__c < :firstDayOfMonth
            AND
                Id NOT IN (
                    SELECT
                        Service_Line_Item__c
                    FROM
                        Prebill_Line_Item__c
                    WHERE
                        Prebill__r.Billing_Account__c IN :billingAccounts
                    AND Prebill__r.Daily_Bill__r.Date__c =:billingDate
                )
            FOR UPDATE
        ];
    }
}