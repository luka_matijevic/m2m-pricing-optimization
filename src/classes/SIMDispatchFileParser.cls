/**
class that exposes methods needed for the parsing of theArvato export file
*/
public class SIMDispatchFileParser {

	/**
	iccid pattern as in the arvato export file since there is always a pair odf the same iccid we'll use the one in quotations
	*/
	private static final String iccidPatternString = '((")(\\d{20}|\\d{19}"))';
	private static final String iccidPatternStringWithoutQuotes = '(\\d{20}|\\d{19})';
	private static Pattern iccidPattern;
	private static Pattern iccidPatternWithoutQuotes;
	public static Set<String> finalIccds = new Set<String>();
	private static final Integer maxStringLength = 100000;


    private static final String iccidAndMSISDN = '(\\d{20}|\\d{19}|\\d{15})';
    private static Pattern iccidAndMSISDNPattern;
    
    
	static
	{
		iccidPattern = Pattern.compile(iccidPatternString);
		iccidPatternWithoutQuotes = Pattern.compile(iccidPatternStringWithoutQuotes);
		iccidAndMSISDNPattern = Pattern.compile(iccidAndMSISDN);
		
	}

	/**
	parses the arvato file and returns a set of found iccids
	*/
	public static Set<String> parseArvatoExportFile(String arvatoFileContents,boolean excludeQuotes)
	{
		Set<String> iccIds = new Set<String>();
		System.debug('String Value Length inside parseArvatoExportFile : ' + arvatoFileContents.length());
		System.debug(LoggingLevel.ERROR, 'Heap Size in inside parseArvatoExportFile(String) : ' + Limits.getHeapSize() + '/' + Limits.getLimitHeapSize());
		Matcher iccidStringMatcher;
		if(excludeQuotes){
		    iccidStringMatcher = iccidPatternWithoutQuotes.matcher(arvatoFileContents);
		}else{
		    iccidStringMatcher = iccidPattern.matcher(arvatoFileContents);
		}
		while(iccidStringMatcher.find())
		{
			string iccId = iccidStringMatcher.group().replace('"', '');
			if(!iccIds.contains(iccId))
				iccIds.add(iccId);
		} 
		
		return iccIds;
	}

	/**
	Converts the Blob to String and divide the string into chunks to avoid hitting "Regex too complicated" error and
	send the chunks to another method to get iccids.
	*/
	public static set<string> parseArvatoExportFile(Blob arvatoFile, boolean excludeQuotes){
	    System.debug(LoggingLevel.ERROR, 'Heap Size in inside parseArvatoExportFile(Blob) : ' + Limits.getHeapSize() + '/' + Limits.getLimitHeapSize());
        Integer noOfParts = Integer.valueOf(Math.Floor(arvatoFile.toString().length() / maxStringLength));
        system.debug('arvatoFile.toString().length() ** '+arvatoFile.toString().length() +'noOfParts **'+noOfParts);
        Integer firstIndex = 0;
        Integer NextIndex = maxStringLength+1; 
        for(Integer i = 2; i <= noOfParts; i++)
        {
            Integer trIndex = arvatoFile.toString().subString(NextIndex,i*maxStringLength).indexOf('</tr>');
            if(trIndex != -1)
            {
                finalIccds.addAll(parseArvatoExportFile(arvatoFile.toString().subString(firstIndex,NextIndex+trIndex),excludeQuotes));
                firstIndex = (i-1)*maxStringLength+trIndex + 1;
                NextIndex = i*maxStringLength;
            }
        }
        finalIccds.addAll(parseArvatoExportFile(arvatoFile.toString().subString(firstIndex),excludeQuotes));
        return finalIccds;
	    return null;    
	}

	/**
	utility method to match just one whole string to the regex
	IMPORTANT:
	string format "ICCID"
	*/
	public static boolean matchString(String stringToMatch)
	{
		Matcher iccidStringMatcher = iccidPattern.matcher(stringToMatch);
		return  iccidStringMatcher.matches();
	}
	
	
	/**
	Converts the Blob to String and divide the string into chunks to avoid hitting "Regex too complicated" error and
	send the chunks to another method to get iccids and MSISDN.
	*/
	public static set<string> parseIccidAndMSISDNExportFile(Blob arvatoFile, boolean excludeQuotes){
	    System.debug(LoggingLevel.ERROR, 'Heap Size in inside parseArvatoExportFile(Blob) : ' + Limits.getHeapSize() + '/' + Limits.getLimitHeapSize());
        Integer noOfParts = Integer.valueOf(Math.Floor(arvatoFile.toString().length() / maxStringLength));
        system.debug('arvatoFile.toString().length() ** '+arvatoFile.toString().length() +'noOfParts **'+noOfParts);
        Integer firstIndex = 0;
        Integer NextIndex = maxStringLength+1; 
        for(Integer i = 2; i <= noOfParts; i++)
        {
            Integer trIndex = arvatoFile.toString().subString(NextIndex,i*maxStringLength).indexOf('</tr>');
            if(trIndex != -1)
            {
                finalIccds.addAll(parseIccidAndMSISDN(arvatoFile.toString().subString(firstIndex,NextIndex+trIndex)));
                firstIndex = (i-1)*maxStringLength+trIndex + 1;
                NextIndex = i*maxStringLength;
            }
        }
        finalIccds.addAll(parseIccidAndMSISDN(arvatoFile.toString().subString(firstIndex)));
        return finalIccds;
	    return null;    
	}
	
	public static Set<String> parseIccidAndMSISDN(String arvatoFileContents)
	{
		Set<String> iccIds = new Set<String>();
		Matcher iccidStringMatcher;
		iccidStringMatcher = iccidAndMSISDNPattern.matcher(arvatoFileContents);
		while(iccidStringMatcher.find())
		{
			string iccId = iccidStringMatcher.group().replace('"', '');
			if(!iccIds.contains(iccId))
				iccIds.add(iccId);
		} 
		
		return iccIds;
	}
	
}