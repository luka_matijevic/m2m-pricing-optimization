public class TicketCommentsInlineController {

    private Case caseObject;
    public List<CaseComment> caseComments{get;set;}
    public Id caseCommentId{get;set;}
    public TicketCommentsInlineController(ApexPages.StandardController controller){
        this.caseObject = (Case) controller.getRecord();
        getCaseComments();
    }
    
    public List<CaseComment> getComments(){
        
        return caseComments;
    }
    
    public void getCaseComments(){
        caseComments = [Select Id, CommentBody, ParentId, IsPublished from CaseComment where ParentId = : caseObject.Id Order by CreatedDate];
    }
    
    public PageReference NewCaseComment()
    {
        PageReference newTarget = new PageReference('/' + Schema.SObjectType.CaseComment.getKeyPrefix()
             + '/e?parent_id=' + caseObject.Id + 
             '&retURL=' + caseObject.Id);
        return newTarget ;
    }
    
    public PageReference EditCaseComment()
    {
        PageReference editTarget = new PageReference('/' + caseCommentId + '/e?parent_id=' + caseObject.Id + '&retURL=' + caseObject.Id);
        return editTarget;
    }
    
    public PageReference DeleteCaseComment()
    {
        List<CaseComment> ccToDelete = [Select Id from CaseComment where Id =:caseCommentId];
        delete ccToDelete;
        caseCommentId = null; 
        getCaseComments();
        return null; 
    }
    
    public void MakeCaseCommentPublic(){
        List<CaseComment> ccToPublish = [Select Id, IsPublished from CaseComment where Id =:caseCommentId];
        ccToPublish[0].IsPublished = true;
        update ccToPublish;
        getCaseComments();
        //return null;
    }
    
    public void ToggleCaseCommentPublicPrivate(){
        List<CaseComment> ccToPublish = [Select Id, IsPublished from CaseComment where Id =:caseCommentId];
        if(ccToPublish[0].IsPublished == false){
            ccToPublish[0].IsPublished = true;
        }else{
            ccToPublish[0].IsPublished = false;
        }
        update ccToPublish;
        getCaseComments();
    }

}