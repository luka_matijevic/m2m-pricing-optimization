/*
* Description: Batch class to count the total number of Stock records for StockServiceV1 processing & update in a CustomSetting field.
* Author: Ritesh Saxena
* Date: 25/08/2014
*/
public class BatchStockCount implements Database.Batchable<sObject>, Database.Stateful {
    
    private static String query = 'Select Id from Stock__c';
    private Integer count=0;
    
    /*
    * Start call to process SIM record query.
    */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        if(test.isRunningTest()){
            query+= ' limit 25';
        }
        return Database.getQueryLocator(query);  
    }
    
    /*
    * Execute call to process SIM record count.
    */
    public void execute(Database.BatchableContext BC, List<SObject> scope) {
        List<Stock__c> simRec = (List<Stock__c>) scope;
        if(null!=simRec && !simRec.isEmpty()){
            count= count+ simRec.size();
        }
    }
    
    /*
    * Finish call to process SIM record count update to custom setting.
    */
    public void finish(Database.BatchableContext BC) {

        BIC_Report_Record_Size__c obj = BIC_Report_Record_Size__c.getOrgDefaults();  
            if(obj!=null){
                obj.Number_Of_SIMs__c = count;
                upsert obj;
            }              
        }
}