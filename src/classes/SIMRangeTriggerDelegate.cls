public class SIMRangeTriggerDelegate extends TriggerHandler.DelegateBase {
    /**
* Check range start, end (numbers, start<end, ranges cant overlap )
*/
    public static Integer ICCIDLength = 18;
    public static Integer IMSILength = 15;
    
    Map<Id, SIM_Range__c> simRangeList = new Map<Id, SIM_Range__c>();
    public override void prepareBefore() {
        if (Trigger.isInsert || Trigger.isUpdate) {
            Map<Id, SIM_Range__c> allRanges = new Map<Id, SIM_Range__c>([select Range_start__c, Range_end__c, Name, Range_Type__c from SIM_Range__c where id not in :Trigger.new]);
            String paddingZero = '0';
            String paddingNine = '9';
           
            for (SIM_Range__c s : (List<SIM_Range__c>) Trigger.new){
                
                if (s.Range_Type__c.equals('ICCID')){
                    s.Range_start__c = s.Range_start__c + paddingZero.repeat(ICCIDLength - s.Range_start__c.length());
                    s.Range_end__c = s.Range_end__c + paddingNine.repeat(ICCIDLength  - s.Range_end__c.length());
                    checkRange(s, allRanges);
                }
                else if (s.Range_Type__c.equals('IMSI')){
                    s.Range_start__c = s.Range_start__c + paddingZero.repeat(IMSILength - s.Range_start__c.length());
                    s.Range_end__c = s.Range_end__c + paddingNine.repeat(IMSILength  - s.Range_end__c.length());
                    checkRange(s, allRanges);
                }
            }
            
            
            
            /* 20.05.2015 Luka - range can be defined for ICCID or IMSI 
Map<Id, SIM_Range__c> allRanges = new Map<Id, SIM_Range__c>([select Range_start__c, Range_end__c, Name from SIM_Range__c where id not in :Trigger.new]);
String paddingZero = '0';
String paddingNine = '9';
for (SIM_Range__c s : (List<SIM_Range__c>) Trigger.new){

String errorText = '';
s.Range_start__c = s.Range_start__c + paddingZero.repeat(SIM_Range__c.Range_start__c.getDescribe().getLength() - s.Range_start__c.length());
s.Range_end__c = s.Range_end__c + paddingNine.repeat(SIM_Range__c.Range_start__c.getDescribe().getLength()  - s.Range_end__c.length());


if ( !s.Range_start__c.isNumeric() ) errorText += ' Range start has to be a number.';
if ( !s.Range_end__c.isNumeric() ) errorText += ' Range end has to be a number.';

if ( (s.Range_start__c.isNumeric() && s.Range_end__c.isNumeric()) && 
(AdditionalUtils.compareBigNumbers( s.Range_start__c, s.Range_end__c ) >= 0 ) ) 
errorText += ' Range end has to be greater than range start.';
//no errors, check overlap
if (errorText.length()==0){
for (SIM_Range__c range : allRanges.values()){
if ((AdditionalUtils.compareBigNumbers(s.Range_start__c ,range.Range_start__c)>=0 &&
AdditionalUtils.compareBigNumbers(s.Range_start__c , range.Range_end__c)<=0)
||
(
AdditionalUtils.compareBigNumbers(s.Range_end__c , range.Range_start__c)>=0 &&
AdditionalUtils.compareBigNumbers(s.Range_end__c , range.Range_end__c)<=0
))
errorText += ' Range overlaps range: ' + range.Name;
}

}


if (errorText.length()>0) s.addError(errorText);
}
}
*/
        }     
    } 
    private void checkRange(Sim_Range__c s, Map<Id, SIM_Range__c> allRanges){
        String errorText = '';
        if ( !s.Range_start__c.isNumeric() ) errorText += ' Range start has to be a number.';
        if ( !s.Range_end__c.isNumeric() ) errorText += ' Range end has to be a number.';
        
        if ( (s.Range_start__c.isNumeric() && s.Range_end__c.isNumeric()) && 
            (AdditionalUtils.compareBigNumbers( s.Range_start__c, s.Range_end__c ) >= 0 ) ) 
            errorText += ' Range end has to be greater than range start.';
        //no errors, check overlap
        if (errorText.length()==0){
            for (SIM_Range__c range : allRanges.values()){
                if ((AdditionalUtils.compareBigNumbers(s.Range_start__c ,range.Range_start__c)>=0 &&
                     AdditionalUtils.compareBigNumbers(s.Range_start__c , range.Range_end__c)<=0)
                    ||
                    (
                        AdditionalUtils.compareBigNumbers(s.Range_end__c , range.Range_start__c)>=0 &&
                        AdditionalUtils.compareBigNumbers(s.Range_end__c , range.Range_end__c)<=0
                    ))
                    errorText += ' Range overlaps range: ' + range.Name;
            }
            
        }
        
        
        if (errorText.length()>0) s.addError(errorText);
    }
    
    
}