/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ModifySIMsInboundProcessorTest {

    @isTest static void testmodifySimProcessing() {
		
		 SIM_Termination_Job__c jobData = new SIM_Termination_Job__c(
			Name = 'SIM Termination JOB @ Test',
			Status__c = 'TERMINATION PENDING'
		);

		insert jobData;

		csam_t1__Outgoing_Message__c outMsg = new csam_t1__Outgoing_Message__c(
			Name = 'outgoing message',
			csam_t1__Callback_Host__c = 'test host', 
			csam_t1__Content_Type__c = 'test type', 
			csam_t1__URL_Host__c = 'test url'
		);

		insert outMsg;

		csam_t1__Outgoing_Message_Record__c record = new csam_t1__Outgoing_Message_Record__c(
			csam_t1__Object_Name__c = 'SIM_Termination_Job__c',
			csam_t1__Object_Record_Id__c = jobData.Id,
			csam_t1__Outgoing_Message__c = outMsg.Id
		);

		insert record;

		csam_t1__Incoming_Message__c inMsg = new csam_t1__Incoming_Message__c(
			csam_t1__Outgoing_Message__c = outMsg.Id,
			csam_t1__HTTP_Method__c = 'PUT',
			csam_t1__Mac_Signature__c = 'mac'
		);

		insert inMsg;

		CompressedMessage cm = new CompressedMessage();
		cm.name = 'test';
		cm.content = 'Y29udGVudA=='; // == base64('content')
 
		// Name has to start with 'Message' as implemented in the 
		// async messaging package functionality 
		Attachment msgAtt = new Attachment(
			Name = 'Message attachment',
			Body = Blob.valueOf(JSON.serialize(cm)),
			ParentId = inMsg.Id
		);

		insert msgAtt;

		ModifySIMsInboundProcessor processor = new ModifySIMsInboundProcessor();

		processor.processMessage(inMsg, new Map<Id, Id>());

		List<Attachment> attachments = [
			Select Id, Name, Body, ParentId, ContentType
			from Attachment 
			where ParentId = :jobData.Id
		];

		System.assertEquals(0, attachments.size());

		
	}	
}