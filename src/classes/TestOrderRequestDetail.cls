@isTest
private class TestOrderRequestDetail
{

	static void createTestData()
	{
		//
		// Create test data
		//
		No_Trigger__c notriggers = new No_Trigger__c();
		notriggers.Flag__c = true;
		insert notriggers;

		TestHelper.createOrderRequest();
		TestHelper.createJournalMappings();

		notriggers.Flag__c = false;
		update notriggers;
	}

	//
	// Get all details about current order
	// Details are:
	//   - product orders
	//     - name
	//     - quantity
	//     - product configurations
	//       - name
	//       - contract term
	//       - billing rate model
	//       - aisRecordType
	//       - product configuration order id
	//       - one time journal mapping id
	//       - recurring journal mapping id
	//       - line items
	//          - name
	//          - description
	//          - recurring
	//       	- calculated price
	//
    static testMethod void retrieveOrderRequestDetail()
	{
    	createTestData();

		Order_Request__c orderRequest =
				[ SELECT Id
				  FROM Order_Request__c
				  LIMIT 1 ];

		Test.startTest();
		OrderRequestDetail orderRequestDetail =
			new OrderRequestDetail(orderRequest.Id);
		List<ProductOrder> productOrders =
			orderRequestDetail.getProductOrders();
		//
		// Tests
		//
		Product_Configuration_Order__c[] pcos =
				[ SELECT Id, Product_Configuration__c
				  FROM Product_Configuration_Order__c ];
		Journal_Mapping__c oneTimeJournalMapping =
				[ SELECT Id
				  FROM Journal_Mapping__c
				  WHERE Name = 'M2M Hardware'
				  AND Country__c = 'Non-DE' ];
		Journal_Mapping__c recurringJournalMapping =
				[ SELECT Id
				  FROM Journal_Mapping__c
				  WHERE Name = 'Recuring Fee'
				  AND Country__c = 'Non-DE' ];

		//
		// Product orders
		//
		System.assertEquals(4, productOrders.size());//changed from 2 to 4 for test resource
		System.assertEquals('SmartMeter', productOrders[0].name);
		System.assertEquals(50, productOrders[0].quantity);
		System.assertEquals('SIM & Tariff', productOrders[1].name);
		System.assertEquals(100, productOrders[1].quantity);
		//
		// Product configuration orders
		//
		System.assertEquals(1, productOrders[0].productConfigurations.size());
		System.assertEquals(1, productOrders[1].productConfigurations.size());

		ProductOrder.ProductConfiguration productConfiguration0 = productOrders[0].productConfigurations[0];
		System.assertEquals('MR_CarModem', productConfiguration0.name);
		System.assertEquals(24, productConfiguration0.contractTerm);
		System.assertEquals('Rate Payment', productConfiguration0.billingRateModel);
		System.assertEquals('Hardware', productConfiguration0.aisRecordType);
		System.assertEquals(pcos[0].Id, productConfiguration0.productConfigurationOrderId);
		System.assertEquals(oneTimeJournalMapping.Id, productConfiguration0.oneTimeJournalMappingId);
		System.assertEquals(recurringJournalMapping.Id, productConfiguration0.recurringJournalMappingId);

		ProductOrder.ProductConfiguration productConfiguration1 = productOrders[1].productConfigurations[0];
		System.debug(productConfiguration1);
		System.debug('Child Order' + productConfiguration1.productOrders.size());
		System.assertEquals('SIM & Tariff', productConfiguration1.name);
		System.assertEquals(24, productConfiguration1.contractTerm);
		System.assertEquals('One Off', productConfiguration1.billingRateModel);
		System.assertEquals('SIM', productConfiguration1.aisRecordType);
		System.assertEquals(pcos[1].Id, productConfiguration1.productConfigurationOrderId);
		System.assertEquals(oneTimeJournalMapping.Id, productConfiguration1.oneTimeJournalMappingId);
		System.assertEquals(recurringJournalMapping.Id, productConfiguration1.recurringJournalMappingId);
		//
		// Line items
		//
		List<ProductOrder.LineItem> lineItems0 = productConfiguration0.lineItems;
		List<ProductOrder.LineItem> lineItems1 = productConfiguration1.lineItems;

		System.assertEquals(2, lineItems0.size());
		System.assertEquals(3, lineItems1.size());

		System.assertEquals('Recurring Target Fee Total', lineItems0[1].name);
		System.assertEquals('100 x Recurring Fee', lineItems0[1].description);
		System.assertEquals(true, lineItems0[1].recurring);
		System.assertEquals(false, lineItems0[1].sm2m);
		System.assertEquals(500, lineItems0[1].price);

		System.assertEquals('One-Time Target Fee Total', lineItems0[0].name);
		System.assertEquals('100 x One-Time Fee', lineItems0[0].description);
		System.assertEquals(false, lineItems0[0].recurring);
		System.assertEquals(false, lineItems0[0].sm2m);
		System.assertEquals(44500, lineItems0[0].price);

		System.assertEquals('SIM One-Time Target Fee Total', lineItems1[0].name);
		System.assertEquals('100 x SIM Card One-Time Fee', lineItems1[0].description);
		System.assertEquals(false, lineItems1[0].recurring);
		System.assertEquals(false, lineItems1[0].sm2m);
		System.assertEquals(150, lineItems1[0].price);

		System.assertEquals('Tariff Recurring Target Fee Total', lineItems1[2].name);
		System.assertEquals('100 x Tariff Recurring Fee', lineItems1[2].description);
		System.assertEquals(true, lineItems1[2].recurring);
		System.assertEquals(true, lineItems1[2].sm2m);
		System.assertEquals(1200, lineItems1[2].price);

		System.assertEquals('Tariff One-Time Target Fee Total', lineItems1[1].name);
		System.assertEquals('100 x Tariff One-Time Fee', lineItems1[1].description);
		System.assertEquals(false, lineItems1[1].recurring);
		System.assertEquals(true, lineItems1[1].sm2m);
		System.assertEquals(3200, lineItems1[1].price);
		//
		// Child product orders
		//
		ProductOrder[] childProductOrders = productConfiguration1.productOrders;
		System.assertEquals(2, childProductOrders.size());

		//
		// Child product order quantities
		//
		System.assertEquals(2, childProductOrders[0].quantity);
		System.assertEquals(3, childProductOrders[1].quantity);

		//
		// Child product configuration sizes
		//
		System.assertEquals(1, childProductOrders[0].productConfigurations.size());
		System.assertEquals(1, childProductOrders[1].productConfigurations.size());

		//
		// Child line item sizes
		//
		ProductOrder.ProductConfiguration childConfiguration0 = childProductOrders[0].productConfigurations[0];
		ProductOrder.ProductConfiguration childConfiguration1 =	childProductOrders[1].productConfigurations[0];

		ProductOrder.LineItem[] childLineItems0 = childConfiguration0.lineItems;
		ProductOrder.LineItem[] childLineItems1 = childConfiguration1.lineItems;
		System.assertEquals(1, childLineItems0.size());
		System.assertEquals(1, childLineItems1.size());


		System.assertEquals('Recurring Target Fee Total', childLineItems0[0].name);
		System.assertEquals('6 x Recurring Fee', childLineItems0[0].description);
		System.assertEquals(true, childLineItems0[0].recurring);
		System.assertEquals(true, childLineItems0[0].sm2m);
		System.assertEquals(0.68, childLineItems0[0].price);

		System.assertEquals('Recurring Target Fee Total', childLineItems1[0].name);
		System.assertEquals('3 x Recurring Fee', childLineItems1[0].description);
		System.assertEquals(true, childLineItems1[0].recurring);
		System.assertEquals(true, childLineItems1[0].sm2m);
		System.assertEquals(12, childLineItems1[0].price);
    }

	//
	// Get all details about current order
	// Details are:
	//   - product orders
	//     - name
	//     - quantity
	//     - product configurations
	//       - name
	//       - contract term
	//       - billing rate model
	//       - aisRecordType
	//       - product configuration order id
	//       - one time journal mapping id
	//       - recurring journal mapping id
	//       - line items
	//          - name
	//          - description
	//          - recurring
	//       	- calculated price
	//
    static testMethod void join()
	{
    	createTestData();

		Order_Request__c orderRequest =
				[ SELECT Id
				  FROM Order_Request__c
				  LIMIT 1 ];

		Test.startTest();
		OrderRequestDetail orderRequestDetail =
			new OrderRequestDetail(orderRequest.Id);
		List<ProductOrder> productOrders =
			orderRequestDetail.join().getProductOrders();
		//
		// Tests
		//
		Product_Configuration_Order__c[] pcos =
				[ SELECT Id, Product_Configuration__c
				  FROM Product_Configuration_Order__c ];
		Journal_Mapping__c oneTimeJournalMapping =
				[ SELECT Id
				  FROM Journal_Mapping__c
				  WHERE Name = 'M2M Hardware'
				  AND Country__c = 'Non-DE' ];
		Journal_Mapping__c recurringJournalMapping =
				[ SELECT Id
				  FROM Journal_Mapping__c
				  WHERE Name = 'Recuring Fee'
				  AND Country__c = 'Non-DE' ];

		//
		// Product orders
		//
		System.assertEquals(2, productOrders.size());
		System.assertEquals('SmartMeter', productOrders[0].name);
		System.assertEquals(50, productOrders[0].quantity);
		System.assertEquals('SIM & Tariff', productOrders[1].name);
		System.assertEquals(100, productOrders[1].quantity);
		//
		// Product configuration orders
		//
		System.assertEquals(1, productOrders[0].productConfigurations.size());
		System.assertEquals(1, productOrders[1].productConfigurations.size());

		ProductOrder.ProductConfiguration productConfiguration0 = productOrders[0].productConfigurations[0];
		System.assertEquals('MR_CarModem', productConfiguration0.name);
		System.assertEquals(24, productConfiguration0.contractTerm);
		System.assertEquals('Rate Payment', productConfiguration0.billingRateModel);
		System.assertEquals('Hardware', productConfiguration0.aisRecordType);
		System.assertEquals(pcos[0].Id, productConfiguration0.productConfigurationOrderId);
		System.assertEquals(oneTimeJournalMapping.Id, productConfiguration0.oneTimeJournalMappingId);
		System.assertEquals(recurringJournalMapping.Id, productConfiguration0.recurringJournalMappingId);

		ProductOrder.ProductConfiguration productConfiguration1 = productOrders[1].productConfigurations[0];
		System.assertEquals('SIM & Tariff', productConfiguration1.name);
		System.assertEquals(24, productConfiguration1.contractTerm);
		System.assertEquals('One Off', productConfiguration1.billingRateModel);
		System.assertEquals('SIM', productConfiguration1.aisRecordType);
		System.assertEquals(pcos[1].Id, productConfiguration1.productConfigurationOrderId);
		System.assertEquals(oneTimeJournalMapping.Id, productConfiguration1.oneTimeJournalMappingId);
		System.assertEquals(recurringJournalMapping.Id, productConfiguration1.recurringJournalMappingId);
		//
		// Child product orders
		//
		System.assertEquals(0, productConfiguration0.productOrders.size());
		System.assertEquals(0, productConfiguration1.productOrders.size());
		//
		// Line items
		//
		List<ProductOrder.LineItem> lineItems0 = productConfiguration0.lineItems;
		List<ProductOrder.LineItem> lineItems1 = productConfiguration1.lineItems;

		System.assertEquals(2, lineItems0.size());
		System.assertEquals(5, lineItems1.size());

		System.assertEquals('One-Time Target Fee Total', lineItems0[0].name);
		System.assertEquals('100 x One-Time Fee', lineItems0[0].description);
		System.assertEquals(false, lineItems0[0].recurring);
		System.assertEquals(false, lineItems0[0].sm2m);
		System.assertEquals(44500, lineItems0[0].price);

		System.assertEquals('Recurring Target Fee Total', lineItems0[1].name);
		System.assertEquals('100 x Recurring Fee', lineItems0[1].description);
		System.assertEquals(true, lineItems0[1].recurring);
		System.assertEquals(false, lineItems0[1].sm2m);
		System.assertEquals(500, lineItems0[1].price);

		System.assertEquals('SIM One-Time Target Fee Total', lineItems1[0].name);
		System.assertEquals('100 x SIM Card One-Time Fee', lineItems1[0].description);
		System.assertEquals(false, lineItems1[0].recurring);
		System.assertEquals(false, lineItems1[0].sm2m);
		System.assertEquals(150, lineItems1[0].price);

		System.assertEquals('Tariff One-Time Target Fee Total', lineItems1[1].name);
		System.assertEquals('100 x Tariff One-Time Fee', lineItems1[1].description);
		System.assertEquals(false, lineItems1[1].recurring);
		System.assertEquals(true, lineItems1[1].sm2m);
		System.assertEquals(3200, lineItems1[1].price);

		System.assertEquals('Tariff Recurring Target Fee Total', lineItems1[2].name);
		System.assertEquals('100 x Tariff Recurring Fee', lineItems1[2].description);
		System.assertEquals(true, lineItems1[2].recurring);
		System.assertEquals(true, lineItems1[2].sm2m);
		System.assertEquals(1200, lineItems1[2].price);

		System.assertEquals('Recurring Target Fee Total', lineItems1[3].name);
		System.assertEquals('6 x Recurring Fee', lineItems1[3].description);
		System.assertEquals(true, lineItems1[3].recurring);
		System.assertEquals(true, lineItems1[3].sm2m);
		System.assertEquals(0.68, lineItems1[3].price);

		System.assertEquals('Recurring Target Fee Total', lineItems1[4].name);
		System.assertEquals('3 x Recurring Fee', lineItems1[4].description);
		System.assertEquals(true, lineItems1[4].recurring);
		System.assertEquals(true, lineItems1[4].sm2m);
		System.assertEquals(12, lineItems1[4].price);
    }

	static testMethod void fiterOneTime()
	{
		createTestData();

		Order_Request__c orderRequest =
				[ SELECT Id
				  FROM Order_Request__c
				  LIMIT 1 ];

		Test.startTest();
		OrderRequestDetail orderRequestDetail =
			new OrderRequestDetail(orderRequest.Id).filterOneTime();
		List<ProductOrder> productOrders =
			orderRequestDetail.getProductOrders();
		//
		// Tests
		//
		Product_COnfiguration_Order__c[] pcos =
				[ SELECT Id
				  FROM Product_Configuration_Order__c ];
		Journal_Mapping__c oneTimeJournalMapping =
				[ SELECT Id
				  FROM Journal_Mapping__c
				  WHERE Name = 'M2M Hardware'
				  AND Country__c = 'Non-DE' ];
		Journal_Mapping__c recurringJournalMapping =
				[ SELECT Id
				  FROM Journal_Mapping__c
				  WHERE Name = 'Recuring Fee'
				  AND Country__c = 'Non-DE' ];

		//
		// Product orders
		//
		System.assertEquals(2, productOrders.size());
		System.assertEquals('SmartMeter', productOrders[0].name);
		System.assertEquals(50, productOrders[0].quantity);
		System.assertEquals('SIM & Tariff', productOrders[1].name);
		System.assertEquals(100, productOrders[1].quantity);
		//
		// Product configuration orders
		//
		System.assertEquals(1, productOrders[0].productConfigurations.size());
		System.assertEquals(1, productOrders[1].productConfigurations.size());

		ProductOrder.ProductConfiguration productConfiguration0 = productOrders[0].productConfigurations[0];
		System.assertEquals('MR_CarModem', productConfiguration0.name);
		System.assertEquals(24, productConfiguration0.contractTerm);
		System.assertEquals('Rate Payment', productConfiguration0.billingRateModel);
		System.assertEquals('Hardware', productConfiguration0.aisRecordType);
		System.assertEquals(pcos[0].Id, productConfiguration0.productConfigurationOrderId);
		System.assertEquals(oneTimeJournalMapping.Id, productConfiguration0.oneTimeJournalMappingId);
		System.assertEquals(recurringJournalMapping.Id, productConfiguration0.recurringJournalMappingId);

		ProductOrder.ProductConfiguration productConfiguration1 = productOrders[1].productConfigurations[0];
		System.assertEquals('SIM & Tariff', productConfiguration1.name);
		System.assertEquals(24, productConfiguration1.contractTerm);
		System.assertEquals('One Off', productConfiguration1.billingRateModel);
		System.assertEquals('SIM', productConfiguration1.aisRecordType);
		System.assertEquals(pcos[1].Id, productConfiguration1.productConfigurationOrderId);
		System.assertEquals(oneTimeJournalMapping.Id, productConfiguration1.oneTimeJournalMappingId);
		System.assertEquals(recurringJournalMapping.Id, productConfiguration1.recurringJournalMappingId);
		//
		// Line items
		//
		List<ProductOrder.LineItem> lineItems0 = productConfiguration0.lineItems;
		List<ProductOrder.LineItem> lineItems1 = productConfiguration1.lineItems;

		System.assertEquals(1, lineItems0.size());
		System.assertEquals(2, lineItems1.size());

		System.assertEquals('One-Time Target Fee Total', lineItems0[0].name);
		System.assertEquals('100 x One-Time Fee', lineItems0[0].description);
		System.assertEquals(false, lineItems0[0].recurring);
		System.assertEquals(false, lineItems0[0].sm2m);
		System.assertEquals(44500, lineItems0[0].price);

		System.assertEquals('SIM One-Time Target Fee Total', lineItems1[0].name);
		System.assertEquals('100 x SIM Card One-Time Fee', lineItems1[0].description);
		System.assertEquals(false, lineItems1[0].recurring);
		System.assertEquals(false, lineItems1[0].sm2m);
		System.assertEquals(150, lineItems1[0].price);

		System.assertEquals('Tariff One-Time Target Fee Total', lineItems1[1].name);
		System.assertEquals('100 x Tariff One-Time Fee', lineItems1[1].description);
		System.assertEquals(false, lineItems1[1].recurring);
		System.assertEquals(true, lineItems1[1].sm2m);
		System.assertEquals(3200, lineItems1[1].price);

		//
		// Child product orders
		//
		ProductOrder[] childProductOrders = productConfiguration1.productOrders;
		System.assertEquals(0, childProductOrders.size());
	}

	static testMethod void filterRecurring()
	{
		//
		// Create test data
		//
		createTestData();

		Order_Request__c orderRequest =
				[ SELECT Id
				  FROM Order_Request__c
				  LIMIT 1 ];

		Test.startTest();
		OrderRequestDetail orderRequestDetail =
			new OrderRequestDetail(orderRequest.Id).filterRecurring();
		List<ProductOrder> productOrders =
			orderRequestDetail.getProductOrders();
		//
		// Tests
		//
		Product_Configuration_Order__c[] pcos =
				[ SELECT Id
				  FROM Product_Configuration_Order__c ];
		Journal_Mapping__c oneTimeJournalMapping =
				[ SELECT Id
				  FROM Journal_Mapping__c
				  WHERE Name = 'M2M Hardware'
				  AND Country__c = 'Non-DE' ];
		Journal_Mapping__c recurringJournalMapping =
				[ SELECT Id
				  FROM Journal_Mapping__c
				  WHERE Name = 'Recuring Fee'
				  AND Country__c = 'Non-DE' ];

		//
		// Product orders
		//
		System.assertEquals(2, productOrders.size());
		System.assertEquals('SmartMeter', productOrders[0].name);
		System.assertEquals(50, productOrders[0].quantity);
		System.assertEquals('SIM & Tariff', productOrders[1].name);
		System.assertEquals(100, productOrders[1].quantity);
		//
		// Product configuration orders
		//
		System.assertEquals(1, productOrders[0].productConfigurations.size());

		ProductOrder.ProductConfiguration productConfiguration0 = productOrders[0].productConfigurations[0];
		System.assertEquals('MR_CarModem', productConfiguration0.name);
		System.assertEquals(24, productConfiguration0.contractTerm);
		System.assertEquals('Rate Payment', productConfiguration0.billingRateModel);
		System.assertEquals('Hardware', productConfiguration0.aisRecordType);
		System.assertEquals(pcos[0].Id, productConfiguration0.productConfigurationOrderId);
		System.assertEquals(oneTimeJournalMapping.Id, productConfiguration0.oneTimeJournalMappingId);
		System.assertEquals(recurringJournalMapping.Id, productConfiguration0.recurringJournalMappingId);

		ProductOrder.ProductConfiguration productConfiguration1 = productOrders[1].productConfigurations[0];
		System.assertEquals('SIM & Tariff', productConfiguration1.name);
		System.assertEquals(24, productConfiguration1.contractTerm);
		System.assertEquals('One Off', productConfiguration1.billingRateModel);
		System.assertEquals('SIM', productConfiguration1.aisRecordType);
		System.assertEquals(pcos[1].Id, productConfiguration1.productConfigurationOrderId);
		System.assertEquals(oneTimeJournalMapping.Id, productConfiguration1.oneTimeJournalMappingId);
		System.assertEquals(recurringJournalMapping.Id, productConfiguration1.recurringJournalMappingId);
		//
		// Line items
		//
		List<ProductOrder.LineItem> lineItems0 = productConfiguration0.lineItems;
		List<ProductOrder.LineItem> lineItems1 = productConfiguration1.lineItems;

		System.assertEquals(1, lineItems0.size());
		System.assertEquals(1, lineItems1.size());

		System.assertEquals('Recurring Target Fee Total', lineItems0[0].name);
		System.assertEquals('100 x Recurring Fee', lineItems0[0].description);
		System.assertEquals(true, lineItems0[0].recurring);
		System.assertEquals(500, lineItems0[0].price);

		System.assertEquals('Tariff Recurring Target Fee Total', lineItems1[0].name);
		System.assertEquals('100 x Tariff Recurring Fee', lineItems1[0].description);
		System.assertEquals(true, lineItems1[0].recurring);
		System.assertEquals(true, lineItems1[0].sm2m);
		System.assertEquals(1200, lineItems1[0].price);

		//
		// Child product orders
		//
		ProductOrder[] childProductOrders = productConfiguration1.productOrders;
		System.assertEquals(2, childProductOrders.size());

		//
		// Child product order quantities
		//
		System.assertEquals(2, childProductOrders[0].quantity);
		System.assertEquals(3, childProductOrders[1].quantity);

		//
		// Child product configuration sizes
		//
		System.assertEquals(1, childProductOrders[0].productConfigurations.size());
		System.assertEquals(1, childProductOrders[1].productConfigurations.size());

		//
		// Child line item sizes
		//
		ProductOrder.ProductConfiguration childConfiguration0 = childProductOrders[0].productConfigurations[0];
		ProductOrder.ProductConfiguration childConfiguration1 =	childProductOrders[1].productConfigurations[0];

		ProductOrder.LineItem[] childLineItems0 = childConfiguration0.lineItems;
		ProductOrder.LineItem[] childLineItems1 = childConfiguration1.lineItems;
		System.assertEquals(1, childLineItems0.size());
		System.assertEquals(1, childLineItems1.size());


		System.assertEquals('Recurring Target Fee Total', childLineItems0[0].name);
		System.assertEquals('6 x Recurring Fee', childLineItems0[0].description);
		System.assertEquals(true, childLineItems0[0].recurring);
		System.assertEquals(true, childLineItems0[0].sm2m);
		System.assertEquals(0.68, childLineItems0[0].price);

		System.assertEquals('Recurring Target Fee Total', childLineItems1[0].name);
		System.assertEquals('3 x Recurring Fee', childLineItems1[0].description);
		System.assertEquals(true, childLineItems1[0].recurring);
		System.assertEquals(true, childLineItems1[0].sm2m);
		System.assertEquals(12, childLineItems1[0].price);
	}
}