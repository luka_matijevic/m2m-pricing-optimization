/**
 * Description: Controller class for ManagerConsole Page. Holds logic to populate all views and chart data.
 * 1.0 | Mahaboob Basha | 19-May-2015 | initial Version
 * 
 */
public with Sharing class ManagerConsoleController {
	
	public List<Case> openCaseList{get; set;}
	public List<PieWedgeData> pieData{get; set;}
	public List<PieWedgeData> pieDataSLA24{get; set;}
	public List<PieWedgeData> pieDataCurrentWeek{get; set;}
	public String selectedPriority{get;set;}
	public String chartColor{get;set;}
	public String allOpenCaseLabel{get;set;}
	public List<SelectOption> selectList{get;set;}
	public String selectedOption{get;set;}
	private List<Case> caseSLAList{get; set;}
	
	/**
	 * Constructor call
	 */
	public ManagerConsoleController(ApexPages.StandardController cntrl){
		
		initiatePage();
		
	}
	
	/**
	 * Method used for fetching the Case data for different views on ManagerView Page 
	 **/
	public void initiatePage(){
		try{
			Decimal highCount=0.0;
			Decimal mediumCount=0.0;
			Decimal lowCount=0.0;
			//SLA 24h count
			Decimal highCountSLA24=0.0;
			Decimal mediumCountSLA24=0.0;
			Decimal lowCountSLA24=0.0;
			//SLA 7 days count
			Decimal highCount7Days=0.0;
			Decimal mediumCount7Days=0.0;
			Decimal lowCount7Days=0.0;
			
			Decimal totalCount=0.0;
			Decimal totalCountSLA24=0.0;
			Decimal totalCount7Days=0.0;
			allOpenCaseLabel='All Open Tickets';
			openCaseList = new List<Case>();
			caseSLAList = new List<Case>();
			pieData = new List<PieWedgeData>(); 
			pieDataSLA24 = new List<PieWedgeData>();
			pieDataCurrentWeek = new List<PieWedgeData>();
			selectedOption = '--All--';
            Set<Id> firstLevelOwners = new Set<Id>();
            Group firstLevelQueue = [select id, name from Group Where developerName = 'DSC_Queue' and Type = 'Queue' LIMIT 1];
            if(firstLevelQueue != null){
                firstLevelOwners.add(firstLevelQueue.Id);
               	List<GroupMember> firstLevelUsers = [select groupid, userorgroupid from GroupMember where groupId = :firstLevelQueue.Id]; 
                for(GroupMember gm : firstLevelUsers){
                	firstLevelOwners.add(gm.userorgroupid);    
                }
            }
            
			Date dt = System.today().toStartOfWeek();
			for(Case c : [Select Id, CaseNumber, Status, OwnerId, Account.Name, SLA_Breach_Status__c, SLA_breached_checkbox__c, SLA_Time__c, Type, Origin, Priority__c, CreatedDate from Case where Status!='Rejected' AND CreatedDate >= :dt AND ownerId IN :firstLevelOwners ORDER BY CreatedDate DESC NULLS LAST LIMIT: 1000]){
				caseSLAList.add(c);
				// data of Open cases created in the past 24h 
				if(c.CreatedDate>(System.now()-1)){
					
					if(null!=c.Priority__c && c.Priority__c.equalsIgnoreCase('High') && c.SLA_breached_checkbox__c){
						highCountSLA24++;
						totalCountSLA24++;
					}
					if(null!=c.Priority__c && (c.Priority__c.equalsIgnoreCase('Medium') || c.Priority__c.equalsIgnoreCase('Low')) && c.SLA_breached_checkbox__c){
						mediumCountSLA24++;
						totalCountSLA24++;
					}
					if(!c.SLA_breached_checkbox__c){
						lowCountSLA24++;
						totalCountSLA24++;
					}
				}
				
				// data of Open cases created in the past 7 days
				if(null!=c.Priority__c && c.Priority__c.equalsIgnoreCase('High') && c.SLA_breached_checkbox__c){
					highCount7Days++;
					totalCount7Days++;
				}
				if(null!=c.Priority__c && (c.Priority__c.equalsIgnoreCase('Low') || c.Priority__c.equalsIgnoreCase('Medium')) && c.SLA_breached_checkbox__c){
					mediumCount7Days++;
					totalCount7Days++;
				}
				if(!c.SLA_breached_checkbox__c){
					lowCount7Days++;
					totalCount7Days++;
				}                                  
			}

			for(Case c : [Select Id, CaseNumber, Status, OwnerId, Account.Name, SLA_Breach_Status__c, SLA_breached_checkbox__c, SLA_Time__c, Type, Origin, Priority__c, CreatedDate from Case where (Status='Open' OR Status='In Progress') AND ownerId IN :firstLevelOwners ORDER BY CreatedDate DESC NULLS LAST LIMIT: 1000]){
				openCaseList.add(c);

				if(null!=c.Priority__c && c.Priority__c.equalsIgnoreCase('High')){
					highCount++;
				}
				if(null!=c.Priority__c && c.Priority__c.equalsIgnoreCase('Medium')){
					mediumCount++;
				}
				if(null!=c.Priority__c && c.Priority__c.equalsIgnoreCase('Low')){
					lowCount++;
				}
			}
			
			totalCount = openCaseList.size();
			//chartColor='#FF4500,#FFFF30,#32CD32';   
			chartColor='#D00000,#FFFF30,#32CD32';     
			
			// Wrapper construction for Chart
			if(totalCount>0.0){
				pieData.add(new PieWedgeData('Prio 1', ((highCount/totalCount)*100).setScale(2)));
				pieData.add(new PieWedgeData('Prio 2', ((mediumCount/totalCount)*100).setScale(2)));
				pieData.add(new PieWedgeData('Prio 3', ((lowCount/totalCount)*100).setScale(2)));                
			}
			// data of Open cases created in the past 24h 
			if(totalCountSLA24>0.0){
				pieDataSLA24.add(new PieWedgeData('24h-P1 breached', ((highCountSLA24/totalCountSLA24)*100).setScale(2)));
				pieDataSLA24.add(new PieWedgeData('24h-P2 or P3 breached', ((mediumCountSLA24/totalCountSLA24)*100).setScale(2)));
				pieDataSLA24.add(new PieWedgeData('24h-No SLA breached', ((lowCountSLA24/totalCountSLA24)*100).setScale(2)));
			}
			// data of cases created in the past 7 days
			if(totalCount7Days>0.0){
				pieDataCurrentWeek.add(new PieWedgeData('CW-P1 breached', ((highCount7Days/totalCount7Days)*100).setScale(2)));
				pieDataCurrentWeek.add(new PieWedgeData('CW-P2 or P3 breached', ((mediumCount7Days/totalCount7Days)*100).setScale(2)));
				pieDataCurrentWeek.add(new PieWedgeData('CW-No SLA breached', ((lowCount7Days/totalCount7Days)*100).setScale(2)));  
			}

			selectList = new List<SelectOption>();
			selectList.add(new SelectOption('', '--All--'));
			selectList.add(new SelectOption('Priority 1', 'Priority 1'));
			selectList.add(new SelectOption('Priority 2', 'Priority 2'));
			selectList.add(new SelectOption('Priority 3', 'Priority 3'));
			selectList.add(new SelectOption('Unassigned', 'Unassigned'));
			selectList.add(new SelectOption('SLA breach 24h', 'SLA breach 24h'));
			selectList.add(new SelectOption('SLA breach current week', 'SLA breach current week'));
			selectList.sort();
			
		}catch(Exception e){system.debug('Exception in AgentViewController:' + e.getMessage()); ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Operation Failed: Please contact your system administrator!', ''));
		}
	}
	
	/**
	 * Wrapper class for Pie Chart.
	 */
	public class PieWedgeData {

		public String name { get; set; }
		public Decimal data { get; set; }

		public PieWedgeData(String name, Decimal data) {
			this.name = name;
			this.data = data;
		}
	}
	

	/**
	 * Method to apply filter & update All Open Case details based on Case priority/ SLA Breach selection
	 */
	public void applyFilters(){
		// storing selection to reset the selectList value on click of refresh to "--All--"
		String selectedOptTemp = selectedOption;
		initiatePage();
		selectedOption = selectedOptTemp;
		List<Case> tempOpenCaseList = new List<Case>();
		// data of Open cases created with P1,P2 P3
		for(Case c : openCaseList){
			if((selectedOption=='Priority 1' || selectedOption=='Prio 1') && c.Priority__c=='High'){
				tempOpenCaseList.add(c);
			} else if((selectedOption=='Priority 2' || selectedOption=='Prio 2') && c.Priority__c=='Medium'){
				tempOpenCaseList.add(c);
			} else if((selectedOption=='Priority 3' || selectedOption=='Prio 3') && c.Priority__c=='Low'){
				tempOpenCaseList.add(c);
			} else if(selectedOption=='Unassigned' && c.Status=='In Progress'){
				tempOpenCaseList.add(c);
			} else if(selectedOption==null || selectedOption=='' || selectedOption=='--All--'){
				tempOpenCaseList.add(c);
			}
		}
		
		// data of Open cases created in the past 24h OR past 7 days
		if(tempOpenCaseList.isEmpty()){
			allOpenCaseLabel = 'All Tickets';
			for(Case c: caseSLAList){
				if(selectedOption=='24h-P1 breached' && (c.CreatedDate>(System.now()-1)) && c.Priority__c.equalsIgnoreCase('High') && c.SLA_breached_checkbox__c){
					tempOpenCaseList.add(c);
				} else if(selectedOption=='24h-P2 or P3 breached' && (c.CreatedDate>(System.now()-1)) && (c.Priority__c.equalsIgnoreCase('Medium') || c.Priority__c.equalsIgnoreCase('Low')) && c.SLA_breached_checkbox__c){
					tempOpenCaseList.add(c);
				} else if(selectedOption=='24h-No SLA breached' && (c.CreatedDate>(System.now()-1)) && !c.SLA_breached_checkbox__c){
					tempOpenCaseList.add(c);
					//((c.CreatedDate.date()).daysBetween(Datetime.now().date())<=7)
				} else if(selectedOption=='CW-P1 breached' && c.Priority__c.equalsIgnoreCase('High') && c.SLA_breached_checkbox__c){
					tempOpenCaseList.add(c);
				} else if(selectedOption=='CW-P2 or P3 breached' && (c.Priority__c.equalsIgnoreCase('Medium') || c.Priority__c.equalsIgnoreCase('Low')) && c.SLA_breached_checkbox__c){
					tempOpenCaseList.add(c);
				} else if(selectedOption=='CW-No SLA breached' && !c.SLA_breached_checkbox__c){
					tempOpenCaseList.add(c);
				} else if(selectedOption=='SLA breach 24h' && (c.CreatedDate>(System.now()-1))){
					tempOpenCaseList.add(c);
				} else if(selectedOption=='SLA breach current week'){
					tempOpenCaseList.add(c);
				}
			}
		}
		allOpenCaseLabel= allOpenCaseLabel+ ((selectedOption!=null && selectedOption!='')?': ' 
		+ (selectedOption.contains('Prio ')? selectedOption.replace('Prio ', 'Priority ') : selectedOption) :'');
		openCaseList.clear();
		openCaseList.addAll(tempOpenCaseList);  
		tempOpenCaseList.clear();
	}
}