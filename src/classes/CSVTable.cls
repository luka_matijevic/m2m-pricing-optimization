//------------------------------------------------------------------------------
// Class for generating CSV table
//------------------------------------------------------------------------------
public with sharing class CSVTable implements ITable
{
	//--------------------------------------------------------------------------
	// Private members
	//--------------------------------------------------------------------------
	private List<IRow> rows;
	private String separator;
	private String newLine;

	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	public CSVTable(List<IRow> rows, String separator, String newLine)
	{
		this.rows = rows;
		this.separator = separator;
		this.newLine = newLine;
	}

	//--------------------------------------------------------------------------
	// Generates table in CSV string
	//--------------------------------------------------------------------------
	public String generate()
	{
		String returnValue = AttachmentUtils.csvBOM();
		for (IRow row : rows)
		{
			returnValue += generateRow(row);
		}
		return returnValue;
	}

	//--------------------------------------------------------------------------
	// Generate one row
	//--------------------------------------------------------------------------
	private String generateRow(IRow row)
	{
		String returnValue = '';
		List<String> cells = row.getCells();
		Iterator<String> it = cells.iterator();
		while (it.hasNext())
		{
			String cell = it.next();
			returnValue += generateCell(cell);
			//
			// If not last element put separator
			//
			if (it.hasNext())
			{
				returnValue += separator;
			}
			//
			// Else put newline
			//
			else
			{
				returnValue += newLine;
			}
		}
		return returnValue;
	}

	//--------------------------------------------------------------------------
	// Generate one cell
	//--------------------------------------------------------------------------
	private String generateCell(String cell)
	{
		String returnValue;
		//
		// Threat null cell like empty
		//
		if (cell == null)
		{
			cell = '';
		}
		//
		// Check if cell contains separator, new line or quote
		//
		if (cell.indexOf(separator) >= 0
			|| cell.indexOf(Parser.LF) >= 0
			|| cell.indexOf(Parser.CR) >= 0
			|| cell.indexOf(Parser.DQUOTE) >= 0)
		{
			//
			// Put string in quotes
			//
			returnValue = Parser.DQUOTE
					+ cell.replaceAll(Parser.DQUOTE, Parser.DQUOTEDQUOTE)
					+ Parser.DQUOTE;
		}
		else
		{
			//
			// Leave cell as it is
			//
			returnValue = cell;
		}
		return returnValue;
	}
}