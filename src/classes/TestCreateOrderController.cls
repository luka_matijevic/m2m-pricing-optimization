@isTest
private class TestCreateOrderController
{
	//--------------------------------------------------------------------------
	// Test create order
	//--------------------------------------------------------------------------
    static testMethod void createOrder()
    {
    	//
    	// Create test data
    	//
    	No_Trigger__c notriggers = new No_Trigger__c();
    	notriggers.Flag__c = true;
    	insert notriggers;

		TestHelper.createOrderRequest();
		TestHelper.createJournalMappings();

		notriggers.Flag__c = false;
		update notriggers;

		Order_Request__c orderRequest = [ SELECT Id, Billing_Account__c, Billing_Account__r.Trial__c, Billing_Account__r.Account_SM2M_ID__c, Billing_Account__r.SM2M_ID__c FROM Order_Request__c LIMIT 1];

		Test.startTest();
		CreateOrderController createOrderController =
				new CreateOrderController(new ApexPages.StandardController(orderRequest));
		PageReference prOrder = createOrderController.create();
		PageReference prOrderRequest = createOrderController.gotoOrderRequest();
		Test.stopTest();

		//
		// Test
		//
		System.assertNotEquals(null, prOrder);
		System.assertNotEquals(null, prOrderRequest);
    }

    //--------------------------------------------------------------------------
    // Test create order without journal mappings
    //--------------------------------------------------------------------------
    static testMethod void createOrderWithoutJournalMappings()
    {
    	//
    	// Create test data
    	//
    	No_Trigger__c notriggers = new No_Trigger__c();
    	notriggers.Flag__c = true;
    	insert notriggers;

    	TestHelper.createOrderRequest();

    	notriggers.Flag__c = false;
    	update notriggers;

    	Order_Request__c orderRequest = [ SELECT Id, Billing_Account__c, Billing_Account__r.Trial__c, Billing_Account__r.Account_SM2M_ID__c, Billing_Account__r.SM2M_ID__c FROM Order_Request__c LIMIT 1 ];

    	Test.startTest();
    	CreateOrderController createOrderController =
    			new CreateOrderController(new ApexPages.StandardController(orderRequest));
    	PageReference prOrder = createOrderController.create();
    	Test.stopTest();

    	//
    	// Test
    	//
    	System.assertEquals(null, prOrder);
    	System.assertNotEquals(null, createOrderController.errorMessage);
    }

    //--------------------------------------------------------------------------
    // Test create order twice
    //--------------------------------------------------------------------------
    static testMethod void craeteOrderTwice()
    {
    	//
    	// Create test data
    	//
    	No_Trigger__c notriggers = new No_Trigger__c();
    	notriggers.Flag__c = true;
    	insert notriggers;

    	TestHelper.createOrderRequest();
    	TestHelper.createJournalMappings();

    	notriggers.Flag__c = false;
    	update notriggers;

    	Order_Request__c orderRequest = [ SELECT Id, Billing_Account__c, Billing_Account__r.Trial__c, Billing_Account__r.Account_SM2M_ID__c, Billing_Account__r.SM2M_ID__c FROM Order_Request__c LIMIT 1 ];

    	Test.startTest();
    	CreateOrderController createOrderController =
    			new CreateOrderController(new ApexPages.StandardController(orderRequest));
    	PageReference prOrder = createOrderController.create();
    	prOrder = createOrderController.create();
    	Test.stopTest();

    	//
    	// Test
    	//
    	System.assertEquals(null, prOrder);
    	System.assertNotEquals(null, createOrderController.errorMessage);
    }
}