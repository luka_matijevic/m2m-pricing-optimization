/**
  class used for insertion of the SIM assets baased on the info stored in the second upload file

*/
global class SecondUploadFileBatchParser implements Database.Batchable<Stock__c>, Database.Stateful  {

	//id od the article info set that represents SIM card model as a base for generated sim assets
	Id articleInfoSetId {get; set;}
	//id of the order request that triggered the creation of sim assets
	Id parentId {get; set;}
	//parsed second upload file [0] iccid [1] msisdn
	List<List<String>> simData {get; set;}
	//class used for tracking of the results of batch processing
	global DMLResults myResults;
	//Name of the article info set to store at the asset record created
	String aisName {get; set;}

	Id idAccount {get; set;}

	public SecondUploadFileBatchParser(Id articleInfoSetId, List<List<String>> simData, Id parentId, Id idAccount)
	{
		this.articleInfoSetId = articleInfoSetId;
		this.simData = simData;
		this.parentId = parentId;
		this.idAccount = idAccount;
	}
	/*public SecondUploadFileBatchParser(Id articleInfoSetId, List<List<String>> simData)
	{
		this.articleInfoSetId = articleInfoSetId;
		this.simData = simData;
	}*/

	/**
	throws exception if article info set id is not of sim type article
	*/
	global Iterable<Stock__c> start(Database.BatchableContext info){

		if(articleInfoSetId == null || simData == null)
		{
			throw new M2MBatchException('No Article is set Assets can not be generated!');
		}
		Article_Info_Set__c article;
		try
		{
		 	article = [Select Id, Name From Article_Info_Set__c WHERE ARTICLE_TYPE__C = 'SIM' and Id = :articleInfoSetId];
		 	aisName = article.Name;
		}
		catch(DMLException dmle)
		{
			throw new M2MBatchException('No article of type SIM exists with Id: ' + articleInfoSetId, dmle);
		}
		myResults = new DMLResults();

		return new SecondUploadFileIterator(articleInfoSetId, simData);
    }

   	global void execute(Database.BatchableContext info, List<Stock__c> scope)
   	{

   		for(Stock__c asset : scope)
   		{
   			asset.SRF__c = parentId;
   			asset.Account__c = idAccount;
   		}
   		myResults.add(Database.insert(scope,false),scope);
   	}

   	global void finish(Database.BatchableContext info)
   	{
   		myResults.batchOnFinish(info.getJobId());
   	}

   	public class M2MBatchException extends Exception{}
}