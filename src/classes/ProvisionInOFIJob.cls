global class ProvisionInOFIJob implements Database.Batchable<SObject>, Database.Stateful
{
	//--------------------------------------------------------------------------
	// Members
	//--------------------------------------------------------------------------
	List<IRow> rows;
	Set<Id> billingAccounts;
	ISendEmail sendEmail;

	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	global ProvisionInOFIJob(ISendEmail sendEmail)
	{
		this.sendEmail = sendEmail;
	}

	//--------------------------------------------------------------------------
	// Start of batch apex
	//--------------------------------------------------------------------------
	global Database.QueryLocator start(Database.BatchableContext bc)
	{
		rows = new List<IRow>();
		rows.add(new StringRow(new List<String>
			{
				'ALTKN',
				'KUNNR',
				'NAME',
				'STRAS',
				'ORT01',
				'PSTLZ',
				'LAND1',
				'SPRAS',
				'TELF1',
				'TELF2',
				'TELFX',
				'FAX_NUMBER',
				'SMTP_ADDR',
				'STCEG',
				'BANKS',
				'BANKL',
				'BANKN',
				'BANKA',
				'ZTERM',
				'ZWELS',
				'MANSP',
				'VTRIEB',
				'UPDTYP'
			}));
		billingAccounts = new Set<Id>();

		String query = 'SELECT Name, Account_Number__c, Account_Street__c, Account_City__c, '
  			  + 'Account_Postcode__c, Account_Country__c, Billing_Language__c, '
  			  + 'Payer__r.Phone, Payer__r.Phone__c, Payer__r.Fax, '
  			  + 'Payer__r.Company_Email__c, Payer__r.Value_Added_Tax_ID__c, '
  			  + 'Bank_Identifier__c, BIC__c, Bank_Name__c, Bank_Address__c, '
  			  + 'Payment_Term__c, Payment_Method__c, Payer__r.Sales_Representative__c, '
  			  + 'OFI_Provisioning_Status__c, '
    		  + 'Override_Account_Address__c, Billing_Street__c, Billing_City__c, '
    		  + 'Billing_Postcode__c, Billing_Country__c '
  	          + 'FROM Billing_Account__c '
  	          + 'WHERE OFI_Provisioning_Status__c = \'' +  Constants.PENDING + '\'';
		return Database.getQueryLocator(query);
	}

	//--------------------------------------------------------------------------
	// Execute of batch apex
	//--------------------------------------------------------------------------
	global void execute(Database.BatchableContext bc, List<SObject> scope)
	{
		for (SObject obj : scope)
		{
			//
			// Add rows and billing account ids
			//
			Billing_Account__c billingAccount = (Billing_Account__c)obj;
			billingAccounts.add(billingAccount.Id);
			rows.add(new CustomerRow(billingAccount));
			billingAccount.OFI_Provisioning_Status__c = Constants.IN_PROGRESS;
		}
		update scope;
	}

	//--------------------------------------------------------------------------
	// Finish of batch apex
	//--------------------------------------------------------------------------
	global void finish(Database.BatchableContext bc)
	{
		if (!billingAccounts.isEmpty())
		{
			CSVTable table = new CSVTable(rows, ',', '\r\n');
			String attachment = table.generate();
			String attachmentName = 'O2_CUSTOMER_EXCEL_' + DateTime.now().format('yyyyMMdd') + '.csv';
			//
			// Send email to OFI
			//
			sendEmail.setAttachment(attachmentName, attachment);
			sendEmail.send();
			//
			// Create attachments to billing accounts with CSV
			//
			List<Attachment> atts = new List<Attachment>();
			for (Id billingAccountId : billingAccounts)
			{
				Attachment att = new Attachment();
				att.Name = attachmentName;
				att.Body = Blob.valueOf(attachment);
				att.ParentId = billingAccountId;
				atts.add(att);
			}
			insert atts;
		}
	}
}