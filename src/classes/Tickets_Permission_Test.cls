@isTest
public class Tickets_Permission_Test {

    static testMethod void Tickets_Permission_To_Service2ndLevel(){
        
        list<Profile> profiles = new list<profile>([select id, name from profile where name = 'M2M Standard User']);
        User u;
        if(!profiles.isEmpty()){
            u = new User(Alias = 'User', Email='m2muser@m2m.com',
                      EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',
                      LocaleSidKey='en_US', ProfileId = profiles[0].Id,
                      TimeZoneSidKey='America/Los_Angeles', UserName='m2muser@m2m.com');
            insert u;
        }
        
        if(u.id != null){
           list<account> accLst = new list<account>();
            for(integer i= 0; i< 10; i++){
                Account acc = new account(Type = 'Customer',Value_Added_Tax_ID__c='1234',VAT__c=10,Language__c= 'German',Billing_Cycle__c='1',VO_Number__c='1111');
                acc.name = 'Test Account'+i;
                string email = 'Test' + i + '@gmail.com';
                acc.Company_Email__c=email;
                acc.SPIN_ID__c='12122'+i; 
                accLst.add(acc);
            }
            insert accLst;
            
            list<Opportunity> oppLst = new list<Opportunity>();
            list<case> caseLst = new list<case>();
            for(Account acc :accLst){
                if(acc.id != null){
                    oppLst.add(new Opportunity(name='Test Opp1',stagename='New',Product_Family__c ='Mobility',accountId= acc.id,Type='New Business',CloseDate = date.valueOf(system.today()),Estimated_Success_Probability__c = 90,ownerId=u.id, Description = 'Test description 20 characters'));
                    caseLst.add(new case(status='Open',type='Standard',Category__c='Service',Origin='Phone',Subject='Subject',Description='DescriptionDescriptionDescription',accountId = acc.id));
                }
            }
            if(accLst.size() > 0){
                oppLst.add(new Opportunity(name='Test Opp1',stagename='New',Product_Family__c ='Mobility',accountId= accLst[0].id,Type='New Business',CloseDate = date.valueOf(system.today()),Estimated_Success_Probability__c = 90,ownerId=u.id, Description = 'Test description 20 characters'));
            }
            insert oppLst; 
            insert caseLst;
            
            test.startTest();
                Database.executeBatch(new Tickets_Permission_To_Service2ndLevel());
            Test.stopTest();
            
            
        }
        
        
    }

}