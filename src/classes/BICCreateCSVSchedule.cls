/**
	* BICCreateCSVSchedule - Scheduler for BICCreateExportCSVJob
	* @author: Zoran Zunko
	* @version: 1.0
*/

global with sharing class BICCreateCSVSchedule implements Schedulable
{
	//--------------------------------------------------------------------------
	// Scheduled job
	//--------------------------------------------------------------------------
	global void execute (SchedulableContext sc)
	{
		//
		// Run apex job
		//
		Database.executeBatch(new BICCreateExportCSVJob());
	}
}