@isTest
private class PricingServicesControllerTest {
 
 private static testMethod void testAll() { 
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers;
        
        Account account = new Account(
         Name = 'Test3',
            Type = 'Business'
         );
         
        insert account;
        
        Contact Con = new Contact(firstName= 'Test',lastName = 'ConTest',Salutation='Mr.',accountID = account.id,email = 'testUSer@gmail.com');
        insert con;
        Opportunity opp = new Opportunity(
   Name = 'Test Opportunity',
   StageName = 'Negotiation',
   CloseDate = Date.newInstance(2014,1,1),
   AccountId = account.Id,
   Description = 'Test description 20 characters'
  );
  
  insert opp;
  
  
  
  OpportunityContactRole OppConRole = new OpportunityContactRole(ContactId = con.id,IsPrimary = true,OpportunityId = opp.id,Role = 'Decision Maker');
  insert OppConRole;
  
  cscfga__Eligibility_Rule__c rule = new cscfga__Eligibility_Rule__c(name = 'Rule',cscfga__Object_Name__c = 'Opportunity');
  insert rule;
  
  cscfga__Configuration_Offer__c ConfigOffer = new cscfga__Configuration_Offer__c(name ='Offer',cscfga__Eligibility_Rule__c = rule.id);
  insert ConfigOffer;
  
  cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(
   cscfga__Synchronised_with_Opportunity__c = false,
   cscfga__Opportunity__c = opp.Id
  );
  
  insert productBundle;
  
  
  
  Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(productBundle.id);
        req1.setSubmitterId(userInfo.getUserId());
        req1.setProcessDefinitionNameOrId('Product_Bundle_Approval_New_v4');
        req1.setSkipEntryCriteria(true);
        Approval.ProcessResult result = Approval.process(req1);
        
        cscfga__Product_Definition__c productDefinition = new cscfga__Product_Definition__c(
   Name = 'Test Name',
   cscfga__Description__c = 'Test description'
  );
  
  insert productDefinition;
  
  cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c(
   cscfga__Product_Bundle__c = productBundle.Id,
   cscfga__Product_Definition__c = productDefinition.Id,
   cscfga__Configuration_Offer__c = ConfigOffer.id
  );
  
  
  insert productConfiguration;

        system.debug('productConfiguration>>>'+productConfiguration.id);
        system.debug('productConfiguration>>>'+productConfiguration.cscfga__Configuration_Offer__c);

        list<cscfga__Attribute__c> attr = new list<cscfga__Attribute__c>();
        attr.add(new cscfga__Attribute__c(name='One-Time Base Fee',cscfga__Value__c = '10',cscfga__Product_Configuration__c =productConfiguration.id ));
        attr.add(new cscfga__Attribute__c(name='Recurring Base Fee',cscfga__Value__c = '10',cscfga__Product_Configuration__c =productConfiguration.id));
        insert attr;
        
        
        Activation_plan__c ac = new Activation_plan__c(Product_Bundle__c=productBundle.id);
        insert ac;

        notriggers.Flag__c = false;
        update notriggers;
        
         Test.startTest();
            PageReference pageRefr = Page.PricingServicesPage;
            pageRefr.getParameters().put('oppId',opp.Id);
            pageRefr.getParameters().put('Id',productBundle.Id);
            Test.setCurrentPageReference(pageRefr); 
            PricingServicesController ctrl = new PricingServicesController (new ApexPages.StandardController(new cscfga__Product_Bundle__c()));
            system.assert(ctrl!=null);
            ctrl.servConfValues = new Services_Config_Value__c();
            //ctrl.fillbaseFees(10,10,ConfigOffer.id ,false);
            //ctrl.fillbaseFees(10,10,ConfigOffer.id ,true);
            ctrl.init();
            
            ctrl.quicksave();
            ctrl.selectedhardwareOffer = ConfigOffer.id;
            //ctrl.OfferChanged();
            boolean OppFCStats = ctrl.getOppFCStatus();
            pageRefr.getParameters().put('msg','msg');
            pageRefr.getParameters().put('severity','info');
            ctrl.init();
            ctrl.AcceptVersion();
            ctrl.approveReject();
            ctrl.submitReject();
            ctrl.submitNewPrice();
            //ctrl.OfferChanged();
            /*
            ctrl.RateCardTargetPriceChanged();
            ctrl.createSuppOption();
            ctrl.removeCreatedSuppOption();
            ctrl.saveCreatedSuppOption();
            ctrl.returnBusinessComments();
            ctrl.addNewComment();
            ctrl.deleteComment();
            ctrl.saveBillingApprovalStatus();
            ctrl.saveSLAApprovalStatus();
            List<SelectOption> x = ctrl.getContractTermItems();
            List<SelectOption> y = ctrl.gethardwareList();
            List<SelectOption> z = ctrl.getserviceList();
            ctrl.Submit();*/
            //ctrl.recallApprovalRequest();
            //ctrl.synchronizeBundle();
            
            ctrl.addNewService();
            ctrl.addNewHardware();
            //ctrl.changeServiceDropdown();
            //ctrl.changeHardwareDropdown();
            ctrl.removeService();
            ctrl.removeHardware();
            ctrl.fillServiceListHolder();
            ctrl.fillHardwareListHolder();
            ctrl.saveBatchable();
         Test.stopTest();    
        
 }
}