/**********************************
 * 
 * @@Description : Below class implements both the batch and schedule apex, It will run once in a week and deletes
 *                 csam_t1__Incoming_Message__c records, whose created date older than two months
 * 
 * 
 * *******************************/
global class DeleteIncominngMessagesJob implements Database.Batchable<sObject>,Schedulable  {
    
    
    global void execute(SchedulableContext sc) {
        DeleteIncominngMessagesJob obj = new DeleteIncominngMessagesJob(); 
        database.executebatch(obj);
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        try{
            Date twoMonthsOld = Date.valueOf(system.today()).addMonths(-2);
            string qry = 'select id from csam_t1__Incoming_Message__c where createdDate < :twoMonthsOld';
            if(test.isRunningTest()){
                qry = 'select id from csam_t1__Incoming_Message__c';
            }
            
            return database.getQueryLocator(qry);
        }catch(exception ex){
            system.debug('Exception in DeleteIncominngMessagesJob start method '+ex.getMessage());
            return null;
        }
    }
    
    global void execute(Database.BatchableContext bc, list<csam_t1__Incoming_Message__c> msgs){
        try{
            if(msgs.size() > 0){
                delete msgs;
            }
        }catch(exception ex){
            system.debug('Exception while deleting csam_t1__Incoming_Message__c in DeleteIncominngMessagesJob '+ex.getMessage());
        }
    } 
    
    global void finish(Database.BatchableContext bc){
        
    }
}