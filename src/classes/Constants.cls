global class Constants {
	global static final String OPPORTUNITY_TYPE_ICAP = 'IoT Connect Advanced';
	global static final String OPPORTUNITY_TYPE_COMMERCIAL_LOCAL = 'Commercial Local';
	global static final String OPPORTUNITY_TYPE_COMMERCIAL_GLOBAL = 'Commercial Global';
	global static final String PRODUCT_TYPE_CONNECTIVITY = 'Connectivity';
    global static final String PRODUCT_TYPE_SERVICE = 'Support & Service';
    global static final String PRODUCT_TYPE_SIM = 'SIM Card';
    global static final String PRODUCT_TYPE_HARDWARE = 'Hardware';
    global static final String PRODUCT_TYPE_FASTLANE = 'Fast Lane Opportunity';
    global static final String ATTRIBUTE_NAME_TARIFF = 'Tariff';
    global static final String ATTRIBUTE_NAME_CLONED_TARIFF = 'Cloned Tariff';
	global static final String ATTRIBUTE_NAME_ARTICLE_INFO_SET = 'Article Info Set';
	global static final String ATTRIBUTE_NAME_QUANTITY = 'Quantity';
    global static final String TARIFF_STATUS_APPROVED = 'Approved';
    global static final String TARIFF_STATUS_DRAFT = 'Draft';
    global static final Integer SFDC_CHUNK_LIMIT = 10;
    // Every chunk has max of 200 records of the same type ? (in case of multiple types in a single DML) -> needs to be confirmed!
    global static final Integer SFDC_CHUNKED_RECORD_COUNT_LIMIT = 200;
    global static final String ORDER_MODULE_NAME = 'M2M';
    global static final String ORDER_MODULE_VERSION = 'v1.0';
    global static final String PENDING = 'Pending';
    global static final String IN_PROGRESS = 'In Progress';
    global static final String PROVISION_IN_OFI = 'Provision_In_OFI';
    global static final String ONE_OFF = 'One Off';
    global static final String RATE_PAYMENT = 'Rate Payment';
	//sim & tarrif product configuration description
	global static final String SIM_AND_TARRIF = 'SIM & Tariff';
	//BIC Export Job Data statuses
	global static final String CSV_GENERATION_STARTED = 'CSV Generation Started';
	global static final String CSV_GENERATION_FINISHED = 'CSV Generation Finished';
	global static final String CSV_GENERATION_ERROR = 'CSV Generation Error';
	global static final String BIC_UPLOAD_STARTED = 'BIC Upload Started';
	global static final String BIC_UPLOAD_FINISHED = 'BIC Upload Finished';
	global static final String BIC_UPLOAD_ERROR = 'BIC Upload Error';
	
	//SIM Termination Job statuses
	global static final String TERMINATION_PENDING = 'TERMINATION PENDING';
	global static final String TERMINATION_COMPLETED = 'TERMINATION COMPLETED';
	global static final String RETIREMENT_PENDING = 'RETIREMENT PENDING';
	global static final String 	RETIREMENT_COMPLETED = 'RETIREMENT COMPLETED';
		
	//BIC Export Job Data status descriptions
	global static final String CSV_GENERATION_FINISHED_CSV_GENERATED = 'BIC CSV File successfully generated';
	global static final String CSV_GENERATION_FINISHED_NO_SIMS = 'No SIM cards to export';
	//SM2M SIM Statuses
	global static final String PENDING_SM2M_STATUS = 'PENDING_SM2M_STATUS';
	global static final String INACTIVE_NEW = 'INACTIVE_NEW';
	global static final String TEST = 'TEST';
	global static final String ACTIVATION_PENDANT = 'ACTIVATION_PENDANT';
	global static final String ACTIVATION_READY = 'ACTIVATION_READY';
	global static final String DEACTIVATED = 'DEACTIVATED';
	global static final String ACTIVE = 'ACTIVE';
	global static final String SUSPENDED = 'SUSPENDED';
	global static final String RETIRED = 'RETIRED';
	global static final String RESTORE = 'RESTORE';

	global static String SIMREPORT_ACTIVATIONS = '1.GA';
	global static String SIMREPORT_DEACTIVATIONS = '2.Churn';
	global static String SIMREPORT_NET_ADS = '3.NA';
	global static String SIMREPORT_CUSTOMER_BASE = '4.CB';
	global static String SIMREPORT_TOTALS = 'X - Totals - X';
}