global class PublishActCenterSubscriptionPoller implements Schedulable {
	public static void startPolling() {
		PublishActCenterSubscriptionPoller.scheduleTick(System.now().addSeconds(10));
	}
	
	global void execute(SchedulableContext sc) {
		List<Activation_Centre__c> actCenterList = [SELECT id, M2M_Order_Request__c, status__c from Activation_Centre__c where status__c = 'TUF Upload Success'];
		List<Id> orderReqList = new List<Id>();
		for (Activation_Centre__c ac : actCenterList){
			orderReqList.add(ac.M2M_Order_Request__c);
		}

		List<Third_Upload_File__c> tufs = [
			Select
				Id, Name, Upload_to_SM2M_Status__c, Order_Request__c, Order_Request__r.Billing_Account__c, Order_Request__r.Billing_Account__r.Billing_Email__c
			from
				Third_Upload_File__c
			where 
				Upload_to_SM2M_Status__c = 'Uploaded'
				and Order_Request__c in :orderReqList
				and Publish_Subscriptions_Status__c != 'Completed'
			Limit 10
		];

		for (Third_Upload_File__c jobData : tufs) {
		    //jobData.Upload_to_SM2M_Status__c = 'Published';
		    //jobData.Publish_Subscriptions_Status__c = 'Published';
		    //P.M. Optimization
		    //update jobData;
		    for (Activation_Centre__c ac : actCenterList){
				if(jobData.Order_Request__c == ac.M2M_Order_Request__c){
					ac.Status__c = 'Publish Started';
					break;
				}
			}
			csam_t1.ObjectGraphCalloutHandler.createAndSend('Activation Centre Publish Subscriptions', jobData.Id);
		}	
		
		update tufs;
		update actCenterList;
		
		Datetime nextTick = System.now().addSeconds(60);
		PublishActCenterSubscriptionPoller.scheduleTick(nextTick);
		System.abortJob(sc.getTriggerId());
	}
	
	private static void scheduleTick(Datetime dt) {
		String chron_exp = dt.format('ss mm HH dd MM ? yyyy');
		PublishActCenterSubscriptionPoller poller = new PublishActCenterSubscriptionPoller();
		// Schedule the next job, and give it the system time so name is unique
		System.schedule(jobName(dt), chron_exp, poller);
	}
	
	private static String jobName(Datetime dt) {
		return 'PublishActCenterSubscriptionPoller' + dt.getTime();
	}
}