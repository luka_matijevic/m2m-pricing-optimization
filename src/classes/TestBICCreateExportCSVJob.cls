/**
 * TestBICCreateExportCSVJob - Test for TestBICCreateExportCSVJob Batch
 */

@isTest
private class TestBICCreateExportCSVJob
{
	static void dataSetup()
	{
		No_Trigger__c notriggers = new No_Trigger__c();
    	notriggers.Flag__c = true;
    	insert notriggers;

		//
    	// Create test data
    	//
    	TestHelper.createOrderRequest();
		notriggers.Flag__c = false;
    	update notriggers;
	}

	//--------------------------------------------------------------------------
	// Test for the first time synchronisation
	//--------------------------------------------------------------------------
	static testMethod void testFirstSync()
	{
        /*
		dataSetup();
		Order_Request__c ord = [ SELECT Id, Account__c FROM Order_Request__c LIMIT 1 ];

		Third_Upload_File__c thirdFile = new Third_Upload_File__c(
			Name = 'name',
			Order_Request__c = ord.Id
		);
		insert thirdFile;
		TestSecondUploadFileDataGenerator.insertSimAssetsBIC();

		List<Stock__c> stocks =
				[ SELECT Id, State_Change_Date__c,
				  SM2M_Status__c, IMSI__c, Third_Upload_File__c
				  FROM Stock__c ];
		Integer i = 0;
		for (Stock__c stock : stocks)
		{
			stock.Third_Upload_File__c = thirdFile.Id;
			//
			// Reset state change date for test
			//
			stock.State_Change_Date__c = null;
			stock.SM2M_Status__c = Math.mod(i, 2) == 0
					? Constants.ACTIVE
					: Constants.ACTIVATION_PENDANT;
			stock.IMSI__c = '123456789012' + String.valueOf(i);
			stock.Account__c = ord.Account__c;
			i++;
		}
		update stocks;

		stocks =
				[ SELECT Id, State_Change_Date__c,
				  SM2M_Status__c, IMSI__c, Third_Upload_File__c
				  FROM Stock__c ];

		Test.startTest();
		Database.executeBatch(new BICCreateExportCSVJob());
		Test.stopTest();

		BIC_Export_Job_Data__c jobData =
				[ SELECT Id, Status__c, Status_Description__c
				  FROM BIC_Export_Job_Data__c ];
		System.assertEquals(Constants.CSV_GENERATION_FINISHED, jobData.Status__c);
		System.assertEquals(Constants.CSV_GENERATION_FINISHED_CSV_GENERATED, jobData.Status_Description__c);
		Attachment att =
				[ SELECT Id, Body
				  FROM Attachment
				  WHERE ParentId = :jobData.Id ];
		List<String> attLines = att.Body.toString().split('\r\n');
		System.assertEquals(201, attLines.size());
        */
	}

	//--------------------------------------------------------------------------
	// Test for the sync with no SIM cards to put in CSV
	//--------------------------------------------------------------------------
	static testMethod void testNoSIMSSync()
	{
        /*
		dataSetup();
		Order_Request__c ord = [ SELECT Id FROM Order_Request__c LIMIT 1 ];

		Third_Upload_File__c thirdFile =
			new Third_Upload_File__c
				( Name = 'name'
				, Order_Request__c = ord.Id );
		insert thirdFile;
		TestSecondUploadFileDataGenerator.insertSimAssetsBIC();

		List<Stock__c> stocks =
			[ SELECT Id, State_Change_Date__c, SM2M_Status__c,
			  IMSI__c, Third_Upload_File__c
			  FROM Stock__c ];
		Integer i = 0;
		for (Stock__c stock : stocks)
		{
			stock.IMSI__c = '123456789012' + String.valueOf(i);
			i++;
		}
		update stocks;

		Test.startTest();
		Database.executeBatch(new BICCreateExportCSVJob());
		Test.stopTest();

		BIC_Export_Job_Data__c jobData =
				[ SELECT Id, Status__c, Status_Description__c
				  FROM BIC_Export_Job_Data__c
				  WHERE Status__c = :Constants.CSV_GENERATION_FINISHED ];
		System.assertEquals(Constants.CSV_GENERATION_FINISHED, jobData.Status__c);
		System.assertEquals(Constants.CSV_GENERATION_FINISHED_NO_SIMS, jobData.Status_Description__c);
		List<Attachment> atts =
				[ SELECT Id, Body
				  FROM Attachment
				  WHERE ParentId = :jobData.Id ];
		System.assertEquals(0, atts.size());
*/
	}
}