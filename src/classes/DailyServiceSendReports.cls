global class DailyServiceSendReports implements Database.Batchable<SObject>, Database.AllowsCallouts{
	
    private String query;
    private String period;

	global DailyServiceSendReports(String period) {
        this.period = period;
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
        List<String> reportIds = new List<String>();
        if(period == 'Daily'){
            List<DailyServiceReportIds__c> reportListDaily = DailyServiceReportIds__c.getall().values();
            for(DailyServiceReportIds__c obj : reportListDaily){

                reportIds.add(obj.Report_ID__c);
            }
        }
        else if(period == 'Weekly'){
            List<WeeklyServiceReportIds__c> reportListWeekly = WeeklyServiceReportIds__c.getall().values();
            for(WeeklyServiceReportIds__c obj : reportListWeekly){

                reportIds.add(obj.Report_ID__c);
            }
        }
        else if(period == 'Monthly'){
            List<MonthlyServiceReportIds__c> reportListMonthly = MonthlyServiceReportIds__c.getall().values();
            for(MonthlyServiceReportIds__c obj : reportListMonthly){

                reportIds.add(obj.Report_ID__c);
            }
        }
        query = 'SELECT Id, Name FROM Report WHERE  Id = :reportIds';
		
        return Database.getQueryLocator(query);
	}

    public void fillAndSend(List<Report> scope){
        List<DailyServiceEmails__c> emails = DailyServiceEmails__c.getall().values();
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();

    //Get report id and report name
        
        List<Messaging.EmailFileAttachment> reports = new List<Messaging.EmailFileAttachment>();
       
        List<String> addresses = new List<String>();

    // Attach reports to email

        for(Report obj : scope){

            ApexPages.PageReference report = new ApexPages.PageReference('/' + String.valueOf(obj.Id) + '?csv=1&enc=UTF-8');
            Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
            attachment.setFileName( String.valueOf(obj.Name) + '.csv');
            
            if(!Test.isRunningTest()){
                attachment.setBody(report.getContent());
            }
            attachment.setContentType('text/csv');
            reports.add(attachment);
        }

        message.setFileAttachments( reports );
        if (period == 'Daily'){
            message.setSubject('Daily Service Reports');
        }
        else if (period == 'Weekly'){
            message.setSubject('Weekly Service Reports');
        }
        else if (period == 'Monthly'){
            message.setSubject('Monthly Service Reports');
        }
        message.setPlainTextBody('The reports are attached.');


    //get Recepients 

        for(DailyServiceEmails__c obj : emails){
            addresses.add(String.valueOf(obj.Email__c));
        }

        message.setToAddresses( addresses );
       
        try{
            Messaging.sendEmail( new Messaging.SingleEmailMessage[] { message } );
        }
        catch(System.EmailException ex){
            System.debug(ex);
            if(!Test.isRunningTest()){
                throw ex;
                }
            }
    }

   	global void execute(Database.BatchableContext BC, List<Report> scope) {
        
        fillAndSend(scope);
    }

    global void finish( Database.BatchableContext BC) {

    	}

}