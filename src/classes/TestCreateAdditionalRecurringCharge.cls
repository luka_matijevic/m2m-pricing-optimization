@isTest
private class TestCreateAdditionalRecurringCharge {

	//get journal mapping name from product type
	static testMethod void getJournalMapNameFromProduct(){
        CreateAdditionalRecurringCharge carc = new CreateAdditionalRecurringCharge();
        String jMapName = carc.getJournalMapNameFromDBIProductType('Data');
        System.assertEquals('Data Usage Fee', jMapName);
        
        jMapName = carc.getJournalMapNameFromDBIProductType('SMS');
        System.assertEquals('SMS Usage Fee', jMapName);
        
        jMapName = carc.getJournalMapNameFromDBIProductType('Voice');
        System.assertEquals('Voice Usage Fee', jMapName);
        
        jMapName = carc.getJournalMapNameFromDBIProductType('Monthly fee');
        System.assertEquals('Grundgebühr', jMapName);
        
        jMapName = carc.getJournalMapNameFromDBIProductType('Hardware');
        System.assertEquals('HW Revenue (Hardware Only)', jMapName);
        
        jMapName = carc.getJournalMapNameFromDBIProductType('Service');
        System.assertEquals('Kosten Serviceeinsatz (einmalig & recurring)', jMapName);
	}

	static testMethod void createM2MOrderRequest(){
		Account acc = new Account(
			Name = 'Test account',
			BillingCity = 'Bremen',
			BillingCountry = 'Germany',
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße',
			Type = 'Customer',
            Global__c = true,
			VAT__c = 1
		);
		insert acc;

		No_Trigger__c notriggers = new No_Trigger__c();
    	notriggers.Flag__c = true;
    	insert notriggers;
    	
    	Billing_Account__c bac = new Billing_Account__c();
		bac.Payer__c = acc.Id;
		bac.Name = 'test billing account';
		bac.VAT__c = '12';
		insert bac;

		Daily_Bill__c db = new Daily_Bill__c();
		db.Date__c = Date.newInstance(2016, 7, 1);
		db.Daily_Bill_Status__c = 'Created';
		db.Daily_Bill_Upload_Status__c = 'Pending';
		insert db;

		Boni_Check__c bc = new Boni_Check__c();
		bc.Status_of_CC__c = 'Accepted';
		bc.Account__c = acc.Id;
		Insert bc;

		Journal_Mapping__c jm = new Journal_Mapping__c();
		jm.Name = 'Data Usage Fee';
		jm.Description__c = 'Data Usage Fee';
		jm.Revenue__c = '19%';
		jm.Country__c = 'DE';
		jm.Konto__c = '4120400';
		jm.KoSt__c = '465605';
		jm.Prod__c = '387654';

		insert jm;

		Journal_Mapping__c jm2 = new Journal_Mapping__c();
		jm2.Name = 'Data Usage Fee';
		jm2.Description__c = 'Data Usage Fee';
		jm2.Revenue__c = '0%';
		jm2.Country__c = 'DE';
		jm2.Konto__c = '4120400';
		jm2.KoSt__c = '465605';
		jm2.Prod__c = '387654';

		insert jm2;
    	
    	notriggers.Flag__c = false;
    	update notriggers;
		
		Id dbiId = [SELECT Id from Daily_Bill__c LIMIT 1][0].Id;

		Daily_Bill_Item__c dbi = new Daily_Bill_Item__c();
		dbi.Account__c = acc.Id;
		dbi.Number_of_months__c = 12;
		dbi.Betrag__c = 1500;
		dbi.Anweisung__c = 'aaaa';
		dbi.Tax_Code__c = '19%';
		dbi.Art__c = 1;
		dbi.Beschreibung__c = 'description';
		dbi.Payment_Type__c = 'Recurring';
		dbi.Product_Type__c = 'Data';

		Daily_Bill_Item__c dbi2 = new Daily_Bill_Item__c();
		dbi2.Account__c = acc.Id;
		dbi2.Number_of_months__c = 12;
		dbi2.Betrag__c = 1500;
		dbi2.Anweisung__c = 'aaaa';
		dbi2.Tax_Code__c = '0%';
		dbi2.Art__c = 1;
		dbi2.Beschreibung__c = 'description';
		dbi2.Payment_Type__c = 'Recurring';
		dbi2.Product_Type__c = 'Data';

		Test.startTest();
		CreateAdditionalRecurringCharge carc = new CreateAdditionalRecurringCharge();
		carc.CreateAdditionalRecurringChargeForDbi(dbi, dbiId);
		carc.CreateAdditionalRecurringChargeForDbi(dbi2, dbiId);
		Test.stopTest();
	}
}