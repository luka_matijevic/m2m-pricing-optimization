/*Petar Matkovic 24.8.2016.
 *Put IoT SIM Cards that have sm2m_status activation_ready more than 60 days status to 'Pending activation'
*/
public class ScheduleBatchActivationIoTSimCards implements Schedulable {
	
	public void execute(SchedulableContext sc) {
		Date dateForActivation = System.today();
		dateForActivation = dateForActivation.addDays(-60);
		Database.executeBatch(new BatchActivateIoTActivationReadySimCards());
	}
}