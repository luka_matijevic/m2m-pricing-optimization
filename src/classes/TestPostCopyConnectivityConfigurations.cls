@isTest
private class TestPostCopyConnectivityConfigurations
{
    //--------------------------------------------------------------------------
    // Test post copy connectivity confgiurations fucntionality
    //--------------------------------------------------------------------------
    static testMethod void testPostCopyConnectivityConfigurations()
    {
        TestHelper.createCopyProductBundle();

        Test.startTest();

        Id productBundleId = [ SELECT Id FROM cscfga__Product_Bundle__c ].Id;
        Id tariffId = [ SELECT Id FROM Tariff__c WHERE Parent_Tariff__c != null ].Id;

        PostCopyConnectivityConfigurations postCopy =
            new PostCopyConnectivityConfigurations(productBundleId);
        postCopy.doExecute();

        //
        // Checks
        //
        //
        // Check if we created the clone
        //
        Tariff__c clonedTariff =
                [ SELECT Id, Name, Status__c,
                  ( SELECT Id, Name
                    FROM Tariff_Options__r)
                  FROM Tariff__c
                  WHERE Parent_Tariff__c != null
                  AND Id != :tariffId ];

        Map<String, Tariff_Option__c> tariffOptions = new Map<String, Tariff_Option__c>();
        for (Tariff_Option__c tariffOption : clonedTariff.Tariff_Options__r)
        {
            tariffOptions.put(tariffOption.Name, tariffOption);
        }

        cscfga__Product_Configuration__c[] configurations =
            [ SELECT Id, Name, Original_Product_Name__c,
              cscfga__Product_Definition__r.Name,
              ( SELECT Id, Name, cscfga__Value__c
                FROM cscfga__Attributes__r
                WHERE Name IN ('Tariff', 'Additional Tariff Option', 'Supplementary Service', 'Requires Commercial Approval'))
              FROM cscfga__Product_Configuration__c
              WHERE cscfga__Product_Bundle__c = :productBundleId ];

        Map<cscfga__Attribute__c, String> attributes = new Map<cscfga__Attribute__c, String>();
        for (cscfga__Product_Configuration__c configuration : configurations)
        {
            for (cscfga__Attribute__c attribute : configuration.cscfga__Attributes__r)
            {
                attributes.put(attribute, configuration.Name);
            }
        }

        Integer requiresCommercialApprovalCount = 0;
        Integer tariffCount = 0;
        Integer tariffOptionCount = 0;
        for (cscfga__Attribute__c attribute : attributes.keySet())
        {
            if ('Requires Commercial Approval' == attribute.Name)
            {
                system.assertEquals('Yes', attribute.cscfga__Value__c);
                requiresCommercialApprovalCount++;
            }
            else if ('Tariff' == attribute.Name)
            {
                system.assertEquals(clonedTariff.Id, attribute.cscfga__Value__c);
                tariffCount++;
            }
            else if ('Additional Tariff Option' == attribute.Name
                     || 'Supplementary Service' == attribute.Name)
            {
                system.assertEquals(tariffOptions.get(attributes.get(attribute)).Id, attribute.cscfga__Value__c);
                tariffOptionCount++;
            }
        }
        system.assertEquals(1, requiresCommercialApprovalCount);
        system.assertEquals(1, tariffCount);
        system.assertEquals(4, tariffOptionCount);
    }
}