@isTest
private class TestUpdateSIMStatusesJob
{
	static void dataSetup()
	{
		No_Trigger__c notriggers = new No_Trigger__c();
		notriggers.Flag__c = true;
		insert notriggers;

		//
		// Create test account
		//
		Account acc = new Account
			( Name = 'Test account'
			, BillingCity = 'Bremen'
			, BillingCountry = 'Germany'
			, BillingPostalCode = '5000'
			, BillingStreet = 'Lindenstraße'
			, Type = 'Customer'
			, SM2M_ID__c = 'TestSM2MId' );
		insert acc;

		TestHelper.createArticleInfoSets();
		Id aisId =
			[ SELECT Id FROM Article_Info_Set__c
			  WHERE Article_Type__c = 'SIM'
			  LIMIT 1 ].Id;

		SRF__c srf = new SRF__c
			( Name = 'Test SRF'
			, SIM_Type__c = aisId
			, RecordTypeId = Cache.RecordTypeId('SRF__c.Internal SIM Delivery')
			, Account__c = acc.Id );
		insert srf;

		TestSecondUploadFileDataGenerator.insertSimAssetsBIC();

		List<Stock__c> stocks =
				[ SELECT Id, IMSI__c, SRF__c
				  FROM Stock__c ];
		Integer i = 0;
		for (Stock__c stock : stocks)
		{
			stock.SRF__c = srf.Id;
			stock.IMSI__c = '123456789012' + String.valueOf(i);
			i++;
		}
		update stocks;

		notriggers.Flag__c = false;
		update notriggers;
	}

	//--------------------------------------------------------------------------
	// Update SIM statuses
	//--------------------------------------------------------------------------
	static testMethod void updateSIMStatuses()
	{
		//
		// Craete test data
		//
		dataSetup();

		Test.startTest();
		Database.executeBatch(new UpdateSIMStatusesJob());
		Test.stopTest();

		//
		// There is no way to check if integration is complete.
		// TODO Use mocks to check result
		//
	}
}