@isTest
private class StornoTriggerDelegateTest {

	static testMethod void StornoTriggerTest() {
        
        Daily_Bill__c db = new Daily_Bill__c();
        db.date__c=date.today();
        insert db;
        
        Storno__c storno = new Storno__c();
        storno.Art__c='6';
        //storno.name='Test';
        storno.reason__c='Test';
        //storno.VO_NR__c='1';
        //storno.Einzel_Nettobetrag__c=10;
        storno.Daily_Bill__c=db.id;
        storno.Invoice_Number__c='10';
        //storno.vat__c=19;
        insert storno;
        
        List<Daily_Bill_Item__c> c = [SELECT name, id from Daily_Bill_Item__c];
        system.assert(!c.isEmpty(),'Trigger not working as expected');
        
	}

}