public class BoniCheckController {

    public final static string BONICHECK_STATUS_DRAFT = 'Draft';
    public final static string BONICHECK_STATUS_SUBMITTED = 'Submitted';
    public static final string BONICHECK_STATUS_ACCEPTED = 'Accepted';
    public static final string BONICHECK_STATUS_DECLINED = 'Declined';
    public static final string BONICHECK_STATUS_INPROGRESS = 'In Progress';
    public static final string BONICHECK_STATUS_INPROGRESS_RISK_PAPER_NEEDED = 'In Progress - Risk Paper needed';
    public static final string BONICHECK_STATUS_INPROGRESS_BANK_GUARANTEE_NEEDED = 'In Progress - Bank guarantee needed';
    public static final string BONICHECK_STATUS_INPROGRESS_DECLINED_RISK_PAPER = 'In Progress - Declined Risk Paper';
    public static final string BONICHECK_STATUS_INPROGRESS_DECLINED_BANK_GUARANTEE = 'In Progress - Declined Bank guarantee';


    public Boni_Check__c CurrentBoniCheck { get; set; }
    public Account Account { get; set; }
    public Boni_Check__c NewBoniCheck { get; set; }
    public sObject sControllerObject { get; set; }
    private No_Trigger__c notrigger = null;
    
    //Added by Mahaboob on 22/4/2014 as part of solution for T-08574
    public String bcRequester{get;set;}
    //Done by Mahaboob

    public boolean displayPopup {get; set;}

    public boolean addAttachment {get; set;}

    public void closePopup() {
        displayPopup = false;
    }
    public void showPopup() {
        displayPopup = true;
    }


    public boolean hideAllButtonsfor1stLevel {get; set;} // If logged in user is 1st Level - DSC - READ permissions profile user, then none of button should visible

    public BoniCheckController(ApexPages.StandardController standardController)
    {
       
         hideAllButtonsfor1stLevel = false;
         
         list<profile> prfl = new list<profile>([select id, name from profile where id=:userinfo.getProfileId()]);
		
		
		if(!prfl.isEMpty()){
		    if(prfl[0].name == '1st Level - DSC - READ permissions'){
		        hideAllButtonsfor1stLevel = true;
		    }
		}
      
         addAttachment = false;
         map<string, string> params = ApexPages.currentPage().getParameters();

        if (!params.containsKey('id') && !params.containsKey('accountId')) {
            throw new BoniCheckControllerException ('No account Id provided!');
        }

        this.sControllerObject = standardController.getSubject();

        this.NewBoniCheck = new Boni_Check__c();
		
        this.NewBoniCheck.Status_Of_CC__c = BONICHECK_STATUS_DRAFT;
		this.NewBoniCheck.Boni_Check_status_history__c = this.CurrentBoniCheck.Boni_Check_status_history__c != null && this.NewBoniCheck.Boni_Check_status_history__c.length() == 0 ? this.NewBoniCheck.Status_Of_CC__c + ' ' + Datetime.now() : this.NewBoniCheck.Boni_Check_status_history__c + '|' + this.NewBoniCheck.Status_Of_CC__c + ' ' + Datetime.now();
		this.NewBoniCheck.Boni_Check_status_history__c = this.NewBoniCheck.Boni_Check_status_history__c.replace('null|', '');
        this.NewBoniCheck.International_Roaming_Voice__c = true;
        this.NewBoniCheck.Premium_Services_0900__c = true;

        Id boniCheckId = params.get('id');
        Id accountId = params.get('accountId');
        this.DebugEnabled = params.get('debug') != null ? Boolean.valueOf(params.get('debug')) : false;

        if (boniCheckId != null) {
            this.CurrentBoniCheck = database.query('select ' + SObjectHelper.getFieldListForSOQL(Boni_Check__c.SObjectType.getDescribe().getName(), null, null) +
                                                    ' from Boni_Check__c where Id = \'' + boniCheckId + '\'');

            accountId = this.CurrentBoniCHeck.Account__c;
            //Added by Mahaboob on 22/4/2014 as part of solution for T-08574
            bcRequester = [select CreatedBy.Name from Boni_Check__c where Id = :boniCheckId].CreatedBy.Name;
            //Done by Mahaboob
        }

        if (accountId != null) {
            this.Account = [select Id, Name, BillingStreet, BillingPostalCode, BillingCity, BillingCountry,
                               Website, NumberOfEmployees, Value_Added_Tax_Id__c, Industry
                        from Account where Id = :accountId];

            this.NewBoniCheck.Account__c = accountId;
        }
    }

    public BoniCheckController() {
        addAttachment = false;
        map<string, string> params = ApexPages.currentPage().getParameters();

        if (!params.containsKey('id') && !params.containsKey('accountId')) {
            throw new BoniCheckControllerException ('No account Id provided!');
        }

        this.NewBoniCheck = new Boni_Check__c();
        this.NewBoniCheck.Status_Of_CC__c = BONICHECK_STATUS_DRAFT;
        this.NewBoniCheck.International_Roaming_Voice__c = true;
        this.NewBoniCheck.Premium_Services_0900__c = true;

        Id boniCheckId = params.get('id');
        Id accountId = params.get('accountId');

        try
        {
        this.DebugEnabled = params.get('debug') != null ? Boolean.valueOf(params.get('debug')) : false;
        }
        catch(Exception ex)
        {
            this.DebugEnabled = false;
        }

        if (boniCheckId != null) {
            this.CurrentBoniCheck = database.query('select ' + SObjectHelper.getFieldListForSOQL(Boni_Check__c.SObjectType.getDescribe().getName(), null, null) +
                                                    ' from Boni_Check__c where Id = \'' + boniCheckId + '\'');

            accountId = this.CurrentBoniCHeck.Account__c;
            //Added by Mahaboob on 22/4/2014 as part of solution for T-08574
            bcRequester = [select CreatedBy.Name from Boni_Check__c where Id = :boniCheckId].CreatedBy.Name;
            //Done by Mahaboob
        }

        if (accountId != null) {
            this.Account = [select Id, Name, BillingStreet, BillingPostalCode, BillingCity, BillingCountry,
                               Website, NumberOfEmployees, Value_Added_Tax_Id__c, Industry
                        from Account where Id = :accountId];

            this.NewBoniCheck.Account__c = accountId;
        }
    }

    public boolean getCanRequestNewBoniCheck() {
        system.debug(this.CurrentBoniCheck);

        return
            isSalesPersonUser() &&
            // only if not viewing an existing one
            this.CurrentBoniCheck == null;
    }

    public boolean getCanApprove() {
        if (CurrentBoniCheck != null) {
            //set<string> boniUserApprovableStatuses = new set<string> { BONICHECK_STATUS_INPROGRESS, BONICHECK_STATUS_SUBMITTED };
            //set<string> salesHeadUserApprovableStatuses = new set<string> ();//{ BONICHECK_STATUS_INPROGRESS_RISK_PAPER_NEEDED };
            return getIsBoniUser();
            //return (getIsBoniUser() && boniUserApprovableStatuses.contains(CurrentBoniCheck.Status_Of_CC__c)) ||
            //        (isSalesHeadUser() && salesHeadUserApprovableStatuses.contains(CurrentBoniCheck.Status_Of_CC__c));
        } else {
            return false;
        }
    }

      public boolean getCanSaveRecord() {
        if (CurrentBoniCheck != null) {
            return getIsBoniUser();
        } else {
            return false;
        }
    }

    public boolean getCanDecline() {
        if (CurrentBoniCheck != null) {
            //set<string> boniUserDeclininableStatuses = new set<string> { BONICHECK_STATUS_INPROGRESS, BONICHECK_STATUS_SUBMITTED };
           // set<string> salesHeadUserDeclinableStatuses = new set<string> { BONICHECK_STATUS_INPROGRESS_RISK_PAPER_NEEDED };

            return getIsBoniUser();
             //&& boniUserDeclininableStatuses.contains(CurrentBoniCheck.Status_Of_CC__c));
             //|| (isSalesHeadUser() && salesHeadUserDeclinableStatuses.contains(CurrentBoniCheck.Status_Of_CC__c));
        } else {
            return false;
        }
    }

    public boolean getCanDeclineBankGuarantee() {
        if (CurrentBoniCheck != null) {
            set<string> boniUserBankGaranteeDeclininableStatuses = new set<string> { BONICHECK_STATUS_INPROGRESS_BANK_GUARANTEE_NEEDED };

            return isSalesPersonAndOwnerUser() && boniUserBankGaranteeDeclininableStatuses.contains(CurrentBoniCheck.Status_Of_CC__c);
        } else {
            return false;
        }
    }

    public boolean getCanRequestBankGuarantee() {
        if (CurrentBoniCheck != null) {
            set<string> boniUserDeclininableStatuses = new set<string> { BONICHECK_STATUS_INPROGRESS, BONICHECK_STATUS_SUBMITTED, BONICHECK_STATUS_INPROGRESS_DECLINED_BANK_GUARANTEE, BONICHECK_STATUS_INPROGRESS_DECLINED_RISK_PAPER };

            return (getIsBoniUser() && boniUserDeclininableStatuses.contains(CurrentBoniCheck.Status_Of_CC__c));
        } else {
            return false;
        }
    }

        public boolean getCanAddBankGuarantee() {
        if (CurrentBoniCheck != null) {
            set<string> salesUserCanAddGuaranteeStatuses = new set<string> { BONICHECK_STATUS_INPROGRESS_BANK_GUARANTEE_NEEDED };
            //sales users can allways add bank guarantee
            //return isSalesPersonAndOwnerUser();
            return (isSalesPersonAndOwnerUser() && salesUserCanAddGuaranteeStatuses.contains(CurrentBoniCheck.Status_Of_CC__c));
        } else {
            return false;
        }
    }

    public boolean getCanDeclineRiskPaper() {
        if (CurrentBoniCheck != null) {
            set<string> boniUserRiskPaperDeclininableStatuses = new set<string> { BONICHECK_STATUS_INPROGRESS_RISK_PAPER_NEEDED };

            return (isSalesHeadUser() && boniUserRiskPaperDeclininableStatuses.contains(CurrentBoniCheck.Status_Of_CC__c));
        } else {
            return false;
        }
    }

    public boolean getCanAddRiskPaper() {
        if (CurrentBoniCheck != null) {
            set<string> salesUserCanAddRiskStatuses = new set<string> { BONICHECK_STATUS_INPROGRESS_RISK_PAPER_NEEDED };
            //sales users can allways add risk person
            //return isSalesHeadUser();
            return (isSalesHeadUser() && salesUserCanAddRiskStatuses.contains(CurrentBoniCheck.Status_Of_CC__c));
        } else {
            return false;
        }
    }

    public boolean getCanRequestRiskPaper() {
        if (CurrentBoniCheck != null) {
            set<string> boniUserDeclininableStatuses = new set<string> { BONICHECK_STATUS_INPROGRESS, BONICHECK_STATUS_SUBMITTED, BONICHECK_STATUS_INPROGRESS_DECLINED_BANK_GUARANTEE, BONICHECK_STATUS_INPROGRESS_DECLINED_RISK_PAPER };

            return (getIsBoniUser() && boniUserDeclininableStatuses.contains(CurrentBoniCheck.Status_Of_CC__c));
        } else {
            return false;
        }
    }

    public PageReference requestNewBoniCheck() {
        // Validate
        if (validateNewRequest(this.NewBoniCheck) == false) {
            return null;
        }
        // Add new request
        this.NewBoniCheck.Status_Of_CC__c = BONICHECK_STATUS_SUBMITTED;
		this.NewBoniCheck.Boni_Check_status_history__c = this.CurrentBoniCheck.Boni_Check_status_history__c != null && this.NewBoniCheck.Boni_Check_status_history__c.length() == 0 ? this.NewBoniCheck.Status_Of_CC__c + ' ' + Datetime.now() : this.NewBoniCheck.Boni_Check_status_history__c + '|' + this.NewBoniCheck.Status_Of_CC__c + ' ' + Datetime.now();
        this.NewBoniCheck.Boni_Check_status_history__c = this.NewBoniCheck.Boni_Check_status_history__c.replace('null|', '');
		insert this.NewBoniCheck;

        PageReference result = Page.BoniCheck;
        result.getParameters().put('id', this.NewBoniCheck.Id);
        result.setRedirect(true);
        return result;
    }

    private boolean validateNewRequest(Boni_Check__c req) {
        
        if (req.International_Roaming_Voice__c == false &&
            req.Blocking_Comment__c == null || req.Blocking_Comment__c == '') {
            req.Blocking_Comment__c.addError('If International Roaming Voice is not blocked, you must enter a comment.');
            return false;
        }
        if (req.Premium_Services_0900__c == false &&
            req.Blocking_Comment__c == null || req.Blocking_Comment__c == '') {
            req.Blocking_Comment__c.addError('If Premium Services is not blocked, you must enter a comment.');
            return false;
        }
        
        if ((req.Fix_Revenue_Data__c == null || req.Fix_Revenue_Data__c == 0) &&
                (req.Fix_Revenue_SMS__c == null || req.Fix_Revenue_SMS__c == 0) &&
                (req.Fix_Revenue_Voice__c == null || req.Fix_Revenue_Voice__c == 0) &&
                (req.Variable_Revenue_Data__c == null || req.Variable_Revenue_Data__c == 0) &&
                (req.Variable_Revenue_SMS__c == null || req.Variable_Revenue_SMS__c == 0) &&
                (req.Variable_Revenue_Voice__c == null || req.Variable_Revenue_Voice__c == 0) &&
                (req.Number_Requested_Data__c == null || req.Number_Requested_Data__c == 0) &&
                (req.Number_Requested_SMS__c == null || req.Number_Requested_SMS__c == 0) &&
                (req.Number_Requested_Voice__c == null || req.Number_Requested_Voice__c == 0) &&
                (req.Fix_Revenue_HW__c == null || req.Fix_Revenue_HW__c == 0) &&
                (req.Variable_Revenue_HW__c == null || req.Variable_Revenue_HW__c == 0) &&
                (req.Requested_Units_HW__c == null || req.Requested_Units_HW__c == 0) &&
                    //Added by Mahaboob on 17.01.2014 as per T-08617
                (req.Fix_Revenue_VAS__c == null || req.Fix_Revenue_VAS__c == 0) &&
                (req.Variable_Revenue_VAS__c == null || req.Variable_Revenue_VAS__c == 0) &&
                (req.Number_Requested_VAS__c == null || req.Number_Requested_VAS__c == 0)) {
            req.addError('You cannot submit a request without any Voice/SMS/Data/HW/VAS values.');
            return false;
        }
            //added by zoran zunko on 19.09.2013. due to change in the calculation logic
         if (checkInvalidValuesForProductTypes(req.Fix_Revenue_Data__c, req.Variable_Revenue_Data__c, req.Number_Requested_Data__c))
         {
                req.addError('You need to fill one of the Fixed/Variable amounts AND the number of units for Data.');
                return false;
         }
         if (checkInvalidValuesForProductTypes(req.Fix_Revenue_SMS__c, req.Variable_Revenue_SMS__c, req.Number_Requested_SMS__c))
         {
                req.addError('You need to fill one of the Fixed/Variable amounts AND the number of units for SMS.');
                return false;
         }
         if (checkInvalidValuesForProductTypes(req.Fix_Revenue_Voice__c, req.Variable_Revenue_Voice__c, req.Number_Requested_Voice__c))
         {
                req.addError('You need to fill one of the Fixed/Variable amounts AND the number of units for Voice.');
                return false;
         }
         if (checkInvalidValuesForProductTypes(req.Fix_Revenue_HW__c, req.Variable_Revenue_HW__c, req.Requested_Units_HW__c))
         {
                req.addError('You need to fill one of the Fixed/Variable amounts AND the number of units for Hardware.');
                return false;
         }
            //Added by Mahaboob on 17.01.2014 to include VAS in the validation as per T-08617
         if (checkInvalidValuesForProductTypes(req.Fix_Revenue_VAS__c, req.Variable_Revenue_VAS__c, req.Number_Requested_VAS__c))
         {
                req.addError('You need to fill one of the Fixed/Variable amounts AND the number of units for VAS.');
                return false;
         }
            //End of code added by Mahaboob on 17.01.2014 as per T-08617
            
            //Added by Mahaboob on 16.01.2014 due to the ticket T-08823
            //Commented by Mahaboob on 22.01.2014
         /*if (checkInvalidValuesForRevenueFields(req.Fix_Revenue_SMS__c,req.Variable_Revenue_SMS__c)) {
                req.addError('You cannot enter comma in any of Fixed/Variable amounts for SMS.');
                return false;
         }
         if (checkInvalidValuesForRevenueFields(req.Fix_Revenue_Data__c, req.Variable_Revenue_Data__c)) {
                req.addError('You cannot enter comma in any of Fixed/Variable amounts for Data.');
                return false;
         }
         if (checkInvalidValuesForRevenueFields(req.Fix_Revenue_Voice__c, req.Variable_Revenue_Voice__c)) {
                req.addError('You cannot enter comma in any of Fixed/Variable amounts for Voice.');
                return false;
         }
         if (checkInvalidValuesForRevenueFields(req.Fix_Revenue_HW__c, req.Variable_Revenue_HW__c)) {
                req.addError('You cannot enter comma in any of Fixed/Variable amounts for Hardware.');
                return false;
         }
         if (checkInvalidValuesForRevenueFields(req.Fix_Revenue_VAS__c, req.Variable_Revenue_VAS__c)) {
                req.addError('You cannot enter comma in any of Fixed/Variable amounts for VAS');
                return false;
         }*/
            //End of Code added by Mahaboob on 16.01.2014 as per T-08823
        return true;
    }
    private boolean checkInvalidValuesForProductTypes(Decimal valueRevenueFix, Decimal valueRevenueVariable, Decimal valueRequested)
    {
        return 
            (
                ((valueRevenueFix != null && valueRevenueFix != 0) ||
                (valueRevenueVariable != null && valueRevenueVariable != 0)) &&
                (valueRequested == null || valueRequested == 0)
                ||
                ((valueRevenueFix == null || valueRevenueFix == 0) &&
                (valueRevenueVariable == null || valueRevenueVariable == 0)) &&
                (valueRequested != null && valueRequested != 0)
            );
    }
    /**
        Added a method on 16.01.2014 by Mahaboob to check for comma in the currency fields
    */
    //Commented by Mahaboob on 22.01.2014
    /*private boolean checkInvalidValuesForRevenueFields(Decimal revenueValueFix, Decimal revenueValueVariable) {
        return (
            (revenueValueFix != null && revenueValueFix.format().contains(',')) || (revenueValueVariable != null && revenueValueVariable.format().contains(','))
        );
    }*/
    /**
        End of method added by Mahaboob on 16.01.2014 
    */
    public void approve() {
        string tmpStatus = this.CurrentBoniCheck.Status_Of_CC__c;
        try
        {
            this.CurrentBoniCheck.Status_Of_CC__c = BONICHECK_STATUS_ACCEPTED;
			this.CurrentBoniCheck.Boni_Check_status_history__c = this.CurrentBoniCheck.Boni_Check_status_history__c != null && this.CurrentBoniCheck.Boni_Check_status_history__c.length() == 0 ? this.CurrentBoniCheck.Status_Of_CC__c + ' ' + Datetime.now() : this.CurrentBoniCheck.Boni_Check_status_history__c + '|' + this.CurrentBoniCheck.Status_Of_CC__c + ' ' + Datetime.now();
            this.CurrentBoniCheck.Boni_Check_status_history__c = this.CurrentBoniCheck.Boni_Check_status_history__c.replace('null|', '');
			update this.CurrentBoniCheck;
            
            List<Order_Request__c> orderList = [SELECT Id,Name,Boni_Check__c,Opportunity__c from Order_Request__c where Boni_Check__c=:this.CurrentBoniCheck.id];
            if (!orderList.isEmpty())
            {
                closeRelatedOpp(orderList[0]);
            }
            
        }
        catch(DMLException ex)
        {
            this.CurrentBoniCheck.Status_Of_CC__c = tmpStatus;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getDmlMessage(0)));
        }
    }

        
     private void closeRelatedOpp(Order_Request__c m2mOrder)
    {

        List<Opportunity> oppList = [SELECT id, name, StageName, Record_Type_Name__c from Opportunity where Record_Type_Name__c ='Fast Lane Opportunity' and id=:m2mOrder.Opportunity__c];
        if (!oppList.isEmpty())
        {
            No_Trigger__c notrigger = null;
	    	boolean noTriggerUser = false;
			// Set trigger to inactive
			List<No_Trigger__c> noList = [Select Id, SetupOwnerId, Flag__c from No_Trigger__c where SetupOwnerId =: UserInfo.getUserId() limit 1];
			if (noList.isEmpty()) {
				noTriggerUser=true;
				notrigger = new No_Trigger__c();
				notrigger.Flag__c = true;
				notrigger.SetupOwnerId = UserInfo.getUserId();
				upsert notrigger;
			} else {
				noList[0].Flag__c = true;
				upsert noList[0];
				notrigger = noList[0];
			}
            
            
            Opportunity opp = oppList[0];
            opp.StageName='Closed Won';
            opp.Win_Reason__c='Other';
            opp.Other_Reason__c='Fast Lane Opportunity';
            update opp;
            
            
            notrigger.Flag__c = false;
			upsert notrigger; 
			if (noTriggerUser) {
				delete notrigger;
			}
        }
    }

    public void saveRecord() {
        try
        {
            update this.CurrentBoniCheck;
        }
        catch(DMLException ex)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getDmlMessage(0)));
        }
    }

    public void decline() {
        string tmpStatus = this.CurrentBoniCheck.Status_Of_CC__c;
        try
        {
            this.CurrentBoniCheck.Status_Of_CC__c = BONICHECK_STATUS_DECLINED;
			this.CurrentBoniCheck.Boni_Check_status_history__c = this.CurrentBoniCheck.Boni_Check_status_history__c != null && this.CurrentBoniCheck.Boni_Check_status_history__c.length() == 0 ? this.CurrentBoniCheck.Status_Of_CC__c + ' ' + Datetime.now() : this.CurrentBoniCheck.Boni_Check_status_history__c + '|' + this.CurrentBoniCheck.Status_Of_CC__c + ' ' + Datetime.now();
            this.CurrentBoniCheck.Boni_Check_status_history__c = this.CurrentBoniCheck.Boni_Check_status_history__c.replace('null|', '');
			update this.CurrentBoniCheck;
        }
        catch(DMLException ex)
        {
            this.CurrentBoniCheck.Status_Of_CC__c = tmpStatus;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getDmlMessage(0)));
        }
    }

    public void declineBankGuarantee() {
        string tmpStatus = this.CurrentBoniCheck.Status_Of_CC__c;
        try
        {
            this.CurrentBoniCheck.Status_Of_CC__c = BONICHECK_STATUS_INPROGRESS_DECLINED_BANK_GUARANTEE;
			this.CurrentBoniCheck.Boni_Check_status_history__c = this.CurrentBoniCheck.Boni_Check_status_history__c != null && this.CurrentBoniCheck.Boni_Check_status_history__c.length() == 0 ? this.CurrentBoniCheck.Status_Of_CC__c + ' ' + Datetime.now() : this.CurrentBoniCheck.Boni_Check_status_history__c + '|' + this.CurrentBoniCheck.Status_Of_CC__c + ' ' + Datetime.now();
            this.CurrentBoniCheck.Boni_Check_status_history__c = this.CurrentBoniCheck.Boni_Check_status_history__c.replace('null|', '');
			update this.CurrentBoniCheck;
        }
        catch(DMLException ex)
        {
            this.CurrentBoniCheck.Status_Of_CC__c = tmpStatus;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getDmlMessage(0)));
        }
    }

    public void requestBankGuarantee() {
        string tmpStatus = this.CurrentBoniCheck.Status_Of_CC__c;
        try
        {
            this.CurrentBoniCheck.Status_Of_CC__c = BONICHECK_STATUS_INPROGRESS_BANK_GUARANTEE_NEEDED;
			this.CurrentBoniCheck.Boni_Check_status_history__c = this.CurrentBoniCheck.Boni_Check_status_history__c != null && this.CurrentBoniCheck.Boni_Check_status_history__c.length() == 0 ? this.CurrentBoniCheck.Status_Of_CC__c + ' ' + Datetime.now() : this.CurrentBoniCheck.Boni_Check_status_history__c + '|' + this.CurrentBoniCheck.Status_Of_CC__c + ' ' + Datetime.now();
            this.CurrentBoniCheck.Boni_Check_status_history__c = this.CurrentBoniCheck.Boni_Check_status_history__c.replace('null|', '');
			update this.CurrentBoniCheck;
        }
        catch(DMLException ex)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getDmlMessage(0)));
        }
    }

    public void addBankGuarantee() {
        showPopup();
    }

     public void addAttachment() {
        addAttachment = true;
        showPopup();
    }

    public void declineRiskPaper() {
        try
        {
            this.CurrentBoniCheck.Status_Of_CC__c = BONICHECK_STATUS_INPROGRESS_DECLINED_RISK_PAPER;
			this.CurrentBoniCheck.Boni_Check_status_history__c = this.CurrentBoniCheck.Boni_Check_status_history__c != null && this.CurrentBoniCheck.Boni_Check_status_history__c.length() == 0 ? this.CurrentBoniCheck.Status_Of_CC__c + ' ' + Datetime.now() : this.CurrentBoniCheck.Boni_Check_status_history__c + '|' + this.CurrentBoniCheck.Status_Of_CC__c + ' ' + Datetime.now();
            this.CurrentBoniCheck.Boni_Check_status_history__c = this.CurrentBoniCheck.Boni_Check_status_history__c.replace('null|', '');
			update this.CurrentBoniCheck;
        }
        catch(DMLException ex)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getDmlMessage(0)));
        }
    }

    public void requestRiskPaper() {
        try
        {
            this.CurrentBoniCheck.Status_Of_CC__c = BONICHECK_STATUS_INPROGRESS_RISK_PAPER_NEEDED;
			this.CurrentBoniCheck.Boni_Check_status_history__c = this.CurrentBoniCheck.Boni_Check_status_history__c != null && this.CurrentBoniCheck.Boni_Check_status_history__c.length() == 0 ? this.CurrentBoniCheck.Status_Of_CC__c + ' ' + Datetime.now() : this.CurrentBoniCheck.Boni_Check_status_history__c + '|' + this.CurrentBoniCheck.Status_Of_CC__c + ' ' + Datetime.now();
            this.CurrentBoniCheck.Boni_Check_status_history__c = this.CurrentBoniCheck.Boni_Check_status_history__c.replace('null|', '');
			update this.CurrentBoniCheck;
        }
        catch(DMLException ex)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getDmlMessage(0)));
        }
    }

    private boolean isSalesPersonUser() {
        if (debugEnabled && debugMode == 'Sales User') {
            return true;
         }
         else if((getUserData().Profile.Name =='M2M Standard User' /* && getUserData().UserRole.Name != 'Head Of Sales' zz test boni check bug*/)
            || getUserData().Profile.Name.indexOf('System Administrator') >= 0 || getUserData().Profile.Name == 'M2M Sales Support')
   //     } else if(getUserData().Profile.Name =='System Administrator' && getUserData().UserRole.Name != 'Head Of Sales')
        {
            return true;
        }
        else
            return false;
    }

    private boolean isSalesPersonAndOwnerUser() {
        if (debugEnabled && debugMode == 'Sales User') {
            return true;
        }
        else if(getUserData().Profile.Name =='M2M Standard User' || getUserData().Profile.Name.indexOf('System Administrator') >= 0 || getUserData().Profile.Name == 'M2M Sales Support')
        //} else if(getUserData().Profile.Name =='System Administrator' && getUserData().UserRole.Name != 'Head Of Sales')
        {
            return true;
        }
        else
            return false;
    }

    private boolean isSalesHeadUser() {
        if (debugEnabled && debugMode == 'Sales Head User') {
            return true;
        } else if(getUserData().UserRole.Name == 'Head Of Sales' || getUserData().Profile.Name.indexOf('System Administrator') >= 0)
        {
            //system.debug(getUserData().UserRole.Name);
            return true;
        }
        else
            return false;
    }

    public boolean getIsBoniUser() {
        if (debugEnabled && debugMode == 'Boni User') {
            return true;
        } /*else if(getUserData().UserRole.Name == 'Head Of Credit Check')//*/
        else if(getUserData().Profile.Name == 'M2M Credit Check User' || getUserData().Profile.Name.indexOf('System Administrator') >= 0)
        {
            return true;
        }
        else
            return false;
    }

    User userData = null;

    private User getUserData() {
        if (userData == null) {
            userData = [select Id, Name, Profile.Name, UserRole.Name
                        from User where Id = :UserInfo.getUserId()];
        }

        return userData;
    }

    public class BoniCheckControllerException extends Exception {}

    /**                  DEBUG MODE                  **/
    public boolean DebugEnabled {
        get;set;
    }
    public string DebugMode { get; set; }

    public SelectOption[] getDebugModes() {
        return new SelectOption[] {new SelectOption('', '-- None --'), new SelectOption('Sales User', 'Sales User'), new SelectOption('Boni User', 'Boni User'), new SelectOption('Sales Head User', 'Sales Head User')};
    }

    public PageReference testAction() {
        return ApexPages.currentPAge();
    }

    public Attachment attachment {
        get {
            if (attachment == null) attachment = new Attachment();
            return attachment;
        }

        set;
    }

    public PageReference uploadFile()
    {
        if(insertAttachment())
        {
            if(!addAttachment)
            {
				Boolean updateStatusHistory = false;
				// this is because in this use case status will change and that is why we need to update the status history field
				if (this.CurrentBoniCheck.Status_Of_CC__c == BONICHECK_STATUS_INPROGRESS_RISK_PAPER_NEEDED || this.CurrentBoniCheck.Status_Of_CC__c == BONICHECK_STATUS_INPROGRESS_BANK_GUARANTEE_NEEDED)
				{
					updateStatusHistory = true;
				}

                this.CurrentBoniCheck.Status_Of_CC__c = this.CurrentBoniCheck.Status_Of_CC__c == BONICHECK_STATUS_INPROGRESS_RISK_PAPER_NEEDED ? BONICHECK_STATUS_INPROGRESS : this.CurrentBoniCheck.Status_Of_CC__c;
                this.CurrentBoniCheck.Status_Of_CC__c = this.CurrentBoniCheck.Status_Of_CC__c == BONICHECK_STATUS_INPROGRESS_BANK_GUARANTEE_NEEDED ? BONICHECK_STATUS_INPROGRESS : this.CurrentBoniCheck.Status_Of_CC__c;
                
				if (updateStatusHistory)
				{
					this.CurrentBoniCheck.Boni_Check_status_history__c = this.CurrentBoniCheck.Boni_Check_status_history__c != null && this.CurrentBoniCheck.Boni_Check_status_history__c.length() == 0 ? this.CurrentBoniCheck.Status_Of_CC__c + ' ' + Datetime.now() : this.CurrentBoniCheck.Boni_Check_status_history__c + '|' + this.CurrentBoniCheck.Status_Of_CC__c + ' ' + Datetime.now();
					this.CurrentBoniCheck.Boni_Check_status_history__c = this.CurrentBoniCheck.Boni_Check_status_history__c.replace('null|', '');
				}
				
				update this.CurrentBoniCheck;
            }
            else
                addAttachment = false;

        }
        return null;
    }



    private boolean insertAttachment()
    {
        boolean succeeded = false;

         if(attachment.Body != null && attachment.Name != null)
        {
          attachment.ParentId = sControllerObject.Id;
          try
          {
            insert attachment;
            succeeded = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'File successfully uploaded'));
          }
          catch (DMLException e)
          {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading file'));
          }
          finally
          {
            closePopup();
            attachment.body = null; // clears the viewstate
            attachment = new Attachment();
          }
        }
        return succeeded;
    }
}

/*
 try
 {
    insert document;
 }
 catch (DMLException e)
 {
    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading file'));
    return null;
}
finally
{
    document.body = null; // clears the viewstate
    document = new Document();
}

*/