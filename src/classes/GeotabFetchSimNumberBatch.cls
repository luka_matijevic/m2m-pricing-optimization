/*
	P. Matkovic
	For each GEOTAB device make callout to fetch ICCID number from GEOTAB
*/
global class GeotabFetchSimNumberBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
	
	String query;
	String deviceSerialNumber;
	String orderRequestId;
	String accountId;
	GEOTAB_Settings__c geotabSettings = GEOTAB_Settings__c.getInstance();
	String userInfoEndPoint = geotabSettings.UserInfoEndPoint__c;
	String deviceOrderEntriesEndPoint = geotabSettings.DeviceOrderEntriesEntPoint__c;
	String lookupDeviceEndPoint = geotabSettings.LookupDeviceEndPoint__c;
	Configurable_Sixt_helper__c csh = null;
	Map<String, GeotabUtils.AuthenticationResponseClass> authorizationResultMap = null;
	Map<String, Id> stockICCIDAccount = new Map<String, Id>();
	Map<String, Id> stockICCIDOrderRequest = new Map<String, Id>();
	Map<String, String> stockICCIDSerialDeviceNumber = new Map<String, String>();
	Map<String, GeotabUtils.InstallLogsClass> geotabDeviceObject = null;
	List<Geotab_orders_log__c> logList = new List<Geotab_orders_log__c>();
	global GeotabFetchSimNumberBatch(String deviceIn, String orderRequestIdIn, String accountIdIn) {
		this.deviceSerialNumber = deviceIn;
		this.orderRequestId = orderRequestIdIn;
		this.accountId = accountIdIn;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		List<Configurable_Sixt_helper__c> customConfig = [SELECT Id, Name,  Geotab_username__c, Geotab_password__c, Sixt_frame_contract__c, Sixt_account__c, Sixt_billing_account__c, Sixt_Opportunity_RecordType__c FROM Configurable_Sixt_helper__c WHERE Name = 'Configurable records' ORDER BY CreatedDate ASC];
		System.debug(customConfig);
		if (customConfig != null && customConfig.size() > 0)
		{
			csh = customConfig[0];
		}
		
		if (csh != null)
		{
			userInfoEndPoint = userInfoEndPoint + csh.Geotab_username__c + '&password=' + csh.Geotab_password__c;
		}

		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		if (!String.isEmpty(deviceSerialNumber) && !String.isEmpty(orderRequestId) && !String.isEmpty(accountId)){
			HttpResponse resp = CaseUtils.executeRequest(userInfoEndPoint, 'GET', null, 32);
			if(resp != null && resp.getStatusCode() == 200) {
				authorizationResultMap = (Map<String, GeotabUtils.AuthenticationResponseClass>)JSON.deserialize(resp.getBody(), Map<String, GeotabUtils.AuthenticationResponseClass>.class);
				if (authorizationResultMap != null){
					String userIdResult, sessionIdResult;
					List<GeotabUtils.GeotabAccount> geotabAccounts = null;

					userIdResult = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').userId : null;
					sessionIdResult = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').sessionId : null;
					geotabAccounts = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').accounts : null;
					
					lookupDeviceEndPoint = lookupDeviceEndPoint + 'apiKey="' + userIdResult + '"&serialNo="' + deviceSerialNumber + '"&sessionId="' + 
										sessionIdResult + '"';

					resp = CaseUtils.executeRequest(lookupDeviceEndPoint, 'GET', null, 32);
					if(resp != null && resp.getStatusCode() == 200) {
						geotabDeviceObject = (Map<String, GeotabUtils.InstallLogsClass>)JSON.deserialize(resp.getBody(), Map<String, GeotabUtils.InstallLogsClass>.class);
						
						// merge account to device's SIM ICCID
						// attach order request to device's ICCID
						if(geotabDeviceObject != null) {
							GeotabUtils.InstallLogsClass resultValue = geotabDeviceObject.get('result') != null ? geotabDeviceObject.get('result') : null;
							System.debug(resultValue);
							if(resultValue != null) {
								stockICCIDAccount.put(resultValue.simNumber, accountId);
								stockICCIDOrderRequest.put(resultValue.simNumber, orderRequestId);
								stockICCIDSerialDeviceNumber.put(resultValue.simNumber, String.valueOf(resultValue.request.device.serialNumber));
								createGeotabOrdersLog(orderRequestId, resp.getBody(), String.valueOf(resp.getStatusCode()), userInfoEndPoint, 'Device found!');
							}
							else{
								createGeotabOrdersLog(orderRequestId, resp.getBody(), String.valueOf(resp.getStatusCode()), userInfoEndPoint, 'Device not found!');
							}
						}
					} else {
						createGeotabOrdersLog(orderRequestId, resp.getBody(), String.valueOf(resp.getStatusCode()), userInfoEndPoint, 'Device information fetch failed!');
					}

				}else{
					createGeotabOrdersLog(orderRequestId, resp.getBody(), String.valueOf(resp.getStatusCode()), userInfoEndPoint, 'authorizationResultMap is null!');
				}
			}
			else{
				createGeotabOrdersLog(orderRequestId, resp.getBody(), String.valueOf(resp.getStatusCode()), userInfoEndPoint, 'User authorization failed!');
			}
		}
		else{
			String descriptionTmp = 'Insufficient information: device serial number: ' + deviceSerialNumber;
			descriptionTmp = descriptionTmp + ', orderRequestId: ' + orderRequestId;
			descriptionTmp = descriptionTmp + ', accountId: ' + accountId;
			createGeotabOrdersLog(orderRequestId, '', '', '', descriptionTmp);
		}
	}
	
	global void finish(Database.BatchableContext BC) {

		if (logList != null){
			insert logList;
		}

		if(stockICCIDAccount != null && stockICCIDAccount.size() > 0) {
			List<Stock__c> stocksToUpdate = [SELECT Id, ICCID__c, Account__c, Order_Request__c FROM Stock__c WHERE ICCID__c in :stockICCIDAccount.keySet()];

			if(stocksToUpdate != null && stocksToUpdate.size() > 0) {
				for(Stock__c item : stocksToUpdate) {
					item.Order_Request__c = stockICCIDOrderRequest.get(item.ICCID__c);
					item.Account__c = stockICCIDAccount.get(item.ICCID__c);
					item.GEOTAB_Stock__c = true;
					item.GEOTAB_Device_Serial_Number__c = stockICCIDSerialDeviceNumber.get(item.ICCID__c);
				}
				update stocksToUpdate;
			}
		}
		
	}

	private void createGeotabOrdersLog(String orderRequestIdIn, String responseMessageIn, String responseStatusCodeIn, String requestIn, String description){
		if (logList == null){
			logList = new List<Geotab_orders_log__c>();
		}

		Geotab_orders_log__c gol = new Geotab_orders_log__c();
		gol.Order_Request__c = orderRequestIdIn;
		gol.Response_Status_Code__c = responseStatusCodeIn;
		gol.Original_response__c = responseMessageIn;
		gol.Request__c = requestIn;
		gol.Description__c = 'GEOTAB ICCID Fetching - ' + description;

		logList.add(gol);
	}
}