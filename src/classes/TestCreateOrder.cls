@isTest
private class TestCreateOrder
{
	//--------------------------------------------------------------------------
	// Create order with journal mappings
	//--------------------------------------------------------------------------
    static testMethod void createOrderFromOrderRequest()
    {
    	//
        // Create test order request
    	//
    	No_Trigger__c notriggers = new No_Trigger__c();
    	notriggers.Flag__c = true;
    	insert notriggers;

    	TestHelper.createOrderRequest();
    	TestHelper.createJournalMappings();

    	notriggers.Flag__c = false;
    	update notriggers;

    	Order_Request__c orderRequest =
    			[ SELECT Id, Name, Frame_Contract__r.Opportunity__r.AccountId
    			  FROM Order_Request__c
    			  LIMIT 1 ];

    	Test.startTest();
    	CreateOrder createOrder = new CreateOrder(orderRequest.Id, new InsertHelper());
    	Id orderId = createOrder.create();
    	Test.stopTest();
    	//
    	// Tests
    	//
    	Journal_Mapping__c journalMapping =
    			[ SELECT Id
    			  FROM Journal_Mapping__c
    			  WHERE Name = 'M2M Hardware'
    			  AND Country__c = 'Non-DE' ];
    	csord__Order__c order =
    			[ SELECT Id, Name, Order_Request__c, csord__Account__c
    			  FROM csord__Order__c ];
    	csord__Order_Line_Item__c[] orderLineItems =
    			[ SELECT Id, Name, csord__Is_Recurring__c, csord__Line_Description__c,
    			  csord__Total_Price__c, Quantity__c, Journal_Mapping__c, Billing_Rate_Model__c
    			  FROM csord__Order_Line_Item__c
    			  WHERE csord__Order__c = :order.Id ];
    	//
    	// Order
    	//
    	System.assertEquals('Order for ' + orderRequest.Name, order.Name);
    	System.assertEquals(orderRequest.Frame_Contract__r.Opportunity__r.AccountId, order.csord__Account__c);
    	System.assertEquals(orderRequest.Id, order.Order_Request__c);
    	//
    	// Order line items
    	//
    	System.assertEquals(2, orderLineItems.size());
    	System.assertEquals('One-Time Target Fee Total', orderLineItems[0].Name);
    	System.assertEquals(false, orderLineItems[0].csord__Is_Recurring__c);
    	System.assertEquals(44500, orderLineItems[0].csord__Total_Price__c);
    	System.assertEquals(journalMapping.Id, orderLineItems[0].Journal_Mapping__c);
    	System.assertEquals(50, orderLineItems[0].Quantity__c);
    	System.assertEquals('Rate Payment', orderLineItems[0].Billing_Rate_Model__c);
    	System.assertEquals('SIM One-Time Target Fee Total', orderLineItems[1].Name);
    	System.assertEquals(false, orderLineItems[1].csord__Is_Recurring__c);
    	System.assertEquals(150, orderLineItems[1].csord__Total_Price__c);
    	System.assertEquals(journalMapping.Id, orderLineItems[1].Journal_Mapping__c);
    	System.assertEquals(100, orderLineItems[1].Quantity__c);
    	System.assertEquals('One Off', orderLineItems[1].Billing_Rate_Model__c);
    }

    //--------------------------------------------------------------------------
    // Craete order without journal mappings
    //--------------------------------------------------------------------------
    static testMethod void createOrderWithoutJournalMappings()
    {
    	//
    	// Create test data
    	//
    	No_Trigger__c notriggers = new No_Trigger__c();
    	notriggers.Flag__c = true;
    	insert notriggers;

    	TestHelper.createOrderRequest();

    	notriggers.Flag__c = false;
    	update notriggers;

    	Order_Request__c orderRequest =
    			[ SELECT Id, Name, Frame_Contract__r.Opportunity__r.AccountId
    			  FROM Order_Request__c
    			  LIMIT 1 ];
    	Test.startTest();
    	try
    	{
	    	CreateOrder createOrder = new CreateOrder(orderRequest.Id, new InsertHelper());
	    	Id orderId = createOrder.create(); // Should throw exception
	    	System.assert(false);
    	}
    	catch (JournalMappingException e) {}
    	Test.stopTest();
    	//
    	// Tests
    	//
    	//
    	// There shouldn't be any orders
    	//
    	csord__Order__c[] orders =
    			[ SELECT Id FROM csord__Order__c];
    	System.assertEquals(0, orders.size());
    }

    //--------------------------------------------------------------------------
    // Create order twice
    //--------------------------------------------------------------------------
    static testMethod void createOrderTwice()
    {
    	//
    	// Create test data
    	//
    	No_Trigger__c notriggers = new No_Trigger__c();
    	notriggers.Flag__c = true;
    	insert notriggers;

    	TestHelper.createOrderRequest();
    	TestHelper.createJournalMappings();

    	notriggers.Flag__c = false;
    	update notriggers;

    	Order_Request__c orderRequest =
				[ SELECT Id, Name, Frame_Contract__r.Opportunity__r.AccountId
				  FROM Order_Request__c
				  LIMIT 1 ];

		Test.startTest();
		try
		{
			CreateOrder createOrder = new CreateOrder(orderRequest.Id, new InsertHelper());
			Id orderId = createOrder.create();
			orderId = createOrder.create(); // Should throw exception
			System.assert(false);
		}
		catch (CreateOrder.OrderException e) {}
		Test.stopTest();
		System.assert(true);
    }

	//--------------------------------------------------------------------------
	// Create order with journal mappings and without rate model
	//--------------------------------------------------------------------------
    static testMethod void createOrderFromOrderRequestWithouRateModel()
    {
    	//
        // Create test order request
    	//
    	No_Trigger__c notriggers = new No_Trigger__c();
    	notriggers.Flag__c = true;
    	insert notriggers;

    	TestHelper.createOrderRequest();
    	TestHelper.createJournalMappings();

    	//
    	// Clear billing rate model
    	//
    	cscfga__Product_Configuration__c[] configs =
    			[ SELECT Billing_Rate_Model__c
    			  FROM cscfga__Product_Configuration__c ];
    	for (cscfga__Product_Configuration__c config : configs)
    	{
    		config.Billing_Rate_Model__c = null;
    	}
    	update configs;

    	notriggers.Flag__c = false;
    	update notriggers;

    	Order_Request__c orderRequest =
    			[ SELECT Id, Name, Frame_Contract__r.Opportunity__r.AccountId
    			  FROM Order_Request__c
    			  LIMIT 1 ];

    	Test.startTest();
    	CreateOrder createOrder = new CreateOrder(orderRequest.Id, new InsertHelper());
    	Id orderId = createOrder.create();
    	Test.stopTest();
    	//
    	// Tests
    	//
    	Journal_Mapping__c journalMapping =
    			[ SELECT Id
    			  FROM Journal_Mapping__c
    			  WHERE Name = 'M2M Hardware'
    			  AND Country__c = 'Non-DE' ];
    	csord__Order__c order =
    			[ SELECT Id, Name, Order_Request__c, csord__Account__c
    			  FROM csord__Order__c ];
    	csord__Order_Line_Item__c[] orderLineItems =
    			[ SELECT Id, Name, csord__Is_Recurring__c, csord__Line_Description__c,
    			  csord__Total_Price__c, Quantity__c, Journal_Mapping__c, Billing_Rate_Model__c
    			  FROM csord__Order_Line_Item__c
    			  WHERE csord__Order__c = :order.Id ];
    	//
    	// Order
    	//
    	System.assertEquals('Order for ' + orderRequest.Name, order.Name);
    	System.assertEquals(orderRequest.Frame_Contract__r.Opportunity__r.AccountId, order.csord__Account__c);
    	System.assertEquals(orderRequest.Id, order.Order_Request__c);
    	//
    	// Order line items
    	//
    	System.assertEquals(2, orderLineItems.size());
    	System.assertEquals('One-Time Target Fee Total', orderLineItems[0].Name);
    	System.assertEquals(false, orderLineItems[0].csord__Is_Recurring__c);
    	System.assertEquals(44500, orderLineItems[0].csord__Total_Price__c);
    	System.assertEquals(journalMapping.Id, orderLineItems[0].Journal_Mapping__c);
    	System.assertEquals(50, orderLineItems[0].Quantity__c);
    	System.assertEquals('One Off', orderLineItems[0].Billing_Rate_Model__c);
    	System.assertEquals('SIM One-Time Target Fee Total', orderLineItems[1].Name);
    	System.assertEquals(false, orderLineItems[1].csord__Is_Recurring__c);
    	System.assertEquals(150, orderLineItems[1].csord__Total_Price__c);
    	System.assertEquals(journalMapping.Id, orderLineItems[1].Journal_Mapping__c);
    	System.assertEquals(100, orderLineItems[1].Quantity__c);
    	System.assertEquals('One Off', orderLineItems[1].Billing_Rate_Model__c);
    }

}