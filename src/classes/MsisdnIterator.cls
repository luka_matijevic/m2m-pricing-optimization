global class MsisdnIterator implements Iterator<MSISDN__c>{
	global long rangeStart;
	global long rangeEnd;
	global long current;
	global Id rangeID;
	global MsisdnIterator(long rangeStart, long rangeEnd, Id rangeID){
		this.rangeStart = rangeStart;
		this.rangeEnd = rangeEnd;
		this.current = rangeStart;
		this.rangeID = rangeID;
	}

	global boolean hasNext(){
		return current <= rangeEnd;
	}

	global MSISDN__c next(){
		if (hasNext()){            
			MSISDN__c msisdn;
			msisdn = new MSISDN__c();
			msisdn.msisdn__c = String.valueOf(current);
			msisdn.isReadyForUse__c = true;
			msisdn.MSISDN_Range__c = rangeID;
			current++;
			return msisdn;
		}
        return null;
	}
	
	

}