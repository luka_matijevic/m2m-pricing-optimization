global class ValidationHelper {

    private static Map<String, csam_t1.ValidationResponse> validatePresence(SObject obj, List<String> mandatoryFields) {
        Map<String, csam_t1.ValidationResponse> validationErrors = new Map<String, csam_t1.ValidationResponse>();
        
        Map<String, String> fieldNameToLabelMap = new Map<String, String>();
        
        Map<String, Schema.SObjectField> fields = obj.getsObjectType().getDescribe().fields.getMap();
        for (Schema.SObjectField f : fields.values()) {
            Schema.DescribeFieldResult describe = f.getDescribe();
            fieldNameToLabelMap.put(describe.getName().toLowerCase(), describe.getLabel());
        }
        
        system.debug('**IP**' + JSON.serializePretty(fieldNameToLabelMap));
                
        for (String field : mandatoryFields) {
            String label = !String.isEmpty(fieldNameToLabelMap.get(field.toLowerCase())) ? fieldNameToLabelMap.get(field.toLowerCase()) : field;
            system.debug('**IP**' + JSON.serializePretty(fieldNameToLabelMap.get(field)));
            system.debug('**IP**' + JSON.serializePretty(label));
            try {
                Object value = obj.get(field);
                if (value == null || String.isEmpty(String.valueOf(value))) {
                    csam_t1.ValidationResponse response = new csam_t1.ValidationResponse();
                    addError(response, 'Mandatory field ' + label + ' is missing or empty.');
                    validationErrors.put(field, response);
                }
            } catch (System.SObjectException e) {
                csam_t1.ValidationResponse response = new csam_t1.ValidationResponse();
                addError(response, 'Mandatory field ' + label + ' is missing or empty.');
                validationErrors.put(field, response);
                system.debug('Field ' + label + ' was not available on ' + obj.getSObjectType());
            }
        }
        return validationErrors;
    }
    
    private static void addError(csam_t1.ValidationResponse response, String message) {
        response.valid = false;
        if (String.isEmpty(response.validationMessage)) {
            response.validationMessage = message;
        } else {
            response.validationMessage += '\n' + message;
        }
    }
    
    webservice static String validateOrderRequest(Id orderRequestId) {
        csam_t1.ValidationResponse response = new csam_t1.ValidationResponse();
        
        Order_Request__c req = [Select Id, Frame_Contract__c from Order_Request__c where Id = :orderRequestId limit 1];
        if (req == null) {
            response.valid = false;
            response.validationMessage = 'Order request is not available.';
            return response.serializeToJson();
        }
        
        List<Frame_Contract__c> frameContracts = [Select Id from Frame_Contract__c where Status__c != NULL and Id = :req.Frame_Contract__c];
        if (frameContracts == null || frameContracts.size() == 0) {
            response.valid = false;
            response.validationMessage = 'Associated Frame Contract is not in a valid status';
            return response.serializeToJson();
        }
        
        response.valid = true;
        response.validationMessage = 'Valid';
        
        return response.serializeToJson();
    }
    
    webservice static String validateProvisionAccount(Id accountId) {
        
        csam_t1.ValidationResponse response = new csam_t1.ValidationResponse();

        Account account = [
            Select 
                Id,
                Name,
                Billing_cycle__c,
                BillingCity,
                BillingCountry,
                BillingPostalCode,
                BillingStreet,
                Company_email__c,
                Currency__c,
                Language__c,
                SM2M_ID__c, 
                Spin_id__c,
                Type,
                Value_added_tax_id__c,
                Vat__c,
                Vo_number__c,
                Local__c,
                Global__c,
                SM2M_ID_Local__c,
                isProvisionedGlobal__c,
                isProvisionedLocal__c,
                (Select Id, Name, SM2M_Type__c from Billing_Accounts__r) 
            from 
                Account 
            where 
                Id =: accountId 
            limit 1
        ];
        
        List<String> mandatoryFields = new List<String>{
            'Name',
            'Billing_cycle__c',
            'BillingCity',
            'BillingCountry',
            'BillingPostalCode',
            'BillingStreet',
            'Company_email__c',
            'Currency__c',
            'Language__c',
            'Spin_id__c',
            'Type',
            'Value_added_tax_id__c',
            'Vat__c',
            'Vo_number__c'
        };
        
        Map<String, csam_t1.ValidationResponse> errors = validatePresence(account, mandatoryFields);
        
        Boolean hasErrors = false;
        
        if (!errors.isEmpty()) {
            for (csam_t1.ValidationResponse error : errors.values()) {
                addError(response, error.validationMessage);
                hasErrors = true;
            }
        }
        
        /*if (!String.isEmpty(account.SM2M_ID__c) && account.Global__c) {
            addError(response, 'Account is already provisioned as Global.');
            hasErrors = true;  
        }
        
        if(!String.isEmpty(account.SM2M_ID_Local__c) && account.Local__c) {
            addError(response, 'Account is already provisioned as Local.');
            hasErrors = true; 
        }*/
        
        if(account.isProvisionedGlobal__c && account.isProvisionedLocal__c) {
            addError(response, 'Account is already provisioned.');
            hasErrors = true; 
        }
        
        if (account.Billing_Accounts__r == null || account.billing_accounts__r.size() == 0) {
            addError(response, 'Account must have at least one billing account assigned to it.'); 
            hasErrors = true;
        }
        
        if(account.Local__c && account.Global__c) {
            Integer countLocal, countGlobal;
            countLocal = countGlobal = 0;
            for(Billing_Account__c ba : account.Billing_Accounts__r) {
                if(ba.SM2M_Type__c != null) {
                    if(ba.SM2M_Type__c.equalsIgnoreCase('Local')) {
                        countLocal += 1;
                    } else if(ba.SM2M_Type__c.equalsIgnoreCase('Global')) {
                        countGlobal += 1;
                    }
                    
                } else {
                    countLocal = countGlobal = 0;
                    addError(response, 'Account must have both local and global billing accounts.'); 
                    hasErrors = true;
                }
            }
            
            if(countLocal == 0 || countGlobal == 0) {
                addError(response, 'Account must have both local and global billing accounts.'); 
                hasErrors = true;
            }
        }
        
        if (!hasErrors) {
            response.valid = true;
            response.validationMessage = 'Valid';
        }

        return response.serializeToJson();
    }
    
    public static String validateProvisionAccountFromTrigger(Id accountId) {
        
        String response = 'VALID';

        Account account = [
            Select 
                Id,
                Name,
                Billing_cycle__c,
                BillingCity,
                BillingCountry,
                BillingPostalCode,
                BillingStreet,
                Company_email__c,
                Currency__c,
                Language__c,
                SM2M_ID__c, 
                Spin_id__c,
                Type,
                Value_added_tax_id__c,
                Vat__c,
                Vo_number__c,
                Local__c,
                Global__c,
                SM2M_ID_Local__c,
                isProvisionedGlobal__c,
                isProvisionedLocal__c,
                (Select Id, Name, SM2M_Type__c from Billing_Accounts__r) 
            from 
                Account 
            where 
                Id =: accountId 
            limit 1
        ];
        
        List<String> mandatoryFields = new List<String>{
            'Name',
            'Billing_cycle__c',
            'BillingCity',
            'BillingCountry',
            'BillingPostalCode',
            'BillingStreet',
            'Company_email__c',
            'Currency__c',
            'Language__c',
            'Spin_id__c',
            'Type',
            'Value_added_tax_id__c',
            'Vat__c',
            'Vo_number__c'
        };
        
        Map<String, csam_t1.ValidationResponse> errors = validatePresence(account, mandatoryFields);
        
        if (!errors.isEmpty()) {
            for (csam_t1.ValidationResponse error : errors.values()) {
                response += '' + error.validationMessage;
            }
        }
        
        /*if (!String.isEmpty(account.SM2M_ID__c) && account.Global__c) {
            response = 'Account is already provisioned as Global.';
        }
        
         if(!String.isEmpty(account.SM2M_ID_Local__c) && account.Local__c) {
            response = 'Account is already provisioned as Local.';
        }*/
        
        if(account.isProvisionedGlobal__c && account.isProvisionedLocal__c) {
            response = 'Account is already provisioned.';
        }
        
        if (account.Billing_Accounts__r == null || account.billing_accounts__r.size() == 0) {
            response = 'Account must have at least one billing account assigned to it.';                       
        }
        
        if(account.Local__c && account.Global__c) {
            Integer countLocal, countGlobal;
            countLocal = countGlobal = 0;
            for(Billing_Account__c ba : account.Billing_Accounts__r) {
                if(ba.SM2M_Type__c != null) {
                    if(ba.SM2M_Type__c.equalsIgnoreCase('Local')) {
                        countLocal += 1;
                    } else if(ba.SM2M_Type__c.equalsIgnoreCase('Global')) {
                        countGlobal += 1;
                    }
                    
                } else {
                    countLocal = countGlobal = 0;
                    response =  'Account must have both local and global billing accounts.';
                }
            }
            
            if(countLocal == 0 || countGlobal == 0) {
                response = 'Account must have both local and global billing accounts.';
            }
        }
        
        return response;
    }
    
    webservice static String validateThirdUploadFile(Id fileId) {
        
        csam_t1.ValidationResponse response = new csam_t1.ValidationResponse();

        response.valid = true;
        response.validationMessage = 'Valid';
        
        return response.serializeToJson();
    }
    
    webservice static String validateBICFile(Id fileId) {
        
        csam_t1.ValidationResponse response = new csam_t1.ValidationResponse();

        response.valid = true;
        response.validationMessage = 'Valid';
        
        return response.serializeToJson();
    }
}