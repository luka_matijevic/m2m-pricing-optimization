public with sharing class OpportunityLineItemTriggerHandler {

  	public static void handleBeforeInsert(OpportunityLineItem[] newOlis) {
  		
  		set<Id> attributeIds = new set<Id>();
  		
  		for (OpportunityLineItem oli : newOlis) {
  			attributeIds.add(oli.cscfga__Attribute__c);
  		}
  		
  		// Mapping of configurator values to OLI fields
  		doConfiguratorMapping(newOlis, attributeIds);
  	}
  	
  	/**
  	 *	Map values from the configurator to fields with same names as attributes.
  	 */
  	private static void doConfiguratorMapping(OpportunityLineItem[] newOlis, set<Id> lineItemAttributeIds) {
  		
  		cscfga__Attribute__c[] lineItemAtts = [select Id, cscfga__Product_Configuration__c
  												from cscfga__Attribute__c
  												where Id IN :lineItemAttributeIds];
 		set<Id> configIds = new set<Id>();
 		for (cscfga__Attribute__c att : lineItemAtts) {
 			configIds.add(att.cscfga__Product_Configuration__c);
 		}
 		
  		map<Id, cscfga__Attribute__c> allRelatedAtts = new map<Id, cscfga__Attribute__c>([
  												select Id, Name, cscfga__Value__c, cscfga__Product_Configuration__c, 
  														cscfga__Is_Line_Item__c, cscfga__Recurring__c
  												from cscfga__Attribute__c
  												where cscfga__Product_Configuration__c IN :configIds]);
  		
  		map<Id, cscfga__Attribute__c[]> configIdToAttributes = new map<Id, cscfga__Attribute__c[]>();
  		for (cscfga__Attribute__c att : allRelatedAtts.values()) {
  			if (!configIdToAttributes.containsKey(att.cscfga__Product_Configuration__c)) {
  				configIdToAttributes.put(att.cscfga__Product_Configuration__c, new cscfga__Attribute__c[] { att });
  			} else {
  				configIdToAttributes.get(att.cscfga__Product_Configuration__c).add(att);
  			}
  		}
  		
  		map<string, Schema.DescribeFieldResult> oliFieldsMap = SObjectHelper.getFieldDescriptions('OpportunityLineItem');
  		map<string, Schema.DescribeFieldResult> olidFieldLabelToDescriptionMap = new map<string, Schema.DescribeFieldResult>();
  		for (string fName : oliFieldsMap.keyset()) {
  			Schema.DescribeFieldResult fDesc = oliFieldsMap.get(fName);
  			olidFieldLabelToDescriptionMap.put(fDesc.getLabel(), fDesc);
  		}
  		
  		for (OpportunityLineItem oli : newOlis) {
  			cscfga__Attribute__c lineItemAtt = allRelatedAtts.get(oli.cscfga__Attribute__c);
  			cscfga__Attribute__c[] relatedAtts = configIdToAttributes.get(lineItemAtt.cscfga__Product_Configuration__c);
  			
  			// Mapping from the line item attribute
  			oli.Recurring__c = lineItemAtt.cscfga__Recurring__c;
  			
  			
  			// Mapping from other attributes
  			decimal targetPrice = 0;
  			string targetPriceAttName = null;
  			if (lineItemAtt.Name.contains('Target')) {
  				targetPriceAttName = lineItemAtt.Name.replace(' Total', '').trim();
  			}
  			
  			decimal basePrice = 0;
  			string basePriceAttName = null;
  			if (lineItemAtt.Name.contains('Target')) {
  				basePriceAttName = lineItemAtt.Name.replace(' Total', '').replace('Target', 'Base').trim();
  			}
  			
  			system.debug('>1>' + lineItemAtt.Name + ', ' + targetPriceAttName + ', ' + basePriceAttName);
  			
  			for (cscfga__Attribute__c att : relatedAtts) {
  				if (att.cscfga__Is_Line_Item__c == false && olidFieldLabelToDescriptionMap.containsKey(att.Name) && att.cscfga__Value__c != null && att.cscfga__Value__c != '') {
  					Schema.DescribeFieldResult fDesc = olidFieldLabelToDescriptionMap.get(att.Name);
  					Schema.DisplayType fType = fDesc.getType();
  					string fName = fDesc.getName();
  					
  					if (fType == Schema.DisplayType.Double || fType == Schema.DisplayType.Currency || fType == Schema.DisplayType.Percent) {
  						oli.put(fName, decimal.valueOf(att.cscfga__Value__c));
  					} else if (fType == Schema.DisplayType.Integer) {
  						oli.put(fName, integer.valueOf(att.cscfga__Value__c));
  					} else {
  						oli.put(fName, att.cscfga__Value__c);
  					}  					
  				} else if (att.Name == targetPriceAttName && att.cscfga__Value__c != null && att.cscfga__Value__c != '') {
  					targetPrice = decimal.valueOf(att.cscfga__Value__c);
  				} else if (att.Name == basePriceAttName && att.cscfga__Value__c != null && att.cscfga__Value__c != '') {
  					basePrice = decimal.valueOf(att.cscfga__Value__c);
  				}
  			}
  			
  			if (targetPrice != 0) {
	  			oli.UnitPrice = targetPrice;
	  			oli.Target_Price__c = targetPrice;
	  			
	  			if (basePrice != null && basePrice != 0) {
		  			oli.Base_Price__c = basePrice;
		  			oli.Discount__c = 100.0 - (oli.Target_Price__c / oli.Base_Price__c) * 100.0;
	  			}
  			}
  		}
  	}
  	
}