@isTest
public class CSVTest {
	
	static testMethod void testOfiZeroDecimalRounding() {
		Decimal num = 0.00023;
		system.assertEquals('0,00', CSV.ofi(num));
		
		Decimal zero = 0;
		system.assertEquals('0,00', CSV.ofi(zero));
	}
}