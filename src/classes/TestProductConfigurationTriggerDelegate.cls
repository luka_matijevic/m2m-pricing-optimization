@isTest
private class TestProductConfigurationTriggerDelegate {

		static testMethod void testOriginalProductNameMapping() {
			cscfga__Product_Definition__c productDefinition = new cscfga__Product_Definition__c(
				Name = 'Connectivity',
				cscfga__Description__c = 'Test description'
			);

			insert productDefinition;

			cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c(
				cscfga__Product_Definition__c = productDefinition.Id
			);

			insert productConfiguration;

			cscfga__Product_Configuration__c insertedProductConfiguration = [
				SELECT Original_Product_Name__c
				FROM cscfga__Product_Configuration__c
				WHERE Id = :productConfiguration.Id	];

			System.assertEquals(productDefinition.Name, insertedProductConfiguration.Original_Product_Name__c);

		}


		static testMethod void testOfferLookupMapping()
		{
			cscfga__Product_Definition__c productDefinition = new cscfga__Product_Definition__c(
					Name = 'Connectivity',
					cscfga__Description__c = 'Test description');

			insert productDefinition;

			cscfga__Configuration_Offer__c offer = new cscfga__Configuration_Offer__c(
					Name = 'Test Offer');
			insert offer;

			cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c(
				cscfga__Product_Definition__c = productDefinition.Id,
				cscfga__Configuration_Offer__c = offer.Id);

			insert productConfiguration;

			cscfga__Product_Configuration__c insertedProductConfiguration =
					[ SELECT From_Offer__c
					  FROM cscfga__Product_Configuration__c
					  WHERE Id = :productConfiguration.Id ];

			System.assertEquals(offer.Id, insertedProductConfiguration.From_Offer__c);
		}


		static testMethod void testOfferNameMapping()
		{
			cscfga__Product_Definition__c productDefinition = new cscfga__Product_Definition__c(
					Name = 'Connectivity',
					cscfga__Description__c = 'Test description');

			insert productDefinition;

			Account acc = new Account(
					Name = 'Test Account',
					Type = 'Other');

			insert acc;

			Opportunity opp = new Opportunity(
					Name = 'Test Opportunity',
					StageName = 'Prospecting',
					CloseDate = Datetime.now().Date(),
					Description = 'Test description 20 characters');

			insert opp;

			cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(
					Name = 'Test Product Bundle',
					cscfga__Opportunity__c = opp.Id);
			insert productBundle;

			cscfga__Configuration_Offer__c offer = new cscfga__Configuration_Offer__c(
					Name = 'Test Offer');
			insert offer;

			cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c(
				cscfga__Product_Definition__c = productDefinition.Id,
				From_Offer__c = offer.Id,
				cscfga__Product_Bundle__c = productBundle.Id);

			insert productConfiguration;

			cscfga__Product_Configuration__c insertedProductConfiguration =
					[ SELECT Offer_Name__c
					  FROM cscfga__Product_Configuration__c
					  WHERE Id = :productConfiguration.Id ];

			System.assertEquals(offer.Name, insertedProductConfiguration.Offer_Name__c);
		}

		//----------------------------------------------------------------------
		// We are persisting billing rate model from offer in product configuration
		// in case that offer gets deleted.
		//----------------------------------------------------------------------
		static testMethod void testRateModelMapping()
		{
			cscfga__Product_Definition__c productDefinition = new cscfga__Product_Definition__c(
					Name = 'Connectivity',
					cscfga__Description__c = 'Test description');

			insert productDefinition;

			Account acc = new Account(
					Name = 'Test Account',
					Type = 'Other');

			insert acc;

			Opportunity opp = new Opportunity(
					Name = 'Test Opportunity',
					StageName = 'Prospecting',
					CloseDate = Datetime.now().Date(),
					Description = 'Test description 20 characters');

			insert opp;

			cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(
					Name = 'Test Product Bundle',
					cscfga__Opportunity__c = opp.Id);
			insert productBundle;

			cscfga__Configuration_Offer__c offer = new cscfga__Configuration_Offer__c(
					Name = 'Test Offer',
					Billing_Rate_Model__c = 'Rate Payment');
			insert offer;

			cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c(
				cscfga__Product_Definition__c = productDefinition.Id,
				From_Offer__c = offer.Id,
				cscfga__Product_Bundle__c = productBundle.Id);

			insert productConfiguration;

			cscfga__Product_Configuration__c insertedProductConfiguration =
					[ SELECT Billing_Rate_Model__c
					  FROM cscfga__Product_Configuration__c
					  WHERE Id = :productConfiguration.Id ];

			System.assertEquals('Rate Payment', insertedProductConfiguration.Billing_Rate_Model__c);
		}
}