/**
 * This class is used as a trigger handler delegate which handles all the triggering events related to
 * Case logic
 * */
public with sharing class CasesTriggerHandler extends TriggerHandler.DelegateBase {
    static final String UDO_QUEUE_GLOBALSIM = 'CS_GLOBALSIM';
    static final String UDO_QUEUE_SM2MS = 'CS_SM2MS';
    static final String ACCOUNT_LOCAL = 'LOCAL';
    static final String ACCOUNT_GLOBAL = 'GLOBAL';
    static final String ACCOUNT_HYBRID = 'HYBRID';



    List<Id> contactIds = new List<Id>();
    List<Id> acctIds = new List<Id>();
    List <EntitlementContact> entlContacts;
    List <Entitlement> entlsAccount;
    List <Entitlement> entlsContact;
    List <RecordType> recTypeStandard;
    List <RecordType> recTypeSixt;
    Id userDoingChanges = UserInfo.getUserId();
    MEAlertAddress__c slaAddresses;
    String toManager;
    String toServiceManager;
    Map<Id, Account> accountsMap = new Map<Id, Account>();
    List<Account> accountsList = new List <Account>();

    List<Contact> contactsList = new List <Contact>();
    Map<String, Contact> contactsMap = new Map <String, Contact>();
    
    Map<Id, Id> conToAccMap = new Map<Id, Id>();
    
    Map<Id, Group> secondThirdLevelQueues = new Map<Id, Group>();
    Id firstLevelQId, secondLevelQId;
    Map<Id, Entitlement> accToEntMap = new Map<Id, Entitlement>();

    String dhlCaseRecordType = Label.DHL_Case_Record_Type;
    public override void prepareBefore()
    {
        for(Case caseTmp : (List<Case>)Trigger.new){
            System.debug(caseTmp);
            if (caseTmp.RecordTypeId == dhlCaseRecordType){
                System.debug(caseTmp);
            }
        }
        
        list<Profile> profileLst = new list<Profile>([select id, name from profile where name ='System Administrator']);
        if(profileLst.size() > 0){
            loggedInUserProfileInfo = profileLst[0];
        }
        
        list<User> usrLst = new list<User>([select id, Name,userName,ProfileID, Allow_To_Use_UDO_Funtionolities__c  from User where id=:userInfo.getUserId() ]);
        if(usrLst.size() > 0){
              loggedInUserInfo = usrLst[0];
        }
        
        recTypeStandard = [SELECT Id FROM RecordType where Name = 'Standard Case']; //need to correct, RecordType can be fetched from cache
        recTypeSixt = [SELECT Id FROM RecordType where Name = 'SIXT' AND SobjectType = 'Case']; //need to correct, RecordType can be fetched from cache (Adam wrote just for testing)

        slaAddresses = MEAlertAddress__c.getValues('SLA');
        if (slaAddresses != null)
        {
            toManager = (String)slaAddresses.ManagerAddress__c;
            toServiceManager = (String)slaAddresses.ServiceManagerAddress__c;
        }


        if (Trigger.isUpdate || Trigger.isInsert)
        {
            for (Case c : (List<Case>)Trigger.new)
            {
                if (c.EntitlementId == null && c.AccountId != null)
                {
                    //contactIds.add(c.ContactId);
                    acctIds.add(c.AccountId);
                }
                
                if(c.ContactId != null)
                {
                    contactIds.add(c.ContactId);
                }
            }

            if(!acctIds.isEmpty())
            {
                entlsAccount = [Select e.StartDate, e.Id, e.EndDate, e.AccountId, e.AssetId, e.SlaProcessId, e.SlaProcess.Name
                                                From Entitlement e
                                                Where e.AccountId in :acctIds And e.EndDate >= Today And e.StartDate <= Today];

                for(Entitlement e : entlsAccount)
                {
                    accToEntMap.put(e.AccountId, e);
                }
                accountsList = [Select id, Name from Account where id IN : acctIds];
                for (Account acc : accountsList)
                {
                    accountsMap.put(acc.Id, acc);
                }
            }
            
            contactsList = [SELECT c.Id, c.Email, c.AccountId from Contact c where c.Id IN :contactIds];
            
            for (Contact cont : contactsList)
            {
                contactsMap.put(cont.Email, cont);
                conToAccMap.put(cont.Id, cont.AccountId);
            }
        
            entlsContact = [Select e.StartDate, e.Id, e.EndDate, e.AccountId, e.AssetId, e.SlaProcessId, e.SlaProcess.Name
                                            From Entitlement e
                                            Where e.AccountId in :conToAccMap.values() And e.EndDate >= Today And e.StartDate <= Today];

        }
        
        //Added by Mahaboob as per T-19182
        if(Trigger.isInsert)
        {
            List<Group> allLevelQueues = [select id, name, developerName, type from Group where Type = 'Queue' and (developerName = 'DSC_Queue' OR developerName = 'CS_NUE' OR developerName = 'Operations_Management')];
            for(Group g : allLevelQueues)
            {
                if(g.developerName.equalsIgnoreCase('CS_NUE') || g.developerName.equalsIgnoreCase('Operations_Management'))
                {
                    secondThirdLevelQueues.put(g.Id, g);
                }
                if(g.developerName.equalsIgnoreCase('DSC_Queue'))
                {
                    firstLevelQId = g.Id;
                }
                if(g.developerName.equalsIgnoreCase('CS_NUE'))
                {
                    secondLevelQId = g.Id;
                }
            }
            if(entlsAccount != null && !entlsAccount.isEmpty()){
                for(Case c : (List<Case>)Trigger.new){
                    if(c.EntitlementId == null && c.AccountId != null){
                        for(Entitlement e:entlsAccount)
                        {
                            if(c.AccountId == e.AccountId)
                            {
                                //Added by Mahaboob on June 19th to assign Entitlement during case insert
                                c.EntitlementId = e.Id;
                            }
                        }
                    }
                }
            }
            else if(entlsContact != null && !entlsContact.isEmpty()){
                for(Case c : (List<Case>)Trigger.new){
                    if(c.EntitlementId == null && c.ContactId != null){
                        for(Entitlement e:entlsContact)
                        {
                            if(conToAccMap.get(c.ContactId) == e.AccountId)
                            {
                                //Added by Mahaboob on June 22nd to assign Entitlement during email-to-case insert
                                c.EntitlementId = e.Id;
                            }
                        }
                    }
                }
            }
        }
    }
    
    public static user loggedInUserInfo;
    public static Profile loggedInUserProfileInfo;
    list<Profile> profiles = new list<profile>([select id, name from profile where name = :label.M2M_Standard_User]);
    public override void prepareAfter()
    {
    
        map<id, id> accountCaseMap = new map<id, id>();
        
        if(trigger.isInsert || trigger.isUpdate){
            for (Case c : (List<Case>)Trigger.new){
                if(c.accountId != null){
                    accountCaseMap.put(c.accountID, c.id);
                }
            }
        }
    
        if(accountCaseMap.size() > 0){
            map<id, list<Opportunity>> accountOppMap = new map<id, list<Opportunity>>();
            if(profiles.size() > 0){
                list<Opportunity> oppLst = new list<opportunity>([select id, accountid,OwnerId, Owner.profileId from Opportunity where accountID In :accountCaseMap.keySet() and Owner.profileId =:profiles[0].id and owner.IsActive = true]);
                if(oppLst.size() > 0){
                    for(Opportunity Opp : oppLst){
                        if(opp.accountID != null){
                            list<Opportunity> tmp = new list<Opportunity>();
                            tmp.add(opp);
                            if(accountOppMap.get(opp.accountId) != null){
                                tmp.addAll(accountOppMap.get(opp.accountId));
                            }
                            accountOppMap.put(opp.accountId, tmp);
                        }
                    }
                }
            }
            for(Id i : accountCaseMap.keySet()){
                if(accountOppMap.get(i) != null){
                    for(Opportunity opp : accountOppMap.get(i)){
                        CaseShare csShare = new CaseShare(caseId=accountCaseMap.get(i),
                                                      CaseAccessLevel = 'Read',
                                                      UserOrGroupId = opp.ownerID);
                        caseShareToinsert.add(csShare);   
                    }
                }
            }
        }
    }

    public override void beforeInsert(sObject o) {
        Case newCase = (Case)o;
        if(newCase.Status == 'Open' && (recTypeSixt == null || newCase.RecordTypeId != ((RecordType)recTypeSixt.get(0)).Id))
        {
            if (newCase.OwnerId != null) {
                Id owner = newCase.OwnerId;

                if (Schema.User.SObjectType == owner.getSobjectType()){
                    newCase.Status = 'In Progress';
                    if(recTypeStandard != null && recTypeStandard.size() == 1){
                        newCase.RecordTypeId = ((RecordType)recTypeStandard.get(0)).Id; //'012m00000004MT3AAM';
                    }
                }
            }
        }

        if(newCase.SuppliedEmail == null)
        {
            // check subject and description validation
            if(String.isBlank(newCase.Subject)){
                newCase.Subject.addError('Check the Ticket\'s Subject field. It can\'t be empty');
            }
            if(String.isBlank(newCase.Description) || (newCase.Description.length() < 20) ){
                newCase.Description.addError('Check the Ticket\'s Description field. It can\'t be empty. Should be at least 20 characters.');
            }
        }
        
        if(recTypeSixt != null && recTypeSixt.size() > 0)
        {
            if(newCase.RecordTypeId == ((RecordType)recTypeSixt.get(0)).Id && newCase.Description != null)
            {
                if(newCase.Subject != null)
                {
                    if(newCase.Subject.toLowerCase().contains('new customer'))
                    {
                        newCase.Sixt_type_of_order__c = 'Standalone';
                    }
                    else if(newCase.Subject.toLowerCase().contains('existing customer'))
                    {
                        newCase.Sixt_type_of_order__c = 'Product';
                    }
                }
                
                newCase.Sixt_buildability_status__c = 'Pending';
                newCase.Sixt_Additional_details__c = returnValueFromEmailForSpecificField(newCase.Description, 'Additional details');
                newCase.Sixt_Contract_duration__c = returnValueFromEmailForSpecificField(newCase.Description, 'Contract duration');
                newCase.Sixt_LVID__c = returnValueFromEmailForSpecificField(newCase.Description, 'LVID');
                newCase.Sixt_Vehicle_manufacturer__c = returnValueFromEmailForSpecificField(newCase.Description, 'Vehicle manufacturer');
                newCase.Sixt_Vehicle_model__c = returnValueFromEmailForSpecificField(newCase.Description, 'Vehicle model');
                newCase.Sixt_Vehicle_production_year__c = returnValueFromEmailForSpecificField(newCase.Description, 'Vehicle production year');
            }
        }
        
        // Adam Mehtic added - Associated_level will be populated with the Level value depending on Owner of the ticket
        //Marko Žulić edited logic
       if(newCase.OwnerId != null)
       {    
            System.debug(newCase);
            System.debug(newCase.AccountId == '001b0000005NsSq');
            if(newCase.AccountId == '001b0000005NsSq'){
                newCase.Associated_level__c = 'Level 2';
            }
            else{ 
  
                newCase.Associated_level__c = populateAssociatedLevelField(newCase.OwnerId);
                // Adam added, T-21053
                if(newCase.Associated_level__c.equalsIgnoreCase('Level 1'))
                {
                     newCase.Was_first_level_ticket__c = true;
                }
            }
       }
       
        Id accountId;
        List<Billing_Account__c> relatedBillingAccounts = null;
        String billingAccountsLinks = '';
        
        if(newCase != null && newCase.AccountId != null) {
            List<Account> relatedAccount = [SELECT Id FROM Account WHERE Id = :newCase.AccountId];
            
            if(relatedAccount != null && relatedAccount.size() > 0)
            {
                relatedBillingAccounts = [SELECT Id, Name, Payer__c FROM Billing_Account__c WHERE Payer__c = :relatedAccount[0].Id];
            
                if(relatedBillingAccounts != null && relatedBillingAccounts.size() > 0) {
                    for(Billing_Account__c ba : relatedBillingAccounts) {
                        billingAccountsLinks += '<a href=/' + ba.Id + '>' + ba.Name + '</a>' + '<br></br>'; //URL.getSalesforceBaseUrl().toExternalForm() + '/' + 
                    }
                    
                    newCase.Related_Billing_Accounts__c = billingAccountsLinks;
                    //update selectedCase;
                }   
            }
        }
    }

    public override void beforeUpdate(sObject old, sObject o) {

       
       Case oldCase = (Case)old;
       Case newCase = (Case)o;
       
       /*Adam Mehtic added on 08.07.2015*/
       // User on Level 1 is not allowed to change owners on higher level Tickets
       // because of assignment rule these wont be fired on Email-To-Case initial operation which is good.
       if(oldCase.OwnerId != newCase.OwnerId)
       {
           List<GroupMember> loggedInUserQueueInfo = [select UserOrGroupId, Group.id from GroupMember where UserOrGroupId = :userDoingChanges and Group.Type = 'Queue' and Group.developerName = 'DSC_Queue'];
           Id userOrGroupIdAssigned = oldCase.OwnerId;
           //System.debug('userOrGroupIdAssigned' + userOrGroupIdAssigned);
           List<GroupMember> oldCaseOwnerQueueInfo = [select UserOrGroupId, Group.id from GroupMember where (UserOrGroupId = :userOrGroupIdAssigned OR Group.Id = :userOrGroupIdAssigned) and Group.Type = 'Queue' and (Group.developerName = 'DSC_Queue' OR Group.developerName = 'CS_NUE' OR Group.developerName = 'Operations_Management')];
    
           if(loggedInUserQueueInfo != null && loggedInUserQueueInfo.size() > 0 && oldCaseOwnerQueueInfo != null)
           {
                GroupMember userQueueInfo = loggedInUserQueueInfo.get(0);
                
                if(userQueueInfo != null)
                {
                    for(GroupMember gm : oldCaseOwnerQueueInfo)
                    {
                        if(gm.UserOrGroupId != userQueueInfo.UserOrGroupId && gm.Group.Id != userQueueInfo.Group.Id)
                        {
                            newCase.addError('You are not allowed to change owner on this case');
                            return;
                        }
                    }   
                }
           }
       }
       
       // Adam Mehtic added - Associated_level will be populated with the Level value depending on Owner of the ticket
       if(oldCase.OwnerId != newCase.OwnerId)
       {
           newCase.Associated_level__c = populateAssociatedLevelField(newCase.OwnerId);
           // Adam added, T-21053
           // M2M Support user Id = 005b0000000EUpz ,
           //Replaced M2m support user id with Cloudsese support and removed hard coded id.
           // When new user gets assigned after M2M Support user, check is that new user on Level 1 and tick the checkbox
           if(loggedInUserInfo != null && loggedInUserInfo.username == Label.Cloudsense_Support_Username && oldCase.OwnerId == loggedInUserInfo.id && newCase.Associated_level__c.equalsIgnoreCase('Level 1'))
           {
               newCase.Was_first_level_ticket__c = true;
           }
       }
       
       /* Adam Mehtić added */
       if (oldCase.OwnerId != newCase.OwnerId)
       {
            if(String.valueOf(newCase.OwnerId).substring(0, 3).equalsIgnoreCase('00G')) //change to Open only if Queue is selected, not User
            {
                newCase.Status = 'Open';
            }
            else if(String.valueOf(newCase.OwnerId).substring(0, 3).equalsIgnoreCase('005')) //change to In Progress only if User is selected, not Queue
            {
                if (Schema.User.SObjectType == newCase.OwnerId.getSobjectType()){
                    newCase.Status = 'In Progress';
                    if(recTypeStandard != null && recTypeStandard.size() == 1 && (recTypeSixt == null || newCase.RecordTypeId != ((RecordType)recTypeSixt.get(0)).Id))
                    {//recTypeSixt part written by Adam just for testing
                        newCase.RecordTypeId = ((RecordType)recTypeStandard.get(0)).Id;
                    }
                }
            }
        }
       /********************/
        
        //Added by Mahaboob as per T-19182
        if((oldCase.OwnerId != newCase.OwnerId)){
            if(!secondThirdLevelQueues.isEmpty() && secondThirdLevelQueues.containsKey(newCase.OwnerId)){
                newCase.Status = 'Open';
            }
        }

       if(oldCase.SuppliedEmail != null && oldCase.SuppliedEmail.length() > 0 ){

            if (oldCase.AccountId != newCase.AccountId &&
                oldCase.ContactId == null && newCase.AccountId != null){

                Contact newContact = this.contactsMap.get(oldCase.SuppliedEmail);

                if(newContact != null && (newContact.AccountId == newCase.AccountId)){
                    newCase.ContactId = newContact.Id;
                } else {
                    newContact = parseEmailToContact (oldCase.SuppliedEmail);
                    newContact.AccountId = newCase.AccountId;
                    insert newContact;
                    newCase.ContactId = newContact.Id;
                }
            }
       }

        if(entlsAccount != null && !entlsAccount.isEmpty())
        {

            for(Case c : (List<Case>)Trigger.new)
            {
                if(c.EntitlementId == null && c.AccountId != null)
                {
                    for(Entitlement e : entlsAccount)
                    {
                        if(e.AccountId==c.AccountId)
                        {
                            c.EntitlementId = e.Id;
                            if(c.AssetId==null && e.AssetId!=null)
                            {
                                c.AssetId=e.AssetId;
                            }
                            break;
                        }
                    }
                }
            }
         }
         else if((entlsAccount == null || entlsAccount.isEmpty()) && (oldCase.AccountId != newCase.AccountId))
         {
            String[] addresses = new String[]{};

            if (this.toManager != null)
            {
                addresses.add(toManager);
            }

            if (this.toServiceManager != null)
            {
                addresses.add(toServiceManager);
            }

            if(newCase.AccountId != null)
            {
                Account acc =   this.accountsMap.get(newCase.AccountId);
                if (acc != null)
                {
                    String body = '\n Account ' + acc.Name +' has no Entitlement record.\n' +
                                '\n Ticket has been raised with details' +
                                '\n Ticket number: ' + newCase.CaseNumber +
                                '\n Ticket subject: ' + newCase.Subject +
                                '\n\n Kind Regards, \n M2M Service Team';

                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    email.setToAddresses(addresses);
                    email.setPlainTextBody(body);
                    email.setWhatId(newCase.Id);

                    if (addresses.Size() > 0)
                    {
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
                    }
                }
            }
        }

     // Marko Zulic added on 25.01.2017, BAU T-021 still not finished
        
        if(newCase.Status =='WVL'){
            newCase.WVL_Status__c = True;
            newCase.Number_of_WVL_statuses__c++;

            newCase.Last_Assignee__c = oldCase.OwnerId;

            Group queue = [SELECT Id, Name, Type FROM Group WHERE Name = 'WVL' AND Type = 'Queue'];
            newCase.OwnerId = queue.Id;
        }
        else if(newCase.Status =='Open'){
            newCase.WVL_Status__c = False;
        }

    
        

    }

    public override void beforeDelete(sObject o)
    {
        Case caseToDelete = (Case)o;
        caseToDelete.addError('You should not delete a ticket!, talk to your Teamlead first.');
    }

    
    list<caseShare> caseShareToinsert = new list<caseShare>();

    public override void afterInsert(sObject o) {
        //Case c = (Case)o;
    }

    public override void afterUpdate(sObject old, sObject o) {
        // Apply after update logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
        
        Case oldCase = (Case)old;
        Case newCase = (Case)o;
        
        Id usersProfileId = UserInfo.getProfileId();

       
        if(((loggedInUserInfo != null && loggedInUserProfileInfo != null) && (loggedInUserProfileInfo.id == loggedInUserInfo.profileId ||loggedInUserInfo.Allow_To_Use_UDO_Funtionolities__c )) && newCase.UDo_Ticket_created__c)  {
            checkChangesForUDo(oldCase, newCase);
        }

    }

    public override void afterDelete(sObject o) {
        // Apply after delete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterUndelete(sObject o) {
        // Apply after undelete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void finish() {
        //for(Case c : (List<Case>)Trigger.New){
            //System.debug('Case Id : ' + c.Id + '---Entitlement Id : ' + c.EntitlementId + '---SLA Level : ' + c.SLA_level__c + '---Account Id : ' + c.AccountId + '---Contact Id : ' + c.ContactId);
        //}
        
        if(!caseShareToinsert.isEmpty()){ 
            database.insert(caseShareToinsert,false);
        }
        
    }

    private void checkChangesForUDo(Case oldCase, Case newCase) {    
    
        system.debug(LoggingLevel.debug,'After Update called');
       
        if (oldCase != null && newCase != null) {
            if (!oldCase.Status.equalsIgnoreCase(newCase.Status) && newCase.Status.equalsIgnoreCase('Closed and Canceled') && !UDOUtils.stopTriggerExecution) {         
                UDOUtils.CancelTicketInUDo(newCase.Id);     
            }       
            
            if (!oldCase.Status.equalsIgnoreCase(newCase.Status) && newCase.Status.equalsIgnoreCase('Closed and Solved') && oldCase.UDo_Status__c.equalsIgnoreCase('Active') && !UDOUtils.stopTriggerExecution) {       
                UDOUtils.CloseTicketInUDo(newCase.Id);      
            }       
            
            //Commented the below if-else block by Mahaboob on 13 Jan 2016 to delete Additional Comment for UDo field       
            /*if (oldCase.UDo_Additional_comment__c == null && newCase.UDo_Additional_comment__c != null && newCase.UDo_Additional_comment__c.length() > 0) {       
            UDOUtils.UpdateTicketCommentInUDo(newCase.Id);      
            } else if (oldCase.UDo_Additional_comment__c != null && !oldCase.UDo_Additional_comment__c.equalsIgnoreCase(newCase.UDo_Additional_comment__c)) {       
            UDOUtils.UpdateTicketCommentInUDo(newCase.Id);      
            }*/     
            //Done by Mahaboob      
            
            if (oldCase.UDo_Description__c == null && newCase.UDo_Description__c != null && newCase.UDo_Description__c.length() > 0 && !UDOUtils.stopTriggerExecution) {        
                UDOUtils.UpdateDescriptionInUDo(newCase.Id);        
            } else if (oldCase.UDo_Description__c != null && !oldCase.UDo_Description__c.equalsIgnoreCase(newCase.UDo_Description__c) && !UDOUtils.stopTriggerExecution) {      
                UDOUtils.UpdateDescriptionInUDo(newCase.Id);        
            }       
            
            if (oldCase.UDo_Severity__c != null && newCase.UDo_Severity__c != null && !oldCase.UDo_Severity__c.equalsIgnoreCase(newCase.UDo_Severity__c) && !UDOUtils.stopTriggerExecution) {       
                UDOUtils.UpdateSeverityInUDo(newCase.Id);       
            } else if (oldCase.UDo_Severity__c == null && newCase.UDo_Severity__c != null && !UDOUtils.stopTriggerExecution) {      
                UDOUtils.UpdateSeverityInUDo(newCase.Id);       
            }       
            
            if (oldCase.Massive__c != null && newCase.Massive__c != null && !oldCase.Massive__c.equalsIgnoreCase(newCase.Massive__c) && !UDOUtils.stopTriggerExecution) {       
                UDOUtils.UpdateMassiveInUDo(newCase.Id);        
            } else if (oldCase.Massive__c == null && newCase.Massive__c != null && !UDOUtils.stopTriggerExecution) {        
                UDOUtils.UpdateMassiveInUDo(newCase.Id);        
            }
        }
    }

    private Contact parseEmailToContact (String email)
    {
        Contact newContact;
        newContact = new Contact();

        String SALUTATION_MR = 'Mr.';
        String firstName;
        String lastName;
        String firstPart;

         if(email == null)
         {
            return null;
         }

        List<String> parts = email.split('@');
        firstPart = parts[0];

        if(firstPart == null || firstPart.length() == 0 )
        {
            return null;
        }


        if(firstPart.contains('.'))
        {
            List<String> namePart = firstPart.split('\\.');

            if(namePart.size() > 1)
            {
                firstName = namePart[0];
                lastName = namePart[1];
                firstName = firstName.substring(0,1).toUpperCase() + firstName.substring(1,firstName.length());
                lastName = lastName.substring(0,1).toUpperCase() + lastName.substring(1,lastName.length());

            }
            else
            {
                firstName = 'Unknown';
                lastName = firstPart;
            }
        }
        else
        {
            firstName = 'N/A';
            lastName = firstPart;
        }

            newContact.Salutation = SALUTATION_MR;
            newContact.LastName = lastName;
            newContact.FirstName = firstName;
            newContact.email = email;

        return newContact;
    }
    
    private String returnValueFromEmailForSpecificField(String email, String valueForSearch)
    {
        string valueForReturn = '';
        Integer sizeOfSearchingValue = 0;
        
        if(email == null || valueForSearch == null)
            return valueForReturn;
            
            
        String[] parsedEmail = email.split('\n');
        
        if(parsedEmail != null && parsedEmail.size() > 0)
        {
            for(String row : parsedEmail)
            {
                if(row != null)
                {
                    if(row.equalsIgnoreCase('END'))
                        return valueForReturn;
                        
                    sizeOfSearchingValue = String.format('{0}{1}', new String[] {valueForSearch, ':'}).length();
                    
                    if(row.toLowerCase().contains(valueForSearch.toLowerCase()))
                    {
                        valueForReturn = row.substring(sizeOfSearchingValue);
                        valueForReturn = valueForReturn.trim();
                        valueForReturn = valueForReturn.replace(':', '');
                    }
                    else
                    {
                        if(valueForSearch.equalsIgnoreCase('Additional details'))
                        {
                            valueForReturn = String.format('{0} {1}', new String[] {valueForReturn, row});
                        }
                    }
                }
            }
        }
        
        return valueForReturn;
    }
    
    private String populateAssociatedLevelField(Id newCaseOwner)
    {
        String returnedLevel = '';
        
        List<GroupMember> newCaseOwnerQueueInfo = [select UserOrGroupId, Group.id, Group.DeveloperName from GroupMember where (UserOrGroupId = :newCaseOwner OR Group.Id = :newCaseOwner) and Group.Type = 'Queue' and (Group.DeveloperName = 'DSC_Queue' OR Group.DeveloperName = 'CS_NUE' OR Group.DeveloperName = 'Operations_Management')];
              
        if(newCaseOwnerQueueInfo != null && newCaseOwnerQueueInfo.size() > 0)
        {
            if(newCaseOwnerQueueInfo[0].Group.DeveloperName.equalsIgnoreCase('DSC_Queue'))
            {
                returnedLevel = 'Level 1';
            }
            else if(newCaseOwnerQueueInfo[0].Group.DeveloperName.equalsIgnoreCase('CS_NUE'))
            {
                returnedLevel = 'Level 2';
            }
            else if(newCaseOwnerQueueInfo[0].Group.DeveloperName.equalsIgnoreCase('Operations_Management'))
            {
                returnedLevel = 'Level 3';
            }
            else
            {
                returnedLevel = '';
            }
        }
        
        return returnedLevel;
    }
}