//------------------------------------------------------------------------------
// Test of the InsertHelper class
//------------------------------------------------------------------------------
@isTest
private class TestInsertHelper
{
	//--------------------------------------------------------------------------
	// Test inserting of related records implicit
	//--------------------------------------------------------------------------
    static testMethod void testInsertingOfConnectedObjectsImplicit()
	{
		InsertHelper tariff = new InsertHelper();
		Tariff__c t = new Tariff__c(Name = 'Test Tariff');
		Tariff_Option__c to = new Tariff_Option__c(Name = 'Test Tariff Option');
		Rate_Card__c rc = new Rate_Card__c(Name = 'Test Rate Card');
		Service_Fee__c sf = new Service_Fee__c(Name = 'Test Service Fee');
		tariff.add(to, 'Tariff__c', t);
		tariff.add(rc, 'Tariff_Option__c', to);
		tariff.add(sf, 'Tariff_Option__c', to);
		tariff.store();
		//
		// Test result
		//
		to = [ SELECT Id, Name,
			   ( SELECT Id, Name
				 FROM Rate_Cards__r),
			   ( SELECT Id, Name
			     FROM Service_Fee__r)
			   FROM Tariff_Option__c LIMIT 1 ];
		system.assertNotEquals(null, to);
		system.assertEquals(1, to.Rate_Cards__r.size());
		system.assertEquals(1, to.Service_Fee__r.size());
    }

	//--------------------------------------------------------------------------
	// Test inserting of related records explicit
	//--------------------------------------------------------------------------
    static testMethod void testInsertingOfConnectedObjectsExplicit()
	{
		InsertHelper tariff = new InsertHelper();
		Tariff__c t = new Tariff__c(Name = 'Test Tariff');
		Tariff_Option__c to = new Tariff_Option__c(Name = 'Test Tariff Option');
		Rate_Card__c rc = new Rate_Card__c(Name = 'Test Rate Card');
		Service_Fee__c sf = new Service_Fee__c(Name = 'Test Service Fee');
		tariff.add(t);
		tariff.add(to, 'Tariff__c', t);
		tariff.add(rc, 'Tariff_Option__c', to);
		tariff.add(sf, 'Tariff_Option__c', to);
		tariff.store();
		//
		// Test result
		//
		to = [ SELECT Id, Name,
			   ( SELECT Id, Name
				 FROM Rate_Cards__r),
			   ( SELECT Id, Name
				 FROM Service_Fee__r)
			   FROM Tariff_Option__c LIMIT 1];
		system.assertNotEquals(null, to);
		system.assertEquals(1, to.Rate_Cards__r.size());
		system.assertEquals(1, to.Service_Fee__r.size());
    }

	//--------------------------------------------------------------------------
	// Test inserting of related records with field that are not exists
	//--------------------------------------------------------------------------
	static testMethod void testNonExistingFieldError()
	{
		InsertHelper tariffOption = new InsertHelper();
		Tariff_Option__c to = new Tariff_Option__c(Name = 'Test Tariff Option');
		Rate_Card__c rc = new Rate_Card__c(Name = 'Test Rate Card');
		Service_Fee__c sf = new Service_Fee__c(Name = 'Test Service Fee');
		try
		{
			tariffOption.add(rc, 'Non_Existing_Field__c', to);
			system.assert(false);
		}
		catch (Exception e)	{}
	}

	//--------------------------------------------------------------------------
	// Test inserting of related records with field that is not lookup
	//--------------------------------------------------------------------------
	static testMethod void testWrongFieldTypeError()
	{
		InsertHelper tariffOption = new InsertHelper();
		Tariff_Option__c to = new Tariff_Option__c(Name = 'Test Tariff Option');
		Rate_Card__c rc = new Rate_Card__c(Name = 'Test Rate Card');
		Service_Fee__c sf = new Service_Fee__c(Name = 'Test Service Fee');
		try
		{
			tariffOption.add(rc, 'Name', to);
			system.assert(false);
		}
		catch (Exception e)	{}
	}

	//--------------------------------------------------------------------------
	// Test SObjectWrapper equals method
	//--------------------------------------------------------------------------
	static testMethod void testSObjectWrapper()
	{
		Account acc1 = new Account(Name = 'Test Account 1');
		Account acc2 = new Account(Name = 'Test Account 1');
		Account acc3 = acc1;
		system.assertEquals(false,
				new InsertHelper.SObjectWrapper(acc1).equals(
					new InsertHelper.SObjectWrapper(acc2)));
		system.assertEquals(true,
				new InsertHelper.SObjectWrapper(acc1).equals(
					new InsertHelper.SObjectWrapper(acc3)));
	}
}