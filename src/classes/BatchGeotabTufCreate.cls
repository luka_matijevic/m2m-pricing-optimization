/*
	Petar Matkovic
	For all stocks assigned to GEOTAB order requests and without appropriate TUF files, create TUFs and upload to sm2m
*/
global class BatchGeotabTufCreate implements Database.Batchable<sObject> {
	
	String query;
	GEOTAB_Settings__c geotabSettings = GEOTAB_Settings__c.getInstance();

	global BatchGeotabTufCreate() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		query = 'SELECT id, name, order_request__c, third_upload_file__c, third_upload_file__r.order_request__c, third_upload_file__r.Account__c'
				+ ' FROM stock__c '
				+ 'WHERE order_request__r.recordtypeid = \'' + geotabSettings.Order_Request_Type_Id__c + '\'';
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<Stock__c> stockList = (List<Stock__c>)scope;
		Set<Id> orderRequestIdSet = new Set<Id>();
		orderRequestIdSet = getOrderRequestIdsFromStockList(stockList);

	}
	
	global void finish(Database.BatchableContext BC) {
		
	}

	private Set<Id> getOrderRequestIdsFromStockList(List<Stock__c> stockListIn){
		Set<Id> returnSet = new Set<Id>();
		for (stock__c stock : stockListIn){
			returnSet.add(stock.order_request__c);
		}
		return returnSet;
	}
	
}