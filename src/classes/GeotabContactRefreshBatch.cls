global class GeotabContactRefreshBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
	
	String query;
	List<Geotab_Contact__c> geotabContacts;
	List<GeotabUtils.GeotabUserContact> geotabContactsApi;
	Map<String, Geotab_Contact__c> sfdcGeotabContactMap = new Map<String, Geotab_Contact__c>();
	List<Geotab_Contact__c> geotabContactsForInsert;

	global GeotabContactRefreshBatch(List<GeotabUtils.GeotabUserContact> geotabContactsApi) {
		this.geotabContactsApi = geotabContactsApi;
		//System.debug(LoggingLevel.INFO, 'Geotab Contacts API ->' + geotabContactsApi);
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		query = 'SELECT Id, Geotab_Id__c, Geotab_Contact_Name__c FROM Geotab_Contact__c';
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		geotabContacts = (List<Geotab_Contact__c>) scope;
		//System.debug(LoggingLevel.INFO, 'Geotab Contacts ->' + geotabContacts);

		for(Geotab_Contact__c gc : geotabContacts) {
			sfdcGeotabContactMap.put(gc.Geotab_Id__c, gc);
			//System.debug(LoggingLevel.INFO, 'sfdcGeotabContactMap execute ->' + sfdcGeotabContactMap);
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		//System.debug(LoggingLevel.INFO, 'Geotab Contacts API ->' + geotabContactsApi);
   		//System.debug(LoggingLevel.INFO, 'sfdcGeotabContactMap ->' + sfdcGeotabContactMap);

   		geotabContactsForInsert = new List<Geotab_Contact__c>();

		for(GeotabUtils.GeotabUserContact gc : geotabContactsApi) {
			//System.debug(LoggingLevel.INFO, 'Geotab Contacts API ->' + gc);
			String gcApiId = String.valueOf(gc.Id);
			Geotab_Contact__c gcSFDC = sfdcGeotabContactMap.get(gcApiId);

			if(gcSFDC == null) {
				Geotab_Contact__c newGC = new Geotab_Contact__c();
				newGC.Name = gc.ContactName;
				newGC.Geotab_Id__c = gcApiId;
				newGC.Geotab_Contact_Name__c = gc.ContactName;
				newGC.Geotab_Display__c = gc.DisplayName.abbreviate(255);
				//System.debug(LoggingLevel.INFO, 'Geotab Contacts New ->' + newGC);
				geotabContactsForInsert.add(newGC);
			}
		}

		if(geotabContactsForInsert.size() > 0)
			insert geotabContactsForInsert;
	}
	
}