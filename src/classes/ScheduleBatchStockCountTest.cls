/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ScheduleBatchStockCountTest {

    static testMethod void batchScheduleTest() {
              
        List<Stock__c> stockList = new List<Stock__c> ();
        
              
        /* Account insert*/ 
        Account acc = new Account(Name = 'Test account', TEF_DE_Sales_Channel__c = 'Test channel', Type = 'Customer');
        insert acc;
        
        /* Stock insert*/ 
        for(Integer i=0;i<50;i++){
            Stock__c sim = new Stock__c(Name = '1234567890'+i, ICCID__c = '123456789012345678'+i, Account__c = acc.Id, Activation_Date__c = Date.newInstance(Date.today().year(), 1, 3));
            stockList.add(sim) ;
        }
        insert stockList;
        
        System.assertEquals(stockList[0].Name,'12345678900');
        
        Test.startTest();
            //Stock Count Schedular Test
            ScheduleBatchStockCount st = new ScheduleBatchStockCount();
            datetime currentDateTime = datetime.now();
            String min = String.valueOf(math.mod((currentDateTime.minute() + 1),60));       
            String schExp = '0 '+min+' * * * ? ';
            
            system.schedule('Schedular Test :'+datetime.now(), schExp, st);          
            
        Test.stopTest();
        
    }
}