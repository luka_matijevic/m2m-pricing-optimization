global class BatchCalculateDBIGesamtAndCreateDBF implements Database.Batchable<sObject>, Database.Stateful {
	
	String query;
	Daily_Bill__c dailyBill;
	private boolean noTriggerUser = false;
    private No_Trigger__c notrigger = null;

    Map<String, Decimal> rechsBrutto = new Map<String, Decimal>();
	Map<String, Decimal> rechsNetto = new Map<String, Decimal>();
	Map<String, Decimal> rechsTax = new Map<String, Decimal>();

	Decimal tmpDecimalBrutto = 0;
	Decimal tmpDecimalNetto = 0;
	Decimal tmpDecimalTax = 0;

	List<Daily_bill_item__c> dailyBillItemListFull = new List<Daily_bill_item__c>();

	global BatchCalculateDBIGesamtAndCreateDBF(Daily_Bill__c dailyBillIn) {
		dailyBill = dailyBillIn;

	}
	
	global List<sObject> start(Database.BatchableContext BC) {
		//return Database.getQueryLocator(query);

		return 
		[
			SELECT	Id, 
					Name, 
					Rechnungsnummer__c, 
					Einzel_Bruttobetrag__c, 
					Einzel_Nettobetrag__c, 
					Einzel_Taxbetrag__c,
					Art__c
			FROM 	Daily_bill_item__c
			WHERE	Daily_bill__c = :this.dailyBill.Id
		];
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<Daily_bill_item__c> dailyBillItemList = (List<Daily_bill_item__c>)scope;

		for (Daily_bill_item__c dbi : dailyBillItemList){				
			if(rechsBrutto.containsKey(dbi.Rechnungsnummer__c)){
				tmpDecimalBrutto = rechsBrutto.get(dbi.Rechnungsnummer__c);
				rechsBrutto.put(dbi.Rechnungsnummer__c, dbi.Einzel_Bruttobetrag__c + tmpDecimalBrutto);
				
				tmpDecimalNetto = rechsNetto.get(dbi.Rechnungsnummer__c);
				rechsNetto.put(dbi.Rechnungsnummer__c, dbi.Einzel_Nettobetrag__c + tmpDecimalNetto);
				
				tmpDecimalTax = rechsTax.get(dbi.Rechnungsnummer__c);
				rechsTax.put(dbi.Rechnungsnummer__c, dbi.Einzel_Taxbetrag__c + tmpDecimalTax);
			}else{
				rechsBrutto.put(dbi.Rechnungsnummer__c, dbi.Einzel_Bruttobetrag__c);
				rechsNetto.put(dbi.Rechnungsnummer__c, dbi.Einzel_Nettobetrag__c);
				rechsTax.put(dbi.Rechnungsnummer__c, dbi.Einzel_Taxbetrag__c);
			}
		}

		dailyBillItemListFull.addAll(dailyBillItemList);
	}
	
	global void finish(Database.BatchableContext BC) {
		disableTriggers();

		for (Daily_bill_item__c dbi : dailyBillItemListFull){
			dbi.Gesamt_Bruttobetrag__c = rechsBrutto.get(dbi.Rechnungsnummer__c).setScale(2, RoundingMode.DOWN);
			dbi.Gesamt_Nettobetrag__c = rechsNetto.get(dbi.Rechnungsnummer__c).setScale(2, RoundingMode.DOWN);
			dbi.Gesamt_Taxbetrag__c = rechsTax.get(dbi.Rechnungsnummer__c).setScale(2, RoundingMode.DOWN);

			if (dbi.Gesamt_Nettobetrag__c < 0){
				dbi.Art__c = 22;
			}else{
				dbi.art__c = 2;
			}
		}

		update dailyBillItemListFull;
		
		enableTriggers();

		FinalPrebillFileCSVGenerator c = new FinalPrebillFileCSVGenerator(dailyBill.date__c);  
		Database.executeBatch(c); 
	}

	private void disableTriggers(){
		List<No_Trigger__c> noList = [Select Id, SetupOwnerId, Flag__c from No_Trigger__c where SetupOwnerId =: UserInfo.getUserId() limit 1];
        if (noList.isEmpty()) {
            noTriggerUser=true;
            notrigger = new No_Trigger__c();
            notrigger.Flag__c = true;
            notrigger.SetupOwnerId = UserInfo.getUserId();
            upsert notrigger;
        } else {
            noList[0].Flag__c = true;
            upsert noList[0];
            notrigger = noList[0];
        }
	}

	private void enableTriggers(){
		notrigger.Flag__c = false;
        upsert notrigger; 
        if (noTriggerUser) {
            delete notrigger;
        }
	}
	
}