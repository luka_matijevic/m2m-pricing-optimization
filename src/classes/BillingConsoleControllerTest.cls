@isTest(seeAllData=false)
private class BillingConsoleControllerTest {

	private static testMethod void testAllView() {
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers;
        
        Daily_Bill__c db1 = new Daily_Bill__c (Prebill_Requests_Sent__c=1,Date__c=Date.today());
        insert db1;
        
        notriggers.Flag__c = false;
        update notriggers;
        
         Test.startTest();
            PageReference pageRefr = Page.BillingConsole;
            Test.setCurrentPageReference(pageRefr); 
            BillingConsoleController ctrl = new BillingConsoleController (new ApexPages.StandardController(new Daily_Bill__c()));
            system.assert(ctrl!=null);
            
            ctrl.refreshReports();
            
            ctrl.refreshLists();
            
            ctrl.CreateStorno();
            
            ctrl.CreateGutschrift();
            
            //ctrl.CreateDBF();
            
            ctrl.OpenDailyBill();
            ctrl.savedailyBillObject();
            ctrl.CreateCharge();
            ctrl.gotoHome();
            ctrl.saveDailyBillItems();
            ctrl.uploadDailyBill();
            ctrl.archiveItems();
            ctrl.refreshForecastReport();
            
            
         Test.stopTest();    
        

	}

}