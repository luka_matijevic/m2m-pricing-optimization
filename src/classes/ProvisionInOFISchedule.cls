global with sharing class ProvisionInOFISchedule implements Schedulable
{
	//--------------------------------------------------------------------------
	// Scheduled job
	//--------------------------------------------------------------------------
	global void execute (SchedulableContext sc)
	{
		//
		// Run apex job
		//
		Database.executeBatch(Factory.createProvisionInOFIJob());
	}
}