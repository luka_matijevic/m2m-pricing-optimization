global class GeotabOrdersCheckBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful  {
	
	String query;
	List<Order_Request__c> orderRequests;
	GEOTAB_Settings__c geotabSettings = GEOTAB_Settings__c.getInstance();
	String userInfoEndPoint = geotabSettings.UserInfoEndPoint__c;
	String deviceOrderEntriesEndPoint = geotabSettings.DeviceOrderEntriesEntPoint__c;
	String lookupDeviceEndPoint = geotabSettings.LookupDeviceEndPoint__c;
	String onlineOrderStatusEndPoint = 'https://myadminapi.geotab.com/v2/MyAdminApi.ashx/GetOnlineOrderStatus?';
	Configurable_Sixt_helper__c csh = null;
	Map<String, GeotabUtils.AuthenticationResponseClass> authorizationResultMap = null;
	Map<String, List<GeotabUtils.GeotabDeviceOrderEntry>> geotabDeviceOrderResultMap = null;
	Map<String, GeotabUtils.InstallLogsClass> geotabDeviceObject = null;
	Map<String, Id> stockICCIDAccount = new Map<String, Id>();
	Map<String, Id> stockICCIDOrderRequest = new Map<String, Id>();
	Map<String, String> stockICCIDSerialDeviceNumber = new Map<String, String>();
	String response = 'Order successfully posted in Geotab!';
	List<Geotab_orders_log__c> geotabLogToInsert = new List<Geotab_orders_log__c>();
	List<List<String>> devicesList = new List<List<String>>();

	global GeotabOrdersCheckBatch(List<Order_Request__c> orderRequests) {
		System.debug(orderRequests);
		this.orderRequests = orderRequests;
	}
	
	global List<Order_Request__c> start(Database.BatchableContext BC) {
		List<Configurable_Sixt_helper__c> customConfig = [SELECT Id, Name,  Geotab_username__c, Geotab_password__c, Sixt_frame_contract__c, Sixt_account__c, Sixt_billing_account__c, Sixt_Opportunity_RecordType__c FROM Configurable_Sixt_helper__c WHERE Name = 'Configurable records' ORDER BY CreatedDate ASC];
		System.debug(customConfig);
		if (customConfig != null && customConfig.size() > 0)
		{
			csh = customConfig[0];
		}
		
		if (csh != null)
		{
			userInfoEndPoint = userInfoEndPoint + csh.Geotab_username__c + '&password=' + csh.Geotab_password__c;
		}
		System.debug(userInfoEndPoint);
		return orderRequests;
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<Order_Request__c> orReqList = (List<Order_Request__c>)scope;

		for(Order_Request__c orq : orReqList) {
			System.debug(orq);
			System.debug(userInfoEndPoint);
			HttpResponse resp = CaseUtils.executeRequest(userInfoEndPoint, 'GET', null, 32);
			System.debug(userInfoEndPoint);
			if(resp != null && resp.getStatusCode() == 200) {
				authorizationResultMap = (Map<String, GeotabUtils.AuthenticationResponseClass>)JSON.deserialize(resp.getBody(), Map<String, GeotabUtils.AuthenticationResponseClass>.class);
				System.debug(resp);
				System.debug(resp.getBody());
				if (authorizationResultMap != null)
				{
					String userIdResult, sessionIdResult;
					List<GeotabUtils.GeotabAccount> geotabAccounts = null;

					userIdResult = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').userId : null;
					sessionIdResult = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').sessionId : null;
					geotabAccounts = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').accounts : null;
					
					if(geotabAccounts != null && geotabAccounts.size() > 0) {
						String tmpString = 'apiKey="' + userIdResult + '"&forAccount="' + geotabAccounts[0].AccountId + '"&sessionId="' + 
								sessionIdResult + '"&poNum="' + orq.Geotab_Purchase_Order_Number__c + '"';

						tmpString = EncodingUtil.urlEncode(tmpString, 'UTF-8');
						tmpString = tmpString.replace('+', '%20'); // Salesforce bug

						deviceOrderEntriesEndPoint = deviceOrderEntriesEndPoint + tmpString;
						System.debug(tmpString);
						System.debug(orq.Geotab_Purchase_Order_Number__c);
						resp = CaseUtils.executeRequest(deviceOrderEntriesEndPoint, 'GET', null, 32);
						System.debug(resp);
						System.debug(resp.getBody());
						if(resp != null && resp.getStatusCode() == 200) {
							geotabDeviceOrderResultMap = (Map<String, List<GeotabUtils.GeotabDeviceOrderEntry>>)JSON.deserialize(resp.getBody(), Map<String, List<GeotabUtils.GeotabDeviceOrderEntry>>.class);
							List<GeotabUtils.GeotabDeviceOrderEntry> devices = geotabDeviceOrderResultMap.get('result') != null ? geotabDeviceOrderResultMap.get('result') : null;
							if (devices != null){
								for(GeotabUtils.GeotabDeviceOrderEntry dev : devices) {
									List<String> deviceInfoList = new List<String>();
									deviceInfoList.add(dev.device.SerialNumber);
									deviceInfoList.add(orq.Id);
									deviceInfoList.add(orq.Account__c);
									devicesList.add(deviceInfoList);
									// lookupdevice
									/*lookupDeviceEndPoint = lookupDeviceEndPoint + 'apiKey="' + userIdResult + '"&serialNo="' + dev.device.SerialNumber + '"&sessionId="' + 
										sessionIdResult + '"';

									resp = CaseUtils.executeRequest(lookupDeviceEndPoint, 'GET', null, 32);
									System.debug(resp);
									System.debug(resp.getBody());
									if(resp != null && resp.getStatusCode() == 200) {
										geotabDeviceObject = (Map<String, GeotabUtils.InstallLogsClass>)JSON.deserialize(resp.getBody(), Map<String, GeotabUtils.InstallLogsClass>.class);
										System.debug(geotabDeviceObject);
										// merge account to device's SIM ICCID
										// attach order request to device's ICCID
										if(geotabDeviceObject != null) {
											GeotabUtils.InstallLogsClass resultValue = geotabDeviceObject.get('result') != null ? geotabDeviceObject.get('result') : null;
											System.debug(resultValue);
											if(resultValue != null) {
												stockICCIDAccount.put(resultValue.simNumber, orq.Account__c);
												stockICCIDOrderRequest.put(resultValue.simNumber, orq.Id);
												stockICCIDSerialDeviceNumber.put(resultValue.simNumber, String.valueOf(resultValue.request.device.serialNumber));
												geotabLogToInsert.add(new Geotab_orders_log__c(Order_Request__c = orq.Id, Response_message__c = 'SIM card with ICCID: ' + resultValue.simNumber + ' is updated with Order Request: ' + 
													orq.Name + ' and Account: ' + orq.Account__r.Name));
											}
										}
									} else {
										geotabLogToInsert.add(new Geotab_orders_log__c(Order_Request__c = orq.Id, Response_message__c = 'Device with Serial number: ' + dev.device.SerialNumber + ' was not found!'));
									}*/
								}
							}
							else{
								geotabLogToInsert.add(new Geotab_orders_log__c(Order_Request__c = orq.Id, Response_message__c = 'Geotab Order with Purchase Order Number: ' + orq.Geotab_Purchase_Order_Number__c + ' device map is null!'));
							}
							
						} else {
							geotabLogToInsert.add(new Geotab_orders_log__c(Order_Request__c = orq.Id, Response_message__c = 'Geotab Order with Purchase Order Number: ' + orq.Geotab_Purchase_Order_Number__c + ' was not found!'));
						}
					} else {
						geotabLogToInsert.add(new Geotab_orders_log__c(Order_Request__c = orq.Id, Response_message__c = 'Fetching Accounts from Geotab was unsuccessful!'));
					}
				}
			} else {
				geotabLogToInsert.add(new Geotab_orders_log__c(Order_Request__c = orq.Id, Response_message__c = 'User authorization failed!'));
			}
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		System.debug(devicesList);
		if (devicesList != null){
			for (List<String> deviceInfoList : devicesList){
				if (deviceInfoList.size() == 3){
					String deviceSerialNumber = deviceInfoList[0];
					String orderRequestId = deviceInfoList[1];
					String accountId = deviceInfoList[2];
					Database.executeBatch(new GeotabFetchSimNumberBatch(deviceSerialNumber, orderRequestId, accountId), 1);
				}
			}
		}
		/*
		if(stockICCIDAccount != null && stockICCIDAccount.size() > 0) {
			List<Stock__c> stocksToUpdate = [SELECT Id, ICCID__c, Account__c, Order_Request__c FROM Stock__c WHERE ICCID__c in :stockICCIDAccount.keySet()];

			if(stocksToUpdate != null && stocksToUpdate.size() > 0) {
				for(Stock__c item : stocksToUpdate) {
					item.Order_Request__c = stockICCIDOrderRequest.get(item.ICCID__c);
					item.Account__c = stockICCIDAccount.get(item.ICCID__c);
					item.GEOTAB_Stock__c = true;
					item.GEOTAB_Device_Serial_Number__c = stockICCIDSerialDeviceNumber.get(item.ICCID__c);
				}
				update stocksToUpdate;
			}
		}*/
	}

	void InsertGeotabOrderLogs(List<Geotab_orders_log__c> listToInsert) {
		insert listToInsert;
	}
	
}