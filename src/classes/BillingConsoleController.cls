public with sharing class BillingConsoleController {
    
    public boolean disableButons {get; set;} // This flag is used to disable all buttons to the M2M 2nd Level Profile User
    public boolean hideAllButtonsfor1stLevel {get; set;} // If logged in user is 1st Level - DSC - READ permissions profile user, then none of button should visible
    public transient List<Daily_Bill_Item__c> dailyBillItemsList {get; set;}
    public List<Prebill__c> prebillList {get; set;}
    public List<Attachment> attList  {get; set;}
    public List<SelectOption> reportList  {get; set;}
    public List<SelectOption> selectItems    {get; set;}
    public List<Daily_Bill_Item__History> history {get; set;}
    public String dailybill {get; set;}
    public String reportid {get; set;}
    public String reportFilter {get; set;}
    public Integer numberOfPrebills {get; set;}
    
    private Map<String,String> reportFilters;
    public Daily_Bill__c dailyBillObject {get; set;}
    
	public BillingConsoleController(ApexPages.StandardController cntrl) {
		disableButons = false;
		hideAllButtonsfor1stLevel = false;
		list<profile> prfl = new list<profile>([select id, name from profile where id=:userinfo.getProfileId()]);
		
		
		if(!prfl.isEMpty()){
		    if(prfl[0].name == 'M2M Service 2nd Level' || prfl[0].name == '1st Level - DSC - READ permissions'){
		        disableButons = true;
		    }
		    if(prfl[0].name == '1st Level - DSC - READ permissions'){
		        hideAllButtonsfor1stLevel = true;
		    }
		}
		//If logged in user assigned with Billing_Console_Permisisons permisison set , then disabling all buttons billing console.
		List<PermissionSet> PermissionSetLst= new list<PermissionSet>([select id,NAme from PermissionSet where Name='Billing_Console_Permisisons']);
        if(PermissionSetLst.size() > 0){
        	list<PermissionSetAssignment> PermissionSetAssignmentLst= new list<PermissionSetAssignment>([SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId=:PermissionSetLst[0].id and AssigneeId=:UserInfo.getUserID()]);
        	if(PermissionSetAssignmentLst.size() > 0){
        		disableButons = true;
        	}
        }
		
		
		initiatePage();
		
		
	}
	
	public void initiatePage() {
	        
	        List<Daily_Bill__c> listDB = [select id,name,Date__c,Total_Amount__c,Number_of_Line_Items__c,Prebill_Responses__c,Prebill_Requests_Sent__c,Daily_Bill_Upload_Status__c,Archiving_Status__c,Official_Invoice_Date__c,Real_Daily_Bill__c  from Daily_Bill__c order by Date__c desc];
	        selectItems = new List<SelectOption>();
	        
	        String currentDBId = ApexPages.currentPage().getParameters().get('daily_bill');
	        
	        
	        
	        if (!listDB.isEmpty())
	        {
	            if (currentDBId!=null)
    	        {
    	            dailyBillObject = [select id,name,Date__c,Total_Amount__c,Number_of_Line_Items__c,Prebill_Responses__c,Prebill_Requests_Sent__c,Daily_Bill_Upload_Status__c,Archiving_Status__c,Official_Invoice_Date__c from Daily_Bill__c where id=:currentDBId];
    	            dailybill = dailyBillObject.id;
    	        }
    	        else dailyBillObject=listDB[0];
    	        for (Daily_Bill__c item : listDB)
    	        {
        	        //Added the below if condition by Mahaboob on 4th Jan 2015
        	        if(item.Real_Daily_Bill__c){
        	            selectItems.add(new SelectOption( String.valueOf(item.id), String.valueOf(item.Date__c)));
        	        }
        	        //Done by Mahaboob
    	        }
	        }
	    
	        system.debug('currentDBId>>>> ** '+currentDBId);
	        system.debug('dailybill>>>> ** '+dailybill);
	        system.debug('dailyBillObject>>>> ** '+dailyBillObject);
	        
	    
	        dailyBillItemsList= [Select Id,Rechnungsnummer__c,Re_Datum__c,Buchungsdatum__c, VO_Nr__c,Beschreibung__c,Art__c, Menge__c,Betrag__c,Einzel_Nettobetrag__c,
	        Einzel_Taxbetrag__c,Einzel_Bruttobetrag__c,Gesamt_Nettobetrag__c,Gesamt_Taxbetrag__c,Gesamt_Bruttobetrag__c, 
	        Waehrung__c,Steuersatz__c,Konto__c,Kost__c,Prod__c,Unique_Identifier__c,I_CO__c,Projekt__c,Anweisung__c
	        from Daily_Bill_Item__c where daily_bill__c=:dailyBillObject.id];
	       
	        
	        prebillList = [select Billing_Account__r.Name,Total_Gross_Amount__c,Billing_Account__r.OFI_id__c,Total_Tax_Amount__c,Total_Net_Amount__c from Prebill__c where daily_bill__c=:dailyBillObject.id LIMIT:1000];
	        
	        attList = [select Id, Name, Description, LastModifiedDate,LastModifiedById
                    from Attachment where parentid=:dailyBillObject.id order by LastModifiedDate desc  LIMIT:1000 ];
	        
	        
	         List<Id> idList = new List<Id>();
	        for(Daily_Bill_Item__c c : dailyBillItemsList ) {
	            idList.add(c.Id);
	        }
	        
	        List<AggregateResult> li = [Select COUNT_DISTINCT(invoice_number__c) c FROM prebill__c WHERE daily_bill__c= :dailyBillObject.id];
	        
	        if (li.isEmpty()) numberOfPrebills=0;
	        else
	        numberOfPrebills = (Integer)li[0].get('c');
	        
	        history = [Select Field,NewValue,OldValue, CreatedBy.Name, CreatedDate From Daily_bill_item__History where field not in ('created','Name') and parentid in :idList order by CreatedDate DESC LIMIT:1000];
	   
	        reportList = new List<SelectOption>();
	        reportList.add(new SelectOption('00Ob00000049lSK','M2M Forecast report'));
	        reportList.add(new SelectOption('00Ob00000049lSL','M2M Revenue'));
	        reportList.add(new SelectOption('00Ob0000003rToW','Gutschrifts'));
	        reportList.add(new SelectOption('00Ob0000003rToY','Storno'));
	        reportList.add(new SelectOption('00Ob00000049lSJ','Billing report compare'));
	        
	        
	        reportid='00Ob00000049lSK';


	        refreshFilters();
	        //reportFilter = reportFilters.get(reportid);
	        //reportFilter = '[{column:\'csord__Order__c.csord__Start_Date__c\',operator:\'greaterThan\',value:\''+ '2013-06-01' +'\'}, {column:\'csord__Order__c.csord__Start_Date__c\',operator:\'lessThan\',value:\''+ '2015-06-01' +'\'}]';
		}
		
		public void refreshFilters() {
		    reportFilters = new Map<String,String>();

	        
	        reportFilters.put('00Ob00000049lSK','[{column:\'Billing_Report__c.Billing_Date__c\',operator:\'greaterThan\',value:\''+ datetime.newInstance(dailyBillObject.Date__c.addDays(-1), time.newInstance(0,0,0,0)).format('yyyy-MM-dd') +'\'}, {column:\'Billing_Report__c.Billing_Date__c\',operator:\'lessThan\',value:\''+ datetime.newInstance(dailyBillObject.Date__c.addmonths(1) , time.newInstance(0,0,0,0)).format('yyyy-MM-dd') +'\'}]');
	        reportFilters.put('00Ob00000049lSL','[{column:\'Billing_Report__c.Billing_Date__c\',operator:\'greaterThan\',value:\''+ datetime.newInstance(dailyBillObject.Date__c.addDays(-1), time.newInstance(0,0,0,0)).format('yyyy-MM-dd') +'\'}, {column:\'Billing_Report__c.Billing_Date__c\',operator:\'lessThan\',value:\''+ datetime.newInstance(dailyBillObject.Date__c.addmonths(1) , time.newInstance(0,0,0,0)).format('yyyy-MM-dd') +'\'}]');
	        reportFilters.put('00Ob0000003rToW','[{column:\'Gutschrift__c.CreatedDate\',operator:\'greaterThan\',value:\''+ datetime.newInstance(dailyBillObject.Date__c.addDays(-1), time.newInstance(0,0,0,0)).format('yyyy-MM-dd') +'\'}, {column:\'Gutschrift__c.CreatedDate\',operator:\'lessThan\',value:\''+ datetime.newInstance(dailyBillObject.Date__c.addmonths(1) , time.newInstance(0,0,0,0)).format('yyyy-MM-dd') +'\'}]');
	        reportFilters.put('00Ob0000003rToY','[{column:\'Storno__c.CreatedDate\',operator:\'greaterThan\',value:\''+ datetime.newInstance(dailyBillObject.Date__c.addDays(-1), time.newInstance(0,0,0,0)).format('yyyy-MM-dd') +'\'}, {column:\'Storno__c.CreatedDate\',operator:\'lessThan\',value:\''+ datetime.newInstance(dailyBillObject.Date__c.addmonths(1) , time.newInstance(0,0,0,0)).format('yyyy-MM-dd') +'\'}]');
	        reportFilters.put('00Ob00000049lSJ','[{column:\'Billing_Report__c.Billing_Date__c\',operator:\'greaterThan\',value:\''+ datetime.newInstance(dailyBillObject.Date__c.addDays(-1), time.newInstance(0,0,0,0)).format('yyyy-MM-dd') +'\'}, {column:\'Billing_Report__c.Billing_Date__c\',operator:\'lessThan\',value:\''+ datetime.newInstance(dailyBillObject.Date__c.addmonths(1) , time.newInstance(0,0,0,0)).format('yyyy-MM-dd') +'\'}]');
	 	    reportFilter = reportFilters.get(reportid);
		}
		
		
		public void refreshReports() {
	    
	        reportList = new List<SelectOption>();
	        reportList.add(new SelectOption('00Ob00000049lSK','M2M Forecast report'));
	        reportList.add(new SelectOption('00Ob00000049lSL','M2M Revenue'));
	        reportList.add(new SelectOption('00Ob0000003rToW','Gutschrifts'));
	        reportList.add(new SelectOption('00Ob0000003rToY','Storno'));
	        reportList.add(new SelectOption('00Ob00000049lSJ','Billing report compare'));
	      
	        
	        refreshFilters();
	        
	       // reportFilter = reportFilters.get(reportid);
	    }
		
		public void refreshLists() {
		    
		    system.debug('dailybill>>>> ** '+dailybill);
		    
		    List<Daily_Bill__c> listDB = [select id,name,Date__c,Total_Amount__c,Number_of_Line_Items__c,Prebill_Responses__c,Prebill_Requests_Sent__c,Daily_Bill_Upload_Status__c,Archiving_Status__c,Official_Invoice_Date__c  from Daily_Bill__c where Prebill_Requests_Sent__c>0 and id=:dailybill order by Date__c desc];
	        
	        if (!listDB.isEmpty())
	        {
	        dailyBillObject=listDB[0];
	        }
		    
		 	dailyBillItemsList= [Select Rechnungsnummer__c,Re_Datum__c,Buchungsdatum__c, VO_Nr__c,Beschreibung__c,Art__c, Menge__c,Betrag__c,Einzel_Nettobetrag__c,
	        Einzel_Taxbetrag__c,Einzel_Bruttobetrag__c,Gesamt_Nettobetrag__c,Gesamt_Taxbetrag__c,Gesamt_Bruttobetrag__c, 
	        Waehrung__c,Steuersatz__c,Konto__c,Kost__c,Prod__c,Unique_Identifier__c,I_CO__c,Projekt__c,Anweisung__c
	        from Daily_Bill_Item__c where daily_bill__c=:dailyBillObject.id];
	        
	        prebillList = [select Billing_Account__r.Name,Billing_Account__r.OFI_id__c,Total_Gross_Amount__c,Total_Tax_Amount__c,Total_Net_Amount__c from Prebill__c where daily_bill__c=:dailyBillObject.id LIMIT:1000];
	        
	        attList = [select Id, Name, Description, LastModifiedDate,LastModifiedById
                    from Attachment where parentid=:dailyBillObject.id order by LastModifiedDate desc  LIMIT:1000 ];
	        
	        List<Id> idList = new List<Id>();
	        for(Daily_Bill_Item__c c : dailyBillItemsList ) {
	            idList.add(c.Id);
	        }
	        
	        history = [Select Field,NewValue,OldValue, CreatedBy.Name, CreatedDate From Daily_bill_item__History where field not in ('created','Name') and parentid in :idList order by CreatedDate DESC LIMIT:1000];
	        
	       List<AggregateResult> li = [Select COUNT_DISTINCT(invoice_number__c) c FROM prebill__c WHERE daily_bill__c= :dailyBillObject.id];
	        
	        if (li.isEmpty()) numberOfPrebills=0;
	        else
	        numberOfPrebills = (Integer)li[0].get('c');
	        
            refreshFilters();
            
            
		}
		
		public PageReference CreateDBF() {
		    
		    //List<Daily_Bill_Item__c> delList = [Select id from Daily_Bill_Item__c  where Daily_Bill__c=:dailyBillObject.id];
                    
            //delete delList;

		    /*FinalPrebillFileCSVGenerator c = new FinalPrebillFileCSVGenerator(dailyBillObject.date__c);  
		    Database.executeBatch(c); */

		    //23.2.2017 Petar MAtkovic added gesamt calculation before dbf created
		    BatchCalculateDBIGesamtAndCreateDBF c = new BatchCalculateDBIGesamtAndCreateDBF(dailyBillObject);  
		    Database.executeBatch(c);
		    addMessage('Daily bill creation process started. Please, check billing email address for details.', ApexPages.severity.INFO);
			return ApexPages.currentPage();
		}


        public PageReference CreateStorno() {
        // Storno__c storno = new Storno__c();
        // storno.Daily_Bill__c=dailyBillObject.id;
         
        // insert storno;
            
            PageReference newocp = new PageReference('/a2I/e?retURL=apex/billingconsole?daily_bill=' + dailyBillObject.id + '&saveURL=apex/billingconsole?daily_bill='+ dailyBillObject.id + '&CF00Nb0000009rwtq=' + dailyBillObject.name);
		    newocp.setRedirect(true);
		
		    return newocp;
            //https://cs20.salesforce.com/a2Gm0000000D3qV/e?retURL=apex/billingconsole
        }
        
        
         public PageReference CreateGutschrift() {
            //Gutschrift__c gut = new Gutschrift__c();
            //gut.Daily_Bill__c=dailyBillObject.id;
           
            //insert gut;
            
            PageReference newocp = new PageReference('/a2H/e?retURL=apex/billingconsole?daily_bill=' + dailyBillObject.id + '&saveURL=apex/billingconsole?daily_bill='+ dailyBillObject.id +'&CF00Nb0000009rwtc=' + dailyBillObject.name);
		    newocp.setRedirect(true);
		
		    return newocp;
            //https://cs20.salesforce.com/a2Gm0000000D3qV/e?retURL=apex/billingconsole
        }
        
          public PageReference CreateCharge() {
           // Daily_Bill_Item__c gut = new Daily_Bill_Item__c();
        //    gut.Daily_Bill__c=dailyBillObject.id;
         //   gut.Einzel_Nettobetrag__c=0;
          //  gut.Steuersatz__c=19;
            //gut.Konto__c='4120100';
            //gut.Kost__c='465605';
            //gut.Menge__c = 0;
            //insert gut;
            
            PageReference newocp = new PageReference('/apex/createadditionalCharge?daily_bill_id='+dailyBillObject.id + '&retURL=apex/billingconsole?daily_bill=' + dailyBillObject.id);
            newocp.setRedirect(true);
		
		    return newocp;
            //https://cs20.salesforce.com/a2Gm0000000D3qV/e?retURL=apex/billingconsole
        }
        
        public PageReference OpenDailyBill() {
            
            PageReference newocp = new PageReference('/' + dailyBillObject.id);
		    newocp.setRedirect(true);
		
		    return newocp;
            //https://cs20.salesforce.com/a2Gm0000000D3qV/e?retURL=apex/billingconsole
        }
        
        
           
        public PageReference gotoHome() {
            
            PageReference newocp = new PageReference('/home/home.jsp');
		    newocp.setRedirect(true);
		
		    return newocp;
            //https://cs20.salesforce.com/a2Gm0000000D3qV/e?retURL=apex/billingconsole
        }
        
        public void saveDailyBillItems() {
                     update dailyBillItemsList;
                    }
                    
			private void addMessage(String message, ApexPages.severity severity) {
		ApexPages.addMessage(new ApexPages.Message(severity, message));
	}

		public void generateBills() {
			 boolean createTicketReports = true;
			CustomerMonthlyBillService.createBills(dailyBillObject.id, false, '', createTicketReports, true); 
		}	

		
		public void uploadDailyBill() {
            List<Attachment> alist = [SELECT Id, parentid  FROM Attachment WHERE ParentId = :dailyBillObject.id and (not name like '%RATE_PAYMENT%') and (not name like 'Sixt%') ORDER BY createddate DESC LIMIT 1]; 
            Attachment attachment = new Attachment();
            if (!alist.isEmpty()){
                attachment = alist[0];
              } 
            
            csam_t1.ObjectGraphCalloutHandler.AdditionalObjects addObjects = new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects();
            addObjects.typeName = 'Attachment';
            addObjects.ids = new Id[]{attachment.id};
    
            csam_t1.ObjectGraphCalloutHandler.createAndSendExtended('Upload Daily Bill', attachment.ParentId, new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects[]{addObjects});
            boolean createTicketReports = true;
			CustomerMonthlyBillService.createBills(dailyBillObject.id, false, '', createTicketReports, false);
			Database.executeBatch(new BatchRefreshForecastReport()); 
		}	
		
		public void archiveItems() {
		    
		String response = csam_t1.ObjectGraphCalloutHandler.createAndSend('Archive Files', dailyBillObject.id);    
		    
		}
		
		public void refreshForecastReport (){
			Database.executeBatch(new BatchRefreshForecastReport());
		    //P.M. Commented out and logic extracted to batch job
		    /*
		    Date startOfMonth = system.today().toStartOfMonth();
            Date endOfNMonth = startOfMonth.addMonths(2);
            endOfNMonth = endOfNMonth.addDays(-1);
        
        //=============== Delete the records to refresh
             List<billing_report__c> billingReportsForCleanupAll = [select id, Billing_Date__c from billing_report__c where Billing_Date__c  >=:startOfMonth AND Billing_Date__c <=:endOfNMonth];
        
            if(billingReportsForCleanupAll != null && billingReportsForCleanupAll .size() > 0){
                delete billingReportsForCleanupAll ;
            }
        
        //============================Order Line Item
            
            List<csord__Order_Line_Item__c> orderLineItems = [SELECT Id, Name, csord__Order__r.CreatedDate,csord__Order__r.csord__Status__c,csord__Order__r.Order_Request__r.Account__r.name, csord__Total_Price__c,Quantity__c, 
            Journal_Mapping__r.description__c, csord__Order__r.Order_Request__r.Billing_account__r.OFI_id__c, csord__Order__r.Order_Request__r.Billing_account__r.SM2M_Type__c FROM csord__Order_Line_Item__c where csord__Order__r.CreatedDate >=:startOfMonth AND csord__Order__r.CreatedDate <=:endOfNMonth];
        
            List<Billing_Report__c> billingReportsFromORL = new List<Billing_Report__c>();
        
            for (csord__Order_Line_Item__c orl :orderLineItems){
                Billing_Report__c billingReport = new Billing_Report__c();
                billingReport.Account__c=orl.csord__Order__r.Order_Request__r.Account__c;
                billingReport.Amount__c= orl.csord__Total_Price__c;
                billingReport.Billing_Date__c=orl.csord__Order__r.CreatedDate.date().addMonths(1).toStartOfMonth();
                billingReport.Description__c=orl.Journal_Mapping__r.description__c;
                billingReport.isBilled__c=false;
                billingReport.isForecast__c=true;
                billingReport.OFI_ID__c=orl.csord__Order__r.Order_Request__r.Billing_account__r.OFI_id__c;
                billingReport.Quantity__c=orl.Quantity__c;
                billingReport.Billing_Account_SM2M_Type__c = orl.csord__Order__r.Order_Request__r.Billing_account__r.SM2M_Type__c;
                
                billingReportsFromORL.add(billingReport);
            }
        
            insert billingReportsFromORL;
        
        
        //============================Daily Bill
        
            List<Daily_Bill_Item__c> dailyBillItems = [SELECT id,name,VO_Nr__c,Einzel_Nettobetrag__c,Re_Datum__c,Beschreibung__c,Menge__c, Daily_Bill__r.date__c from Daily_Bill_Item__c where Daily_Bill__r.date__c >=:startOfMonth AND Daily_Bill__r.date__c <=:endOfNMonth];
        
        
            List<String> ofiid = new List<string>();
            for (Daily_Bill_Item__c orl :dailyBillItems) {
                ofiid.add(orl.VO_Nr__c);
            }
            List<billing_account__c> billingAccounts = [Select payer__c,ofi_id__c, SM2M_Type__c from billing_account__c where ofi_id__c in :ofiid];
        
            Map<Id,billing_account__c> accountmap = new Map<Id,billing_account__c>();
        
            for (Daily_Bill_Item__c orl :dailyBillItems){
                for(billing_account__c ba :billingAccounts){
                    if(ba.ofi_id__c==orl.VO_Nr__c){ 
                        accountmap.put(orl.id,ba);
                    }
                }
            }
            
            List<Billing_Report__c> billingReportsFromDB = new List<Billing_Report__c>();
        
            for (Daily_Bill_Item__c orl :dailyBillItems){
                Billing_Report__c billingReport = new Billing_Report__c();
        
            if (!billingAccounts.isEmpty()){
                billingReport.Account__c=accountmap.get(orl.id).payer__c;
                billingReport.Billing_Account_SM2M_Type__c = accountmap.get(orl.id).SM2M_Type__c;
            }
        
                billingReport.Amount__c= orl.Einzel_Nettobetrag__c;
                billingReport.Billing_Date__c=orl.Daily_Bill__r.date__c.toStartOfMonth();
                billingReport.Description__c=orl.Beschreibung__c;
                billingReport.isBilled__c=true;
                billingReport.isForecast__c=false;
                billingReport.OFI_ID__c=orl.VO_Nr__c;
                billingReport.Quantity__c=orl.Menge__c;
                                
                billingReportsFromDB.add(billingReport);
            }
        
            insert billingReportsFromDB;
        
        //========================================= 
        
            List<Daily_Bill_Item__c> ll = [SELECT id,name,VO_Nr__c,Einzel_Nettobetrag__c,Re_Datum__c,Beschreibung__c,Menge__c, Daily_Bill__r.date__c from Daily_Bill_Item__c where Daily_Bill__r.date__c >=:startOfMonth AND Daily_Bill__r.date__c <=:endOfNMonth];
            
            
            List<String> ofiid2= new List<string>();
            for (Daily_Bill_Item__c orl :ll)
                ofiid2.add(orl.VO_Nr__c);
            
            List<billing_account__c> al = [Select payer__c,ofi_id__c, SM2M_Type__c from billing_account__c where ofi_id__c in :ofiid2];
            
            Map<Id,billing_account__c> accountmap2 = new Map<Id,billing_account__c>();
            for (Daily_Bill_Item__c orl :ll)
            for(billing_account__c ba :al)
            if(ba.ofi_id__c==orl.VO_Nr__c) accountmap2.put(orl.id,ba);
            
            
            List<Billing_Report__c> brl = new List<Billing_Report__c>();
            
            for (Daily_Bill_Item__c orl :ll){
                Billing_Report__c brc = new Billing_Report__c();
            
            if (!al.isEmpty()){
                brc.Account__c=accountmap2.get(orl.id).payer__c;
                brc.Billing_Account_SM2M_Type__c =accountmap2.get(orl.id).SM2M_Type__c;
            }
            
                brc.Amount__c= orl.Einzel_Nettobetrag__c;
                brc.Billing_Date__c=orl.Daily_Bill__r.date__c.toStartOfMonth().addMonths(1);
                brc.Description__c=orl.Beschreibung__c;
                brc.isBilled__c=false;
                brc.isForecast__c=true;
                brc.OFI_ID__c=orl.VO_Nr__c;
                brc.Quantity__c=orl.Menge__c;
                
                brl.add(brc);
            } 
            
                insert brl;
            //======================================== Cleanup
            //select records for delete
            List<billing_report__c> billingReportsForCleanup = [select id, Billing_Date__c from billing_report__c where isForecast__c=true and ( description__c='Aktivierungsgebühr' OR description__c like 'Sim Gebühr%' OR description__c like 'GS%' ) AND Billing_Date__c  >=:startOfMonth AND Billing_Date__c <=:endOfNMonth];
        
            if(billingReportsForCleanup != null && billingReportsForCleanup.size() > 0){
                delete billingReportsForCleanup;
            }*/
		}
		
		
		public PageReference savedailyBillObject(){
		    try{
		        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info,'Save Successfull'));
		        update dailyBillObject;
		    }catch(exception e){
		        system.debug('Error while updating Official Invoice date :: '+e.getMessage());
		    }
		    return null;
		}
		
}