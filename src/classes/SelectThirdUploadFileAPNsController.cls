public with sharing class SelectThirdUploadFileAPNsController {

    private ApexPages.StandardSetController stdSetController;
    
    // proxy apn
    public APN__c apn { get; set; }
    public TufApnSelection apnSelection { get; set; }
    private Id tufId;

    public SelectThirdUploadFileAPNsController() {
        this.apn = new APN__c();
        
        this.tufId = ApexPages.currentPage().getParameters().get('id');
        this.apnSelection = new TufApnSelection(tufId);
    }
    
    public PageReference createAndAssign() {
        if (String.isBlank(apn.Name)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'APN Name: You must enter a value'));
            return null;
        } 
        
        if (!apn.Is_Default__c && apnSelection.getTuf().Order_Request__r.Account__c != apn.Account__c) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'One of the conditions must match: 1. Is Default should be true or 2. Account should match the TUF Order Request Account'));
            return null;
        }
        
        insert apn;
        insert new APN_TUF__c(APN__c = apn.Id, TUF__c = tufId, Name=apn.Name);
        
        return tufPage();
    }
    
    public PageReference confirm() {
        this.apnSelection.updateState();
        return tufPage();
    }
    
    private PageReference tufPage() {
        PageReference pageRef = new PageReference('/' + tufId);
        return pageRef;
    }
    
    public PageReference cancel() {
        return tufPage();
    }

    public class TufApnSelection {
        
        private Third_Upload_File__c tuf;
        private List<ApnChoice> apnChoices;
        private Map<Id, APN_TUF__c> apnTufsMap;
        public List<ApnChoice> choiceList { get; set; }
        
        public Third_Upload_File__c getTuf() {
            return tuf;
        }
               
        public TufApnSelection(Id tufId) {

            this.tuf = [SELECT Id, Name, Order_Request__r.Account__c FROM Third_Upload_File__c WHERE Id = :tufId];
            this.apnChoices = new List<ApnChoice>();
            this.choiceList = new List<ApnChoice>();
            this.apnTufsMap = new Map<Id, APN_TUF__c>();
            
            this.updateApnTufsMap(tuf.Id);
                        
            for (APN__c apnOption : availableApns(tufId)) {
                if (apnOption != null) {
                    apnChoices.add(new ApnChoice(apnOption, apnTufsMap.containsKey(apnOption.Id)));
                    choiceList.add(new ApnChoice(apnOption, apnTufsMap.containsKey(apnOption.Id)));
                }
            }
        }
        
        public void updateState() {
            
            Set<Id> previousSelectedIds = selectedApnIds(apnChoices);
            Set<Id> currentSelectedIds = selectedApnIds(choiceList);
            
            Set<Id> newIds = currentSelectedIds.clone();
            newIds.removeAll(previousSelectedIds);
            
            Set<Id> removedIds = previousSelectedIds.clone();
            removedIds.removeAll(currentSelectedIds);
             
            List<Apn_Tuf__c> apnTufsToDelete = [Select Id from APN_TUF__c where TUF__c = :tuf.Id and APN__c in :removedIds];
            delete apnTufsToDelete;
            
            List<Apn_Tuf__c> apnTufsIn = new List<Apn_Tuf__c>();
            // APN__c Name is fetched to populate on Name on junction object APN_TUF__c
            List<APN__c> apnList = [Select Id, Name from APN__c where Id IN : newIds];
            for (APN__c apnObj : apnList) {
               apnTufsIn.add(new APN_TUF__c(TUF__c = tuf.Id, APN__c = apnObj.Id, Name=apnObj.Name));
            }
            
            if (!apnTufsIn.isEmpty()) {
                upsert apnTufsIn;
                for (APN_TUF__c apnTuf : apnTufsIn) {
                    apnTufsMap.put(apnTuf.APN__c, apnTuf);
                }
            }
        }
        
        private List<APN__c> availableApns(Id tufId) {
           return [
               SELECT 
                   Id, Name, Account__c, Domain__c, Filter_Type__c, IP_Mask__c, Port_or_Port_Range__c, Protocol_Number__c, Is_Default__c 
               FROM 
                   APN__c 
               WHERE 
                   Is_Default__c = true OR Account__c = :tuf.Order_Request__r.Account__c
            ]; 
        }
        
        private void updateApnTufsMap(Id tufId) {
            for (APN_TUF__c apnTuf : [SELECT Id, Name, APN__c FROM APN_TUF__c WHERE TUF__c = :tufId]) {
                if (apnTuf != null) {
                    this.apnTufsMap.put(apnTuf.APN__c, apnTuf); 
                }
            }
        }
        
        private Set<Id> selectedApnIds(List<ApnChoice> choices) {
            Set<Id> selectedIds = new Set<Id>();
            for (ApnChoice choice : choices) {
                if (choice.isChecked) {
                    selectedIds.add(choice.apn.Id);
                }
            }
            return selectedIds;
        }
    }

    public class ApnChoice {
        public APN__c apn { get; set; }
        public Boolean isChecked { get; set; }

        public ApnChoice(APN__c apn, Boolean isChecked) {
            this.apn = apn;
            this.isChecked = isChecked;
        }
    }
}