/*
* Description: Test class for AllCaseTriggers & related handler class
*/
@isTest (SeeAllData=false)
class TestAllCaseTriggers{

    /*
    * Validate Case trigger call.
    */
    testMethod static void validateCaseTriggers() {

        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers;
        // Insert account
        List<Account> accList = new List<Account>();
        Account acc1 = new Account(Company_Email__c = 'company@email.com',
        Unique_Business_Number__c = 'UBN 123', BillingCountry = 'Ireland', BillingStreet = 'Warwick street 15', BillingCity = 'Cork',
        Jurisdiction__c = 'Jurisdiction', VO_Number__c = '4567898', VAT__c = 12.50, Phone = '12345', Type = 'Customer',
        BillingPostalCode = '57000', Sales_Employee__c = 'John Voight', Frame_Contract__c = false, Website = 'www', Name = 'CS Service Test Acct');
        accList.add(acc1);

        Account acc2 = new Account(Company_Email__c = 'company@email.com',
        Unique_Business_Number__c = 'UBN 12344', BillingCountry = 'Ireland', BillingStreet = 'Warwick street 15', BillingCity = 'Cork',
        Jurisdiction__c = 'Jurisdiction', VO_Number__c = '45678989', VAT__c = 12.50, Phone = '12345', Type = 'Customer',
        BillingPostalCode = '57000', Sales_Employee__c = 'John Voight', Frame_Contract__c = false, Website = 'www', Name = 'CS Service Test Acct');
        accList.add(acc2);
        insert accList;

        Asset a = new Asset(Name = 'AssetNameTest', AccountId=accList[0].Id);
        insert a;
        Asset a1 = new Asset(Name = 'AssetNameTest2', AccountId=accList[1].Id);
        insert a1;

        List<Entitlement> enList = new List<Entitlement>();
            enList.add(new Entitlement(Name = 'TestEntitlement123', AccountId = accList[0].Id, AssetId=a.Id, EndDate=System.Today()+1, StartDate =System.Today()-1));
            enList.add(new Entitlement(Name = 'TestEntitlement124', AccountId = accList[1].Id, AssetId=a1.Id, EndDate=System.Today()+1, StartDate =System.Today()-1));
        insert enList;

        notriggers.Flag__c = false;
        update notriggers;

        Test.StartTest();

        // Insert Case records
        List<Case> caseList = new List<Case>();
        caseList.add(new Case(Category__c= 'Service', Subcategory__c = 'Complaints',Status='Open', AccountId=accList[0].Id, Subject='Test Demo111', SLA_Time__c='24', Type='Standard', Origin='Email', Priority='High', SuppliedEmail='test@t.com'));
        insert caseList;

        // Create CaseEmail
        notriggers.Flag__c = true;
        update notriggers;
        // create user
        User u = createUser(1);
        EmailMessage newCaseEmail = new EmailMessage(FromAddress = 'someone@test.com', Incoming = True, ToAddress= 'someone@salesforce.com',
                                    Subject = 'Test email', TextBody = 'Test Data', ParentId = caseList[0].Id);
        insert newCaseEmail;

        notriggers.Flag__c = false;
        update notriggers;

        caseList[0].OwnerId = u.Id;
        caseList[0].Status = 'Open';
        caseList[0].AccountId = accList[1].Id;
        caseList[0].SuppliedEmail =  'test@t1.com';
        caseList[0].Category__c= 'Service';
        caseList[0].Subcategory__c = 'Complaints';
  //      update caseList;

        // delete case list
 //       delete caseList;
        // undelete case list
//        undelete caseList;

        Test.StopTest();
    }

    /*
    * Validate Case trigger call  with email containing "." .
    */
    testMethod static void validateCaseTriggers2() {

        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers;
        // Insert account
        List<Account> accList = new List<Account>();
        Account acc1 = new Account(Company_Email__c = 'company@email.com',
        Unique_Business_Number__c = 'UBN 123', BillingCountry = 'Ireland', BillingStreet = 'Warwick street 15', BillingCity = 'Cork',
        Jurisdiction__c = 'Jurisdiction', VO_Number__c = '4567898', VAT__c = 12.50, Phone = '12345', Type = 'Customer',
        BillingPostalCode = '57000', Sales_Employee__c = 'John Voight', Frame_Contract__c = false, Website = 'www', Name = 'CS Service Test Acct');
        accList.add(acc1);

        Account acc2 = new Account(Company_Email__c = 'company@email.com',
        Unique_Business_Number__c = 'UBN 12344', BillingCountry = 'Ireland', BillingStreet = 'Warwick street 15', BillingCity = 'Cork',
        Jurisdiction__c = 'Jurisdiction', VO_Number__c = '45678989', VAT__c = 12.50, Phone = '12345', Type = 'Customer',
        BillingPostalCode = '57000', Sales_Employee__c = 'John Voight', Frame_Contract__c = false, Website = 'www', Name = 'CS Service Test Acct');
        accList.add(acc2);
        insert accList;

        Asset a = new Asset(Name = 'AssetNameTest', AccountId=accList[0].Id);
        insert a;
        Asset a1 = new Asset(Name = 'AssetNameTest2', AccountId=accList[1].Id);
        insert a1;

        List<Entitlement> enList = new List<Entitlement>();
            enList.add(new Entitlement(Name = 'TestEntitlement123', AccountId = accList[0].Id, AssetId=a.Id, EndDate=System.Today()+1, StartDate =System.Today()-1));
            enList.add(new Entitlement(Name = 'TestEntitlement124', AccountId = accList[1].Id, AssetId=a1.Id, EndDate=System.Today()+1, StartDate =System.Today()-1));
        insert enList;

        notriggers.Flag__c = false;
        update notriggers;

        Test.StartTest();

        // Insert Case records
        List<Case> caseList = new List<Case>();
        caseList.add(new Case(Category__c= 'Service', Subcategory__c = 'Complaints',Status='Open', AccountId=accList[0].Id, Subject='Test Demo111', SLA_Time__c='24', Type='Standard', Origin='Email', Priority='High', SuppliedEmail='test.test@t.com'));
        insert caseList;

        // Create CaseEmail
        notriggers.Flag__c = true;
        update notriggers;
        // create user
        User u = createUser(2);
        EmailMessage newCaseEmail = new EmailMessage(FromAddress = 'someone@test.com', Incoming = True, ToAddress= 'someone@salesforce.com',
                                    Subject = 'Test email', TextBody = 'Test Data', ParentId = caseList[0].Id);
        insert newCaseEmail;

        notriggers.Flag__c = false;
        update notriggers;

        caseList[0].OwnerId = u.Id;
        caseList[0].Status = 'Open';
        caseList[0].AccountId = accList[1].Id;
        caseList[0].SuppliedEmail =  'test.test@t1.com';
        caseList[0].Category__c= 'Service';
        caseList[0].Subcategory__c = 'Complaints';
 //       update caseList;

        // delete case list
//        delete caseList;
        // undelete case list
 //       undelete caseList;

        Test.StopTest();
    }

    //--------------------------------------------------------------------------
    // Create test user
    //--------------------------------------------------------------------------
    private static User createUser(Integer i)
    {
        // Setup test data
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = i+'testu', Email='testuser@i.com',
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', UserName=i+'testuser@i.com');
        insert u;
        return u;
    }
}