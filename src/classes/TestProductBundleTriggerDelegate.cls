@isTest
private class TestProductBundleTriggerDelegate {

	static testMethod void testSingleBundleContractTermPerSIM() {

		Integer shorterContractTerm = 6;
		Integer contractTerm = 36;
		String productDefinitionName = 'Connectivity';

		Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1),
			Description = 'Test description 20 characters'
		);

		insert opp;

		Contact contact = new Contact(
			FirstName = 'Test',
			Email = 'test@email.com',
			LastName = 'Contact'
		);

		insert contact;

		OpportunityContactRole ocr = new OpportunityContactRole(
			contactId = contact.id,
			opportunityId = opp.id
		);

		insert ocr;

		cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(
			cscfga__Synchronised_with_Opportunity__c = false,
			cscfga__Opportunity__c = opp.Id
		);

		insert productBundle;

		cscfga__Product_Definition__c productDefinition = new cscfga__Product_Definition__c(
			Name = productDefinitionName,
			cscfga__Description__c = 'Test description'
		);

		insert productDefinition;

		List<cscfga__Product_Configuration__c> productConfigurations = new List<cscfga__Product_Configuration__c>();

		productConfigurations.add(new cscfga__Product_Configuration__c(
			cscfga__Contract_Term__c = shorterContractTerm,
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinition.Id
		));

		productConfigurations.add(new cscfga__Product_Configuration__c(
			cscfga__Contract_Term__c = contractTerm,
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinition.Id
		));

		insert productConfigurations;

		productBundle.cscfga__Synchronised_with_Opportunity__c = true;
		update productBundle;

		Opportunity updatedOpp = [Select Contract_Term_Per_SIM__c from Opportunity where Id = :opp.Id];
		System.assertEquals(contractTerm, updatedOpp.Contract_Term_Per_SIM__c);
	}

	static testMethod void testSingleBundleContractTermPerSubscription() {

		Integer shorterContractTerm = 6;
		Integer contractTerm = 36;
		String productDefinitionName = 'Support & Service';

		Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1),
			Description = 'Test description 20 characters'
		);

		insert opp;

		Contact contact = new Contact(
			FirstName = 'Test',
			Email = 'test@email.com',
			LastName = 'Contact'
		);

		insert contact;

		OpportunityContactRole ocr = new OpportunityContactRole(
			contactId = contact.id,
			opportunityId = opp.id
		);

		insert ocr;

		cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(
			cscfga__Synchronised_with_Opportunity__c = false,
			cscfga__Opportunity__c = opp.Id
		);

		insert productBundle;

		cscfga__Product_Definition__c productDefinition = new cscfga__Product_Definition__c(
			Name = productDefinitionName,
			cscfga__Description__c = 'Test description'
		);

		insert productDefinition;

		List<cscfga__Product_Configuration__c> productConfigurations = new List<cscfga__Product_Configuration__c>();

		productConfigurations.add(new cscfga__Product_Configuration__c(
			cscfga__Contract_Term__c = shorterContractTerm,
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinition.Id
		));

		productConfigurations.add(new cscfga__Product_Configuration__c(
			cscfga__Contract_Term__c = contractTerm,
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinition.Id
		));

		insert productConfigurations;

		productBundle.cscfga__Synchronised_with_Opportunity__c = true;
		update productBundle;

		Opportunity updatedOpp = [Select Contract_Term_Per_Subscription__c from Opportunity where Id = :opp.Id];
		System.assertEquals(contractTerm, updatedOpp.Contract_Term_Per_Subscription__c);
	}

	static testMethod void testBundleContractTerms() {

		Integer simContractTerm = 10;
		Integer serviceContractTerm = 11;

		Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1),
			Description = 'Test description 20 characters'
		);

		insert opp;

		Contact contact = new Contact(
			FirstName = 'Test',
			Email = 'test@email.com',
			LastName = 'Contact'
		);

		insert contact;

		OpportunityContactRole ocr = new OpportunityContactRole(
			contactId = contact.id,
			opportunityId = opp.id
		);

		insert ocr;

		List<cscfga__Product_Bundle__c> productBundles = new List<cscfga__Product_Bundle__c>();

		productBundles.add(new cscfga__Product_Bundle__c(
			cscfga__Synchronised_with_Opportunity__c = false,
			cscfga__Opportunity__c = opp.Id
		));

		productBundles.add(new cscfga__Product_Bundle__c(
			cscfga__Synchronised_with_Opportunity__c = false,
			cscfga__Opportunity__c = opp.Id
		));

		insert productBundles;

		List<cscfga__Product_Definition__c> productDefinitions = new List<cscfga__Product_Definition__c>();

		productDefinitions.add(new cscfga__Product_Definition__c(
			Name = 'Connectivity',
			cscfga__Description__c = 'Test description'
		));

		productDefinitions.add(new cscfga__Product_Definition__c(
			Name = 'Support & Service',
			cscfga__Description__c = 'Test description'
		));

		insert productDefinitions;

		List<cscfga__Product_Configuration__c> productConfigurations = new List<cscfga__Product_Configuration__c>();

		productConfigurations.add(new cscfga__Product_Configuration__c(
			cscfga__Contract_Term__c = simContractTerm,
			cscfga__Product_Bundle__c = productBundles[0].Id,
			cscfga__Product_Definition__c = productDefinitions[0].Id
		));

		productConfigurations.add(new cscfga__Product_Configuration__c(
			cscfga__Contract_Term__c = serviceContractTerm,
			cscfga__Product_Bundle__c = productBundles[1].Id,
			cscfga__Product_Definition__c = productDefinitions[1].Id
		));

		insert productConfigurations;

		productBundles[0].cscfga__Synchronised_with_Opportunity__c = true;
		productBundles[1].cscfga__Synchronised_with_Opportunity__c = true;
		update productBundles;

		Opportunity updatedOpp = [Select Contract_Term_Per_SIM__c, Contract_Term_Per_Subscription__c from Opportunity where Id = :opp.Id];
		System.assertEquals(simContractTerm, updatedOpp.Contract_Term_Per_SIM__c);
		System.assertEquals(serviceContractTerm, updatedOpp.Contract_Term_Per_Subscription__c);
	}

	static testMethod void testMonthlyRevenuePerSIM() {

		String productDefinitionName = 'Connectivity';

		Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1),
			Description = 'Test description 20 characters'
		);

		insert opp;

		Contact contact = new Contact(
			FirstName = 'Test',
			Email = 'test@email.com',
			LastName = 'Contact'
		);

		insert contact;

		OpportunityContactRole ocr = new OpportunityContactRole(
			contactId = contact.id,
			opportunityId = opp.id
		);

		insert ocr;

		cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(
			cscfga__Synchronised_with_Opportunity__c = false,
			cscfga__Opportunity__c = opp.Id
		);

		insert productBundle;

		cscfga__Product_Definition__c productDefinition = new cscfga__Product_Definition__c(
			Name = productDefinitionName,
			cscfga__Description__c = 'Test description'
		);

		insert productDefinition;

		List<cscfga__Product_Configuration__c> productConfigurations = new List<cscfga__Product_Configuration__c>();

		productConfigurations.add(new cscfga__Product_Configuration__c(
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinition.Id
		));

		productConfigurations.add(new cscfga__Product_Configuration__c(
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinition.Id
		));

		insert productConfigurations;

		List<cscfga__Attribute__c> attributes = new List<cscfga__Attribute__c>();

		attributes.add(new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfigurations[0].Id,
			cscfga__Price__c = 120.0,
			cscfga__Is_Line_Item__c = true,
			cscfga__Recurring__c = true
		));

		attributes.add(new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfigurations[0].Id,
			Name = 'Quantity',
			cscfga__Value__c = '10'
		));

		attributes.add(new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfigurations[1].Id,
			cscfga__Price__c = 100.0,
			cscfga__Is_Line_Item__c = true,
			cscfga__Recurring__c = true
		));

		attributes.add(new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfigurations[1].Id,
			Name = 'Quantity',
			cscfga__Value__c = '10'
		));

		insert attributes;

		/*productBundle.cscfga__Synchronised_with_Opportunity__c = true;
		update productBundle;

		Opportunity updatedOpp = [Select Monthly_Revenue_Per_SIM__c from Opportunity where Id = :opp.Id];
		System.assertEquals(11.0, updatedOpp.Monthly_Revenue_Per_SIM__c);*/
	}

	static testMethod void testMonthlyRevenue() {

		Integer contractTerm = 12;



		Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1),
			Description = 'Test description 20 characters'
		);

		insert opp;

		Contact contact = new Contact(
			FirstName = 'Test',
			Email = 'test@email.com',
			LastName = 'Contact'
		);

		insert contact;

		OpportunityContactRole ocr = new OpportunityContactRole(
			contactId = contact.id,
			opportunityId = opp.id
		);

		insert ocr;

		cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(
			cscfga__Synchronised_with_Opportunity__c = false,
			cscfga__Opportunity__c = opp.Id
		);

		insert productBundle;

		List<cscfga__Product_Definition__c> productDefinitions = new List<cscfga__Product_Definition__c>();

		productDefinitions.add(new cscfga__Product_Definition__c(
			Name = 'Connectivity',
			cscfga__Description__c = 'Test description'
		));

		productDefinitions.add(new cscfga__Product_Definition__c(
			Name = 'Support & Service',
			cscfga__Description__c = 'Test description'
		));

		productDefinitions.add(new cscfga__Product_Definition__c(
			Name = 'Hardware',
			cscfga__Description__c = 'Test description'
		));

		insert productDefinitions;

		List<cscfga__Product_Configuration__c> productConfigurations = new List<cscfga__Product_Configuration__c>();

		productConfigurations.add(new cscfga__Product_Configuration__c(
			cscfga__Contract_Term__c = contractTerm,
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinitions[0].Id
		));

		productConfigurations.add(new cscfga__Product_Configuration__c(
			cscfga__Contract_Term__c = contractTerm,
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinitions[1].Id
		));

		productConfigurations.add(new cscfga__Product_Configuration__c(
			cscfga__Contract_Term__c = contractTerm,
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinitions[2].Id
		));

		insert productConfigurations;

		List<cscfga__Attribute__c> attributes = new List<cscfga__Attribute__c>();

		attributes.add(new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfigurations[0].Id,
			cscfga__Price__c = 120.0,
			cscfga__Is_Line_Item__c = true,
			cscfga__Recurring__c = true
		));

		attributes.add(new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfigurations[0].Id,
			Name = 'Quantity',
			cscfga__Value__c = '10'
		));

		attributes.add(new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfigurations[1].Id,
			cscfga__Price__c = 100.0,
			cscfga__Is_Line_Item__c = true,
			cscfga__Recurring__c = true
		));

		attributes.add(new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfigurations[1].Id,
			Name = 'Quantity',
			cscfga__Value__c = '10'
		));

		attributes.add(new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfigurations[2].Id,
			cscfga__Price__c = 120.0,
			cscfga__Is_Line_Item__c = true,
			cscfga__Recurring__c = true
		));

			attributes.add(new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfigurations[2].Id,
			cscfga__Price__c = 200.0,
			cscfga__Is_Line_Item__c = true,
			cscfga__Recurring__c = false
		));

		attributes.add(new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfigurations[2].Id,
			Name = 'Quantity',
			cscfga__Value__c = '10'
		));

		insert attributes;

		productBundle.cscfga__Synchronised_with_Opportunity__c = true;
		opp.Synched_Bundle_Id__c = productBundle.Id;
		update productBundle;

		//add one US to test deletion trigger.
		Unit_Schedule__c oldUS = new Unit_Schedule__c(
                    Year__c = String.valueOf(Date.today().year()),
                    Opportunity__c = opp.Id
                );
        insert oldUS;

		Opportunity updatedOpp = [Select Monthly_Revenue_Per_SIM__c, Monthly_Revenue_Per_Service__c from Opportunity where Id = :opp.Id];
		System.assertEquals(12.0, updatedOpp.Monthly_Revenue_Per_SIM__c);
		System.assertEquals(10.0, updatedOpp.Monthly_Revenue_Per_Service__c);
	}

	static testMethod void testMonthlyRevenueWithChildren() {

		Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1),
			Description = 'Test description 20 characters'
		);

		insert opp;

		Contact contact = new Contact(
			FirstName = 'Test',
			Email = 'test@email.com',
			LastName = 'Contact'
		);

		insert contact;

		OpportunityContactRole ocr = new OpportunityContactRole(
			contactId = contact.id,
			opportunityId = opp.id
		);

		insert ocr;

		cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(
			cscfga__Synchronised_with_Opportunity__c = false,
			cscfga__Opportunity__c = opp.Id
		);

		insert productBundle;

		List<cscfga__Product_Definition__c> productDefinitions = new List<cscfga__Product_Definition__c>();

		productDefinitions.add(new cscfga__Product_Definition__c(
			Name = 'Connectivity',
			cscfga__Description__c = 'Test description'
		));

		productDefinitions.add(new cscfga__Product_Definition__c(
			Name = 'Data package',
			cscfga__Description__c = 'Test description'
		));

		insert productDefinitions;

		cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c(
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinitions[0].Id
		);

		insert productConfiguration;

		cscfga__Product_Configuration__c childProductConfiguration = new cscfga__Product_Configuration__c(
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinitions[1].Id,
			parent_product_configuration__c = productConfiguration.Id
		);

		insert childProductConfiguration;

		List<cscfga__Attribute__c> attributes = new List<cscfga__Attribute__c>();

		attributes.add(new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfiguration.Id,
			cscfga__Price__c = 120.0,
			cscfga__Is_Line_Item__c = true,
			cscfga__Recurring__c = true
		));

		attributes.add(new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfiguration.Id,
			Name = 'Quantity',
			cscfga__Value__c = '10'
		));

		attributes.add(new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = childProductConfiguration.Id,
			cscfga__Price__c = 100.0,
			cscfga__Is_Line_Item__c = true,
			cscfga__Recurring__c = true
		));

		attributes.add(new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = childProductConfiguration.Id,
			Name = 'Quantity',
			cscfga__Value__c = '5'
		));

		insert attributes;

		productBundle.cscfga__Synchronised_with_Opportunity__c = true;
	/*	update productBundle;

		Opportunity updatedOpp = [Select Monthly_Revenue_Per_SIM__c from Opportunity where Id = :opp.Id];
		System.assertEquals(22.0, updatedOpp.Monthly_Revenue_Per_SIM__c);*/
	}

	static testMethod void testGeneratingUnitSchedules() {

		Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1),
			Description = 'Test description 20 characters'
		);

		insert opp;

		Contact contact = new Contact(
			FirstName = 'Test',
			Email = 'test@email.com',
			LastName = 'Contact'
		);

		insert contact;

		OpportunityContactRole ocr = new OpportunityContactRole(
			contactId = contact.id,
			opportunityId = opp.id
		);

		insert ocr;

		cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(
			cscfga__Synchronised_with_Opportunity__c = false,
			cscfga__Opportunity__c = opp.Id
		);

		insert productBundle;

		List<cscfga__Product_Definition__c> productDefinitions = new List<cscfga__Product_Definition__c>();

		productDefinitions.add(new cscfga__Product_Definition__c(
			Name = 'Connectivity',
			cscfga__Description__c = 'Test description'
		));

		productDefinitions.add(new cscfga__Product_Definition__c(
			Name = 'Data package',
			cscfga__Description__c = 'Test description'
		));

		insert productDefinitions;

		cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c(
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinitions[0].Id
		);

		insert productConfiguration;

		cscfga__Product_Configuration__c childProductConfiguration = new cscfga__Product_Configuration__c(
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinitions[1].Id,
			cscfga__Parent_Configuration__c = productConfiguration.Id
		);

		insert childProductConfiguration;

		List<cscfga__Attribute__c> attributes = new List<cscfga__Attribute__c>();

		attributes.add(new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfiguration.Id,
			cscfga__Price__c = 120.0,
			cscfga__Is_Line_Item__c = true,
			cscfga__Recurring__c = true
		));

		attributes.add(new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfiguration.Id,
			Name = 'Quantity',
			cscfga__Value__c = '10'
		));

		attributes.add(new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = childProductConfiguration.Id,
			cscfga__Price__c = 100.0,
			cscfga__Is_Line_Item__c = true,
			cscfga__Recurring__c = true
		));

		attributes.add(new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = childProductConfiguration.Id,
			Name = 'Quantity',
			cscfga__Value__c = '5'
		));

		insert attributes;

		productBundle.cscfga__Synchronised_with_Opportunity__c = true;
	//	update productBundle;

		/*List<Unit_Schedule__c> unitSchedules = [Select Id from Unit_Schedule__c where Opportunity__c = :opp.Id];
		System.assertEquals(1, unitSchedules.size());*/
	}
}