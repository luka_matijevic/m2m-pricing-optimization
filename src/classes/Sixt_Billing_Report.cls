/*******************************************************
*@@Description : To prepare Billing Report for Daily Bill
*@@VF Page: Sixt_Billing_Report
*@@Date: 09-Nov-15
*@@Test class: Sixt_Billing_Report_Test
*@@Author : Pavan 
*******************************************************/
public class Sixt_Billing_Report {
    
    public string columnHeaders;
    public string rows ='';
    public string csvData {get; set;}
    
    public Sixt_Billing_Report(ApexPages.StandardController std){ 
        columnHeaders ='Lieferantenbelegnummer,Rechnungsdatum (dd.MM.YYYY),Barcode,Sixt Vertragsnummer,Amtl. Kennzeichen,Artikelnr,Leistungszeitraum von,Leistungszeitraum bis,Menge,Nettobetrag,Umsatzsteuer in %,Hiso';
        Id dailyBillRecId = apexPages.currentPage().getParameters().get('id');
        if(dailyBillRecId != null){
            Daily_Bill__c dailyBill = [select id, Official_Invoice_Date__c from Daily_Bill__c where id =:dailyBillRecId];
            list<Prebill__c> preBills = new list<Prebill__c>();
            preBills = [select id,(select Id, Line_Item_Description__c, Net_Amount__c,
                                             Order_Line_Item__r.csord__Order__r.Order_Request__r.Frame_Contract__r.type__c,
                                             Order_Line_Item__r.csord__Order__r.Order_Request__r.Ticket__r.Sixt_LVID__c,
                                             Order_Line_Item__r.csord__Order__r.Order_Request__r.Ticket__r.Sixt_SIM_activation_date__c,
                                             Order_Line_Item__c, Service_Line_Item__c, Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c,
											 Service_Line_Item__r.csord__Service__r.csord__Deactivation_Date__c
                                   from Prebill_Line_Items__r 
                                   where (Order_Line_Item__c != null AND Order_Line_Item__r.csord__Order__r.Order_Request__r.Frame_Contract__r.type__c ='Sixt') OR Service_Line_Item__c != null) 
                        from Prebill__c 
                        where Daily_Bill__c =:dailyBillRecId];
                        
            set<id> orderIds = new set<id>();
            set<id> ordReqIds = new set<id>();
            
            for(Prebill__c preBill: preBills){
                if(preBill.Prebill_Line_Items__r != null && preBill.Prebill_Line_Items__r.size() > 0){
                    for(Prebill_Line_Item__c preBillLi: preBill.Prebill_Line_Items__r){
                        if(preBillLi.Order_Line_Item__c != null && preBillLi.Order_Line_Item__r.csord__Order__c != null && preBillLi.Order_Line_Item__r.csord__Order__r.Order_Request__r.Frame_Contract__r.type__c == 'Sixt'){
                            orderIds.add(preBillLi.Order_Line_Item__r.csord__Order__c);
                        }   
                        if(preBillLi.Service_Line_Item__c != null && preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c!= null){
                            ordReqIds.add(preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c);
                        }  
                    }
                }
            }

            map<id, Order_Request__c> m2mOrdReqMap = new map<id, Order_Request__c>();
            if(ordReqIds.size() > 0){
                m2mOrdReqMap = new map<id, Order_Request__c>([select id, Frame_Contract__r.type__c,Ticket__r.Sixt_LVID__c,Ticket__r.Sixt_SIM_activation_date__c from Order_Request__c where id In :ordReqIds and Frame_Contract__r.type__c = 'Sixt']);
            }
            
            
            
            map<id,csord__Service__c> serviceRecs = new map<id,csord__Service__c>(); // Map of M2M order request as key and service record as value
            
            list<csord__Order__c> orderRec = new list<csord__Order__c>();
            if(orderIds.size() > 0){
                list<csord__Service__c> servicesTmp = new list<csord__Service__c>();
                servicesTmp = [select id, csord__Deactivation_Date__c, csord__Subscription__r.csord__Order__c from csord__Service__c where csord__Subscription__r.csord__Order__c in :orderIds];
                for(csord__Service__c ser : servicesTmp){
                    if(ser.csord__Subscription__r.csord__Order__c != null){
                        serviceRecs.put(ser.csord__Subscription__r.csord__Order__c,ser);
                    }
                }
            }

            for(Prebill__c preBill: preBills){
                if(preBill.Prebill_Line_Items__r != null && preBill.Prebill_Line_Items__r.size() > 0){
                    for(Prebill_Line_Item__c preBillLi: preBill.Prebill_Line_Items__r){
                        /* For valid Order Line Item*/
                        if(preBillLi.Order_Line_Item__c != null && preBillLi.Order_Line_Item__r.csord__Order__r.Order_Request__r.Frame_Contract__r.type__c =='Sixt'){
                            rows = rows + '\n' + ','+
                                   prepareDateFormat(dailyBill.Official_Invoice_Date__c)+
                                   ',,'+
                                   checkNullString(string.valueOf(preBillLi.Order_Line_Item__r.csord__Order__r.Order_Request__r.Ticket__r.Sixt_LVID__c))+
                                   ',,'+checkNullString(preBillLi.Line_Item_Description__c)+
                                   ','+
                                   prepareDateTimeFormat(preBillLi.Order_Line_Item__r.csord__Order__r.Order_Request__r.Ticket__r.Sixt_SIM_activation_date__c)+
                                   ','+orderLineItemServiceDeactivationDate(serviceRecs,preBillLi)+
                                   ','+
                                   '1,'+
                                   checkNullString(string.valueOf(preBillLi.Net_Amount__c))+
                                   ',19,DE'; 
                        }
                        /* For valid Service Line Item*/
                        if(preBillLi.Service_Line_Item__c != null && preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c != null && 
							m2mOrdReqMap.get(preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c) != null){
                            rows = rows + '\n' + ','+
                                   prepareDateFormat(dailyBill.Official_Invoice_Date__c)+
                                   ',,'+caseLVID(m2mOrdReqMap,preBillLi)+
                                   ',,'+
                                   checkNullString(preBillLi.Line_Item_Description__c)+
                                   ','+simDeactivationDate(m2mOrdReqMap,preBillLi)+
                                   ','+
                                   prepareDateFormat(preBillLi.Service_Line_Item__r.csord__Service__r.csord__Deactivation_Date__c)+
                                   ','+
                                   '1,'+
                                   checkNullString(string.valueOf(preBillLi.Net_Amount__c))+
                                   ',19,DE'; 
                        }  
                    }
                }
            }
            
            
            csvData = columnHeaders + rows;
            
        }
    }
    
    
    public string checkNullString(string tmp){
        string s = '';
        if(tmp != null){
            return tmp;
        }else{
            return s;
        }
    }
    
    public string prepareDateFormat(date dt){
        string dateFormat = '';
        if(dt != null){
            integer day = dt.day();
            integer month = dt.Month();
            integer year = dt.year();
            dateFormat = string.valueOf(day)+'.'+string.valueOf(month)+'.'+string.valueOf(year);
        }
        return dateFormat;
    }
    
    public string prepareDateTimeFormat(dateTime dt){
        string dateFormat = '';
        if(dt != null){
            integer day = dt.day();
            integer month = dt.Month();
            integer year = dt.year();
            integer hour = dt.hour();
            integer minutes = dt.minute();
            
            dateFormat = string.valueOf(day)+'.'+string.valueOf(month)+'.'+string.valueOf(year)+' '+string.valueOf(hour)+':'+string.valueOf(minutes);
        }
        return dateFormat;
    }
    
    
    public string simDeactivationDate(map<id, Order_Request__c> m2mOrdReqMap,Prebill_Line_Item__c preBillLi){
        string tmp;
        if(m2mOrdReqMap.get(preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c) != null && 
			m2mOrdReqMap.get(preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c).Ticket__r.Sixt_SIM_activation_date__c != null){
            tmp = prepareDateTimeFormat(m2mOrdReqMap.get(preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c).Ticket__r.Sixt_SIM_activation_date__c);
        }
        return tmp;
    }
    
    public string caseLVID(map<id, Order_Request__c> m2mOrdReqMap,Prebill_Line_Item__c preBillLi){
        string tmp;
        if(m2mOrdReqMap.get(preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c) != null && 
			m2mOrdReqMap.get(preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c).Ticket__r.Sixt_LVID__c != null){
            tmp = checkNullString(string.valueOf(m2mOrdReqMap.get(preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c).Ticket__r.Sixt_LVID__c));
        }
        return tmp;
    }
    
    
    public string orderLineItemServiceDeactivationDate(map<id,csord__Service__c> serviceRecs,Prebill_Line_Item__c preBillLi){
        string tmp;
        if(serviceRecs.get(preBillLi.Order_Line_Item__r.csord__Order__c) != null && serviceRecs.get(preBillLi.Order_Line_Item__r.csord__Order__c).csord__Deactivation_Date__c != null){
            tmp = prepareDateFormat(serviceRecs.get(preBillLi.Order_Line_Item__r.csord__Order__c).csord__Deactivation_Date__c);
        }
        return tmp;
    }

}