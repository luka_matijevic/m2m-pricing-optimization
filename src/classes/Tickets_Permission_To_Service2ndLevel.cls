/* This class is to be used as post deployment script for service cloud requirements.
 * This class will provide read permission to tickets for M2M standard user profle users,
 * if user having an opportunity for an account, then user can view all cases attahced to that account
 * Auhtor : pavan kumar
 *
 * script to intiate batch execution : >>> Database.executeBatch(new Tickets_Permission_To_Service2ndLevel());
 */
 
 
 
public class Tickets_Permission_To_Service2ndLevel implements Database.Batchable<sObject>{ 
    public list<Profile> profiles;
    
    public Tickets_Permission_To_Service2ndLevel(){
        profiles = new list<profile>([select id, name from profile where name =:label.M2M_Standard_User]);
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([select id, accountId from Case where accountId != null]);
    }
    
    
    public void execute(Database.BatchableContext BC, List<case> scope){
        list<caseShare> caseShareToinsert = new list<caseShare>();
        if(!scope.isEmpty()){
            map<id, id> accountCaseMap = new map<id, id>();
            
            for (Case c : scope){
                if(c.accountId != null){
                    accountCaseMap.put(c.id,c.accountID);
                }
            }
            if(accountCaseMap.size() > 0){
                map<id, list<Opportunity>> accountOppMap = new map<id, list<Opportunity>>();
                if(true){
                    list<Opportunity> oppLst = new list<opportunity>([select id, accountid,OwnerId, Owner.profileId from Opportunity where accountID In :accountCaseMap.values() and Owner.profileId = '00eb0000000DviUAAS' and owner.IsActive = true]);
                    if(oppLst.size() > 0){
                        for(Opportunity Opp : oppLst){
                            if(opp.accountID != null){
                                list<Opportunity> tmp = new list<Opportunity>();
                                tmp.add(opp);
                                if(accountOppMap.get(opp.accountId) != null){
                                    tmp.addAll(accountOppMap.get(opp.accountId));
                                }
                                accountOppMap.put(opp.accountId, tmp);
                            }
                        }
                    }
                }
                for(Id i : accountCaseMap.keySet()){
                    if(accountCaseMap.get(i) != null){
                        if (accountOppMap.containsKey(accountCaseMap.get(i)))
                        for(Opportunity opp : accountOppMap.get(accountCaseMap.get(i))){
                            CaseShare csShare = new CaseShare(caseId=i,
                                                          CaseAccessLevel = 'Read',
                                                          UserOrGroupId = opp.ownerID);
                            caseShareToinsert.add(csShare);   
                        }
                    }
                }
            }
        }
        if(!caseShareToinsert.isEmpty()){
              insert caseShareToinsert;
        }
    }
    
    public void finish(Database.BatchableContext BC){

    }

}