/**
 * Description: Controller class for AgentView Page. Holds logic to populate all views and chart data.
 * 1.0 | Ritesh Saxena | 2014 | initial Version
 * 2.0 | Ritesh Saxena | 25-Nov-2014 | Changes, Text 'Priority' changed to 'Prio' on charts and description text.
 *								In AgentView page 'Refresh All Open Case' button added on top left.
 *								'All Open Case' view updated with "Unassigned fileter - to fetch all 'In Progress' tickets.
 *								Dropdown 'All' filter updated to fetch all 'Open' + 'In Progress' tickets.
 */
public with Sharing class AgentViewController {

	public List<Case> openCaseList{get; set;}
	public List<Case> myOpenCaseList{get; set;}
	public List<PieWedgeData> pieData{get; set;}
	public List<CaseComment> commentList{get;set;}
	public List<EmailMessage> caseEmailList{get;set;}
	public Id caseId{get; set;}
	public String selectedPriority{get;set;}
	public String chartColor{get;set;}
	public String allOpenCaseLabel{get;set;}
	public String caseNum{get;set;}
	public List<SelectOption> selectList{get;set;}

	/**
	 * Constructor call
	 */
	public AgentViewController(ApexPages.StandardController cntrl) {

		initiatePage();

	}

	/**
	 * Method used for fetching the Case data for different views on AgentView Page
	 */
	public void initiatePage() {
		try {
			Decimal highCount=0.0;
			Decimal mediumCount=0.0;
			Decimal lowCount=0.0;
			Decimal totalCount=0.0;
			caseNum='';
			allOpenCaseLabel='All Open Tickets';
			commentList= new List<CaseComment>();
			caseEmailList= new List<EmailMessage>();
			// setting selection to reset the values on click of refresh.
			selectedPriority = '--All--';

			openCaseList = new List<Case>();
			myOpenCaselist = new List<Case>();
			pieData = new List<PieWedgeData>();

			myOpenCaselist= [Select Id,Subject, CaseNumber,Has_new_Email__c, Status, OwnerId, Account.Name, SLA_Breach_Status__c, SLA_Time__c, Type, Origin, Priority__c, CreatedDate from Case
			where OwnerId=:UserInfo.getUserId() AND Status!='Closed' AND Status!='Rejected' ORDER BY CreatedDate DESC NULLS LAST LIMIT:1000];

			for (Case c : [Select Id, CaseNumber,Has_new_Email__c, Status, OwnerId, Account.Name, Subject, SLA_Breach_Status__c, SLA_Time__c, Type, Origin, Priority__c, CreatedDate from Case where Status='Open' OR Status='In Progress' ORDER BY CreatedDate DESC NULLS LAST LIMIT: 1000]) {
				openCaseList.add(c);
				/*if(String.valueOf(UserInfo.getUserId()).equals(String.valueOf(c.OwnerId))){
					myOpenCaselist.add(c);
				}*/
				if (null!=c.Priority__c && c.Priority__c.equalsIgnoreCase('High')) {
					highCount++;
				}
				if (null!=c.Priority__c && c.Priority__c.equalsIgnoreCase('Medium')) {
					mediumCount++;
				}
				if (null!=c.Priority__c && c.Priority__c.equalsIgnoreCase('Low')) {
					lowCount++;
				}
			}
			totalCount = openCaseList.size();

			// Wrapper construction for Chart
			if (totalCount>0.0) {
				pieData.add(new PieWedgeData('Prio 1', ((highCount/totalCount)*100).setScale(2)));
				pieData.add(new PieWedgeData('Prio 2', ((mediumCount/totalCount)*100).setScale(2)));
				pieData.add(new PieWedgeData('Prio 3', ((lowCount/totalCount)*100).setScale(2)));
				chartColor='#D00000,#FFFF30,#32CD32';
			}

			selectList = new List<SelectOption>();
			selectList.add(new SelectOption('', '--All--'));
			selectList.add(new SelectOption('Priority 1', 'Priority 1'));
			selectList.add(new SelectOption('Priority 2', 'Priority 2'));
			selectList.add(new SelectOption('Priority 3', 'Priority 3'));
			selectList.add(new SelectOption('Unassigned', 'Unassigned'));
			selectList.sort();

		} catch(Exception e) {
			system.debug('Exception in AgentViewController:' + e.getMessage()); ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Operation Failed: Please contact your system administrator!', ''));
		}
	}

	/**
	 * Wrapper class for Pie Chart.
	 */
	public class PieWedgeData {

		public String name { get; set; }
		public Decimal data { get; set; }

		public PieWedgeData(String name, Decimal data) {
			this.name = name;
			this.data = data;
		}
	}

	/**
	 * Method to display Case details i.e. case comments and email history in Case email history/comments section
	 */
	public void viewCaseDetails() {
		try {
			commentList=new List<CaseComment>();
			caseEmailList=new List<EmailMessage>();
			if(null!=caseId){

				commentList = [SELECT Id, CommentBody, CreatedDate, IsDeleted, IsPublished, ParentId, LastModifiedDate FROM CaseComment
				where IsDeleted=false AND ParentId=:caseId Order By CreatedDate DESC NULLS LAST LIMIT:(Limits.getLimitQueryRows()-Limits.getQueryRows())];

				caseEmailList = [SELECT Id,CreatedDate,FromAddress,FromName,Incoming,IsDeleted,MessageDate,ParentId,ReplyToEmailMessageId,
				Status,Subject,TextBody,ToAddress FROM EmailMessage WHERE IsDeleted=false AND ParentId=:caseId
				Order By CreatedDate DESC NULLS LAST LIMIT:(Limits.getLimitQueryRows()-Limits.getQueryRows())];

				caseNum = [Select Id, CaseNumber from Case where Id=:caseId][0].CaseNumber;
			}
		} catch(Exception e) {
			system.debug('Exception in AgentViewController:' + e.getMessage()); ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Operation Failed: Please contact your system administrator!', ''));
		}

	}

	/**
	 * Method to update All Open Case details based on Case priority
	 */
	public void updateAllCaseView() {
		// storing selection to reset the selectList value on click of refresh to "--All--"
		String selectedOption=selectedPriority;
		initiatePage();
		List<Case> tempOpenCaseList = new List<Case>();
		for (Case c : openCaseList) {
			if ((selectedOption == 'Priority 1' || selectedOption == 'Prio 1') && c.Priority__c == 'High') {
				tempOpenCaseList.add(c);
			} else if ((selectedOption == 'Priority 2' || selectedOption == 'Prio 2') && c.Priority__c == 'Medium') {
				tempOpenCaseList.add(c);
			} else if ((selectedOption == 'Priority 3' || selectedOption == 'Prio 3') && c.Priority__c == 'Low') {
				tempOpenCaseList.add(c);
			} else if (selectedOption == 'Unassigned' && c.Status == 'In Progress') {
				tempOpenCaseList.add(c);
			} else if (selectedOption == '' || selectedOption == null || selectedOption == '--All--') {
				tempOpenCaseList.add(c);
			}
		}
		allOpenCaseLabel= allOpenCaseLabel+ ((selectedOption!=null && selectedOption!='')?': ' +
		(selectedOption.contains('Prio ')? selectedOption.replace('Prio ', 'Priority ') : selectedOption) :'');
		selectedPriority = selectedOption;

		openCaseList.clear();
		openCaseList.addAll(tempOpenCaseList);
		tempOpenCaseList.clear();
	}

}