@isTest
private class BatchUpdateSimActivationDateTest {
	
	@isTest static void testUpdateActivationDate() {
		No_Trigger__c notriggers = new No_Trigger__c();
  		notriggers.Flag__c = true;
  		insert notriggers;

  		Stock__c sim = new Stock__c(
  			SM2M_Status__c = Constants.ACTIVE,
  			State_Change_Date__c = Datetime.newInstance(2013, 5, 13, 0, 0, 0)
  		);

  		insert sim;

  		Test.startTest();
  		Database.executeBatch(new BatchUpdateSimActivationDate());
  		Test.stopTest();

  		Stock__c updatedSim = [Select Activation_Date__c from Stock__c where Id = :sim.Id];
  		System.assertEquals(sim.State_Change_Date__c, updatedSim.Activation_Date__c);
  	}

	@isTest static void testUpdateDeactivationDate() {
		No_Trigger__c notriggers = new No_Trigger__c();
  		notriggers.Flag__c = true;
  		insert notriggers;

  		Stock__c sim = new Stock__c(
  			SM2M_Status__c = Constants.DEACTIVATED,
  			State_Change_Date__c = Datetime.newInstance(2013, 5, 13, 0, 0, 0)
  		);

  		insert sim;

  		Test.startTest();
  		Database.executeBatch(new BatchUpdateSimActivationDate());
  		Test.stopTest();

  		Stock__c updatedSim = [Select Deactivation_Date__c from Stock__c where Id = :sim.Id];
  		System.assertEquals(sim.State_Change_Date__c, updatedSim.Deactivation_Date__c);
  	}
}