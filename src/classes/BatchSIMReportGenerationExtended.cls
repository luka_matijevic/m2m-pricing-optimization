/*
* SIM report generation batch class. Creates Non Trial & Commercial SIM reports as well.
*/
public class BatchSIMReportGenerationExtended implements Database.Batchable<sObject>, Database.Stateful {
	
	private SIMReportBuilder simReportBuilder;
	private List<SIM_Report__c> simsList;
	
	/*
	* Constructor call.
	*/
	public BatchSIMReportGenerationExtended(SIMReportBuilder simReportBuilder) {
		this.simReportBuilder = simReportBuilder;
	}
		
	/*
	* Query execution.
	*/
	public List<SIM_Report__c> start(Database.BatchableContext BC) {

		if(this.simReportBuilder != null)
			simsList = this.simReportBuilder.buildReports();

		return simsList;  
	}
	
	/*
	* Execute call.
	*/
	public void execute(Database.BatchableContext BC, List<SObject> scope) {
		List<SIM_Report__c> sims = (List<SIM_Report__c>) scope;

		for (SIM_REport__c ar : sims) {
			ar.customerRelated__c = true;
			ar.Account__c = ar.TEF_DE_Sales_Channel__c;
		}

		if (sims != null && !sims.isEmpty()) {
			insert sims;
		}
	}
	
	/*
	* Finish call.
	*/
	public void finish(Database.BatchableContext BC) {
		
	}
}