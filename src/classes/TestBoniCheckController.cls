@isTest
private class TestBoniCheckController {
	
	static testMethod void testBoniCheckControllerFunctionality(){
		
		// Creating test data, Not sure what fields needed on Boni_Check__c
		TestHelper.createAccount();
		Account acc = [Select Id,Name FROM Account];
		PageReference newPage = Page.BoniCheck;
		Boni_Check__c bc = new Boni_Check__c(
			Name = 'TestBC',
			Account__c = acc.Id,
			Status_of_CC__c = 'Submited',
			International_Roaming_Voice__c = false,
			Blocking_Comment__c  = null,
			Premium_Services_0900__c = false
			
		);
		insert bc;
		// Try catching exception
		ApexPages.StandardController stdctrl = new ApexPages.StandardController(acc);
		try{
			BoniCheckController bcc = new BoniCheckController();
		
		} catch(BoniCheckController.BoniCheckControllerException e){
		}
		
		// Try other functionality - Not sure how to assert everything is ok 
		System.currentPageReference().getParameters().put('accountId', acc.id);
		System.currentPageReference().getParameters().put('Id', bc.id);
		BoniCheckController bcc = new BoniCheckController();
		bcc.closePopup();
		System.assertEquals(bcc.displayPopup,false);
		bcc.showPopup();
		System.assertEquals(bcc.displayPopup,true);
		bcc = new BoniCheckController(stdctrl);
		Boolean deb = bcc.getCanRequestNewBoniCheck();
		System.assertEquals(deb,false);
		deb = bcc.getIsBoniUser();
		System.assertEquals(deb,true);
		deb = bcc.getCanApprove();
		System.assertEquals(deb,true);
		deb = bcc.getCanSaveRecord();
		System.assertEquals(deb,true);
		deb = bcc.getCanDecline();
		System.assertEquals(deb,true);
		deb = bcc.getCanDeclineBankGuarantee();
		System.assertEquals(deb,false);
		deb = bcc.getCanRequestBankGuarantee();
		System.assertEquals(deb,false);
		deb = bcc.getCanAddBankGuarantee();
		System.assertEquals(deb,false);
		deb = bcc.getCanDeclineRiskPaper();
		System.assertEquals(deb,false);
		deb = bcc.getCanAddRiskPaper();
		System.assertEquals(deb,false);
		deb = bcc.getCanRequestRiskPaper();
		System.assertEquals(deb,false);
		newPage = bcc.requestNewBoniCheck();
		bcc.addBankGuarantee();
		System.assertEquals(bcc.displayPopup,true);
		newPage = bcc.uploadFile();
		System.assertEquals(newPage,null);
		bcc.addAttachment();
		System.assertEquals(bcc.addAttachment,true);
		bcc.attachment.Body = Blob.valueOf('Unit Test Document Body');
		bcc.attachment.Name = 'Test';
		bcc.addAttachment = true;
		bcc.uploadFile();
		newPage = bcc.testAction();
		bcc.requestRiskPaper();
		bcc.approve();
		bcc.decline();
		bcc.requestBankGuarantee();
		bcc.declineBankGuarantee();
		bcc.declineRiskPaper();
		SelectOption[] so = bcc.getDebugModes();
		bcc.NewBoniCheck = bc;
		newPage = bcc.requestNewBoniCheck();
		
		
		
		
	}
	
}