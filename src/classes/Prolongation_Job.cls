/*
@@Description : Batch job on Stock Records
                If Stock__c.End_Date__C = Today+3 Months, then updating below fields
                Field update 1 --> Stock__C.VVL_date__c = Today
                Field update 2 --> Stock__C.End_Date__c = Old Stock__c.End_Date__c + 12 Month

*/

global class Prolongation_Job implements Database.Batchable<sObject>{

    static final Date tdy  = system.today();
    global Prolongation_Job(){
        
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        string supspended = 'SUSPENDED';
        Date third_Month_from_Now  = tdy.addMonths(3);
        string RETIRED = 'RETIRED';
        try{
            string qry = 'select id,VVL_date__c, Order_Request__c,Start_Date__c,Potential_Termination_Date__c,SM2M_Status__c from Stock__c where SM2M_Status__c != :supspended AND SM2M_Status__c !=:RETIRED AND Potential_Termination_Date__c = :third_Month_from_Now AND Termination_Status__c =null '; // Querying Stock record, Whose SM2M Staus != SUSPENDED and End_Date == Today date + Three Months
            system.debug('qry *** '+qry);
            return Database.getQueryLocator(qry);
        }catch(exception ex){
            system.debug('No Record Available for the date' +third_Month_from_Now );
            return null;
        }
    }
    
    global void execute(Database.BatchableContext BC, List<Stock__c> scope){
        if(scope != null && scope.size() > 0 ){
            /*for(Stock__c sim : scope){
                sim.VVL_date__c = tdy;
                sim.Start_Date__c = sim.End_date__c;
                date prolongedDate = sim.End_date__c.addMonths(12);
                sim.End_date__c = prolongedDate;
                sim.IsProlonged__c = true;
            }*/
            //Petar Matkovic New Implementation 16.6.2016.
            for(Stock__c sim : scope){
                sim.VVL_date__c = tdy;

                //PM 23.3.2017.
                sim.Contract_Cycle_Type__c = 'Implicit';
                //End Of PM Change 23.3.2017.

                sim.Start_Date__c = sim.Potential_Termination_Date__c;
                date prolongedDate = sim.Potential_Termination_Date__c.addMonths(12);
                sim.Potential_Termination_Date__c = prolongedDate;
                sim.IsProlonged__c = true;
            }
            update scope;
        }
    }
    
    global void finish(Database.BatchableContext BC){

    }
}