@isTest
public class UserTriggerDelegate_Test {
    
    static testMethod void groupAssignTest(){
        list<Profile> profiles = new list<profile>([select id, name from profile where name = 'M2M Service 2nd Level' Or name = 'Standard User' order by name]);
        list<user> usrLst = new list<user>();
        
        if(!profiles.isEmpty() && profiles.size() == 2){
            user u1 = new User(Alias = 'User', Email='m2muser@m2m.com',
                      EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',
                      LocaleSidKey='en_US', ProfileId = profiles[0].Id,
                      TimeZoneSidKey='America/Los_Angeles', UserName='m2muser1@m2m.com');
            usrLst.add(u1);
            user u2 = new User(Alias = 'User', Email='m2muser@m2m.com',
                      EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',
                      LocaleSidKey='en_US', ProfileId = profiles[1].Id,
                      TimeZoneSidKey='America/Los_Angeles', UserName='m2muser2@m2m.com');
            usrLst.add(u2);
            
            test.startTest();
            insert usrLst;
            
            if(usrLst.size() > 1){
                usrLst[1].profileId = profiles[0].id;
                update usrLst[1];
            }
            
            system.assertEquals(usrLst[0].profileId, profiles[0].id);
            system.assertEquals(usrLst[1].profileId, profiles[0].id);
            test.stopTest();
            
            
        }
    }
    
}