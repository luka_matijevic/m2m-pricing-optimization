global with sharing class FinalSixtBillingReportGenerator implements Database.Batchable<sObject>, Database.stateful {
	
    private static final String FINAL_PREBILL_FILE_LINE_FORMAT = '{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}\n';
    private static final String DOT_DATE_FORMAT = 'dd.MM.yyyy';  
    private Id dailyBillId;
    private String csvSixtReport = '';
	private List<Prebill__c> preBillsFetch = new List<Prebill__c>();
	private Date offInvoiceDate, dailyBillDate;

	public FinalSixtBillingReportGenerator(Id dailyBillId){
        this.dailyBillId = dailyBillId;
    }

	global List<sObject> start(Database.BatchableContext BC) {
		Daily_Bill__c dailyBill = [select Id, Official_Invoice_Date__c, Date__c from Daily_Bill__c where Id = :dailyBillId];
		if (dailyBill != null)
		{
			offInvoiceDate = dailyBill.Official_Invoice_Date__c;
			dailyBillDate = dailyBill.Date__c;
			preBillsFetch = [select id,(select Id, Line_Item_Description__c, Net_Amount__c,
										Order_Line_Item__r.csord__Order__r.Order_Request__r.Frame_Contract__r.type__c,
										Order_Line_Item__r.csord__Order__r.Order_Request__r.Ticket__r.Sixt_LVID__c,
										Order_Line_Item__r.csord__Order__r.Order_Request__r.Ticket__r.Sixt_SIM_activation_date__c,
										Order_Line_Item__c, Service_Line_Item__c, Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c,
										Service_Line_Item__r.csord__Service__r.csord__Deactivation_Date__c,
										Article_Name__c
								from Prebill_Line_Items__r 
								where (Order_Line_Item__c != null AND Order_Line_Item__r.csord__Order__r.Order_Request__r.Frame_Contract__r.type__c ='Sixt') OR Service_Line_Item__c != null) 
					from Prebill__c 
					where Daily_Bill__c = :dailyBillId];
		}
		
		return preBillsFetch;
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope)  {
		List<Prebill__c> prebills = (List<Prebill__c>) scope;
		csvSixtReport = prepareCSVData(prebills);
	}

	global void finish(Database.BatchableContext BC) {
		List<Daily_Bill__c> dailyBill = [Select Id, Name, Date__c from Daily_Bill__c where Id = :dailyBillId];

		this.csvSixtReport = prependSixtHeader(this.csvSixtReport);

		List<Attachment> att = [select id, name from Attachment where parentid= :dailyBillId and name like '%Sixt%' ];

        Integer attCount = att.size();

		if (dailyBill != null)
		{
			Attachment sixtAtt = new Attachment();
            sixtAtt.ParentId = dailyBillId;
            sixtAtt.Name = reportName(dailyBillDate, attCount);
            sixtAtt.Body = blob.valueOf(AttachmentUtils.csvBOM() + this.csvSixtReport);
            sixtAtt.ContentType = 'text/csv';
            insert sixtAtt;
		}
	}

	private String prepareCSVData(List<Prebill__c> prebillsIn) {
		String rows = '';        

		Set<Id> orderIds = new Set<Id>();
        Set<Id> ordReqIds = new Set<Id>();
            
        for(Prebill__c preBill : prebillsIn){
            if(preBill.Prebill_Line_Items__r != null && preBill.Prebill_Line_Items__r.size() > 0){
                for(Prebill_Line_Item__c preBillLi: preBill.Prebill_Line_Items__r){
                    if(preBillLi.Order_Line_Item__c != null && preBillLi.Order_Line_Item__r.csord__Order__c != null && preBillLi.Order_Line_Item__r.csord__Order__r.Order_Request__r.Frame_Contract__r.type__c == 'Sixt'){
                        orderIds.add(preBillLi.Order_Line_Item__r.csord__Order__c);
                    }

                    if(preBillLi.Service_Line_Item__c != null && preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c!= null){
                        ordReqIds.add(preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c);
                    }  
                }
            }
        }

        Map<Id, Order_Request__c> m2mOrdReqMap = new Map<Id, Order_Request__c>();

        if(ordReqIds.size() > 0){
            m2mOrdReqMap = new Map<Id, Order_Request__c>([select Id, Frame_Contract__r.type__c,Ticket__r.Sixt_LVID__c,Ticket__r.Sixt_SIM_activation_date__c from Order_Request__c where Id In :ordReqIds and Frame_Contract__r.type__c = 'Sixt']);
        }

		Map<Id, csord__Service__c> serviceRecs = new Map<Id, csord__Service__c>();
            
        List<csord__Order__c> orderRec = new List<csord__Order__c>();
        if(orderIds.size() > 0) {
            List<csord__Service__c> servicesTmp = new List<csord__Service__c>();
            servicesTmp = [select id, csord__Deactivation_Date__c, csord__Subscription__r.csord__Order__c from csord__Service__c where csord__Subscription__r.csord__Order__c in :orderIds];
            for(csord__Service__c ser : servicesTmp){
                if(ser.csord__Subscription__r.csord__Order__c != null){
                    serviceRecs.put(ser.csord__Subscription__r.csord__Order__c, ser);
                }
            }
        }

		for(Prebill__c preBill : prebillsIn) {
            if(preBill.Prebill_Line_Items__r != null && preBill.Prebill_Line_Items__r.size() > 0){
                for(Prebill_Line_Item__c preBillLi : preBill.Prebill_Line_Items__r){
                    
                    if(preBillLi.Order_Line_Item__c != null && preBillLi.Order_Line_Item__r.csord__Order__r.Order_Request__r.Frame_Contract__r.type__c == 'Sixt') {
                        rows = rows + '\n' + ',' + prepareDateFormat(offInvoiceDate) +
                            ',,' + checkNullString(string.valueOf(preBillLi.Order_Line_Item__r.csord__Order__r.Order_Request__r.Ticket__r.Sixt_LVID__c)) +
                            ',,' + checkNullString(preBillLi.Article_Name__c) +
                            ',' + prepareDateTimeFormat(preBillLi.Order_Line_Item__r.csord__Order__r.Order_Request__r.Ticket__r.Sixt_SIM_activation_date__c) +
                            ',' + orderLineItemServiceDeactivationDate(serviceRecs, preBillLi) +
                            ',' + '1,' + checkNullString(string.valueOf(preBillLi.Net_Amount__c)) + ',19,DE';
                    }
                    
                    if(preBillLi.Service_Line_Item__c != null && preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c != null && 
						m2mOrdReqMap.get(preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c) != null) {
                        rows = rows + '\n' + ',' + prepareDateFormat(offInvoiceDate) +
                                ',,' + caseLVID(m2mOrdReqMap, preBillLi) +
                                ',,' + checkNullString(preBillLi.Article_Name__c) +
                                ',' + simDeactivationDate(m2mOrdReqMap, preBillLi) +
                                ',' + prepareDateFormat(preBillLi.Service_Line_Item__r.csord__Service__r.csord__Deactivation_Date__c) +
                                ',' + '1,' + checkNullString(string.valueOf(preBillLi.Net_Amount__c)) + ',19,DE'; 
                    }  
                }
            }
        }

		return rows;
	}

	private String reportName(Date billingDate, Integer lastCount) {        
        String name = 'Sixt_Billing';
        name += '_';
        name += Datetime.newInstance(billingdate, Time.newInstance(0, 0, 0, 0)).format('ddMMYYYY');
        name += '_' +  string.valueOf(lastCount+1);
        name += '.csv';
        return name;
    }

	private String sixtHeaderRow() {
        return String.format(FINAL_PREBILL_FILE_LINE_FORMAT,
            new List<String>{
                'Lieferantenbelegnummer', 
				'Rechnungsdatum (dd.MM.YYYY)', 
				'Barcode', 
				'Sixt Vertragsnummer', 
				'Amtl. Kennzeichen', 
				'Artikelnr', 
				'Leistungszeitraum von', 
				'Leistungszeitraum bis', 
				'Menge', 
				'Nettobetrag', 
				'Umsatzsteuer in %', 
				'Hiso'
            });
    }

	private String prependSixtHeader(String text) {
        return (text == null) ? sixtHeaderRow() : sixtHeaderRow() + text;
    }

	public string checkNullString(string tmp){
        string s = '';
        if(tmp != null){
            return tmp;
        }else{
            return s;
        }
    }
    
    public string prepareDateFormat(Date dt){
        string dateFormat = '';
        if(dt != null){
            integer day = dt.day();
            integer month = dt.Month();
            integer year = dt.year();
            dateFormat = string.valueOf(day)+'.'+string.valueOf(month)+'.'+string.valueOf(year);
        }
        return dateFormat;
    }
    
    public string prepareDateTimeFormat(DateTime dt){
        string dateFormat = '';
        if(dt != null){
            integer day = dt.day();
            integer month = dt.Month();
            integer year = dt.year();
            integer hour = dt.hour();
            integer minutes = dt.minute();
            
            dateFormat = string.valueOf(day)+'.'+string.valueOf(month)+'.'+string.valueOf(year)+' '+string.valueOf(hour)+':'+string.valueOf(minutes);
        }
        return dateFormat;
    }
    
    
    public string simDeactivationDate(Map<Id, Order_Request__c> m2mOrdReqMap,Prebill_Line_Item__c preBillLi){
        string tmp;
        if(m2mOrdReqMap.get(preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c) != null && 
			m2mOrdReqMap.get(preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c).Ticket__r.Sixt_SIM_activation_date__c != null) {
            tmp = prepareDateTimeFormat(m2mOrdReqMap.get(preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c).Ticket__r.Sixt_SIM_activation_date__c);
        }
        return tmp;
    }
    
    public string caseLVID(Map<Id, Order_Request__c> m2mOrdReqMap,Prebill_Line_Item__c preBillLi){
        string tmp;
        if(m2mOrdReqMap.get(preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c) != null && 
			m2mOrdReqMap.get(preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c).Ticket__r.Sixt_LVID__c != null) {
            tmp = checkNullString(string.valueOf(m2mOrdReqMap.get(preBillLi.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c).Ticket__r.Sixt_LVID__c));
        }
        return tmp;
    }
    
    
    public string orderLineItemServiceDeactivationDate(Map<Id, csord__Service__c> serviceRecs,Prebill_Line_Item__c preBillLi) {
        string tmp;
        if(serviceRecs.get(preBillLi.Order_Line_Item__r.csord__Order__c) != null && serviceRecs.get(preBillLi.Order_Line_Item__r.csord__Order__c).csord__Deactivation_Date__c != null) {
            tmp = prepareDateFormat(serviceRecs.get(preBillLi.Order_Line_Item__r.csord__Order__c).csord__Deactivation_Date__c);
        }
        return tmp;
    }
}