@isTest
private class SObjectHelperTest {

	static testMethod void testMergeFieldsSkipsIds() {
		
		Account dummyAccount = new Account(Name = 'Test account', Type = 'Customer');
		insert dummyAccount;
		
		Attachment first = new Attachment(Name = 'One', ParentId = dummyAccount.Id, Body = Blob.valueOf('Attachment body'));
		insert first;
		Attachment second = new Attachment(Name = 'Two', ParentId = dummyAccount.Id, Body = Blob.valueOf('Attachment body'));
		insert second;

		Id firstId = first.Id;
		SObjectHelper.mergeFields(first, second);
		
		System.assertEquals(firstId, first.Id);
		System.assertNotEquals(firstId, second.Id);
	}

	static testMethod void testExtractSObjects() {
		List<sObject> objects = new List<sObject>();
		objects.add(new Account(Name = 'Account 1'));
		objects.add(new Account(Name = 'Account 2'));
		objects.add(new Opportunity(Name = 'Opportunity 1'));
		objects.add(new Opportunity(Name = 'Opportunity 2'));
		objects.add(new Opportunity(Name = 'Opportunity 3'));

		List<sObject> extractedAccounts = SObjectHelper.extractSObjects(objects, Account.sObjectType);
		
		// accounts should be extracted out of the original list leaving only opportunities
		System.assertEquals(2, extractedAccounts.size());
		System.assertEquals(3, objects.size());

		for (sObject obj : extractedAccounts) {
			System.assertEquals(Account.sObjectType, obj.getSObjectType());
		}

		for (sObject obj : objects) {
			System.assertEquals(Opportunity.sObjectType, obj.getSObjectType());
		}

		List<sObject> sameObjects = new List<sObject>();
		for (Integer i = 0; i < 5; i++) {
			sameObjects.add(new Contact());
		} 
		List<sObject> allExtracted = SObjectHelper.extractSObjects(sameObjects, Contact.sObjectType);

		System.assertEquals(0, sameObjects.size());
		System.assertEquals(5, allExtracted.size());
	}


	static testMethod void testFilterSObjects() {
		List<sObject> objects = new List<sObject>();

		objects.add(new Account(Name = 'Account 1'));
		objects.add(new Account(Name = 'Account 2'));
		objects.add(new Opportunity(Name = 'Opportunity 1'));
		objects.add(new Opportunity(Name = 'Opportunity 2'));
		objects.add(new Opportunity(Name = 'Opportunity 3'));

		List<sObject> filteredOpps = SObjectHelper.filterSObjects(objects, Opportunity.sObjectType);
		
		// filtered objects should be returned but the original list should not change
		System.assertEquals(3, filteredOpps.size());
		System.assertEquals(5, objects.size());

		for (sObject obj : filteredOpps) {
			System.assertEquals(Opportunity.sObjectType, obj.getSObjectType());
		}
	}

	static testMethod void testFilterSObjectsMutation() {
		List<sObject> objects = new List<sObject>();

		objects.add(new Account(Name = 'Account 1'));
		objects.add(new Account(Name = 'Account 2'));
		objects.add(new Opportunity(Name = 'Opportunity 1'));
		objects.add(new Opportunity(Name = 'Opportunity 2'));
		objects.add(new Opportunity(Name = 'Opportunity 3'));

		List<Opportunity> opps = (List<Opportunity>) SObjectHelper.filterSObjects(objects, Opportunity.sObjectType);

		opps.get(0).Name = 'Mutated opportunity';
		System.assertEquals(((Opportunity)objects.get(2)).Name, opps.get(0).Name);
	}
}