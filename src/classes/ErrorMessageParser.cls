public abstract class ErrorMessageParser {

	protected ErrorResponse response;
	private final String UNRECOVERABLE_EXCEPTION_NAME = 'com.cloudsense.messaging.UnrecoverableException: ';
	private final String SOCKET_TIMEOUT_NAME = 'java.net.SocketTimeoutException: ';

	public ErrorMessageParser(csam_t1__Incoming_Message__c incomingMessage) {
		Attachment attach = [Select Body from Attachment where ParentId = :incomingMessage.Id limit 1];
		this.response = (ErrorResponse) JSON.deserialize(attach.Body.toString(), ErrorResponse.class);
	}

	public ErrorResponse getErrorResponse() {
		return new ErrorResponse(response);
	}

	public String getUnrecoverableException() {
		return exceptionMessage(UNRECOVERABLE_EXCEPTION_NAME);
	}
	
	public Boolean hasUnrecoverableException() {
		return getUnrecoverableException() != null;
	}

	
	public String getSocketTimeoutException() {
		return exceptionMessage(SOCKET_TIMEOUT_NAME);
	}
	
	public Boolean hasSocketTimeoutException() {
		return getSocketTimeoutException() != null;
	}

	private String exceptionMessage(String exceptionName) {
		if (this.getStackTrace().indexOf(exceptionName) == -1) {
			return null;
		}
		Integer startIndex = getStackTrace().indexOf(exceptionName) + exceptionName.length();
		Integer endIndex = getStackTrace().indexOf('\n', startIndex);
		return endIndex == -1 ? getStackTrace().substring(startIndex) : getStackTrace().substring(startIndex, endIndex);
	}

	public String getStackTrace() {
		return response.stackTrace;
	}

	public String getErrorMessage() {
		return response.errorMessage;
	}

	public String getCurrentPayload() {
		return response.currentPayload;
	}
}