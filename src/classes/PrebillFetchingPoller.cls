global class PrebillFetchingPoller implements Schedulable {
	
	public static void startPolling() {
		PrebillFetchingPoller.scheduleTick(System.now().addSeconds(10));
	}
	
	global void execute(SchedulableContext sc) {
		
		List<Daily_Bill__c> completedDailyBills = [
			Select
				Id, Name, Date__c
			from
				Daily_Bill__c
			where 
				All_Prebills_Requested__c = true 
			and
				Prebill_Retrieval_Complete__c = false
			and
				Prebill_Responses_Missing__c = 0		
		];
		
		for (Daily_Bill__c dailyBill : completedDailyBills) {
			dailyBill.Prebill_Retrieval_Complete__c = true;
			update dailyBill;
			Database.executeBatch(new BatchPrebillLineItemGenerator(dailyBill.Date__c), 5);
		}
		
		if (!BillingSettingsHelper.isDisablePrebillPollerReschedule()) {
			Datetime nextTick = System.now().addSeconds(BillingSettingsHelper.getPrebillPollerDelaySeconds());
			PrebillFetchingPoller.scheduleTick(nextTick);
		}
		System.abortJob(sc.getTriggerId());
	}
	
	private static void scheduleTick(Datetime dt) {
		String chron_exp = dt.format('ss mm HH dd MM ? yyyy');
		PrebillFetchingPoller poller = new PrebillFetchingPoller();
		// Schedule the next job, and give it the system time so name is unique
		System.schedule(jobName(dt), chron_exp, poller);
	}
	
	private static String jobName(Datetime dt) {
		return 'PrebillFetchingPoller_' + dt.getTime();
	}
}