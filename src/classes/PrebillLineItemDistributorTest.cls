@isTest
public class PrebillLineItemDistributorTest {
	
	static testMethod void testAdding() {
	
		Date billingDate = Date.newInstance(2014,2,1);
	
		Account acc = new Account(
			Name = 'Test account',
			BillingCity = 'Bremen',
			BillingCountry = 'Germany',
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße',
			Type = 'Customer',
			Billing_Cycle__c = String.valueOf(billingDate),
			VAT__c = 1,
            Global__c = true
		);
	
		insert acc;
		
		Billing_Account__c billingAccount = new Billing_Account__c(
			Billing_Country__c = 'Deutschland',
			Payer__c = acc.Id,
			VAT__c = '1'
		);
		
		insert billingAccount;
		
		Daily_Bill__c dailyBill = new Daily_Bill__c(
			Date__c = billingDate
		);
		
		insert dailyBill;
		
		Prebill__c prebill = new Prebill__c(
			Billing_Account__c = billingAccount.Id,
			Billing_Rate_Model__c = BillingRateModels.toString(BillingRateModel.ONEOFF)
		);
		
		PrebillLineItemDistributor pd = new PrebillLineItemDistributor(billingDate);
		
		System.assertEquals(true, pd.addPrebill(prebill));
		System.assertEquals(false, pd.addPrebill(prebill));
		
		Prebill__c installmentPrebill = new Prebill__c(
			Billing_Account__c = billingAccount.Id,
			Billing_Rate_Model__c = BillingRateModels.toString(BillingRateModel.INSTALLMENTS)
		);
		
		System.assertEquals(true, pd.addPrebill(installmentPrebill));
		System.assertEquals(false, pd.addPrebill(installmentPrebill));
	}
	
	static testMethod void testFindFromOrderLineItem() {
		
		Date billingDate = Date.newInstance(2014,2,1);
	
		// required for billing account validation
		Account acc = new Account(
			Name = 'Test account',
			BillingCity = 'Bremen',
			BillingCountry = 'Germany',
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße',
			Type = 'Customer',
			Billing_Cycle__c = String.valueOf(billingDate),
			VAT__c = 1,
            Global__c = true
		);
	
		insert acc;
		
		Billing_Account__c billingAccount = new Billing_Account__c(
			Billing_Country__c = 'Deutschland',
			Payer__c = acc.Id,
			VAT__c = '1'
		);
		
		insert billingAccount;
		
		// M2M Order request
		Order_Request__c orderRequest = new Order_Request__c(
			Billing_Account__c = billingAccount.Id, Shipping_City__c='Test City', Shipping_Country__c = 'Test Country', Shipping_Postal_Code__c='10000', Shipping_Street__c='Test shipping street');
        
		insert orderRequest;
		
		csord__Order_Request__c csordRequest = new csord__Order_Request__c(
			csord__Module_Name__c = 'test', 
			csord__Module_Version__c = 'test'
		);
		insert csordRequest;
		
		csord__Order__c ord = new csord__Order__c(
			Order_Request__c = orderRequest.Id,
			// mandatory fields below
			csord__Identification__c = 'test', 
			csord__Order_Request__c = csordRequest.Id, 
			csord__Status2__c = 'test'
		);
		insert ord;
		
		Journal_Mapping__c journalMapping = new Journal_Mapping__c();
		insert journalMapping;
		
		csord__Order_Line_Item__c oli = new csord__Order_Line_Item__c(
			csord__Order__c = ord.Id,
			// validation below
			csord__Discount_Type__c = 'Amount',
			// mandatory fields below
			csord__Identification__c = 'test',
			csord__Order_Request__c = csordRequest.Id,
			Journal_Mapping__c = journalMapping.Id
		);
		insert oli;
		
		Daily_Bill__c dailyBill = new Daily_Bill__c(
			Date__c = billingDate
		);
		insert dailyBill;
		
		Prebill__c oliPrebill = PrebillHelper.buildPrebill(oli, dailyBill);
		
		PrebillLineItemDistributor pd = new PrebillLineItemDistributor(billingDate);
		pd.addPrebill(oliPrebill);
		
		Prebill__c foundPrebill = pd.findPrebill(oli);
		System.assertEquals(oliPrebill, foundPrebill);
	}
	
	static testMethod void testFindFromServiceLineItem() {
		Date billingDate = Date.newInstance(2014,2,1);
	
		// required for billing account validation
		Account acc = new Account(
			Name = 'Test account',
			BillingCity = 'Bremen',
			BillingCountry = 'Germany',
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße',
			Type = 'Customer',
			Billing_Cycle__c = String.valueOf(billingDate),
			VAT__c = 1,
            Global__c = true
		);
	
		insert acc;
		
		Billing_Account__c billingAccount = new Billing_Account__c(
			Billing_Country__c = 'Deutschland',
			Payer__c = acc.Id,
			VAT__c = '1'
		);
		
		insert billingAccount;
		
		csord__Order_Request__c csordRequest = new csord__Order_Request__c(
			csord__Module_Name__c = 'test', 
			csord__Module_Version__c = 'test'
		);
		insert csordRequest;
		
		csord__Subscription__c subscription =  new csord__Subscription__c(
			Billing_Account__c = billingAccount.Id,
			// mandatory fields below
			csord__Identification__c = 'test',
			csord__Order_Request__c = csordRequest.Id, 
			csord__Status__c = 'test'
		);
		insert subscription;
		
		csord__Service__c service = new csord__Service__c(
			csord__Subscription__c = subscription.Id,
			// mandatory fields below
			csord__Identification__c = 'test', 
			csord__Order_Request__c = csordRequest.Id, 
			csord__Status__c = 'test'
		);
		insert service;
		
		Journal_Mapping__c journalMapping = new Journal_Mapping__c();
		insert journalMapping;
		
		csord__Service_Line_Item__c sli = new csord__Service_Line_Item__c(
			csord__Service__c = service.Id,
			csord__Identification__c = 'test',
			csord__Order_Request__c = csordRequest.Id,
			Journal_Mapping__c = journalMapping.Id
		);
		insert sli;
		
		Daily_Bill__c dailyBill = new Daily_Bill__c(
			Date__c = billingDate
		);
		insert dailyBill;
		
		Prebill__c sliPrebill = PrebillHelper.buildPrebill(sli, dailyBill);
		
		PrebillLineItemDistributor pd = new PrebillLineItemDistributor(billingDate);
		pd.addPrebill(sliPrebill);
		
		Prebill__c foundPrebill = pd.findPrebill(sli);
		System.assertEquals(sliPrebill, foundPrebill);
	}
}