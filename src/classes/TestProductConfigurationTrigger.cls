@isTest
private class TestProductConfigurationTrigger
{
	//--------------------------------------------------------------------------
	// Test for deleting parent product configuration
	//--------------------------------------------------------------------------
    static testMethod void testDeleteProductConfiguration()
    {/* Commented - Pavan
        TestHelper.createProductConfigurations();
        cscfga__Product_Configuration__c  productConfiguration =
			[ SELECT Id, Name
			  FROM cscfga__Product_Configuration__c
			  WHERE Parent_Configuration__c = null ];
        List<cscfga__Product_Configuration__c>  productConfigurations =
			[ SELECT Id, Name
			  FROM cscfga__Product_Configuration__c
			  WHERE Parent_Configuration__c != null ];

        //system.assert(productConfigurations.size() > 0);

        delete productConfiguration;

		//
		// Check deleting
		//
		checkDeleting();
    */}

	//--------------------------------------------------------------------------
	// Test bulk deleting all product configurations
	//--------------------------------------------------------------------------
	static testMethod void testBulkDeleteProductConfigurations()
	{/* Commented whole method - pavan
		TestHelper.createProductConfigurations();
		List<cscfga__Product_Configuration__c> productConfigurations =
			[ SELECT Id, Name
			  FROM cscfga__Product_Configuration__c ];
		system.assert(productConfigurations.size() > 0);

		delete productConfigurations;

		//
		// Check deleting
		//
		checkDeleting();
	*/}

	//--------------------------------------------------------------------------
	// Check result of deleting product configurations
	//--------------------------------------------------------------------------
	static void checkDeleting()
	{
		//
		// Check if there are some configurations left
		//
        List<cscfga__Product_Configuration__c> productConfigurations =
			[ SELECT Id, Name
			  FROM cscfga__Product_Configuration__c
			  WHERE parent_product_configuration__c != null ];
        system.assertEquals(0, productConfigurations.size());
        List<Tariff__c> tariffList = [ SELECT Id,Parent_Tariff__c, Name FROM Tariff__c ];
        system.assertEquals(1, tariffList.size());
        system.assertEquals(null, tariffList[0].Parent_Tariff__c);
	}
}