public class SM2MErrorMessageParser extends ErrorMessageParser {
	
	private final String JSON_START_TAG = '[JSON]';
	private final String JSON_END_TAG = '[/JSON]';
	
	public SM2MErrorMessageParser(csam_t1__Incoming_Message__c incomingMessage) {
		super(incomingMessage);
	}
	
	public Boolean hasSM2MError() {
		Integer startIndex = this.getStackTrace().indexOf(JSON_START_TAG), endIndex = this.getStackTrace().indexOf(JSON_END_TAG);
		return startIndex != -1 && endIndex != -1 && startIndex < endIndex; 
	}
	
	public String getSM2MError() {
		if (!hasSM2MError()) {
			return null;
		} else {
			Integer startIndex = this.getStackTrace().indexOf(JSON_START_TAG) + JSON_START_TAG.length();
			Integer endIndex = this.getStackTrace().indexOf(JSON_END_TAG, startIndex);
			return this.getStackTrace().substring(startIndex, endIndex);
		}
	}
}