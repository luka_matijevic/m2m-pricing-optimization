public without sharing class TariffOption implements ITariffOption
{
	//--------------------------------------------------------------------------
	// Private members
	//--------------------------------------------------------------------------
	private Tariff_Option__c to;
	private List<IRateCard> rateCards;
	private List<IServiceFee> serviceFees;
	//private Integer quantity;
	private Boolean isSelected;

	//--------------------------------------------------------------------------
	// Public methods
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
		public TariffOption(Tariff_Option__c to, List<IRateCard> rateCards, List<IServiceFee> serviceFees, Boolean isSelected, Integer quantity)
	{
		//
		// Set values
		//
		this.to = to;
		this.rateCards = rateCards;
		this.serviceFees = serviceFees;
		this.isSelected = isSelected;
		this.setQuantity(quantity);
		//
		// If there is no list create empty list 
		//
		if (this.rateCards == null)
		{
			this.rateCards = new List<IRateCard>();
		}
		if (this.serviceFees == null)
		{
			this.serviceFees = new List<IServiceFee>();
		}
	}

	public TariffOption(Tariff_Option__c to, List<IRateCard> rateCards, List<IServiceFee> serviceFees, Boolean isSelected, Integer quantity, Integer displayQuantity)
	{
		//
		// Set values
		//
		this.to = to;
		this.rateCards = rateCards;
		this.serviceFees = serviceFees;
		this.isSelected = isSelected;
		this.setQuantity(quantity);
		this.setDisplayQuantity(displayQuantity);
		//
		// If there is no list create empty list 
		//
		if (this.rateCards == null)
		{
			this.rateCards = new List<IRateCard>();
		}
		if (this.serviceFees == null)
		{
			this.serviceFees = new List<IServiceFee>();
		}
	}

	//--------------------------------------------------------------------------
	// Getters
	//--------------------------------------------------------------------------
	public List<IRateCard> getRateCards()
	{
		return rateCards;
	}

	public List<IServiceFee> getServiceFees()
	{
		return serviceFees;
	}

	//--------------------------------------------------------------------------
	// Getters / setters
	//--------------------------------------------------------------------------
	public Tariff_Option__c getTariffOption()
	{
		return to;
	}

	public Boolean getIsSelected()
	{
		return isSelected;
	}

	public void setIsSelected(Boolean isSelected)
	{
		this.isSelected = isSelected;
	}

	public Integer getQuantity()
	{
	    return Integer.valueOf(to.Quantity__c);
	}

	public void setQuantity(Integer quantity)
	{
	    to.Quantity__c = quantity;
	}	

	public Integer getDisplayQuantity()
	{
	    return Integer.valueOf(to.Display_Quantity__c);
	}

	public void setDisplayQuantity(Integer quantity)
	{
	    to.Display_Quantity__c = quantity;
	}

	public void setshowOnQuote(Boolean showOnQuote)
	{
		to.Show_on_Quote__c = showOnQuote;
	}

	public Boolean getShowOnQuote()
	{
		return to.Show_on_Quote__c;
	}

	public void setName(String name)
	{
		to.Name = name;
	}

	public String getName()
	{
		return to.Name;
	}
	
	public void setOptionType(String optionType)
	{
		to.optionType__c = optionType;
	}

	public String getOptionType()
	{
		return to.optionType__c;
	}
	
	public void setType(String optionType)
	{
		to.Type__c = optionType;
	}

	public String getType()
	{
		return to.Type__c;
	}
	
	public void setDefaultConfiguration(Id defaultConfiguration)
	{
		to.Default_Configuration__c = defaultConfiguration;
	}

	public Id getDefaultConfiguration()
	{
		return to.Default_Configuration__c;
	}

	public void setUnit(String unit)
	{
		to.Usage_Unit__c = unit;
	}

	public String getUnit()
	{
		return to.Usage_Unit__c;
	}

	public Decimal getRecurringTargetFee()
	{
		return to.Recurring_Target_Fee__c;
	}

	public void setRecurringTargetFee(Decimal rcf)
	{
		to.Recurring_Target_Fee__c = rcf;
	}

	public Decimal getRecurringBaseFee()
	{
		return to.Recurring_Base_Fee__c;
	}

	public void setRecurringBaseFee(Decimal rcf)
	{
		to.Recurring_Base_Fee__c = rcf;
	}


    public Decimal getOneTimeTargetFee()
	{
		return to.One_Time_Target_Fee__c;
	}

	public void setOneTimeTargetFee(Decimal rcf)
	{
		to.One_Time_Target_Fee__c = rcf;
	}

	public Decimal getOneTimeBaseFee()
	{
		return to.One_Time_Base_Fee__c;
	}

	public void setOneTimeBaseFee(Decimal rcf)
	{
		to.One_Time_Base_Fee__c = rcf;
	}

	public Boolean getCanDiscount()
	{
		return to.Can_Discount__c;
	}

	public void setCanDiscount(Boolean canDiscount)
	{
		to.Can_Discount__c = canDiscount;
	}

	public Boolean getPerSim()
	{
		return to.Per_SIM__c;
	}

	public void setPerSim(Boolean perSim)
	{
		to.Per_SIM__c = perSim;
	}
	
	public Boolean getIsUsed()
	{
		return to.isUsed__c;
	}

	public void setIsUsed(Boolean isUsed)
	{
		to.isUsed__c = isUsed;
	}
	
	public Boolean getIsCustomOption()
	{
		return to.isCustomOption__c;
	}

	public void setIsCustomOption(Boolean isCustomOption)
	{
		to.isCustomOption__c = isCustomOption;
	}
	
	
	
	public Boolean getIsConfiguration()
	{
		return to.isConfiguration__c;
	}

	public void setIsConfiguration(Boolean isConfiguration)
	{
		to.isConfiguration__c = isConfiguration;
	}
		
	public Boolean getIsAnyTargetFeeDiscounted()
	{
		return (isTargetFeeDiscounted() || isAnyRateCardDiscounted() || isAnyServiceFeeDiscounted());
	}

	private Boolean isTargetFeeDiscounted()
	{
		if (to.Recurring_Target_Fee__c != null && to.Recurring_Base_Fee__c != null)
			return to.Recurring_Target_Fee__c != to.Recurring_Base_Fee__c;

		return false;
	}

	private Boolean isAnyRateCardDiscounted()
	{
		for (IRateCard rateCard : rateCards)
		{
			if (rateCard.getIsTargetFeeDiscounted())
				return true;
		}
		return false;
	}

	private Boolean isAnyServiceFeeDiscounted()
	{
		for (IServiceFee serviceFee : serviceFees)
		{
			if (serviceFee.getIsTargetFeeDiscounted())
				return true;
		}
		return false;
	}


    public Decimal getEditedFee()
	{
		return to.Edited_Fee__c;
	}

	public void setEditedFee(Decimal rcf)
	{
		to.Edited_Fee__c = rcf;
	}

    
	//--------------------------------------------------------------------------
	// Clone tariff option with all rate cards and service fees
	//--------------------------------------------------------------------------
	public ITariffOption deepClone()
	{
		//
		// Clone rate cards
		//
		List<IRateCard> clonedRateCards = new List<IRateCard>();
		for (IRateCard rateCard : getRateCards())
		{
			clonedRateCards.add(rateCard.deepClone());
		}

		//
		// Clone service fees
		//
		List<IServiceFee> clonedServiceFees = new List<IServiceFee>();
		for (IServiceFee serviceFee : getServiceFees())
		{
			clonedServiceFees.add(serviceFee.deepClone());
		}

		//
		// Clone Tariff_Option__c
		//
		Tariff_Option__c clonedTo = to.clone();

		ITariffOption clonedTariffOption =
			new TariffOption(clonedTo, clonedRateCards, clonedServiceFees,
					getIsSelected(), getQuantity(), getDisplayQuantity());
		return clonedTariffOption;
	}

	//--------------------------------------------------------------------------
	// Compares two tariff options
	//--------------------------------------------------------------------------
	public Boolean equals(ITariffOption tariffOption)
	{
		//
		// Compare tariff option
		//
		if (getQuantity() !=  tariffOption.getQuantity())
		{
			return false;
		}

		return equalsWithoutQuantity(tariffOption);
	}

	//--------------------------------------------------------------------------
	// Compare two tariff options ignoring quantity
	//--------------------------------------------------------------------------
	public Boolean equalsWithoutQuantity(ITariffOption tariffOption)
	{
		//
		// Compare tariff option without quantity
		//
		if (!equalsTOWithoutQuantity(tariffOption))
		{
			return false;
		}

		//
		// Compare rate cards
		//
		if (getRateCards().size() != tariffOption.getRateCards().size())
		{
			return false;
		}
		for (Integer i = 0; i < getRateCards().size(); i++)
		{
			if (!getRateCards()[i].equals(tariffOption.getRateCards()[i]))
			{
				return false;
			}
		}

		//
		// Compare service fees
		//
		if (getServiceFees().size() != tariffOption.getServiceFees().size())
		{
			return false;
		}
		for (Integer i = 0; i < getServiceFees().size(); i++)
		{
			if (!getServiceFees()[i].equals(tariffOption.getServiceFees()[i]))
			{
				return false;
			}
		}
		return true;
	}

	//--------------------------------------------------------------------------
	// Compare two tariff options ignoring configuration
	//--------------------------------------------------------------------------
	public Boolean equalsWithoutConfiguration(ITariffOption tariffOption)
	{
		//
		// Compare tariff option without quantity
		//
		if (!equalsTOWithoutConfiguration(tariffOption))
		{
			return false;
		}

		//
		// Compare rate cards
		//
		if (getRateCards().size() != tariffOption.getRateCards().size())
		{
			return false;
		}
		for (Integer i = 0; i < getRateCards().size(); i++)
		{
			if (!getRateCards()[i].equalsWithoutConfiguration(tariffOption.getRateCards()[i]))
			{
				return false;
			}
		}

		//
		// Compare service fees
		//
		if (getServiceFees().size() != tariffOption.getServiceFees().size())
		{
			return false;
		}
		for (Integer i = 0; i < getServiceFees().size(); i++)
		{
			if (!getServiceFees()[i].equalsWithoutConfiguration(tariffOption.getServiceFees()[i]))
			{
				return false;
			}
		}
		return true;
	}

	//--------------------------------------------------------------------------
	// Compare only tariff option without quantity
	//--------------------------------------------------------------------------
	private Boolean equalsTOWithoutQuantity(ITariffOption tariffOption)
	{
		if (getIsSelected() != tariffOption.getIsSelected()
			|| getTariffOption().Show_on_Quote__c != tariffOption.getTariffOption().Show_on_Quote__c
			|| getTariffOption().Recurring_Target_Fee__c != tariffOption.getTariffOption().Recurring_Target_Fee__c)
		{
			return false;
		}
		return equalsTOWithoutConfiguration(tariffOption);
	}

	//--------------------------------------------------------------------------
	// Compare only tariff option without configuration
	//--------------------------------------------------------------------------
	private Boolean equalsTOWithoutConfiguration(ITariffOption tariffOption)
	{
		if (getTariffOption().Name != tariffOption.getTariffOption().Name
			|| getTariffOption().Can_Discount__c != tariffOption.getTariffOption().Can_Discount__c
			|| getTariffOption().Clock_Rate__c != tariffOption.getTariffOption().Clock_Rate__c
			|| getTariffOption().Commercial_Plan__c != tariffOption.getTariffOption().Commercial_Plan__c
			|| getTariffOption().Description_DE__c != tariffOption.getTariffOption().Description_DE__c
			|| getTariffOption().Description_ENG__c != tariffOption.getTariffOption().Description_ENG__c
			|| getTariffOption().No_of_Included_Units__c != tariffOption.getTariffOption().No_of_Included_Units__c
			|| getTariffOption().Recurring_Base_Fee__c != tariffOption.getTariffOption().Recurring_Base_Fee__c
			|| getTariffOption().Sequence_No__c != tariffOption.getTariffOption().Sequence_No__c
			|| getTariffOption().Type__c != tariffOption.getTariffOption().Type__c
			|| getTariffOption().Usage_Unit__c != tariffOption.getTariffOption().Usage_Unit__c)
		{
			return false;
		}
		return true;
	}
}