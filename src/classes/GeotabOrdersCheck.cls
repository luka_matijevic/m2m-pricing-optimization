global class GeotabOrdersCheck implements Schedulable {
	global void execute(SchedulableContext sc) {
		GEOTAB_Settings__c geotabSettings = GEOTAB_Settings__c.getInstance();
		//get all GEOTAB order request
		List<Order_request__c> orderRequestList = [SELECT id, order_quantity__c from order_request__c where recordtypeId = :geotabsettings.Order_Request_Type_Id__c and order_posted_in_geotab__c = true];
		
		AggregateResult[] groupedResults = [select count(id) numOfStocks, order_request__r.Id ordReqId from stock__c where order_request__r.recordtypeId = :geotabsettings.Order_Request_Type_Id__c
		group by order_request__r.id];

		Map<Id, Integer> ordReqStockNumberMap = new Map<Id, Integer>();
		Set<Id> ordReqIdSet = new Set<Id>();
		for (AggregateResult ar : groupedResults){
		    String ordReqId = String.valueOf(ar.get('ordReqId'));
		    Integer sumQuantity = Integer.valueOf(String.valueOf(ar.get('numOfStocks')));
		    ordReqStockNumberMap.put(ordReqId, sumQuantity);
		    ordReqIdSet.add(ordReqId);
		}


		/*List<Order_request__c> ordReqList = [SELECT id, name, order_quantity__c 
		                                     from order_request__c 
		                                     where id in :ordReqIdSet];*/

		Set<Id> ordReqIdToPingGeotabSet = new Set<Id>();
		for (Order_request__c ordReq : orderRequestList){
		    Integer expected = Integer.valueOf(ordReq.order_quantity__c);
		    Integer realNumber = 0;
		    if (ordReqStockNumberMap.get(ordReq.Id) != null){
		    	realNumber = Integer.valueOf(ordReqStockNumberMap.get(ordReq.Id));
		    }
		    if (expected > realNumber){
		        ordReqIdToPingGeotabSet.add(ordReq.Id);
		    }
		}

		List<Order_Request__c> orRequests = [SELECT Id, Name, Frame_Contract__c, Geotab_Purchase_Order_Number__c, Account__c, Account__r.Name FROM Order_Request__c WHERE Id in :ordReqIdToPingGeotabSet];

		if(orRequests != null && orRequests.size() > 0) {
			System.debug(orRequests);
			Database.executeBatch(new GeotabOrdersCheckBatch(orRequests), 1);
		}
		GeotabOrdersCheck.scheduleTick(DateTime.now().addDays(1));
	}



	private static void scheduleTick(Datetime dt) {
		String chron_exp = dt.format('ss mm HH dd MM ? yyyy');
		GeotabOrdersCheck poller = new GeotabOrdersCheck();
		System.schedule(jobName(dt), chron_exp, poller);
	}

	private static String jobName(Datetime dt) {
		return 'GeotabOrdersCheck_' + dt.getTime();
	}

}