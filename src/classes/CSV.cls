public class CSV {

	private static final String OFI_DATE_FORMAT = 'ddMMyyyy';

	public static String ofi(Object o) {
		String retText = '';
		if (o == null){
			retText = '';
		} else if (o instanceof String) {
			retText = ((String) o).escapeCsv();
		} else if (o instanceof Date) {
			retText = ofiDate((Date) o);
		} else if (o instanceof Double) {
			retText = ofiDouble((Double) o);
		} else if (o instanceof Integer) {
			retText = ofiInteger((Integer) o);
		} else if (o instanceof Decimal) {
			retText = ofiDecimal((Decimal) o);
		} else if (o instanceof Long) {
			retText = ofiLong((Long) o);
		} else if (o instanceof DateTime) {
			retText = ofiDatetime((DateTime) o);
		}
		else{
			// for not supported types, return empty string
			retText = '';
		}
		
		retText = removeSpecialCharacters(retText);
		return retText;
	}
	
	public static String escape(String str) {
		return str.escapeCsv();
	}

	private static String ofiDate(Date d) {
		return DateUtils.formatDate(d, OFI_DATE_FORMAT).escapeCsv();
	}
	
	private static String ofiDatetime(Datetime dt) {
		return dt.format(OFI_DATE_FORMAT).escapeCsv();
	}

	private static String ofiInteger(Integer num) {
		return String.valueOf(num);
	}

	private static String ofiDouble(Double num) {
		return ofiDecimal(Decimal.valueOf(num));
	}

	private static  String ofiLong(Long num) {
		return ofiDecimal(Decimal.valueOf(num));
	}

	// CR Dev_324
	private static String ofiDecimal(Decimal num) {
		return num.setScale(2, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString().replace('.', ',');
	}

	private static String removeSpecialCharacters(String text){
		String textReplaced = text;
		textReplaced = textReplaced.replace('Ö', 'OE');
		textReplaced = textReplaced.replace('ö', 'oe');
		textReplaced = textReplaced.replace('Ü', 'UE');
		textReplaced = textReplaced.replace('ü', 'ue');
		textReplaced = textReplaced.replace('Ä', 'AE');
		textReplaced = textReplaced.replace('ä', 'ae');
		return textReplaced;
	}
}