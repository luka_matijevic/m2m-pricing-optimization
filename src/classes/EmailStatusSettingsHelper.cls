public class EmailStatusSettingsHelper {

	public static Integer getEmailStatusPollerDelaySeconds() {
		return (Integer) Case_Email_Status_Settings__c.getOrgDefaults().Poller_Delay_Seconds__c;
	}

	public static Boolean isDisableEmailStatusPollerReschedule() {
		return Case_Email_Status_Settings__c.getOrgDefaults().Disable_Poller_Reschedule__c == true;
	}
}