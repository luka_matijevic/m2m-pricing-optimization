global with sharing class simData {
	public Id stockId {get;set;}
	public String ICCID {get;set;}
	public String LifeCycleStatus{get;set;}
	public String SubscriptionGroup{get;set;}
	public String APN {get;set;}
	public String message {get; set;}
	public Id articleInfoSetId {get;set;}
	public boolean exists {get;set;}
	public boolean published {get;set;}
	public boolean locked{get;set;}
	public Id tufId {get;set;}
	public String articleInfoSetName{get;set;}
	public Integer positionInList {get;set;}
	public String msisdn {get;set;}
	public simData() {
		
	}
}