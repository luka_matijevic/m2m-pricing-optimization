@isTest
private class GeotabWarrantyRefreshBatchTest {
	
	@isTest static void test_method_one() {
		List<GeotabUtils.GeotabWarrantyOption> geotabWarrantyApi = new List<GeotabUtils.GeotabWarrantyOption>();
		GeotabUtils.GeotabWarrantyOption geotabWarranty = new GeotabUtils.GeotabWarrantyOption();

		geotabWarranty.Name = 'test display name';
		geotabWarranty.Id = 2;

		geotabWarrantyApi.add(geotabWarranty);

		Geotab_Warranty_Option__c gwarrant = new Geotab_Warranty_Option__c();
		gwarrant.Geotab_Id__c = '1';
		gwarrant.Geotab_Warranty_Name__c = 'First Warranty';
		insert gwarrant;

		Test.startTest();
		Database.executeBatch(new GeotabWarrantyOptionRefreshBatch(geotabWarrantyApi));
		Test.stopTest();
	}	
	
}