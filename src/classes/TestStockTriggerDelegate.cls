@isTest
private class TestStockTriggerDelegate
{
	static void dataSetup()
	{
		No_Trigger__c notriggers = new No_Trigger__c();
		notriggers.Flag__c = true;
		insert notriggers;

		//
		// Create test data
		//
		TestHelper.createOrderRequest();
		notriggers.Flag__c = false;
		update notriggers;
	}

	//--------------------------------------------------------------------------
	// Test inserting new SIMs attached to SRF and Third Upload File
	//--------------------------------------------------------------------------
	static testMethod void testInsertingSIMs()
	{
		dataSetup();

		Test.startTest();

		Account account = [ SELECT Id FROM Account LIMIT 1 ];
		Order_Request__c ord = [ SELECT Id FROM Order_Request__c LIMIT 1 ];
		Article_Info_Set__c ais =
				[ SELECT Id
				  FROM Article_Info_Set__c
				  WHERE Article_Type__c = 'SIM'
				  LIMIT 1 ];

		Third_Upload_File__c thirdFile = new Third_Upload_File__c
			( Name = 'name'
			, Order_Request__c = ord.Id );
		insert thirdFile;

		SRF__c srf = new SRF__c
				( Account__c = account.Id
				, SIM_Type__c = ais.Id
				, Order_Request__c = ord.Id );
		insert srf;

		Stock__c[] assets = new List<Stock__c>();
		assets.add(new Stock__c
			( Name = 'Stock1'
			, SIM_Type__c = 'LOCAL'
			, SRF__c = srf.Id
			, Third_Upload_File__c = thirdFile.Id ));
		assets.add(new Stock__c
			( Name = 'Stock2'
			, SIM_Type__c = 'LOCAL'
			, SRF__c = srf.Id
			, Third_Upload_File__c = thirdFile.Id  ));
		insert assets;
		//
		// Assertions
		//
		assets = [ SELECT Account__c FROM Stock__c ];
		System.assertEquals(2, assets.size());
	}

	//--------------------------------------------------------------------------
	// Test updating SIMs attached to SRF and Third Upload File
	//--------------------------------------------------------------------------
	static testMethod void testUpdatingSIMs()
	{
		dataSetup();

		Test.startTest();

		Account account = [ SELECT Id FROM Account LIMIT 1 ];
        account.Global__c = true;
        update account;
		Order_Request__c ord = [ SELECT Id FROM Order_Request__c LIMIT 1 ];
		Article_Info_Set__c ais =
				[ SELECT Id
				  FROM Article_Info_Set__c
				  WHERE Article_Type__c = 'SIM'
				  LIMIT 1 ];

		Third_Upload_File__c thirdFile = new Third_Upload_File__c
			( Name = 'name'
			, Order_Request__c = ord.Id );
		insert thirdFile;

		SRF__c srf = new SRF__c
				( Account__c = account.Id
				, SIM_Type__c = ais.Id );
		insert srf;

		Stock__c[] assets = new List<Stock__c>();
		assets.add(new Stock__c
				( Name = 'Stock1'
				, SIM_Type__c = 'LOCAL' ));
		assets.add(new Stock__c
				( Name = 'Stock2'
				, SIM_Type__c = 'LOCAL' ));

		assets.add(new Stock__c
				( Name = 'Stock3'
				, SIM_Type__c = 'LOCAL'
				, MSISDN__c = '123' ));

		insert assets;

		assets[0].Third_Upload_File__c = thirdFile.Id;
		assets[0].SM2M_Status__c = 'ACTIVE'; /* SM2M_Status__c & State_Change_Date__c Added for T-14128 */

		assets[0].State_Change_Date__c = System.now()+10;
		assets[1].SRF__c = srf.Id;
		assets[1].SM2M_Status__c = 'DEACTIVATED'; /* SM2M_Status__c & State_Change_Date__c Added for T-14128 */
		assets[1].State_Change_Date__c = System.now()+10;
		update assets;
		
		assets[1].VVL_date__c = null;

		/*Quarantine*/
		assets[2].SM2M_Status__c = 'RETIRED'; 
		assets[2].State_Change_Date__c = System.now()+10;
		update assets;
		/*Prevent setting quarantined MSISDN*/
		try 
		{	 
			assets[2].MSISDN__c = '';
			assets[0].MSISDN__c = '123';       
			update assets;
		}
		catch (Exception e)
		{
			Boolean expectedException = e.getMessage().contains('MSISDN is quarantined until') ;
			System.assertEquals(expectedException, true); 
		}

		
		//
		// Assertions
		//
		assets = [ SELECT Account__c FROM Stock__c ];

		System.assertEquals(3, assets.size());
	}
	
	static testMethod void endDateCalulationTest_Trail(){
        dataSetup();
	    Id commercialRTId = Schema.SObjectType.Frame_Contract__c.getRecordTypeInfosByName().get('Trial').getRecordTypeId();
	    Account account = [ SELECT Id FROM Account LIMIT 1 ];
	    Frame_Contract__c fc = new Frame_Contract__c(RecordTypeID =commercialRTId );
        fc.Customer__c = account.id;
	    insert fc;
	    
	    Order_Request__c ordReq = new Order_Request__c(Frame_Contract__c = fc.id,Runtime__c = 12, Shipping_City__c= 'Test City',Shipping_Country__c='Test Country', Shipping_Postal_Code__c='10000', Shipping_Street__c = 'Test shipping street', Use_Account_Address__c = true);
	    insert ordReq;
	    
	    Third_Upload_File__c tuf = new Third_Upload_File__c(Order_Request__c = ordReq.id,Name='TUf Test');
	    insert tuf;
	    
	    Stock__c stk = new Stock__c(Name = 'Stock',Third_Upload_File__c = tuf.id,Activation_Date__c = system.today(),Commercial_Date__c =system.today() );
	    insert stk;
	    
	    stk.VVL_date__c = null;
	    update stk;
	    
	    
	}
	
	static testMethod void endDateCalulationTest_Commercial(){
        dataSetup();
	    Id commercialRTId = Schema.SObjectType.Frame_Contract__c.getRecordTypeInfosByName().get('Commercial').getRecordTypeId();
	    Account account = [ SELECT Id FROM Account LIMIT 1 ];
	    Frame_Contract__c fc = new Frame_Contract__c(RecordTypeID =commercialRTId );
        fc.Customer__c = account.id;
	    insert fc;
	    
	    Order_Request__c ordReq = new Order_Request__c(Frame_Contract__c = fc.id,Runtime__c = 12, Shipping_City__c= 'Test City',Shipping_Country__c='Test Country', Shipping_Postal_Code__c='10000', Shipping_Street__c = 'Test shipping street', Use_Account_Address__c = true);
	    insert ordReq;
	    
	    Third_Upload_File__c tuf = new Third_Upload_File__c(Order_Request__c = ordReq.id,Name='TUf Test');
	    insert tuf;
	    
	    Stock__c stk = new Stock__c(Name = 'Stock',Third_Upload_File__c = tuf.id,Activation_Date__c = system.today(),Commercial_Date__c =system.today(),SM2M_Status__c = Constants.ACTIVE );
	    insert stk;
	    
	    stk.VVL_date__c = null;
	    stk.SM2M_Status__c = Constants.DEACTIVATED;
	    stk.State_Change_Date__c = system.now()+15;
	    update stk;
	    
	    
	}
	
	
	static testMethod void simDelivery(){
	    
	    Account acc = Test_Util.createAccount('Account');
        acc.Global__c = true;
	    insert acc;
	    
	    Opportunity opp = Test_Util.createOpportynity('Opportunity', acc.id, System.today(),'Draft','Web',100, 100);
	    opp.Description = 'Test description 20 characters';
	    insert opp;
	    
	    
	    
	    Article_Info_Set__c articleInfo = new Article_Info_Set__c(Global_Local__c = 'Local',Name = 'Article Onfo Set',Status__c ='Active',One_Off_Journal_Mapping__c ='Data Usage fee',Journal_Mapping__c = 'Data Usage Fee',Contract_Term__c ='12',One_Time_Fee__c = 100,Recurring_Fee__c =100,Article_Type__c= 'SIM');
	    insert articleInfo;
	    
	    
	    Id simDeliveryRtId = Schema.SObjectType.SRF__c.getRecordTypeInfosByName().get('Direct SIM Delivery').getRecordTypeId();
	    
	    SRF__c srf = new SRF__c(recordTypeId=simDeliveryRtId,Name ='SRF',Opportunity__c = opp.id,Account__c = acc.id,SIM_Type__c =articleInfo.id);
	    insert srf;
	    
	    
	    Id commercialRTId = Schema.SObjectType.Frame_Contract__c.getRecordTypeInfosByName().get('Commercial').getRecordTypeId();
	    Frame_Contract__c fc = new Frame_Contract__c(RecordTypeID =commercialRTId, customer__c = acc.id );
        
	    insert fc;
	    
	    Order_Request__c ordReq = new Order_Request__c(Frame_Contract__c = fc.id,Runtime__c = 12, Shipping_City__c= 'Test City',Shipping_Country__c='Test Country', Shipping_Postal_Code__c='10000', Shipping_Street__c = 'Test shipping street', Use_Account_Address__c = true);
	    insert ordReq;
	    
	    Third_Upload_File__c tuf = new Third_Upload_File__c(Order_Request__c = ordReq.id,Name='TUf Test');
	    insert tuf;
	    
	    
	    
	    
	    Id simRtId = Schema.SObjectType.Stock__C.getRecordTypeInfosByName().get('SIM').getRecordTypeId();
	    Stock__c stk = new Stock__c(recordTypeId=simRtId,Name = 'Stock',Third_Upload_File__c = tuf.id,Activation_Date__c = system.today(),Commercial_Date__c =system.today(),SM2M_Status__c = 'Active' );
	    stk.End_Customer_SPIN_ID__c = '1234567898765432';
	    insert stk;
	    

	}
	
	
	
	
}