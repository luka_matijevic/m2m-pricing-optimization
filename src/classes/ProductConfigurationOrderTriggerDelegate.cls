public with sharing class ProductConfigurationOrderTriggerDelegate extends TriggerHandler.DelegateBase {

	List<product_configuration_order__c> pcoList = new List<product_configuration_order__c>();
	List<Order_Request__c> orderRequestList = new List<Order_Request__c>();
	public ProductConfigurationOrderTriggerDelegate() {}

	public override void prepareBefore() {}

	public override void prepareAfter() {

        List<product_configuration_order__c> ordReqIds = [select 	Product_Order__r.Order_request__r.Id
        			from 	product_configuration_order__c 
        			where 	id in :trigger.new AND
        					product_configuration__r.name = 'CS-APN / VPN'];

        if(ordReqIds != null && ordReqIds.size() > 0){
        	List<String> ids = new List<String>();
        	for (product_configuration_order__c pco : ordReqIds){
        		ids.add(pco.Product_Order__r.Order_request__r.Id);
        	}
        	if (ids != null && ids.size() > 0){
        		orderRequestList = [SELECT 	id, 
        								name, 
        								Createddate 
        						FROM Order_Request__c
        						where Id in :ids];
        	}
        	
        }
    }

	public override void beforeInsert(sObject o) {
        // Apply before insert logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method    
    }

    public override void beforeUpdate(sObject old, sObject o) {
    	// Apply before update logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void beforeDelete(sObject o) {
    	// Apply before delete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }


    public override void afterInsert(sObject o) {
        // Apply after insert logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterUpdate(sObject old, sObject o) {
        // Apply after update logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterDelete(sObject old) {
        // Apply after delete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method        
    }

    public override void afterUndelete(sObject old) {
        // Apply after undelete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method        
    }

    public override void finish() {
    	if (orderRequestList != null && orderRequestList.size() > 0){
    		ApnOrderNotificator apnNotificator = new ApnOrderNotificator();
    		for (Order_Request__c ordReq : orderRequestList){
    				apnNotificator.sendNotification(ordReq);
    		}
    	}
	}
}