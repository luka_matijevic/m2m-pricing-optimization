public class OfiUtils {
	
	private static final Decimal ONE_CENT = 0.01;
	
	public static Boolean hasOneCentOrMore(Prebill_Line_Item__c pbli) {
		if (pbli.Line_Item_Amount__c == null) {
			return false;
		} else {
			return pbli.Line_Item_Amount__c >= ONE_CENT;
		}
	}
	
	public static Boolean isProjektZero(Prebill_Line_Item__c pbli) {
		Boolean isZero = false;
		if (pbli.Projekt__c == null) {
			isZero = true;
		}
		try {
			if (Integer.valueOf(pbli.Projekt__c) == 0) {
				isZero = true;
			}
		} catch (Exception e) {
			// swallowed intentionally
		}
		return isZero;
	}
}