public with sharing class ProductBundleTriggerDelegate extends TriggerHandler.DelegateBase  {

	List<Opportunity> syncedBundleOpportunities = new List<Opportunity>();

	List<Unit_Schedule__c> oldUnitSchedules = new List<Unit_Schedule__c>();
	List<Unit_Schedule__c> newUnitSchedules = new List<Unit_Schedule__c>();

	List<Opportunity> opportunitiesWithSynchronizationState = new List<Opportunity>();
	
	Map<Id, String> bundleAttributesToUpdate = new Map<Id, String>();
	Set<Id> listOfTechnicalIds = new Set<Id>();
	Set<Id> listOfCommercialIds = new Set<Id>();
	
	List<cscfga__Attribute__c> Technicalattrs = new List<cscfga__Attribute__c>();
    List<cscfga__Attribute__c> Commercialattrs = new List<cscfga__Attribute__c>();
    
	String requiresTechnicalApproval, requiresCommercialApproval;
	
	ProcessInstance currentApprovalProcess;

	List<ICAP_Bundle_Number__c> icapBundleNumeberSetting = [select id, name, order_number__c from icap_bundle_number__c];
	Map<Id, Opportunity> mapOpportunity = new Map<Id, Opportunity>();
	Boolean icapBundleNumberChanged = false;
	Integer icapBundleNumber;


	public override void prepareBefore() {

		if(Avoid_Recursive.doRun){
			Avoid_Recursive.runOnce();
			// all bundles that are being synced have to have at least one opportunity contact role on their opportunities
			if(Trigger.isUpdate && Trigger.isBefore) {
				Map<Id, Id> bundleToOppMap = new Map<Id, Id>();
				for(cscfga__Product_Bundle__c bundle : (List<cscfga__Product_Bundle__c>) trigger.new) {
					cscfga__Product_Bundle__c afterUpdate = (cscfga__Product_Bundle__c) trigger.oldMap.get(bundle.Id);
					if(bundle.cscfga__Synchronised_with_Opportunity__c == true && afterUpdate.cscfga__Synchronised_with_Opportunity__c == false){
						bundleToOppMap.put(bundle.Id, bundle.cscfga__Opportunity__c);
					}
				}
				List<cscfga__Product_Bundle__c> bundlesWithNoOCRs = [SELECT Id
																	FROM cscfga__product_bundle__c
																	WHERE cscfga__opportunity__c NOT IN
																		(SELECT OpportunityId
																		FROM OpportunityContactRole
																		WHERE OpportunityId IN:bundleToOppMap.values()) AND Id IN:bundleToOppMap.keySet()];
	
				for(cscfga__Product_Bundle__c bundle : bundlesWithNoOCRs){
					trigger.newMap.get(bundle.id).addError('The bundle cannot be synced with opportunity in case the opportunity has no opportunity contact roles defined.');
				}
			    if (!Test.isRunningTest()) {
	    		    AttributeService attribService = new AttributeService();
	    		    for(cscfga__Product_Bundle__c b : (List<cscfga__Product_Bundle__c>) Trigger.new) {
	    			if(b.cscfga__Bundle_Status__c.equalsIgnoreCase('Valid')) {
	    				if(b.Technical_Approval_Status__c == null) {
	                    	listOfTechnicalIds.add(b.Id);
	    				}
	    				
	    				if(b.Commercial_Approval_Status__c == null) {			   
	    				   listOfCommercialIds.add(b.Id);
	        				}
	        			}
	        		} 
	    		
	        		Technicalattrs = attribService.getRequiresApprovalAttribute(listOfTechnicalIds, new String[] { 'Requires Technical Approval' });
	        		Commercialattrs = attribService.getRequiresApprovalAttribute(listOfCommercialIds, new String[] { 'Requires Commercial Approval' });
	                }
	          }
		}

		Set<String> setOppId = new Set<String>();
		for(cscfga__Product_Bundle__c bundle : (List<cscfga__Product_Bundle__c>) trigger.new){
			setOppId.add(bundle.cscfga__opportunity__c);
		}
		//18.3.2017. Pm Added
		System.debug('opps' + setOppId);
		mapOpportunity = new Map<Id, Opportunity>([Select id, recordType.Name, Record_Type_Name__c from Opportunity where id in :setOppId]);
		System.debug('mapOpps ' + mapOpportunity);
		if (icapBundleNumeberSetting != null && icapBundleNumeberSetting.size() > 0)
			icapBundleNumber = Integer.valueOf(icapBundleNumeberSetting[0].order_number__c);
			system.debug(icapBundleNumber);
	}



	public override void prepareAfter() {
		
		//if(Avoid_Recursive.doRun){
			//Avoid_Recursive.runOnce();
			// do any preparation here – bulk loading of data etc
			ProductConfigurationService prodConfigService = new ProductConfigurationService();
			OpportunityService oppService = new OpportunityService();
			AttributeService attribService = new AttributeService();
	
			List<cscfga__Product_Bundle__c> bundles = (trigger.isDelete) ? trigger.old : trigger.new;
			List<Opportunity> relatedOpportunities = oppService.extractRelatedOpportunities(bundles);
	
			// after any change to the product bundle, the HasSynchedBundle__c and Synched_Bundle_Id__c should be updated on the related opportunities
			opportunitiesWithSynchronizationState = oppService.prepareSynchronizationState(relatedOpportunities);
	        system.debug('opportunitiesWithSynchronizationState'+opportunitiesWithSynchronizationState);
			// if an update was made to the product bundle, revenue, contract terms and unit schedules should be recalculated
			
			if (Trigger.isUpdate) {
	
				List<cscfga__Product_Bundle__c> syncedBundles = getSynchronizedBundles((Map<Id, cscfga__Product_Bundle__c>)Trigger.oldMap, (List<cscfga__Product_Bundle__c>)Trigger.new);
				Map<Id, Opportunity> opportunityMap = oppService.extractRelatedOpportunitiesMap(syncedBundles);
				// Add a check for opportunity contact role - SPIN integration requirement (Hueseyin on 12.2.2014.)
				System.debug('syncedBundles ' + syncedBundles);
				List<cscfga__Product_Configuration__c> productConfigurations = prodConfigService.getProductConfigurations(syncedBundles);
	
				Boolean isRecurring = true;
				List<cscfga__Attribute__c> recurringPriceAttributes = attribService.getPriceAttributes(productConfigurations, isRecurring);
				List<cscfga__Attribute__c> onetimePriceAttributes = attribService.getPriceAttributes(productConfigurations, !isRecurring);
				List<cscfga__Attribute__c> quantityAttributes = attribService.getQuantityAttributes(productConfigurations);
				
				System.debug('productConfigurations '+ productConfigurations);
				System.debug('Testing quantityAttributes' + quantityAttributes);
	
				Map<Id, Opportunity> oppWithRevenueMap = prodConfigService.calculateRevenue(productConfigurations, recurringPriceAttributes, onetimePriceAttributes, quantityAttributes);
				Map<Id, Opportunity> oppWithContractTermsMap = prodConfigService.calculateContractTerms(productConfigurations);
	
				for (Id oppId : opportunityMap.keySet()) {
					SObjectHelper.mergeFields(opportunityMap.get(oppId), oppWithRevenueMap.get(oppId));
					SObjectHelper.mergeFields(opportunityMap.get(oppId), oppWithContractTermsMap.get(oppId));
				}
	
				syncedBundleOpportunities = opportunityMap.values();
	
				//UnitScheduleService unitScheduleService = new UnitScheduleService();
				//System.debug('oppMap - ' + opportunityMap + ' ;productConfigurations - ' + productConfigurations + ' ;quantityAttributes - ' + quantityAttributes);
				//newUnitSchedules = unitScheduleService.buildUnitSchedules(opportunityMap.values(), productConfigurations, quantityAttributes);
				oldUnitSchedules = [Select Id from Unit_Schedule__c where Opportunity__c IN :opportunityMap.keySet()];
			}
			
			
		//}
		
	}

    public override void beforeUpdate(sObject old, sObject o) {
    	// Apply beforeUpdate logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
       cscfga__Product_Bundle__c oldPB = (cscfga__Product_Bundle__c)old;
       cscfga__Product_Bundle__c newPB = (cscfga__Product_Bundle__c)o;
       
       if( newPB.cscfga__Synchronised_with_Opportunity__c &&oldPB.cscfga__Synchronised_with_Opportunity__c != newPB.cscfga__Synchronised_with_Opportunity__c){
           newPB.Synchronization_Date__c = Date.valueOf(System.today());
       }
       
       if(Technicalattrs != null) {
	   		for(cscfga__Attribute__c atr : Technicalattrs) {
	   			if(atr.cscfga__Product_Configuration__r.cscfga__Product_Bundle__c == newPB.Id) {
	   				if(atr.Name.equalsIgnoreCase('Requires Technical Approval') && String.valueOf(atr.cscfga__Value__c).equalsIgnoreCase('Yes')) {
	   					newPB.Technical_Approval_Status__c = 'Required';       					
	            	}
	   			}
	   		}
       }
        if(Commercialattrs != null) {
	   		for(cscfga__Attribute__c atr : Commercialattrs) {
	   			if(atr.cscfga__Product_Configuration__r.cscfga__Product_Bundle__c == newPB.Id) {
	            	if(atr.Name.equalsIgnoreCase('Requires Commercial Approval') && String.valueOf(atr.cscfga__Value__c).equalsIgnoreCase('Yes')) {
						newPB.Commercial_Approval_Status__c = 'Required';				
	            	}
	   			}
	   		}
       }
    }
	
    public override void beforeInsert(sObject o){
    	System.debug('after insert ' + 0);
    	System.debug('mapa ' + mapOpportunity);
    	if (Trigger.isInsert){
    		cscfga__product_bundle__c prodBundle = (cscfga__product_bundle__c)o;
    		system.debug(mapOpportunity.get(prodBundle.cscfga__opportunity__c).Record_Type_Name__c);
    		if (mapOpportunity.containsKey(prodBundle.cscfga__opportunity__c) && icapBundleNumber != null && mapOpportunity.get(prodBundle.cscfga__opportunity__c).Record_Type_Name__c == 'IoT Connect Advanced'){
    			system.debug('ICap!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    			prodBundle.Name = 'ICAP-' + icapBundleNumber;
    			icapBundleNumberChanged = true;
    			icapBundleNumber++;
    		}
			/*
    		else{
    			system.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    			prodBundle.Name = prodBundle.Pricing_Reference_Number__c;
    		}
			*/
    		
    	}
    }
		
		/*
	public override void afterInsert(sObject o){
		if (Trigger.isInsert){
    		cscfga__product_bundle__c prodBundle = (cscfga__product_bundle__c)o;
    		system.debug(mapOpportunity.get(prodBundle.cscfga__opportunity__c).Record_Type_Name__c);
    		if (mapOpportunity.containsKey(prodBundle.cscfga__opportunity__c) && icapBundleNumber != null && mapOpportunity.get(prodBundle.cscfga__opportunity__c).Record_Type_Name__c == 'IoT Connect Advanced'){
    			system.debug('ICap!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    			prodBundle.Name = 'ICAP-' + icapBundleNumber;
    			icapBundleNumberChanged = true;
    			icapBundleNumber++;
    		}
    		else{
    			system.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    			prodBundle.Name = prodBundle.Pricing_Reference_Number__c;
    		}
    		
    	}
	}
	*/


	public override void finish() {

		if (!oldUnitSchedules.isEmpty()) {
			delete oldUnitSchedules;
		}
		/*if (!newUnitSchedules.isEmpty()) {
			insert newUnitSchedules;
		}*/
        system.debug('opportunitiesWithSynchronizationState'+opportunitiesWithSynchronizationState);
		if(!opportunitiesWithSynchronizationState.isEmpty()) {
		    system.debug('opportunitiesWithSynchronizationState'+opportunitiesWithSynchronizationState);
			SystemUpdateFlags.NoOpportunityTrigger = true;
			update opportunitiesWithSynchronizationState;
			SystemUpdateFlags.NoOpportunityTrigger = false;
		}

		if (!syncedBundleOpportunities.isEmpty()) {
			update syncedBundleOpportunities;

			Map<String, List<Opportunity>> recordTypeOpps = splitByRecordTypes(addRecordTypes(syncedBundleOpportunities));
			if (recordTypeOpps.get(Constants.OPPORTUNITY_TYPE_COMMERCIAL_LOCAL) != null) {
				for (Opportunity opp : recordTypeOpps.get(Constants.OPPORTUNITY_TYPE_COMMERCIAL_LOCAL)) {
					// initiate SPIN Project creation/update
					SPINProjectHandler.upsertProjectToSpin(opp);
				}
			}
		}

		if (icapBundleNumberChanged){
			icapBundleNumeberSetting[0].Order_Number__c = icapBundleNumber;
			update icapBundleNumeberSetting;
		}
	}

	private List<Opportunity> addRecordTypes(List<Opportunity> opportunities) {

		Map<Id, Opportunity> oppsWithRecordTypes = new Map<Id, Opportunity>([Select Id, RecordTypeId, RecordType.Name from Opportunity where Id in :opportunities]);

		for (Opportunity opp : opportunities) {
			opp.RecordType = oppsWithRecordTypes.get(opp.Id).RecordType;
		}
		return opportunities;
	}

	private Map<String, List<Opportunity>> splitByRecordTypes(List<Opportunity> opportunities) {
		Map<String, List<Opportunity>> splitOpps = new Map<String, List<Opportunity>>();

		for (Opportunity opp : opportunities) {
			if (splitOpps.get(opp.RecordType.Name) == null) {
				splitOpps.put(opp.RecordType.Name, new List<Opportunity>());
			}
			splitOpps.get(opp.RecordType.Name).add(opp);
		}
		return splitOpps;
	}

	private List<cscfga__Product_Bundle__c> getSynchronizedBundles(Map<Id, cscfga__Product_Bundle__c> oldBundleMap, List<cscfga__Product_Bundle__c> updatedBundles) {

		Set<Id> synchronizedBundleIds = new Set<Id>();

		for (cscfga__Product_Bundle__c afterUpdate : updatedBundles) {
			cscfga__Product_Bundle__c beforeUpdate = oldBundleMap.get(afterUpdate.Id);
			if (beforeUpdate.cscfga__Synchronised_with_Opportunity__c == false && afterUpdate.cscfga__Synchronised_with_Opportunity__c == true) {
				synchronizedBundleIds.add(afterUpdate.Id);
			}
		}
		system.debug(oldBundleMap);
		system.debug(synchronizedBundleIds);
		System.debug(updatedBundles);
		return [
			SELECT
				cscfga__Opportunity__r.Contract_Term__c, cscfga__Opportunity__r.Id, cscfga__Opportunity__c
			FROM
				cscfga__Product_Bundle__c
			WHERE
				Id IN :synchronizedBundleIds
		];
	}
}