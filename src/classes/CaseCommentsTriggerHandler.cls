/**
 * This class is used as a trigger handler delegate which handles all the triggering events related to
 * Case Comment logic
 * */
public with sharing class CaseCommentsTriggerHandler extends TriggerHandler.DelegateBase {


    public override void prepareBefore() {
        
    }
    
    
    public override void prepareAfter() {
        
    }
    
    
    public override void beforeInsert(sObject o) {
        
    }
    
    
    public override void beforeUpdate(sObject old, sObject o) {
        
    }
    
    
    public override void beforeDelete(sObject o) {
        
    }
    
    
    public override void afterInsert(sObject o) {
        CaseComment cComment = (CaseComment)o;
        if(cComment != null && cComment.parentId != null && cComment.CommentBody != null && !cComment.CommentBody.contains('UPDATED_FROM_UDO')){
            UDOUtils.UpdateTicketCommentInUDo(cComment.Id);
        }
    }
    
    
    public override void afterUpdate(sObject old, sObject o) {
        
    }
    
    
    public override void afterDelete(sObject o) {
        
    }
    
    
    public override void afterUndelete(sObject o) {
        
    }
    
    
    public override void finish() {
        
    }
}