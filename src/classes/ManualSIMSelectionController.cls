/*Class Used to display all the SIMs in stock*/
public with sharing class ManualSIMSelectionController {
    
    /*Wrapper Class to hold the data sepecific to the page size*/
    public class StockWrapper
    {
        public Stock__c simInStock{get;set;}
        public boolean selected;
        
        /*Contructor of the stock wrapper which stores the stock instance*/
        public StockWrapper(Stock__c simInStock)
        { 
            this.simInStock = simInStock;
            this.selected = false;
        }
    }
    
    // the soql without the order and limit
    private String soql {get;set;}

    public List<StockWrapper> simCardWrapperList{get;set;}
    public List<StockWrapper> selectedSIMsWrapperList{get;set;}
    public static List<Stock__c> simCardList = new List<Stock__c>();
    public static List<Stock__c> selectedSimCardList = new List<Stock__c>();


    public Integer noOfRecordsAvailable{get; set;}
    public Integer noOfRecordsSelected{get; set;}
    public Integer pageSize{get;set;}{pageSize = 20;}
    public Integer currentPage{get;set;}
    public Integer currentPageSelected{get;set;}
    private Integer noOfPagesAvailable;
    private Integer noOfPagesSelected;
    
    
    public boolean showFirstPageButton{get;set;}
    public boolean showPreviousPageButton{get;set;}
    public boolean showNextPageButton{get;set;}
    public boolean showLastPageButton{get;set;}
    
    
    public boolean showFirstPageButtonSelected{get;set;}
    public boolean showPreviousPageButtonSelected{get;set;}
    public boolean showNextPageButtonSelected{get;set;}
    public boolean showLastPageButtonSelected{get;set;}

    public Third_Upload_File__c parent {get;set;}

    /*Constructor which uses standardcontroller to get the current TUF record*/
    public ManualSIMSelectionController(ApexPages.StandardController stdController)
    {
        simCardWrapperList = new List<StockWrapper>();
        selectedSIMsWrapperList = new List<StockWrapper>();
        noOfRecordsAvailable = noOfPagesAvailable = 0;
        noOfRecordsSelected = noOfPagesSelected = 0;
        currentPage = currentPageSelected = 1;
        showFirstPageButton = showPreviousPageButton = showNextPageButton = showLastPageButton = true;
        showFirstPageButtonSelected = showPreviousPageButtonSelected = showNextPageButtonSelected = showLastPageButtonSelected = true;
        this.parent = (Third_Upload_File__c)stdController.getRecord();
        this.parent = [SELECT SIM_Article_Type__c from Third_Upload_File__c where Id = :this.parent.Id];
        getSelected();
        queryStock();
    }

    /*Method fetches all the SIMs which are in stock*/
    private void queryStock()
    {
        soql = 'Select Name, ICCID__c, Article_Info_Set__r.Name from Stock__c where Article_Info_Set__c = \''+ parent.SIM_Article_Type__c +'\' and Status__c = \'On stock\'';

        if (!iccidPart.equals('') && iccidPart.length() > 3)
        {
            soql += ' and ICCID__c LIKE \'%'+String.escapeSingleQuotes(iccidPart)+'%\'';
        }
        try
        {
            system.debug(soql + ' order by ' + sortField + ' ' + sortDir);
            for(Stock__c sim : Database.query(soql + ' order by ' + sortField + ' ' + sortDir))
            {
                simCardList.add(sim);
            }
            
            if(simCardList != null && simCardList.size()>0)
            {
                noOfRecordsAvailable =  simCardList.size();
                Decimal noOfRecordsAvailableDecimal = Decimal.valueOf(noOfRecordsAvailable);            
                noOfPagesAvailable = noOfRecordsAvailableDecimal.divide(pageSize, 0, System.RoundingMode.UP).intValue();
                queryStockPerPage();
            }
            else
            {
                noOfRecordsAvailable = 0;
                noOfPagesAvailable = 0;
                resetStockWrapper();
            }
        }
        catch (Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage() ));
        }
    }
    
    /*Method fetches the SIMs in stock and assign them to wrapper as per the pagesize specified*/
    @testvisible private void queryStockPerPage()
    {
        System.debug('Total SIM Card List size in queryStockPerPage Method : '+simCardList.size());
        resetStockWrapper();
        renderingButtons();
        /*noOfRecordsAvailable =  simCardList.size();
        Decimal noOfRecordsAvailableDecimal = Decimal.valueOf(noOfRecordsAvailable);            
        noOfPagesAvailable = noOfRecordsAvailableDecimal.divide(pageSize, 0, System.RoundingMode.UP).intValue();   */
        Integer firstRecordIndex = getFirstRecordIndex();
        Integer lastRecordIndex = firstRecordIndex + pageSize; 
        System.debug('QueryStockPerPage=====> '+' firstRecordIndex '+firstRecordIndex+' lastRecordIndex '+lastRecordIndex+' totalNoOfRecords '+noOfRecordsAvailable + ' noOfPagesAvailable '+noOfPagesAvailable);   
        StockWrapper stock;
        for(Integer i = firstRecordIndex ; i < lastRecordIndex && i <= noOfRecordsAvailable-1 ; i++){
            stock = new StockWrapper(simCardList[i]);
            simCardWrapperList.add(stock);
        }
    }
    
    /*Method clears the Stock wrapper data once it navigates to other page in pagination*/
    private void resetStockWrapper()
    {
        if(simCardWrapperList == null){
            simCardWrapperList = new List<StockWrapper>();
        } else {
            simCardWrapperList.clear();
        }
    }
    
    /*Method sets the current page on click of First Button*/
    public void setUpFirstCurrentPage()
    {
        currentPage = 1;
        queryStock();
        System.debug('No of Queries : '+Limits.getQueries());
    }
    
    /*Method sets the current page on click of Next Button*/
    public void setUpNextCurrentPage()
    {
        currentPage = currentPage + 1;
        queryStock();
        System.debug('No of Queries : '+Limits.getQueries());
    }
    
    /*Method sets the current page on click of Previous Button*/
    public void setUpPreviousCurrentPage()
    {
        currentPage = currentPage - 1;
        queryStock();
        System.debug('No of Queries : '+Limits.getQueries());
    }
    
    /*Method sets the current page on click of Last Button*/
    public void setUpLastCurrentPage()
    {
        currentPage = noOfPagesAvailable;
        queryStock();
        System.debug('No of Queries : '+Limits.getQueries());
    }
    
    /*Method gets the first record index to be displayed on UI*/
    private Integer getFirstRecordIndex(){
        if(currentPage == 1){
            return 0;
        }
        else{
            return (pageSize * (currentPage-1));
        }
    } 
    
    /*Method which renders the buttons based on current page value*/
    public void renderingButtons()
    {
        showFirstPageButton = currentPage == 1 ? true : false;
        showPreviousPageButton = currentPage == 1 ? true : false;
        showNextPageButton = currentPage == noOfPagesAvailable ? true : false;
        showLastPageButton = currentPage == noOfPagesAvailable ? true : false;
    }
    
    
    
    public string iccidPart{get;set;}
    {iccidPart = '';}

    /* runs the search with parameters passed via Javascript*/
    public PageReference runSearch()
    {
        queryStock();

        return null;
    }

    /*Method fetches the SIMs which are already selected for the TUF*/
    public PageReference getSelected()
    {
        for(Stock__c sim : [Select Name, ICCID__c, Article_Info_Set__r.Name from Stock__c where Third_Upload_File__c = :this.parent.Id])
        {
            selectedSimCardList.add(sim);
        }

        if(selectedSimCardList != null && selectedSimCardList.size()>0)
        {
            noOfRecordsSelected =  selectedSimCardList.size();
            Decimal noOfRecordsSelectedDecimal = Decimal.valueOf(noOfRecordsSelected);            
            noOfPagesSelected = noOfRecordsSelectedDecimal.divide(pageSize, 0, System.RoundingMode.UP).intValue();
            querySelectedStockPerPage();
        }
        else
        {
            noOfRecordsSelected = 0;
            noOfPagesSelected = 0;
            resetSelectedStockWrapper();
        }

        return null;
    }

    /*Method fetches the SIMs already selected and assign them to wrapper as per the pagesize specified*/
    @testvisible private void querySelectedStockPerPage()
    {
        System.debug('Total SIM Card List size in querySelectedStockPerPage Method : '+simCardList.size());
        resetSelectedStockWrapper();
        renderingSelectedButtons();
        Integer firstRecordIndexSelected = getFirstRecordIndexSelected();
        Integer lastRecordIndexSelected = firstRecordIndexSelected + pageSize; 
        System.debug('QuerySelectedStockPage=====> '+' firstRecordIndexSelected '+firstRecordIndexSelected+' lastRecordIndexSelected '+lastRecordIndexSelected+' totalNoOfRecordsSelected '+noOfRecordsSelected + ' noOfPagesSelected '+noOfPagesSelected);   
        StockWrapper stockSelected;
        for(Integer i = firstRecordIndexSelected ; i < lastRecordIndexSelected && i <= noOfRecordsSelected-1 ; i++){
            stockSelected = new StockWrapper(selectedSimCardList[i]);
            selectedSIMsWrapperList.add(stockSelected);
        }
    }
    
    /*Method clears the selected stock wrapper data once it navigates to other page in pagination*/
    @testvisible private void resetSelectedStockWrapper()
    {
        if(selectedSIMsWrapperList == null){
            selectedSIMsWrapperList = new List<StockWrapper>();
        } else {
            selectedSIMsWrapperList.clear();
        }
    }
    
    /*Method which renders the buttons based on current page value in case of selected SIMs*/
    public void renderingSelectedButtons()
    {
        showFirstPageButtonSelected = currentPageSelected == 1 ? true : false;
        showPreviousPageButtonSelected = currentPageSelected == 1 ? true : false;
        showNextPageButtonSelected = currentPageSelected == noOfPagesSelected ? true : false;
        showLastPageButtonSelected = currentPageSelected == noOfPagesSelected ? true : false;
    }
    
    /*Method gets the first record index to be displayed on UI in case of selected SIMs*/
    @testvisible private Integer getFirstRecordIndexSelected(){
        if(currentPageSelected == 1){
            return 0;
        }
        else{
            return (pageSize * (currentPageSelected-1));
        }
    } 
    
    /*Method sets the current page on click of First Button in case of selected SIMs*/
    public void setUpFirstCurrentPageSelected()
    {
        currentPageSelected = 1;
        getSelected();
        System.debug('No of Queries : '+Limits.getQueries());
    }
    
    /*Method sets the current page on click of Next Button in case of selected SIMs*/
    public void setUpNextCurrentPageSelected()
    {
        currentPageSelected = currentPageSelected + 1;
        getSelected();
        System.debug('No of Queries : '+Limits.getQueries());
    }
    
    /*Method sets the current page on click of Previous Button in case of selected SIMs*/
    public void setUpPreviousCurrentPageSelected()
    {
        currentPageSelected = currentPageSelected - 1;
        getSelected();
        System.debug('No of Queries : '+Limits.getQueries());
    }
    
    /*Method sets the current page on click of Last Button in case of selected SIMs*/
    public void setUpLastCurrentPageSelected()
    {
        currentPageSelected = noOfPagesSelected;
        getSelected();
        System.debug('No of Queries : '+Limits.getQueries());
    }
    
    
    /*Method Used to update the SIM card on click of button either add or remove*/
    public PageReference updateSIMCard()
    {
        try
        {
            Stock__c stockToUpdate = [Select Name, ICCID__c, Article_Info_Set__r.Name, Third_Upload_File__c from Stock__c where Id = :stockToUpdateId];
            if(stockToUpdateChecked)
            {
                stockToUpdate.Third_Upload_File__c = parent.Id;
                stockToUpdate.Status__c = 'Reserved';
            }
            else
            {
                stockToUpdate.Third_Upload_File__c = null;
                stockToUpdate.Status__c = 'On stock';
            }
            update stockToUpdate;
            if(currentPage != 1 && currentPage == noOfPagesAvailable && (simCardWrapperList != null && simCardWrapperList.size() == 1)){
                currentPage = currentPage - 1;
            }
            if(currentPageSelected != 1 && currentPageSelected == noOfPagesSelected && (selectedSIMsWrapperList != null && selectedSIMsWrapperList.size() == 1)){
                currentPageSelected = currentPageSelected - 1;
            }
            getSelected();
            queryStock();
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getMessage()));
        }
        return null;
    }

    public string stockToUpdateId{get;set;}
    { stockToUpdateId = ''; }
    public boolean stockToUpdateChecked{get;set;}

    /*Redirct to coessponding page*/
    public PageReference openArvatoUploadPage()
    {
        //PageReference arvatoPage = Page.ArvatoFileUploadPage;
        PageReference arvatoPage = new PageReference('/apex/ArvatoFileUploadPage?id='+parent.id+'&excludeQuotes=true');
        return arvatoPage;
    }

    /* toggles the sorting of query from asc<-->desc */
    public void toggleSort()
    {
        // simply toggle the direction
        sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
        // run the query again
        queryStock();
    }

    /* the current sort direction. defaults to asc */
    public String sortDir
    {
        get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
        set;
    }

    /* the current field to sort by. defaults to last name */
    public String sortField
    {
        get  { if (sortField == null) {sortField = 'ICCID__c'; } return sortField;  }
        set;
    }
}