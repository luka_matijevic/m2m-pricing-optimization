@isTest
private class DailyBillItemTriggerTest {

    static testMethod void testInsert() {
	        
	        Invoice_Number__c i = new Invoice_Number__c();
	        i.Name='Currrent';
	        i.Value__c=100;
	        
	        insert i;
	        
	        Daily_Bill__c cc = new Daily_Bill__c();
	        
	        insert cc;
	        
	        Daily_Bill_Item__c c = new Daily_Bill_Item__c();
            c.Art__c=32;
            c.Steuersatz__c=19;
            c.Daily_Bill__c = cc.id;
            c.menge__c=1;
            insert c;
            
            //system.assertEquals(c.Rechnungsnummer__c,'100','Invoice number should not be null');
            
            c.Einzel_Nettobetrag__c = 100;
            
            update c;
            
            //system.assert(c.Einzel_Taxbetrag__c!=0,'Tax should be different than zero');

	}

}