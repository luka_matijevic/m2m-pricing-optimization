@isTest
public class Test_Util {
    
    
    public static list<EU_Countries__c> createCountries(){
        list<EU_Countries__c> obj = new list<EU_Countries__c>();
        obj.add(new EU_Countries__c(name = 'Austria', Language__c = 'ENG', Code__c = 'AT'));
        obj.add(new EU_Countries__c(name = 'Belgien', Language__c = 'ENG', Code__c = 'BE'));
        obj.add(new EU_Countries__c(name = 'Belgium', Language__c = 'ENG', Code__c = 'BE'));
        obj.add(new EU_Countries__c(name = 'Bulgaria', Language__c = 'ENG', Code__c = 'BG'));
        obj.add(new EU_Countries__c(name = 'Bulgarien', Language__c = 'ENG', Code__c = 'BG'));
        obj.add(new EU_Countries__c(name = 'Croatia', Language__c = 'ENG', Code__c = 'HR'));
        return obj;
    }
    
    public static Tariff__c createTarrif(string name, string prodFamily,string contractTerm,string gloablOrLocal,string status, decimal oneOffPrice, decimal recPrice){
        Tariff__c tf = new Tariff__c(Name = name,
                                     Product_Family__c = prodFamily,
                                     Contract_Term__c = contractTerm,
                                     Global_Local__c = gloablOrLocal,
                                     Status__c = status,
                                     One_Off_Price__c = oneOffPrice,
                                     Recurring_Price__c = recPrice);
        return tf;
    }
    
    public static No_Trigger__c noTrigger(boolean flag){
        No_Trigger__c nt = new No_Trigger__c(Flag__c = flag);
        return nt;
    }
    
    public static account createAccount(string name){
        Account acc = new account(name=name, SM2M_Customer_Name__c = name);
        acc.Type = 'Business';
        return acc;
    }
    
    public static Contact createContact(id accountId){
        Contact con = new Contact(AccountId =accountId, lastName ='lastName', email = 'abcd@m2m.com',phone='123456789',firstName ='FirstName');
        return con;
    }
    
    public static cscfga__Product_Bundle__c createProdBundle(id oppID){
        cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(
			cscfga__Synchronised_with_Opportunity__c = false,
			cscfga__Opportunity__c = oppId
		);
		
		return productBundle;
    }
    
    public static Frame_Contract__c createFC(id bundleId){
        Frame_Contract__c obj = new Frame_Contract__c(Product_Bundle__c = bundleId);
        return obj;
    }
    
    public static OpportunityContactRole oppContactRole(id ContactId,boolean IsPrimary, id OpportunityId, string Role){
        OpportunityContactRole op = new OpportunityContactRole(ContactId = ContactId,
                                                               IsPrimary = IsPrimary,
                                                               OpportunityId = OpportunityId,
                                                               Role = Role);
        return op;
    }
    
    public static case createCase(string Category, string Subcategory,string Status, string AccountId, string Subject, string SLA_Time,string Type,string Origin,string Priority,string SuppliedEmail){
        Case cs = new case(Category__c= Category, Subcategory__c = Subcategory,Status=Status, AccountId=AccountId, Subject=Subject, SLA_Time__c=SLA_Time, Type=Type, Origin=Origin, Priority=Priority, SuppliedEmail=SuppliedEmail);
        return cs;
    }
    
    public static case createCaseForUdo(string Category, string Subcategory,string Status, string AccountId, string Subject, string SLA_Time,string Type,string Origin,string Priority,string SuppliedEmail, string UDo_Description){
        Case cs = new case(Category__c= Category, Subcategory__c = Subcategory,Status=Status, AccountId=AccountId, Subject=Subject, SLA_Time__c=SLA_Time, Type=Type, Origin=Origin, Priority=Priority, SuppliedEmail=SuppliedEmail, 
            UDo_Description__c = UDo_Description, UDo_Ticket_created__c = true, UDo_Severity__c = 'Slight', Customer_Service_Type__c = 'Order', Massive__c = 'NO', Order_Type__c = 'Service Ticket');
 
        return cs;
    }
    
    public static Opportunity createOpportynity(string name, string accountId, date closeDate, string StageName,string winReason,integer Contract_Term_Per_SIM, decimal Monthly_Revenue_Per_SIM){
        Opportunity opp = new opportunity(Name=Name,AccountId= AccountId,CloseDate = closeDate,StageName=StageName,Win_Reason__c = WinReason,Contract_Term_Per_SIM__c = Contract_Term_Per_SIM,Monthly_Revenue_Per_SIM__c = Monthly_Revenue_Per_SIM);
        return opp;
    }
    
    public static user createUser(id profileId, string userName, string alias,string lname){ 
        user u = new User(Alias = alias, Email='testuser@test.com', 
                          EmailEncodingKey='UTF-8', LastName=lname, LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = profileId, 
                          TimeZoneSidKey='America/Los_Angeles', UserName=userName);
        return u;
    }
    
    public static Order_Request__c createM2MOrderRequest(id bonicheck, id OppId, id Accountid){
        Order_Request__c oreq = new Order_Request__c();
        if(bonicheck != null)
            oreq.Boni_Check__c = bonicheck;
        if(OppId != null)
            oreq.Opportunity__c = OppId;
        if(Accountid != null)
            oreq.Account__c = Accountid;
        return oreq;
    }
    
    
    public static Rate_Card__c createRateCard(id tariffId, integer SequenceNo,string worldZone){
        Rate_Card__c rc = new Rate_Card__c();
        rc.name = 'Rate Card Name'+SequenceNo;
        rc.Tariff_Option__c = tariffId;
        rc.Worldzone__c = worldZone;
        rc.Base_Usage_Fee__c = 100;
        rc.Sequence_No__c = SequenceNo;
        return rc;
    }
    
    
    public static Service_Fee__c createServiceFee(id tariffID,integer SequenceNo){
        Service_Fee__c sf = new Service_Fee__c();
        sf.name = 'Service Fee'+SequenceNo;
        sf.Tariff_Option__c = tariffID;
        sf.Charged_Per__c = 'Volume';
        sf.One_Time_Base_Fee__c = 100;
        sf.Recurring_Base_Fee__c = 100;
        return sf;
    }
    
    public static Rate_Card_Country_Management__c createRateCardMngmt(id rateCard,string countryCode,decimal Ratio){
        Rate_Card_Country_Management__c obj = new Rate_Card_Country_Management__c();
        obj.Rate_Card__c = rateCard;
        obj.Country_code__c = countryCode;
        obj.Ratio__c = ratio;
        return obj;
    }
     
    public static Business_comment__c createBusinessComments(id bundleId,string Comment){
        Business_comment__c obj = new Business_comment__c();
        obj.Product_Bundle__c = bundleId;
        obj.Comment__c = Comment;
        return obj;
    }
    
    public static map<string,cscfga__Attribute_Definition__c> attrDefinations(string prodDefId){
        
        map<string,cscfga__Attribute_Definition__c> attrDefMap = new map<string,cscfga__Attribute_Definition__c>();
        
        list<cscfga__Attribute_Definition__c> attrDefLst = new list<cscfga__Attribute_Definition__c>();
        list<string> attrDefNames = new list<string>();
        attrDefNames.add('Account Id');
        attrDefNames.add('Activation Fee Lookup');
        attrDefNames.add('Activation Fee Name');
        attrDefNames.add('Activation Fee TCV');
        attrDefNames.add('Activation fee');
        attrDefNames.add('Activation Fee Total');
        attrDefNames.add('Additional Tariff Option');
        attrDefNames.add('Additional Tariff Option Name');
        attrDefNames.add('Article Info Set');
        attrDefNames.add('Article Info Set Name');
        attrDefNames.add('Bundle Id');
        attrDefNames.add('Cloned Tariff');
        attrDefNames.add('Config Id');
        attrDefNames.add('Connectivity Type');
        attrDefNames.add('Contract Term');
        attrDefNames.add('Connect Plus');
        attrDefNames.add('Deliver SIM Directly to Client');
        attrDefNames.add('Dynamic pool'); 
        attrDefNames.add('Discount');
        attrDefNames.add('Data per sim');
        attrDefNames.add('Discount value');
        attrDefNames.add('Global Local');
        attrDefNames.add('hidd_Pricing_Matrix');
        attrDefNames.add('hidd_SIM Nr Range');
        attrDefNames.add('LTE Option');
        attrDefNames.add('Minimum ARPU per SIM');
        attrDefNames.add('Montly Fee Platform Lookup');
        attrDefNames.add('Montly Fee Platform Name');
        attrDefNames.add('Montly Fee Platform TCV');
        attrDefNames.add('Montly Fee Platform Total');
        attrDefNames.add('Montly Fee Tariff Lookup');
        attrDefNames.add('Montly Fee Tariff Name');
        attrDefNames.add('Montly Fee Tariff Option Lookup');
        attrDefNames.add('Montly Fee Tariff Option Name');
        attrDefNames.add('Montly Fee Tariff Option TCV');
        attrDefNames.add('Montly Fee Tariff Option Total');
        attrDefNames.add('Montly Fee Tariff TCV');
        attrDefNames.add('Montly Fee Tariff Total');
        attrDefNames.add('Name');
        attrDefNames.add('One-Time Base Fee');
        attrDefNames.add('One-Time Discount');
        attrDefNames.add('One-Time Target Fee');
        attrDefNames.add('One-Time Target Fee per Customer');
        attrDefNames.add('One-Time Target Fee per Volume');
        attrDefNames.add('One-Time Target Fee Total');
        attrDefNames.add('Pack price');
        attrDefNames.add('Product Family');
        attrDefNames.add('Quantity');
        attrDefNames.add('Quantity Additional Options');
        attrDefNames.add('Quantity PerSIM Supplementary Options');
        attrDefNames.add('Read Only');
        attrDefNames.add('Real Time');
        attrDefNames.add('Real Time Activation Fee Total Label');
        attrDefNames.add('Real Time Activation Fee Total Value');
        attrDefNames.add('Real Time Montly Fee Platform Total Label');
        attrDefNames.add('Real Time Montly Fee Platform Total Value');
        attrDefNames.add('Real Time Montly Fee Tariff Total Label');
        attrDefNames.add('Real Time Montly Fee Tariff Total Value');
        attrDefNames.add('Real Time Non Recurring Totals');
        attrDefNames.add('Real Time One-Time Target Fee Total Label');
        attrDefNames.add('Real Time One-Time Target Fee Total Value');
        attrDefNames.add('Real Time Recurring Target Fee Total Label');
        attrDefNames.add('Real Time Recurring Target Fee Total Value');
        attrDefNames.add('Real Time SIM One-Time Target Fee Total Label');
        attrDefNames.add('Real Time SIM One-Time Target Fee Total Value');
        attrDefNames.add('Real Time Tariff One-Time Target Fee Total Label');
        attrDefNames.add('Real Time Tariff One-Time Target Fee Total Value');
        attrDefNames.add('Real Time Tariff Recurring Target Fee Total Label');
        attrDefNames.add('Real Time Tariff Recurring Target Fee Total Value');
        attrDefNames.add('Real Time Tariff Recurring Totals');
        attrDefNames.add('Real Time TCV Totals');
        attrDefNames.add('Real Time Total Expected Data Over-Usage Label');
        attrDefNames.add('Real Time Total Expected Data Over-Usage Value');
        attrDefNames.add('Real Time Total Expected SMS Over-Usage Label');
        attrDefNames.add('Real Time Total Expected SMS Over-Usage Value');
        attrDefNames.add('Real Time Total Expected Text Over-Usage Label');
        attrDefNames.add('Real Time Total Expected Text Over-Usage Value');
        attrDefNames.add('Real Time Total Expected Voice Over-Usage Label');
        attrDefNames.add('Real Time Total Expected Voice Over-Usage Value');
        attrDefNames.add('Real Time Usage Target Fee Total Label');
        attrDefNames.add('Real Time Usage Target Fee Total Value');
        attrDefNames.add('Reason for Price Change');
        attrDefNames.add('Recurring Base Fee');
        attrDefNames.add('Recurring Discount');
        attrDefNames.add('Recurring Target Fee');
        attrDefNames.add('Recurring Target Fee per Customer');
        attrDefNames.add('Recurring Target Fee per Volume');
        attrDefNames.add('Recurring Target Fee Total');
        attrDefNames.add('Request Technical Change');
        attrDefNames.add('Requires Commercial Approval');
        attrDefNames.add('Requires Commercial Approval VF');
        attrDefNames.add('Requires Technical Approval');
        attrDefNames.add('Shadow Article Info Set');
        attrDefNames.add('Shadow Tariff');
        attrDefNames.add('SIM Nr Range');
        attrDefNames.add('SIM One-Time Allowed Discount');
        attrDefNames.add('SIM One-Time Base Fee');
        attrDefNames.add('SIM One-Time Discount');
        attrDefNames.add('SIM One-Time Target Fee');
        attrDefNames.add('SIM One-Time Target Fee Revised');
        attrDefNames.add('SIM One-Time Target Fee Total');
        attrDefNames.add('SIMType');
        attrDefNames.add('SM2M Platform');
        attrDefNames.add('SoQ One-Time Fee');
        attrDefNames.add('SoQ Recurring Fee');
        attrDefNames.add('SoQ SIM One-Time Fee');
        attrDefNames.add('SoQ Tariff One-Time Fee');
        attrDefNames.add('SoQ Tariff Recurring Fee');
        attrDefNames.add('SoQ Usage Fee');
        attrDefNames.add('Submit warning');
        attrDefNames.add('Supplementary Service');
        attrDefNames.add('Supplementary Service Name');
        attrDefNames.add('Tariff');
        attrDefNames.add('Tariffa');
        attrDefNames.add('Tariff One-Time Allowed Discount');
        attrDefNames.add('Tariff One-Time Base Fee');
        attrDefNames.add('Tariff One-Time Discount');
        attrDefNames.add('Tariff One-Time Target Fee');
        attrDefNames.add('Tariff One-Time Target Fee Revised');
        attrDefNames.add('Tariff One-Time Target Fee Total');
        attrDefNames.add('Tariff Option iFrame');
        attrDefNames.add('Tariff Option iFrame Store');
        attrDefNames.add('Tariff Option iFrameEnd');
        attrDefNames.add('Tariff Option iFrameStart');
        attrDefNames.add('Tariff Options');
        attrDefNames.add('Tariff Quantity Utility');
        attrDefNames.add('Tariff Recurring Allowed Discount');
        attrDefNames.add('Tariff Recurring Base Fee');
        attrDefNames.add('Tariff Recurring Discount');
        attrDefNames.add('Tariff Recurring Target Fee');
        attrDefNames.add('Tariff Recurring Target Fee Revised');
        attrDefNames.add('Tariff Recurring Target Fee Total');
        attrDefNames.add('Tariffaa');
        attrDefNames.add('Total All Activation Fees');
        attrDefNames.add('Total All Recurring Fees');
        attrDefNames.add('Total Contract Value');
        attrDefNames.add('Total Expected Data Over-Usage');
        attrDefNames.add('Total Expected Text Over-Usage');
        attrDefNames.add('Total Expected Voice Over-Usage');
        attrDefNames.add('Unique Key');
        attrDefNames.add('Usage Base Fee');
        attrDefNames.add('Usage Discount');
        attrDefNames.add('Usage Target Fee');
        attrDefNames.add('Usage Target Fee Total');
        attrDefNames.add('User Role');

        integer x= 1;
        for(string name : attrDefNames){
            cscfga__Attribute_Definition__c obj = new cscfga__Attribute_Definition__c();
            obj.name = name;
            obj.cscfga__Product_Definition__c = prodDefId;
            obj.cscfga__Row__c = x;
            x++;
            attrDefLst.add(obj);
        }
        insert attrDefLst;
        
        for(cscfga__Attribute_Definition__c tmpObj : attrDefLst){
            attrDefMap.put(tmpObj.name,tmpObj);
        }
        
        return attrDefMap;
    } 
    
    public static list<cscfga__Attribute_Definition__c> attrDefinations(list<id> prodDefinations){
        
        list<cscfga__Attribute_Definition__c> attrDefLst = new list<cscfga__Attribute_Definition__c>();
        list<string> attrDefNames = new list<string>();
       	attrDefNames.add('Name');
        attrDefNames.add('Cloned Tariff');
        attrDefNames.add('SIM One-Time Target Fee Revised');
        attrDefNames.add('Tariff One-Time Target Fee Revised');
        attrDefNames.add('Tariff Recurring Target Fee Revised');
        attrDefNames.add('Quantity');
        attrDefNames.add('One-Time Discount');
        attrDefNames.add('Contract Term');
        attrDefNames.add('Recurring Target Fee Total');
        attrDefNames.add('Recurring Discount');
        attrDefNames.add('Requires Technical Approval');
        attrDefNames.add('Article Info Set');
        attrDefNames.add('One-Time Base Fee');
        attrDefNames.add('One-Time Target Fee');
        attrDefNames.add('One-Time Target Fee Total');
        attrDefNames.add('Recurring Base Fee');
        attrDefNames.add('Recurring Target Fee');
        attrDefNames.add('User Role');
        attrDefNames.add('Read Only');
        attrDefNames.add('Product Family');
        attrDefNames.add('Global Local');
        attrDefNames.add('Usage Base Fee');
        attrDefNames.add('Usage Target Fee');
        attrDefNames.add('Usage Discount');
        attrDefNames.add('Request Technical Change');
        attrDefNames.add('Requires Commercial Approval');
        attrDefNames.add('SoQ One-Time Fee');
        attrDefNames.add('SoQ Recurring Fee');
        attrDefNames.add('SoQ Usage Fee');
        attrDefNames.add('Usage Target Fee Total');
        attrDefNames.add('Connectivity Type');
        attrDefNames.add('Article Info Set Name');
        attrDefNames.add('Shadow Article Info Set');
        attrDefNames.add('Submit warning');
        attrDefNames.add('Shadow Tariff');
        attrDefNames.add('Tariff One-Time Allowed Discount');
        attrDefNames.add('Tariff One-Time Discount');
        attrDefNames.add('Unique Key');
        attrDefNames.add('Config Id');
        attrDefNames.add('Tariff');
        attrDefNames.add('SIM One-Time Base Fee');
        attrDefNames.add('Tariff Recurring Base Fee');
        attrDefNames.add('SIM One-Time Target Fee');
        attrDefNames.add('Tariff Recurring Target Fee');
        attrDefNames.add('Deliver SIM Directly to Client');
        attrDefNames.add('Tariff Recurring Target Fee Total');
        attrDefNames.add('SIM One-Time Target Fee Total');
        attrDefNames.add('Tariff One-Time Base Fee');
        attrDefNames.add('Tariff One-Time Target Fee');
        attrDefNames.add('Tariff One-Time Target Fee Total');
        attrDefNames.add('SIM One-Time Discount');
        attrDefNames.add('Minimum ARPU per SIM');
        attrDefNames.add('Tariff Recurring Allowed Discount');
        attrDefNames.add('Total Expected Data Over-Usage');
        attrDefNames.add('SIM One-Time Allowed Discount');
        attrDefNames.add('Total Expected Voice Over-Usage');
        attrDefNames.add('Tariff Recurring Discount');
        attrDefNames.add('Total Expected Text Over-Usage');
        attrDefNames.add('Tariff Option iFrame Store');
        attrDefNames.add('Quantity Additional Options');
        attrDefNames.add('Quantity PerSIM Supplementary Options');
        attrDefNames.add('Bundle Id');
        attrDefNames.add('Tariff Quantity Utility');
        attrDefNames.add('Tariff Option iFrame');
        attrDefNames.add('Account Id');
        attrDefNames.add('Tariff Option iFrameEnd');
        attrDefNames.add('Tariff Option iFrameStart');
        attrDefNames.add('Requires Commercial Approval VF');
        attrDefNames.add('SoQ Tariff Recurring Fee');
        attrDefNames.add('SoQ Tariff One-Time Fee');
        attrDefNames.add('SoQ SIM One-Time Fee');
        attrDefNames.add('Supplementary Service');
        attrDefNames.add('One-Time Target Fee per Volume');
        attrDefNames.add('Recurring Target Fee per Volume');
        attrDefNames.add('Supplementary Service Name');
        attrDefNames.add('Additional Tariff Option');
        attrDefNames.add('Additional Tariff Option Name');
        attrDefNames.add('One-Time Target Fee per Customer');
        attrDefNames.add('Recurring Target Fee per Customer');
        attrDefNames.add('Reason for Price Change');
        attrDefNames.add('Tariffaa');
        attrDefNames.add('Tariff Options');
        attrDefNames.add('SIM Nr Range');
        attrDefNames.add('Activation Fee Lookup');
        attrDefNames.add('Montly Fee Tariff Lookup');
        attrDefNames.add('Montly Fee Platform Lookup');
        attrDefNames.add('Montly Fee Tariff Option Lookup');
        attrDefNames.add('hidd_SIM Nr Range');
        attrDefNames.add('hidd_Pricing_Matrix');
        attrDefNames.add('Montly Fee Tariff Name');
        attrDefNames.add('Activation Fee Name');
        attrDefNames.add('Montly Fee Platform Name');
        attrDefNames.add('Montly Fee Tariff Option Name');
        attrDefNames.add('Total All Activation Fees');
        attrDefNames.add('Total All Recurring Fees');
        attrDefNames.add('Total Contract Value');
        attrDefNames.add('Montly Fee Tariff Option Total');
        attrDefNames.add('Montly Fee Platform Total');
        attrDefNames.add('Montly Fee Tariff Total');
        attrDefNames.add('Activation Fee Total');
        attrDefNames.add('Activation Fee TCV');
        attrDefNames.add('Montly Fee Tariff TCV');
        attrDefNames.add('Montly Fee Platform TCV');
        attrDefNames.add('Montly Fee Tariff Option TCV');
        attrDefNames.add('SM2M Platform');
        attrDefNames.add('SIMType');
        attrDefNames.add('Real Time');
        attrDefNames.add('Dynamic pool');
        attrDefNames.add('Real Time One-Time Target Fee Total Value');
        attrDefNames.add('Real Time Recurring Target Fee Total Value');
        attrDefNames.add('Real Time Tariff Recurring Totals');
        attrDefNames.add('Real Time Tariff One-Time Target Fee Total Value');
        attrDefNames.add('Real Time Tariff One-Time Target Fee Total Label');
        attrDefNames.add('Real Time SIM One-Time Target Fee Total Label');
        attrDefNames.add('Real Time Non Recurring Totals');
        attrDefNames.add('Real Time SIM One-Time Target Fee Total Value');
        attrDefNames.add('Real Time Total Expected SMS Over-Usage Value');
        attrDefNames.add('Real Time Total Expected SMS Over-Usage Label');
        attrDefNames.add('Real Time Total Expected Voice Over-Usage Value');
        attrDefNames.add('Real Time TCV Totals');
        attrDefNames.add('Real Time Tariff Recurring Target Fee Total Label');
        attrDefNames.add('Real Time Tariff Recurring Target Fee Total Value');
        attrDefNames.add('Real Time Recurring Target Fee Total Label');
        attrDefNames.add('Real Time One-Time Target Fee Total Label');
        attrDefNames.add('Real Time Total Expected Voice Over-Usage Label');
        attrDefNames.add('Real Time Usage Target Fee Total Value');
        attrDefNames.add('Real Time Total Expected Data Over-Usage Label');
        attrDefNames.add('Real Time Montly Fee Tariff Total Label');
        attrDefNames.add('Real Time Total Expected Text Over-Usage Label');
        attrDefNames.add('Real Time Total Expected Text Over-Usage Value');
        attrDefNames.add('Real Time Montly Fee Platform Total Value');
        attrDefNames.add('Real Time Total Expected Data Over-Usage Value');
        attrDefNames.add('Real Time Usage Target Fee Total Label');
        attrDefNames.add('Real Time Activation Fee Total Value');
        attrDefNames.add('Real Time Activation Fee Total Label');
        attrDefNames.add('Real Time Montly Fee Tariff Total Value');
        attrDefNames.add('Real Time Montly Fee Platform Total Label');

        for(string name : attrDefNames){
            for(id i : prodDefinations){
                cscfga__Attribute_Definition__c obj = new cscfga__Attribute_Definition__c();
                obj.name = name;
                obj.cscfga__Product_Definition__c = i;
                attrDefLst.add(obj);
            }
        }
        return attrDefLst; 
    }
    
    public static cscfga__Product_Definition__c createProdDefination(string Name, string description){
        cscfga__Product_Definition__c obj = new cscfga__Product_Definition__c();
        obj.name = name;
        obj.cscfga__Description__c =  description;
        return obj;
    }
    
    public static cscfga__Attribute_Definition__c attrDef(string name,id prodDefId){
        cscfga__Attribute_Definition__c obj = new cscfga__Attribute_Definition__c();
        obj.name = name;
        obj.cscfga__Product_Definition__c = prodDefId;
        return obj;
    }
    
    
    public static cscfga__Attribute__c createAttrDef(string name,id configId, string val,id attrDefId){
        cscfga__Attribute__c obj = new cscfga__Attribute__c();
        obj.name = name;
        obj.cscfga__Value__c = val;
        obj.cscfga__Product_Configuration__c = configId;
        obj.cscfga__Attribute_Definition__c = attrDefId;
        return obj;
    }
    
    public static OpportunityContactRole createOppConRole(id oppId, id ConId, string role){
        OpportunityContactRole obj = new OpportunityContactRole();
        obj.ContactId = conId;
        obj.OpportunityId = oppid;
        obj.Role = role;
        return obj;
    }
    
    public static Stock__c createStock(string name, Id accId){
        Stock__c obj = new Stock__c();
        obj.Name = name;
        if(accId != null){
            obj.Account__c = accId;
        }
        obj.ICCID__c = name;
        return obj;
    }

    public static Attachment createAttachment(string Name, Blob Body, string  Description, Id ParentId) {
        Attachment att = new Attachment(Name = Name, Body = Body, Description = Description, ParentId = ParentId);
        return att;
    }
    
    public static CaseComment createCaseComment(string CommentBody, Id ParentId){
        CaseComment cc = new CaseComment (CommentBody = CommentBody, ParentId = ParentId);
        return cc;
    }
}