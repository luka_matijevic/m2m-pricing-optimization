@isTest
public class AccountTicketsReportScheduleTest {
	static testMethod void testCreateAccountTicketsReport() {
        Account acc = Test_Util.createAccount('Account Test');
        acc.Global__c = true;
        //Address addr = new Address(BillingAddress = 'Test', BillingCity = 'Zagreb', BillingCountry = 'Croatia', BillingCountryCode = 'DE', BillingPostalCode = '10000', BillingState = 'Zagreb', BillingStreet = 'Empty 46');
        acc.Billing_Cycle__c = '2';
        acc.VAT__c = 19;
        acc.BillingCity = 'Troisdorf';
        acc.BillingCountry = 'Germany';
        acc.BillingPostalCode = '53844';
        acc.BillingStreet = 'Junkersring 57';
        insert acc;
        Billing_Account__c bac = new Billing_Account__c(Payer__c = acc.Id, Name = 'Test Billing Acc', SM2M_Type__c = 'Global', VAT__c = '19', Activation_token__c = 'NotNull');
        insert bac;
        Case c = Test_Util.createCaseForUdo('Service', 'Activation','Open', acc.id, 'Subject', '2','Standard','Email','High','abc@abc.com', 'udo description, testing characther length, twenty');
        insert c;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UDOUtilsHTTPFakeCalloutsImpl());
        System.enqueueJob(new AccountTicketsReportSchedule());
        //System.assertEquals('Customer bills are getting created.', res);
        Test.stopTest();
    }
}