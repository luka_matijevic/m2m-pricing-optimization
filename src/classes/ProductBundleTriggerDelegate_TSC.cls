/**
* Trigger Delegate class fireing after product bundle commercial approval status has been set. Checks for any present
* Connectivity configurations within the baundle and sets their associated tariff status to either Approved or Rejected
* according to the result of the approval process
*/
public without sharing class ProductBundleTriggerDelegate_TSC extends TriggerHandler.DelegateBase  {

    private List<Tariff__c> tarrifsToUpdate = new List<Tariff__c>();


    public override void prepareBefore() {
        // do any preparation here – bulk loading of data etc
    }

    public override void prepareAfter() {
        // do any preparation here – bulk loading of data etc
        if (Trigger.isUpdate) {

        	List<cscfga__Product_Bundle__c> commercialAprrovalBundles = findCommercialApprovalChanged((Map<Id, cscfga__Product_Bundle__c>)Trigger.oldMap, (List<cscfga__Product_Bundle__c>)Trigger.new);

			if (!commercialAprrovalBundles.isEmpty()) {
							// alternatively, pick up "OR root_configuration..."
				Map<Id, cscfga__Product_Configuration__c> productConfigurationMap = new Map<Id, cscfga__Product_Configuration__c>([
						SELECT
						Original_Product_Name__c,
						cscfga__Product_Bundle__c
						FROM
						cscfga__Product_Configuration__c
						WHERE
						cscfga__Product_Bundle__c IN :commercialAprrovalBundles
						AND
						Original_Product_Name__c = :Constants.PRODUCT_TYPE_CONNECTIVITY
					]);
	
				List<cscfga__Attribute__c> tariffPriceAttributes = [
						SELECT
						cscfga__Product_Configuration__c, cscfga__Value__c
						FROM
						cscfga__Attribute__c
						WHERE
						Name = 'Tariff'
						AND
						cscfga__Product_Configuration__c IN :productConfigurationMap.values()
				];
	
	
				for (cscfga__Attribute__c tariffAttribute : tariffPriceAttributes) {
					tarrifsToUpdate.add(new Tariff__c(Id = tariffAttribute.cscfga__Value__c,
						Status__c =
							((cscfga__Product_Bundle__c)
							Trigger.newMap.get
								(
									(
										productConfigurationMap.get
										(
											tariffAttribute.cscfga__Product_Configuration__c
										)
										.cscfga__Product_Bundle__c
									)
								)
							)
							.Commercial_Approval_Status__c)
					);
				}
			}
	        

        }
    }

    public override void beforeInsert(sObject o) {
        // Apply before insert logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void beforeUpdate(sObject old, sObject o) {
        // Apply before update logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void beforeDelete(sObject o) {
        // Apply before delete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterInsert(sObject o) {
        // Apply after insert logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterUpdate(sObject old, sObject o) {
        // Apply after insert logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterDelete(sObject o) {
        // Apply after delete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterUndelete(sObject o) {
        // Apply after undelete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void finish() {

    	if (!tarrifsToUpdate.isEmpty()) {
        	update tarrifsToUpdate;
        }
    }

    /**
     * This method should scan the System trigger variables for bundles that were not previously commercially approved and were updated to
     * approved or rejected state.
     * @param oldBundleMap Map of Id->cscfga__Product_Bundle__c product bundles
     * @param updatedBundles List of cscfga__Product_Bundle__c product bundles
     */
    private List<cscfga__Product_Bundle__c> findCommercialApprovalChanged(Map<Id, cscfga__Product_Bundle__c> oldBundleMap, List<cscfga__Product_Bundle__c> updatedBundles) {

    	List<cscfga__Product_Bundle__c> approvedOrRejectedBundles = new List<cscfga__Product_Bundle__c>();

    	for (cscfga__Product_Bundle__c afterUpdate : updatedBundles) {
    		cscfga__Product_Bundle__c beforeUpdate = oldBundleMap.get(afterUpdate.Id);
    		if ((beforeUpdate.Commercial_Approval_Status__c == 'Required') && (beforeUpdate.Commercial_Approval_Status__c != afterUpdate.Commercial_Approval_Status__c)) {
    			approvedOrRejectedBundles.add(afterUpdate);
    		}
    	}
    	return approvedOrRejectedBundles;
    }
}