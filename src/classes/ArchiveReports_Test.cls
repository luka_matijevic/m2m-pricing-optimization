@IsTest
public class ArchiveReports_Test {
    static testMethod void archiveReportTest(){
        
        
         //Test Data creation
        Account acc = new Account();
            acc.Name = 'AP Test 121';
            acc.Company_Email__c = 'company222@email.com';
            acc.Unique_Business_Number__c = 'UBN 12333';
            acc.BillingCountry = 'Ireland';
            acc.BillingStreet = 'Warwick street 115';
            acc.BillingCity = 'Cork';
            acc.BillingPostalCode = '57000';
            acc.Jurisdiction__c = 'Jurisdiction';
            acc.VO_Number__c = '4678398';
            acc.VAT__c = 12.50;
            acc.Phone = '1234567890';
            acc.Type = 'Customer';
            acc.Sales_Employee__c = 'John Voight';
            acc.Frame_Contract__c = false;
            acc.Website = 'www';
        	acc.Global__c = true;
            
        insert acc;
        Billing_Account__c billingAccount = new Billing_Account__c();
        billingAccount.Account_Number__c = 1234;
        billingAccount.Name = 'Unit Test Billing Account';
        billingAccount.Billing_Language__c = 'English';
        billingAccount.Bank_Identifier__c = 'Bank Identifier';
        billingAccount.BIC__c = 8764321;
        billingAccount.Bank_Name__c = 'Bank Name';
        billingAccount.Bank_Address__c = 'Bank Address';
        billingAccount.Payment_Term__c = '30 Days';
        billingAccount.Payment_Method__c = 'Bank Transfer';
        billingAccount.Trial__c = true;
        billingAccount.Billable__c = false;
        billingAccount.Payer__c = acc.Id;
        billingAccount.VAT__c='0';
        insert billingAccount;
        Daily_Bill__c db = new Daily_Bill__c();

       	date d = system.today().toStartOfMonth().addMonths(-6);
        system.debug('dt: ' + d);
		db.Date__c=d;
        insert db;
        Customer_Monthly_Bill__c cmb = new Customer_Monthly_Bill__c();
        cmb.Archived__c = false;
        cmb.PDF_Bill_Link__c = 'http://something/filename.pdf';
        cmb.Daily_Bill__c = db.id;

        cmb.Billing_Account__c = billingAccount.id;
        insert cmb;
        Datetime dt = System.now().addSeconds(1);
        String chron_exp = dt.format('ss mm HH dd MM ? yyyy');
        Test.startTest();
      	//ArchiveReports.scheduleTick();
        Test.stopTest();
        
    }
}