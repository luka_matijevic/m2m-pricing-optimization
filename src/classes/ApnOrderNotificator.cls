public class ApnOrderNotificator {
	EmailTemplate emailTemplate;
	List<String> emailRecipient;
	Messaging.SingleEmailMessage email;

	public ApnOrderNotificator() {
		emailTemplate = [SELECT Id, Name, Subject, HtmlValue FROM EmailTemplate where DeveloperName = 'APN_Order_Notification'];
		
		emailRecipient = new List<String>();
		List<APN_Order_Mail_Addresses__c> emailAdressList = APN_Order_Mail_Addresses__c.getAll().values();
		for (APN_Order_Mail_Addresses__c apnEmailAddress : emailAdressList){
			if (apnEmailAddress.Email_Address__c != null){
				emailRecipient.add(apnEmailAddress.Email_Address__c);
			}
		}
	}

	public void sendNotification(Order_Request__c orderRequest){
		if (emailTemplate != null){
			try{
				email = new Messaging.SingleEmailMessage();
				email.setToAddresses(emailRecipient);
				
				String mailBody = emailTemplate.HtmlValue;
				String linkOrderRequest = '<a href="'+URL.getSalesforceBaseUrl().toExternalForm() + '/' + String.valueOf(orderRequest.Id) + '">' + String.valueOf(orderRequest.Name) + '</a>';
				mailBody = mailBody.replace('{Created_Date}', String.valueOf(orderRequest.CreatedDate));
				mailBody = mailBody.replace('{Order_request}', linkOrderRequest);
				email.setHtmlBody(mailBody);
				email.setSubject(emailTemplate.Subject);
				
    			Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    		}catch(Exception ex){
    			System.debug(ex);
    		}
		}
	}
}