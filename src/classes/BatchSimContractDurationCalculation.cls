global class BatchSimContractDurationCalculation implements Database.Batchable<sObject> {
	
	private static final String query = 'Select Id, ICCID__c, Third_Upload_File__r.Order_Request__c from Stock__c';

	global Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<Stock__c> sims = (List<Stock__c>) scope;
		
		SimContractDurationCalculator durationCalc = new SimContractDurationCalculator();
		Map<Id, Integer> contractDurations = durationCalc.calculateContractDurations(sims);
		
		for (Stock__c sim : sims) {
			sim.BIC_Contract_Duration__c = contractDurations.get(sim.Id);
		}
		update scope;
	}

	global void finish(Database.BatchableContext BC) {
		// Mortal Kombat: "Finish him!"
	}
}