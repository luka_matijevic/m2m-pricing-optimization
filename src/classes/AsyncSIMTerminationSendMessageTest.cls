@isTest
public class AsyncSIMTerminationSendMessageTest {
	static testMethod void testSIMTerminationSendMessage() {
        SIM_Termination_Job__c simTerm = new SIM_Termination_Job__c(Name = 'Test SIM Termination');
        insert simTerm;
        Test.startTest();
        System.enqueueJob(new AsyncSIMTerminationSendMessage(simTerm));
        Test.stopTest();
    }
}