global class ScheduleIoTMobilityHardSimActivation implements Schedulable {
	global void execute(SchedulableContext sc) {
		//MyBatchClass b = new MyBatchClass();
		//database.executebatch(b);
		Database.executebatch(new BatchHardActivateIoTMobilitySIMCards());
	}
}