/**
 This tests functionality of AllQuoteLineItemsTrigger and AllOpportunityLineItemTriggers
 Also does coverage for methods called by triggers: OpportunityLineItemTriggerHandled and QuoteLineItemTriggerHandler
 */
@isTest(seealldata = true)
private class TestAllQuoteLineItemTriggers {

    static testMethod void testhandleAfterInsert() {
        
        Account account = new Account(
        	Name = 'Test3',
            Type = 'Business'
        	);
        	
        insert account;
        
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1),
			AccountId = account.Id,
			Description = 'Test description 20 characters'
		);
		
		insert opp;
		
		cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(
			cscfga__Synchronised_with_Opportunity__c = false,
			cscfga__Opportunity__c = opp.Id
		);
		
		insert productBundle;
		
		cscfga__Product_Definition__c productDefinition = new cscfga__Product_Definition__c(
			Name = 'Test Name',
			cscfga__Description__c = 'Test description'
		);
		
		insert productDefinition;
		
		cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c(
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinition.Id
		);
		
		
		insert productConfiguration;
		
		cscfga__Attribute__c attribute = new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfiguration.Id,
			cscfga__Price__c = 120.0,
			cscfga__Is_Line_Item__c = true,
			cscfga__Recurring__c = true,
			Name = 'Target_Price__c',
			cscfga__Value__c = '10'
		);
		insert attribute;
		
		cscfga__Attribute__c attribute2 = new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfiguration.Id,
			cscfga__Price__c = 120.0,
			cscfga__Is_Line_Item__c = false,
			Name = 'SortOrder',
			cscfga__Value__c = '1'
			
		);
		
		insert attribute2;
		
		cscfga__Attribute__c attribute3 = new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfiguration.Id,
			cscfga__Is_Line_Item__c = false,
			Name = 'Contact Term',
			cscfga__Value__c = '24'
			
		);
		
		insert attribute3;
		
		cscfga__Attribute__c attribute4 = new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfiguration.Id,
			cscfga__Price__c = 120.0,
			cscfga__Is_Line_Item__c = false,
			Name = 'Quantity',
			cscfga__Value__c = '100'
			
		);
		
		insert attribute4;
		
		cscfga__Attribute__c attribute5 = new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfiguration.Id,
			cscfga__Is_Line_Item__c = false,
			Name = 'Request Technical Change',
			cscfga__Value__c = 'Yes'
			
		);
		
		insert attribute5;
			
		cscfga__Attribute__c attribute6 = new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = productConfiguration.Id,
			cscfga__Is_Line_Item__c = false,
			Name = 'Description',
			cscfga__Value__c = 'Test'
			
		);
		
		insert attribute6;
		
		Product2 p = new product2(
			name='Test_Product'
		);
		
		insert p;
		
		Pricebook2 standard = [Select Id, Name, IsActive From Pricebook2 where IsStandard = true LIMIT 1];
		PricebookEntry pbe = new PricebookEntry(
			pricebook2id = standard.id, 
			product2id = p.id,
			unitprice=1.0,
			isActive=true
		);
		
		insert pbe;
		
		OpportunityLineItem oli = new OpportunityLineItem(
			PricebookEntryId=pbe.Id,
			OpportunityId=opp.Id,
			Quantity=10,
			cscfga__Attribute__c=attribute.Id,
			UnitPrice = 10.0
			
		);
    	insert oli;
    	
    	
    	
    	Quote qt = new Quote(
    		Name = 'Test Quote',
    		OpportunityId = opp.id,
    		Pricebook2Id=standard.Id
    	);
    	
    	insert qt;
    	
    	QuoteLineItem qtli = new QuoteLineItem(
    		PricebookEntryId=pbe.Id,
			QuoteId=qt.Id,
			Quantity=10,
			UnitPrice = 10.0,
			cscfga__Attribute__c=attribute.Id
    	);
    	
    	insert qtli;
    }
}