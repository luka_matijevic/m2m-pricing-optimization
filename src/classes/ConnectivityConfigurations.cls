//------------------------------------------------------------------------------
// Class for handling with connectivity product configurations
//------------------------------------------------------------------------------
public without sharing class ConnectivityConfigurations implements IConnectivityConfigurations
{
	//--------------------------------------------------------------------------
	// Constants
	//--------------------------------------------------------------------------
	private static final String TARIFF_ADDITIONAL_SERVICE = 'Tariff Additional Service';
	private static final String TARIFF_SUPPLEMENTARY_SERVICE = 'Tariff Supplementary Service';
	private static final String QUANTITY = 'Quantity';
	private static final String ADDITIONAL_TARIFF_OPTION = 'Additional Tariff Option';
	private static final String SUPPLEMENTARY_SERVICE = 'Supplementary Service';
	private static final String SUPPORTSERVICE = 'Support & Service';
	private static final String PRODUCT_CONFIGURATION = 'cscfga__Product_Configuration__c';

	//--------------------------------------------------------------------------
	// Private members
	//--------------------------------------------------------------------------
	private Map<String, cscfga__Product_Definition__c> productDefinitionMap;
	private Map<String, Map<String, cscfga__Attribute_Definition__c>> attributeDefinitionMap;
	private List<cscfga__Product_Configuration__c> productConfigurations;
    private cscfga__Product_Configuration__c pc;
	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	public ConnectivityConfigurations(Id configurationId)
	{
		retrieveProductDefinitions();
		retrieveProductConfigurations(configurationId);
	}

	//--------------------------------------------------------------------------
	// Set quantities from configuration to tariff options on the tariff
	//--------------------------------------------------------------------------
	public void setQuantities(ITariff tariff)
	{
		//
		// Create all data needed for retrieving quantities
		//
		Map<String, String> tariffOptionQuantityMap = new Map<String, String>();

		//
		// Retrieve tariff option - quantity map
		//
		for (cscfga__Product_Configuration__c configuration : productConfigurations)
		{
			String definitionName = configuration.cscfga__Product_Definition__r.Name;
			String quant;
			String tariffOptionId;
			if (definitionName == TARIFF_ADDITIONAL_SERVICE)
			{
				for (cscfga__Attribute__c attribute : configuration.cscfga__Attributes__r)
				{
					if (attribute.Name == QUANTITY)
					{
						quant = attribute.cscfga__Value__c;
					}
					else if (attribute.Name == ADDITIONAL_TARIFF_OPTION)
					{
						tariffOptionId = attribute.cscfga__Value__c;
					}
					if (quant != null && tariffOptionId != null)
					{
						tariffOptionQuantityMap.put(tariffOptionId, quant);
						break;
					}
				}
			}
			else if (definitionName == TARIFF_SUPPLEMENTARY_SERVICE)
			{
				for (cscfga__Attribute__c attribute : configuration.cscfga__Attributes__r)
				{
					if (attribute.Name == QUANTITY)
					{
						quant = attribute.cscfga__Value__c;
					}
					else if (attribute.Name == SUPPLEMENTARY_SERVICE)
					{
						tariffOptionId = attribute.cscfga__Value__c;
					}
					if (quant != null && tariffOptionId != null)
					{
						tariffOptionQuantityMap.put(tariffOptionId, quant);
						break;
					}
				}
			}
		}

		//
		// Set quantities for additional tariff options
		//
		setTariffOptionQuantities(tariff.getAdditionalTariffOptions(), tariffOptionQuantityMap);
		//
		// Set quantities for supplementary service tariff options
		//
		setTariffOptionQuantities(tariff.getSupplementaryServiceTariffOptions(), tariffOptionQuantityMap);
	}

	//--------------------------------------------------------------------------
	// Delete product current product configurations
	//--------------------------------------------------------------------------
	public void deleteProductConfigurations()
	{
		delete productConfigurations;
		productConfigurations.clear();
	}

	//--------------------------------------------------------------------------
	// Create product configurations from tariff
	//--------------------------------------------------------------------------
	public String createProductConfigurations(ITariff tariff)
	{
		productConfigurations = new List<cscfga__Product_Configuration__c>();
		InsertHelper insertHelper = new InsertHelper();

		String uniqueKey = getUniqueKey();
		//
		// Create Tariff Additional Service configurations with attributes
		//
		createTariffAdditoinalServices(tariff, uniqueKey, insertHelper);
		//
		// Create Tariff Supplementary Service configurations with attributes
		//
		createTariffSupplementaryServices(tariff, uniqueKey, insertHelper);
		
	    createTariffSupportServices(tariff, uniqueKey, insertHelper);
		//
		// Store product configurations with attributes
		//
		insertHelper.store();

		return uniqueKey;
	}

	//--------------------------------------------------------------------------
	// Private methods
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Retrieve product definitions with attiributes definitions
	//--------------------------------------------------------------------------
	private void retrieveProductDefinitions()
	{
		//
		// Retrieve product definitions and attributes
		//
		Set<String> productSet = new Set<String>();
		productSet.add(TARIFF_ADDITIONAL_SERVICE);
		productSet.add(TARIFF_SUPPLEMENTARY_SERVICE);
		productSet.add(SUPPORTSERVICE);
		List<cscfga__Product_Definition__c> productDefinitions =
			[ SELECT Id, Name, cscfga__Description__c,
			  ( SELECT Id, Name
				FROM cscfga__Attribute_Definitions__r
				ORDER BY Name ASC )
			  FROM cscfga__Product_Definition__c
			  WHERE Name IN :productSet
			  ORDER BY Name ASC ];

		productDefinitionMap = new Map<String, cscfga__Product_Definition__c>();
		attributeDefinitionMap = new Map<String, Map<String, cscfga__Attribute_Definition__c>>();
		for (cscfga__Product_Definition__c productDefinition : productDefinitions)
		{
			Map<String, cscfga__Attribute_Definition__c> ids = new Map<String, cscfga__Attribute_Definition__c>();
			productDefinitionMap.put(productDefinition.Name, productDefinition);
			for (cscfga__Attribute_Definition__c ad
					: productDefinition.cscfga__Attribute_Definitions__r)
			{
				ids.put(ad.Name, ad);
			}
			attributeDefinitionMap.put(productDefinition.Name, ids);
		}
	}

	//--------------------------------------------------------------------------
	// Retrieve product configurations
	//--------------------------------------------------------------------------
	private void retrieveProductConfigurations(Id configurationId)
	{
		//
		// Initialize data
		//
		productConfigurations = new List<cscfga__Product_Configuration__c>();
		if (configurationId != null)
		{
			productConfigurations =
				[ SELECT Id, Name, cscfga__Product_Definition__r.Name,
				  ( SELECT Id, Name, cscfga__Attribute_Definition__c, cscfga__Value__c
					FROM cscfga__Attributes__r )
				  FROM cscfga__Product_Configuration__c
				  WHERE parent_product_configuration__c = :configurationId ];
		}
	}

	//--------------------------------------------------------------------------
	// Create Support and Service
	//--------------------------------------------------------------------------
	private void createTariffSupportServices(
			ITariff tariff,
			String uniqueKey,
			InsertHelper insertHelper)
	{
	 List<Tariff_Option__c> toUpdateList = new List<Tariff_Option__c>();
	    for (ITariffOption tariffOption : tariff.getSupplementaryServiceTariffOptions())		{
		    system.debug(tariffOption);
			if (tariffOption.getIsSelected() && tariffOption.getIsUsed() && tariffOption.getQuantity() != 0 && tariffOption.getTariffOption().Type__c=='Support&Service')
			{
				//
				// Product configuration
				//
				cscfga__Product_Configuration__c productConfiguration =
					createProductConfiguration(
							SUPPORTSERVICE, uniqueKey);
				
				Tariff_Option__c to = tariffOption.getTariffOption();
				System.debug('tariffOption' + tariffOption);
				System.debug('to ' + to);
				TariffOptionCalc calc = new TariffOptionCalc(tariffOption);
				//
				// Attributes
				//			
				cscfga__Attribute__c oneTimeTargetFee =
					createAttribute('One-Time Target Fee', SUPPORTSERVICE);

               //PM 18.3.2017. Remove cs apn article info set and create new for supportservice
            	/*cscfga__Attribute__c articleInfoSet =
					createAttribute('Article Info Set', SUPPORTSERVICE);
				articleInfoSet.cscfga__Value__c='a15b0000000NEt8AAG';
				
					
				cscfga__Attribute__c articleInfoSetName =
					createAttribute('Article Info Set Name', SUPPORTSERVICE);	
                articleInfoSetName.cscfga__Value__c ='CS-APN / VPN';
            	*/

            	cscfga__Attribute__c articleInfoSet;
            	cscfga__Attribute__c articleInfoSetName;
            	if(!Test.isRunningTest()){
					Id newAisId = cloneArticleInfoSetCsApn(to.Name);
	                articleInfoSet =
						createAttribute('Article Info Set', SUPPORTSERVICE);
					articleInfoSet.cscfga__Value__c=newAisId;
					
						
					articleInfoSetName =
						createAttribute('Article Info Set Name', SUPPORTSERVICE);	
	                articleInfoSetName.cscfga__Value__c =to.Name;
				}
				else{
					articleInfoSet =
						createAttribute('Article Info Set', SUPPORTSERVICE);
					articleInfoSet.cscfga__Value__c='a15b0000000NEt8AAG';
					
						
					articleInfoSetName =
						createAttribute('Article Info Set Name', SUPPORTSERVICE);	
	                articleInfoSetName.cscfga__Value__c ='CS-APN / VPN';
				}	

                //PM End of changes 18.3.2017.
                
				cscfga__Attribute__c oneTimeTargetFeeTotal =
					createAttribute('One-Time Target Fee Total', SUPPORTSERVICE);
				oneTimeTargetFeeTotal.cscfga__Line_Item_Sequence__c = 10;
				oneTimeTargetFeeTotal.cscfga__Recurring__c=false;
				oneTimeTargetFeeTotal.cscfga__Line_Item_Description__c =
					String.valueOf(tariffOption.getQuantity()) + ' x One-Time Fee';
						if(to.One_Time_Target_Fee__c!=null) {
						setOneTimeAttributePrice(
						oneTimeTargetFeeTotal,
						to.One_Time_Target_Fee__c * tariffOption.getQuantity());
						}
				
				cscfga__Attribute__c quant =
					createAttribute(QUANTITY, SUPPORTSERVICE);

				quant.cscfga__Value__c = 
					String.valueOf(tariffOption.getQuantity());
				
				cscfga__Attribute__c recurringTargetFee =
					createAttribute('Recurring Target Fee', SUPPORTSERVICE);
				recurringTargetFee.cscfga__Value__c =
					toString(to.Recurring_Target_Fee__c);

				cscfga__Attribute__c recurringTargetFeeTotal =
					createAttribute('Recurring Target Fee Total', SUPPORTSERVICE);
				recurringTargetFeeTotal.cscfga__Line_Item_Sequence__c = 20;
				recurringTargetFeeTotal.cscfga__Line_Item_Description__c =
					String.valueOf(tariffOption.getQuantity()) + ' x Recurring Fee';
				setRecurringAttributePrice(
						recurringTargetFeeTotal,
						to.Recurring_Target_Fee__c * tariffOption.getQuantity());

				//
				// Product configuration
				//
				productConfiguration.Name = tariffOption.getTariffOption().Name;
				//
				// Unit price is zero
				//
				Decimal unitPrice = 0;
				unitPrice = Utils.addValues(unitPrice, calc.getRecurringTargetFeeTotal());
				unitPrice *= 12;
				unitPrice = Utils.addValues(unitPrice, calc.getOneTimeTargetFeeTotal());
				productConfiguration.cscfga__Unit_Price__c = unitPrice;
				//
				// Set contract term
				//
				productConfiguration.cscfga__Contract_Term__c =
					Decimal.valueOf(tariff.getTariff().Contract_Term__c);							
						
				productConfigurations.add(productConfiguration);
                system.debug(productConfiguration);
                system.debug(quant);
                system.debug(articleInfoSet);
				insertHelper.add(productConfiguration);			
				insertHelper.add(oneTimeTargetFee, PRODUCT_CONFIGURATION, productConfiguration);		
				insertHelper.add(oneTimeTargetFeeTotal, PRODUCT_CONFIGURATION, productConfiguration);		
				insertHelper.add(quant, PRODUCT_CONFIGURATION, productConfiguration);		
				insertHelper.add(recurringTargetFee, PRODUCT_CONFIGURATION, productConfiguration);		
				insertHelper.add(recurringTargetFeeTotal, PRODUCT_CONFIGURATION, productConfiguration);		
                insertHelper.add(articleInfoSet, PRODUCT_CONFIGURATION, productConfiguration);	
                insertHelper.add(articleInfoSetName, PRODUCT_CONFIGURATION, productConfiguration);
						
                to.isConfiguration__c = true;
			    toUpdateList.add(to);
			}
		}
		update toUpdateList;
	}


	//--------------------------------------------------------------------------
	// Create Tariff Additional Service
	//--------------------------------------------------------------------------
	private void createTariffAdditoinalServices(
			ITariff tariff,
			String uniqueKey,
			InsertHelper insertHelper)
	{
	    	 List<Tariff_Option__c> toUpdateList = new List<Tariff_Option__c>();
		for (ITariffOption tariffOption : tariff.getAdditionalTariffOptions())
		{
		    system.debug(tariffOption);
			if (tariffOption.getIsSelected() && tariffOption.getIsUsed() && tariffOption.getTariffOption().Type__c=='Additional')
			{
				//
				// Product configuration
				//
				cscfga__Product_Configuration__c productConfiguration =
					createProductConfiguration(
							TARIFF_ADDITIONAL_SERVICE, uniqueKey);

				Tariff_Option__c to = tariffOption.getTariffOption();
				TariffOptionCalc calc = new TariffOptionCalc(tariffOption);
				//
				// Attributes
				//
				cscfga__Attribute__c additionalTariffOption =
					createAttribute(ADDITIONAL_TARIFF_OPTION, TARIFF_ADDITIONAL_SERVICE);
				additionalTariffOption.cscfga__Value__c = to.Id;

				cscfga__Attribute__c additionalTariffOptionName =
					createAttribute('Additional Tariff Option Name', TARIFF_ADDITIONAL_SERVICE);
				additionalTariffOptionName.cscfga__Value__c = to.Name;

				cscfga__Attribute__c contractTerm =
					createAttribute('Contract Term', TARIFF_ADDITIONAL_SERVICE);
				contractTerm.cscfga__Value__c = tariff.getTariff().Contract_Term__c;

				cscfga__Attribute__c oneTimeTargetFee =
					createAttribute('One-Time Target Fee', TARIFF_ADDITIONAL_SERVICE);

				cscfga__Attribute__c oneTimeTargetFeeTotal =
					createAttribute('One-Time Target Fee Total', TARIFF_ADDITIONAL_SERVICE);
				oneTimeTargetFeeTotal.cscfga__Line_Item_Sequence__c = 10;
				oneTimeTargetFeeTotal.cscfga__Line_Item_Description__c =
					String.valueOf(tariffOption.getQuantity()) + ' x One-Time Fee';

				cscfga__Attribute__c quant =
					createAttribute(QUANTITY, TARIFF_ADDITIONAL_SERVICE);
				quant.cscfga__Value__c =
					toString(tariffOption.getQuantity());

				cscfga__Attribute__c recurringTargetFee =
					createAttribute('Recurring Target Fee', TARIFF_ADDITIONAL_SERVICE);
				recurringTargetFee.cscfga__Value__c =
					toString(to.Recurring_Target_Fee__c);

				cscfga__Attribute__c recurringTargetFeeTotal =
					createAttribute('Recurring Target Fee Total', TARIFF_ADDITIONAL_SERVICE);
				recurringTargetFeeTotal.cscfga__Line_Item_Sequence__c = 20;
				recurringTargetFeeTotal.cscfga__Line_Item_Description__c =
					String.valueOf(tariffOption.getQuantity()) + ' x Recurring Fee';
				setRecurringAttributePrice(
						recurringTargetFeeTotal,
						to.Recurring_Target_Fee__c * tariffOption.getQuantity());

				cscfga__Attribute__c totalExpectedDataOverUsage =
					createAttribute('Total Expected Data Over-Usage', TARIFF_ADDITIONAL_SERVICE);
				totalExpectedDataOverUsage.cscfga__Line_Item_Sequence__c = 30;
				totalExpectedDataOverUsage.cscfga__Line_Item_Description__c =
					'Total Expected Data Over-Usage';
				setRecurringAttributePrice(totalExpectedDataOverUsage, calc.getDataOverUsage());

				cscfga__Attribute__c totalExpectedTextOverUsage =
					createAttribute('Total Expected Text Over-Usage', TARIFF_ADDITIONAL_SERVICE);
				totalExpectedTextOverUsage.cscfga__Line_Item_Sequence__c = 40;
				totalExpectedTextOverUsage.cscfga__Line_Item_Description__c =
					'Total Expected Text Over-Usage';
				setRecurringAttributePrice(totalExpectedTextOverUsage, calc.getTextOverUsage());

				cscfga__Attribute__c totalExpectedVoiceOverUsage =
					createAttribute('Total Expected Voice Over-Usage', TARIFF_ADDITIONAL_SERVICE);
				totalExpectedVoiceOverUsage.cscfga__Line_Item_Sequence__c = 50;
				totalExpectedVoiceOverUsage.cscfga__Line_Item_Description__c =
					'Total Expected Voice Over-Usage';
				setRecurringAttributePrice(totalExpectedVoiceOverUsage, calc.getVoiceOverUsage());

				
                
				//
				// Product configuration
				//
				productConfiguration.Name = tariffOption.getTariffOption().Name;
				//
				// Set unit price
				//
				Decimal unitPrice = 0;
				unitPrice = Utils.addValues(unitPrice, calc.getRecurringTargetFeeTotal());
				unitPrice = Utils.addValues(unitPrice, calc.getDataOverUsage());
				unitPrice = Utils.addValues(unitPrice, calc.getTextOverUsage());
				unitPrice = Utils.addValues(unitPrice, calc.getVoiceOverUsage());
				unitPrice *= 12;
				unitPrice = Utils.addValues(unitPrice, calc.getOneTimeTargetFeeTotal());
				productConfiguration.cscfga__Unit_Price__c = unitPrice;
				//
				// Set contract term
				//
				productConfiguration.cscfga__Contract_Term__c = Decimal.valueOf(tariff.getTariff().Contract_Term__c);

				productConfigurations.add(productConfiguration);
                system.debug(productConfiguration);
				insertHelper.add(productConfiguration);
				insertHelper.add(additionalTariffOption, PRODUCT_CONFIGURATION, productConfiguration);
				insertHelper.add(additionalTariffOptionName, PRODUCT_CONFIGURATION, productConfiguration);
				insertHelper.add(contractTerm, PRODUCT_CONFIGURATION, productConfiguration);
				insertHelper.add(oneTimeTargetFee, PRODUCT_CONFIGURATION, productConfiguration);
				insertHelper.add(oneTimeTargetFeeTotal, PRODUCT_CONFIGURATION, productConfiguration);
				insertHelper.add(quant, PRODUCT_CONFIGURATION, productConfiguration);
				insertHelper.add(recurringTargetFee, PRODUCT_CONFIGURATION, productConfiguration);
				insertHelper.add(recurringTargetFeeTotal, PRODUCT_CONFIGURATION, productConfiguration);
				insertHelper.add(totalExpectedDataOverUsage, PRODUCT_CONFIGURATION, productConfiguration);
				insertHelper.add(totalExpectedTextOverUsage, PRODUCT_CONFIGURATION, productConfiguration);
				insertHelper.add(totalExpectedVoiceOverUsage, PRODUCT_CONFIGURATION, productConfiguration);

	            to.isConfiguration__c = true;
			    toUpdateList.add(to);
			    }
		    }
		    		update toUpdateList;
		}


	//--------------------------------------------------------------------------
	// Create Tariff Supplementary Services
	//--------------------------------------------------------------------------
	private void createTariffSupplementaryServices(
			ITariff tariff,
			String uniqueKey,
			InsertHelper insertHelper)
	{
	    	 List<Tariff_Option__c> toUpdateList = new List<Tariff_Option__c>();
		for (ITariffOption tariffOption : tariff.getSupplementaryServiceTariffOptions())
		{
			if (tariffOption.getIsSelected() && tariffOption.getIsUsed() && tariffOption.getQuantity() != 0 && tariffOption.getTariffOption().Type__c=='Supplementary Service')
			{
				//
				// Product configuration
				//
				cscfga__Product_Configuration__c productConfiguration =
					createProductConfiguration(
							TARIFF_SUPPLEMENTARY_SERVICE, uniqueKey);
				Tariff_Option__c to = tariffOption.getTariffOption();
				TariffOptionCalc calc = new TariffOptionCalc(tariffOption);
				//
				// Attributes
				//
				cscfga__Attribute__c contractTerm =
					createAttribute('Contract Term', TARIFF_SUPPLEMENTARY_SERVICE);
				contractTerm.cscfga__Value__c = tariff.getTariff().Contract_Term__c;

				cscfga__Attribute__c oneTimeTargetFeePerCustomer =
					createAttribute('One-Time Target Fee per Customer', TARIFF_SUPPLEMENTARY_SERVICE);
				oneTimeTargetFeePerCustomer.cscfga__Value__c =
					toString(calc.getOneTimeTargetFeePerCustomer());

				cscfga__Attribute__c  oneTimeTargetFeePerVolume =
					createAttribute('One-Time Target Fee per Volume', TARIFF_SUPPLEMENTARY_SERVICE);
				oneTimeTargetFeePerVolume.cscfga__Value__c =
					toString(calc.getOneTimeTargetFeePerVolume());

				cscfga__Attribute__c oneTimeTargetFeeTotal =
					createAttribute('One-Time Target Fee Total', TARIFF_SUPPLEMENTARY_SERVICE);
				oneTimeTargetFeeTotal.cscfga__Line_Item_Sequence__c = 10;
				oneTimeTargetFeeTotal.cscfga__Line_Item_Description__c =
					String.valueOf(tariffOption.getQuantity()) + ' x One-Time Fee';
				setOneTimeAttributePrice(oneTimeTargetFeeTotal, calc.getOneTimeTargetFeeTotal());

				cscfga__Attribute__c quant =
					createAttribute(QUANTITY, TARIFF_SUPPLEMENTARY_SERVICE);
				quant.cscfga__Value__c = toString(tariffOption.getQuantity());

				cscfga__Attribute__c recurringTargetFeePerCustomer =
					createAttribute('Recurring Target Fee per Customer', TARIFF_SUPPLEMENTARY_SERVICE);
				recurringTargetFeePerCustomer.cscfga__Value__c =
					toString(calc.getRecurringTargetFeePerCustomer());

				cscfga__Attribute__c recurringTargetFeePerVolume =
					createAttribute('Recurring Target Fee per Volume', TARIFF_SUPPLEMENTARY_SERVICE);
				recurringTargetFeePerVolume.cscfga__Value__c =
					toString(calc.getRecurringTargetFeePerVolume());

				cscfga__Attribute__c recurringTargetFeeTotal =
					createAttribute('Recurring Target Fee Total', TARIFF_SUPPLEMENTARY_SERVICE);
				recurringTargetFeeTotal.cscfga__Line_Item_Sequence__c = 20;
				recurringTargetFeeTotal.cscfga__Line_Item_Description__c =
					String.valueOf(tariffOption.getQuantity()) + ' x Recurring Fee';
				setRecurringAttributePrice(recurringTargetFeeTotal, calc.getRecurringTargetFeeTotal());

				cscfga__Attribute__c supplementaryService =
					createAttribute(SUPPLEMENTARY_SERVICE, TARIFF_SUPPLEMENTARY_SERVICE);
				supplementaryService.cscfga__Value__c =	to.Id;

				cscfga__Attribute__c supplementaryServiceName =
					createAttribute('Supplementary Service Name', TARIFF_SUPPLEMENTARY_SERVICE);
				supplementaryServiceName.cscfga__Value__c = to.Name;

				//
				// Product configuration
				//
				productConfiguration.Name = tariffOption.getTariffOption().Name;
				//
				// Unit price is zero
				//
				Decimal unitPrice = 0;
				unitPrice = Utils.addValues(unitPrice, calc.getRecurringTargetFeeTotal());
				unitPrice *= 12;
				unitPrice = Utils.addValues(unitPrice, calc.getOneTimeTargetFeeTotal());
				productConfiguration.cscfga__Unit_Price__c = unitPrice;
				//
				// Set contract term
				//
				productConfiguration.cscfga__Contract_Term__c =
					Decimal.valueOf(tariff.getTariff().Contract_Term__c);

				productConfigurations.add(productConfiguration);

				insertHelper.add(productConfiguration);
				insertHelper.add(contractTerm, PRODUCT_CONFIGURATION, productConfiguration);
				insertHelper.add(oneTimeTargetFeePerCustomer, PRODUCT_CONFIGURATION, productConfiguration);
				insertHelper.add(oneTimeTargetFeePerVolume, PRODUCT_CONFIGURATION, productConfiguration);
				insertHelper.add(oneTimeTargetFeeTotal, PRODUCT_CONFIGURATION, productConfiguration);
				insertHelper.add(quant, PRODUCT_CONFIGURATION, productConfiguration);
				insertHelper.add(recurringTargetFeePerCustomer, PRODUCT_CONFIGURATION, productConfiguration);
				insertHelper.add(recurringTargetFeePerVolume, PRODUCT_CONFIGURATION, productConfiguration);
				insertHelper.add(recurringTargetFeeTotal, PRODUCT_CONFIGURATION, productConfiguration);
				insertHelper.add(supplementaryService, PRODUCT_CONFIGURATION, productConfiguration);
				insertHelper.add(supplementaryServiceName, PRODUCT_CONFIGURATION, productConfiguration);
		        to.isConfiguration__c = true;
			    toUpdateList.add(to);
			    }
		    }
		    		update toUpdateList;
		}

	//--------------------------------------------------------------------------
	// Create product configuration with name
	//--------------------------------------------------------------------------
	private cscfga__Product_Configuration__c createProductConfiguration(
			String name,
			String uniqueKey)
	{
		cscfga__Product_Configuration__c returnValue =
			new cscfga__Product_Configuration__c();
		returnValue.Name = name;
		returnValue.cscfga__Product_Definition__c =
			productDefinitionMap.get(name).Id;
		returnValue.cscfga__Description__c =
			productDefinitionMap.get(name).cscfga__Description__c;
		returnValue.cscfga__Recurrence_Frequency__c = 12;
		returnValue.cscfga__Contract_Term_Period__c = 12;
		returnValue.Unique_Key__c = uniqueKey;
		return returnValue;
	}

	//--------------------------------------------------------------------------
	// Create attribute with name
	//--------------------------------------------------------------------------
	private cscfga__Attribute__c createAttribute(String name,
			String definitionName)
	{
		cscfga__Attribute__c returnValue = new cscfga__Attribute__c();
		returnValue.Name = name;
		Map<String, cscfga__Attribute_Definition__c> attrMap = attributeDefinitionMap.get(definitionName);
		cscfga__Attribute_Definition__c attr = attrMap.get(name);
		returnValue.cscfga__Attribute_Definition__c = attr.Id;
		/*returnValue.cscfga__Attribute_Definition__c =
			attributeDefinitionMap.get(definitionName).get(name).Id;*/
		return returnValue;
	}

	//--------------------------------------------------------------------------
	// Set quantities to tariff options from tariff option - quantity map
	//--------------------------------------------------------------------------
	private static void setTariffOptionQuantities(
			List<ITariffOption> tariffOptions,
			Map<String, String> tariffOptionQuantityMap)
	{
		system.debug('tariffOptionQuantityMap: ' + tariffOptionQuantityMap);
		for (ITariffOption tariffOption : tariffOptions)
		{
			String tariffOptionId = tariffOption.getTariffOption().Id;
			String value = tariffOptionQuantityMap.get(tariffOptionId);
			system.debug('tariffOptionId: ' + tariffOptionId);
			system.debug('value: ' + value);
			try
			{
				if (String.isNotBlank(value))
				{
					Integer quant = Integer.valueOf(value);
					tariffOption.setQuantity(quant);
				}
			}
			catch (Exception e) {}
		}
	}

	//--------------------------------------------------------------------------
	// Set attribute price and line and recurring item
	//--------------------------------------------------------------------------
	private static void setRecurringAttributePrice(
			cscfga__Attribute__c attribute, Decimal price)
	{
		attribute.cscfga__Price__c = price;
		attribute.cscfga__Value__c = toString(price);
		attribute.cscfga__Recurring__c = true;
		if (price != null)
		{
			attribute.cscfga__Is_Line_Item__c = true;
		}
	}

	//--------------------------------------------------------------------------
	// Set attribute price and line and one time item
	//--------------------------------------------------------------------------
	private static void setOneTimeAttributePrice(
			cscfga__Attribute__c attribute, Decimal price)
	{
		attribute.cscfga__Price__c = price;
		attribute.cscfga__Value__c = toString(price);
		attribute.cscfga__Recurring__c = false;
		if (price != null)
		{
			attribute.cscfga__Is_Line_Item__c = true;
		}
	}

	//--------------------------------------------------------------------------
	// Show value as string
	// If value is null returns null
	//--------------------------------------------------------------------------
	private static String toString(Object value)
	{
		if (value == null)
		{
			return null;
		}
		return String.valueOf(value);
	}

	//--------------------------------------------------------------------------
	// Creates unique key
	//--------------------------------------------------------------------------
	private static String getUniqueKey()
	{
		DateTime now = DateTime.now();
		return String.valueOf(now) + '-' + Crypto.getRandomLong();
	}

	private Id cloneArticleInfoSetCsApn(String aisName){
		Article_Info_Set__c aisCsApn = database.query('select ' + SObjectHelper.getFieldListForSOQL(Article_Info_Set__c.SObjectType.getDescribe().getName(), null, null) +
                                                    ' from Article_Info_Set__c where Id = \'a15b0000000NEt8AAG\'');

		//clone
		Article_Info_Set__c newAis = aisCsApn.clone(false, true, false, false);
		newAis.Name = aisName;
		upsert newAis;
		return newAis.Id;
	}
}