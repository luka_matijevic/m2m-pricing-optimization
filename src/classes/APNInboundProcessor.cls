global class APNInboundProcessor  implements csam_t1.InboundMessageProcessor {

    /**
    * Accepts all archiving messages (should be put before the standard Inbound Message Handler)
    * @param incomingMessage message to be processed
    * @return Boolean returns true
    */
    public Boolean isMessageProcessible(csam_t1__Incoming_Message__c incomingMessage) {     
        return incomingMessage.csam_t1__Incoming_URL_Path__c == '/csam_t1/callback/account/APNs';

    }
      /**
   * The constructor shouldn't do any processing, because the class is initialized for the isProcessible check
   */ 
    public APNInboundProcessor() {
  
     }

    /**
    * Assumes that the message payload is a status message from integration platform
    * 
    * @param incomingMessage message to be processed with corresponding payload attachment
    * @param oldToNewIdsMap map of old-to-new ids used when inserting in chunks to pass the references between messages
    */
    public void processMessage(csam_t1__Incoming_Message__c incomingMessage, Map<Id, Id> oldToNewIdsMap) {

    System.debug('SubscriptionGroupsInboundProcessor processMessage');
    
        try {           
            Attachment attachment = [SELECT Id, Name, Body
                                        FROM Attachment
                                        WHERE ParentId = :incomingMessage.Id
                                        AND Name like 'Message%' LIMIT 1];
            CallbackMessage cm = (CallbackMessage) JSON.deserialize(attachment.Body.toString(), CallbackMessage.class);
            if (cm.objectsToUpdate != null && cm.objectsToUpdate.size() > 0) {
                processUpdates(cm);
            }
            if (cm.objectsToInsert != null && cm.objectsToInsert.size() > 0) {
                processInsert(cm);
            }
            incomingMessage.csam_t1__Status__c = 'Processed';
            

        } 
        catch (Exception e) {
            incomingMessage.csam_t1__Status__c = 'Process Error';

            Attachment errorAttach = new Attachment();
            errorAttach.Body = Blob.valueOf(e.getStackTraceString());
            errorAttach.parentId = incomingMessage.Id;

            String fullName = 'Error_' + e.getMessage();
            errorAttach.Name = fullName.abbreviate(255);
            
            insert errorAttach;
        }
        
        incomingMessage.csam_t1__Processed_At__c = System.now();
        update incomingMessage; 

    }

    /**
     * @description Processes all updates defined by CallbackMessage.objectsToUpdate. First objects are separated based
     * on sObject type. Then, to prevent different sObject types in single collection and corresponding chunking limits,
     * each object type collection is separately updated.
     * @param cm CallbackMessage
     */
 
    private void processUpdates(CallbackMessage cm) {
        Map<String, List<sObject>> objectsToUpdateMap = new Map<String, List<sObject>>();
        for (sObject objectToUpdate : cm.objectsToUpdate) {
            String objectToUpdateName = objectToUpdate.getSObjectType().getDescribe().getName();
            if (!objectsToUpdateMap.containsKey(objectToUpdateName)) {
                objectsToUpdateMap.put(objectToUpdateName, new List<sObject>());
            }
            objectsToUpdateMap.get(objectToUpdateName).add(objectToUpdate);
        }

        // avoid updates in chunks with different sObject types in a collection
        for (String key : objectsToUpdateMap.keySet()) {
            update objectsToUpdateMap.get(key);
        }
    }

    private void processInsert(CallbackMessage cm) {
        Map<String, List<sObject>> objectsToInsertMap = new Map<String, List<sObject>>();
        for (sObject objectToInsert : cm.objectsToInsert) {
            String objectToInsertName = objectToInsert.getSObjectType().getDescribe().getName();
            if (!objectsToInsertMap.containsKey(objectToInsertName)) {
                objectsToInsertMap.put(objectToInsertName, new List<sObject>());
            }
            objectsToInsertMap.get(objectToInsertName).add(objectToInsert);
        }

        // avoid updates in chunks with different sObject types in a collection
        for (String key : objectsToInsertMap.keySet()) {
            insert objectsToInsertMap.get(key);
        }
    }

}