/**
 *  Test StockHistoryTrackBatch execution. Test class to provide coverage to one time execution of StockHistoryTrackBatch .
 */
@isTest (seeAllData=false)
private class StockCommercialDataBatchTest {
	
	@isTest static void testCommercialDateUpdate() {
		
		No_Trigger__c notriggers = new No_Trigger__c();
		notriggers.Flag__c = true;
		insert notriggers;

		// Create Order request
		TestHelper.createOrderRequest();
		notriggers.Flag__c = false;
		update notriggers;
		
		Integer year = Date.today().year();

		Account acc = new Account(Name = 'Test account', TEF_DE_Sales_Channel__c = 'Test channel', Type = 'Customer');
		insert acc;
		
		Date testDate = Date.newInstance(2014,2,10);
		Account accPayer = new Account(
			Name = 'Test account', 
			BillingCity = 'Bremen', 
			BillingCountry = 'Germany', 
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße', 
			Type = 'Customer', 
			Billing_Cycle__c = String.valueOf(testDate.day()),
			VAT__c = 1,
            Global__c = true
		);
		insert accPayer;
 Contact con = Test_Util.createContact(accPayer.id);
        
        con.Country__c = 'Germany';
        insert con;
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1)
			, recordtypeid = Cache.getRecordTypeId('Opportunity.Commercial_Global'),
			Description = 'Test description 20 characters'
		);
		insert opp;		
		
		
        Frame_Contract__c fc = new Frame_Contract__c();
        fc.contact__c = con.id;
        fc.customer__c = accPayer.id;
        fc.opportunity__c = opp.id;
         insert fc;
		Billing_Account__c billingAccount = new Billing_Account__c(
			Payer__c = accPayer.Id, 
			Trial__c=false, 
			Billable__c=true,
			VAT__c = '1'
			, OFI_ID__c = '123'
		);
		insert billingAccount;
		
		Order_Request__c ord = [ SELECT Id FROM Order_Request__c LIMIT 1];
		ord.Billing_Account__c = billingAccount.Id;
		update ord;
		
		Article_Info_Set__c ais = [ SELECT Id FROM Article_Info_Set__c WHERE Article_Type__c = 'SIM' LIMIT 1];

		Third_Upload_File__c thirdFile = new Third_Upload_File__c ( Name = 'name123', Order_Request__c = ord.Id );
		insert thirdFile;

		SRF__c srf = new SRF__c ( Account__c = acc.Id, SIM_Type__c = ais.Id );
		insert srf;
		
		List<Stock__c> simList = new List<Stock__c>();

		Stock__c sim1 = new Stock__c(
		Name = '1234567801234567892',
		ICCID__c = '1234567801234567892',
		Account__c = acc.Id,
		Third_Upload_File__c = thirdFile.Id,
		Activation_Date__c = Date.newInstance(year, 1, 3)
		);
		
		simList.add(sim1);
		insert simList;
		
		Test.startTest();
		Database.executeBatch(new StockCommercialDataBatch());
		Test.stopTest();

		Stock__c st = [Select Id, Commercial_Date__c from Stock__c where Id=: simList[0].Id limit 1];
		System.assert(st.Commercial_Date__c!=null);
	}
	
	@isTest static void testCommercialDateUpdate2() {
		
		No_Trigger__c notriggers = new No_Trigger__c();
		notriggers.Flag__c = true;
		insert notriggers;

		// Create Order request
		TestHelper.createOrderRequest();
		notriggers.Flag__c = false;
		update notriggers;
		
		Integer year = Date.today().year();

		Account acc = new Account(
			Name = 'Test account111', 
			TEF_DE_Sales_Channel__c = 'Test channel', 
			Type = 'Customer',
			VAT__c = 1,
			BillingCity = 'Bremen', 
			BillingCountry = 'Germany', 
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße',
            Global__c = true
		);
		insert acc;
		
		Date testDate = Date.newInstance(2014,2,10);
		Account accPayer = new Account(
			Name = 'Test account', 
			BillingCity = 'Bremen', 
			BillingCountry = 'Germany', 
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße', 
			Type = 'Customer', 
			Billing_Cycle__c = String.valueOf(testDate.day()),
			VAT__c = 1,
            Global__c = true
			
		);
		insert accPayer;
 Contact con = Test_Util.createContact(accPayer.id);
        
        con.Country__c = 'Germany';
        insert con;
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1)
			, recordtypeid = Cache.getRecordTypeId('Opportunity.Commercial_Global'),
			Description = 'Test description 20 characters'
		);
		insert opp;		
		
		
        Frame_Contract__c fc = new Frame_Contract__c();
        fc.contact__c = con.id;
        fc.customer__c = accPayer.id;
        fc.opportunity__c = opp.id;
         insert fc;
		Billing_Account__c billingAccount = new Billing_Account__c(
			Payer__c = accPayer.Id, 
			Trial__c=false, 
			Billable__c=true,
			VAT__c = '1'
			, OFI_ID__c = '123'
		);
		insert billingAccount;
		
		Order_Request__c ord = [ SELECT Id FROM Order_Request__c LIMIT 1];
		ord.Billing_Account__c = billingAccount.Id;
		update ord;
		
		Article_Info_Set__c ais = [ SELECT Id FROM Article_Info_Set__c WHERE Article_Type__c = 'SIM' LIMIT 1];

		SRF__c srf = new SRF__c ( Account__c = acc.Id, SIM_Type__c = ais.Id, Order_Request__c = ord.Id  );
		insert srf;
		
		List<Stock__c> simList = new List<Stock__c>();

		Stock__c sim1 = new Stock__c(
		Name = '123458123456789111',
		ICCID__c = '1234590123456789111',
		Account__c = acc.Id,
		SRF__c = srf.Id,
		Activation_Date__c = Date.newInstance(year, 1, 3)
		);
		
		simList.add(sim1);
		insert simList;
		
		Test.startTest();
		Database.executeBatch(new StockCommercialDataBatch());
		Test.stopTest();

		Stock__c st = [Select Id, Commercial_Date__c from Stock__c where Id=: simList[0].Id limit 1];
		System.assert(st.Commercial_Date__c!=null);
	}
}