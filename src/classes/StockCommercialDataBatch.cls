/**
 * Description: Batch class to update Stock records Commercial date field (batch will be executed as one time script to sync existing records).
 * MAKE SURE TO BYPASS TRIGGERS & VALIDATION USING CUSTOM SETTINGS DATA (AS REQUIRED).
 * This field will be populated with either Activation Date or with Billing Account last modified date (if Billing Account is commercial) 
 * it will take latest date (e.g. if activation date is May and last modified date of Billing Account which is commercial is in August, it will populate the date field with August).
 * Author: Ritesh Saxena
 * Date: 22/12/2014
 */
public class StockCommercialDataBatch implements Database.Batchable<sObject>{
	
	private static String query = 'Select Id, Account__c, SRF__c, Third_Upload_File__c, Activation_Date__c, Commercial_Date__c from Stock__c';
	
	/*
	 * Start call to process SIM record query.
	 */
	public Database.QueryLocator start(Database.BatchableContext BC) {
		if (test.isRunningTest()) {
			query+= ' limit 25';
		}
		return Database.getQueryLocator(query);  
	}
	
	/*
	 * Execute call to process SIM record.
	 */
	public void execute(Database.BatchableContext BC, List<SObject> scope) {
		List<Stock__c> stockList = new List<Stock__c>();
		stockList = processStockList(scope);
		if (!stockList.isEmpty()) {         
			update stockList;          
		}
	}
	
	/*
	 * Finish call.
	 */
	public void finish(Database.BatchableContext BC) {

		//Nothing to-do            
	}
	
	/*
	 * Process SIM records & update commercial date from billing account's (if Commercial) last modified date
	 */
	private static List<sObject> processStockList(List<sObject> sObjectList) {
		Set<Id> srfIdSet = new Set<Id>();
		Set<Id> tufIdSet = new Set<Id>();
		Set<Id> accIdSet = new Set<Id>();
		Map<Id,SRF__c> srfSObjectList = new Map<Id,SRF__c>();
		Map<Id,Third_Upload_File__c> tufSObjectList = new Map<Id,Third_Upload_File__c>();
		List<Billing_Account__c> billingAccountList = new List<Billing_Account__c>();
		Map<Id,Billing_Account__c> accIdToBillingAccMap = new Map<Id,Billing_Account__c>();
		Map<Id,Account> accountList = new Map<Id,Account>();
		List<sObject> newStockList = new List<sObject>();
		
		for (Stock__c obj : (List<Stock__c>) sObjectList) {
			if (obj.SRF__c != null) {
				srfIdSet.add(obj.SRF__c);
			}
			if (obj.Third_Upload_File__c != null) {
				tufIdSet.add(obj.Third_Upload_File__c);
			}
			if (obj.Account__c != null) {
				accIdSet.add(obj.Account__c);
			}
		}

		if (srfIdSet.size()>0) { 
			srfSObjectList = new Map<Id,SRF__c>([Select Account__c, Id, Order_Request__c, Order_Request__r.Billing_Account__c, Order_Request__r.Billing_Account__r.Trial__c, Order_Request__r.Billing_Account__r.LastModifiedDate, Order_Request__r.Billing_Account__r.Billable__c, Order_Request__r.Account__c from SRF__c where Id in :srfIdSet]);
		}
		if (tufIdSet.size()>0) { 
			tufSObjectList = new Map<Id,Third_Upload_File__c>([Select Account__c,Id, Order_Request__c, Order_Request__r.Billing_Account__c, Order_Request__r.Billing_Account__r.Trial__c, Order_Request__r.Billing_Account__r.LastModifiedDate, Order_Request__r.Billing_Account__r.Billable__c, Order_Request__r.Account__c from Third_Upload_File__c where Id in :tufIdSet]);
		}
		if (srfSObjectList.size()>0) {
			for (SRF__c srf : srfSObjectList.values()) {
				accIdSet.add(srf.Account__c);
			}
		}

		if (tufSObjectList.size()>0) {
			for (Third_Upload_File__c tuf : tufSObjectList.values()) {
				accIdSet.add(tuf.Account__c);
			}
		}
		if (accIdSet.size()>0) {
			accountList = new Map<Id, Account>([Select Id,Name from Account where Id in :accIdSet]);
		}
		if (accIdSet.size()>0) {
			billingAccountList = [Select Payer__c, Trial__c, Billable__c, LastModifiedDate, Name from Billing_Account__c where Payer__c in :accIdSet];
		}
		if (billingAccountList.size()>0) {
			for (Billing_Account__c bacc : billingAccountList) {
				if (bacc.Name.contains('default') || bacc.Name.contains('Default') || accIdToBillingAccMap.get(bacc.Payer__c) == null) {
					accIdToBillingAccMap.put(bacc.Payer__c, bacc);
				}
			}
		}
		for (sObject obj : sObjectList) {
			Stock__c sim = (Stock__c) obj;
			//boolean srfFlag = false;
			//boolean tufFlag = false;
			//Boolean defaultBillingAccountTrial = false;
			DateTime srfDate = null;
			DateTime tufDate = null;
			if (srfSObjectList.size()>0) {
				if (sim.SRF__c != null && srfSObjectList.get(sim.SRF__c) != null && srfSObjectList.get(sim.SRF__c).Order_Request__c != null 
						&& srfSObjectList.get(sim.SRF__c).Order_Request__r.Billing_Account__c != null && srfSObjectList.get(sim.SRF__c).Order_Request__r.Billing_Account__r.Billable__c
						&& !srfSObjectList.get(sim.SRF__c).Order_Request__r.Billing_Account__r.Trial__c) {
					srfDate = srfSObjectList.get(sim.SRF__c).Order_Request__r.Billing_Account__r.LastModifiedDate;
				}
			}
			if (srfDate!=null) {
				if (srfDate> sim.Activation_Date__c) {
					sim.Commercial_Date__c= srfDate;
				} else {
					sim.Commercial_Date__c = sim.Activation_Date__c;
				}
			}
			
			if (tufSObjectList.size()>0) {
				if (sim.Third_Upload_File__c != null && tufSObjectList.get(sim.Third_Upload_File__c) != null && tufSObjectList.get(sim.Third_Upload_File__c).Order_Request__c != null 
						&& tufSObjectList.get(sim.Third_Upload_File__c).Order_Request__r.Billing_Account__c != null && tufSObjectList.get(sim.Third_Upload_File__c).Order_Request__r.Billing_Account__r.Billable__c
						&& !tufSObjectList.get(sim.Third_Upload_File__c).Order_Request__r.Billing_Account__r.Trial__c) {
					tufDate = tufSObjectList.get(sim.Third_Upload_File__c).Order_Request__r.Billing_Account__r.LastModifiedDate;
				}
			}
			if (srfDate==null && tufDate!=null) {
				if (tufDate> sim.Activation_Date__c) {
					sim.Commercial_Date__c= tufDate;
				} else {
					sim.Commercial_Date__c = sim.Activation_Date__c;
				}
			}
			if (sim.Commercial_Date__c==null) {
				if ((sim.SRF__c == null && sim.Third_Upload_File__c == null) || !((sim.SRF__c != null && srfSObjectList.get(sim.SRF__c) != null && srfSObjectList.get(sim.SRF__c).Order_Request__c != null 
								&& srfSObjectList.get(sim.SRF__c).Order_Request__r.Billing_Account__c != null ) || (sim.Third_Upload_File__c != null && tufSObjectList.get(sim.Third_Upload_File__c) != null 
								&& tufSObjectList.get(sim.Third_Upload_File__c).Order_Request__c != null && tufSObjectList.get(sim.Third_Upload_File__c).Order_Request__r.Billing_Account__c != null))) {
					Id stockAccountId = sim.Account__c;
					if (stockAccountId != null) {
						if (accountList.get(stockAccountId) != null && accIdToBillingAccMap.size()>0 && accIdToBillingAccMap.get(stockAccountId) != null) {
							if (!accIdToBillingAccMap.get(stockAccountId).Trial__c && accIdToBillingAccMap.get(stockAccountId).Billable__c) {
								if (accIdToBillingAccMap.get(stockAccountId).LastModifiedDate > sim.Activation_Date__c) {
									sim.Commercial_Date__c= accIdToBillingAccMap.get(stockAccountId).LastModifiedDate;
								} else {
									sim.Commercial_Date__c = sim.Activation_Date__c;
								}
							}
						}
					}
				}
			}
			if (sim.Commercial_Date__c==null && sim.Activation_Date__c!=null) {
				sim.Commercial_Date__c = sim.Activation_Date__c;
			}
			newStockList.add(sim);
		}
		return newStockList;
	}
}