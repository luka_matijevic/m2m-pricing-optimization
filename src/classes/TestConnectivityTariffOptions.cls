//------------------------------------------------------------------------------
// Tests for ConnectivityTariffOptions class
//------------------------------------------------------------------------------
@isTest
private class TestConnectivityTariffOptions
{
	//--------------------------------------------------------------------------
	// Test retrieving tariff data
	//--------------------------------------------------------------------------
	static testMethod void testRetrievingTariffOptions()
	{
		//
		// Create test tariff data
		//
		TestHelper.createTariffs();
		Id tariffId = [SELECT Id FROM Tariff__c WHERE Parent_Tariff__c = null].Id;

		//
		// Start test
		//
		Test.startTest();

		IConnectivityTariffOptions connectivityTariffOptions =
			Factory.createConnectivityTariffOptions(tariffId, null, null);
		List<ITariffOption> tariffOptions =
			connectivityTariffOptions.getTariffOptions();
		List<ITariffOption> includedTariffOptions =
			connectivityTariffOptions.getIncludedTariffOptions();
		List<ITariffOption> additionalTariffOptions =
			connectivityTariffOptions.getAdditionalTariffOptions();
		List<ITariffOption> supplementaryServiceTariffOptions =
			connectivityTariffOptions.getSupplementaryServiceTariffOptions();

		//
		// Stop test
		//
		Test.stopTest();

		//
		// Test tariff options
		//
		//
		// Test tariff options
		//
		TestHelper.checkTariffOptions(tariffOptions,
				includedTariffOptions,
				additionalTariffOptions,
				supplementaryServiceTariffOptions);
	}

	//--------------------------------------------------------------------------
	// Test getters after cloning without configuration changes
	//--------------------------------------------------------------------------
	static testMethod void testGettersAfterClonningWithoutChanges()
	{
		//
		// Create test tariff data
		//
		TestHelper.createTariffs();
		Id tariffId = [SELECT Id FROM Tariff__c WHERE Parent_Tariff__c = null].Id;

		//
		// Start test
		//
		Test.startTest();

		IConnectivityTariffOptions connectivityTariffOptions =
			Factory.createConnectivityTariffOptions(tariffId, null, null);

		connectivityTariffOptions.cloneTariff('24');
		//
		// Stop test
		//
		Test.stopTest();

		system.assertEquals(false, connectivityTariffOptions.getCommercialApprovalRequired());
		system.assertEquals(null, connectivityTariffOptions.getTotalExpectedDataOverUsage());
		system.assertEquals(null, connectivityTariffOptions.getTotalExpectedTextOverUsage());
		system.assertEquals(null, connectivityTariffOptions.getTotalExpectedVoiceOverUsage());
	}

	//--------------------------------------------------------------------------
	// Test getters after cloning with configuration changes
	//--------------------------------------------------------------------------
	static testMethod void testGettersAfterCloningWithChanges()
	{
		TestHelper.createTariffs();
		Id tariffId = [SELECT Id FROM Tariff__c WHERE Parent_Tariff__c != null].Id;

		//
		// Start test
		//
		Test.startTest();

		IConnectivityTariffOptions connectivityTariffOptions =
			Factory.createConnectivityTariffOptions(tariffId, null, null);

		List<ITariffOption> includedTariffOptions =
				connectivityTariffOptions.getIncludedTariffOptions();

		includedTariffOptions[0].getRateCards()[0].getRateCard().Target_Usage_Fee__c = 43.54;
		includedTariffOptions[0].getRateCards()[0].getRateCard().Expected_Usage__c = 54.13;

		connectivityTariffOptions.cloneTariff('24');
		//
		// Stop test
		//
		Test.stopTest();

		system.assertEquals(true, connectivityTariffOptions.getCommercialApprovalRequired());
		system.assertEquals(2356.8202, connectivityTariffOptions.getTotalExpectedDataOverUsage());
		system.assertEquals(null, connectivityTariffOptions.getTotalExpectedTextOverUsage());
		system.assertEquals(null, connectivityTariffOptions.getTotalExpectedVoiceOverUsage());
	}

	//--------------------------------------------------------------------------
	// Test configuring and cloning of tariff options
	//--------------------------------------------------------------------------
	static testMethod void testConfiguringAndCloningTariff()
	{
		//
		// Create test tariff data
		//
		TestHelper.createTariffs();
		Id tariffId = [SELECT Id FROM Tariff__c WHERE Parent_Tariff__c = null].Id;

		//
		// Start test
		//
		Test.startTest();
		//
		// Retrieve tariff options
		//
		IConnectivityTariffOptions connectivityTariffOptions =
			Factory.createConnectivityTariffOptions(tariffId, null, null);

		List<ITariffOption> includedTariffOptions =
			connectivityTariffOptions.getIncludedTariffOptions();
		List<ITariffOption> additionalTariffOptions =
			connectivityTariffOptions.getAdditionalTariffOptions();
		List<ITariffOption> supplementaryServiceTariffOptions =
			connectivityTariffOptions.getSupplementaryServiceTariffOptions();
		//
		// Configure tariff options
		//
		configureTariffOptions(connectivityTariffOptions);
		//
		// Clone tariff with configuration
		//
		Id clonedTariffId = connectivityTariffOptions.cloneTariff('6');

		//
		// Stop test
		//
		Test.stopTest();

		//
		// Check cloned tariff
		//
		checkClonedTariff(clonedTariffId, tariffId, 'Test tariff', null, '6');
	}

	//--------------------------------------------------------------------------
	// Test configuring and cloning of tariff options with account Id
	//--------------------------------------------------------------------------
	static testMethod void testConfiguringAndCloningTariffWithAccount()
	{
        TestHelper.createTariffs();
		Id tariffId = [SELECT Id FROM Tariff__c WHERE Parent_Tariff__c = null].Id;

		TestHelper.createAccount();
		Id accountId = [SELECT Id FROM Account].Id;

		//
		// Start test
		//
		Test.startTest();
		//
		// Retrieve tariff options
		//
		IConnectivityTariffOptions connectivityTariffOptions =
			Factory.createConnectivityTariffOptions(tariffId, accountId, null);

		List<ITariffOption> includedTariffOptions =
			connectivityTariffOptions.getIncludedTariffOptions();
		List<ITariffOption> additionalTariffOptions =
			connectivityTariffOptions.getAdditionalTariffOptions();
		List<ITariffOption> supplementaryServiceTariffOptions =
			connectivityTariffOptions.getSupplementaryServiceTariffOptions();
		//
		// Configure tariff options
		//
		configureTariffOptions(connectivityTariffOptions);
		//
		// Clone tariff with configuration
		//
		Id clonedTariffId = connectivityTariffOptions.cloneTariff('24');

		//
		// Stop test
		//
		Test.stopTest();

		//
		// Check cloned tariff
		//
		checkClonedTariff(clonedTariffId, tariffId, 'Test tariff for AP Test Acct', accountId, '24');
	}

	//--------------------------------------------------------------------------
	// Test retrieving of cloned tariff options
	//--------------------------------------------------------------------------
	static testMethod void testRetrievingClonedTariffOptions()
	{
		//
		// Create test tariff data
		//
		TestHelper.createTariffs();
		Id tariffId = [SELECT Id FROM Tariff__c WHERE Parent_Tariff__c = null].Id;

		//
		// Start test
		//
		Test.startTest();
		//
		// Retrieve tariff options
		//
		IConnectivityTariffOptions connectivityTariffOptions =
			Factory.createConnectivityTariffOptions(tariffId, null, null);

		List<ITariffOption> includedTariffOptions =
			connectivityTariffOptions.getIncludedTariffOptions();
		List<ITariffOption> additionalTariffOptions =
			connectivityTariffOptions.getAdditionalTariffOptions();
		List<ITariffOption> supplementaryServiceTariffOptions =
			connectivityTariffOptions.getSupplementaryServiceTariffOptions();
		//
		// Configure tariff options
		//
		configureTariffOptions(connectivityTariffOptions);
		//
		// Clone tariff with configuration
		//
		Id clonedTariffId = connectivityTariffOptions.cloneTariff('24');

		connectivityTariffOptions = Factory.createConnectivityTariffOptions(clonedTariffId, null, null);
		List<ITariffOption> tariffOptions =
			connectivityTariffOptions.getTariffOptions();
		includedTariffOptions =
			connectivityTariffOptions.getIncludedTariffOptions();
		additionalTariffOptions =
			connectivityTariffOptions.getAdditionalTariffOptions();
		supplementaryServiceTariffOptions =
			connectivityTariffOptions.getSupplementaryServiceTariffOptions();
		//
		// Stop test
		//
		Test.stopTest();
		//
		// Test tariff options
		//
		TestHelper.checkClonedTariffOptions(tariffOptions,
				includedTariffOptions,
				additionalTariffOptions,
				supplementaryServiceTariffOptions);
	}

	//--------------------------------------------------------------------------
	// Create configurations for additional options and supplementary services
	//--------------------------------------------------------------------------
	static testMethod void testCreateProductConfigurations()
	{/* Commented whole method - pavan
		//
		// Create test data
		//
		TestHelper.createTariffs();
		Id tariffId = [SELECT Id FROM Tariff__c WHERE Parent_Tariff__c = null].Id;

		//
		// Create product definitions
		//
		TestHelper.createProductDefinitions();

		//
		// Start test
		//
		Test.startTest();

		//
		// Retrieve tariff options
		//
		IConnectivityTariffOptions connectivityTariffOptions =
			Factory.createConnectivityTariffOptions(tariffId, null, null);

		List<ITariffOption> includedTariffOptions =
			connectivityTariffOptions.getIncludedTariffOptions();
		List<ITariffOption> additionalTariffOptions =
			connectivityTariffOptions.getAdditionalTariffOptions();
		List<ITariffOption> supplementaryServiceTariffOptions =
			connectivityTariffOptions.getSupplementaryServiceTariffOptions();
		//
		// Configure tariff options
		//
		configureTariffOptions(connectivityTariffOptions);

		//
		// Check product definitions
		//
		List<cscfga__Product_Definition__c> productDefinitions =
			[ SELECT Id, Name,
			  ( SELECT Id, Name
				FROM cscfga__Attribute_Definitions__r
				ORDER BY Name ASC)
			  FROM cscfga__Product_Definition__c
			  ORDER BY Name ASC];
		system.assertEquals(2, productDefinitions.size());
		system.assertEquals('Tariff Additional Service', productDefinitions[0].Name);
		system.assertEquals(11, productDefinitions[0].cscfga__Attribute_Definitions__r.size());
		system.assertEquals('Tariff Supplementary Service', productDefinitions[1].Name);
		system.assertEquals(10, productDefinitions[1].cscfga__Attribute_Definitions__r.size());

		Id clonedTariffId = connectivityTariffOptions.cloneTariff('24');
		//String productKey = connectivityTariffOptions.createConfigurations();

		//
		// Stop test
		//
		Test.stopTest();

		//TestHelper.checkCreatedProductConfigurations(clonedTariffId, productKey);
	*/}

	//--------------------------------------------------------------------------
	// Do not clone tariff if configuration is the same
	//--------------------------------------------------------------------------
	static testMethod void testNotCloninigIfConfigurationIsTheSame()
	{
	}

	//--------------------------------------------------------------------------
	// Test retrieving cloned tariff options with existing configuration
	//--------------------------------------------------------------------------
	static testMethod void testRetrievingCloneTariffOptionsWithConfiguration()
	{
	}

	//--------------------------------------------------------------------------
	// Test creating product configurations with existing configuration
	//--------------------------------------------------------------------------
	static testMethod void testCreateConfigurationWithConfiguration()
	{
	}

	static void configureTariffOptions(IConnectivityTariffOptions connectivityTariffOptions)
	{
		List<ITariffOption> includedTariffOptions =
			connectivityTariffOptions.getIncludedTariffOptions();
		List<ITariffOption> additionalTariffOptions =
			ConnectivityTariffOptions.getAdditionalTariffOptions();
		List<ITariffOption> supplementaryServiceTariffOptions =
			ConnectivityTariffOptions.getSupplementaryServiceTariffOptions();
		//
		// Configure tariff options
		//
		//
		// Included tariff options
		//
		includedTariffOptions[0].getTariffOption().Show_on_Quote__c = true;
		includedTariffOptions[0].getTariffOption().Recurring_Target_Fee__c = 13.49;
		includedTariffOptions[0].getRateCards()[0].getRateCard().Show_on_Quote__c = false;
		//
		// Additional tariff options
		//
		additionalTariffOptions[0].setIsSelected(false);
		additionalTariffOptions[0].getTariffOption().Show_on_Quote__c = false;
		additionalTariffOptions[0].getTariffOption().Recurring_Target_Fee__c = 15.64;

		additionalTariffOptions[1].setIsSelected(true);
		additionalTariffOptions[1].setQuantity(3);
		additionalTariffOptions[1].getTariffOption().Show_on_Quote__c = true;
		additionalTariffOptions[1].getRateCards()[0].getRateCard().Show_on_Quote__c = false;
		additionalTariffOptions[1].getRateCards()[0].getRateCard().Target_Usage_Fee__c = 24.50;
		additionalTariffOptions[1].getRateCards()[0].getRateCard().Expected_Usage__c = 200.43;
		additionalTariffOptions[1].getRateCards()[1].getRateCard().Show_on_Quote__c = true;
		additionalTariffOptions[1].getRateCards()[2].getRateCard().Show_on_Quote__c = true;
		additionalTariffOptions[1].getRateCards()[2].getRateCard().Target_Usage_Fee__c = 13.59;
		additionalTariffOptions[1].getRateCards()[2].getRateCard().Expected_Usage__c = 1000.43;
		//
		// Supplementary service tariff options
		//
		supplementaryServiceTariffOptions[0].setIsSelected(true);
		supplementaryServiceTariffOptions[0].setQuantity(49);
		supplementaryServiceTariffOptions[0].getServiceFees()[0].getServiceFee().Show_on_Quote__c = true;
		supplementaryServiceTariffOptions[0].getServiceFees()[0].getServiceFee().Recurring_Target_Fee__c = 23.43;
		supplementaryServiceTariffOptions[0].getServiceFees()[0].getServiceFee().One_Time_Target_Fee__c = 12.43;
		supplementaryServiceTariffOptions[0].getServiceFees()[1].getServiceFee().Show_on_Quote__c = true;
		supplementaryServiceTariffOptions[0].getServiceFees()[1].getServiceFee().Recurring_Target_Fee__c = 23.43;
		supplementaryServiceTariffOptions[0].getServiceFees()[1].getServiceFee().One_Time_Target_Fee__c = 12.43;
	}

	//--------------------------------------------------------------------------
	// Check cloned tariff
	//--------------------------------------------------------------------------
	static void checkClonedTariff(Id clonedTariffId, Id tariffId, String tariffName, Id accountId, String contractTerm)
	{
		//
		// Check cloned tariff
		//
		Tariff__c tariff = [SELECT Id, Name, Code__c, Commercial_Plan__c,
				  Contract_Term__c, Description_DE__c, Description_ENG__c,
				  Global_Local__c, Group__c, One_Off_Price__c, Account__c,
				  Recurring_Price__c, Product_Family__c, Sales_Channel__c,
				  Status__c, Volume_Start__c, Volume_End__c, Parent_Tariff__c
				  FROM Tariff__c
				  WHERE Id = :clonedTariffId];
		system.assertEquals(tariffName, tariff.Name);
		system.assertEquals(accountId, tariff.Account__c);
		system.assertEquals('code', tariff.Code__c);
		system.assertEquals('commercial plan', tariff.Commercial_Plan__c);
		system.assertEquals(contractTerm, tariff.Contract_Term__c);
		system.assertEquals('german description', tariff.Description_DE__c);
		system.assertEquals('english description', tariff.Description_ENG__c);
		system.assertEquals('Global', tariff.Global_Local__c);
		system.assertEquals('group', tariff.Group__c);
		system.assertEquals(12.00, tariff.One_Off_Price__c);
		system.assertEquals(6.00, tariff.Recurring_Price__c);
		system.assertEquals('Connectivity', tariff.Product_Family__c);
		system.assertEquals('Direct', tariff.Sales_Channel__c);
		system.assertEquals('Draft', tariff.Status__c);
		system.assertEquals(1.00, tariff.Volume_Start__c);
		system.assertEquals(100.00, tariff.Volume_End__c);
		system.assertEquals(tariffId, tariff.Parent_Tariff__c);
		//
		// Check cloned tariff options
		//

		
		// Included tariff options
		//
		List<Tariff_Option__c> included = [ SELECT Id, Name, Can_Discount__c,
				Clock_Rate__c, Commercial_Plan__c, Description_DE__c,
				Description_ENG__c, No_of_Included_Units__c,
				Recurring_Base_Fee__c,
				Recurring_Target_Fee__c, Sequence_No__c, Show_on_Quote__c,
				Tariff__c, Type__c, Usage_Unit__c,
				( SELECT Id, Name, Base_Usage_Fee__c, Can_Discount__c,
				  Clock_Rate__c, Description_DE__c, Description_ENG__c,
				  Direction__c, Expected_Usage__c, Sequence_No__c,
				  Show_on_Quote__c, Target_Usage_Fee__c, Tariff_Option__c,
				  Worldzone__c
				  FROM Rate_Cards__r
				  ORDER BY Sequence_No__c ASC),
				( SELECT Id, Name, Can_Discount__c, Charged_Per__c,
				  Description_DE__c, Description_ENG__c, One_Time_Base_Fee__c,
				  One_Time_Target_Fee__c, Recurring_Base_Fee__c,
				  Recurring_Target_Fee__c, Sequence_No__c, Show_on_Quote__c,
				  Tariff_Option__c
				  FROM Service_Fee__r
				  ORDER BY Sequence_No__c ASC)
				FROM Tariff_Option__c
				WHERE Tariff__c = :clonedTariffId AND Type__c = 'Included'];
		//
		// Check tariff option
		//
		system.assertEquals(1, included.size());
		system.assertEquals('Tariff Option Included', included[0].Name);
		system.assertEquals(true, included[0].Can_Discount__c);
		system.assertEquals('MB', included[0].Clock_Rate__c);
		system.assertEquals('commercial plan', included[0].Commercial_Plan__c);
		system.assertEquals('german description', included[0].Description_DE__c);
		system.assertEquals('english description', included[0].Description_ENG__c);
		system.assertEquals(100, included[0].No_of_Included_Units__c);
		system.assertEquals(19.4, included[0].Recurring_Base_Fee__c);
		system.assertEquals(13.49, included[0].Recurring_Target_Fee__c);
		system.assertEquals(10, included[0].Sequence_No__c);
		system.assertEquals(true, included[0].Show_on_Quote__c);
		system.assertEquals('MB', included[0].Usage_Unit__c);
		//
		// Check rate cards
		//
		system.assertEquals(1, included[0].Rate_Cards__r.size());
		system.assertEquals('Included Rate Card', included[0].Rate_Cards__r[0].Name);
		system.assertEquals(10.54, included[0].Rate_Cards__r[0].Base_Usage_Fee__c);
		system.assertEquals(true, included[0].Rate_Cards__r[0].Can_Discount__c);
		system.assertEquals('MB', included[0].Rate_Cards__r[0].Clock_Rate__c);
		system.assertEquals('german rate card description', included[0].Rate_Cards__r[0].Description_DE__c);
		system.assertEquals('english rate card description', included[0].Rate_Cards__r[0].Description_ENG__c);
		system.assertEquals('Incoming', included[0].Rate_Cards__r[0].Direction__c);
		system.assertEquals(null, included[0].Rate_Cards__r[0].Expected_Usage__c);
		system.assertEquals(30, included[0].Rate_Cards__r[0].Sequence_No__c);
		system.assertEquals(false, included[0].Rate_Cards__r[0].Show_on_Quote__c);
		system.assertEquals(10.54, included[0].Rate_Cards__r[0].Target_Usage_Fee__c);
		system.assertEquals('worldzone', included[0].Rate_Cards__r[0].Worldzone__c);
		//
		// Check service fees
		//
		system.assertEquals(0, included[0].Service_Fee__r.size());

		//
		// Additional tariff options
		//
		List<Tariff_Option__c> additional = [ SELECT Id, Name, Can_Discount__c,
				Clock_Rate__c, Commercial_Plan__c, Description_DE__c,
				Description_ENG__c, No_of_Included_Units__c,
				Recurring_Base_Fee__c,
				Recurring_Target_Fee__c, Sequence_No__c, Show_on_Quote__c,
				Tariff__c, Type__c, Usage_Unit__c,
				( SELECT Id, Name, Base_Usage_Fee__c, Can_Discount__c,
				  Clock_Rate__c, Description_DE__c, Description_ENG__c,
				  Direction__c, Expected_Usage__c, Sequence_No__c,
				  Show_on_Quote__c, Target_Usage_Fee__c, Tariff_Option__c,
				  Worldzone__c
				  FROM Rate_Cards__r
				  ORDER BY Sequence_No__c ASC),
				( SELECT Id, Name, Can_Discount__c, Charged_Per__c,
				  Description_DE__c, Description_ENG__c, One_Time_Base_Fee__c,
				  One_Time_Target_Fee__c, Recurring_Base_Fee__c,
				  Recurring_Target_Fee__c, Sequence_No__c, Show_on_Quote__c,
				  Tariff_Option__c
				  FROM Service_Fee__r
				  ORDER BY Sequence_No__c ASC)
				FROM Tariff_Option__c
				WHERE Tariff__c = :clonedTariffId AND Type__c = 'Additional'];
		//
		// Check tariff option
		//
		system.assertEquals(1, additional.size());
		system.assertEquals('Tariff Option Additional 1', additional[0].Name);
		system.assertEquals(true, additional[0].Can_Discount__c);
		system.assertEquals('Min', additional[0].Clock_Rate__c);
		system.assertEquals('commercial plan', additional[0].Commercial_Plan__c);
		system.assertEquals('german description', additional[0].Description_DE__c);
		system.assertEquals('english description', additional[0].Description_ENG__c);
		system.assertEquals(40, additional[0].No_of_Included_Units__c);
		system.assertEquals(24.4, additional[0].Recurring_Base_Fee__c);
		system.assertEquals(24.4, additional[0].Recurring_Target_Fee__c);
		system.assertEquals(20, additional[0].Sequence_No__c);
		system.assertEquals(true, additional[0].Show_on_Quote__c);
		system.assertEquals('Min', additional[0].Usage_Unit__c);
		//
		// Check rate cards
		//
		system.assertEquals(3, additional[0].Rate_Cards__r.size());
		//
		// First rate card
		//
		system.assertEquals('Additional 1 Rate Card 2', additional[0].Rate_Cards__r[0].Name);
		system.assertEquals(12.56, additional[0].Rate_Cards__r[0].Base_Usage_Fee__c);
		system.assertEquals(false, additional[0].Rate_Cards__r[0].Can_Discount__c);
		system.assertEquals('SMS', additional[0].Rate_Cards__r[0].Clock_Rate__c);
		system.assertEquals('german rate card description', additional[0].Rate_Cards__r[0].Description_DE__c);
		system.assertEquals('english rate card description', additional[0].Rate_Cards__r[0].Description_ENG__c);
		system.assertEquals('Incoming', additional[0].Rate_Cards__r[0].Direction__c);
		system.assertEquals(200.43, additional[0].Rate_Cards__r[0].Expected_Usage__c);
		system.assertEquals(10, additional[0].Rate_Cards__r[0].Sequence_No__c);
		system.assertEquals(false, additional[0].Rate_Cards__r[0].Show_on_Quote__c);
		system.assertEquals(24.50, additional[0].Rate_Cards__r[0].Target_Usage_Fee__c);
		system.assertEquals('worldzone', additional[0].Rate_Cards__r[0].Worldzone__c);
		//
		// Second rate card
		//
		system.assertEquals('Additional 1 Rate Card 1', additional[0].Rate_Cards__r[1].Name);
		system.assertEquals(43.54, additional[0].Rate_Cards__r[1].Base_Usage_Fee__c);
		system.assertEquals(true, additional[0].Rate_Cards__r[1].Can_Discount__c);
		system.assertEquals('Min', additional[0].Rate_Cards__r[1].Clock_Rate__c);
		system.assertEquals('german rate card description', additional[0].Rate_Cards__r[1].Description_DE__c);
		system.assertEquals('english rate card description', additional[0].Rate_Cards__r[1].Description_ENG__c);
		system.assertEquals('Outgoing', additional[0].Rate_Cards__r[1].Direction__c);
		system.assertEquals(null, additional[0].Rate_Cards__r[1].Expected_Usage__c);
		system.assertEquals(20, additional[0].Rate_Cards__r[1].Sequence_No__c);
		system.assertEquals(true, additional[0].Rate_Cards__r[1].Show_on_Quote__c);
		system.assertEquals(43.54, additional[0].Rate_Cards__r[1].Target_Usage_Fee__c);
		system.assertEquals('worldzone', additional[0].Rate_Cards__r[1].Worldzone__c);
		//
		// Third rate card
		//
		system.assertEquals('Additional 1 Rate Card 3', additional[0].Rate_Cards__r[2].Name);
		system.assertEquals(17.91, additional[0].Rate_Cards__r[2].Base_Usage_Fee__c);
		system.assertEquals(true, additional[0].Rate_Cards__r[2].Can_Discount__c);
		system.assertEquals('SMS', additional[0].Rate_Cards__r[2].Clock_Rate__c);
		system.assertEquals('german rate card description', additional[0].Rate_Cards__r[2].Description_DE__c);
		system.assertEquals('english rate card description', additional[0].Rate_Cards__r[2].Description_ENG__c);
		system.assertEquals('Outgoing', additional[0].Rate_Cards__r[2].Direction__c);
		system.assertEquals(1000.43, additional[0].Rate_Cards__r[2].Expected_Usage__c);
		system.assertEquals(30, additional[0].Rate_Cards__r[2].Sequence_No__c);
		system.assertEquals(true, additional[0].Rate_Cards__r[2].Show_on_Quote__c);
		system.assertEquals(13.59, additional[0].Rate_Cards__r[2].Target_Usage_Fee__c);
		system.assertEquals('worldzone', additional[0].Rate_Cards__r[2].Worldzone__c);
		//
		// Check service fees
		//
		system.assertEquals(0, additional[0].Service_Fee__r.size());

		//
		// Supplementary service tariff options
		//
		List<Tariff_Option__c> service = [ SELECT Id, Name, Can_Discount__c,
				Clock_Rate__c, Commercial_Plan__c, Description_DE__c,
				Description_ENG__c, No_of_Included_Units__c,
				Recurring_Base_Fee__c, Recurring_Target_Fee__c, Sequence_No__c,
				Show_on_Quote__c, Tariff__c, Type__c, Usage_Unit__c,
				( SELECT Id, Name, Base_Usage_Fee__c, Can_Discount__c,
				  Clock_Rate__c, Description_DE__c, Description_ENG__c,
				  Direction__c, Expected_Usage__c, Sequence_No__c,
				  Show_on_Quote__c, Target_Usage_Fee__c, Tariff_Option__c,
				  Worldzone__c
				  FROM Rate_Cards__r
				  ORDER BY Sequence_No__c ASC),
				( SELECT Id, Name, Can_Discount__c, Charged_Per__c,
				  Description_DE__c, Description_ENG__c, One_Time_Base_Fee__c,
				  One_Time_Target_Fee__c, Recurring_Base_Fee__c,
				  Recurring_Target_Fee__c, Sequence_No__c, Show_on_Quote__c,
				  Tariff_Option__c
				  FROM Service_Fee__r
				  ORDER BY Sequence_No__c ASC)
				FROM Tariff_Option__c
				WHERE Tariff__c = :clonedTariffId AND Type__c = 'Supplementary Service'];
		//
		// Check tariff option
		//
		system.assertEquals(1, service.size());
		system.assertEquals('Tariff Option Service', service[0].Name);
		system.assertEquals(false, service[0].Can_Discount__c);
		system.assertEquals('SMS', service[0].Clock_Rate__c);
		system.assertEquals('commercial plan', service[0].Commercial_Plan__c);
		system.assertEquals('german description', service[0].Description_DE__c);
		system.assertEquals('english description', service[0].Description_ENG__c);
		system.assertEquals(32, service[0].No_of_Included_Units__c);
		system.assertEquals(13.9, service[0].Recurring_Base_Fee__c);
		system.assertEquals(13.9, service[0].Recurring_Target_Fee__c);
		system.assertEquals(10, service[0].Sequence_No__c);
		system.assertEquals(true, service[0].Show_on_Quote__c);
		system.assertEquals('SMS', service[0].Usage_Unit__c);
		//
		// Check rate cards
		//
		system.assertEquals(1, service[0].Rate_Cards__r.size());
		system.assertEquals('Service Rate Card 1', service[0].Rate_Cards__r[0].Name);
		system.assertEquals(24.43, service[0].Rate_Cards__r[0].Base_Usage_Fee__c);
		system.assertEquals(false, service[0].Rate_Cards__r[0].Can_Discount__c);
		system.assertEquals('Min', service[0].Rate_Cards__r[0].Clock_Rate__c);
		system.assertEquals('german rate card description', service[0].Rate_Cards__r[0].Description_DE__c);
		system.assertEquals('english rate card description', service[0].Rate_Cards__r[0].Description_ENG__c);
		system.assertEquals('N/A', service[0].Rate_Cards__r[0].Direction__c);
		system.assertEquals(null, service[0].Rate_Cards__r[0].Expected_Usage__c);
		system.assertEquals(20, service[0].Rate_Cards__r[0].Sequence_No__c);
		system.assertEquals(false, service[0].Rate_Cards__r[0].Show_on_Quote__c);
		system.assertEquals(24.43, service[0].Rate_Cards__r[0].Target_Usage_Fee__c);
		system.assertEquals('worldzone', service[0].Rate_Cards__r[0].Worldzone__c);
		//
		// Check service fees
		//
		system.assertEquals(2, service[0].Service_Fee__r.size());
		//
		// First service fee
		//
		system.assertEquals('Service Fee 2', service[0].Service_Fee__r[0].Name);
		system.assertEquals(false, service[0].Service_Fee__r[0].Can_Discount__c);
		system.assertEquals('Volume', service[0].Service_Fee__r[0].Charged_Per__c);
		system.assertEquals('german service fee description', service[0].Service_Fee__r[0].Description_DE__c);
		system.assertEquals('english service fee description', service[0].Service_Fee__r[0].Description_ENG__c);
		system.assertEquals(42.51, service[0].Service_Fee__r[0].One_Time_Base_Fee__c);
		system.assertEquals(12.43, service[0].Service_Fee__r[0].One_Time_Target_Fee__c);
		system.assertEquals(12.39, service[0].Service_Fee__r[0].Recurring_Base_Fee__c);
		system.assertEquals(23.43, service[0].Service_Fee__r[0].Recurring_Target_Fee__c);
		system.assertEquals(10, service[0].Service_Fee__r[0].Sequence_No__c);
		system.assertEquals(true, service[0].Service_Fee__r[0].Show_on_Quote__c);
		//
		// Second service fee
		//
		system.assertEquals('Service Fee 1', service[0].Service_Fee__r[1].Name);
		system.assertEquals(true, service[0].Service_Fee__r[1].Can_Discount__c);
		system.assertEquals('Customer', service[0].Service_Fee__r[1].Charged_Per__c);
		system.assertEquals('german service fee description', service[0].Service_Fee__r[1].Description_DE__c);
		system.assertEquals('english service fee description', service[0].Service_Fee__r[1].Description_ENG__c);
		system.assertEquals(12.39, service[0].Service_Fee__r[1].One_Time_Base_Fee__c);
		system.assertEquals(12.43, service[0].Service_Fee__r[1].One_Time_Target_Fee__c);
		system.assertEquals(43.95, service[0].Service_Fee__r[1].Recurring_Base_Fee__c);
		system.assertEquals(23.43, service[0].Service_Fee__r[1].Recurring_Target_Fee__c);
		system.assertEquals(20, service[0].Service_Fee__r[1].Sequence_No__c);
		system.assertEquals(true, service[0].Service_Fee__r[1].Show_on_Quote__c);
	}
}