global class CustomerBillBatch implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts
{
    private Id dailyBillId;
    private List<Daily_Bill_Item__c> dailyBillItemList = new List<Daily_Bill_Item__c>();
    private Set<String> invoiceNumberSet = new Set<String>();
    private Map<Account, List<Daily_Bill_Item__c>> accountBillMap = new Map<Account, List<Daily_Bill_Item__c>>();
    private Map<String, String> invoiceAccountMap = new Map<String, String>();
    private Map<String, Account> accountMap = new Map<String, Account>();
    private List<Account> accountList = new List<Account>();
    private Boolean createHistoryData = false;
    private Date fromDate;
    private Date toDate;
    private Boolean forOneBillingAccount;
    private String billingAccountId;
    
    public CustomerBillBatch(Id dailyBillId, Boolean createHistoryData, Date fromDate, Date toDate, Boolean forOneBillingAccount, String billingAccountId) {
        this.dailyBillId = dailyBillId;
        this.createHistoryData = createHistoryData;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.forOneBillingAccount = forOneBillingAccount;
        this.billingAccountId = billingAccountId;
    }
    //---------------------------------------------------------------------------
    // Start of batch apex
    //---------------------------------------------------------------------------
    global Iterable<sObject> start(Database.BatchableContext bc)
    {
        //
        // Filter records
        //
        List<Prebill__c> prebillList = new List<Prebill__c>();
        
        if(forOneBillingAccount) {
            Map<String, String> monthDailyBillMap = new Map<String, String>();
            //List<Prebill__c> prebills = [SELECT Id, Invoice_Number__c, Billing_Account__c, Billing_Account__r.SM2M_Type__c, Billing_Account__r.SM2M_ID__c, Billing_Account__r.Name, Billing_Account__r.OFI_ID__c, Account_ID__c, Total_Gross_Amount__c, Total_Net_Amount__c, Total_Tax_Amount__c, Daily_Bill__c, Daily_Bill__r.Date__c FROM Prebill__c WHERE Billing_Account__c = :billingAccountId ORDER BY Daily_Bill__r.Date__c DESC];
            
            List<Daily_Bill_Item__c> dailyBillItemList = [SELECT Id, Daily_Bill__c, Art__c, Beschreibung__c, Account__c, Rechnungsnummer__c, Anweisung__c, Steuersatz__c, Einzel_Nettobetrag__c, Einzel_Taxbetrag__c, Einzel_Bruttobetrag__c, VO_Nr__c, Menge__c, Daily_Bill__r.Date__c FROM Daily_Bill_Item__c WHERE VO_Nr__c = :billingAccountId ORDER BY Daily_Bill__r.Date__c DESC];
            
            if(dailyBillItemList != null && dailyBillItemList.size() > 0) {
                Integer monthsFound = 0;
                for(Daily_Bill_Item__c pr : dailyBillItemList) {
                    if(monthsFound == 6) {
                    	break;    
                    }
                    
                    String gsItem = '';
                    
                    if(pr.Beschreibung__c != null && pr.Beschreibung__c.startsWith('GS -')) {
                        //System.debug(LoggingLevel.INFO, 'found GS - ' + pr.Id);
                        gsItem = 'GS';
                    }
                    
                    String key = gsItem + String.valueOf(pr.Daily_Bill__r.Date__c.month()) + String.valueOf(pr.Daily_Bill__r.Date__c.year()) + pr.Rechnungsnummer__c; /*LM 21.07.2016 - added pr.Rechnungsnummer__c*/
                    if(!monthDailyBillMap.containsKey(key)) {
                        monthDailyBillMap.put(key, pr.Rechnungsnummer__c);
                        
                        if(gsItem.length() == 0) // increment counter only if we didnt find Gutschrift item, because we need gutschrift as a part of invoices
                        	monthsFound++;
                    }
                }
            }
            
            Set<String> invoiceNumbers = new Set<String>();
            
            for(String key : monthDailyBillMap.keySet()) {
                String p = monthDailyBillMap.get(key);
                invoiceNumbers.add(p);
            }
            
            for(String inv : invoiceNumbers) {
                Prebill__c preb = new Prebill__c();
                preb.Invoice_Number__c = inv;
                prebillList.add(preb);
            }
            
            //dbiList = 'SELECT Id, CreatedDate, Invoice_Number__c, Billing_Account__c, Billing_Account__r.SM2M_Type__c, Billing_Account__r.SM2M_ID__c, Billing_Account__r.Name, Billing_Account__r.OFI_ID__c, Account_ID__c, Total_Gross_Amount__c, Total_Net_Amount__c, Total_Tax_Amount__c, Daily_Bill__c, Daily_Bill__r.Date__c FROM Prebill__c WHERE Id = :prebillIds';
        } else {
            if(createHistoryData) {
    			//dbiList = 'SELECT Id, Invoice_Number__c, Billing_Account__c, Billing_Account__r.SM2M_Type__c, Billing_Account__r.SM2M_ID__c, Billing_Account__r.Name, Billing_Account__r.OFI_ID__c, Account_ID__c, Total_Gross_Amount__c, Total_Net_Amount__c, Total_Tax_Amount__c, Daily_Bill__c, Daily_Bill__r.Real_Daily_Bill__c, Daily_Bill__r.Prebill_Requests_Sent__c, Daily_Bill__r.Date__c FROM Prebill__c WHERE Daily_Bill__r.Real_Daily_Bill__c = true AND Daily_Bill__r.Prebill_Requests_Sent__c > 0 AND Daily_Bill__r.Date__c >= :fromDate AND Daily_Bill__r.Date__c <= :toDate';
                List<Daily_Bill_Item__c> dailyBillItemList = [SELECT Id, Daily_Bill__c, Art__c, Beschreibung__c, Account__c, Rechnungsnummer__c, Anweisung__c, Steuersatz__c, Einzel_Nettobetrag__c, Einzel_Taxbetrag__c, Einzel_Bruttobetrag__c, VO_Nr__c, Menge__c, Daily_Bill__r.Date__c, Daily_Bill__r.Real_Daily_Bill__c, Daily_Bill__r.Prebill_Requests_Sent__c FROM Daily_Bill_Item__c WHERE Daily_Bill__r.Real_Daily_Bill__c = true AND Daily_Bill__r.Prebill_Requests_Sent__c > 0 AND Daily_Bill__r.Date__c >= :fromDate AND Daily_Bill__r.Date__c <= :toDate];
                
                Set<String> invoiceNumbers = new Set<String>();
                
                for(Daily_Bill_Item__c dbi : dailyBillItemList) {
                    invoiceNumbers.add(dbi.Rechnungsnummer__c);
                }
                
                for(String inv : invoiceNumbers) {
                    Prebill__c preb = new Prebill__c();
                    preb.Invoice_Number__c = inv;
                    prebillList.add(preb);
                }
            } else {
    			//dbiList = 'SELECT Id, Invoice_Number__c, Billing_Account__c, Billing_Account__r.SM2M_Type__c, Billing_Account__r.SM2M_ID__c, Billing_Account__r.Name, Billing_Account__r.OFI_ID__c, Account_ID__c, Total_Gross_Amount__c, Total_Net_Amount__c, Total_Tax_Amount__c, Daily_Bill__c, Daily_Bill__r.Date__c FROM Prebill__c WHERE Daily_Bill__c = :dailyBillId';
    			List<Daily_Bill_Item__c> dailyBillItemList = [SELECT Id, Daily_Bill__c, Art__c, Beschreibung__c, Account__c, Rechnungsnummer__c, Anweisung__c, Steuersatz__c, Einzel_Nettobetrag__c, Einzel_Taxbetrag__c, Einzel_Bruttobetrag__c, VO_Nr__c, Menge__c, Daily_Bill__r.Date__c, Daily_Bill__r.Real_Daily_Bill__c, Daily_Bill__r.Prebill_Requests_Sent__c FROM Daily_Bill_Item__c WHERE Daily_Bill__c = :dailyBillId];
                
                Set<String> invoiceNumbers = new Set<String>();
                
                for(Daily_Bill_Item__c dbi : dailyBillItemList) {
                    invoiceNumbers.add(dbi.Rechnungsnummer__c);
                }
                
                for(String inv : invoiceNumbers) {
                    Prebill__c preb = new Prebill__c();
                    preb.Invoice_Number__c = inv;
                    prebillList.add(preb);
                }
            }
        }
        
        for(Prebill__c pre : prebillList) {
            system.debug('pre.Invoice ' + pre.Invoice_Number__c);
        }
        
        return prebillList;
    }

    //--------------------------------------------------------------------------
    // Execute of batch apex
    //--------------------------------------------------------------------------
    global void execute(Database.BatchableContext bc, List<SObject> scope)
    {
        for (Prebill__c preb : (List<Prebill__c>)scope)
        {            
            PageReference pdf = Page.CustomerBill;
            pdf.getParameters().put('preb', preb.Invoice_Number__c);
            
            Blob body;
            String tmp, tmpBillingAcctAddition, tmpLocalOrGlobal;
            String tmpDate;

            try {
                body = pdf.getContentAsPDF();
                Prebill__c prebillToUse = new Prebill__c();
                Billing_Account__c baToUse = new Billing_Account__c();
                Daily_Bill_Item__c dbiToUse = new Daily_Bill_Item__c();
                
                List<Prebill__c> tmpPrebill = [SELECT Id, Invoice_Number__c, Daily_Bill__c, Billing_Account__c, Billing_Account__r.EVN_Report__c, Billing_Account__r.EDIFACT__c, Billing_Account__r.OFI_ID__c, Daily_Bill__r.Date__c, Billing_Account__r.SM2M_Type__c, Billing_Account__r.SM2M_ID__c FROM Prebill__c WHERE Invoice_Number__c = :preb.Invoice_Number__c];
                
                if(tmpPrebill != null && tmpPrebill.size() > 0) {
                    prebillToUse = tmpPrebill[0];
                    baToUse.OFI_ID__c = prebillToUse.Billing_Account__r.OFI_ID__c;                    
                    baToUse.SM2M_Type__c = prebillToUse.Billing_Account__r.SM2M_Type__c;
                    baToUse.SM2M_ID__c = prebillToUse.Billing_Account__r.SM2M_ID__c;
                    baToUse.Id = prebillToUse.Billing_Account__c;
                    baToUse.EVN_Report__c = prebillToUse.Billing_Account__r.EVN_Report__c;
                    baToUse.EDIFACT__c = prebillToUse.Billing_Account__r.EDIFACT__c;
                    
                }
                else {
                    List<Daily_Bill_Item__c> dailyBillItemList = [SELECT Id, Daily_Bill__c, Art__c, Beschreibung__c, Account__c, Rechnungsnummer__c, Anweisung__c, Steuersatz__c, Einzel_Nettobetrag__c, Einzel_Taxbetrag__c, Einzel_Bruttobetrag__c, VO_Nr__c, Menge__c, Daily_Bill__r.Date__c, Daily_Bill__r.Real_Daily_Bill__c, Daily_Bill__r.Prebill_Requests_Sent__c FROM Daily_Bill_Item__c WHERE Rechnungsnummer__c = :preb.Invoice_Number__c];
                    
                    if(dailyBillItemList != null && dailyBillItemList.size() > 0) {
                        dbiToUse = dailyBillItemList[0];
                        
                        List<Billing_Account__c> baList = [SELECT Id, OFI_ID__c, SM2M_Type__c, SM2M_ID__c, EDIFACT__c, EVN_Report__c FROM Billing_Account__c WHERE OFI_ID__c = :dbiToUse.VO_Nr__c];
                        baToUse = baList[0];
                        prebillToUse.Billing_Account__c = baToUse.Id;
                        prebillToUse.Invoice_Number__c = dbiToUse.Rechnungsnummer__c;
                        prebillToUse.Daily_Bill__r = dbiToUse.Daily_Bill__r;
                        //System.debug(LoggingLevel.INFO, 'dbiToUse.Daily_Bill__c ' + dbiToUse.Daily_Bill__c);
                        //prebillToUse.Daily_Bill__r.Date__c = dbiToUse.Daily_Bill__r.Date__c;
                    }
                }
                
                
                Customer_Monthly_Bill__c cmb = new Customer_Monthly_Bill__c();
                cmb.Invoice_Number__c = prebillToUse.Invoice_Number__c;
                cmb.Daily_Bill__c = prebillToUse.Daily_Bill__r.Id;
                cmb.Billing_Account__c = baToUse.Id;
                tmp = baToUse.OFI_ID__c; //Billing_Account__r.Name.replace(' ', '_');
                tmpDate = prebillToUse.Daily_Bill__r.Date__c.format().replace('.', '_');
                String sm2mType = baToUse.SM2M_Type__c;
                tmpLocalOrGlobal = sm2mType != null && !sm2mType.equalsIgnoreCase('null') ? sm2mType.equalsIgnoreCase('Local') ? 'TEFDEL' : 'TEFDEG' : '';
                String sm2mId = baToUse.SM2M_ID__c != null ? String.valueOf(baToUse.SM2M_ID__c.intValue()) : '';
                tmpBillingAcctAddition = String.format('{0}{1}', new String[] {sm2mId, !String.isEmpty(tmpLocalOrGlobal) ? '_' + tmpLocalOrGlobal : ''});
                
                cmb.PDFBlob__c = EncodingUtil.base64Encode(body);
                cmb.PDF_Bill_Link__c = Label.CustomerBillDownloadLink + tmp + '_' + tmpDate + '_' + tmpBillingAcctAddition + '_' + prebillToUse.Invoice_Number__c + '.pdf';
                
                /* additional reports */

                /* 35005564_01_07_2016_0_2nd_Report_TEFDEG.zip */
                cmb.Sim_Card_Summary__c = Label.SimCardSummaryDownloadLink + tmp + '_' + tmpDate + '_' + sm2mId + '_' + '2nd_Report_' + (sm2mType.equalsIgnoreCase('Local') ? 'TEFDEL' : 'TEFDEG') + '.zip';                
                System.debug('Id: ' + baToUse.Id);
                System.debug('EDIFACT__c: ' + baToUse.EDIFACT__c);
                System.debug('EVN_Report__c: ' + baToUse.EVN_Report__c);
                if (baToUse.EDIFACT__c){
                    /* 35005811_01_07_2016_0_6th_Report_TEFDEG.zip */
                    cmb.EDIFACT__c = Label.EdifactDownloadLink + tmp + '_' + tmpDate + '_' + sm2mId + '_' + '6th_Report_' + (sm2mType.equalsIgnoreCase('Local') ? 'TEFDEL' : 'TEFDEG') + '.zip'; 
                    /* 35005811_01_07_2016_0_5th_Report_TEFDEG.zip */
                    cmb.EVN_EDIFACT__c = Label.EvnEdifactDownloadLink + tmp + '_' + tmpDate + '_' + sm2mId + '_' + '5th_Report_' + (sm2mType.equalsIgnoreCase('Local') ? 'TEFDEL' : 'TEFDEG') + '.zip';                     
                }
                if (baToUse.EVN_Report__c){
                    /* PDF 35005811_01_07_2016_0_3rd_Report_TEFDEG.zip */
                    cmb.EVN_Report_PDF__c = Label.EvnPDFDownloadLink + tmp + '_' + tmpDate + '_' + sm2mId + '_' + '3rd_Report_' + (sm2mType.equalsIgnoreCase('Local') ? 'TEFDEL' : 'TEFDEG') + '.zip';                     
                    /* CSV 35005811_01_07_2016_0_4th_Report_TEFDEG.zip */
                    cmb.EVN_Report_CSV__c = Label.EvnCSVDownloadLink + tmp + '_' + tmpDate + '_' + sm2mId + '_' + '4th_Report_' + (sm2mType.equalsIgnoreCase('Local') ? 'TEFDEL' : 'TEFDEG') + '.zip';                     
                }
                

                /* end additional reports */
                cmb.Uploaded__c = false;
                upsert cmb Invoice_Number__c;
                
            } catch (VisualforceException e) {
               
            }
        }
    }

    //--------------------------------------------------------------------------
    // Finish of batch apex
    //--------------------------------------------------------------------------
    global void finish(Database.BatchableContext bc)
    {
        System.enqueueJob(new AsyncCustomerBillUpload());
    }
}