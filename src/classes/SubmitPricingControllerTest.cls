@isTest
private class SubmitPricingControllerTest {
    
    @testSetup
    static void prepareTestData(){
        list<EU_Countries__c> countries = Test_Util.createCountries();
	    insert countries;
	    
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers;
        
        Account account = new Account(
        	Name = 'Test3',
            Type = 'Business'
        	);
        	
        insert account;
        
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1),
			AccountId = account.Id,
            Description ='Test Test Test Test Test Test Test Test'
		);
		
		insert opp;
		
		Contact con = Test_Util.createContact(account.id);
		insert con;
		
		OpportunityContactRole oppConRole = Test_Util.createOppConRole(opp.id,con.id,'Decision Maker');
		insert oppConRole;
		
		cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(
			cscfga__Synchronised_with_Opportunity__c = false,
			cscfga__Opportunity__c = opp.Id
		);
		
		insert productBundle;
        
        list<cscfga__Product_Definition__c> productDefinitionLst = new list<cscfga__Product_Definition__c>();
        
        
        cscfga__Product_Definition__c productDefinition = Test_Util.createProdDefination('Connectivity','Test description');
		productDefinitionLst.add(productDefinition);
		
		cscfga__Product_Definition__c productDefinition1 = Test_Util.createProdDefination('Tariff Additional Service','Test description');
		productDefinitionLst.add(productDefinition1);
		
		cscfga__Product_Definition__c productDefinition2= Test_Util.createProdDefination('Tariff Supplementary Service','Test description');
		productDefinitionLst.add(productDefinition2);
		
		cscfga__Product_Definition__c productDefinition3 = Test_Util.createProdDefination('Support & Service','Test description');
		productDefinitionLst.add(productDefinition3);
		
		insert productDefinitionLst;
		
		list<id> prodDefinations = new list<id>();
		prodDefinations.add(productDefinition1.id);
		prodDefinations.add(productDefinition2.id); 
		prodDefinations.add(productDefinition3.id);
		list<cscfga__Attribute_Definition__c> attrDefinationsLst = Test_Util.attrDefinations(prodDefinations);
		insert attrDefinationsLst;
		
		cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c(
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinition.Id,
			name = 'SIM & Tariff'
		);
		
		
		insert productConfiguration;
        
        list<Tariff__c> tariffLst = new list<Tariff__c>();
        
        Tariff__c tariff = Test_Util.createTarrif('Tariff Test','Connectivity','1','Global','Draft',1000,1000);
        Tariff__c tariff1 = Test_Util.createTarrif('Tariff Test2','Connectivity','1','Global','Draft',1000,1000);
        Tariff__c tariff2 = Test_Util.createTarrif('Tariff Test','Connectivity','1','Global','Draft',1000,1000);
        tariff2.Contract_Term__c = '48';
        tariff2.Global_Local__c = 'Global';
        tariff2.Status__c = 'Approved';
        
        tariffLst.add(tariff);
        tariffLst.add(tariff1);
        tariffLst.add(tariff2);
        insert tariffLst;
        
        
        list<Tariff_Option__c> tariffOptionLst = new list<Tariff_Option__c>();
        Tariff_Option__c tariffOption = new Tariff_Option__c(isUsed__c = true,name ='Tariff Option Test1',Tariff__c = tariff.id,Type__c='Additional',Recurring_Base_Fee__c =1000,Quantity__c=10);
        Tariff_Option__c tariffOption1 = new Tariff_Option__c(isUsed__c = true,name ='Tariff Option Test2',Tariff__c = tariff1.id,Type__c='Additional',Recurring_Base_Fee__c =1000,Quantity__c=10);
        tariffOptionLst.add(tariffOption);
        tariffOptionLst.add(tariffOption1);
        
        Tariff_Option__c tariffOption2 = new Tariff_Option__c(isUsed__c = true,name ='Tariff Option Test3',Tariff__c = tariff1.id,Type__c='Support&Service',Recurring_Base_Fee__c =1000,Quantity__c=10);
        tariffOptionLst.add(tariffOption2);
        
        Tariff_Option__c tariffOption3 = new Tariff_Option__c(isUsed__c = true,name ='Tariff Option Test4',Tariff__c = tariff1.id,Type__c='Supplementary Service',Recurring_Base_Fee__c =1000,Quantity__c=10);
        tariffOptionLst.add(tariffOption3);
        
        Tariff_Option__c tariffOption4 = new Tariff_Option__c(isUsed__c = true,name ='Tariff Option Test5',Tariff__c = tariff1.id,Type__c='Included',Recurring_Base_Fee__c =1000,Quantity__c=10);
        tariffOptionLst.add(tariffOption4);
        
        insert tariffOptionLst;
        
        list<Rate_Card__c> rcLst = new list<Rate_Card__c>();
        Rate_Card__c rc1 = Test_Util.createRateCard(tariffOption.id,1,'Europe');
        rcLst.add(rc1);
        rcLst.add(Test_Util.createRateCard(tariffOption1.id,2,'Europe'));
        rcLst.add(Test_Util.createRateCard(tariffOption2.id,3,'Europe'));
        rcLst.add(Test_Util.createRateCard(tariffOption3.id,4,'Europe'));
        rcLst.add(Test_Util.createRateCard(tariffOption4.id,5,'Europe'));
        insert rcLst;

        list<Service_Fee__c> sf = new list<Service_Fee__c>();
        sf.add(Test_Util.createServiceFee(tariffOption.id,1));
        sf.add(Test_Util.createServiceFee(tariffOption1.id,2));
        sf.add(Test_Util.createServiceFee(tariffOption2.id,3));
        sf.add(Test_Util.createServiceFee(tariffOption3.id,4));
        sf.add(Test_Util.createServiceFee(tariffOption4.id,5));
        insert sf;
        
        list<Rate_Card_Country_Management__c> rcm = new list<Rate_Card_Country_Management__c>();
        Rate_Card_Country_Management__c rcm1 = Test_Util.createRateCardMngmt(rc1.id,'EU',0);
        rcm.add(rcm1);
        insert rcm;

        list<Business_comment__c> buComments = new list<Business_comment__c>();
        buComments.add(Test_Util.createBusinessComments(productBundle.id,'Commnent 1'));
        insert buComments;
        
        Article_Info_Set__c articleInfoSet = new Article_Info_Set__c(name ='Article Info set',
		                                                             Article_Type__c='SIM',
		                                                             Global_Local__c='Global',
		                                                             Status__c='Active');
		insert articleInfoSet;
        
        map<string,cscfga__Attribute_Definition__c> attrDefinations = Test_Util.attrDefinations(productDefinition.Id); 

        list<cscfga__Attribute__c> attLst = new list<cscfga__Attribute__c>();
        attLst.add(Test_Util.createAttrDef('Quantity',productConfiguration.id,'12',attrDefinations.get('Quantity').id ));
        attLst.add(Test_Util.createAttrDef('Name',productConfiguration.id,'abc',attrDefinations.get('Name').id ));
        insert attLst;

        Activation_plan__c ac = new Activation_plan__c(Product_Bundle__c=productBundle.id);
        insert ac; 

        notriggers.Flag__c = false;
        update notriggers;
    }

	static testMethod void testmethod1(){
        
         Opportunity opp = [select id, name from Opportunity limit 1];
         cscfga__Product_Bundle__c theBundle = [select id from cscfga__Product_Bundle__c limit 1 ];
         Tariff__c tariff1 = [select id from Tariff__c where name ='Tariff Test2' limit 1];
         
         Test.startTest();
            PageReference pageRefr = Page.PricingMainPage;
            pageRefr.getParameters().put('oppId',opp.Id);
            pageRefr.getParameters().put('Id',theBundle.Id);
            pageRefr.getParameters().put('msg','msg');
            Test.setCurrentPageReference(pageRefr); 
            SubmitPricingController ctrl = new SubmitPricingController (new ApexPages.StandardController(new cscfga__Product_Bundle__c()));
            ctrl.tariffSelected = tariff1.id;
            system.assert(ctrl!=null);
            ctrl.init();
            ctrl.saveActivationPlan();
            ctrl.getContractTermItems();
            ctrl.getApprovalStatuses();
            ctrl.saveBillingApprovalStatus();
         Test.stopTest();    

	}
	
	static testMethod void testmethod2(){
	    
	     Opportunity opp = [select id, name from Opportunity limit 1];
         cscfga__Product_Bundle__c theBundle = [select id from cscfga__Product_Bundle__c limit 1 ];
         Tariff__c tariff1 = [select id from Tariff__c where name ='Tariff Test2' limit 1];
        
         Test.startTest();
            PageReference pageRefr = Page.PricingMainPage;
            pageRefr.getParameters().put('oppId',opp.Id);
            pageRefr.getParameters().put('Id',theBundle.Id);
            pageRefr.getParameters().put('msg','msg');
            Test.setCurrentPageReference(pageRefr); 
            SubmitPricingController ctrl = new SubmitPricingController (new ApexPages.StandardController(new cscfga__Product_Bundle__c()));
            ctrl.tariffSelected = tariff1.id;
            system.assert(ctrl!=null);
            ctrl.init();
            ctrl.saveSLAApprovalStatus();
         Test.stopTest();    

	}


    static testMethod void testmethod3(){
	    
	     Opportunity opp = [select id, name from Opportunity limit 1];
         cscfga__Product_Bundle__c theBundle = [select id from cscfga__Product_Bundle__c limit 1 ];
         Tariff__c tariff1 = [select id from Tariff__c where name ='Tariff Test2' limit 1];
        
         Test.startTest();
            PageReference pageRefr = Page.PricingMainPage;
            pageRefr.getParameters().put('oppId',opp.Id);
            pageRefr.getParameters().put('Id',theBundle.Id);
            pageRefr.getParameters().put('msg','msg');
            Test.setCurrentPageReference(pageRefr); 
            SubmitPricingController ctrl = new SubmitPricingController (new ApexPages.StandardController(new cscfga__Product_Bundle__c()));
            ctrl.tariffSelected = tariff1.id;
            system.assert(ctrl!=null);
            ctrl.init();
            
            ctrl.getSimItems();
            ctrl.addSimType();
            ctrl.updateSimType();  
            
            for(SimType s : ctrl.simTypeList){
                s.isSelected = true;
                break;
            }
            ctrl.deleteSimType();
            
         Test.stopTest();    

	}


    static testMethod void testmethod4(){
	    
	     Opportunity opp = [select id, name from Opportunity limit 1];
         cscfga__Product_Bundle__c theBundle = [select id from cscfga__Product_Bundle__c limit 1 ];
         Tariff__c tariff1 = [select id from Tariff__c where name ='Tariff Test2' limit 1];
        
         Test.startTest();
            PageReference pageRefr = Page.PricingMainPage;
            pageRefr.getParameters().put('oppId',opp.Id);
            pageRefr.getParameters().put('Id',theBundle.Id);
            pageRefr.getParameters().put('msg','msg');
            Test.setCurrentPageReference(pageRefr); 
            SubmitPricingController ctrl = new SubmitPricingController (new ApexPages.StandardController(new cscfga__Product_Bundle__c()));
            ctrl.tariffSelected = tariff1.id;
            system.assert(ctrl!=null);
            ctrl.init();
            
            ctrl.businessCommentText= 'addNewComment';
            ctrl.addNewComment();
            ctrl.synchronizeBundle();
            
         Test.stopTest();    

	}
	
	static testMethod void testmethod5(){
	    
	     Opportunity opp = [select id, name from Opportunity limit 1];
         cscfga__Product_Bundle__c theBundle = [select id from cscfga__Product_Bundle__c limit 1 ];
         Tariff__c tariff1 = [select id from Tariff__c where name ='Tariff Test2' limit 1];
         theBundle.Billing_Approval_Status__c = 'Required';
         update theBundle;
         
        Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
        approvalRequest.setComments('Submitting request for approval.');
        approvalRequest.setObjectId(theBundle.id);
        approvalRequest.setSkipEntryCriteria(false); 
        Approval.ProcessResult apprRequestResult = Approval.process(approvalRequest);
         
         Test.startTest();
            PageReference pageRefr = Page.PricingMainPage;
            pageRefr.getParameters().put('oppId',opp.Id);
            pageRefr.getParameters().put('Id',theBundle.Id);
            pageRefr.getParameters().put('msg','msg');
            Test.setCurrentPageReference(pageRefr); 
            SubmitPricingController ctrl = new SubmitPricingController (new ApexPages.StandardController(new cscfga__Product_Bundle__c()));
            ctrl.tariffSelected = tariff1.id;
            system.assert(ctrl!=null);
            ctrl.init();
            ctrl.getApproverUser();
            ctrl.submitReject();
            
            
         Test.stopTest();    

	}
	
	static testMethod void testmethod6(){
	    
	     Opportunity opp = [select id, name from Opportunity limit 1];
         cscfga__Product_Bundle__c theBundle = [select id from cscfga__Product_Bundle__c limit 1 ];
         Tariff__c tariff1 = [select id from Tariff__c where name ='Tariff Test2' limit 1];
         theBundle.Billing_Approval_Status__c = 'Required';
         update theBundle;
         
        Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
        approvalRequest.setComments('Submitting request for approval.');
        approvalRequest.setObjectId(theBundle.id);
        approvalRequest.setSkipEntryCriteria(false);
        Approval.ProcessResult apprRequestResult = Approval.process(approvalRequest);
         
         Test.startTest();
            PageReference pageRefr = Page.PricingMainPage;
            pageRefr.getParameters().put('oppId',opp.Id);
            pageRefr.getParameters().put('Id',theBundle.Id);
            pageRefr.getParameters().put('msg','msg');
            Test.setCurrentPageReference(pageRefr); 
            SubmitPricingController ctrl = new SubmitPricingController (new ApexPages.StandardController(new cscfga__Product_Bundle__c()));
            ctrl.tariffSelected = tariff1.id;
            system.assert(ctrl!=null);
            ctrl.init();
            ctrl.getApproverUser();
            ctrl.recallApprovalRequest();
            
            
         Test.stopTest();    

	}
	
	static testMethod void testmethod7(){
	    
	     Opportunity opp = [select id, name from Opportunity limit 1];
         cscfga__Product_Bundle__c theBundle = [select id from cscfga__Product_Bundle__c limit 1 ];
         Tariff__c tariff1 = [select id from Tariff__c where name ='Tariff Test2' limit 1];
         theBundle.Billing_Approval_Status__c = 'Required';
         update theBundle;
         
        Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
        approvalRequest.setComments('Submitting request for approval.');
        approvalRequest.setObjectId(theBundle.id);
        approvalRequest.setSkipEntryCriteria(false);
        Approval.ProcessResult apprRequestResult = Approval.process(approvalRequest);
         
         Test.startTest();
            PageReference pageRefr = Page.PricingMainPage;
            pageRefr.getParameters().put('oppId',opp.Id);
            pageRefr.getParameters().put('Id',theBundle.Id);
            pageRefr.getParameters().put('msg','msg');
            Test.setCurrentPageReference(pageRefr); 
            SubmitPricingController ctrl = new SubmitPricingController (new ApexPages.StandardController(new cscfga__Product_Bundle__c()));
            ctrl.tariffSelected = tariff1.id;
            system.assert(ctrl!=null);
            ctrl.init();
            ctrl.getApproverUser();
            ctrl.reassign();
            
            
         Test.stopTest();    

	}
	
	static testMethod void testmethod8(){
	    
	     Opportunity opp = [select id, name from Opportunity limit 1];
         cscfga__Product_Bundle__c theBundle = [select id from cscfga__Product_Bundle__c limit 1 ];
         Tariff__c tariff1 = [select id from Tariff__c where name ='Tariff Test2' limit 1];
         theBundle.Billing_Approval_Status__c = 'Required';
         update theBundle;
         
        Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
        approvalRequest.setComments('Submitting request for approval.');
        approvalRequest.setObjectId(theBundle.id);
        approvalRequest.setSkipEntryCriteria(false);
        
        Approval.ProcessResult apprRequestResult = Approval.process(approvalRequest);
         
         Test.startTest();
            PageReference pageRefr = Page.PricingMainPage;
            pageRefr.getParameters().put('oppId',opp.Id);
            pageRefr.getParameters().put('Id',theBundle.Id);
            pageRefr.getParameters().put('msg','msg');
            Test.setCurrentPageReference(pageRefr); 
            SubmitPricingController ctrl = new SubmitPricingController (new ApexPages.StandardController(new cscfga__Product_Bundle__c()));
            ctrl.tariffSelected = tariff1.id;
            system.assert(ctrl!=null);
            ctrl.init();
            ctrl.getApproverUser();
            ctrl.approveReject();
         Test.stopTest();    

	}
	
	static testMethod void testmethod9(){
	    
	     Opportunity opp = [select id, name from Opportunity limit 1];
         cscfga__Product_Bundle__c theBundle = [select id from cscfga__Product_Bundle__c limit 1 ];
         Tariff__c tariff1 = [select id from Tariff__c where name ='Tariff Test2' limit 1];
         
         Test.startTest();
            PageReference pageRefr = Page.PricingMainPage;
            pageRefr.getParameters().put('oppId',opp.Id);
            pageRefr.getParameters().put('Id',theBundle.Id);
            pageRefr.getParameters().put('msg','msg');
            Test.setCurrentPageReference(pageRefr); 
            SubmitPricingController ctrl = new SubmitPricingController (new ApexPages.StandardController(new cscfga__Product_Bundle__c()));
            ctrl.tariffSelected = tariff1.id;
            system.assert(ctrl!=null);
            ctrl.init();
            ctrl.theBundle.Pricing_Approval_Status__c = 'Approved';
            update ctrl.theBundle;
            try{
                ctrl.saveIncludedOptions();
            }catch(exception e){
                
            }
            ctrl.additionalTariffSelected = 'Tariff Option Test1';
            ctrl.addAdditionallOption();
            ctrl.createAdditionallOption();
            try{
                ctrl.saveCreatedAdditionalOption();
            }catch(exception e){
                
            }
            
            ctrl.createAdditionallOption();
            ctrl.removeCreatedAdditionalOption();
            ctrl.tariffOption = 'Tariff Option Test1';
            ctrl.removeAdditionalOption();
            
            ctrl.createSuppOption();
            
            try{
                ctrl.saveCreatedSuppOption();
            }catch(exception e){
                
            }
            ctrl.createSuppOption();
            ctrl.removeCreatedSuppOption();
            
            ctrl.suppTariffSelected = 'Tariff Option Test3';
            ctrl.addSupplementalOption();
            
            ctrl.tariffOption = 'Tariff Option Test3';
            ctrl.removeSupplementalOption();
         Test.stopTest();    

	}
	
	static testMethod void testmethod10(){
	    
	     Opportunity opp = [select id, name from Opportunity limit 1];
         cscfga__Product_Bundle__c theBundle = [select id from cscfga__Product_Bundle__c limit 1 ];
         Tariff__c tariff1 = [select id from Tariff__c where name ='Tariff Test2' limit 1];
         Rate_Card__c rc1 = [select id from Rate_Card__c limit 1];
         Rate_Card_Country_Management__c rcm1 = [select id from Rate_Card_Country_Management__c limit 1];
         Test.startTest();
            PageReference pageRefr = Page.PricingMainPage;
            pageRefr.getParameters().put('oppId',opp.Id);
            pageRefr.getParameters().put('Id',theBundle.Id);
            pageRefr.getParameters().put('msg','msg');
            Test.setCurrentPageReference(pageRefr); 
            SubmitPricingController ctrl = new SubmitPricingController (new ApexPages.StandardController(new cscfga__Product_Bundle__c()));
            ctrl.tariffSelected = tariff1.id;
            system.assert(ctrl!=null);
            ctrl.init();
            ctrl.RateCardTargetPriceChanged();
            ctrl.rateCardId = rc1.id;
            ctrl.ratio = 0;
            ctrl.rateCardCountryManagementId = rcm1.id;
            ctrl.selectedCountryId = 'Austria';
            ctrl.selectedCountryId = 'AT';
            ctrl.addNewRccm();
            ctrl.updateRccm();
            ctrl.deleteRccm();
            
         Test.stopTest();    

	}
	
	static testMethod void testmethod11(){
	    
	     Opportunity opp = [select id, name from Opportunity limit 1];
         cscfga__Product_Bundle__c theBundle = [select id from cscfga__Product_Bundle__c limit 1 ];
         Tariff__c tariff1 = [select id from Tariff__c where name ='Tariff Test2' limit 1];
         Rate_Card__c rc1 = [select id from Rate_Card__c limit 1];
         Rate_Card_Country_Management__c rcm1 = [select id from Rate_Card_Country_Management__c limit 1];
         
        theBundle.Billing_Approval_Status__c = 'Required';
        update theBundle;
        
        Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
        approvalRequest.setComments('Submitting request for approval.');
        approvalRequest.setObjectId(theBundle.id);
        approvalRequest.setSkipEntryCriteria(false);
        
        Approval.ProcessResult apprRequestResult = Approval.process(approvalRequest);
         
         Test.startTest();
            PageReference pageRefr = Page.PricingMainPage;
            pageRefr.getParameters().put('oppId',opp.Id);
            pageRefr.getParameters().put('Id',theBundle.Id);
            pageRefr.getParameters().put('msg','msg');
            Test.setCurrentPageReference(pageRefr); 
            SubmitPricingController ctrl = new SubmitPricingController (new ApexPages.StandardController(new cscfga__Product_Bundle__c()));
            ctrl.tariffSelected = tariff1.id;
            system.assert(ctrl!=null);
            ctrl.init();
            try{
                ctrl.AcceptVersion();
            }catch(exception e){
                
            }
            
            
         Test.stopTest();    

	}
	
	
	static testMethod void testmethod12(){
	    
	     Opportunity opp = [select id, name from Opportunity limit 1];
         cscfga__Product_Bundle__c theBundle = [select id from cscfga__Product_Bundle__c limit 1 ];
         Tariff__c tariff1 = [select id from Tariff__c where name ='Tariff Test2' limit 1];
         Rate_Card__c rc1 = [select id from Rate_Card__c limit 1];
         Rate_Card_Country_Management__c rcm1 = [select id from Rate_Card_Country_Management__c limit 1];
         
        theBundle.Billing_Approval_Status__c = 'Required';
        update theBundle;
        
        Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
        approvalRequest.setComments('Submitting request for approval.');
        approvalRequest.setObjectId(theBundle.id);
        approvalRequest.setSkipEntryCriteria(false);
        
        Approval.ProcessResult apprRequestResult = Approval.process(approvalRequest);
         
         Test.startTest();
            PageReference pageRefr = Page.PricingMainPage;
            pageRefr.getParameters().put('oppId',opp.Id);
            pageRefr.getParameters().put('Id',theBundle.Id);
            pageRefr.getParameters().put('msg','msg');
            Test.setCurrentPageReference(pageRefr); 
            SubmitPricingController ctrl = new SubmitPricingController (new ApexPages.StandardController(new cscfga__Product_Bundle__c()));
            ctrl.tariffSelected = tariff1.id;
            system.assert(ctrl!=null);
            ctrl.init();
            ctrl.submitNewPrice();
            ctrl.save();
            
            
         Test.stopTest();    

	}
}