global class RefreshTicketsFromUDOBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    
    List<String> statuses = new List<String>{'Closed and Canceled', 'Closed and Solved'};
    List<String> udoStatuses = new List<String>{'Closed', 'Solved', 'Canceled'};
    public String query = 'select Id, Subject, CreatedDate, CaseNumber, Account.Name, Account.SM2M_Customer_Name__c, UDo_Contact_Id__c, UDo_Contact_eId__c, Status, History_tracking_UDo_changes__c, UDo_Ticket_created__c, Description, Customer_Service_Type__c, Order_Type__c, UDo_Description__c, Massive__c, UDo_Status__c, UDo_Severity__c from Case where UDo_Ticket_created__c = true AND Status NOT IN :statuses AND UDo_Status__c NOT IN :udoStatuses';
              
    global Database.QueryLocator start(Database.BatchableContext BC){
        //System.debug('Query : ' + query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> tickets){
        try{
            List<Case> casesToUpdate = new List<Case>();
            List<Casecomment> casecommentsToDel = new List<Casecomment>();
            List<Casecomment> casecommentsToInsert = new List<Casecomment>();
            
            set<id> caseIds = new set<id>();
            
            for(Case c : (List<Case>)tickets){
                CaseNCaseComments rCase = UDOUtils.RefreshTicketFromUDo(c);
                system.debug('rCase  '+rCase);
                if(rCase != null && rCase.cs != null ){
                    casesToUpdate.add(rCase.cs);
                    if(rCase.caseCmnt != null && rCase.caseCmnt.size() > 0){
                        casecommentsToInsert.addAll(rCase.caseCmnt);
                    }
                }
                caseIds.add(c.id);
            }
            
            if(caseIds.size() > 0){
                casecommentsToDel = [select id from CaseComment where parentId In:caseIds];
                if(casecommentsToDel.size() > 0){
                    delete casecommentsToDel;
                }
            }
            
            if(!casesToUpdate.isEmpty()){
                UDOUtils.stopTriggerExecution = true;
                update casesToUpdate;
            }
            
            if(casecommentsToInsert != null && casecommentsToInsert.size() > 0){
                insert casecommentsToInsert;
            }
            system.debug('casecommentsToInsert  '+casecommentsToInsert);
        }catch(exception e){
            system.debug('Exception ::'+e.getMessage());
        }
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
}