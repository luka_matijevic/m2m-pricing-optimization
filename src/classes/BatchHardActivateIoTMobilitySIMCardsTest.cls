@isTest
private class BatchHardActivateIoTMobilitySIMCardsTest {
	
	@isTest static void test_method_one() {
		No_Trigger__c notriggers = new No_Trigger__c();
  		notriggers.Flag__c = true;
  		insert notriggers;

		Frame_Contract__c fc = new Frame_Contract__c();
        insert fc;
        fc = [SELECT Id, Name from Frame_Contract__c limit 1];

        Order_Request__c m2mor = new Order_Request__c(
        	Frame_Contract__c = fc.Id
        );
        insert m2mor;
        m2mor = [SELECT Id, Name, CreatedDate, RecordTypeId from Order_Request__c limit 1];

        Account acc = new Account(
            Name = 'Test account',
            TEF_DE_Sales_Channel__c = 'Test channel',
            Type = 'Customer'
        );
        insert acc;

        Date stockChangeDate = System.today();
        stockChangeDate = stockChangeDate.addDays(-65);
        List<Stock__c> simList = new List<Stock__c>();

        Stock__c sim1 = new Stock__c(
            Name = '12345678901234567891',
            ICCID__c = '12345678901234567891',
            Account__c = acc.Id,
            SM2M_Status__c = 'INACTIVE_NEW',
            Order_Request__c = m2mor.Id,
            State_Change_Date__c = stockChangeDate
        );
        simList.add(sim1);
        Stock__c sim2 = new Stock__c(
            Name = '12345678901234567892',
            ICCID__c = '12345678901234567892',
            Account__c = acc.Id,
            SM2M_Status__c = 'INACTIVE_NEW',
            Order_Request__c = m2mor.Id,
            State_Change_Date__c = stockChangeDate
        );
        simList.add(sim2);
        Stock__c sim3 = new Stock__c(
            Name = '12345678901234567893',
            ICCID__c = '12345678901234567893',
            Account__c = acc.Id,
            SM2M_Status__c = 'INACTIVE_NEW',
            Order_Request__c = m2mor.Id,
            State_Change_Date__c = stockChangeDate
        );
        simList.add(sim3);

        insert simList;

        
        
        Date myDate = Date.newInstance(m2mor.createdDate.year(), m2mor.createdDate.month(), m2mor.createdDate.day());

        Test.startTest();
        Database.executeBatch(new BatchHardActivateIoTMobilitySIMCards(m2mor.RecordTypeId));
        Database.executeBatch(new BatchHardActivateIoTMobilitySIMCards(myDate, m2mor.RecordTypeId));
        Test.stopTest();
	}
}