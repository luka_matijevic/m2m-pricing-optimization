@isTest
/**
 * TestSecondUploadFileDataGenerator
 * used for loading the data needed and for the test of the second upload file batch apex
 */
public class TestSecondUploadFileDataGenerator {
	//-----------------------------------------------------------------------------
	// Constants
	//-----------------------------------------------------------------------------
	public static final String TWO_HUNDRED_SIMS_IN_CSV = 
		TestHelper.loadStaticResource('Test200SimsCSV');
	public static final String ONE_SIM_IN_CSV = '1235791085088000000,34698700000';

	//-----------------------------------------------------------------------------
	//
	//-----------------------------------------------------------------------------
    public static void insertSimAssets()
    {
    	List<List<String>> simList = CSVReader.readIETFRFC4180CSVFile
			(Blob.valueOf(TWO_HUNDRED_SIMS_IN_CSV));
    	System.assertEquals(200, simList.size());

		//
		// Create article info sets
		//
		TestHelper.createArticleInfoSets();
    	Id aisId = 
			[ SELECT Id FROM Article_Info_Set__c 
			  WHERE ARTICLE_TYPE__C = 'SIM' 
			  LIMIT 1 ].Id;
    	Account acc = TestHelper.createAccount();

    	SRF__c srf = new SRF__c
				( SIM_Type__c = aisId
				, RecordTypeId = Cache.RecordTypeId('SRF__c.Internal SIM Delivery')
				, Account__c = acc.Id);
    	insert srf;

    	SecondUploadFileBatchParser batchParse = new SecondUploadFileBatchParser
			(aisId, simList, srf.Id, acc.Id);
    	Iterable<Stock__c> simIterator = batchParse.start(null);
    	SeconUploadFileListIterator iterator = 
			(SeconUploadFileListIterator) simIterator.iterator();
    	List<Stock__c> simAssets = new List<Stock__c>();
		   /*LM test fix*/
           SIM_Range__c sr = new SIM_Range__c();
            sr.Range_start__c = '0';
            sr.Range_end__c = '99999999999999999999';
            sr.SIM_Origin__c = 'Global';
        insert sr;
         /*LM test fix end*/
    	while(iterator.hasNext())
    	{
    		Stock__c asset = iterator.next();
    		asset.SRF__c = srf.Id;
         
            
           
    		simAssets.add(asset);
    	}
       
        /*LM test fix end*/
    	insert simAssets;
    }

	//-----------------------------------------------------------------------------
	//
	//-----------------------------------------------------------------------------
	public static void insertSimAssetsBIC()
    {
    	List<List<String>> simList = CSVReader.readIETFRFC4180CSVFile
			(Blob.valueOf(TWO_HUNDRED_SIMS_IN_CSV));
    	System.assertEquals(200, simList.size());

    	Id aisId = 
			[ SELECT Id 
			  FROM Article_Info_Set__c 
			  WHERE ARTICLE_TYPE__C = 'SIM' 
			  LIMIT 1 ].Id;
    	Account acc = [SELECT Id FROM Account];

    	SRF__c srf = new SRF__c
			( SIM_Type__c = aisId
			, RecordTypeId = Cache.RecordTypeId('SRF__c.Internal SIM Delivery')
			, Account__c = acc.Id );
    	insert srf;

    	SecondUploadFileBatchParser batchParse = new SecondUploadFileBatchParser(aisId, simList, srf.Id, acc.Id);
    	Iterable<Stock__c> simIterator = batchParse.start(null);
    	SeconUploadFileListIterator iterator = (SeconUploadFileListIterator) simIterator.iterator();
    	List<Stock__c> simAssets = new List<Stock__c>();

    	while(iterator.hasNext())
    	{
    		Stock__c asset = iterator.next();
    		asset.SRF__c = srf.Id;
    		simAssets.add(asset);
    	}
    	insert simAssets;
    }

	//-----------------------------------------------------------------------------
	//
	//-----------------------------------------------------------------------------
	public static void insertSimAsset()
    {
    	List<List<String>> simList = CSVReader.readIETFRFC4180CSVFile
			(Blob.valueOf(ONE_SIM_IN_CSV));
    	System.assertEquals(1, simList.size());

    	Id aisId = 
			[ SELECT Id 
			  FROM Article_Info_Set__c 
			  WHERE ARTICLE_TYPE__C = 'SIM' 
			  LIMIT 1 ].Id;
    	Account acc = [ SELECT Id FROM Account ];
    	SRF__c srf = new SRF__c
			( SIM_Type__c = aisId
			, RecordTypeId = Cache.RecordTypeId('SRF__c.Internal SIM Delivery')
			, Account__c = acc.Id );
    	insert srf;

    	SecondUploadFileBatchParser batchParse = new SecondUploadFileBatchParser(aisId, simList, srf.Id, acc.Id);
    	Iterable<Stock__c> simIterator = batchParse.start(null);
    	SeconUploadFileListIterator iterator = (SeconUploadFileListIterator) simIterator.iterator();
    	List<Stock__c> simAssets = new List<Stock__c>();

    	while(iterator.hasNext())
    	{
    		Stock__c asset = iterator.next();
    		asset.SRF__c = srf.Id;
    		simAssets.add(asset);
    	}
    	insert simAssets;
    }

	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
    static testmethod void testGenerateSIMAssets()
    {
    	List<List<String>> simList = CSVReader.readIETFRFC4180CSVFile
			(Blob.valueOf(TWO_HUNDRED_SIMS_IN_CSV));
    	System.assertEquals(200, simList.size());

		//
		// Create article info sets
		//
		TestHelper.createArticleInfoSets();
    	Id aisId = 
			[ SELECT Id 
			  FROM Article_Info_Set__c 
			  WHERE ARTICLE_TYPE__C = 'SIM' 
			  LIMIT 1 ].Id;
		Account acc = TestHelper.createAccount();
    	SRF__c srf = new SRF__c
			( SIM_Type__c = aisId
			, RecordTypeId = Cache.RecordTypeId('SRF__c.Internal SIM Delivery')
			, Account__c = acc.Id);
    	insert srf;

    	SecondUploadFileBatchParser batchParse = new SecondUploadFileBatchParser(aisId, simList, srf.Id, acc.Id);
    	Iterable<Stock__c> simIterator = batchParse.start(null);
    	SeconUploadFileListIterator iterator = (SeconUploadFileListIterator) simIterator.iterator();
    	List<Stock__c> simAssets = new List<Stock__c>();

    	while (iterator.hasNext())
    	{
    		Stock__c asset = iterator.next();
    		asset.SRF__c = srf.Id;
    		simAssets.add(asset);
    	}

    	Test.startTest();
    	batchParse.execute(null, simAssets);
    	List<Stock__c> insertedAssets = 
			[ SELECT Id 
			  FROM Stock__c 
			  WHERE Stock__c.RecordTypeId = :Cache.RecordTypeId('Stock__c.SIM') ];
    	Test.stopTest();
    	System.assertEquals(200, insertedAssets.size());
    }
}