public class AttachmentTriggerDelegate extends TriggerHandler.DelegateBase {
	
	private List<Attachment> bicReportAttachments = new List<Attachment>();
	private List<Case> c = null;
	private Id insertedAttachmentParentId, insertedAttachmentId;

	public override void prepareBefore() {
		// do any preparation here – bulk loading of data etc
	}
	
	public override void prepareAfter() {
		// do any preparation here – bulk loading of data etc
	}
	
	public override void beforeInsert(sObject o) {
		// Apply before insert logic to this sObject. DO NOT do any SOQL
		// or DML here – store records to be modified in an instance variable
		// which can be processed by the finish() method
		Attachment attach = (Attachment) o;
		
		if (isPdfAttachment(attach)) {
			system.debug(LoggingLevel.INFO, 'Detected PDF - changing content type to application/pdf'); 
			attach.ContentType = 'application/pdf';
		}
	}
	
	public override void beforeUpdate(sObject old, sObject o) {
		// Apply before update logic to this sObject. DO NOT do any SOQL
		// or DML here – store records to be modified in an instance variable
		// which can be processed by the finish() method
	}

	public override void beforeDelete(sObject o) {
		// Apply before delete logic to this sObject. DO NOT do any SOQL
		// or DML here – store records to be modified in an instance variable
		// which can be processed by the finish() method
	}

	public override void afterInsert(sObject o) {
		// Apply after insert logic to this sObject. DO NOT do any SOQL
		// or DML here – store records to be modified in an instance variable
		// which can be processed by the finish() method
		
		Attachment attach = (Attachment) o;
		if (isBicReportAttachment(attach)) {
			bicReportAttachments.add(attach);
		}

		insertedAttachmentId = attach.Id;
		insertedAttachmentParentId = attach.ParentId;		
	}

	public override void afterUpdate(sObject old, sObject o) {
		// Apply after update logic to this sObject. DO NOT do any SOQL
		// or DML here – store records to be modified in an instance variable
		// which can be processed by the finish() method
	}

	public override void afterDelete(sObject o) {
		// Apply after delete logic to this sObject. DO NOT do any SOQL
		// or DML here – store records to be modified in an instance variable
		// which can be processed by the finish() method
	}

	public override void afterUndelete(sObject o) {
		// Apply after undelete logic to this sObject. DO NOT do any SOQL
		// or DML here – store records to be modified in an instance variable
		// which can be processed by the finish() method
	}

	public override void finish() {
		if (!bicReportAttachments.isEmpty()) {
			
			Set<Id> parentIds = new Set<Id>();
			for (Attachment attach : bicReportAttachments) {
				parentIds.add(attach.ParentId);
			}
			
			List<BIC_Export_Job_Data__c> jobData = [Select Id from BIC_Export_Job_Data__c where Id in :parentIds];
			for (BIC_Export_Job_Data__c job : jobData) {
				job.Report_Generated__c = true;	
			}
			update jobData;
		}
				
		c = [SELECT Id, UDo_Ticket_created__c FROM Case WHERE Id =: insertedAttachmentParentId];

		if (c != null && c.size() > 0) {
			if (c[0].UDo_Ticket_created__c)
			{
				UDOUtils.SendFileInUDo(insertedAttachmentId);
			}
		}
	}
	
	private Boolean isPdfAttachment(Attachment attach) {
		return attach.Name.endsWith('.pdf');
	}
	
	private Boolean isBicReportAttachment(Attachment attach) {
		return attach.Name.contains('m2m_to_telefonica_sim_lifecycle');
	}
}