/**
 * Used for calculating SIM contract durations for Stock__c object. 
 * Used by at least:
 * - BatchSimContractDurationCalculation
 * - before insert and update triggers on Stock__c
 *
 * Those classes assure that contract duration is propagated to Stock__c level.
 * The Stock__c objects can then be used through the StockService to provide all
 * the relevant information for BIC reports to Heroku.
 */ 
public class SimContractDurationCalculator {
	
	public SimContractDurationCalculator() {
		
	}

	/**
	 * Updates the SIMs with BIC_Contract_Duration__c and returns them.
	 * The idea is to find the Third Upload File for the SIM, and then find the originating
	 * Product Configuration and read the duration from there.
	 *
	 * Data model as of 2014-04-10: 
	 * Stock → TUF → M2M Order Request ← Product Order ← Product Configuration Order → Product Configuration
	 */ 
	public Map<Id, Integer> calculateContractDurations(List<Stock__c> sims) {
		
		Map<Id, Integer> simContractDurations = new Map<Id, Integer>();

		Set<Id> orderReqIds = new Set<Id>();
		
		for (Stock__c sim : sims) {
			if (sim.Order_Request__c != null) {
				orderReqIds.add(sim.Order_Request__c);
			}
		}

		List<Product_Order__c> productOrders = [
			SELECT Id
			FROM Product_Order__c
			WHERE Order_Request__c IN :orderReqIds
		];

		List<Product_Configuration_Order__c> productConfigurationOrders = [
			SELECT
				Product_Configuration__r.cscfga__Contract_Term__c, 
				Product_Configuration__r.cscfga__Description__c, 
				Product_Order__r.Order_Request__c
			FROM 
				Product_Configuration_Order__c
			WHERE 
				Product_Order__c IN :productOrders
		];

		// if there are multiple sim & tarrif product configuration orders 
		// on a same order request, the highest contract term is taken
		Map<Id, Integer> orderReqContractDurations = new Map<Id, Integer>();

		for (Product_Configuration_Order__c pco : productConfigurationOrders) {
			
			if (isConfigSimAndTariff(pco) && hasConfigWithOrderRequest(pco)) {

				Integer contractTerm = Integer.valueOf(pco.Product_Configuration__r.cscfga__Contract_Term__c);
				Id orderRequestId = pco.Product_Order__r.Order_Request__c;

				if (!orderReqContractDurations.containsKey(orderRequestId)) {
					orderReqContractDurations.put(orderRequestId, contractTerm);

				} else if (orderReqContractDurations.get(orderRequestId) < contractTerm) {
					
					orderReqContractDurations.put(orderRequestId, contractTerm);
				}
			}
		}

		for (Stock__c sim : sims) {
			simContractDurations.put(sim.Id, orderReqContractDurations.get(sim.Order_Request__c));
		}

		return simContractDurations;
	}

	private Boolean isConfigSimAndTariff(Product_Configuration_Order__c pco) {
		return pco.Product_Configuration__r.cscfga__Description__c == Constants.SIM_AND_TARRIF;
	}

	private Boolean hasConfigWithOrderRequest(Product_Configuration_Order__c pco) {
		return pco.Product_Order__r.Order_Request__c != null;
	}
}