//------------------------------------------------------------------------------
// Tests for tariff functionality
//------------------------------------------------------------------------------
@isTest
private class TestTariff
{
	static testMethod void testRetrieveTariff()
	{
		TestHelper.createTariffs();
		Id tariffId = [SELECT Id FROM Tariff__c WHERE Parent_Tariff__c = null].Id;
		ITariff tariff = Factory.createTariff(tariffId);
		//
		// Tests
		//
		List<ITariffOption> tariffOptions =
			tariff.getTariffOptions();
		List<ITariffOption> includedTariffOptions =
			tariff.getIncludedTariffOptions();
		List<ITariffOption> additionalTariffOptions =
			tariff.getAdditionalTariffOptions();
		List<ITariffOption> supplementaryServiceTariffOptions =
			tariff.getSupplementaryServiceTariffOptions();

		//
		// Test tariff
		//
		system.assertEquals('Test tariff', tariff.getTariff().Name);
		system.assertEquals('code', tariff.getTariff().Code__c);
		system.assertEquals('commercial plan', tariff.getTariff().Commercial_Plan__c);
		system.assertEquals('24', tariff.getTariff().Contract_Term__c);
		system.assertEquals('german description', tariff.getTariff().Description_DE__c);
		system.assertEquals('english description', tariff.getTariff().Description_ENG__c);
		system.assertEquals('Global', tariff.getTariff().Global_Local__c);
		system.assertEquals('group', tariff.getTariff().Group__c);
		system.assertEquals(12.00, tariff.getTariff().One_Off_Price__c);
		system.assertEquals(6.00, tariff.getTariff().Recurring_Price__c);
		system.assertEquals('Horizontals', tariff.getTariff().Product_Family__c);
		system.assertEquals('Direct', tariff.getTariff().Sales_Channel__c);
		system.assertEquals('Approved', tariff.getTariff().Status__c);
		system.assertEquals(1.00, tariff.getTariff().Volume_Start__c);
		system.assertEquals(100.00, tariff.getTariff().Volume_End__c);

		//
		// Test tariff options
		//
		TestHelper.checkTariffOptions(tariffOptions,
				includedTariffOptions,
				additionalTariffOptions,
				supplementaryServiceTariffOptions);
	}

	//--------------------------------------------------------------------------
	// Test cloning of tariff
	//--------------------------------------------------------------------------
	static testMethod void testCloneTariff()
	{
		TestHelper.createTariffs();
		Id tariffId = [SELECT Id FROM Tariff__c WHERE Parent_Tariff__c = null].Id;
		ITariff tariff = Factory.createTariff(tariffId);
		ITariff clonedTariff = tariff.deepClone();
		//
		// Tests
		//
		List<ITariffOption> tariffOptions =
			clonedTariff.getTariffOptions();
		List<ITariffOption> includedTariffOptions =
			clonedTariff.getIncludedTariffOptions();
		List<ITariffOption> additionalTariffOptions =
			clonedTariff.getAdditionalTariffOptions();
		List<ITariffOption> supplementaryServiceTariffOptions =
			clonedTariff.getSupplementaryServiceTariffOptions();

		//
		// Test tariff options
		//
		TestHelper.checkTariffOptions(tariffOptions,
				includedTariffOptions,
				additionalTariffOptions,
				supplementaryServiceTariffOptions);
	}

	//--------------------------------------------------------------------------
	// Test compare functionality
	//--------------------------------------------------------------------------
	static testMethod void testCompareTariff()
	{
		TestHelper.createTariffs();
		Id tariffId = [SELECT Id FROM Tariff__c WHERE Parent_Tariff__c = null].Id;
		ITariff tariff = Factory.createTariff(tariffId);
		ITariff clonedTariff = tariff.deepClone();
		//
		// Test equal tariffs
		//
		system.assertEquals(true, tariff.equals(clonedTariff));
		system.assertEquals(true, tariff.equalsWithoutQuantity(clonedTariff));
		//
		// Test tariffs with different quantities
		//
		List<ITariffOption> additionalTariffOptions =
			clonedTariff.getAdditionalTariffOptions();
		additionalTariffOptions[0].setQuantity(2);
		system.assertEquals(false, tariff.equals(clonedTariff));
		system.assertEquals(true, tariff.equalsWithoutQuantity(clonedTariff));
		//
		// Test tariffs with different Account__c
		//
		clonedTariff.getTariff().Account__c = tariffId;
		system.assertEquals(false, tariff.equals(clonedTariff));
		system.assertEquals(false, tariff.equalsWithoutQuantity(clonedTariff));
		//
		// Test tariffs with different is selected
		//
		additionalTariffOptions[0].setIsSelected(false);
		system.assertEquals(false, tariff.equals(clonedTariff));
		system.assertEquals(false, tariff.equalsWithoutQuantity(clonedTariff));
		//
		// Test tariffs with different show on quote
		//
		additionalTariffOptions[0].getTariffOption().Show_on_Quote__c = true;
		system.assertEquals(false, tariff.equals(clonedTariff));
		system.assertEquals(false, tariff.equalsWithoutQuantity(clonedTariff));
		//
		// Test tariffs with different target price
		//
		additionalTariffOptions[0].getTariffOption().Recurring_Target_Fee__c = 54.92;
		system.assertEquals(false, tariff.equals(clonedTariff));
		system.assertEquals(false, tariff.equalsWithoutQuantity(clonedTariff));
		//
		// Test tariffs with different target price and selected option
		//
		additionalTariffOptions[0].setIsSelected(true);
		system.assertEquals(false, tariff.equals(clonedTariff));
		system.assertEquals(false, tariff.equalsWithoutQuantity(clonedTariff));
	}

	//--------------------------------------------------------------------------
	// Test merge functionality
	//--------------------------------------------------------------------------
	static testMethod void testMergeTariff()
	{
		TestHelper.createTariffs();
		Id parentTariffId = [ SELECT Id, Parent_Tariff__c
			FROM Tariff__c
			WHERE Parent_Tariff__c = null].Id;
		Id clonedTariffId = [ SELECT Id, Parent_Tariff__c
			FROM Tariff__c
			WHERE Parent_Tariff__c = :parentTariffId].Id;

		ITariff parentTariff = Factory.createTariff(parentTariffId);
		ITariff clonedTariff = Factory.createTariff(clonedTariffId);
		clonedTariff.deepMerge(parentTariff);
		//
		// Tests
		//
		List<ITariffOption> tariffOptions =
			clonedTariff.getTariffOptions();
		List<ITariffOption> includedTariffOptions =
			clonedtariff.getIncludedTariffOptions();
		List<ITariffOption> additionalTariffOptions =
			clonedTariff.getAdditionalTariffOptions();
		List<ITariffOption> supplementaryServiceTariffOptions =
			clonedTariff.getSupplementaryServiceTariffOptions();
		//
		// Test cloned tariff options
		//
		TestHelper.checkClonedTariffOptions(tariffOptions,
				includedTariffOptions,
				additionalTariffOptions,
				supplementaryServiceTariffOptions);
	}

	//--------------------------------------------------------------------------
	// Test merge at the end of the list
	//--------------------------------------------------------------------------
	static testMethod void testMergeTariffAtTheEnd()
	{
		TestHelper.createTariffsCloneWithoutOptions();
		Id parentTariffId = [ SELECT Id, Parent_Tariff__c
			FROM Tariff__c
			WHERE Parent_Tariff__c = null].Id;
		Id clonedTariffId = [ SELECT Id, Parent_Tariff__c
			FROM Tariff__c
			WHERE Parent_Tariff__c = :parentTariffId AND Status__c = 'Approved'].Id;

		ITariff parentTariff = Factory.createTariff(parentTariffId);
		ITariff clonedTariff = Factory.createTariff(clonedTariffId);
		clonedTariff.deepMerge(parentTariff);
		//
		// Tests
		//
		List<ITariffOption> tariffOptions =
			clonedTariff.getTariffOptions();
		List<ITariffOption> includedTariffOptions =
			clonedtariff.getIncludedTariffOptions();
		List<ITariffOption> additionalTariffOptions =
			clonedTariff.getAdditionalTariffOptions();
		List<ITariffOption> supplementaryServiceTariffOptions =
			clonedTariff.getSupplementaryServiceTariffOptions();
		//
		// Test cloned tariff options
		//
		TestHelper.checkClonedTariffOptionsWithoutOptions(tariffOptions,
				includedTariffOptions,
				additionalTariffOptions,
				supplementaryServiceTariffOptions);
	}

	//--------------------------------------------------------------------------
	// Test copy quantities from destination tariff
	//--------------------------------------------------------------------------
	static testMethod void testCopyQuantities()
	{
		TestHelper.createTariffs();
		Id tariffId = [ SELECT Id FROM Tariff__c WHERE Parent_Tariff__c = null ].Id;
		ITariff tariff = Factory.createTariff(tariffId);
		ITariff clonedTariff = tariff.deepClone();

		clonedTariff.getAdditionalTariffOptions()[0].setQuantity(4);
		clonedTariff.getSupplementaryServiceTariffOptions()[0].setQuantity(5);

		tariff.copyQuantities(clonedTariff);

		//
		// Test
		//
		system.assertEquals(4, tariff.getAdditionalTariffOptions()[0].getQuantity());
		system.assertEquals(5, tariff.getSupplementaryServiceTariffOptions()[0].getQuantity());
	}

	//--------------------------------------------------------------------------
	// Test store tariff
	//--------------------------------------------------------------------------
	static testMethod void testStoreAndRetrievingStoredTariff()
	{
		TestHelper.createTariffs();
		Id tariffId = [SELECT Id FROM Tariff__c WHERE Parent_Tariff__c = null].Id;
		ITariff tariff = Factory.createTariff(tariffId);
		Id clonedTariffId = tariff.deepClone().store();
		ITariff clonedTariff = Factory.createTariff(clonedTariffId);
		//
		// Tests
		//
		List<ITariffOption> tariffOptions =
			clonedTariff.getTariffOptions();
		List<ITariffOption> includedTariffOptions =
			clonedTariff.getIncludedTariffOptions();
		List<ITariffOption> additionalTariffOptions =
			clonedTariff.getAdditionalTariffOptions();
		List<ITariffOption> supplementaryServiceTariffOptions =
			clonedTariff.getSupplementaryServiceTariffOptions();

		//
		// Test tariff
		//
		system.assertEquals('Test tariff', clonedTariff.getTariff().Name);
		system.assertEquals('code', clonedTariff.getTariff().Code__c);
		system.assertEquals('commercial plan', clonedTariff.getTariff().Commercial_Plan__c);
		system.assertEquals('24', clonedTariff.getTariff().Contract_Term__c);
		system.assertEquals('german description', clonedTariff.getTariff().Description_DE__c);
		system.assertEquals('english description', clonedTariff.getTariff().Description_ENG__c);
		system.assertEquals('Global', clonedTariff.getTariff().Global_Local__c);
		system.assertEquals('group', clonedTariff.getTariff().Group__c);
		system.assertEquals(12.00, clonedTariff.getTariff().One_Off_Price__c);
		system.assertEquals(6.00, clonedTariff.getTariff().Recurring_Price__c);
		system.assertEquals('Horizontals', clonedTariff.getTariff().Product_Family__c);
		system.assertEquals('Direct', clonedTariff.getTariff().Sales_Channel__c);
		system.assertEquals('Approved', clonedTariff.getTariff().Status__c);
		system.assertEquals(1.00, clonedTariff.getTariff().Volume_Start__c);
		system.assertEquals(100.00, clonedTariff.getTariff().Volume_End__c);

		//
		// Test tariff options
		//
		TestHelper.checkTariffOptions(tariffOptions,
				includedTariffOptions,
				additionalTariffOptions,
				supplementaryServiceTariffOptions);
	}

	//--------------------------------------------------------------------------
	// Test store tariff with different user.
	// Owner Id should be changed to user that is storing tariff.
	//--------------------------------------------------------------------------
	static testMethod void testStoreWithDifferentUser()
	{
		TestHelper.createTariffs();
		User testUser = TestHelper.createUser();
		Id tariffId = [ SELECT Id FROM Tariff__c WHERE Parent_Tariff__c = null ].Id;
		ITariff tariff = Factory.createTariff(tariffId);
		Id clonedTariffId;

		system.runAs(testUser)
		{
			clonedTariffId = tariff.deepClone().store();
		}

		ITariff clonedTariff = Factory.createTariff(clonedTariffId);

		//
		// Test owner id
		//
		system.assertEquals(testUser.Id, clonedTariff.getTariff().OwnerId);
	}

	//--------------------------------------------------------------------------
	// Test preparing of the tariff.
	// Status should be approved if quantities and show on quote changes.
	// On discount status should be daft.
	//--------------------------------------------------------------------------
	static testMethod void testPrepareChild()
	{
		TestHelper.createTariffs();
		Account acc = TestHelper.createAccount();
		Id tariffId = [ SELECT Id FROM Tariff__c WHERE Parent_Tariff__c = null ].Id;

		Test.startTest();

		ITariff tariff = Factory.createTariff(tariffId);
		ITariff approvedTariff = tariff.deepClone();
		ITariff draftTariff = tariff.deepClone();

		approvedTariff.getAdditionalTariffOptions()[1].setQuantity(4);
		approvedTariff.getAdditionalTariffOptions()[1].setShowOnQuote(false);
		approvedTariff.getAdditionalTariffOptions()[1].setIsSelected(true);
		approvedTariff.getAdditionalTariffOptions()[1].getRateCards()[0].setTargetUsageFee(12.56);
		approvedTariff.getAdditionalTariffOptions()[1].getRateCards()[0].setExpectedUsage(100);
		approvedTariff.getSupplementaryServiceTariffOptions()[0].setQuantity(5);
		approvedTariff.getSupplementaryServiceTariffOptions()[0].setShowOnQuote(false);
		approvedTariff.getSupplementaryServiceTariffOptions()[0].setIsSelected(true);
		approvedTariff.prepareChild(tariff, acc.Id, acc.Name, '6');

		draftTariff.getAdditionalTariffOptions()[1].setQuantity(4);
		draftTariff.getAdditionalTariffOptions()[1].setShowOnQuote(false);
		draftTariff.getAdditionalTariffOptions()[1].setIsSelected(true);
		draftTariff.getAdditionalTariffOptions()[1].getRateCards()[0].setTargetUsageFee(11.56);
		draftTariff.getAdditionalTariffOptions()[1].getRateCards()[0].setExpectedUsage(100);
		draftTariff.prepareChild(tariff, acc.Id, acc.Name, '6');

		Test.stopTest();

		//
		// Test status, account and tariff name
		//
		system.assertEquals('Approved', approvedTariff.getTariff().Status__c);
		system.assertEquals(acc.Id, approvedTariff.getTariff().Account__c);
		system.assertEquals('6', approvedTariff.getTariff().Contract_Term__c);
		system.assertEquals('Test tariff for AP Test Acct', approvedTariff.getTariff().Name);

		system.assertEquals('Draft', draftTariff.getTariff().Status__c);
		system.assertEquals(acc.Id, draftTariff.getTariff().Account__c);
		system.assertEquals('6', draftTariff.getTariff().Contract_Term__c);
		system.assertEquals('Test tariff for AP Test Acct', draftTariff.getTariff().Name);
	}
}