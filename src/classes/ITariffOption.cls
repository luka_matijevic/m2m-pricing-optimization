public interface ITariffOption
{
	//--------------------------------------------------------------------------
	// Getters
	//--------------------------------------------------------------------------
	List<IRateCard> getRateCards();

	List<IServiceFee> getServiceFees();

	//--------------------------------------------------------------------------
	// Getters / Setters
	//--------------------------------------------------------------------------
	Tariff_Option__c getTariffOption();

	Boolean getIsSelected();

	Integer getQuantity();

	Integer getDisplayQuantity();

	void setIsSelected(Boolean isSelected);

	void setQuantity(Integer quantity);

	void setDisplayQuantity(Integer quantity);

	Boolean getShowOnQuote();

	void setShowOnQuote(Boolean showOnQoute);

	String getName();

	void setName(String name);
	
	String getOptionType();

	void setOptionType(String optionType);

    String getType();

	void setType(String optionType);

	String getUnit();

	void setUnit(String unit);

	Decimal getRecurringTargetFee();

	void setRecurringTargetFee(Decimal rcf);

	Decimal getRecurringBaseFee();

	void setRecurringBaseFee(Decimal rbf);
	
	Decimal getOneTimeTargetFee();

	void setOneTimeTargetFee(Decimal rcf);

	Decimal getOneTimeBaseFee();

	void setOneTimeBaseFee(Decimal rbf);

	Boolean getCanDiscount();

	void setCanDiscount(Boolean canDiscount);

	Boolean getPerSim();

	void setPerSim(Boolean perSim);

	Boolean getIsAnyTargetFeeDiscounted();

    Decimal getEditedFee();

	void setEditedFee(Decimal rcf);
	
	Id getDefaultConfiguration();

	void setDefaultConfiguration(Id DefaultConfiguration);
	
	Boolean getIsUsed();

	void setisUsed(Boolean isUsed);
	
	Boolean getIsConfiguration();

	void setIsConfiguration(Boolean isConfiguration);
	
	Boolean getIsCustomOption();

	void setIsCustomOption(Boolean isCustomOption);


	//--------------------------------------------------------------------------
	// Clone tariff option with all rate cards and service fees
	//--------------------------------------------------------------------------
	ITariffOption deepClone();

	//--------------------------------------------------------------------------
	// Compare two tariff options
	//--------------------------------------------------------------------------
	Boolean equals(ITariffOption tariffOption);

	//--------------------------------------------------------------------------
	// Compare two tariff options ignoring quantity
	//--------------------------------------------------------------------------
	Boolean equalsWithoutQuantity(ITariffOption tariffOption);

	//--------------------------------------------------------------------------
	// Compare two tariff options ignoring configuration
	//--------------------------------------------------------------------------
	Boolean equalsWithoutConfiguration(ITariffOption tariffOption);
}