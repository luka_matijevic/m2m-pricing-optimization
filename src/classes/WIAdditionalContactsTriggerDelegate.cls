public class WIAdditionalContactsTriggerDelegate extends TriggerHandler.DelegateBase {
	    public override void prepareAfter() {
        // do any preparation here – bulk loading of data etc     
        	List<WI_Additional_Contacts__c> acList = (List<WI_Additional_Contacts__c>)Trigger.new;   
            if (Trigger.isInsert) {
                for (WI_Additional_Contacts__c ac : acList){
                	SendInitialPassword.updateAdditionalContactAndSendInitialPassword(String.valueOf(ac.Id));
                    System.enqueueJob(new queueableWIUserUpdate(String.valueOf(ac.Billing_Account__c)));
                }
            }
		}
}