global class CopyConfigAfterMigrationAsync
{
    static cscfga__Product_Configuration__c pc = null;
    static cscfga__Product_Bundle__c bundle = null;
    static cscfga__Product_Basket__c basket = null;
    static String labelConfigCopyRequestId = /*PROD'a0nb000000IV4Ny'; // UAT = */ 'a0n7E00000054WVQAY';
    static List<Account> acctList = null;
    static List<Opportunity> oppList = null;
    static Map<Id, Id> bundleBasketMap = new Map<Id, Id>();
    static cscfga__Product_Bundle__c targetBundle = null;
    static Integer bundleFieldLength = cscfga__Product_Bundle__c.Name.getDescribe().getLength();
    
    public static void createProcess() {
        //acctList = [SELECT Id, Eplus_Customer__c FROM Account WHERE Eplus_Customer__c = true];
        oppList = [SELECT Id, AccountId, HasSynchedBundle__c, CloseDate, Synched_Bundle_Id__c, RecordTypeId, Contract_Term_Per_SIM__c, Contract_Term_Per_Subscription__c FROM Opportunity WHERE Account.Eplus_Customer__c = true AND RecordType.Name = 'Global Local Commercial K2'];      
    
        for (Opportunity opp : oppList) {
            bundle = [SELECT Id, Name, cscfga__Opportunity__c FROM cscfga__Product_Bundle__c WHERE cscfga__Opportunity__c = :opp.Id LIMIT 1];
            //basket = [SELECT Id, cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE cscfga__Opportunity__c = :opp.Id LIMIT 1];
            
            //bundleBasketMap.put(bundle.Id, basket.Id);
        }
        
        //Set<Id> setColl = new Set<Id>();
        //setColl.add(labelConfigCopyRequestId);
        
        //for (Id keyId : bundleBasketMap.keySet()){
        //    cscfga.ProductConfigurationBulkActions.copyProductConfigsAsync(setColl, keyId, bundleBasketMap.get(keyId));
        //}
        
        //
        // Set target bundle
        //
        targetBundle = new cscfga__Product_Bundle__c();
        String targetName = 'Copy of ' + bundle.Name;
        if (targetName.length() > bundleFieldLength)
        {
            targetName = targetName.substring(0, bundleFieldLength);
        }
        targetBundle.Name = targetName;
        targetBundle.cscfga__Opportunity__c = bundle.cscfga__Opportunity__c;
        
        basket = new cscfga__Product_Basket__c();
        basket.Name = 'Basket for Opportunity: ' + bundle.cscfga__Opportunity__c;   // use the same opp id
        // targetBasket.cscfga__Opportunity__c is set by the page automatically (on the press of the clone button setter is called by the page)
        basket.cscfga__Basket_Status__c = 'Valid';
        
        //
        // Create bundle and basket
        //
        insert targetBundle;
        insert basket;
        //
        // Copy configurations to product bundle
        //
        List<cscfga__Product_Configuration__c> configurations =
                [ SELECT Id
                  FROM cscfga__Product_Configuration__c
                  WHERE cscfga__Product_Bundle__c = :bundle.Id
                  ORDER BY CreatedDate ASC ];

        List<Id> childIdList = cscfga.API_1.cloneConfigurations(configurations, targetBundle, basket);
        
        List<cscfga__Attribute__c> attributesProdConfigList = new List<cscfga__Attribute__c>();
        Set<String> nameSet = new Set<String>();
        nameSet.add('Account Id');
        nameSet.add('Bundle Id');
        nameSet.add('Config Id');
        for(cscfga__Attribute__c att : [SELECT cscfga__Value__c, Name, cscfga__Product_Configuration__c, Id,
                                    cscfga__Product_Configuration__r.cscfga__Product_Bundle__c, 
                                    cscfga__Product_Configuration__r.cscfga__Product_Bundle__r.cscfga__Opportunity__r.AccountId
                                    FROM cscfga__Attribute__c where cscfga__Product_Configuration__c IN: childIdList AND Name IN: nameSet]){
                                    
            if(att.Name.equalsIgnoreCase('Account Id')){
                att.cscfga__Value__c = att.cscfga__Product_Configuration__r.cscfga__Product_Bundle__r.cscfga__Opportunity__r.AccountId;
            }
            if(att.Name.equalsIgnoreCase('Bundle Id')){
                att.cscfga__Value__c = att.cscfga__Product_Configuration__r.cscfga__Product_Bundle__c;
            }
            if(att.Name.equalsIgnoreCase('Config Id')){
                att.cscfga__Value__c = att.cscfga__Product_Configuration__c;
            }
            attributesProdConfigList.add(att);
        }
        if(!attributesProdConfigList.isEmpty()){
            update attributesProdConfigList;
        }
        
        PostCopyConnectivityConfigurations connector = new PostCopyConnectivityConfigurations(targetBundle.Id);
        connector.doExecute();
    }
}