public with sharing class CleanUpCronJobsBatch implements Database.Batchable<sObject> {

	public Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator('SELECT Id FROM CronTrigger WHERE State = \'DELETED\'');
	}

	public void execute(Database.BatchableContext BC, List<sObject> scope) {
		for(sObject obj : scope) {
			System.abortJob(obj.id);
		}
	}

	public void finish(Database.BatchableContext BC) {
		System.purgeOldAsyncJobs(Date.today());
	}
}