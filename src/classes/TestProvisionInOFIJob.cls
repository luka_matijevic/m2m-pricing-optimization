@isTest
private class TestProvisionInOFIJob
{

	//--------------------------------------------------------------------------
	// Provision in OFI
	//--------------------------------------------------------------------------
    static testMethod void provisionInOFI()
    {
    	//
        // Create test data
    	//
			Account accPayer = new Account(
			Name = 'Test account', 
			BillingCity = 'Bremen', 
			BillingCountry = 'Germany', 
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße', 
			Type = 'Customer', 

			VAT__c = 1,
            Global__c = true
			
		);
		insert accPayer;
        
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1)
			, recordtypeid = Cache.getRecordTypeId('Opportunity.Commercial_Global'),
			Description = 'Test description 20 characters'
		);
		insert opp;		
		 Contact con = Test_Util.createContact(accPayer.id);
		
        Frame_Contract__c fc = new Frame_Contract__c();
        fc.contact__c = con.id;
        fc.customer__c = accPayer.id;
        fc.opportunity__c = opp.id;
         insert fc;      
        
		Billing_Account__c billingAccount = new Billing_Account__c(
			Payer__c = accPayer.Id, 
			Trial__c=false, 
			Billable__c=false,
			VAT__c = '1'
		);
		insert billingAccount;
		//
		// Set pending status
		//
		
		billingAccount.OFI_Provisioning_Status__c = 'Pending';

		update billingAccount;
		//
		// Custom settings
		//
		Email_Settings__c emailSettings = new Email_Settings__c(
				Name = 'Provision_In_OFI',
				Email__c = 'test@m2m.com',
				Subject__c = 'Test OFI',
				Body__c = 'Test OFI');
		insert emailSettings;

		MockSendEmail sendEmail = new MockSendEmail();
		Test.startTest();
		Database.executeBatch(new ProvisionInOFIJob(sendEmail));
		Test.stopTest();

		//
		// Test
		//
		billingAccount =
				[ SELECT Id, OFI_Provisioning_Status__c
				  FROM Billing_Account__c
				  LIMIT 1 ];
		Attachment[] atts =
				[ SELECT Id, Body
				  FROM Attachment
				  WHERE ParentId = :billingAccount.Id ];
		String expected = AttachmentUtils.csvBOM() + 'ALTKN,KUNNR,NAME,STRAS,ORT01,PSTLZ,LAND1,SPRAS,TELF1,TELF2,TELFX,FAX_NUMBER,SMTP_ADDR,STCEG,BANKS,BANKL,BANKN,BANKA,ZTERM,ZWELS,MANSP,VTRIEB,UPDTYP';
		System.assertEquals('In Progress', billingAccount.OFI_Provisioning_Status__c);
		System.assertEquals(1, atts.size());
		System.assert(atts[0].Body.toString().indexOf(expected) == 0);
    }
}