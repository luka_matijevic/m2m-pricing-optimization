public with sharing class RefreshForecastReportController {
	public RefreshForecastReportController(ApexPages.StandardController controller) {
		
	}

	public void refreshForecastReports(){
		Date startOfMonth = system.today().toStartOfMonth();
        Date endOfNMonth = startOfMonth.addMonths(2);
        endOfNMonth = endOfNMonth.addDays(-1);

        //=============== Delete the records to refresh
         List<billing_report__c> billingReportsForCleanupAll = [select id, Billing_Date__c from billing_report__c where Billing_Date__c  >=:startOfMonth AND Billing_Date__c <=:endOfNMonth];
    
        if(billingReportsForCleanupAll != null && billingReportsForCleanupAll .size() > 0){
            delete billingReportsForCleanupAll ;
        }
        
        //============================Order Line Item
        List<csord__Order_Line_Item__c> orderLineItems = [SELECT Id, Name, csord__Order__r.CreatedDate,csord__Order__r.csord__Status__c,
        												csord__Order__r.Order_Request__r.Account__r.name, csord__Total_Price__c,Quantity__c, 
            											Journal_Mapping__r.description__c, csord__Order__r.Order_Request__r.Billing_account__r.OFI_id__c, 
            											csord__Order__r.Order_Request__r.Billing_account__r.SM2M_Type__c 
            											FROM csord__Order_Line_Item__c 
            											where csord__Order__r.CreatedDate >=:startOfMonth AND 
            												csord__Order__r.CreatedDate <=:endOfNMonth];
        
            List<Billing_Report__c> billingReportsFromORL = new List<Billing_Report__c>();
        
            for (csord__Order_Line_Item__c orl :orderLineItems){
                Billing_Report__c billingReport = new Billing_Report__c();
                billingReport.Account__c=orl.csord__Order__r.Order_Request__r.Account__c;
                billingReport.Amount__c= orl.csord__Total_Price__c;
                billingReport.Billing_Date__c=orl.csord__Order__r.CreatedDate.date().addMonths(1).toStartOfMonth();
                billingReport.Description__c=orl.Journal_Mapping__r.description__c;
                billingReport.isBilled__c=false;
                billingReport.isForecast__c=true;
                billingReport.OFI_ID__c=orl.csord__Order__r.Order_Request__r.Billing_account__r.OFI_id__c;
                billingReport.Quantity__c=orl.Quantity__c;
                billingReport.Billing_Account_SM2M_Type__c = orl.csord__Order__r.Order_Request__r.Billing_account__r.SM2M_Type__c;
                
                billingReportsFromORL.add(billingReport);
            }
        
            insert billingReportsFromORL;

            //============================Daily Bill
        
            List<Daily_Bill_Item__c> dailyBillItems = [SELECT id,name,VO_Nr__c,Einzel_Nettobetrag__c,Re_Datum__c,Beschreibung__c,Menge__c, Daily_Bill__r.date__c from Daily_Bill_Item__c where Daily_Bill__r.date__c >=:startOfMonth AND Daily_Bill__r.date__c <=:endOfNMonth];
        
        
            Map<String, String> dbiVoNr_dbiIdMap = new Map<String, String>();
            for (Daily_Bill_Item__c orl :dailyBillItems) {
                dbiVoNr_dbiIdMap.put(orl.VO_Nr__c, orl.Id);
            }

            List<billing_account__c> billingAccounts = [Select payer__c,ofi_id__c, SM2M_Type__c from billing_account__c where ofi_id__c in :dbiVoNr_dbiIdMap.keyset()];
        
            Map<Id,billing_account__c> accountmap = new Map<Id,billing_account__c>();
            /*
            for (Daily_Bill_Item__c orl :dailyBillItems){
                for(billing_account__c ba :billingAccounts){
                    if(ba.ofi_id__c==orl.VO_Nr__c){ 
                        accountmap.put(orl.id,ba);
                    }
                }
            }*/

            for(billing_account__c ba :billingAccounts){
                if (dbiVoNr_dbiIdMap.containsKey(ba.ofi_id__c)){
                    accountmap.put(dbiVoNr_dbiIdMap.get(ba.ofi_id__c),ba);
                }
            }
            
            List<Billing_Report__c> billingReportsFromDB = new List<Billing_Report__c>();
        
            for (Daily_Bill_Item__c orl :dailyBillItems){
                Billing_Report__c billingReport = new Billing_Report__c();
        
            if (!billingAccounts.isEmpty()){
                billingReport.Account__c=accountmap.get(orl.id).payer__c;
                billingReport.Billing_Account_SM2M_Type__c = accountmap.get(orl.id).SM2M_Type__c;
            }
        
                billingReport.Amount__c= orl.Einzel_Nettobetrag__c;
                billingReport.Billing_Date__c=orl.Daily_Bill__r.date__c.toStartOfMonth();
                billingReport.Description__c=orl.Beschreibung__c;
                billingReport.isBilled__c=true;
                billingReport.isForecast__c=false;
                billingReport.OFI_ID__c=orl.VO_Nr__c;
                billingReport.Quantity__c=orl.Menge__c;
                                
                billingReportsFromDB.add(billingReport);
            }
        
            insert billingReportsFromDB;
        
        //========================================= 

        //List<Daily_Bill_Item__c> ll = [SELECT id,name,VO_Nr__c,Einzel_Nettobetrag__c,Re_Datum__c,Beschreibung__c,Menge__c, Daily_Bill__r.date__c from Daily_Bill_Item__c where Daily_Bill__r.date__c >=:startOfMonth AND Daily_Bill__r.date__c <=:endOfNMonth];
            
            
        //List<String> ofiid2= new List<string>();
        /*for (Daily_Bill_Item__c orl :dailyBillItems)
            ofiid2.add(orl.VO_Nr__c);
        */
        /*
        Map<String, String> dbiVoNr_dbiIdMap2 = new Map<String, String>();
        for (Daily_Bill_Item__c orl :dailyBillItems) {
            dbiVoNr_dbiIdMap2.put(orl.VO_Nr__c, orl.Id);
        }

        List<billing_account__c> al = [Select payer__c,ofi_id__c, SM2M_Type__c from billing_account__c where ofi_id__c in :dbiVoNr_dbiIdMap2.keyset()];
        
        Map<Id,billing_account__c> accountmap2 = new Map<Id,billing_account__c>();
        
        for (Daily_Bill_Item__c orl :dailyBillItems)
        for(billing_account__c ba :al)
        if(ba.ofi_id__c==orl.VO_Nr__c) accountmap2.put(orl.id,ba);
        */
        
        List<Billing_Report__c> brl = new List<Billing_Report__c>();
        
        for (Daily_Bill_Item__c orl :dailyBillItems){
            Billing_Report__c brc = new Billing_Report__c();
        
        if (!billingAccounts.isEmpty()){
            brc.Account__c=accountmap.get(orl.id).payer__c;
            brc.Billing_Account_SM2M_Type__c =accountmap.get(orl.id).SM2M_Type__c;
        }
        
            brc.Amount__c= orl.Einzel_Nettobetrag__c;
            brc.Billing_Date__c=orl.Daily_Bill__r.date__c.toStartOfMonth().addMonths(1);
            brc.Description__c=orl.Beschreibung__c;
            brc.isBilled__c=false;
            brc.isForecast__c=true;
            brc.OFI_ID__c=orl.VO_Nr__c;
            brc.Quantity__c=orl.Menge__c;
            
            brl.add(brc);
        } 
        
            insert brl;
        //======================================== Cleanup
        //select records for delete
        List<billing_report__c> billingReportsForCleanup = [select id, Billing_Date__c from billing_report__c where isForecast__c=true and (description__c='Aktivierunggebühr' OR description__c='Aktivierungsgebühr' OR description__c like '%Sim Gebühr%' OR description__c like 'GS%' ) AND Billing_Date__c  >=:startOfMonth AND Billing_Date__c <=:endOfNMonth];
    
        if(billingReportsForCleanup != null && billingReportsForCleanup.size() > 0){
            delete billingReportsForCleanup;
        }
	}
}