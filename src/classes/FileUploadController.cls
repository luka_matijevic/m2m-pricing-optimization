public class FileUploadController {
    
    public blob fileBody {get; set;}
    public string fileName {get; set;}
    public List<Stock__c> fileData;
    public String searchKey {get; set;}
    public ApexPages.StandardSetController sController;
    private static final Integer pageSize = 3;
    public Set<String> iccIds = new Set<String>();
    
    public FileUploadController(){
    }
    
    public PageReference uploadSims(){
        try{
            iccIds = SIMDispatchFileParser.parseArvatoExportFile(fileBody,true);
            if(!iccIds.isEmpty()){
                putDataIntoSetCtrl();
            }
        }catch(Exception e){
            if(e.getMessage().contains('Argument cannot be null')){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, 'Please select a file'));
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
            }
        }
        return null;
    }
    
    public void putDataIntoSetCtrl(){
        String query = 'Select Id,Name,ICCID__c,MSISDN__c,IMSI__c,Activation_Date__c,Runtime__c,End_date__c from stock__C where ICCID__c IN:iccIds OR MSISDN__c IN:iccIds Limit ' + (Limits.getLimitQueryRows()-Limits.getQueryRows());
        sController = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        sController.setPageSize(pageSize);
    }
    
    public List<SimWrapper> getfileData(){
        List<SimWrapper> fileContent = new List<SimWrapper>();
        if(sController != null){
            for(Stock__c stock : (List<Stock__c>)sController.getRecords()){            
                if(stock.ICCID__c != null){
                    fileContent.add(new SimWrapper(stock.ICCID__c,null));
                }else if(stock.MSISDN__c != null){
                    fileContent.add(new SimWrapper(null,stock.MSISDN__c));
                }
            }
        }
        return fileContent;
    }
    
    public Boolean hasNext {
        get {
            return sController.getHasNext();
        }
        set;
    }
    
    public Boolean hasPrevious {
        get {
            return sController.getHasPrevious();
        }
        set;
    }
    
    public Integer getPageNumber(){
        return sController.getPageNumber();
    }
    
    public Integer getTotalPages(){
        Decimal noPages = (sController.getResultSize() / sController.getPageSize());
        noPages = Math.floor(noPages) + ((Math.mod(sController.getResultSize(), sController.getPageSize()) > 0) ? 1 : 0);
        return (Integer.valueOf(noPages));
    }
    
    public void first() {
        sController.first();
    }
    
    public void last() {
        sController.last();
    }
    
    public void previous() {
        sController.previous();
    }
    
    public void next() {
        sController.next();
    }
    
    public PageReference addAllToSimList(){
        
        return null;
    }
    
    public PageReference performSearch(){
        return null;
    }
}