public class StockSynchronizationResult {
	
	public StockSynchronizationResult() {
		this.StocksToInsert = new List<Stock__c>();
		this.StocksToUpdate = new List<Stock__c>();
	}
	
	public List<Stock__c> StocksToInsert { get; set; }
	public List<Stock__c> StocksToUpdate { get; set; }
}