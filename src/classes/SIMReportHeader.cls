public class SIMReportHeader {

	public String salesChannel { get; set; }
	public Integer year { get; set; }
	public String name { get; set; }

	public SIMReportHeader(String salesChannel, Integer year, String name) {
		this.salesChannel = salesChannel;
		this.year = year;
		this.name = name;
	}

	public Boolean equals(Object obj) {
		if (obj instanceof SIMReportHeader) {
			SIMReportHeader other = (SIMReportHeader) obj;
			return salesChannel == other.salesChannel && year == other.year && name == other.name;
		}
		return false;
	}

	public Integer hashCode() {
		return (salesChannel + name).hashCode() + year;
	}
}