/*
To Schedule Prolongation_Job, It will execute everyday at 1:00AM
Schedule Job Name : Prolongation_Job
*/
global with sharing class Prolongation_Schedule  implements Schedulable {
      global void execute(SchedulableContext sc)  {
        Database.executeBatch(new Prolongation_Job());
      }
}