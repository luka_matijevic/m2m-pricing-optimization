public class CongaComposerCommunicator {
	
	public CongaComposerCommunicator() {

	}

	@future (callout=true)
    public static void generateOrderForm(String sessionIdIn, String orgIdIn, String orderRequestIdIn)
    {
    	String sessionId = sessionIdIn;
		String orgId = orgIdIn;
		String orderRequestId = orderRequestIdIn;

    	String baseUrl = getBaseURL(sessionId, orgId);
    	String parameters = getParameters(orderRequestId);

    	if (baseUrl != null && parameters != null){
            String finalUrl = baseUrl+parameters;
    		Http http = new Http();
			HttpRequest req = new HttpRequest();

			req.setEndpoint(finalUrl);
			req.setMethod('GET');
			req.setTimeout(60000);

			try{
			    HttpResponse res = http.send(req);
                Integer statusCode = res.getStatusCode();
                
                if (statusCode/100 == 2){
                    //Request status code OK
                    //Delete note
                    deleteNoteFromOrderRequest(orderRequestIdIn);
                }
                System.debug(res);
			}
			catch (Exception ex){
			    System.debug(ex);
			}
    	}
    }

    private static String getBaseURL(String sessionId, String orgId){
    	String bUrl = null;
    	if (sessionId != null && orgId != null){
    		bUrl = 'https://www.appextremes.com/apps/conga/pm.aspx?sessionId=' + sessionId;
    		bUrl = bUrl + '&serverUrl=' + URL.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/28.0/' + orgId;
    	}
    	return bUrl;
    }

    private static String getParameters(String orderRequestId){
    	String parameters = null;
    	Conga_Custom_Settings__c ccs = getParameterValues();
    	Order_Request__c ordReq = getOrderRequest(orderRequestId);
    	if (orderRequestId != null && ccs != null && ordReq != null){
    		String orderFormId = getCongaTemplateId(ccs, ordReq);
    		if (orderFormId != null){
    			parameters = '&id=' + orderRequestId;
	    		parameters += '&DefaultPDF=1&DS7=1&TemplateId='+ ccs.IoT_Order_Form_Id__c;
	    		parameters += '&QueryId=[Order]'+ ccs.Order_Query_ID__c + '?pv0=' + orderRequestId + '~pv1='+ordReq.Account__c;
	    		parameters += '&QueryId=[ConRole]'+ ccs.OppContactRole_Query_Id__c + '?pv0=' + orderRequestId + '~pv1='+ordReq.Opportunity__c;
	    		parameters += '&QueryId=[BillAcc]'+ ccs.Billing_Account_Query_Id__c + '?pv0=' + orderRequestId + '~pv1='+ordReq.Account__c;
	    		parameters += '&QueryId=[simType]'+ ccs.SIM_Type_Query_Id__c + '?pv0=' + ordReq.Opportunity__c + '~pv1=Article+Info+Set+Name+Shadow';
	    		parameters += '&QueryId=[simTypeHelp]'+ ccs.SIM_Type_Query_Id__c + '?pv0=' + ordReq.Opportunity__c + '~pv1=Article+Info+Set';
	    		parameters += '&QueryId=[discount]'+ ccs.SIM_Type_Query_Id__c + '?pv0=' + ordReq.Opportunity__c + '~pv1=Discount+value';
	    		parameters += '&QueryId=[quantity]'+ ccs.SIM_Type_Query_Id__c + '?pv0=' + ordReq.Opportunity__c + '~pv1=Tariffa';
	    		parameters += '&QueryId=[dataPerSim]'+ ccs.SIM_Type_Query_Id__c + '?pv0=' + ordReq.Opportunity__c + '~pv1=Data+per+sim';
	    		parameters += '&QueryId=[dyPool]'+ ccs.SIM_Type_Query_Id__c + '?pv0=' + ordReq.Opportunity__c + '~pv1=Dynamic+pool';
	    		parameters += '&QueryId=[ConnectPlus]'+ ccs.SIM_Type_Query_Id__c + '?pv0=' + ordReq.Opportunity__c + '~pv1=Connect+Plus';
	    		parameters += '&QueryId=[LTEO]'+ ccs.SIM_Type_Query_Id__c + '?pv0=' + ordReq.Opportunity__c + '~pv1=LTE+Option';
                parameters += '&OFN=IoT+Order+Form+-+' + ordReq.Name;
    		}
    	}
    	return parameters;
    }

    private static Order_Request__c getOrderRequest(String orderRequestIdIn){
    	Order_Request__c ordreq = null;

    	if (orderRequestIdIn != null){
    		ordreq = [SELECT Id, Name, Opportunity__c, Account__c, Frame_Contract__c from Order_Request__c where id = :orderRequestIdIn limit 1];
    	}
    	return ordreq;
    }

    private static Conga_Custom_Settings__c getParameterValues(){
    	Conga_Custom_Settings__c congaCustomSettings = [SELECT
    										Additional_Agreement_Query_ID__c,
    										Additional_Agreement_Template_ID__c,
    										Billing_Account_Query_Id__c,
    										Frame_Contract_Commercial_Template_ID__c,
    										Frame_Contract_Query_ID__c,
    										Frame_Contract_Trial_Template_ID__c,
    										OppContactRole_Query_Id__c,
    										Order_Query_ID__c,
    										Order_Template_ID__c,
    										Override_Address_Query_ID__c,
    										PBundleExport2_Query_ID__c,
    										PBundleExport_Query_ID__c,
    										Quote_Query_ID__c,
    										Quote_Template_ID__c,
    										SIM_Query_ID__c,
    										SIM_Type_Query_Id__c,
    										IoT_Order_Form_Id__c
    										FROM Conga_Custom_Settings__c limit 1];
    	return congaCustomSettings;
    }

    private static String getCongaTemplateId(Conga_Custom_Settings__c ccs, Order_Request__c ordReq){
    	String templateId = null;
    	Frame_Contract__c fc = [SELECT Id, Name from Frame_Contract__c where id = :ordReq.Frame_Contract__c limit 1];
    	if (fc.Name == 'FC - 4991'){
    		templateId = ccs.IoT_Order_Form_Id__c;
    	}
    	return templateId;
    }

    public static void addNoteToOrderRequest(String orderRequestIdIn){
        Note note = new Note();
        note.Title = 'Das Dokument wird in 1 bis 2 Minuten erstellt, bitte die Seite aktualisieren...';
        note.Body = 'Order Form is being generated.\nPlease refresh Order Request page after 1 minute.\n'+
                    'This note should be deleted automatically after order form is successfully created.\n'+
                    'Date and time: ' + System.now();
        note.ParentId = orderRequestIdIn;
        insert note;
    }

    private static void deleteNoteFromOrderRequest(string orderRequestIdIn){
        List<Note> notes = [select id from note where parentId = :orderRequestIdIn];
        if (notes != null && notes.size() > 0){
            delete notes;
        }
    }
}