@isTest
public with sharing class RefreshForecastReportControllerTest {
	static testMethod void testController(){
		Test.startTest();
		Invoice_Number__c i = new Invoice_Number__c();
        i.Name='Currrent';
        i.Value__c=100;
        
        insert i;
        
        Daily_Bill__c cc = new Daily_Bill__c();
        
        insert cc;
        
		RefreshForecastReportController rrpController = new RefreshForecastReportController(new ApexPages.StandardController(cc));
		rrpController.refreshForecastReports();
		Test.stopTest();
	}
}