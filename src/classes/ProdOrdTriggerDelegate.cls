public class ProdOrdTriggerDelegate extends TriggerHandler.DelegateBase {


    map<id,Order_Request__c> ordReqToUpdate = new map<id,Order_Request__c>();

    public override void prepareBefore() {
    
        
    }

    public override void prepareAfter() {
    
        if(trigger.isInsert || trigger.isUpdate){
            /* Start : Assigining product configurations Contract term to the order requests requests runtime*/
			
			set<id> orderIds = new set<id>();
			for(Product_Order__c pr: (list<Product_Order__c>) trigger.new){
				if(pr.Order_Request__c != null){
					orderIds.add(pr.Order_Request__c);
				}
			}
			
			if(orderIds.size() > 0){
				for(Product_Configuration_Order__c tmp : [select id,Product_Configuration__r.cscfga__Contract_Term__c, Product_Order__r.Order_Request__c from Product_Configuration_Order__c where Product_Order__r.Order_Request__r.Runtime__c = null and Product_Configuration__r.cscfga__Contract_Term__c != null and Product_Order__r.Order_Request__c In :orderIds]){
					id ordReqId;
					if(tmp.Product_Order__r.Order_Request__c != null){
						ordReqId = tmp.Product_Order__r.Order_Request__c;
					}
					
					if( ordReqId != null &&  ordReqToUpdate.get(ordReqId) == null){
						ordReqToUpdate.put(ordReqId, new Order_Request__c(id=ordReqId,Runtime__c = tmp.Product_Configuration__r.cscfga__Contract_Term__c));
					}
				}
			}
			
            
            /* End : Assigining product configurations Contract term to the order requests requests runtime*/
        }
        
    }

    public override void beforeInsert(sObject o) {
    }
    
    public override void beforeUpdate(sObject old, sObject o) {
        
    }
    
    public override void beforeDelete(sObject o) {
    }

    public override void afterInsert(sObject o) {
    }

    public override void afterUpdate(sObject old, sObject o) {
    }

    public override void afterDelete(sObject o) {
    }

    public override void afterUndelete(sObject o) {
    }

    public override void finish() {
       if(ordReqToUpdate.size() > 0){
           update ordReqToUpdate.values();
       }
    }
}