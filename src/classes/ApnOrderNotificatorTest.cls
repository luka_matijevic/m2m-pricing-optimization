@isTest
public class ApnOrderNotificatorTest {
	static testMethod void testApnOrderNotificator() {
        EmailTemplate emailTemplate = new EmailTemplate(Name = 'test_template', FolderId = '00D2600000013bE', TemplateType = 'Text', Subject = 'empty', HtmlValue = '<br/>', DeveloperName = 'APN_Order_Notification');
        insert emailTemplate;
                
        Test.startTest();
        ApnOrderNotificator apnOrder = new ApnOrderNotificator();        
        apnOrder.sendNotification(new Order_Request__c());
        Test.stopTest();
    }
}