public with sharing class MissingPrebillsController {
    
public List<Billing_Account__c> billingAccountsList {get; set;}



public Date inDate {get; set;}
public Date startDate {get; set;}
public Date endDate {get; set;}

public MissingPrebillsController(ApexPages.StandardController cntrl) {
		
		initiatePage();
		inDate = Date.parse(ApexPages.currentPage().getParameters().get('date'));
		startDate =  inDate.toStartOfMonth();
		endDate = startDate.addMonths(1);
		
		List<csam_t1__Outgoing_Message_Record__c> outid = 
			[select csam_t1__Object_Record_Id__c from csam_t1__Outgoing_Message_Record__c where csam_t1__Outgoing_Message__c in 
				(select id from csam_t1__Outgoing_Message__c where csam_t1__ObjectGraph_Callout_Handler__r.Name='Retrieve Prebill') //and csam_t1__Status__c <>'Response Received')					
					and createddate>=:startDate and createddate<:endDate];
					
		Set<String> outIdSet = new Set<String>();

		for (csam_t1__Outgoing_Message_Record__c rec : outid){ 
			outIdSet.add(string.valueOf(rec.csam_t1__Object_Record_Id__c));
		}
		
		
		endDate = inDate.toStartOfMonth();
		List<Prebill__c> inid = [select Billing_Account__c from Prebill__c where Daily_Bill__r.Date__c=:endDate];
		
		Set<String> inIdSet = new Set<String>(); 

		for (Prebill__c recpre : inid) {
			inIdSet.add(string.valueOf(recpre.Billing_Account__c));
		}

		outIdSet.removeall(inIdSet);
		
		
		billingAccountsList =  [select id, name from Billing_account__c where id in :outidSet];
	}

	public void initiatePage() {
	
	       
	}
	
	public void RetrieveMissing() {
	    endDate = inDate.toStartOfMonth();
	    
	    List<Prebill__c> inid = [select Billing_Account__c from Prebill__c where Daily_Bill__r.Date__c=:endDate and billing_account__c in :billingAccountsList];
		
		if (inid.size() == 0) {
	        Database.executeBatch(new RetrieveMissingPrebillsBatch(billingAccountsList), 1);
		}
	}
	
}