public class NewAdditionalTariffOption {
    String tariffOptionName;
    String unit;
    String optionType;
    Decimal targetPrice;
    Decimal oneTimeFee;
    Integer quantity;
    Integer displayQuantity;
    
    public String getTariffOptionName() {
        return tariffOptionName;
    }
    
    public String getUnit() {
        return unit;
    }
    
     public String getOptionType() {
        return optionType;
    }
    
    public Decimal getTargetPrice() {
        return targetPrice;
    }
    
     public Decimal getOneTimeFee() {
        return oneTimeFee;
    }
    
     public Integer getQuantity() {
        return quantity;
    }  
    
    
    public void setTariffOptionName(String tariffOptionName) {
       this.tariffOptionName = tariffOptionName;
    }
    
    public void setUnit(String unit) {
        this.unit = unit;
    }
    
     public void setOptionType(String optionType) {
        this.optionType = optionType;
    }
    
    public void setTargetPrice(Decimal targetPrice) {
        this.targetPrice = targetPrice;
    }
    
     public void setOneTimeFee(Decimal oneTimeFee) {
        this.oneTimeFee = oneTimeFee;
    }
    
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getDisplayQuantity() {
        return displayQuantity;
    }

    public void setDisplayQuantity(Integer quantity) {
        this.displayQuantity = quantity;
    }
}