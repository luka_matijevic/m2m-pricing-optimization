@isTest
public class TestProlongation_Job {

    @testSetup
    static void prepareData(){
        Account acc = Test_Util.createAccount('Account Test');
        insert acc;
        
        Opportunity opp = Test_Util.createOpportynity('Opp', acc.id, system.today(), 'Dratf','PRice',1, 1);
        opp.Description = 'Test description 20 characters';
        insert opp;
        
        Id commercialRTId = Schema.SObjectType.Frame_Contract__c.getRecordTypeInfosByName().get('Commercial').getRecordTypeId();
        Frame_Contract__c fc = new Frame_Contract__c(recordtypeId = commercialRTId);
        fc.Customer__c = acc.id;
        fc.Opportunity__c = opp.id;
        insert fc;
        
        
        Date tdy  = system.today();
        
        
        Order_Request__c orReq =         new Order_Request__c(Frame_Contract__c = fc.id, Runtime__c = 3, Shipping_City__c='Test City', Shipping_Country__c = 'Test Country', Shipping_Postal_Code__c='10000', Shipping_Street__c='Test shipping street');
  
        insert orReq;
        
        Third_Upload_File__c tuf = new Third_Upload_File__c(Name = 'TUF',Order_Request__c = orReq.id);
        insert tuf;
        
        list<Stock__C> stkLst = new list<Stock__c>();
        for(integer i=0; i < 10 ; i++){
            string name = '893407110028938747'+i;
            Stock__C st = Test_Util.createStock(name,acc.id);
            st.SM2M_Status__c = 'ACTIVE';
            st.Commercial_Date__c = tdy;
            st.Third_Upload_File__c = tuf.id;
            stkLst.add(st);
        }
        for(integer i=0; i < 2 ; i++){
            string name = '893407110028938748'+i;
            Stock__C st = Test_Util.createStock(name,acc.id);
            st.SM2M_Status__c = 'ACTIVE';
            st.Commercial_Date__c = tdy-10;
            st.Third_Upload_File__c = tuf.id;
            stkLst.add(st);
        }
        insert stkLst;
    }


    static testMethod void Prolongation_Job(){
        
        Test.startTest();
        Database.executeBatch(new Prolongation_Job());
        list<Stock__C> stkLst = new list<Stock__c>();
        stkLst = [select id,Potential_Termination_Date__c,Commercial_Date__c,SM2M_Status__c from stock__c];
        if(stkLst.size() > 0){
            Date tdy  = system.today();
            Date prolongDate  = tdy.addMonths(3);
            system.assertEquals(prolongDate,stkLst[0].Potential_Termination_Date__c);
        }
        Test.stopTest();
    }
    
    static testMethod void Prolongation_Schedule_Job(){
        
        Test.startTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        System.schedule('Prolongation_Schedule',CRON_EXP,new Prolongation_Schedule());

        list<Stock__C> stkLst = new list<Stock__c>();
        stkLst = [select id,Potential_Termination_Date__c,Commercial_Date__c,SM2M_Status__c from stock__c];
        if(stkLst.size() > 0){
            Date tdy  = system.today();
            Date prolongDate  = tdy.addMonths(3);
            system.assertEquals(prolongDate,stkLst[0].Potential_Termination_Date__c);
        }
        Test.stopTest();
    }
    
    static testMethod void Extend_Past_EndDate(){
        
        Test.startTest();
        Database.executeBatch(new Extend_Past_EndDate());
        list<Stock__C> stkLst = new list<Stock__c>();
        stkLst = [select id,Potential_Termination_Date__c,Commercial_Date__c,SM2M_Status__c from stock__c];
        Test.stopTest();
    }
    
}