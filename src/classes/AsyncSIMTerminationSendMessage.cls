public with sharing class AsyncSIMTerminationSendMessage implements Queueable{
	private SIM_Termination_Job__c jobData;
	public AsyncSIMTerminationSendMessage(SIM_Termination_Job__c jobDataIn){
		this.jobData = jobDataIn;
	}

	public void execute(QueueableContext context) {
		if (this.jobData != null){
        	csam_t1.ObjectGraphCalloutHandler.createAndSend('Modify SIM', this.jobData.Id);
		}
    }
}