/**
    Test class for ProductBundleToExcelExportController
 */

@isTest
private class TestProductBundleToExcelExportController
{
    static testMethod void testProductBundleToExcelExportController()
    {
        // Create test data
       // TestHelper.createProductBundle();
    //    cscfga__Product_Bundle__c Pbundle = [SELECT Id FROM cscfga__Product_Bundle__c];

        // Create page and controllers
      //  PageReference newPage = Page.ProductBundleToExcelExport;
    //    Test.setCurrentPage(newPage);
     //   System.currentPageReference().getParameters().put('bundleId', Pbundle.Id);
     //   String result;
      //  ApexPages.StandardController stdctrl = new ApexPages.StandardController(Pbundle);
        Test.startTest();
    //    ProductBundleToExcelExportController cont = new ProductBundleToExcelExportController(stdctrl);


        // Test ProductBundleToExcelExportController functionality
      //  result = cont.getExportData();
        Test.stopTest();
//        System.debug('Result: ' + result);
    /*    System.assertNotEquals(result.indexof('"Base Price";"Target Price"\r\n'),-1 );
        System.assertNotEquals(result.indexof('Tariff Recurring Fee";"2.49000";\r\n'),-1 );
        System.assertNotEquals(result.indexof('MR_Telefonica Data M2M Global - 24 for AP Test Acct"\r\n'),-1 );
        System.assertNotEquals(result.indexof('"SIM & Tariff"\r\n'),-1 );
        System.assertNotEquals(result.indexof('Tariff One-Time Fee";"21.50000";\r\n'),-1 );
        System.assertNotEquals(result.indexof('Data Pack M2M 1 MB Zone Europe"\r\n'),-1 );
        System.assertNotEquals(result.indexof('No of Included MB";"1"\r\n'),-1 );
        System.assertNotEquals(result.indexof('Recurring Fee";"0,4";"0,34"\r\n'),-1 );
        System.assertNotEquals(result.indexof('Additional MB - Europe Clock Rate";"kB"\r\n'),-1 );
        System.assertNotEquals(result.indexof('Additional MB - Europe Price per MBOutgoing";"0,45";\r\n'),-1 );
        System.assertNotEquals(result.indexof('Price per MB in Rest of World Clock Rate";"10 kB"\r\n'),-1 );
        System.assertNotEquals(result.indexof('Price per MB in Rest of World Price per MBOutgoing";"7,5";\r\n'),-1 );
        System.assertNotEquals(result.indexof('Data - Pay as you go"\r\n'),-1 );
        System.assertNotEquals(result.indexof('Zone - Europe Clock Rate";"kB"\r\n'),-1 );
        System.assertNotEquals(result.indexof('Zone - Europe Price per MBOutgoing";"0,45";\r\n'),-1 );
        System.assertNotEquals(result.indexof('Zone - Rest of World Clock Rate";"10 kB"\r\n'),-1 );
        System.assertNotEquals(result.indexof('Zone - Rest of World Price per MBOutgoing";"7,5";\r\n'),-1 );
        System.assertNotEquals(result.indexof('SMS - Included"\r\n'),-1 );
        System.assertNotEquals(result.indexof('SMS - Zone Europe Clock Rate";"SMS"\r\n'),-1 );
        System.assertNotEquals(result.indexof('SMS - Zone Europe Price per SMSN/A";"0,08";\r\n'),-1 );
        System.assertNotEquals(result.indexof('SMS - Zone Rest of World Clock Rate";"SMS"\r\n'),-1 );
        System.assertNotEquals(result.indexof('SMS - Zone Rest of World Price per SMSN/A";"0,49";\r\n'),-1 );
        System.assertNotEquals(result.indexof('Voice - Included"\r\n'),-1 );
        System.assertNotEquals(result.indexof('Voice In - Zone Europe Clock Rate";"Min"\r\n'),-1 );
        System.assertNotEquals(result.indexof('Voice In - Rest of World Clock Rate";"Min"\r\n'),-1 );
        System.assertNotEquals(result.indexof('Voice In - Zone Europe Price per MinIncoming";"0,07";\r\n'),-1 );
        System.assertNotEquals(result.indexof('Voice In - Rest of World Price per MinIncoming";"1,35";\r\n'),-1 );
        System.assertNotEquals(result.indexof('Voice Out - Zone Europe Clock Rate";"Min"\r\n'),-1 );
        System.assertNotEquals(result.indexof('Voice Out - Zone Europe Price per MinOutgoing";"0,24";\r\n'),-1 );
        System.assertNotEquals(result.indexof('Voice Out - Rest of World Clock Rate";"Min"\r\n'),-1 );
        System.assertNotEquals(result.indexof('Voice Out - Rest of World Price per MinOutgoing";"2,55";\r\n'),-1 );
        System.assertNotEquals(result.indexof('Special Services"\r\n'),-1 );
        System.assertNotEquals(result.indexof('Recurring Fee";"0";\r\n'),-1 );
        System.assertNotEquals(result.indexof('Smart M2M Platform One-Time Fee per Volume";"0";\r\n'),-1 );
        System.assertNotEquals(result.indexof('Smart M2M Platform Recurring Fee per Volume";"1";\r\n'),-1 );
        System.assertNotEquals(result.indexof('1/1 Voice Clock rate One-Time Fee per Volume";"0";\r\n'),-1 );
        System.assertNotEquals(result.indexof('1/1 Voice Clock rate Recurring Fee per Volume";"3";\r\n'),-1 );
        System.assertNotEquals(result.indexof('SIM One-Time Base Fee";"34.00000";\r\n'),-1 );
        System.assertNotEquals(result.indexof('MR_CarModem"\r\n'),-1 );
        System.assertNotEquals(result.indexof('One-Time Base Fee";"890.00000";\r\n'),-1 );
        System.assertNotEquals(result.indexof('Recurring Fee"\r\n'),-1 ); */
    }

    static testMethod void testProductBundleToExcelExportControllerWithoutConnections()
    {
     	// Create test data
        //TestHelper.createProductBundleWithoutConnections();
        //cscfga__Product_Bundle__c Pbundle = [SELECT Id FROM cscfga__Product_Bundle__c];

        // Create page and controllers
        //PageReference newPage = Page.ProductBundleToExcelExport;
        //Test.setCurrentPage(newPage);
        //System.currentPageReference().getParameters().put('bundleId', Pbundle.Id);
        //String result;
        //ApexPages.StandardController stdctrl = new ApexPages.StandardController(Pbundle);
        Test.startTest();
        //ProductBundleToExcelExportController cont = new ProductBundleToExcelExportController(stdctrl);
        //result = cont.getExportData();
        Test.stopTest();
     }
}