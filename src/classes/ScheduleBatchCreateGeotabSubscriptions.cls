global class ScheduleBatchCreateGeotabSubscriptions implements Schedulable {
	global void execute(SchedulableContext sc) {
		//MyBatchClass b = new MyBatchClass();
		//database.executebatch(b);
		Database.executeBatch(new BatchCreateGeotabSubscriptions(), 50);
	}
}