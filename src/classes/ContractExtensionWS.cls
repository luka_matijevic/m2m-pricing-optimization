/**
 * Global class to execute Contract extension process and update the status on Frame Contract record.
 */
global class ContractExtensionWS {
	
	/**
	 * ContractExtension call executed on click of button from FrameContract contact
	 */
	webservice static String ContractExtension (Id frameContractId, String option) {
		String sError= '';
		Frame_Contract__c fc = null;
		Frame_Contract__c parentFc = null;
		Opportunity opp = null;
		boolean noTriggerUser = false;
		No_Trigger__c notrigger = null;
		try{
			// Set trigger to inactive
			List<No_Trigger__c> noList = [Select Id, SetupOwnerId, Flag__c from No_Trigger__c where SetupOwnerId =: UserInfo.getUserId() limit 1];
			if (noList.isEmpty()) {
				noTriggerUser=true;
				notrigger = new No_Trigger__c();
				notrigger.Flag__c = true;
				notrigger.SetupOwnerId = UserInfo.getUserId();
				upsert notrigger;
			} else {
				noList[0].Flag__c = true;
				upsert noList[0];
				notrigger = noList[0];
			}
			
			fc= [Select id, Name, Extension_Counter__c, Repriced_From__c,Product_Bundle__c, Repricing_Counter__c, ContractExtensionStatus__c, LastContractExtension__c from Frame_Contract__c where id =: frameContractId];
			
			// Always reprice parent FC
			if (fc.Repriced_From__c != null) {
				parentFc = [Select id, Name, Extension_Counter__c, Repriced_From__c, Repricing_Counter__c, ContractExtensionStatus__c, LastContractExtension__c from Frame_Contract__c where id =:fc.Repriced_From__c ];
			} else {
				parentFc = fc;
			}
			opp = ContractExtentionController.processFC(fc.Id, parentFc.Id, noTrigger, option);
			
			
			if (parentFc != null && opp != null) {
				// Set trigger to inactive
				notrigger.Flag__c = true;
				upsert notrigger;
				
				if (option.contains('VVL')) {
					if (parentFc.Extension_Counter__c == null ) {
						parentFc.Extension_Counter__c = 0;
					}
					parentFc.Extension_Counter__c = parentFc.Extension_Counter__c + 1;
					parentFc.ContractExtensionStatus__c = 'Contract Extended successfully';
					parentFc.Status__c='Contract Extend Requested';
				} else if (option.contains('Repricing')) {
					if (parentFc.Repricing_Counter__c == null) {
						parentFc.Repricing_Counter__c = 0;
					}
					parentFc.Repricing_Counter__c = parentFc.Repricing_Counter__c + 1;
					parentFc.ContractExtensionStatus__c = 'Repricing successfull';
					parentFc.Status__c='Repricing Requested';
				}
				parentFc.LastContractExtension__c = system.today();
				parentFc.Repriced_To__c = opp.Id;
				update parentFc;
				
				if (parentFc.Id != fc.Id) {
					if (option.contains('VVL')){
						fc.ContractExtensionStatus__c = 'Contract Extended successfully';
						fc.Status__c='Contract Extend Requested';
					} else if (option.contains('Repricing')){
						fc.ContractExtensionStatus__c = 'Repricing successfull';
						fc.Status__c='Repricing Requested';
					}
					fc.LastContractExtension__c = system.today();
					fc.Repriced_To__c = opp.Id;
					update fc;
				}
				
				notrigger.Flag__c = false;
				upsert notrigger;
			}
			
			if (noTriggerUser) {
				delete notrigger;
			}
			
			if (opp != null) {
				sError = opp.Id;
			} else {
				sError = 'ERROR - Opportunity was not created';
			}
			return sError;
			
		} catch(Exception e) {
			sError = 'ERROR: ' + e.getMessage();
			sError = sError.replace('CloneException:[]:','');
			
			// Set trigger to inactive
			notrigger.Flag__c = true;
			upsert notrigger;
			if (fc == null) {
				fc = [Select Id, ContractExtensionStatus__c, LastContractExtension__c from Frame_Contract__c where Id=:frameContractId];
			}
			fc.ContractExtensionStatus__c = sError;
			fc.LastContractExtension__c = system.today();
			update fc;
			
			// Activate triggers & remove the created NoTrigger record (if any)
			notrigger.Flag__c = false;
			upsert notrigger; 
			if (noTriggerUser) {
				delete notrigger;
			}
			return sError;
		}
	}
}