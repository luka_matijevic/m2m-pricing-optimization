global class GenerateBicReportInboundProcessor implements csam_t1.InboundMessageProcessor {
	
	/**
	* Accepts all archiving messages (should be put before the standard Inbound Message Handler)
	* @param incomingMessage message to be processed
	* @return Boolean returns true
	*/
	public Boolean isMessageProcessible(csam_t1__Incoming_Message__c incomingMessage) {
		return incomingMessage.csam_t1__Incoming_URL_Path__c == '/csam_t1/callback/bic/generate-report';
	}

	/**
	* Assumes that the message payload is a status message from integration platform
	* 
	* @param incomingMessage message to be processed with corresponding payload attachment
	* @param oldToNewIdsMap map of old-to-new ids used when inserting in chunks to pass the references between messages
	*/
	public void processMessage(csam_t1__Incoming_Message__c incomingMessage, Map<Id, Id> oldToNewIdsMap) {
		
		String status = Constants.CSV_GENERATION_FINISHED;
		System.debug(LoggingLevel.INFO, 'processMessage');
			
		try {
			incomingMessage.csam_t1__Status__c = 'Processed';

            Attachment attachment = [
				SELECT Id, Name, Body
				FROM Attachment
				WHERE ParentId = :incomingMessage.Id
				AND Name like 'Message%' LIMIT 1
			];
			
			System.debug(LoggingLevel.INFO, 'before bicExportJobId');
			//CompressedMessage report = (CompressedMessage) InboundProcessorHelper.getDeserializedMessageAttachment(incomingMessage.Id, CompressedMessage.class);
			Id bicExportJobId = originatingBicExportJob(incomingMessage);
			System.debug(LoggingLevel.INFO, 'bicExportJobId ' + bicExportJobId);
			
			//insertBicReport(report, bicExportJobId);
			
			CallbackMessage cm = (CallbackMessage) JSON.deserialize(attachment.Body.toString(), CallbackMessage.class);
			
			if (cm.objectsToUpdate != null && cm.objectsToUpdate.size() > 0) {
			    System.debug(LoggingLevel.INFO, 'cm.objectsToUpdate.size() ' + cm.objectsToUpdate.size());
				updateBicReport(cm, bicExportJobId);
			}
			
			//BIC_Export_Job_Data__c bicToUpdate = new BIC_Export_Job_Data__c();
			//bicToUpdate = (BIC_Export_Job_Data__c)JSON.deserialize(attachment.Body.toString(), BIC_Export_Job_Data__c.class);
			//bicToUpdate.Id = bicExportJobId;
			//update bicToUpdate;
		} catch (Exception e) {
			incomingMessage.csam_t1__Status__c = 'Process Error';
			InboundProcessorHelper.insertErrorAttachment(incomingMessage, e);
			
			status = Constants.CSV_GENERATION_ERROR;
		}
		
		InboundProcessorHelper.updateStatuses(incomingMessage, status);
		
		incomingMessage.csam_t1__Processed_At__c = System.now();
		update incomingMessage;
	}

	/*private void insertBicReport(CompressedMessage report, Id objectId) {
		
		Attachment attach = new Attachment(
			ParentId = objectId,
			Name = report.name,
			ContentType = report.mediaType,
			Body = EncodingUtil.base64Decode(report.content)
		);

		insert attach;
	}*/
	
	private void updateBicReport(CallbackMessage cm, Id bicReportId) {
		Map<String, List<sObject>> objectsToUpdateMap = new Map<String, List<sObject>>();
		for (sObject objectToUpdate : cm.objectsToUpdate) {
			String objectToUpdateName = objectToUpdate.getSObjectType().getDescribe().getName();
			if (!objectsToUpdateMap.containsKey(objectToUpdateName)) {
				objectsToUpdateMap.put(objectToUpdateName, new List<sObject>());
			}
			objectToUpdate.Id = bicReportId;
			objectsToUpdateMap.get(objectToUpdateName).add(objectToUpdate);
		}

		// avoid updates in chunks with different sObject types in a collection
		for (String key : objectsToUpdateMap.keySet()) {
		    System.debug(LoggingLevel.INFO, 'update objectsToUpdateMap.get(key) STARTING');
			update objectsToUpdateMap.get(key);
		}
	}

	/**
	 * Tries to determine the originating BIC Export Job object
	 */ 
	private Id originatingBicExportJob(csam_t1__Incoming_Message__c incomingMessage) {
		List<csam_t1__Outgoing_Message_Record__c> records = InboundProcessorHelper.outgoingMessageRecords(incomingMessage);
		for (csam_t1__Outgoing_Message_Record__c record : records) {
			if (record.csam_t1__Object_Name__c == 'BIC_Export_Job_Data__c') {
				return record.csam_t1__Object_Record_Id__c;
			}
		}
		return null;
	}
}