@isTest
public class DateUtilsTest {

	@isTest static void testFormat() {
		System.assertEquals('20140201', DateUtils.formatDate(Date.newInstance(2014,2,1), 'yyyyMMdd'));
	}

	@isTest static void testDatetimeOrNow() {
		System.assertNotEquals(null, DateUtils.datetimeOrNow(null));
		System.assertEquals(Datetime.now().date(), DateUtils.datetimeOrNow(null).date());

		Datetime dt = Datetime.now();
		System.assertEquals(dt, DateUtils.datetimeOrNow(dt));
	}
	
}