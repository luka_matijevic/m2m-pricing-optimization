/*
@@Description : If Stock is associated with TUF, TUF with OR, If OR with FC, and if FC type trail, 
                Then need to change TUF's Order Request to another Order request 
                whose status=Closed and Order request's parent frame contract type "Commercial"
*/
global class Assign_commericial_FC implements Database.Batchable<sObject>{

    
    global Database.QueryLocator start(Database.BatchableContext BC){
        string typeTrial = 'Trial';
        string query = 'Select id,Third_Upload_File__c,Third_Upload_File__r.Account__C,Activation_Date__c,Commercial_Date__c from Stock__c where Third_Upload_File__r.Order_Request__r.Frame_Contract__r.Type__C=:typeTrial Order by Third_Upload_File__c';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Stock__c> sims){
        
        map<id,Third_Upload_File__c> tufToUpdate = new map<id,Third_Upload_File__c>();
        
        if(sims != null && sims.size() > 0){
            set<id> accIds = new set<id>();
            
            for(Stock__c sim : sims) {
               if(sim.Third_Upload_File__r.Account__C != null){
                   accIds.add(sim.Third_Upload_File__r.Account__C);
               } 
            }
            
            list<Frame_Contract__c> fcLst = new list<Frame_Contract__c>();
            if(accIds.size() > 0){
                fcLst = [select id, Customer__c,(select id,Status__c,Frame_Contract__r.Type__c from Order_Requests__r) from Frame_Contract__c where Customer__c In:accIds];
            }
            
            map<id, list<Frame_Contract__c>> fcByAcc = new map<id, list<Frame_Contract__c>>();
            
            for(Frame_Contract__c fc : fcLst){
                list<Frame_Contract__c> tmp = new list<Frame_Contract__c>();
                tmp.add(fc); 
                if(fcByAcc.get(fc.Customer__c) != null){
                    tmp.addAll(fcByAcc.get(fc.Customer__c));
                }
                fcByAcc.put(fc.Customer__c,tmp);
            }
            
            for (Stock__c sim : sims) {
                Id AccountId;
                if(sim.Third_Upload_File__r.Account__C != null){
                    AccountId = sim.Third_Upload_File__r.Account__C;
                }
                if(AccountId != null && ((sim.Activation_Date__c == null && sim.Commercial_Date__c !=null) || (sim.Activation_Date__c != null && sim.Commercial_Date__c !=null && sim.Commercial_Date__c >= sim.Activation_Date__c)) && fcByAcc.get(AccountId) != null && fcByAcc.get(AccountId).size() >1){
                    
                    Third_Upload_File__c tuf = new Third_Upload_File__c();
                    tuf.id = sim.Third_Upload_File__c;
                    if(tufToUpdate.get(sim.Third_Upload_File__c) != null){
                        tuf = tufToUpdate.get(sim.Third_Upload_File__c);
                    }
                    Order_Request__c ordReq;
                    for(Frame_Contract__c fc : fcByAcc.get(AccountId)){
                        for(Order_Request__c ord: fc.Order_Requests__r){
                            if(ord.Status__c != null && ord.Status__c =='Closed' && ord.Frame_Contract__r.Type__c == 'Commercial'){
                                ordReq = ord;
                                break;
                            }
                        }
                    }
                    if(AccountId != null && ordReq != null){
                        tuf.Order_Request__c = ordReq.id;
                        tufToUpdate.put(tuf.id,tuf);
                    }
                }
            }
            if(tufToUpdate.size() > 0){
                update tufToUpdate.values();
            }
        }
    }
    
    global void finish(Database.BatchableContext BC){

    }
}