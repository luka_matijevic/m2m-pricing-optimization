/**
 * Changes has new email flag on case if there is unread email
 * @author Kristijan Kosutic
 *
 */ 

global class CaseEmailStatusPoller implements Schedulable {
	
	private List<EmailMessage> unreadEmails;
	private List<EmailMessage> readEmails;
	private Map<Id,Boolean> caseWithUnreadEmails;
	
	public static void startPolling() {
		CaseEmailStatusPoller.scheduleTick(System.now().addSeconds(10));
	}
	
	global void execute(SchedulableContext sc) {
		caseWithUnreadEmails = new Map<Id,Boolean>();
		changeEmailStatusForNewEmails();
		changeEmailStatusForReadEmails();
		if (!EmailStatusSettingsHelper.isDisableEmailStatusPollerReschedule()) {
			Datetime nextTick = System.now().addSeconds(EmailStatusSettingsHelper.getEmailStatusPollerDelaySeconds());
			CaseEmailStatusPoller.scheduleTick(nextTick);
		}
		
		System.abortJob(sc.getTriggerId());
	}
	
	private static void scheduleTick(Datetime dt) {
		String chron_exp = dt.format('ss mm HH dd MM ? yyyy');
		CaseEmailStatusPoller poller = new CaseEmailStatusPoller();
		System.schedule(jobName(dt), chron_exp, poller);
	}
	
	private static String jobName(Datetime dt) {
		return 'CaseEmailStatusPoller_' + dt.getTime();
	}
	
	/**
	 * Updates case flag when there are new emails
	 */
	private void changeEmailStatusForNewEmails() {
		Set<Id> caseIds = new Set<Id>();
		unreadEmails = [Select Status,ParentId,Incoming,Parent.AccountId,Id from EmailMessage where Incoming = true and Status = '0'];
		for (EmailMessage em : unreadEmails) {
			caseIds.add(em.ParentId);
			caseWithUnreadEmails.put(em.ParentId,true);
		}
		List<Case> cases = [Select Has_new_Email__c,Contact.AccountId,AccountId, Id from Case where Id in :caseIds and status!='Closed' and status!='Closed and Canceled'];
		List<Case> casesToUpdate = new List<Case>();
		if (!cases.isEmpty()) {
			for (Case cs : cases) {
				if (cs.Has_new_Email__c == false) {
					cs.Has_new_Email__c = true;
					casesToUpdate.add(cs);
				}
			}
			if (!casesToUpdate.isEmpty()) {
				update casesToUpdate;
			}
		}
	}
	
	/**
	 * Updates case flag when email is read
	 */
	private void changeEmailStatusForReadEmails() {
		Set<Id> caseIds = new Set<Id>();
		readEmails = [Select Status,ParentId,Incoming from EmailMessage where Incoming = true and Status = '1' and Parent.Has_new_Email__c = true];
		for (EmailMessage em : readEmails) {
			caseIds.add(em.ParentId);
		}
		List<Case> cases = [Select Has_new_Email__c,Contact.AccountId,AccountId,Id from Case where Id in :caseIds and status!='Closed' and status!='Closed and Canceled'];
		List<Case> casesToUpdate = new List<Case>();
		if (!cases.isEmpty()) {
			for (Case cs : cases) {
				if (caseWithUnreadEmails.get(cs.Id) == null) {
					cs.Has_new_Email__c = false;
					casesToUpdate.add(cs);
				}
			}
			if (!casesToUpdate.isEmpty()) {
				update casesToUpdate;
			}
		}
	}
}