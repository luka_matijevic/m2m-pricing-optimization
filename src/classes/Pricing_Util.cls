public class Pricing_Util {
 
    public static List<SelectOption> contractTermItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('1','1'));
        options.add(new SelectOption('3','3'));
        options.add(new SelectOption('6','6'));
        options.add(new SelectOption('12','12'));
        options.add(new SelectOption('24','24'));
        options.add(new SelectOption('36','36'));
        options.add(new SelectOption('48','48'));
        options.add(new SelectOption('60','60'));
        return options;
    }
    
    public static List<SelectOption> approvalStatuses() {
        List<SelectOption> approvalStatuses = new List<SelectOption>();
        approvalStatuses.add(new SelectOption('Not required','Not required'));
        approvalStatuses.add(new SelectOption('Required','Required'));
        return approvalStatuses;
    }
    
    
    public static Activation_plan__c returnActivationPlanObject(List<Activation_plan__c> oppActPlans, Integer month) {
        return oppActPlans.get((month - 1) / 12);
    }
    
    public static String returnFieldName(String productName, String monthName) {
        return productName + monthName + '__c';
    }
    
    public static Integer returnCurrentQty(Activation_plan__c actPlan, String fieldName) {
        return Integer.valueOf(actPlan.get(fieldName));
    }
    
    public static Integer max(Set<Integer> numbers) {
    	Integer max = 0;
		for (Integer num : numbers) {
			if (num != null && num > max) {
				max = num;
			}
		}
		return max;
    }
    
    public static Decimal returnDecimalValue(Decimal value) {
        return value != null ? value : 0;
    }
    
    public static List<Business_comment__c> returnBusinessComments(id bundleId) {
        return [SELECT Id, Product_Bundle__c, Comment__c, CreatedBy.Name, Created_date__c, Is_deleted__c FROM Business_comment__c WHERE Product_Bundle__c = :bundleId AND Is_deleted__c = false ORDER BY Created_date__c DESC];
    }
    
    
    
    public static List<Activation_plan__c> returnActivationPlanList(id theBundleId) {
        return [SELECT Id, Year__c, Opportunity__c, Product_Bundle__c, Connectivity_Sims_January__c, Connectivity_Sims_February__c, Connectivity_Sims_March__c, Connectivity_Sims_April__c, Connectivity_Sims_May__c, Connectivity_Sims_June__c, Connectivity_Sims_July__c,
                                Connectivity_Sims_August__c, Connectivity_Sims_September__c, Connectivity_Sims_October__c, Connectivity_Sims_November__c, Connectivity_Sims_December__c FROM Activation_plan__c WHERE Product_Bundle__c = :theBundleId];
    }
    
    public static cscfga__Attribute__c setRecurringAttributePrice(cscfga__Attribute__c attribute, Decimal price){
		if(attribute != null ){
		    attribute.cscfga__Price__c = price;
    		attribute.cscfga__Value__c = string.valueOf(price);
    		attribute.cscfga__Recurring__c = true;
    		if (price != null){
    			attribute.cscfga__Is_Line_Item__c = true;
    		}
    		return attribute;
		}else {
		    return null;
		}
	}
	
	public static cscfga__Attribute__c setOneTimeAttributePrice(cscfga__Attribute__c attribute, Decimal price){
		if(attribute != null ){
		    attribute.cscfga__Price__c = price;
    		attribute.cscfga__Value__c = string.valueOf(price);
    		attribute.cscfga__Recurring__c = false;
    		if (price != null){
    			attribute.cscfga__Is_Line_Item__c = true;
    		}
    		return attribute;
		}else {
		    return null;
		}
	}
	
	public static list<SIMType> fetchConfigurationsForBundle(id theBudnleId) {
	    
	    List<SelectOption> options = new List<SelectOption>();
        map<id, Article_Info_Set__c> optionsMap = new map<id, Article_Info_Set__c>([select id,name from Article_Info_Set__c where Article_Type__c='SIM' and Global_Local__c='Global' and Status__c='Active']);
        for(Article_Info_Set__c ll :optionsMap.values() ) {
            options.add(new SelectOption(ll.id,ll.name));
        }
        
        
	    
        list<SIMType> simTypeList = new List<SIMType>();
        List<cscfga__Product_Configuration__c> pConfigurationsForBundle = [SELECT Id, cscfga__Product_Bundle__c  FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Bundle__c = :theBudnleId AND Name = 'SIM & Tariff'];
        if(pConfigurationsForBundle != null && pConfigurationsForBundle.size() > 0) {
            set<id> prodConfigIds = new set<id>();
            for(cscfga__Product_Configuration__c pc : pConfigurationsForBundle) {
                if(pc.id != null){
                    prodConfigIds.add(pc.id);
                }
            }
            map<id, List<cscfga__Attribute__c>> configMap = new map<id, List<cscfga__Attribute__c>>();
            if(prodConfigIds.size() > 0){
                for(cscfga__Attribute__c attrs : [SELECT Id, Name, cscfga__Value__c, cscfga__Product_Configuration__c FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__c = :prodConfigIds AND Name in ('Article Info Set','Quantity')]){
                    list<cscfga__Attribute__c> tmp = new list<cscfga__Attribute__c>();
                    tmp.add(attrs);
                    if(configMap.get(attrs.cscfga__Product_Configuration__c) != null){
                        tmp.addAll(configMap.get(attrs.cscfga__Product_Configuration__c));
                    }
                    configMap.put(attrs.cscfga__Product_Configuration__c,tmp);
                }
            }
            Integer i=0;
            for(cscfga__Product_Configuration__c pc : pConfigurationsForBundle) {
                SIMType s = new SIMType();
                s.isSelected = false;
                s.setProductConfigurationId(pc.id);
                s.setProductBundleId(theBudnleId);
                
                if(configMap != null && configMap.get(pc.id) != null){
                    for(cscfga__Attribute__c attr : configMap.get(pc.id)) {
                        if((s.ArticleInfo == null || s.ArticleInfo == '') && attr.Name.equalsIgnoreCase('Article Info Set') && attr.cscfga__Value__c != null) {
                            s.setArticleInfo(Id.valueOf(attr.cscfga__Value__c));
                        }else if( s.Quantity == null && attr.Name.equalsIgnoreCase('Quantity') && attr.cscfga__Value__c != null) {
                            s.setQuantity(Integer.valueOf(attr.cscfga__Value__c));
                        }
                    }
                }
                
                /*
                set<string> selectedArticleInfo = new set<string>();
                for(SimType ss : simTypeList){
                    if(ss.articleInfo != null){
                        selectedArticleInfo.add(ss.articleInfo);
                    }
                }
                List<selectOption> tmp = new list<selectOption>();
                for(selectOption so:options ){
                    string s1 = so.getValue();
                    boolean isSuccess = true;
                    for(string s2 : selectedArticleInfo){
                        if(s1 == s2){
                            isSuccess = false;
                        }
                    }
                    if(isSuccess){
                        tmp.add(so);
                    }
                }
                s.SimItems.clear();
                s.SimItems.addAll(tmp);*/
                simTypeList.add(s);
            }
            //add remaining options into each simType
            for(SIMType sType : simTypeList){
                List<selectOption> tmp = new list<selectOption>();
                for(selectOption so:options ){
                    string s1 = so.getValue();
                    boolean addOption = true;
                    for (SIMType sTypeTmp : simTypeList){
                        if (sTypeTmp != sType && sTypeTmp.articleInfo == s1){
                            addOption = false;
                        }
                    }
                    if (addOption){
                        tmp.add(so);
                    }
                }
                sType.SimItems.clear();
                sType.SimItems.addAll(tmp);
            }
        }
        
        return simTypeList;
    }
    

}