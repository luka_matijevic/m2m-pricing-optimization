public without sharing class OpportunityTriggerDelegate extends TriggerHandler.DelegateBase {

	List<Opportunity> opportunities = new List<Opportunity>();
	Map<Id,Frame_Contract__c> frameContractMap = new Map<Id,Frame_Contract__c>();
	Map<Id,Order_Request__c> orderRequestMap = new Map<Id,Order_Request__c>();
    
	
	map<id,User> stadardUserMap = new map<id, User>([select id, profileId from user where profile.Name = :label.M2M_Standard_User]);
	
	//List<caseShare> caseshareToInsert = new list<caseShare>();
	
	public override void prepareBefore() {
		// do any preparation here – bulk loading of data etc
		if (Trigger.isInsert) {
			for (Integer i = 0; i < Trigger.new.size(); i++) {
				Opportunity newOpp = (Opportunity) Trigger.new[i];
				newOpp.Total_Connectivity_Rev_in_current_year__c = M2MYearlyTotalCalculator.calculateConnectivityRevenue(newOpp.Id, Date.today().year());
				newOpp.Total_Service_Revenue_in_current_year__c = M2MYearlyTotalCalculator.calculateServiceRevenue(newOpp.Id, Date.today().year());
				newOpp.Total_Hardware_Revenue_in_current_year__c = M2MYearlyTotalCalculator.calculateHardwareRevenue(newOpp.Id, Date.today().year());
			}
		}
		else if (Trigger.isUpdate) {
			// TODO: add one-off checks
			for (Integer i = 0; i < Trigger.new.size(); i++) {
				Opportunity newOpp = (Opportunity) Trigger.new[i];
				Opportunity oldOpp = (Opportunity) Trigger.oldMap.get(newOpp.Id);

				if (oldOpp.Monthly_Revenue_Per_SIM__c		!= newOpp.Monthly_Revenue_Per_SIM__c ||
					oldOpp.Monthly_Revenue_Per_Service__c	!= newOpp.Monthly_Revenue_Per_Service__c ||
					oldOpp.Monthly_Revenue_Per_Hardware__c	!= newOpp.Monthly_Revenue_Per_Service__c ||
					oldOpp.One_Time_Revenue_Per_SIM__c		!= newOpp.Monthly_Revenue_Per_SIM__c ||
					oldOpp.One_Time_Revenue_Per_Service__c	!= newOpp.Monthly_Revenue_Per_Service__c ||
					oldOpp.Revenue_Per_Unit__c				!= newOpp.Monthly_Revenue_Per_Service__c ||
					oldOpp.Contract_Term_Per_SIM__c 		!= newOpp.Contract_Term_Per_SIM__c ||
					oldOpp.Contract_Term_Per_Subscription__c!= newOpp.Contract_Term_Per_Subscription__c ||
					oldOpp.Contract_Term_Per_Hardware__c 	!= newOpp.Contract_Term_Per_Hardware__c) {
					    if(newOpp.Record_Type_Name__c!='Fast Lane Opportunity' && newOpp.Record_Type_Name__c!='Fast Lane Standard Pricing' && newOpp.Record_Type_Name__c!='IoT Connect')
				{
						newOpp.Total_Connectivity_Rev_in_current_year__c = M2MYearlyTotalCalculator.calculateConnectivityRevenue(newOpp.Id, Date.today().year());
						newOpp.Total_Service_Revenue_in_current_year__c = M2MYearlyTotalCalculator.calculateServiceRevenue(newOpp.Id, Date.today().year());
						newOpp.Total_Hardware_Revenue_in_current_year__c = M2MYearlyTotalCalculator.calculateHardwareRevenue(newOpp.Id, Date.today().year());
				}
				}
				if(newOpp.Record_Type_Name__c!='Fast Lane Opportunity' &&  newOpp.Record_Type_Name__c!='Fast Lane Standard Pricing' && newOpp.Record_Type_Name__c!='IoT Connect' ) {
    				List<Frame_Contract__c> frameContractList = [Select Id, Name, Status__c, Opportunity__c from Frame_Contract__c where Opportunity__c in :Trigger.new];
    				for (Frame_Contract__c fc : frameContractList) {
    					frameContractMap.put(fc.Opportunity__c, fc);
    				}
				}
				else {
        				List<Order_Request__c> orList = [Select id, Name, Boni_Check_Status__c,Opportunity__c from Order_Request__c where Opportunity__c in :Trigger.new];
        				for (Order_Request__c ol: orList) {
        				    orderRequestMap.put(ol.Opportunity__c,ol);
        				}
				}
			}
			//Added by Mahaboob
		}
	}

	public override void prepareAfter() {
		if(trigger.isInsert || trigger.isUpdate){ 
		    // Giving case read permission to opportunity owner, If Oppportunity owner profile is M2M standard user - Pavan
    		
			map<id,id> accIds = new map<id,id>();
			for(Opportunity opp : (list<Opportunity>)trigger.new){
				if(stadardUserMap != null && opp.ownerId != null && stadardUserMap.containsKey(opp.ownerId) && opp.AccountId != null){
					accIds.put(opp.accountId,opp.ownerId);
				}
				 
			}
		    
            system.debug(accIds + ' accIds  '+stadardUserMap);
				
			if(accIds.size() > 0){
				database.executeBatch(new CaseSharing_Job(accIds));    
			}
        		   
    		 
		}
	}

	public override void beforeInsert(sObject o) {
		// Apply before insert logic to this sObject. DO NOT do any SOQL
		// or DML here – store records to be modified in an instance variable
		// which can be processed by the finish() method

		//Added by Mahaboob on 28th Jan 2014 for T-08974
		Opportunity newOpp = (Opportunity)o;
		//System.debug('Preliminary TCV value : '+newOpp.Preliminary_TCV__c);
		if	(newOpp.StageName != 'Prospecting' && newOpp.StageName != 'Qualification' && newOpp.Preliminary_TCV__c != null) {
			newOpp.addError('You cannot provide TCV when the Stage is other than Prospecting or Qualification.');
		}
		else if (newOpp.Preliminary_TCV__c == null) {
			newOpp.Preliminary_TCV__c = newOpp.Total_Connectivity_Revenue__c + newOpp.Total_Hardware_Revenue__c + newOpp.Total_Other_Consulting_Service__c + newOpp.Total_Service_Revenue__c;
		}
		//Done by Mahaboob
	}

	public override void beforeUpdate(sObject old, sObject o) {
		// Apply before update logic to this sObject. DO NOT do any SOQL
		// or DML here – store records to be modified in an instance variable
		// which can be processed by the finish() method

		//Added by Mahaboob on 28th Jan 2014 for T-08974
		Opportunity oldOpp = (Opportunity)old;
		Opportunity newOpp = (Opportunity)o;
		if (oldOpp.HasSynchedBundle__c == true && oldOpp.Preliminary_TCV__c != newOpp.Preliminary_TCV__c){
			newOpp.addError('You cannot change TCV for Opportunity with synchronized bundle.');
		}

		if (newOpp.StageName != 'Prospecting' && newOpp.StageName != 'Qualification' && oldOpp.Preliminary_TCV__c != newOpp.Preliminary_TCV__c && newOpp.Preliminary_TCV__c != null && newOpp.Preliminary_TCV__c > 0) {
			newOpp.addError('You cannot provide TCV when the Stage is other than Prospecting or Qualification.');
		}
		else if (
		    (newOpp.Total_Connectivity_Revenue__c != null && newOpp.Total_Connectivity_Revenue__c != 0) || (newOpp.Total_Hardware_Revenue__c != null && newOpp.Total_Hardware_Revenue__c != 0) || (newOpp.Total_Other_Consulting_Service__c != null && newOpp.Total_Other_Consulting_Service__c != 0) || (newOpp.Total_Service_Revenue__c != null && newOpp.Total_Service_Revenue__c != 0)) {
			newOpp.Preliminary_TCV__c = newOpp.Total_Connectivity_Revenue__c + newOpp.Total_Hardware_Revenue__c + newOpp.Total_Other_Consulting_Service__c + newOpp.Total_Service_Revenue__c;
		}
		//Done by Mahaboob
		
		// Opportuntiy Validation -> Cannot set Closed Won unless Opportuntiy has a Frame Contract in the final stage (Archived and sent to Customer)
			if (newOpp.StageName == 'Closed Won' && oldOpp.StageName != 'Closed Won') {
		    	
		    if (newOpp.Record_Type_Name__c!='Fast Lane Standard Pricing' && newOpp.Record_Type_Name__c!='IoT Connect' ) {
    			if (!frameContractMap.isEmpty() && frameContractMap.get(newOpp.Id) != null) {
    				if (frameContractMap.get(newOpp.Id).Status__c != 'Archived Contract And Sent To Customer') {
    					newOpp.addError('Frame Contract needs to be in Archived Contract And Sent To Customer stage before you can set Opportunity stage to Closed Won.');
    				}
    			} else {
    				newOpp.addError('Opportunity must have a Frame Contract before you can set Opportunity Stage to Closed Won.');
    			}
		    }
		    else
		    {
		        	if (!orderRequestMap.isEmpty() && orderRequestMap.get(newOpp.Id) != null) {
    				if (orderRequestMap.get(newOpp.Id).Boni_Check_Status__c != 'Accepted') {
    					newOpp.addError('BONI check status should be Approved before you can set Opportunity stage to Closed Won.');
    				}
    			} else {
    				newOpp.addError('BONI check status should be Approved before you can set Opportunity stage to Closed Won.');
    			}
		        
		    }
		    
		}
	}

	public override void beforeDelete(sObject o) {
		// Apply before delete logic to this sObject. DO NOT do any SOQL
		// or DML here – store records to be modified in an instance variable
		// which can be processed by the finish() method
	}

	public override void afterInsert(sObject o) {
		// Apply after insert logic to this sObject. DO NOT do any SOQL
		// or DML here – store records to be modified in an instance variable
		// which can be processed by the finish() method
	}

	public override void afterUpdate(sObject old, sObject o) {
		// Apply after update logic to this sObject. DO NOT do any SOQL
		// or DML here – store records to be modified in an instance variable
		// which can be processed by the finish() method
		Opportunity oldOpp = (Opportunity) old;
		Opportunity newOpp = (Opportunity) o;
		if(oldOpp.Record_Type_Name__c.equals(Constants.OPPORTUNITY_TYPE_COMMERCIAL_GLOBAL) && oldOpp.StageName != 'Closed Won' && newOpp.StageName == 'Closed Won') {
			SPINProjectHandler.upsertProjectToSpin((Opportunity) newOpp);
		}
	}

	public override void afterDelete(sObject o) {
		// Apply after delete logic to this sObject. DO NOT do any SOQL
		// or DML here – store records to be modified in an instance variable
		// which can be processed by the finish() method
	}

	public override void afterUndelete(sObject o) {
		// Apply after undelete logic to this sObject. DO NOT do any SOQL
		// or DML here – store records to be modified in an instance variable
		// which can be processed by the finish() method
	}

	public override void finish() {
	    /*system.debug('caseshareToInsert ** '+caseshareToInsert);
	    //if(caseshareToInsert.size() > 0){
	    //    database.insert(caseshareToInsert,false);
	    }*/
	}
}