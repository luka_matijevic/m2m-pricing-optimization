public class ProductBundleRedirectController {
    public cscfga__Product_bundle__c productBundle {get;set;}
    private Opportunity bundleOpportunity {get;set;}
    private Id fastLaneOppRecordTypeId {get;set;}
    private Id icapRecordTypeId {get;set;}
    private List<RecordType> fastLaneOppRecordType {get;set;}
    private List<RecordType> icapRecordType {get;set;}
    
    public ProductBundleRedirectController(ApexPages.StandardController controller) {
		// initialization is done through visual force page action
		this.productBundle = (cscfga__Product_bundle__c)controller.getRecord();

	}
	
	public PageReference init() {
	    PageReference newocp = null;
	    if(productBundle != null) {
	        productBundle = [SELECT Id, Name, cscfga__Opportunity__c FROM cscfga__Product_bundle__c WHERE Id = :productBundle.Id];
	        
	        if(productBundle != null) {
	            fastLaneOppRecordType = [SELECT Id, Name FROM RecordType WHERE Name = 'Fast Lane Opportunity'];
	            if(fastLaneOppRecordType != null) {
	                fastLaneOppRecordTypeId = fastLaneOppRecordType[0].Id;
	            }

	            icapRecordType = [SELECT Id, Name FROM RecordType WHERE Name = 'IoT Connect Advanced'];
	            if(icapRecordType != null) {
	                icapRecordTypeId = icapRecordType[0].Id;
	            }
	            
	            
	            bundleOpportunity = [SELECT Id, Name, RecordTypeId, Product_Family__c FROM Opportunity WHERE Id = :productBundle.cscfga__Opportunity__c];
	            
	            if(bundleOpportunity != null) {
	                if(bundleOpportunity.RecordTypeId == fastLaneOppRecordTypeId) {
	                    //fastlane redirect
	                    newocp = new PageReference('/apex/FastLaneOpp?oppId=' + bundleOpportunity.Id + '&id=' + productBundle.Id);
		                newocp.setRedirect(true);
	                } else if (bundleOpportunity.RecordTypeId == icapRecordTypeId){
	                    	newocp = new PageReference('/apex/IoTConnectAdvancedPricingNew?idOpp=' + bundleOpportunity.Id + '&Id=' + productBundle.Id);
	                        newocp.setRedirect(true);
	                }
	                else {
	                    //pricing main page
	                    if(bundleOpportunity.Product_Family__c=='Horizontals') {
    	                    newocp = new PageReference('/apex/pricingmainpage?oppId=' + bundleOpportunity.Id + '&id=' + productBundle.Id); 
    	                    newocp.setRedirect(true); 
	                    }
	                    else {
	                        newocp = new PageReference('/apex/pricingservicespage?oppId=' + bundleOpportunity.Id + '&Id=' + productBundle.Id);
	                        newocp.setRedirect(true);
	                    }


	                }
	            }
	        }
	    }
		
		return newocp;
	}
}