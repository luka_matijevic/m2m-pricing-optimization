//-------------------------------------------------------------------------------
// 
//-------------------------------------------------------------------------------
global class CreateOrderForAccountBatchableStepTwo implements Database.Batchable<SObject>
{
    Opportunity opportunityObj = null;
    cscfga__Product_Configuration__c pc = null;
    cscfga__Product_Bundle__c bundle = null;
    
    //---------------------------------------------------------------------------
    // Start of batch apex
    //---------------------------------------------------------------------------
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        //
        // Filter records
        //
        //List<Account> acctList = [SELECT Id, Eplus_Customer__c FROM Account WHERE Eplus_Customer__c = true];
        string opquery = 'SELECT Id, AccountId, HasSynchedBundle__c, CloseDate, Synched_Bundle_Id__c, RecordTypeId, Contract_Term_Per_SIM__c, Contract_Term_Per_Subscription__c FROM Opportunity WHERE Account.Eplus_Customer__c = true AND RecordType.Name = \'Global Local Commercial K2\' and StageName != \'Closed Won\' ';      
        return Database.getQueryLocator(opquery);
    }

    //--------------------------------------------------------------------------
    // Execute of batch apex
    //--------------------------------------------------------------------------
    global void execute(Database.BatchableContext bc, List<SObject> scope)
    {
        for (Opportunity opp : (List<Opportunity>)scope)
        {
            createProcess(opp);
        }
    }

    //--------------------------------------------------------------------------
    // Finish of batch apex
    //--------------------------------------------------------------------------
    global void finish(Database.BatchableContext bc)
    {
        
    }
    
    private void createProcess(Opportunity opp) {
        opportunityObj = opp;
        bundle = [SELECT Id, cscfga__Opportunity__c, cscfga__Synchronised_with_Opportunity__c FROM cscfga__Product_Bundle__c WHERE cscfga__Opportunity__c = :opportunityObj.Id LIMIT 1];
        system.debug('>>>>>>>>>>>>>>>>>>>>111111' + opportunityObj);
        string query_pc = 'SELECT ' +  SObjectHelper.getFieldListForSOQL('cscfga__product_configuration__c', null, null) + ' FROM cscfga__product_configuration__c where cscfga__Product_Bundle__c=\'' + bundle.Id + '\'';    
        List<cscfga__Product_Configuration__c> pclist = database.query(query_pc);
        
        pc = pclist[0];
        system.debug('>>>>>>>>>>>>>>>>>>>>22222');
        Frame_Contract__c fc = insertFrameContract(opportunityObj, bundle);
        
        bundle = upsertBundle(bundle);
            
        opportunityObj.Contract_Term_Per_SIM__c = 24;
        opportunityObj.Contract_Term_Per_Subscription__c = 0;
    
        update opportunityObj;
        
        //sync bundle
        cscfga.Api_1.syncBundleToOpportunity(bundle.Id);
            
        fc.Opportunity__c = opportunityObj.Id;
        
        update fc;
        
        string BusinessModelDescription = 'This is K2 Project Migration Order Request';
        
        Order_Request__c oc = insertOrderRequest(opportunityObj, fc, BusinessModelDescription);
        
        
        //create other related products
        Product_Order__c pcor = insertProductOrder(BusinessModelDescription, oc);
        
        Product_Configuration_Order__c cor = insertProductConfigOrder(pcor, pc);
        
        Third_Upload_File__c tuf = insertTUF(oc, opportunityObj);
        
        // Create order
        Id orderId = Factory.createCreateOrder(oc.Id).create();
        // Create subscriptions
        Factory.createCreateSubscriptions(orderId).create();
        oc.Status__c = 'Closed';
        update oc;
    }

    private Frame_Contract__c insertFrameContract(Opportunity opportunityObj, cscfga__Product_Bundle__c bundle) {
        List<Frame_Contract__c> fcs = [SELECT Id, Opportunity__c, Valid_From__c, Product_Bundle__c, Customer__c, Status__c, Send_Email_to_BSS_Inbox__c, RecordTypeId FROM Frame_Contract__c WHERE Opportunity__c = :opportunityObj.Id];
        List<OpportunityContactRole> contactRoles = [SELECT Id, OpportunityId, ContactId FROM OpportunityContactRole WHERE OpportunityId = :opportunityObj.Id];
        
        if(fcs != null && fcs.size() > 0)
            return fcs[0];
        
        Frame_Contract__c fc = new Frame_Contract__c(Opportunity__c = opportunityObj.Id,
             Valid_From__c = opportunityObj.CloseDate,
             Product_Bundle__c = bundle.Id,
             Customer__c = opportunityObj.AccountId,
             Status__c = 'Archived Contract And Sent To Customer',
             Send_Email_to_BSS_Inbox__c = false,
             Contact__c = contactRoles[0].ContactId);
             
        if(opportunityObj.RecordTypeId == Cache.RecordTypeId('Opportunity.Trial Local') || opportunityObj.RecordTypeId == Cache.RecordTypeId('Opportunity.Trial Global')){
            fc.RecordTypeId = Cache.RecordTypeId('Frame_Contract__c.Trial');
        }
        else if(opportunityObj.RecordTypeId == Cache.RecordTypeId('Opportunity.Global Local Commercial K2') || opportunityObj.RecordTypeId == Cache.RecordTypeId('Opportunity.Commercial Global')){
            fc.RecordTypeId = Cache.RecordTypeId('Frame_Contract__c.Commercial');
        }
        
        insert fc;
        
        return fc;
    }

    private cscfga__Product_Bundle__c upsertBundle(cscfga__Product_Bundle__c bundle) {
        if (bundle.cscfga__Opportunity__c == null)  
        {
            bundle.cscfga__Synchronised_with_Opportunity__c = true;
            bundle.cscfga__Opportunity__c = opportunityObj.Id;
        }
        
        upsert bundle;
        
        return bundle;
    }
    
    private Order_Request__c insertOrderRequest(Opportunity opportunityObj, Frame_Contract__c fc, string BusinessModelDescription) {
        //create order request and connect it with opportunity and its attributes
        List<Order_Request__c> ocList = [SELECT Id, Business_Model_Description__c, Account__c, Opportunity__c, Billing_Account__c, Boni_Check__c FROM Order_Request__c WHERE Frame_Contract__c = :fc.Id];
        List<OpportunityContactRole> contactRoles = [SELECT Id, OpportunityId, ContactId FROM OpportunityContactRole WHERE OpportunityId = :opportunityObj.Id];
        
        if(ocList != null && ocList.size() > 0)
            return ocList[0];
        
        Order_Request__c oc = new Order_Request__c(Frame_Contract__c = fc.Id);
        
        oc.Business_Model_Description__c = BusinessModelDescription;
        oc.Account__c = opportunityObj.AccountId;
        oc.Contact__c = contactRoles[0].ContactId;
        oc.Opportunity__c = opportunityObj.Id;
        
        List<Billing_Account__c> bics = [select id,name from Billing_account__c where Payer__c = :opportunityObj.Accountid and sm2m_type__c = 'Local' limit 1];
        Billing_Account__c bic = new Billing_Account__c();
        
        if (bics != null && bics.size() > 0) {
            bic = bics[0];
            oc.Billing_Account__c = bic.id;
        }
        system.debug(':opportunityObj.Accountid: '+ opportunityObj.Accountid);
        List<Boni_Check__c> accountBoniChecks = [SELECT Id, CreatedDate FROM Boni_Check__c WHERE Account__c = :oc.Account__c ORDER BY CreatedDate DESC];
        if (accountBoniChecks != null && accountBoniChecks.size() > 0)
        {
            oc.Boni_Check__c = accountBoniChecks[0].Id;
            system.debug('oc.Boni_Check__c: ' + oc.Boni_Check__c);
        }
        else
        {
            Boni_Check__c bCheck = new Boni_Check__c();
            
            bCheck.Account__c = oc.Account__c;
            bCheck.Name = 'CCR for ' + oc.Name;
    		bCheck.Requisition_Note__c = 'Credit Check Request for the customer order '+ oc.Name;
    		bCheck.Legitimate_Interest__c = true;
    		bCheck.Premium_Services_0900__c = true;
    		bCheck.International_Roaming_Voice__c = true;
    		bCheck.Status_of_CC__c = BoniCheckController.BONICHECK_STATUS_ACCEPTED;
    		bCheck.Comments__c = 'comment';
    
    		bCheck.Number_Requested_SMS__c = 1;
    		bCheck.Requested_Units_HW__c = 1;
    		bCheck.Number_Requested_VAS__c = 1;
    
    		bCheck.Comment_Fix_SMS__c = 'commentsSIM';
    		bCheck.Comment_Fix_HW__c = 'commentsHW';
    		bCheck.Comment_Fix_VAS__c = 'commentsVAS';
    
    		bCheck.Fix_Revenue_SMS__c = 1;
    		bCheck.Fix_Revenue_HW__c = 1;
    		bCheck.Fix_Revenue_VAS__c = 1;
    
    		bCheck.Variable_Revenue_SMS__c = 1;
    		bCheck.Variable_Revenue_HW__c = 1;
    		bCheck.Variable_Revenue_VAS__c = 1;
            
            insert bCheck;
            system.debug('bCheck.Id: ' + bCheck.Id);
            oc.Boni_Check__c = bCheck.Id;
        }
        //Luka 19.05.2016 - validation on shipping address fields
        oc.use_account_address__c = true;
        insert oc;
        
        return oc;
    }
    
    private Product_Order__c insertProductOrder(string BusinessModelDescription, Order_Request__c oc) {
        List<Product_Order__c> poList = [SELECT Id, Comment__c, Order_Request__c FROM Product_Order__c WHERE Order_Request__c = :oc.id];
        
        if(poList != null && poList.size() > 0) {
            return poList[0];
        }
        
        Product_Order__c pcor = new Product_Order__c();
        pcor.Comment__c = BusinessModelDescription;
        pcor.Order_Request__c = oc.id;       
            
        upsert pcor;
        
        return pcor;
    }
    
    private Product_Configuration_Order__c insertProductConfigOrder(Product_Order__c pcor, cscfga__Product_Configuration__c pc) {
        List<Product_Configuration_Order__c> corList = [SELECT Id, Product_Configuration__c, Product_Order__c FROM Product_Configuration_Order__c WHERE Product_Configuration__c = :pc.Id AND Product_Order__c = :pcor.Id];
        
        if(corList != null && corList.size() > 0)
            return corList[0];
        
        Product_Configuration_Order__c cor = new Product_Configuration_Order__c();
        
        cor.Product_Configuration__c = pc.id;
        cor.Product_Order__c = pcor.id;
        
        upsert cor;
        
        return cor;
    }
    
    private Third_Upload_File__c insertTUF(Order_Request__c oc, Opportunity opportunityObj) {
        Third_Upload_File__c tuf = new Third_Upload_File__c();
        tuf.SIM_Article_Type__c = 'a15b0000005l3ML';  /*UAT =  'a157E000000Fa4v';*/
        tuf.Order_Request__c = oc.id; 
        tuf.Account__c = opportunityObj.AccountId ;
        tuf.Type__c = 'Local';
        
        upsert tuf;
        
        return tuf;
    }
}