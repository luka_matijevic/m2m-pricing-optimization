@isTest
public class ArchiveFilesMessageInboundProcessorTest {
	static testMethod void testIsMessageProcessible() {
		ArchiveFilesMessageInboundProcessor proc = new ArchiveFilesMessageInboundProcessor();
		System.assertEquals(true, proc.isMessageProcessible(new csam_t1__Incoming_Message__c(csam_t1__Incoming_URL_Path__c = '/csam_t1/callback/archiving/archive')));
	}	
}