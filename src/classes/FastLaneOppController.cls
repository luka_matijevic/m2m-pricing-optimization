public class FastLaneOppController  { 
	
	public cscfga__Product_Bundle__c theBundle {get;set;}
	public cscfga.ProductConfiguration productConfiguration {get; set;}

	public List<cscfga.Attribute> screenAttributes {get;set;}
	public List<cscfga.Attribute> itemNameAttributes {get;set;}
	public List<cscfga.Attribute> itemValueAttributes {get;set;}
	public List<cscfga.Attribute> itemValueRecAttributes {get;set;}
	public List<cscfga.Attribute> hiddenAttributes {get;set;}
	public List<FLOWrapper> lineItems {get; set;} 
	public Opportunity opp  {get;set;}
	
	private ApexPages.StandardController standardController;
	private cscfga__Product_Definition__c productDefinition;
	private cscfga__Product_Configuration__c pc;
	private cscfga.Api_1.ApiSession apiSession;
	private cscfga.ProductConfiguratorController pcController;
	
	public FastLaneOppController(ApexPages.StandardController controller) {
		
		// initialization is done through visual force page action
		standardController = controller;
		
	}
	public PageReference cancelBundle()
	{   
		// return to opportunity page
		string oppId = ApexPages.currentPage().getParameters().get('oppId');
		
		PageReference newocp = new PageReference('/' + oppId);
		newocp.setRedirect(true);
		
		return newocp;
	}
	public PageReference saveBundleAndCreateContract()
	{      
		try
		
		{
			//check bundle validity
			string oppId = ApexPages.currentPage().getParameters().get('oppId');
			
			if (theBundle.cscfga__Opportunity__c == null)
			{
				addMessage('You need to save bundle before you can continue with the Order Creation process.', ApexPages.severity.ERROR);
				return ApexPages.currentPage();
			}
			else if (opp.HasSynchedBundle__c == true)
			{
				addMessage('The Order for this Fast Lane Opportunity is already created.', ApexPages.severity.ERROR);
				return ApexPages.currentPage();
			}
			else
			{
				
				//save bundle
				if (theBundle.cscfga__Opportunity__c == null)  
				{
					theBundle.cscfga__Synchronised_with_Opportunity__c = true;
					theBundle.cscfga__Opportunity__c = oppId;
				}
				
				upsert theBundle;
				
				pc.cscfga__Product_Bundle__c=theBundle.id;
				//pc.Original_Product_Name__c = 'Fast Lane Opportunity';

				pc.Original_Product_Name__c = 'Connectivity'; //Changed Original product name from Fast Lane Opportunity to Connectivity  -- Pavan 

				
				
				//save configuration
				productConfiguration.setOppId(oppId);
				
				apiSession.setBundle(theBundle);
				pc.cscfga__Product_Bundle__c = theBundle.id;
				
				apiSession.updateConfig();
				apiSession.persistConfiguration(true);
				
				for (cscfga.Attribute anAttribute : productConfiguration.getAttributes()){
					cscfga__Attribute__c att = anAttribute.getSObject();
					if (att.Name=='Total All Recurring Fees')
					opp.Monthly_Revenue_Per_SIM__c=decimal.valueof(anAttribute.getValue());
					if (att.Name=='Activation Fee Lookup')
					opp.One_Time_Revenue_Per_SIM__c=decimal.valueof(anAttribute.getValue());
				}
				
				opp.Contract_Term_Per_SIM__c = 24;
				opp.Contract_Term_Per_Subscription__c = 0;
				
				update opp;
				
				
				//sync bundle
				cscfga.Api_1.syncBundleToOpportunity(theBundle.id);
				
				// PRODUCTION : 
				string query = 'select id,name from frame_contract__c where name=\'FC - 4991\''; 
				
				// UAT:
				//string query = 'select id,name from frame_contract__c where name=\'FC - 4914\'';
				
				//connect with open frame contract
				Frame_Contract__c cont = Database.query(query);
				
				cont.Opportunity__c = oppId;
				
				update cont;
				
				//create order request and connect it with opportunity and its attributes
				Order_Request__c oc = new Order_Request__c(Frame_Contract__c = cont.id);
				
				string BusinessModelDescription = '';
				for (cscfga.Attribute anAttribute : productConfiguration.getAttributes()){
					cscfga__Attribute__c att = anAttribute.getSObject();
					if (att.Name=='Tariffaa' || att.Name=='Tariff Options' || att.Name=='SIMType' )                        
					BusinessModelDescription=BusinessModelDescription+' '+anAttribute.getValue();
                    if (att.Name=='Quantity')
                    {
                        BusinessModelDescription=BusinessModelDescription+' ' +anAttribute.getValue()+ ' SIMs';
                    }
                    if (att.Name=='Dynamic pool')
                    {
                        if (anAttribute.getValue()=='Yes') BusinessModelDescription = BusinessModelDescription + ' with dynamic pooling';
                            else  BusinessModelDescription = BusinessModelDescription + ' without dynamic pooling';
                    }
				}
				
				oc.Business_Model_Description__c=BusinessModelDescription;
				oc.Account__c=opp.AccountId ;
				
				oc.Opportunity__c=oppId;
                Account acc = [select id, VAT__c, Language__c, Billing_Cycle__c, Currency__c, ORV_Customer__c, BillingCountry from Account where id=:opp.AccountId ];
                AccountHelper.PopulateAccountSM2MInformation(acc);
				AccountHelper.CreateBillingAccount(cont);
				List<Billing_Account__c> bics = [select id,name from Billing_account__c where Payer__c=:opp.Accountid limit 1];
				Billing_Account__c bic = new Billing_Account__c();
				
				if (bics != null && bics.size() > 0) {
					bic = bics[0];
					oc.Billing_Account__c=bic.id;
				}
				
				insert oc;
				
				//create other related products
				Product_Order__c pcor = new Product_Order__c();
				pcor.Comment__c=BusinessModelDescription;
				pcor.Order_Request__c=oc.id;       
				
				for (cscfga.Attribute anAttribute : productConfiguration.getAttributes()){
					cscfga__Attribute__c att = anAttribute.getSObject();
					if (att.Name=='Quantity')
					pcor.Quantity__c=integer.valueof(anAttribute.getValue());
				}
				
				upsert pcor;
				
				Product_Configuration_Order__c cor = new Product_Configuration_Order__c();
				
				cor.Product_Configuration__c=pc.id;
				cor.Product_Order__c=pcor.id;
				
				upsert cor;
				
				
				Third_Upload_File__c tuf = new Third_Upload_File__c();
				tuf.SIM_Article_Type__c = 'a15b0000000Lva7AAC';
				tuf.Order_Request__c = oc.id; 
				tuf.Account__c=opp.AccountId ;
				
				upsert tuf;
				
				
				//redirect to newly created M2M order request
				PageReference newocp = new PageReference('/'+oc.id);
				newocp.setRedirect(true);
				
				return newocp;
			}
			
			return null;
			
		}
		
		catch(Exception e) {
			System.debug('An exception in Fast Lane initialization occurred: ' + e.getMessage());
			return null;
		}

	}
	
	public PageReference saveBundle()
	{      
		string oppId = ApexPages.currentPage().getParameters().get('oppId');
		
		if (opp.HasSynchedBundle__c == true)
		{
			addMessage('This Fast Lane Opportunity is already saved and has synced with Product Bundle.', ApexPages.severity.ERROR);
			return ApexPages.currentPage();
		}
		else
		{
			
			if (theBundle.cscfga__Opportunity__c == null)  
			{
				theBundle.cscfga__Synchronised_with_Opportunity__c = true;
				theBundle.cscfga__Opportunity__c = oppId;
			}
			upsert theBundle;
			
			pc.cscfga__Product_Bundle__c=theBundle.id;
			productConfiguration.setOppId(oppId);
			
			apiSession.setBundle(theBundle);
			pc.cscfga__Product_Bundle__c = theBundle.id;
			
			apiSession.updateConfig();
			apiSession.persistConfiguration(true);
			
			PageReference newocp = new PageReference('/' + oppId);
			newocp.setRedirect(true);
			
			return newocp;
		}
	}
	
	public PageReference quicksaveBundle()
	{      
		string oppId = ApexPages.currentPage().getParameters().get('oppId');
		
		
		if (opp.HasSynchedBundle__c == true)
		{
			addMessage('This Fast Lane Opportunity is already saved and has synced with Product Bundle.', ApexPages.severity.ERROR);
			return ApexPages.currentPage();
		}
		else
		{
			
			if (theBundle.cscfga__Opportunity__c == null)  
			{
				theBundle.cscfga__Synchronised_with_Opportunity__c = true;
				theBundle.cscfga__Opportunity__c = oppId;
			}
			upsert theBundle;
			
			pc.cscfga__Product_Bundle__c=theBundle.id;
			productConfiguration.setOppId(oppId);
			
			apiSession.setBundle(theBundle);
			pc.cscfga__Product_Bundle__c = theBundle.id;
			
			apiSession.updateConfig();
			apiSession.persistConfiguration(true);
			
			PageReference ref=ApexPages.currentPage();
			
			Map<String,String> currentPageParameters = ref.getParameters();
			
			addMessage('Pricing succesfully saved.', ApexPages.severity.INFO);
			
			PageReference nextPage = new PageReference('/apex/FastLaneOpp?oppId='+currentPageParameters.get('oppId')+'&id='+theBundle.id);
			
			nextPage.setRedirect(false);
			
			return nextPage;
			
		}
	}
	
	
	public void init()
	{
		//initialization for the new bundle/productconfiguraion process
	//	try
	//	{
			string query = 'select id,name from cscfga__Product_Definition__c where name=\'FLO Connectivity\'';
			productDefinition = Database.query(query);
			
			string oppId = ApexPages.currentPage().getParameters().get('oppId');
			
			query = 'select id,name, AccountId, HasSynchedBundle__c, RecordType.Name, Account.Name, Account.BillingCountry, Account.BillingStreet , Account.BillingCity , Account.BillingPostalCode from Opportunity where id=\''+oppId+'\'';
			opp = Database.query(query);
			
			itemNameAttributes =  new List<cscfga.Attribute>();
			itemValueAttributes =  new List<cscfga.Attribute>();
			itemValueRecAttributes =  new List<cscfga.Attribute>();
			screenAttributes =  new cscfga.Attribute[10];
			hiddenAttributes =  new List<cscfga.Attribute>();
			lineItems = new List<FLOWrapper>();
			
			string bundlid =  ApexPages.currentPage().getParameters().get('id');

			
			if (bundlid==null)
			{
				theBundle = (cscfga__Product_Bundle__c) standardController.getRecord();
				apiSession = cscfga.Api_1.getApiSession(productDefinition);
                
                if (theBundle.cscfga__Opportunity__c == null)  
    			{
    				//theBundle.cscfga__Synchronised_with_Opportunity__c = true;
    				theBundle.cscfga__Opportunity__c = oppId;
    			}
    			upsert theBundle;
    		    productConfiguration = apiSession.getConfiguration();
    			
    			pc =  productConfiguration.getSObject();
    			
    			pc.cscfga__Product_Bundle__c=theBundle.id;
    			productConfiguration.setOppId(oppId);
    			
    			apiSession.setBundle(theBundle);
    			pc.cscfga__Product_Bundle__c = theBundle.id;
    			

    			apiSession.persistConfiguration(true);
    		    apiSession.setConfigurationToEdit(pc);
                
			}
			else
			{
				string query_pc = 'SELECT ' +  SObjectHelper.getFieldListForSOQL('cscfga__product_configuration__c',null,null) + ' FROM cscfga__product_configuration__c where cscfga__product_bundle__c=\''+bundlid+'\'';    
				List<cscfga__Product_Configuration__c> pclist = database.query(query_pc);   
				pc = pclist[0];
				apiSession = cscfga.Api_1.getApiSession(pc);
				
				query_pc = 'SELECT ' +  SObjectHelper.getFieldListForSOQL('cscfga__product_bundle__c',null,null) + ' FROM cscfga__product_bundle__c where id=\''+bundlid+'\'';    
				List<cscfga__Product_Bundle__c> pblist = database.query(query_pc);   
				theBundle = pblist[0];
				
				apiSession = cscfga.Api_1.getApiSession(pc);
				
				
			}
			
			productConfiguration = apiSession.getConfiguration();
			
			
			pc =  productConfiguration.getSObject();
			pc.cscfga__Configuration_Offer__c = null;
			
			for (cscfga.Attribute anAttribute : productConfiguration.getAttributes()){
				cscfga__Attribute__c att = anAttribute.getSObject();
				
				
				if(att.Name.Contains('Name')){
					FLOWrapper fwrapper = new FLOWrapper();
					fwrapper.attName=anAttribute;
					for (cscfga.Attribute anAttribute2 : productConfiguration.getAttributes()){
						cscfga__Attribute__c att2 = anAttribute2.getSObject();
						if (att.Name.Replace('Name','Total')==att2.Name) {
							if (att2.cscfga__Recurring__c==TRUE){
								fwrapper.nonRecCharge=anAttribute2;
							}
							else if (att2.cscfga__Recurring__c==FALSE){
								fwrapper.recCharge=anAttribute2;
							}
						}
						if (att.Name=='Quantity'){
							fwrapper.quantity=integer.valueof(anAttribute2);
						}
						if (att.Name.Replace('Name','TCV')==att2.Name){
							fwrapper.TCV=anAttribute2;
						}
					}
					lineItems.add(fwrapper); 
				}
				else if (!att.Name.Contains('Name') && !att.Name.Contains('Total') && !att.Name.Contains('TCV') && !test.isRunningTest()) {
				screenAttributes.add(Integer.valueOf(anAttribute.getDefinition().cscfga__Row__c), anAttribute); 
				}
			}
			
			
	//	}
		
	//	catch(Exception e) {
	//		System.debug('An exception in Fast Lane initialization occurred: ' + e.getMessage());
	//	}
        
	}

	private void addMessage(String message, ApexPages.severity severity) {
		ApexPages.addMessage(new ApexPages.Message(severity, message));
	}

	public class FLOWrapper
	{
	    //wrapper class used for live view table
	    //gets many attributes in one collection for line items presentation
		public cscfga.Attribute attName {get;set;} 
		public cscfga.Attribute recCharge {get;set;} 
		public cscfga.Attribute nonRecCharge {get;set;}
		public cscfga.Attribute TCV {get;set;}
		integer quantity {get; set;}
		public FLOWrapper()
		{

		}
	}

}