@isTest
public class GeotabUtilsTest {
	public static testMethod void testGeotabUtils() {
        Account account = new Account(
            Name = 'Test3',
            Type = 'Business'
        );        	
        insert account;
        
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1),
			AccountId = account.Id,
            Description = 'Test description 20 characters'
		);        
        insert opp;
        
        Boni_Check__c boniCheck = new Boni_Check__c(Status_of_CC__c='Accepted', Account__c = account.Id);
        insert boniCheck;

        Geotab_Contact__c geotabContact = new Geotab_Contact__c();
        geotabContact.Geotab_Contact_Name__c = 'test name';
        geotabContact.Geotab_Id__c = 'testId';
        insert geotabContact;

        Geotab_Warranty_Option__c warrantyOption = new Geotab_Warranty_Option__c();
        warrantyOption.Geotab_Id__c = 'testId';
        warrantyOption.Geotab_Warranty_Name__c = 'name test';
        insert warrantyOption;

        Order_Request__c m2mOrdReq = new Order_Request__c(Boni_Check__c = boniCheck.Id, Opportunity__c = opp.Id, Shipping_City__c='Test City', Shipping_Country__c = 'Test Country', Shipping_Postal_Code__c='10000', Shipping_Street__c='Test shipping street');
        m2mOrdReq.Geotab_Contact__c = geotabContact.Id;
        m2mOrdReq.Geotab_Warranty_Option__c = warrantyOption.Id;
        insert m2mOrdReq;
        
        test.startTest();
		GeotabUtils.PostOrder(m2mOrdReq.Id);
        GeotabUtils.InsertGeotabOrderLog(m2mOrdReq.Id, 'test log');
        
        GeotabUtils.ApiOrderHeader aoh = new GeotabUtils.ApiOrderHeader();
        aoh.Comments = 'test';
        aoh.DevicePlanLevel = 1;
		aoh.ForAccount = 'TEF';
		aoh.InternalComments = 'test';
		aoh.LeadId = 'TEF';
		aoh.LeadIdZip = 'TEF';
		aoh.OrderItems = null;
		aoh.PromoCode = 'TEF';
		aoh.PurchaseOrderNo = 'TEF';
		aoh.ResellerReference = 'TEF';
		aoh.ShipToId = 1;
		aoh.ShippingFeeId = 1;
		aoh.WarrantyOptionId = 1;
        
        GeotabUtils.ApiOrderItem aoi = new GeotabUtils.ApiOrderItem();        
        aoi.ProductCode = 'test';
		aoi.Quantity = 1;
                
        GeotabUtils.AuthenticationResponseClass arc = new GeotabUtils.AuthenticationResponseClass();
        arc.userId = 'testid';
		arc.sessionId = 'testsession';
		arc.lastLogonDate = Datetime.newInstance(2016, 10, 14);
		arc.accounts = null;
		arc.name = 'testname';
		arc.roles = null;
        
        
        GeotabUtils.GeotabOnlineOrder goo = new GeotabUtils.GeotabOnlineOrder();
        goo.Account = null;
		goo.Comments = 'test comment';
		goo.CurrentStatus = 'test status';
		goo.OnlineOrderItems = null;
		goo.OrderDate = '2016-21-02';
		goo.OrderMonthlyTotalUsd = 0;
		goo.OrderNo = '1';
		goo.OrderTotalUsd = 1;
		goo.OrderedBy = '2';
		goo.PurchaseOrderNo = '222';
		goo.RateCode = '2233';
		goo.ResellerReference = 'reseller';        
        
        GeotabUtils.GeotabDeviceOrderEntry gdoe = new GeotabUtils.GeotabDeviceOrderEntry();
        gdoe.Account = null;
		gdoe.Comments = 'test';
		gdoe.device = null;
		gdoe.DevicePlanLevel = 1;
		gdoe.PurchaseOrderNo = '121231';
		gdoe.RateCode = 'rate';        
        
        GeotabUtils.GeotabDevice gd = new GeotabUtils.GeotabDevice();
        gd.Id = 12;
		gd.SerialNumber = '23222';
                
        GeotabUtils.GeotabUserContact guc = new GeotabUtils.GeotabUserContact();
        guc.Active = true;
		guc.Address = 'nowhere 39';
		guc.City = 'nowhere 39';
		guc.ContactEmail = 'nothing@bing.com';
		guc.ContactName = 'NameSurname';
		guc.Country = 'Croatia';
		guc.DisplayName = 'displayname';
		guc.Id = 2;
		guc.State = 'State';
		guc.Street1 = 'Street1';
		guc.Street2 = 'Street2';
		guc.Telephone1 = '09109209';
		guc.Telephone2 = '422444224';
		guc.UserCompany = null;
		guc.ZipCode = '10000';
                
        GeotabUtils.GeotabUserCompany gucm = new GeotabUtils.GeotabUserCompany();
        gucm.Account = null;
		gucm.Active = true;
		gucm.Id = 1;      
        
        GeotabUtils.GeotabAccount ga = new GeotabUtils.GeotabAccount();
        ga.AccountId = '1';
        
        GeotabUtils.GeotabShippingFee gsf = new GeotabUtils.GeotabShippingFee();
        gsf.Name = 'name';
		gsf.Code = 'code';
		gsf.Id = 2;       
                
        GeotabUtils.GeotabWarrantyOption gwo = new GeotabUtils.GeotabWarrantyOption();
        gwo.Name = 'name';
		gwo.Id = 2;
                
        GeotabUtils.GeotabCurrency gc = new GeotabUtils.GeotabCurrency();
        gc.Code = 'code';        
        
        GeotabUtils.AuthenticationRoles ar = new GeotabUtils.AuthenticationRoles();
        ar.comments = 'ssss';
		ar.name = 'fsfsd';
                
        GeotabUtils.InstallLogsClass ilc = new GeotabUtils.InstallLogsClass();
        ilc.request = null;
		ilc.resultDateUtc = Datetime.newInstance(2016, 4, 5);
		ilc.lastServerCommunication = Datetime.newInstance(2016, 5, 4);
		ilc.simNumber = '48374794';
		ilc.simActive = true;
		ilc.comments = 'comments';
		ilc.possibleIssues = 'possible issues';
		ilc.firmwareVersion = 'firmware version';
                
        GeotabUtils.InstallLogsRequest ilr = new GeotabUtils.InstallLogsRequest();
        ilr.device = null;
		ilr.installerName = 'installer name';
		ilr.installerCompany = 'installer company';
		ilr.asset = 'asset';        
        
        GeotabUtils.InstallLogsRequestDevice ilrd = new GeotabUtils.InstallLogsRequestDevice();
        ilrd.id = '22';
		ilrd.serialNumber = '32424';
		ilrd.deviceType = null;        
        
        GeotabUtils.InstallLogsRequestDeviceType ilrdt = new GeotabUtils.InstallLogsRequestDeviceType();
        ilrdt.name = 'name';

        GeotabUtils.Currency3 cur3 = new GeotabUtils.Currency3();
        cur3.code = 'test code';
        cur3.name = 'test name';

        GeotabUtils.ActiveCurrencyRate acr = new GeotabUtils.ActiveCurrencyRate();
        acr.factor = 1.12;
        acr.effectiveFrom = 'test from';

        GeotabUtils.DevicePlan dp = new GeotabUtils.DevicePlan();
        dp.id = 1;
        dp.name = 'test name';
        dp.level = 2;
        dp.validForOrder = true;

        GeotabUtils.OnlineOrderItem ooi = new GeotabUtils.OnlineOrderItem();
        ooi.productCode = 'test';
        ooi.productDescription = 'test';
        ooi.quantity = 2;
        ooi.unitCost = 12;
        ooi.monthlyCost = 13;
        ooi.itemStatus = 'test';

        GeotabUtils.Currency4 cur4 = new GeotabUtils.Currency4();
        cur4.code = 'code';
        cur4.name = 'name';

        GeotabUtils.Account3 acc3 = new GeotabUtils.Account3();
        acc3.accountId = '1';

        GeotabUtils.Currency5 cur5 = new GeotabUtils.Currency5();
        cur5.code = 'code';
        cur5.name = 'name';

        GeotabUtils.Account4 acc4 = new GeotabUtils.Account4();
        acc4.accountId = '12';

        GeotabUtils.UserCompany2 uc2 = new GeotabUtils.UserCompany2();
        uc2.id = 1;
        uc2.account = acc4;
        uc2.active = true;
        uc2.name = 'name';

        GeotabUtils.UserContact ucontact = new GeotabUtils.UserContact();
        ucontact.id = 1;
        ucontact.userCompany = uc2;
        ucontact.contactName = 'name';
        ucontact.contactEmail = 'mail';
        ucontact.telephone1 = '1';
        ucontact.telephone2 = '2';
        ucontact.street1 = 'street';
        ucontact.city = 'city';
        ucontact.country = 'country';
        ucontact.zipCode = '123';
        ucontact.active = true;
        ucontact.displayName = 'name';
        ucontact.address = 'add';
        ucontact.state = 'state';

        GeotabUtils.ShipItem sitem = new GeotabUtils.ShipItem();
        sitem.account = acc3;
        sitem.shipDate = 'a';
        sitem.purchaseOrderNo = 'aaa';
        sitem.erpReference = 'erp';
        sitem.userContact = ucontact;
        sitem.trackingNo = '1';
        sitem.trackingURL = '2';

        GeotabUtils.Account gAcc = new GeotabUtils.Account();
        gAcc.accountId = 'asdasd0';

        GeotabUtils.Currency2 cur2 = new GeotabUtils.Currency2();
        cur2.code = 'code';
        cur2.name = 'name';

        GeotabUtils.Account2 acc2 = new GeotabUtils.Account2();
        acc2.accountId = 'name';

        GeotabUtils.UserCompany ucompany = new GeotabUtils.UserCompany();
        ucompany.id = 1;
        ucompany.account = acc2;
        ucompany.active = true;
        ucompany.name = 'name';

        GeotabUtils.ShippingContact scontact = new GeotabUtils.ShippingContact();
        scontact.id = 1;
	    scontact.userCompany = ucompany;
	    scontact.contactName = 'name';
	    scontact.contactEmail = 'mail';
	    scontact.telephone1 = 'p1';
	    scontact.telephone2 = 'p2';
	    scontact.street1 = 'street';
	    scontact.city = 'city';
	    scontact.country = 'country';
	    scontact.zipCode = 'zip';
	    scontact.active = true;
	    scontact.displayName = 'dname';
	    scontact.address = 'address';
	    scontact.state = 'state';

        GeotabUtils.Result result = new GeotabUtils.Result();
        result.account = null;
        result.onHold = false;
        result.purchaseOrderNo = 'po2';
        result.orderNo = '21';
        result.orderDate = '2';
        result.estimatedCompletionDate = '124';
        result.shippingContact = scontact;
        result.activeCurrencyRate = acr;
        result.comments = 'test';
        result.orderedBy = 'test';
        result.orderTotalUsd = 12;
        result.orderMonthlyTotalUsd = 12;
        result.devicePlan = dp;
        result.onlineOrderItems = new List<GeotabUtils.OnlineOrderItem>();
        result.shipItems = new List<GeotabUtils.ShipItem>();
        result.currentStatus = 'test';
        result.resellerReference = 'ref';
        result.rateCode = '1';

        GeotabUtils.RootResponse rr = new GeotabUtils.RootResponse();
        rr.result = new List<GeotabUtils.Result>();
        rr.result.add(result);

        test.stopTest();
	}
}