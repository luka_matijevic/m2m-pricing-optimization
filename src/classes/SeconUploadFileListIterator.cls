global class SeconUploadFileListIterator
   implements Iterator<Stock__c>{

    global List<List<String>> simData {get; set;}
    global Id articleInfoSetId {get; set;}
    //ArticleWrapper article;
    Integer i {get; set;}

    public SeconUploadFileListIterator(Id articleInfoSetId, List<List<String>> simData)
    {
        this.simData = simData;
        this.articleInfoSetId = articleInfoSetId;
        i = 0;
    }

    global boolean hasNext()
    {
        return (i<simData.size());
    }

    global Stock__c next()
    {
        if(hasNext())
        {
            Stock__c response;
            if(simData.get(i).size() == 2)
            {
                response = new Stock__c(
                                        Article_Info_Set__c = articleInfoSetId,
                                        ICCID__c = simData.get(i).get(0),
                                        MSISDN__c = simData.get(i).get(1),
                                        RecordTypeId = Cache.RecordTypeId('Stock__c.SIM')
                                        );
            }
            else if(simData.get(i).size() == 1)
            {
                // Dino, 5.2.2014 - Avoid adding Stock__c for blank newlines in the second upload file
                if (string.isNotBlank(simData.get(i).get(0))) 
                {   
                    response = new Stock__c(
                                            Article_Info_Set__c = articleInfoSetId,
                                            ICCID__c = simData.get(i).get(0),
                                            RecordTypeId = Cache.RecordTypeId('Stock__c.SIM')
                                            );
                }
            }
            i++;
            return response;
        }
        else
        {
            throw new M2MNoSuchElementException();
        }
    }
    public class M2MNoSuchElementException extends Exception {}
}