global class SecondUploadFileIterator implements iterable<Stock__c>{

    public List<List<String>> simData {get; set;}
   	public Id articleInfoSetId {get; set;}

    public SecondUploadFileIterator(Id articleInfoSetId, List<List<String>> simData)
   	{
   		this.simData = simData;
   		this.articleInfoSetId = articleInfoSetId;
   	}

   global Iterator<Stock__c> Iterator(){
      return new SeconUploadFileListIterator(articleInfoSetId, simData);
   }
}