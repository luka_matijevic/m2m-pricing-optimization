public class tempController {

    public Case c {get;set;}
    public Account acc {get; set;}
    public String terminationDate {get; set;}
    public String dateOfReceipt {get; set;}
    public static final Map<Integer, String> germanMonthNames = new Map<Integer, String>{1 => 'Januar', 2 => 'Februar', 3 => 'März', 4 => 'April', 5 => 'Mai', 6 => 'Juni', 7 => 'Juli', 8 => 'August', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Dezember'};

    public tempController(){
        c = [Select Id, CaseNumber, Frame_Contract__c, Frame_Contract__r.Name, Sixt_Termination_date__c, AccountId from Case where Id = '5007E0000016sGd' LIMIT 1];
        acc = new Account();
        //c.AccountId = '001b0000005NsSq';
        prepareTerminationConfirmationLetter();
    }

    public void prepareTerminationConfirmationLetter(){
        try{
            acc = [Select Id,Name,BillingAddress,BillingCity,BillingCountry,BillingCountryCode,BillingPostalCode,BillingState,BillingStateCode,BillingStreet from Account where Id = :c.AccountId];
            terminationDate = convertDateToGermanFormat(System.today());
            dateOfReceipt = convertDateToGermanFormat(c.Sixt_Termination_date__c);
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
    }
    
    public List<SimWrapperForLetter> getTerminatedSIMs(){
        List<SimWrapperForLetter> terminatedSims = new List<SimWrapperForLetter>();
        try{
            if(c != null && c.AccountId != null){
                for(Stock__c sim : [Select BIC_Framework_Contract_No__c,ICCID__c,MSISDN__c,End_date__c from stock__c where Account__c = :c.AccountId and Termination_Status__c = 'REQUESTED']){
                //for(Stock__c sim : [Select BIC_Framework_Contract_No__c,ICCID__c,MSISDN__c,End_date__c from stock__c LIMIT 100]){
                    terminatedSims.add(new SimWrapperForLetter(sim, convertDateToGermanFormat(sim.End_date__c)));
                }
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
        return terminatedSims;
    }
    
    public String convertDateToGermanFormat(Date reqDate){
        Integer day, month, year;
        String germanDate = '';
        try{
            if(reqDate != null){
                day = reqDate.day();
                month = reqDate.month();
                year = reqDate.year();
                if(!germanMonthNames.isEmpty() && germanMonthNames.containsKey(month)){
                    germanDate = day + '. ' + germanMonthNames.get(month) + ' ' + year;
                }
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
        return germanDate;
    }
}