global class MonthlyServiceScheduleRefresh implements Schedulable {
	global void execute(SchedulableContext sc) {
		DailyServiceRefreshReports rr = new DailyServiceRefreshReports('Monthly');
		Database.executeBatch(rr);

	}
}