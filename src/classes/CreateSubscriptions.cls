//------------------------------------------------------------------------------
// Creating of subscriptions, services and service line items
//------------------------------------------------------------------------------
public with sharing class CreateSubscriptions
{
	//--------------------------------------------------------------------------
	// Members
	//--------------------------------------------------------------------------
	private Id orderId;
	private IInsertHelper insertHelper;

	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	public CreateSubscriptions(Id orderId, IInsertHelper insertHelper)
	{
		this.orderId = orderId;
		this.insertHelper = insertHelper;
	}

	//--------------------------------------------------------------------------
	// Creating subscriptions, services and service line items
	//--------------------------------------------------------------------------
	public void create()
	{
		//
		// Retrieve order
		//
		csord__Order__c order =
				[ SELECT Id, Order_Request__c, csord__Order_Request__c,
				  Order_Request__r.Name, Order_Request__r.Billing_Account__c,
				  Order_Request__r.Billing_Account__r.Name,
				  Order_Request__r.Billing_Account__r.Payer__r.BillingCountry,
				  Order_Request__r.Frame_Contract__r.Customer__c
				  FROM csord__Order__c
				  WHERE Id = :orderId ];
		//
		// Retrieve product orders
		//
		Map<Id, Product_Order__c> productOrderMap = new Map<Id, Product_Order__c>(
				[ SELECT Id, Quantity__c
				  FROM Product_Order__c
				  WHERE Order_Request__c = :order.Order_Request__c ]);
		//
		// Retrieve product configuration orders
		//
		Product_Configuration_Order__c[] productConfigurationOrders =
				[ SELECT Id, Product_Configuration__c, Quantity__c
				  FROM Product_Configuration_Order__c
				  WHERE Product_Order__c IN :productOrderMap.keySet() ];
		//
		// Create map of product configuration order regarding product configuration id
		//
		Map<Id, Product_Configuration_Order__c> productConfigurationOrderMap =
			new Map<Id, Product_Configuration_Order__c>();
		for (Product_Configuration_Order__c pco : productConfigurationOrders)
		{
			productConfigurationOrderMap.put(pco.Product_Configuration__c, pco);
		}

		//
		// Retrieve product configuration names
		//
		cscfga__Product_Configuration__c[] configs =
				[ SELECT Id, Name, Original_Product_Name__c,
				  cscfga__Contract_Term__c, parent_product_configuration__c,
				  Billing_Rate_Model__c,
				  ( SELECT Id, Name, cscfga__Is_Line_Item__c, cscfga__Recurring__c,
				    cscfga__Price__c, cscfga__Value__c, cscfga__Product_Configuration__c,
				    cscfga__Line_Item_Description__c
				    FROM cscfga__Attributes__r
				    WHERE ( ( cscfga__Is_Line_Item__c = true
							AND cscfga__Recurring__c = true
							AND cscfga__Product_Configuration__r.Original_Product_Name__c != 'Connectivity')
						  OR Name = :Constants.ATTRIBUTE_NAME_QUANTITY
						  OR Name = :Constants.ATTRIBUTE_NAME_ARTICLE_INFO_SET) )
				  FROM cscfga__Product_Configuration__c
				  WHERE parent_product_configuration__c = null
				  AND Id IN :productConfigurationOrderMap.keySet() ];

		//
		// Put quantities to products
		//
		Map<Id, Integer> configurationQuantities = new Map<Id, Integer>();
		Map<Id, Id> configurationArticleInfoSets = new Map<Id, Id>();
		for (cscfga__Product_Configuration__c config : configs)
		{
			System.debug('Configuration: ' + config);
			for (cscfga__Attribute__c att : config.cscfga__Attributes__r)
			{
				System.debug('Attribute: ' + att);
				if (att.Name == Constants.ATTRIBUTE_NAME_QUANTITY)
				{
					configurationQuantities.put(att.cscfga__Product_Configuration__c, Integer.valueOf(att.cscfga__Value__c));
				}
				else if (att.Name == Constants.ATTRIBUTE_NAME_ARTICLE_INFO_SET)
				{
					try
					{
						System.debug('Putting article info set: ' + att);
						configurationArticleInfoSets.put(att.cscfga__Product_Configuration__c, att.cscfga__Value__c);
					}
					catch(Exception e)
					{
						System.debug('Wrong Id in attribute: ' + att.cscfga__Value__c);
						throw e;
					}
				}
			}
		}
		//
		// Get journal mappings
		//
		Map<Id, Article_Info_Set__c> articleInfoSets = new Map<Id, Article_Info_Set__c>(
				[ SELECT Id, Journal_Mapping__c
				  FROM Article_Info_Set__c
				  WHERE Id IN :configurationArticleInfoSets.values() ]);
		Set<String> journalMappingNames = new Set<String>();
		for (Article_Info_Set__c ais : articleInfoSets.values())
		{
			journalMappingNames.add(ais.Journal_Mapping__c);
		}

		String country = getCountry(order.Order_Request__r.Billing_Account__r.Payer__r.BillingCountry);

		Journal_Mapping__c[] journalMappings =
				[ SELECT Id, Name
				  FROM Journal_Mapping__c
				  WHERE Name IN :journalMappingNames
				  AND Country__c = :country ];
		Map<String, Id> journalMappingMap = new Map<String, Id>();
		for (Journal_Mapping__c jm : journalMappings)
		{
			journalMappingMap.put(jm.Name, jm.Id);
		}
		//
		// Retrieve created subscriptions
		//
		csord__Subscription__c[] subscriptions =
				[ SELECT Id, RecordType.DeveloperName
				  FROM csord__Subscription__c
				  WHERE csord__Order__c = :order.Id ];
		csord__Subscription__c connectivitySubscription;
		csord__Subscription__c hardwareSupportSubscription;
		for (csord__Subscription__c subs : subscriptions)
		{
			if (subs.RecordType.DeveloperName == 'Hardware_Support'
				&& hardwareSupportSubscription == null)
			{
				hardwareSupportSubscription = subs;
			}
			if (subs.RecordType.DeveloperName == 'Connectivity'
				&& connectivitySubscription == null)
			{
				connectivitySubscription = subs;
			}
		}

		//
		// Create subscriptions
		//
		for (cscfga__Product_Configuration__c config : configs)
		{
			//
			// If this is connectivity
			//
			if (config.Original_Product_Name__c == Constants.PRODUCT_TYPE_CONNECTIVITY
				&& connectivitySubscription == null)
			{
				//
				// Create connectivity subscription
				//
				connectivitySubscription = new csord__Subscription__c();
				connectivitySubscription.Name = 'Connectivity for ' + order.Order_Request__r.Name;
				connectivitySubscription.RecordTypeId =
					Cache.getRecordTypeId('csord__Subscription__c.Connectivity');
				connectivitySubscription.csord__Identification__c = 'Connectivity';
				connectivitySubscription.csord__Status__c = 'Active';
				connectivitySubscription.csord__Order_Request__c = order.csord__Order_Request__c;
				connectivitySubscription.csord__Order__c = order.Id;
				connectivitySubscription.Billing_Account__c = order.Order_Request__r.Billing_Account__c;
				connectivitySubscription.csord__Account__c = order.Order_Request__r.Frame_Contract__r.Customer__c;
				insertHelper.add(connectivitySubscription);
			}
			//
			// Not connectivity product configuration
			//
			else
			{
				//
				// Create hardware & support subscription
				//
				if (hardwareSupportSubscription == null)
				{
					hardwareSupportSubscription = new csord__Subscription__c();
					hardwareSupportSubscription.Name = 'Subscription for ' + order.Order_Request__r.Name;
					hardwareSupportSubscription.RecordTypeId =
						Cache.getRecordTypeId('csord__Subscription__c.Hardware_Support');
					hardwareSupportSubscription.csord__Identification__c = 'Hardware & Support';
					hardwareSupportSubscription.csord__Status__c = 'Active';
					hardwareSupportSubscription.csord__Order_Request__c = order.csord__Order_Request__c;
					hardwareSupportSubscription.csord__Order__c = order.Id;
					hardwareSupportSubscription.Billing_Account__c = order.Order_Request__r.Billing_Account__c;
					hardwareSupportSubscription.csord__Account__c = order.Order_Request__r.Frame_Contract__r.Customer__c;
					insertHelper.add(hardwareSupportSubscription);
				}
			}
		}
		insertHelper.store();

		Set<Id> aisWithoutJournalMapping = new Set<Id>();

		//
		// Create services if there are service line items
		//
		for (cscfga__Product_Configuration__c config : configs)
		{
			csord__Service__c service = null;
			for (cscfga__Attribute__c att : config.cscfga__Attributes__r)
			{
				if (att.cscfga__Is_Line_Item__c == true
					&& att.cscfga__Price__c != null
					&& att.cscfga__Price__c != 0)
				{
					//
					// Create service
					//
					if (service == null)
					{
						service = new csord__Service__c();
						service.csord__Identification__c = config.Name;
						service.csord__Status__c = 'Active';
						service.csord__Order_Request__c = order.csord__Order_Request__c;
						service.csord__Subscription__c = hardwareSupportSubscription.Id;
						service.csord__Activation_Date__c = DateTime.now().date();
						service.csord__Deactivation_Date__c =
							DateTime.now().date().addMonths(Integer.valueOf(config.cscfga__Contract_Term__c));
						service.Product_Configuration_Order__c =
							productConfigurationOrderMap.get(config.Id).Id;
						String billingRateModel = config.Billing_Rate_Model__c;
						service.Billing_Rate_Model__c =
								billingRateModel != null ? billingRateModel : Constants.ONE_OFF;
						insertHelper.add(service);
					}
					//
					// Calculate price
					//
					Decimal price = att.cscfga__Price__c;
					price *= productConfigurationOrderMap.get(att.cscfga__Product_Configuration__c).Quantity__c;
					price /= configurationQuantities.get(att.cscfga__Product_Configuration__c);
					Id articleInfoSet = configurationArticleInfoSets.get(att.cscfga__Product_Configuration__c);
					Id journalMapping = null;
					if (articleInfoSet != null)
					{
						String journalMappingName = articleInfoSets.get(articleInfoSet).Journal_Mapping__c;
						if (journalMappingName != null)
						{
							journalMapping = journalMappingMap.get(journalMappingName);
						}
					}
					//
					// Check if journal mapping exists
					//
					if (journalMapping == null)
					{
						if (articleInfoSet == null)
						{
							aisWithoutJournalMapping.add(att.cscfga__Product_Configuration__c);
						}
						else
						{
							aisWithoutJournalMapping.add(articleInfoSet);
						}
					}
					//
					// Service line item
					//
					csord__Service_Line_Item__c serviceLineItem = new csord__Service_Line_Item__c();
					serviceLineItem.Name = att.Name;
					serviceLineItem.csord__Identification__c = att.Name;
					serviceLineItem.csord__Order_Request__c = order.csord__Order_Request__c;
					serviceLineItem.Journal_Mapping__c = journalMapping;
					serviceLineItem.csord__Is_Recurring__c = true;
					serviceLineItem.Total_Price__c = price;
					serviceLineItem.Article_Info_Set__c = articleInfoSet;

					insertHelper.add(serviceLineItem, 'csord__Service__c', service);
				}
			}
		}
		//
		// If there is no journal mapping throw exception
		// because line items without journal mapping cannot be billed
		//
		if (!aisWithoutJournalMapping.isEmpty())
		{
			String message = '';
			Iterator<Id> it = aisWithoutJournalMapping.iterator();
			while (it.hasNext())
			{
				Id aisId = it.next();
				message += aisId;
				if (it.hasNext())
				{
					message += ', ';
				}
			}
			throw new JournalMappingException(message);
		}

		insertHelper.store();
	}

	//--------------------------------------------------------------------------
	// Returns DE or Non-DE depending on country name
	//--------------------------------------------------------------------------
	private static String getCountry(String country)
	{
		System.debug('Country: ' + country);
		if (country == 'Germany'
			|| country == 'Deutschland')
		{
			return 'DE';
		}
		else
		{
			return 'Non-DE';
		}
	}
}