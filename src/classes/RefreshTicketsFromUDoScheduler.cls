global class RefreshTicketsFromUDoScheduler implements Schedulable{

    public static Id scheduleIt(){
        Id jobId = System.schedule('RefreshTicketsFromUDo', '0 0 0/2 * * ?', new RefreshTicketsFromUDoScheduler());
        return jobId;
    }
    global void execute(SchedulableContext sc){ 
        RefreshTicketsFromUDOBatch refreshBatch = new RefreshTicketsFromUDOBatch(); 
        database.executebatch(refreshBatch); 
    } 
}