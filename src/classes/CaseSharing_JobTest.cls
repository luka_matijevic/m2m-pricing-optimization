@isTest
public class CaseSharing_JobTest {

    @testSetup
    static void prepareData(){
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.Type = 'Customer'; 
        insert acc;
        
        Case cs = new Case(Subject='Subject',Description='DescriptionDescriptionDescriptionDescription',AccountId = acc.id);
        insert cs;
        
    } 
    
    static testMethod void testSharing(){
        Account acc = [select id from Account limit 1];
        map<id, id> accIds = new map<id, id>();
        accIds.put(acc.id, userInfo.getUserId());
        
        test.startTest();
         database.executeBatch(new CaseSharing_Job(accIds));  
        test.stopTest();
    }
}