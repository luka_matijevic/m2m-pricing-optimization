public class DailyBillItemTriggerDelegate extends TriggerHandler.DelegateBase {
	
	public map<id, Account> accountMap = new map<id, Account>();
	public override void prepareBefore() {
	    set<id> accIds = new set<id>();
	    if(trigger.isInsert || trigger.isUpdate){
	        for(Daily_Bill_Item__c db : (list<Daily_Bill_Item__c >) trigger.new){
    		    if(db.Account__c != null){
    		        accIds.add(db.Account__c);
    		    }
    		    
    		}
    		if(accIds.size() > 0){
    		    accountMap = new map<id, Account>([select id,BillingCountry from Account where id IN :accIds]);
    		}
    		for(Daily_Bill_Item__c db : (list<Daily_Bill_Item__c >) trigger.new){
    		    if(db.Account__c != null && accountMap.get(db.Account__c) != null){
    		        Account acc  = accountMap.get(db.Account__c);
    		        if(acc.BillingCountry != null && acc.BillingCountry == Label.Germany){
                        db.Tax_Code__c = '19%';
                        db.Steuersatz__c = 19;
                    }else{
                        db.Tax_Code__c = '0%';
                        db.Steuersatz__c = 0;
                    }
    		    }
    		}
	    }
	}
	
	public override void prepareAfter() {
	}
	
	 public override void beforeInsert(sObject o) {
        Daily_Bill_Item__c db = (Daily_Bill_Item__c)o;
        
        if (db.tax_code__c==null) {
        	if (db.Steuersatz__c==19) db.tax_code__c='19%'; 
    			else db.tax_code__c='0%';
    		//up.add(db);
        }
        
        if (db.Rechnungsnummer__c==null) {
             if(!test.isrunningtest())
        {
            List<Invoice_Number__c> ll = [select id, name, value__c from invoice_number__c where name='Current' FOR UPDATE];
            Invoice_Number__c invoiceNumber = ll[0];
        
				
	        String text=String.valueOf(invoiceNumber.Value__c.setScale(0)); 
        	while (text.length() < 10)  
            { 
            text = '0' + text; 
            }
        	db.Rechnungsnummer__c=text;
        
   /*     if (db!=null && db.Art__c<>6) {
            if (db.Steuersatz__c<>0) {
            db.Einzel_Taxbetrag__c = db.Einzel_Nettobetrag__c * (db.Steuersatz__c/100); 
            db.Einzel_Bruttobetrag__c = db.Einzel_Nettobetrag__c * (1 + (db.Steuersatz__c/100)); 
            }
            else {
                db.Einzel_Taxbetrag__c = db.Einzel_Nettobetrag__c * 0; 
                db.Einzel_Bruttobetrag__c = db.Einzel_Nettobetrag__c ;
            }
            db.Gesamt_Bruttobetrag__c = db.Einzel_Bruttobetrag__c * db.Menge__c;
            db.Gesamt_Nettobetrag__c = db.Einzel_Nettobetrag__c * db.Menge__c;
            db.Gesamt_Taxbetrag__c = db.Einzel_Taxbetrag__c * db.Menge__c;
            db.Betrag__c = db.Einzel_Nettobetrag__c;
            db.VO_Nr__c = db.Account__r.VO_Number__c;
        } */
        	invoiceNumber.Value__c=invoiceNumber.Value__c+1;
	        update invoiceNumber;
        }
            
        }
        	if (db.VO_Nr__c==null && db.Account__c!=null) 
        	{
        	    List<Billing_Account__c> billAccountList = [SELECT id, OFI_ID__c from Billing_Account__c where payer__c=:db.Account__c];
        	    if (!billAccountList.isEmpty()) db.VO_Nr__c=billAccountList[0].OFI_ID__c;
        	}
	 }     
  
  
  public override void beforeUpdate (sObject old, sObject o) {
     
        Daily_Bill_Item__c db = (Daily_Bill_Item__c)o;
          
          if (db.tax_code__c==null) {
        		if (db.Steuersatz__c==19) db.tax_code__c='19%'; 
    			else db.tax_code__c='0%';
    			//up.add(db);
        	}
          
          if(db.Art__c<>2) {
             if (db.Steuersatz__c<>0 && db.Art__c<>2) {
            db.Einzel_Taxbetrag__c = db.Einzel_Nettobetrag__c * (db.Steuersatz__c/100); 
            db.Einzel_Bruttobetrag__c = db.Einzel_Nettobetrag__c * (1 + (db.Steuersatz__c/100)); 
            }
            else {
                db.Einzel_Taxbetrag__c = db.Einzel_Nettobetrag__c * 0; 
                db.Einzel_Bruttobetrag__c = db.Einzel_Nettobetrag__c ;
            }
            db.Gesamt_Bruttobetrag__c = db.Einzel_Bruttobetrag__c * db.Menge__c;
            db.Gesamt_Nettobetrag__c = db.Einzel_Nettobetrag__c * db.Menge__c;
            db.Gesamt_Taxbetrag__c = db.Einzel_Taxbetrag__c * db.Menge__c;
            db.Betrag__c = db.Einzel_Nettobetrag__c;
            //db.VO_Nr__c = db.Account__r.VO_Number__c;
           if (db.VO_Nr__c==null && db.Account__c!=null) 
        	{
        	    List<Billing_Account__c> billAccountList = [SELECT id, OFI_ID__c from Billing_Account__c where payer__c=:db.Account__c];
        	    if (!billAccountList.isEmpty()) db.VO_Nr__c=billAccountList[0].OFI_ID__c;
        	}
            }
      
        }
	
	public override void finish() {
	}
	
	/*
	    If daily bill item inserted wuth PAyment type = 'Recuuring', then inserting subscription, service and service line item
	*/
	public void handleBilling(){
	    if(accountMap.size() > 0){
	        
	        
	        list<Daily_Bill_Item__c> dbItems = new list<Daily_Bill_Item__c>();
	        dbItems = [select id, Account__c,Daily_Bill__c, Daily_Bill__r.Date__c,Payment_Type__c,Number_of_months__c,M2M_Order_Request__c from Daily_Bill_Item__c where id IN:trigger.newMap.keySet()];
	        
	        
	        list<csord__Order_Request__c> ordRequest = new list<csord__Order_Request__c>();
	        //ordRequest = [select id, Account__c from Order_Request__c where Account__c In:accountMap.keySet()];
	        map<id, id> accOrdReq = new map<id, id>();
	       // for(Order_Request__c ord : ordRequest){
	      //      if(ord.Account__c != null){
	       //         accOrdReq.put(ord.Account__c , ord.id);
	       //     }
	    //    }
	        
	        map<id, csord__Subscription__c> subscription = new map<id, csord__Subscription__c>();
	        map<id, csord__Service__c> services = new map<id, csord__Service__c>();
	        map<id, csord__Service_Line_Item__c> serviceLineItems = new map<id, csord__Service_Line_Item__c>();
	        for(Daily_Bill_Item__c db : (list<Daily_Bill_Item__c >) trigger.new){
    		    if(db.Payment_Type__c =='Payment_Type__c' && db.Account__c != null && accountMap.get(db.Account__c) != null){
    		        string key = db.id+ '-' + db.Account__c;
    		        
    		    }
    		}
    		
    		
	    }
	}
	
}