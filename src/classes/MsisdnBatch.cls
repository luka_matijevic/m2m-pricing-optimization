global class  MsisdnBatch implements Database.Batchable<Msisdn__c> {
    long rangeStart;
    long rangeEnd;
    Id rangeID;
    
    public MsisdnBatch(long rangeStart, long rangeEnd, Id rangeID){
        this.rangeStart = rangeStart;
        this.rangeEnd = rangeEnd;
        this.rangeId = rangeId;
        
    }
    global Iterable<Msisdn__c> start(Database.BatchableContext info){
        return new MsisdnIterable(rangeStart, rangeEnd, rangeID);
    }
    global void execute(Database.BatchableContext info, List<Msisdn__c> scope)
   	{
   		upsert scope MSISDN__c;
   	}

    global void finish(Database.BatchableContext info){
   		
   	}
}