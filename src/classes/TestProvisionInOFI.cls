@isTest
private class TestProvisionInOFI
{

    static testMethod void testProvisionInOFI()
    {
		//
		// Create test data
		//
				Account accPayer = new Account(
			Name = 'Test account', 
			BillingCity = 'Bremen', 
			BillingCountry = 'Germany', 
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße', 
			Type = 'Customer', 

			VAT__c = 1,
            Global__c = true
			
		);
		insert accPayer;
        
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1)
			, recordtypeid = Cache.getRecordTypeId('Opportunity.Commercial_Global'),
			Description = 'Test description 20 characters'
		);
		insert opp;		
		 Contact con = Test_Util.createContact(accPayer.id);
		
        Frame_Contract__c fc = new Frame_Contract__c();
        fc.contact__c = con.id;
        fc.customer__c = accPayer.id;
        fc.opportunity__c = opp.id;
         insert fc;      
        
		Billing_Account__c billingAccount = new Billing_Account__c(
			Payer__c = accPayer.Id, 
			Trial__c=false, 
			Billable__c=false,
			VAT__c = '1'
		);
		insert billingAccount;
		MockSendEmail sendEmail = new MockSendEmail();
		
		Id billingAccountId = [ SELECT Id FROM Billing_Account__c LIMIT 1].Id;

		Test.startTest();
		new ProvisionInOFI(billingAccountId, sendEmail).provision();
		Test.stopTest();
		//
		// Test
		//
		String ofiProvisioningStatus =
				[ SELECT OFI_Provisioning_Status__c
				  FROM Billing_Account__c
				  WHERE Id = :billingAccountId ].OFI_Provisioning_Status__c;
		String expected = AttachmentUtils.csvBOM() + 'ALTKN,KUNNR,NAME,STRAS,ORT01,PSTLZ,LAND1,SPRAS,TELF1,TELF2,TELFX,FAX_NUMBER,SMTP_ADDR,STCEG,BANKS,BANKL,BANKN,BANKA,ZTERM,ZWELS,MANSP,VTRIEB,UPDTYP';
		String attachmentName = sendEmail.getAttachmentName();
		String attachment = sendEmail.getAttachment();
		String attBody = [ SELECT Body FROM Attachment WHERE ParentId = :billingAccountId ].Body.toString();
		System.assertEquals('In Progress', ofiProvisioningStatus);
		System.assertNotEquals(null, attachmentName);
        system.debug(attachment);
		System.assert(attachment.indexOf(expected) == 0);
		System.assert(attBody.indexOf(expected) == 0);
    }
}