/**
 * Test class for Contract Extension process. Validates the Opportunity, Contact Roles, Product Bundle & configuration.
 * Created By: ritesh.saxena@cloudsense.com
 */
@isTest
private class ContractExtensionTest {
    
    /*
    * Test Contract Extension process.
    */ 
    static testMethod void cloneOpptyBundleTest() {
        
        // Triggers set Inactive 
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers;
        // Create test data
   
        // create a bundle
        TestHelper.createProductBundle();
        Id fcRecType = [Select id from RecordType where sObjectType = 'Frame_Contract__c' and DeveloperName = 'Trial'].Id;
        // get the existing bundle and opportunity
        cscfga__Product_Bundle__c bundle = [SELECT Id, Name, cscfga__Opportunity__c, cscfga__Synchronised_with_Opportunity__c FROM cscfga__Product_Bundle__c LIMIT 1];
        if(!bundle.cscfga__Synchronised_with_Opportunity__c){
            bundle.cscfga__Synchronised_with_Opportunity__c = true;
            update bundle;
        }
        Map<Id, cscfga__Product_Configuration__c> configsMap = new Map<Id, cscfga__Product_Configuration__c>([SELECT Id, Name FROM cscfga__Product_Configuration__c]);
        Id oppId = bundle.cscfga__Opportunity__c;
        
        Opportunity opp = [Select Id, StageName from Opportunity where Id=:oppId limit 1];
        opp.StageName = 'Closed Won';
        opp.Win_Reason__c = 'Test';
        update opp;
        
        Date testDate = Date.newInstance(2014,2,10);
        Account acc = new Account(
          Name = 'Test account11', 
          BillingCity = 'Bremen', 
          BillingCountry = 'Germany', 
          BillingPostalCode = '5000',
          BillingStreet = 'Lindenstraße', 
          Type = 'Customer', 
          Billing_Cycle__c = String.valueOf(testDate.day()),
          VAT__c = 1,
        	Global__c = true);
        insert acc;
        
        Contact c = new Contact();
        c.FirstName = 'Bob12';
        c.LastName = 'TestContact12';
        c.email = 'test111.contact@email.com';
        c.AccountId= acc.Id;
        insert c;
        
        OpportunityContactRole ocr = new OpportunityContactRole(ContactId = c.id, opportunityId = oppId);
        insert ocr;
        
        Frame_Contract__c fc = new Frame_Contract__c(recordTypeId = fcRecType, Customer__c = acc.Id, Opportunity__c = oppId, Status__c = 'Archived Contract And Sent To Customer');
        insert fc;
        
        notriggers.Flag__c = false;
        update notriggers;
        
        Test.startTest();
            ContractExtensionWS.ContractExtension(fc.Id, 'VVL');  
        Test.stopTest();
        
        // assert that it configurations got copied
        Map<Id, cscfga__Product_Configuration__c> copiedConfigsMap = new Map<Id, cscfga__Product_Configuration__c>([SELECT Id, Name FROM cscfga__Product_Configuration__c WHERE Id NOT IN :configsMap.keyset()]);
        system.AssertEquals(configsMap.keyset().size(), copiedConfigsMap.keyset().size());
    }
    
    /*
    * Test Contract Extension process.
    */ 
    static testMethod void cloneOpptyBundleTest2() {
        
        // Triggers set Inactive 
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers;
        // Create test data
   
        // create a bundle
        TestHelper.createProductBundle();
        Id fcRecType = [Select id from RecordType where sObjectType = 'Frame_Contract__c' and DeveloperName = 'Trial'].Id;
        // get the existing bundle and opportunity
        cscfga__Product_Bundle__c bundle = [SELECT Id, Name, cscfga__Opportunity__c, cscfga__Synchronised_with_Opportunity__c FROM cscfga__Product_Bundle__c LIMIT 1];
        if(!bundle.cscfga__Synchronised_with_Opportunity__c){
            bundle.cscfga__Synchronised_with_Opportunity__c = true;
            update bundle;
        }
        Map<Id, cscfga__Product_Configuration__c> configsMap = new Map<Id, cscfga__Product_Configuration__c>([SELECT Id, Name FROM cscfga__Product_Configuration__c]);
        Id oppId = bundle.cscfga__Opportunity__c;
        
        Opportunity opp = [Select Id, StageName from Opportunity where Id=:oppId limit 1];
        opp.StageName = 'Closed Won';
        opp.Win_Reason__c = 'Test';
        update opp;
        
        Date testDate = Date.newInstance(2014,2,10);
        Account acc = new Account(
          Name = 'Test account11', 
          BillingCity = 'Bremen', 
          BillingCountry = 'Germany', 
          BillingPostalCode = '5000',
          BillingStreet = 'Lindenstraße', 
          Type = 'Customer', 
          Billing_Cycle__c = String.valueOf(testDate.day()),
          VAT__c = 1,
        Global__c = true);
        insert acc;
        
        Contact c = new Contact();
        c.FirstName = 'Bob12';
        c.LastName = 'TestContact12';
        c.email = 'test111.contact@email.com';
        c.AccountId= acc.Id;
        insert c;
        
        OpportunityContactRole ocr = new OpportunityContactRole(ContactId = c.id, opportunityId = oppId);
        insert ocr;
        
        Frame_Contract__c fc = new Frame_Contract__c(recordTypeId = fcRecType, Customer__c = acc.Id, Opportunity__c = oppId, Status__c = 'Archived Contract And Sent To Customer');
        insert fc;
        
        notriggers.Flag__c = false;
        update notriggers;
        
        Test.startTest();
            ContractExtensionWS.ContractExtension(fc.Id, 'Repricing'); 
        Test.stopTest();
        
        // assert that it configurations got copied
        Map<Id, cscfga__Product_Configuration__c> copiedConfigsMap = new Map<Id, cscfga__Product_Configuration__c>([SELECT Id, Name FROM cscfga__Product_Configuration__c WHERE Id NOT IN :configsMap.keyset()]);
        system.AssertEquals(configsMap.keyset().size(), copiedConfigsMap.keyset().size());
    }
    
    
    /*
    * Test Contract Extension process.
    * Option - 'Repricing'
    */ 
    static testMethod void cloneOpptyBundleTest3() {
        
        // Triggers set Inactive 
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers;
        // Create test data
   
        // create a bundle
        TestHelper.createProductBundle();
        Id fcRecType = [Select id from RecordType where sObjectType = 'Frame_Contract__c' and DeveloperName = 'Trial'].Id;
        // get the existing bundle and opportunity
        cscfga__Product_Bundle__c bundle = [SELECT Id, Name, cscfga__Opportunity__c, cscfga__Synchronised_with_Opportunity__c FROM cscfga__Product_Bundle__c LIMIT 1];
        if(!bundle.cscfga__Synchronised_with_Opportunity__c){
            bundle.cscfga__Synchronised_with_Opportunity__c = true;
            update bundle;
        }
        Map<Id, cscfga__Product_Configuration__c> configsMap = new Map<Id, cscfga__Product_Configuration__c>([SELECT Id, Name FROM cscfga__Product_Configuration__c]);
        Id oppId = bundle.cscfga__Opportunity__c;
        
        Opportunity opp = [Select Id, StageName from Opportunity where Id=:oppId limit 1];
        opp.StageName = 'Closed Won';
        opp.Name = 'Test_Repricing_1';
        opp.Win_Reason__c = 'Test';
        update opp;
        
        Date testDate = Date.newInstance(2014,2,10);
        Account acc = new Account(
          Name = 'Test account11', 
          BillingCity = 'Bremen', 
          BillingCountry = 'Germany', 
          BillingPostalCode = '5000',
          BillingStreet = 'Lindenstraße', 
          Type = 'Customer', 
          Billing_Cycle__c = String.valueOf(testDate.day()),
          VAT__c = 1,
        Global__c = true);
        insert acc;
        
        Contact c = new Contact();
        c.FirstName = 'Bob12';
        c.LastName = 'TestContact12';
        c.email = 'test111.contact@email.com';
        c.AccountId= acc.Id;
        insert c;
        
        OpportunityContactRole ocr = new OpportunityContactRole(ContactId = c.id, opportunityId = oppId);
        insert ocr;
        
        Frame_Contract__c fc = new Frame_Contract__c(recordTypeId = fcRecType, Customer__c = acc.Id, Opportunity__c = oppId, Status__c = 'Archived Contract And Sent To Customer');
        insert fc;
        
        notriggers.Flag__c = false;
        update notriggers;
        
        Test.startTest();
            ContractExtensionWS.ContractExtension(fc.Id, 'Repricing'); 
        Test.stopTest();       
    }
    
    
    /*
    *   Test Contract Extension process exceptions.
    */
    static testMethod void cloneOpptyBundleNegTest2() {
        
        // Triggers set Inactive 
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers;
        // Create test data     
        // create a bundle
        TestHelper.createProductBundle();
        Id fcRecType = [Select id from RecordType where sObjectType = 'Frame_Contract__c' and DeveloperName = 'Trial'].Id;
        // get the existing bundle and opportunity
        cscfga__Product_Bundle__c bundle = [SELECT Id, Name, cscfga__Opportunity__c, cscfga__Synchronised_with_Opportunity__c FROM cscfga__Product_Bundle__c LIMIT 1];
        if(!bundle.cscfga__Synchronised_with_Opportunity__c){
            bundle.cscfga__Synchronised_with_Opportunity__c = true;
            update bundle;
        }
        //Map<Id, cscfga__Product_Configuration__c> configsMap = new Map<Id, cscfga__Product_Configuration__c>([SELECT Id, Name FROM cscfga__Product_Configuration__c]);
        
        Id oppId = bundle.cscfga__Opportunity__c;
        
        Opportunity opp = [Select Id, StageName from Opportunity where Id=:oppId limit 1];
        opp.StageName = 'Prospecting';
        update opp;
        
        Date testDate = Date.newInstance(2014,2,10);
        Account acc = new Account(
          Name = 'Test account11', 
          BillingCity = 'Bremen', 
          BillingCountry = 'Germany', 
          BillingPostalCode = '5000',
          BillingStreet = 'Lindenstraße', 
          Type = 'Customer', 
          Billing_Cycle__c = String.valueOf(testDate.day()),
          VAT__c = 1,
        Global__c = true);
        insert acc;
        Contact c = new Contact();
        c.FirstName = 'Bob12';
        c.LastName = 'TestContact12';
        c.email = 'test111.contact@email.com';
        c.AccountId= acc.Id;
        insert c;
        
        Frame_Contract__c fc = new Frame_Contract__c(recordTypeId = fcRecType, Customer__c = acc.Id, Opportunity__c = oppId, Status__c = 'Archived Contract And Sent To Customer');
        insert fc;
        
        notriggers.Flag__c = false;
        update notriggers;
        
        Test.startTest();
            ContractExtensionWS.ContractExtension(fc.Id,'VVL');          
            // assert exception
            Frame_Contract__c f = [Select Id, ContractExtensionStatus__c from Frame_Contract__c fc where Id=: fc.Id];
            system.assert(f.ContractExtensionStatus__c.contains('ERROR'));
        Test.stopTest();
    }
}