public with sharing class DirectSRFAfterInsertHandler extends TriggerHandler.DelegateBase
{
    Set<Id> srfIds;// = Trigger.newMap.keySet();
    List<Third_Upload_File__c> tUFsToInsert = new List<Third_Upload_File__c>();

    public override void prepareAfter()
    {
        if(!Trigger.isDelete)
            srfIds = Trigger.newMap.keySet();
    }

    public override void afterInsert(sObject o)
    {

            SRF__c item = (SRF__c)o;
            if((item.RecordTypeId == Cache.RecordTypeId('SRF__c.Direct SIM Delivery') || item.RecordTypeId == Cache.RecordTypeId('SRF__c.Standard SRF')  ) &&  item.Order_Request__c != null)
                tUFsToInsert.add
                (
                    new Third_Upload_File__c
                    (
                        Order_Request__c = item.Order_Request__c,
                        SIM_Article_Type__c = item.SIM_Type__c,
                        Account__c = item.Account__c
                    )
                );

    }

    public override void afterUpdate(sObject old, sObject o)
    {

            SRF__c item = (SRF__c)o;
            SRF__c oldItem = (SRF__c)old;
            if((item.RecordTypeId == Cache.RecordTypeId('SRF__c.Direct SIM Delivery') || item.RecordTypeId == Cache.RecordTypeId('SRF__c.Standard SRF')  ) &&  item.Order_Request__c != null && oldItem.Order_Request__c == null)
                tUFsToInsert.add
                (
                    new Third_Upload_File__c
                    (
                        Order_Request__c = item.Order_Request__c,
                        SIM_Article_Type__c = item.SIM_Type__c,
                        Account__c = item.Account__c
                    )
                );

    }

    public override void finish()
    {
        insert tUFsToInsert;
    }
}