/**
* Processes the retrieve prebill message and inserts the retrieved Prebill and Prebill Line Item objects
*
* @author Ilija Pavlic
*/
global class RetrievePrebillsMessageInboundProcessor implements csam_t1.InboundMessageProcessor {

  private Map<String, String> productJournalMappingsMap;
  private Map<String, List<String>> productDescriptionsMap;
  private Map<String, Map<String, Journal_Mapping__c>> nameVATJournalMappingsMap;
      
  /**
   * The constructor shouldn't do any processing, because the class is initialized for the isProcessible check
   */ 
  public RetrievePrebillsMessageInboundProcessor() {
  
  }

  // the processor should only process the retrieve prebills message
  public Boolean isMessageProcessible(csam_t1__Incoming_Message__c incomingMessage) {
    return incomingMessage.csam_t1__Incoming_URL_Path__c == '/csam_t1/callback/prebills/retrieve';
  }

  public void processMessage(csam_t1__Incoming_Message__c incomingMessage, Map<Id, Id> oldToNewIdsMap) {
  
    // the members are initialized here to avoid initialization before processing (the processor is 
    // instantiated to check whether the message is processible)
    initializeMembers();
    
    try {
      
      CallbackMessage cm = InboundProcessorHelper.getCallbackMessage(incomingMessage.Id);
      insertPayloadObjects(cm, oldToNewIdsMap);

      incomingMessage.csam_t1__Status__c = 'Processed';
      InboundProcessorHelper.updateStatuses(incomingMessage, 'Completed');

    } catch (Exception e) {
      
      system.debug('**IP** '+ e.getLineNumber() + ' ' + e.getMessage());
      system.debug('**IP** '+ e.getStackTraceString());
      
      incomingMessage.csam_t1__Status__c = 'Process Error';

      Attachment errorAttach = new Attachment();
      errorAttach.Body = Blob.valueOf(e.getStackTraceString());
      errorAttach.Name = ('Error: ' + e.getMessage()).abbreviate(255);
      errorAttach.parentId = incomingMessage.Id;

      insert errorAttach;
    }

    incomingMessage.csam_t1__Processed_At__c = System.now();

    update incomingMessage;
  }
  
  /**
   * @description Initializes member variables required for processing. This should likely called in process method to
   * avoid initialization just for processiblity check which is done for roughly every message that is processed
   */ 
  private void initializeMembers() {
    this.productJournalMappingsMap = buildProductJournalMappingsMap(Product_Journal_Mapping_Map__c.getall().values()); 
    this.productDescriptionsMap = buildProductDescriptionsMap(Product_Journal_Mapping_Map__c.getall().values()); 
    
    this.nameVatJournalMappingsMap = new Map<String, Map<String, Journal_Mapping__c>>();

    for (Journal_Mapping__c journalMapping : selectJournalMappingsByName(productJournalMappingsMap.values())) {
      if (this.nameVatJournalMappingsMap.get(journalMapping.Name) == null) {
        this.nameVatJournalMappingsMap.put( journalMapping.Name, new Map<String, Journal_Mapping__c>() );
      }
      this.nameVATJournalMappingsMap.get(journalMapping.Name).put(journalMapping.Revenue__c, journalMapping);
    }
  }
  
  private Map<String, List<String>> buildProductDescriptionsMap(List<Product_Journal_Mapping_Map__c> productJournalMappings) {
    Map<String, List<String>> productDescriptionsMap = new Map<String, List<String>>();
    for (Product_Journal_Mapping_Map__c pair : productJournalMappings) {
      List<String> languageList = new List<String>();
      languageList.add(pair.Daily_Bill_Description__c);
      languageList.add(pair.Daily_Bill_Description_English__c);
      productDescriptionsMap.put(pair.Product_Name__c, languageList);
    }
    return productDescriptionsMap;
  }
  
  private Map<String, String> buildProductJournalMappingsMap(List<Product_Journal_Mapping_Map__c> productJournalMappings) {
    Map<String, String> productJournalMappingsMap = new Map<String, String>();
    for (Product_Journal_Mapping_Map__c pair : productJournalMappings) {
      productJournalMappingsMap.put(pair.Product_Name__c, pair.Journal_Mapping_Name__c);
    }
    return productJournalMappingsMap;
  }

  /**
   * @description Inserts the objects in the Callback Message object, taking care to convert the
   * attachment from the integration platform format (where Body is stored as Description) to the
   * standard format.
   */ 
  private void insertPayloadObjects(CallbackMessage cm, Map<Id, Id> oldToNewIdsMap) {
    
    if (cm.objectsToInsert != null && cm.objectsToInsert.size() > 0) {     

      addJournalMappingInformation(cm);
      
      List<Prebill_Line_Item__c> messagePrebillLineItems = (List<Prebill_Line_Item__c>) SObjectHelper.filterSObjects(cm.objectsToInsert, Prebill_Line_Item__c.sObjectType);
        for (Prebill_Line_Item__c pbli : messagePrebillLineItems) {
          List<String> languageValue = productDescriptionsMap.get(pbli.Name);

          if (languageValue.size() > 0) {
            if (pbli.Line_Item_Description__c==pbli.Name) {
              if(pbli.Billing_Language__c != null && pbli.Billing_Language__c.equalsIgnoreCase('English')) {
                pbli.Line_Item_Description__c = languageValue[1];
              } else {
                pbli.Line_Item_Description__c = languageValue[0];
              }
            }
            else {
              pbli.Line_Item_Description__c = productDescriptionsMap.get(pbli.Name)[0] + ' ' + pbli.Line_Item_Description__c;
            }
        }
			}

      List<Prebill__c> messagePrebills = (List<Prebill__c>) SObjectHelper.filterSObjects(cm.objectsToInsert, Prebill__c.sObjectType);
      
      if (!messagePrebills.isEmpty()) {
        
        Prebill__c prebill = messagePrebills.get(0);
      
        Account parentAccount = selectParentAccount(prebill);
        Integer billingDay = Integer.valueOf(parentAccount.Billing_Cycle__c);
  
        Daily_Bill__c dailyBill = selectDailyBill(BillingHelper.billingDate(billingDay, Date.today()));
              
        prebill.MAHN_Status__c = false;
        prebill.Billing_Rate_Model__c = BillingRateModels.toString(BillingRateModel.ONEOFF);
        prebill.Daily_Bill__c = dailyBill.Id;
        
        
        String description = prebill.Integration_Description__c;
        prebill.Integration_Description__c = null;
        
        // we need to add the dailyBill id into the map so that Object Graph picks it up when copying id fields to cloned objects
        oldToNewIdsMap.put(dailyBill.Id, dailyBill.Id);
        insertObjects(cm, oldToNewIdsMap);
        
        // after insertion of message records, we generate and insert the attachment from description
        Attachment attach = new Attachment(
          ContentType = 'application/xml',
          Body = Blob.valueOf(description),
          Name = prebill.Name,
          ParentId = oldToNewIdsMap.get(prebill.Id)
        );
        
        insert attach;
        
        
        Prebill__c pb = [SELECT id, invoice_Number__c from Prebill__c where id = :oldToNewIdsMap.get(prebill.Id)];
        
        //Invoice_Number__c invoiceNumber = Invoice_Number__c.getInstance('Current');  
           if(!test.isrunningtest())
        {
        List<Invoice_Number__c> ll = [select id, name, value__c from invoice_number__c where name='Current' FOR UPDATE];
        Invoice_Number__c invoiceNumber = ll[0];
        
        
        String text=String.valueOf(invoiceNumber.Value__c.setScale(0)); 
        while (text.length() < 10)  
                { 
                text = '0' + text; 
                }
        pb.Invoice_Number__c=text;
        
        invoiceNumber.Value__c=invoiceNumber.Value__c+1;
        update invoiceNumber;
        system.debug('**MJ**  '+ JSON.serializePretty(prebill));
        system.debug('**MJ**  '+ text);
        update pb;
        }
        
        Prebill_Response__c prebillResponse = new Prebill_Response__c(
          Daily_Bill__c = dailyBill.Id,
          Name = parentAccount.Name
        );
        insert prebillResponse;
      }
    }
  }
  
  private Map<Id, Id> reverseIdMap(Map<Id, Id> idMap) {
    Map<Id, Id> reversedMap = new Map<Id, Id>();
    for (Id key : idMap.keySet()) {
      reversedMap.put(idMap.get(key), key);
    }
    return reversedMap;
  }
  
  private Daily_Bill__c selectDailyBill(Date billingDate) {

    return [
      Select
        Id, Prebill_Responses_Processed__c, Date__c
      from
        Daily_Bill__c
      where
        Date__c = :billingDate

      limit 1
    ];
  }

  private Account selectParentAccount(Prebill__c prebill) {
    return [
      Select
        Id, Billing_Cycle__c, Name
      from
        Account
      where 
        Id in (Select Payer__c from Billing_Account__c where Id = :prebill.Billing_Account__c)
      limit 1
    ];
  }

  private Boolean isGermanyMapping(Journal_Mapping__c journalMapping) {
    return 'DE' == journalMapping.Country__c || 'Deutschland' == journalMapping.Country__c;
  }

  /**
   * @description Mutates the callback message, adding the information extracted from journal mappings
   */ 
  private void addJournalMappingInformation(CallbackMessage cm) {
    // prebill id → billing account id 
    map<Id, Id> prebillToBillingAccountIds = new map<Id, Id>();

    for (SObject obj : cm.objectsToInsert) {
      if (Prebill__c.sObjectType == obj.getSObjectType()) {
        Prebill__c prebill = (Prebill__c) obj;
        prebillToBillingAccountIds.put(prebill.Id, prebill.Billing_Account__c);
      }
    }

    // billing account id → billing account with payer VAT information
    Map<Id, Billing_Account__c> VATBillingAccounts = new Map<Id, Billing_Account__c>(
      [Select Id, VAT__c From Billing_Account__c where Id in :prebillToBillingAccountIds.values()]
    );

    for (SObject obj : cm.objectsToInsert) {
      if (Prebill_Line_Item__c.sObjectType == obj.getSObjectType()) {
        
        Prebill_Line_Item__c pbli = (Prebill_Line_Item__c) obj;

        String journalMappingName = productJournalMappingsMap.get(pbli.Name);

        Billing_Account__c billingAccount = VATBillingAccounts.get(prebillToBillingAccountIds.get(pbli.Prebill__c));
        String billingAccountVAT = billingAccount.VAT__c;
        
        if (this.nameVATJournalMappingsMap.get(journalMappingName) != null) {
          JournalMappingHelper.addInformation(pbli, this.nameVatJournalMappingsMap.get(journalMappingName).get(billingAccountVAT));
        }
      }
    }
    system.debug(cm);
  }

  private List<Journal_Mapping__c> selectJournalMappingsByName(List<String> names) {
    return [
      Select
        Id,
        Name,
        Konto__c,
        KoSt__c,
        Prod__c,
        Projekt__c,
        Revenue__c,
        Country__c,
        I_CO__c,
        Unique_Identifier__c
      from
        Journal_Mapping__c
      where
        Name in :names
    ];
  }

  /**
   * @description Processes all inserts defined by CallbackMessage.objectsToInsert. ObjectGraph instance is created
   * and idMapInitializers are set as defined by the CallbackMessage. ObjectGraph executes the inserts.
   * @param cm CallbackMessage
   * @param oldToNewIdsMap map of old-to-new ids used when inserting in chunks to pass the references between messages
   */
  private void insertObjects(CallbackMessage cm, Map<Id, Id> oldToNewIdsMap) {
    
    system.debug('**IP** callback message before insert '+ JSON.serializePretty(cm));
    Map<Id, String> idsMap = new Map<Id, String>();
    idsMap.putAll((Map<Id, String>)oldToNewIdsMap);
    for (String idStr : cm.idsToMap) {
      idsMap.put(idStr, idStr);
    }
    idsMap.putAll(cm.idsToSoqlMap);

    csog_tw1.ObjectGraph insertOg = objectGraphFactory(cm);
    insertOg.addObjects(cm.objectsToInsert, false);
    insertOg.addIdMapInitializers(idsMap);
    List<List<sObject>> ogGenerations = insertOg.getGraph();
    system.debug('**IP** ogGenerations '+ JSON.serializePretty(ogGenerations));

    insertOg.storeGraph(true);

    for (SObject obj : cm.objectsToInsert) {
      oldToNewIdsMap.put(obj.Id, insertOg.resolveReference(obj.Id));
    }
  }

  /**
   * @description Create new ObjectGraph instance from CallbackMessage instance. ObjectGraph is constructed using all
   * objects from CallbackMessage.objectsInGraph defined with csog_tw1.ObjectGraph.STRATEGY_INCLUDE_ALL.
   * @param cm CallbackMessage
   * @return csog_tw1.ObjectGraph instance
   */
  private static csog_tw1.ObjectGraph objectGraphFactory(CallbackMessage cm) {
    List<String> objectsInGraph = cm.objectsInGraph;
    Map<String, String> objectGraphInitMap = new Map<String, String>();

    for (String objectInGraph : objectsInGraph) {
      objectGraphInitMap.put(objectInGraph, csog_tw1.ObjectGraph.STRATEGY_INCLUDE_ALL);
    }

    csog_tw1.ObjectGraph og = new csog_tw1.ObjectGraph(objectGraphInitMap,
                  csog_tw1.ObjectGraph.NO_READONLY_DATETIME_FIELDS);

    return og;
  }
}