global class ArchiveFilesMessageInboundProcessor implements csam_t1.InboundMessageProcessor {
	
	/**
	* Accepts all archiving messages (should be put before the standard Inbound Message Handler)
	* @param incomingMessage message to be processed
	* @return Boolean returns true
	*/
	public Boolean isMessageProcessible(csam_t1__Incoming_Message__c incomingMessage) {
		return incomingMessage.csam_t1__Incoming_URL_Path__c == '/csam_t1/callback/archiving/archive';
	}
	
	/**
	* Assumes that the message payload is a status message from integration platform
	* 
	* @param incomingMessage message to be processed with corresponding payload attachment
	* @param oldToNewIdsMap map of old-to-new ids used when inserting in chunks to pass the references between messages
	*/
	public void processMessage(csam_t1__Incoming_Message__c incomingMessage, Map<Id, Id> oldToNewIdsMap) {
		try {
			
			Attachment attachment = [
				SELECT Id, Name, Body
				FROM Attachment
				WHERE ParentId = :incomingMessage.Id
				AND Name like 'Message%' LIMIT 1
			];
			
			incomingMessage.csam_t1__Status__c = 'Processed';
			
			String message = attachment.Body.toString();
			updateStatuses(incomingMessage, message);
			
		} catch (Exception e) {
			incomingMessage.csam_t1__Status__c = 'Process Error';

			Attachment errorAttach = new Attachment();
			errorAttach.Body = Blob.valueOf(e.getStackTraceString());
			errorAttach.parentId = incomingMessage.Id;

			String fullName = 'Error_' + e.getMessage();
			errorAttach.Name = fullName.abbreviate(255);
			
			insert errorAttach;
		}
		
		incomingMessage.csam_t1__Processed_At__c = System.now();
		update incomingMessage;
	}
	
	/**
	 * Updates status fields
	 */ 
	private void updateStatuses(csam_t1__Incoming_Message__c incomingMessage, String status) {
		
		String statusField = incomingMessage.csam_t1__Outgoing_Message__r.csam_t1__ObjectGraph_Callout_Handler__r.csam_t1__Status_Field__c;
		system.debug('**IP** statusField: ' + statusField);
			
		if (!String.isEmpty(statusField)) {
			
			List<csam_t1__Outgoing_Message_Record__c> outgoingMessageRecords = [
				SELECT
					csam_t1__Object_Name__c, csam_t1__Object_Record_Id__c
				FROM
					csam_t1__Outgoing_Message_Record__c
				WHERE
					csam_t1__Outgoing_Message__c = :incomingMessage.csam_t1__Outgoing_Message__c
			];
							
			List<Id> outgoingRecordIds = new List<Id>();
			
			for (csam_t1__Outgoing_Message_Record__c outgoingMessageRecord : outgoingMessageRecords) {
				outgoingRecordIds.add(outgoingMessageRecord.csam_t1__Object_Record_Id__c);
			}

			String queryString = 'SELECT Id, ' + statusField
				+ ' FROM ' + incomingMessage.csam_t1__Outgoing_Message__r.csam_t1__ObjectGraph_Callout_Handler__r.csam_t1__Startpoint_Type_Name__c
				+ ' WHERE Id in (\''
				+ String.join(outgoingRecordIds, '\',\'') + '\')';
			
			system.debug('**IP** queryString: ' + queryString); 
			
			List<sObject> statusUpdateList = Database.query(queryString);
			system.debug('**IP** statusUpdateList: '+ JSON.serializePretty(statusUpdateList)); 

			for (sObject o : statusUpdateList) {
				o.put(statusField, status.abbreviate(255));
			}

			update statusUpdateList;
		}
	}
}