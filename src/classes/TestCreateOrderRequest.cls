@isTest
private class TestCreateOrderRequest
{
	static testMethod void retrieveProducts()
	{
		//
		// Create frame contract with product bundle
		//
    	No_Trigger__c notriggers = new No_Trigger__c();
    	notriggers.Flag__c = true;
    	insert notriggers;

		TestHelper.createFrameContract();
		Id frameContractId = [ SELECT Id FROM Frame_Contract__c LIMIT 1].Id;

    	notriggers.Flag__c = false;
    	update notriggers;

		Test.startTest();
		//
		// Retrieving of products
		//
		CreateOrderRequest orderRequest = new CreateOrderRequest(frameContractId, new InsertHelper());
		List<ProductOrderContainer> products = orderRequest.getProductOrders();

		Test.stopTest();

		//
		// Test products
		//
		System.assertEquals(3, products.size());
		System.assertEquals('SmartMeter - MR_CarModem', products[0].name);
		System.assertEquals(false, products[0].child);
		System.assertEquals('SIM & Tariff', products[1].name);
		System.assertEquals(false, products[1].child);
		System.assertEquals('Tariff Option (SIM AIS Test JS)', products[2].name);
		System.assertEquals(true, products[2].child);
	}

	static testMethod void createOrderRequest()
	{
		//
		// Create frame contract with product bundle
		//
    	No_Trigger__c notriggers = new No_Trigger__c();
    	notriggers.Flag__c = true;
    	insert notriggers;

		TestHelper.createFrameContract();
		Frame_Contract__c frameContract =
				[ SELECT Id, Product_Bundle__c, Opportunity__c,
				  Contact__c, Customer__c
				  FROM Frame_Contract__c
				  LIMIT 1];
		Order_Request__c orderReq = new Order_Request__c();
		orderReq.Account__c = [ SELECT Id FROM Account LIMIT 1].Id;
		Id billingAccountId = [ SELECT Id FROM Billing_Account__c LIMIT 1].Id;
		orderReq.Billing_Account__c = billingAccountId;

    	notriggers.Flag__c = false;
    	update notriggers;

		Test.startTest();
		//
		// Test creating of order request
		//
		CreateOrderRequest orderRequest = new CreateOrderRequest(frameContract.Id, new InsertHelper());
		List<ProductOrderContainer> products = orderRequest.getProductOrders();
		products[0].quantity = 50;
		products[1].quantity = 0;
		products[2].quantity = 20;
		Id orderRequestId = orderRequest.create(orderReq);

		Test.stopTest();
		//
		// Test order request
		//
		orderReq =
				[ SELECT Id, Name, Frame_Contract__c, Status__c, Account__c,
				  Billing_Account__c
				  FROM Order_Request__c
				  WHERE Id = :orderRequestId ];
		Product_Order__c[] productOrders =
				[ SELECT Id, Name, Quantity__c FROM Product_Order__c WHERE Order_Request__c = :orderRequestId ];
		Product_Configuration_Order__c[] productConfigurationOrders =
				[ SELECT Id, Name, Quantity__c, Product_Configuration__r.Offer_Name__c
				  FROM Product_Configuration_Order__c
				  WHERE Product_Order__c = :new Map<Id, Product_Order__c>(productOrders).keySet() ];
		//
		// Order request
		//
		System.assertEquals(frameContract.Id, orderReq.Frame_Contract__c);
		System.assertEquals(frameContract.Customer__c, orderReq.Account__c);
		System.assertEquals(billingAccountId, orderReq.Billing_Account__c);
		System.assertEquals('Open', orderReq.Status__c);
		//
		// Product orders
		//
		System.assertEquals(2, productOrders.size());
		System.assertEquals('SmartMeter - MR_CarModem', productOrders[0].Name);
		System.assertEquals(50, productOrders[0].Quantity__c);

		System.assertEquals('Tariff Option (SIM AIS Test JS)', productOrders[1].Name);
		System.assertEquals(20, productOrders[1].Quantity__c);
		//
		// Product configuration orders
		//
		System.assertEquals(2, productConfigurationOrders.size());
		System.assertEquals(50, productConfigurationOrders[0].Quantity__c);
		System.assertEquals('SmartMeter', productConfigurationOrders[0].Product_Configuration__r.Offer_Name__c);

		System.assertEquals(20, productConfigurationOrders[1].Quantity__c);
		System.assertEquals(null, productConfigurationOrders[1].Product_Configuration__r.Offer_Name__c);
	}

	static testMethod void createOrderRequestWithSRF()
	{
		//
		// Create frame contract with product bundle
		//
    	No_Trigger__c notriggers = new No_Trigger__c();
    	notriggers.Flag__c = true;
    	insert notriggers;

		TestHelper.createFrameContract();
		Frame_Contract__c frameContract =
				[ SELECT Id, Product_Bundle__c, Opportunity__c,
				  Contact__c, Customer__c
				  FROM Frame_Contract__c
				  LIMIT 1];
		Order_Request__c orderReq = new Order_Request__c();
		orderReq.Account__c = [ SELECT Id FROM Account LIMIT 1].Id;
		Id billingAccountId = [ SELECT Id FROM Billing_Account__c LIMIT 1].Id;
		orderReq.Billing_Account__c = billingAccountId;

    	notriggers.Flag__c = false;
    	update notriggers;

		Test.startTest();
		//
		// Test creating of order request
		//
		CreateOrderRequest orderRequest = new CreateOrderRequest(frameContract.Id, new InsertHelper());
		List<ProductOrderContainer> products = orderRequest.getProductOrders();
		products[0].quantity = 0;
		products[1].quantity = 50;
		products[2].quantity = 20;
		Id orderRequestId = orderRequest.create(orderReq);

		Test.stopTest();
		//
		// Test order request
		//
		orderReq =
				[ SELECT Id, Name, Frame_Contract__c, Status__c
				  FROM Order_Request__c
				  WHERE Id = :orderRequestId ];
		SRF__c[] srfs =
				[ SELECT Account__c, Contact__c, SIM_Type__c,
				  Opportunity__c, RecordType.DeveloperName
				  FROM SRF__c
				  WHERE Order_Request__c = :orderReq.Id];
		Article_Info_Set__c[] articleInfoSets =
				[ SELECT Id
				  FROM Article_Info_Set__c
				  WHERE RecordType.Name = 'SIM'];
		//
		// SRFs
		//
		System.assertEquals(1, srfs.size());
		System.assertEquals('Standard_SRF', srfs[0].RecordType.DeveloperName);
		System.assertEquals(frameContract.Customer__c, srfs[0].Account__c);
		System.assertEquals(frameContract.Contact__c, srfs[0].Contact__c);
		System.assertEquals(frameContract.Opportunity__c, srfs[0].Opportunity__c);
		System.assertEquals(articleInfoSets[0].Id, srfs[0].SIM_Type__c);
	}

	static testMethod void createOrderRequestWithoutSRF()
	{
		//
		// Create frame contract with product bundle
		//
    	No_Trigger__c notriggers = new No_Trigger__c();
    	notriggers.Flag__c = true;
    	insert notriggers;

		TestHelper.createFrameContract();
		Frame_Contract__c frameContract =
				[ SELECT Id, Product_Bundle__c, Opportunity__c,
				  Contact__c, Customer__c
				  FROM Frame_Contract__c
				  LIMIT 1];
		Order_Request__c orderReq = new Order_Request__c();
		orderReq.Account__c = [ SELECT Id FROM Account LIMIT 1].Id;
		Id billingAccountId = [ SELECT Id FROM Billing_Account__c LIMIT 1].Id;
		orderReq.Billing_Account__c = billingAccountId;

    	notriggers.Flag__c = false;
    	update notriggers;

		Test.startTest();
		//
		// Test creating of order request
		//
		CreateOrderRequest orderRequest = new CreateOrderRequest(frameContract.Id, new InsertHelper());
		List<ProductOrderContainer> products = orderRequest.getProductOrders();
		products[0].quantity = 50;
		products[1].quantity = 0;
		products[2].quantity = 20;
		Id orderRequestId = orderRequest.create(orderReq);

		Test.stopTest();
		//
		// Test order request
		//
		orderReq =
				[ SELECT Id, Name, Frame_Contract__c, Status__c
				  FROM Order_Request__c
				  WHERE Id = :orderRequestId ];
		SRF__c[] srfs =
				[ SELECT Account__c, Contact__c, SIM_Type__c
				  FROM SRF__c
				  WHERE Order_Request__c = :orderReq.Id];
		//
		// SRFs
		//
		System.assertEquals(0, srfs.size());
	}

}