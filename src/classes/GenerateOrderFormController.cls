public class GenerateOrderFormController {
    
    private final String congaURL;
    
    public GenerateOrderFormController(ApexPages.StandardController stdCtrl) {
        congaURL = ApexPages.currentPage().getParameters().get('congaURL');
    }
    
    public String getCongaURL() {
        return congaUrl;
    }
}