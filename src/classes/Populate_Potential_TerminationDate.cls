/*
@@Description : To Populate Stock's End date as Potential Termination date.
*/
global class Populate_Potential_TerminationDate implements Database.Batchable<sObject>{

    static final Date tdy  = system.today();
    global Database.QueryLocator start(Database.BatchableContext BC){
        Date third_Month_from_Now  = tdy.addMonths(3);
        string active = 'ACTIVE';
        string query = 'select id,End_Date__c,Potential_Termination_Date__c from Stock__c where End_date__c !=null and Potential_Termination_Date__c = null' ;
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Stock__c> scope){
        if(scope != null && scope.size() > 0 ){
            for(Stock__c sim : scope){
                if(sim.End_date__c != null)
                    sim.Potential_Termination_Date__c = sim.End_date__c;
            }
            update scope;
        }
    }
    
    global void finish(Database.BatchableContext BC){

    }
}