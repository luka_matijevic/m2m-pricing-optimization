@isTest
private class MonthlyServiceScheduleRefreshTest
{
	@isTest static void test_execute() {
		test.startTest();

	        String CRON_EXP = '0 0 0 15 3 ? 2022';
	        System.schedule('ScheduleApexClassTest', CRON_EXP, new MonthlyServiceScheduleRefresh());

        test.stopTest();
        
	}
}