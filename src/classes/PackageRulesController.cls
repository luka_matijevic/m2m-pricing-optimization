public class PackageRulesController {
    public string S = '{"keydefinitions":[{"keyname":"key1","configuration":"BAiO","attribute":"numberofUsers","value":"value"},{"keyname":"key2","configuration":"BAiO","attribute":"numberofIPUsers","value":"value"},{"keyname":"key3","configuration":"BAiO","attribute":"numberofAllUsers","value":"value"}],"affectedconfigs":[{"configuration":"Hosted Voice"},{"configuration":"Broadband"}],"mappings":[{"keyname":"key1","configuration":"Broadband","attribute":"numberofUsers",},{"keyname":"key2","configuration":"Broadband","attribute":"numberofIPUsers",},{"keyname":"key3","configuration":"Hosted Voice","attribute":"numberofAllUsers"}]}';
    public  List<String> GetAttributes(String ConfigName) {        
        List<String> attributes = new List<String>();
        JSONStructure sc = (JSONStructure)JSON.deserialize(S, JSONStructure.class);
        for(Mapping m:sc.Mappings) {
            if (m.Configuration==ConfigName) attributes.add(m.Attribute);
        }
        system.debug(attributes);
        return(attributes);
    }
     
    public List<String> GetAttributeValue(Id ConfigId,String ConfigName,String attributeName) {        
   List<String> attributes = new List<String>();
        JSONStructure sc = (JSONStructure)JSON.deserialize(S, JSONStructure.class);
        for(Mapping m:sc.Mappings) {
            if (m.Configuration==ConfigName) attributes.add(m.Attribute);
        }
        system.debug(attributes);
        return(attributes);
    } 

public class JSONStructure
{
    public List<KeyDefinition> Keys;
    public List<AffectedConfig> Configs;
    public List<Mapping> Mappings;
}

public class KeyDefinition
{
    public String KeyName;
    public String Configuration;
    public String Attribute;
    public String Value;
}
public class AffectedConfig
{
    public String Configs;
}
public class Mapping
{
    public String Keyname;
    public String Configuration;
    public String Attribute;
    
}
    
}