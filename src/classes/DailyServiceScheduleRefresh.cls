global class DailyServiceScheduleRefresh implements Schedulable {
	global void execute(SchedulableContext sc) {

		DailyServiceRefreshReports rr = new DailyServiceRefreshReports('Daily');
		Database.executeBatch(rr);

	}
}