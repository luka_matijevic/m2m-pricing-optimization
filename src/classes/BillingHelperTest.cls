@isTest
private class BillingHelperTest {

	@isTest static void myUnitTest() {
		System.assertEquals(Date.newInstance(2014,6,13), BillingHelper.billingDate(13, Date.newInstance(2014,6,13)));
		System.assertEquals(Date.newInstance(2014,5,13), BillingHelper.billingDate(13, Date.newInstance(2014,5,14)));
		System.assertEquals(Date.newInstance(2014,2,27), BillingHelper.billingDate(27, Date.newInstance(2014,3,14)));
		System.assertEquals(null, BillingHelper.billingDate(52, Date.newInstance(2014,3,14)));
	}
}