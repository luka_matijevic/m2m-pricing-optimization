global class BatchUpdateSimActivationDate implements Database.Batchable<sObject> {
	
	private static final String query = 'Select Id, Activation_Date__c, Deactivation_Date__c, Status__c, SM2M_Status__c, State_Change_Date__c from Stock__c';
	
	public Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);	
	}
	
	public void execute(Database.BatchableContext BC, List<SObject> scope) {
		List<Stock__c> sims = (List<Stock__c>) scope;
		for (Stock__c sim : sims) {
			if (sim.SM2M_Status__c == Constants.ACTIVE && sim.Activation_Date__c == null) {
				sim.Activation_Date__c = sim.State_Change_Date__c;
			} else if (sim.SM2M_Status__c == Constants.DEACTIVATED && sim.Deactivation_Date__c == null) {
				sim.Deactivation_Date__c = sim.State_Change_Date__c;
			} 
		}
		update sims;
	}
	
	public void finish(Database.BatchableContext BC) {
		
	}
}