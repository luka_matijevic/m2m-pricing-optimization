/*
* BillingAcountTriggerDelegate extends Trigger handler to execute calls based on events.
*/
public with sharing class ContactTriggerDelegate extends TriggerHandler.DelegateBase {

    public override void prepareBefore() {
    }

    public override void prepareAfter() {
        // do any preparation here – bulk loading of data etc
    }
    
  public override void beforeInsert(sObject o) {
        // Apply before insert logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method    
           Contact oldContact = (Contact)o;
        
        if (oldContact.Secured_Account__c) {
            Contact c = new Contact();
            oldContact.FirstName = 'Marcel';
            oldContact.LastName = 'Gross';
            oldContact.email = 'marcel.gross@telefonica.com ';
            //c.AccountId= oldContact.AccountId;
            
            //oldContact=c;
        }
        
        
    }
    
    public override void beforeUpdate(sObject old, sObject o) {
          Contact oldContact = (Contact)old;
        Contact newContact = (Contact)o;
        
        if (oldContact.Secured_Account__c==true) {
            Contact c = new Contact();
            newContact.FirstName = 'Marcel';
            newContact.LastName = 'Gross';
            newContact.email = 'marcel.gross@telefonica.com ';
            //c.AccountId= oldContact.AccountId;
            
            //newContact=c;
            system.debug(newContact);
        }
    }

    public override void beforeDelete(sObject o) {
        // Apply before delete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterInsert(sObject o) {
      
        
        // Apply after insert logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterUpdate(sObject old, sObject o) {
      
        
        // Apply after update logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterDelete(sObject o) {
        // Apply after delete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterUndelete(sObject o) {
        // Apply after undelete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void finish() {
       /*if(provisionAccount) {
           provisionAccountMethod(acctID);
       }*/
    }
    
}