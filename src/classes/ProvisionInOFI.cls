public with sharing class ProvisionInOFI
{
	private Id billingAccountId;
	private ISendEmail sendEmail;

	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	public ProvisionInOFI(Id billingAccountId, ISendEmail sendEmail)
	{
		this.billingAccountId = billingAccountId;
		this.sendEmail = sendEmail;
	}

	//--------------------------------------------------------------------------
	// Provision billing account in OFI
	//--------------------------------------------------------------------------
	public void provision()
	{
		Billing_Account__c billingAccount =
				[ SELECT Name, Account_Number__c, Account_Street__c, Account_City__c,
    			  Account_Postcode__c, Account_Country__c, Billing_Language__c,
    			  Payer__r.Phone, Payer__r.Phone__c, Payer__r.Fax,
    			  Payer__r.Company_Email__c, Payer__r.Value_Added_Tax_ID__c,
    			  Bank_Identifier__c, BIC__c, Bank_Name__c, Bank_Address__c,
    			  Payment_Term__c, Payment_Method__c, Payer__r.Sales_Representative__c,
    			  OFI_Provisioning_Status__c,
    			  Override_Account_Address__c, Billing_Street__c, Billing_City__c,
    			  Billing_Postcode__c, Billing_Country__c
    	          FROM Billing_Account__c
				  WHERE Id = :billingAccountId ];

		//
		// Generate CSV table
		//
		StringRow header = new StringRow(new List<String>
		{
			'ALTKN',
			'KUNNR',
			'NAME',
			'STRAS',
			'ORT01',
			'PSTLZ',
			'LAND1',
			'SPRAS',
			'TELF1',
			'TELF2',
			'TELFX',
			'FAX_NUMBER',
			'SMTP_ADDR',
			'STCEG',
			'BANKS',
			'BANKL',
			'BANKN',
			'BANKA',
			'ZTERM',
			'ZWELS',
			'MANSP',
			'VTRIEB',
			'UPDTYP'
		});
		CustomerRow customer = new CustomerRow(billingAccount);
		CSVTable table = new CSVTable(new List<IRow>{ header, customer }, ',', '\r\n');
		String attachment = table.generate();
		String attachmentName = 'O2_CUSTOMER_EXCEL_' + DateTime.now().format('yyyyMMdd') + '.csv';
		//
		// Send email to OFI
		//
		sendEmail.setAttachment(attachmentName, attachment);
		sendEmail.send();
		//
		// Create attachment to billing account with CSV
		//
		Attachment att = new Attachment();
		att.Name = attachmentName;
		att.Body = Blob.valueOf(attachment);
		att.ParentId = billingAccountId;
		insert att;

		billingAccount.OFI_Provisioning_Status__c = Constants.IN_PROGRESS;
		update billingAccount;
	}
}