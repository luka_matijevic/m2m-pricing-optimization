public with sharing class GutschriftTriggerDelegate extends TriggerHandler.DelegateBase  {
    
	Storno__c storno = new Storno__c();
	public override void prepareBefore() {

	}

	public override void prepareAfter() {
	
	}
     
     public override void beforeUpdate(sObject o, sObject n) {
     Gutschrift__c gsOld = (Gutschrift__c)o;
     Gutschrift__c gsNew = (Gutschrift__c)n;
     
     if (gsOld.status__c!='Cancelled' && gsNew.status__c=='Cancelled') {
     List<Daily_Bill_Item__c> lb = [Select id from Daily_Bill_Item__c where Rechnungsnummer__c=:gsOld.Invoice_Number__c];
     if (!lb.isEmpty()) delete lb[0];
     }
     
       
     if (gsOld.status__c=='Cancelled' && gsNew.status__c!='Cancelled') {
        Daily_Bill_Item__c db = new Daily_Bill_Item__c();
                
        List<Billing_Account__c> billAccountList = [SELECT id, OFI_ID__c from Billing_Account__c where payer__c=:gsNew.Account__c];
        if (!billAccountList.isEmpty()) db.VO_Nr__c=billAccountList[0].OFI_ID__c;
          
 
        db.Anweisung__c=gsNew.Anweisung__c;
        db.Account__c=gsNew.Account__c;
        db.Art__c=integer.valueOf(gsNew.Art__c);
        db.VO_Nr__c=gsNew.Billing_Account__r.OFI_ID__c;
        db.Beschreibung__c=gsNew.name + ' ' + gsNew.reason__c;
        db.Betrag__c=gsNew.Einzel_Nettobetrag__c;
        db.Daily_Bill__c=gsNew.Daily_Bill__c;
        db.Einzel_Bruttobetrag__c=gsNew.Einzel_Bruttobetrag__c;
        db.Einzel_Nettobetrag__c=gsNew.Einzel_Nettobetrag__c;
        db.Einzel_Taxbetrag__c=gsNew.Einzel_Taxbetrag__c;
        db.Gesamt_Bruttobetrag__c=gsNew.Einzel_Bruttobetrag__c;
        db.Gesamt_Nettobetrag__c=gsNew.Einzel_Nettobetrag__c;
        db.Gesamt_Taxbetrag__c=gsNew.Einzel_Taxbetrag__c;
        db.I_CO__c='';
        db.Konto__c='4120100';
        db.kost__c='465605';
        db.prod__c='387654';
        db.menge__c=1;
        db.Rechnungsnummer__c=gsNew.Invoice_Number__c;
        db.Steuersatz__c=19;
        db.Waehrung__c='EUR';
        
        insert db;
     }
       
     }
     
    public override void afterInsert(sObject o) {
        Gutschrift__c gs = (Gutschrift__c)o;
        Daily_Bill_Item__c db = new Daily_Bill_Item__c();
                
        List<Billing_Account__c> billAccountList = [SELECT id, OFI_ID__c from Billing_Account__c where payer__c=:gs.Account__c];
        if (!billAccountList.isEmpty()) db.VO_Nr__c=billAccountList[0].OFI_ID__c;
          
 
        db.Anweisung__c=gs.Anweisung__c;
        db.Account__c=gs.Account__c;
        db.Art__c=integer.valueOf(gs.Art__c);
        db.VO_Nr__c=gs.Billing_Account__r.OFI_ID__c;
        db.Beschreibung__c=gs.name + ' ' + gs.reason__c;
        db.Betrag__c=gs.Einzel_Nettobetrag__c;
        db.Daily_Bill__c=gs.Daily_Bill__c;
        db.Einzel_Bruttobetrag__c=gs.Einzel_Bruttobetrag__c;
        db.Einzel_Nettobetrag__c=gs.Einzel_Nettobetrag__c;
        db.Einzel_Taxbetrag__c=gs.Einzel_Taxbetrag__c;
        db.Gesamt_Bruttobetrag__c=gs.Einzel_Bruttobetrag__c;
        db.Gesamt_Nettobetrag__c=gs.Einzel_Nettobetrag__c;
        db.Gesamt_Taxbetrag__c=gs.Einzel_Taxbetrag__c;
        db.I_CO__c='';
        db.Konto__c='4120100';
        db.kost__c='465605';
        db.prod__c='387654';
        db.menge__c=1;
        db.Rechnungsnummer__c=gs.Invoice_Number__c;
        db.Steuersatz__c=19;
        db.Waehrung__c='EUR';
        
        insert db;
          
    }

    public override void beforeInsert(sObject o) {
    
    Gutschrift__c gs = (Gutschrift__c)o;
  
    List<Prebill__c> prebills = [Select id, 	Total_Net_Amount__c,Account_ID__c,Billing_Account_ID__c,
					Total_Tax_Amount__c,Invoice_Number__c,Daily_bill__c,
					Total_Gross_Amount__c from Prebill__c where id= :gs.Prebill__c];
  
      if(!test.isrunningtest())
        {
  	Invoice_Number__c invoiceNumber = Invoice_Number__c.getInstance('Current');  
				
	String text=String.valueOf(invoiceNumber.Value__c.setScale(0)); 
	while (text.length() < 10)  
    { 
    text = '0' + text; 
    }
	gs.Invoice_Number__c=text;
	
	if (!prebills.isEmpty()){
	    Prebill__c prebill = prebills[0];
        //gs.Daily_bill__c = prebill.Daily_bill__c;
        gs.Account__c=prebill.Account_ID__c;
        gs.Billing_account__c=prebill.Billing_Account_ID__c;
    }
    else
    {
        List<Billing_Account__c> ba = [select id from billing_account__c where Payer__c=:gs.Account__c];
        if(!ba.isEmpty() && gs.Account__c!=null)
        {
            gs.Billing_Account__c=ba[0].id;
        }
        else 
        {
            List<Billing_Account__c> ba2 = [select Payer__c from billing_account__c where id=:gs.Billing_account__c];
            if (!ba2.isEmpty())
            {
            List<Account> ac = [select id from account where id=:ba2[0].Payer__c ];
            if (!ac.isEmpty())
            {
                gs.account__c=ac[0].id;    
            }
                
            }
        }
    }
	
	invoiceNumber.Value__c=invoiceNumber.Value__c+1;
	update invoiceNumber;
        }
    }
	
	
	public override void finish() {

    
	}
}