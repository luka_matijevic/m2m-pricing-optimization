@isTest
public class UDOUtils_Test {
    /*
    @testSetup
    static void prepareData(){
        UDO_credentials__c ucred = new UDO_credentials__c(username__c = 'userName',password__c = 'password');
        insert ucred;
    
        Account acc = Test_Util.createAccount('Account Test');
        insert acc;
        
        List<Case> caseLst = new list<Case>();
        for(integer i=0;i<10;i++){
            case c = Test_Util.createCase('Service', 'Activation','Open', acc.id, 'Subject', '2','Standard','Email','High','abc@abc.com');
            c.UDo_Ticket_created__c = true;
            caseLst.add(c);
        }
        insert caseLst;
    }
*/
    
    static testMethod void RefreshTicketsFromUDOBatch_Test(){
        test.startTest();
        
        CaseNCaseComments c = new CaseNCaseComments(new case(), new list<CaseComment>());
        
        Database.executeBatch(new RefreshTicketsFromUDOBatch());
        test.stopTest();
        
    }
    
    static testMethod void RefreshTicketsFromUDoScheduler_Test(){
        Test.startTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        //RefreshTicketsFromUDoScheduler.scheduleIt();
        System.schedule('RefreshTicketsFromUDoScheduler',CRON_EXP,new RefreshTicketsFromUDoScheduler());
        Test.stopTest();
    }
    
    static testMethod void testSendToUdoCallout() {
        
        Account acc = Test_Util.createAccount('Account Test');
        insert acc;
        Case c = Test_Util.createCaseForUdo('Service', 'Activation','Open', acc.id, 'Subject', '2','Standard','Email','High','abc@abc.com', 'udo description, testing characther length, twenty');
        insert c;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UDOUtilsHTTPFakeCalloutsImpl());
        String res = UDOUtils.SendToUDo(c.Id, 'CS_GLOBALSIM');
        
        /*String contentType = res.getHeader('Content-Type');
        System.assert(contentType == 'application/json');
        String actualValue = res.getBody();
        String expectedValue = '{"foo":"bar"}';
        System.assertEquals(actualValue, expectedValue);*/
        //System.assertEquals('Ticket is already created in UDo!', res);
        Test.stopTest();
    }
    
    static testMethod void testResendToUdoCallout() {
        Account acc = Test_Util.createAccount('Account Test');
        insert acc;
        Case c = Test_Util.createCaseForUdo('Service', 'Activation','Open', acc.id, 'Subject', '2','Standard','Email','High','abc@abc.com', 'udo description, testing characther length, twenty');
        insert c;
        c.UDo_Ticket_created__c = true;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UDOUtilsHTTPFakeCalloutsImpl());
        String res = UDOUtils.ResendToUDo(c.Id);
        Test.stopTest();
    }
    
    static testMethod void testRefreshFromUdoCallout() {
        Account acc = Test_Util.createAccount('Account Test');
        insert acc;
        Case c = Test_Util.createCaseForUdo('Service', 'Activation','Open', acc.id, 'Subject', '2','Standard','Email','High','abc@abc.com', 'udo description, testing characther length, twenty');
        insert c;
        c.UDo_Ticket_created__c = true;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UDOUtilsHTTPFakeCalloutsImpl());
        String res = UDOUtils.RefreshFromUDo(c.Id);
        Test.stopTest();
    }
    
    static testMethod void testRefreshTicketFromUdoCallout() {
        Account acc = Test_Util.createAccount('Account Test');
        insert acc;
        Case c = Test_Util.createCaseForUdo('Service', 'Activation','Open', acc.id, 'Subject', '2','Standard','Email','High','abc@abc.com', 'udo description, testing characther length, twenty');
        insert c;
        c.UDo_Ticket_created__c = true;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UDOUtilsHTTPFakeCalloutsImpl());
        Case res = UDOUtils.RefreshTicketFromUDo(c.Id);
        Test.stopTest();
    }
    
    static testMethod void testRefreshTicketFromUdoCallout2() {
        Account acc = Test_Util.createAccount('Account Test');
        insert acc;
        Case c = Test_Util.createCaseForUdo('Service', 'Activation','Open', acc.id, 'Subject', '2','Standard','Email','High','abc@abc.com', 'udo description, testing characther length, twenty');
        insert c;
        c.UDo_Ticket_created__c = true;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UDOUtilsHTTPFakeCalloutsImpl());
        CaseNCaseComments res = UDOUtils.RefreshTicketFromUDo(c);
        Test.stopTest();
    }
    
    static testMethod void testCancelTicketInUdoCallout() {
        Account acc = Test_Util.createAccount('Account Test');
        insert acc;
        Case c = Test_Util.createCaseForUdo('Service', 'Activation','Open', acc.id, 'Subject', '2','Standard','Email','High','abc@abc.com', 'udo description, testing characther length, twenty');
        insert c;
        c.UDo_Ticket_created__c = true;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UDOUtilsHTTPFakeCalloutsImpl());
        UDOUtils.CancelTicketInUDo(c.Id);
        Test.stopTest();
    }
    
    static testMethod void testCloseTicketInUdoCallout() {
        Account acc = Test_Util.createAccount('Account Test');
        insert acc;
        Case c = Test_Util.createCaseForUdo('Service', 'Activation','Open', acc.id, 'Subject', '2','Standard','Email','High','abc@abc.com', 'udo description, testing characther length, twenty');
        insert c;
        c.UDo_Ticket_created__c = true;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UDOUtilsHTTPFakeCalloutsImpl());
        UDOUtils.CloseTicketInUDo(c.Id);
        Test.stopTest();
    }
    
    static testMethod void testUpdateTicketCommentInUdoCallout() {
        Account acc = Test_Util.createAccount('Account Test');
        insert acc;
        Case c = Test_Util.createCaseForUdo('Service', 'Activation','Open', acc.id, 'Subject', '2','Standard','Email','High','abc@abc.com', 'udo description, testing characther length, twenty');
        insert c;
        c.UDo_Ticket_created__c = true;
        CaseComment cc = Test_Util.createCaseComment('test', c.Id);
        insert cc;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UDOUtilsHTTPFakeCalloutsImpl());
        UDOUtils.UpdateTicketCommentInUDo(cc.Id);
        Test.stopTest();
    }
    
    static testMethod void testUpdateDescritpionInUdoCallout() {
        Account acc = Test_Util.createAccount('Account Test');
        insert acc;
        Case c = Test_Util.createCaseForUdo('Service', 'Activation','Open', acc.id, 'Subject', '2','Standard','Email','High','abc@abc.com', 'udo description, testing characther length, twenty');
        insert c;
        c.UDo_Ticket_created__c = true;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UDOUtilsHTTPFakeCalloutsImpl());
        UDOUtils.UpdateDescriptionInUDo(c.Id);
        Test.stopTest();
    }
    
    static testMethod void testUpdateSeverityInUdoCallout() {
        Account acc = Test_Util.createAccount('Account Test');
        insert acc;
        Case c = Test_Util.createCaseForUdo('Service', 'Activation','Open', acc.id, 'Subject', '2','Standard','Email','High','abc@abc.com', 'udo description, testing characther length, twenty');
        insert c;
        c.UDo_Ticket_created__c = true;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UDOUtilsHTTPFakeCalloutsImpl());
        UDOUtils.UpdateSeverityInUDo(c.Id);
        Test.stopTest();
    }
    
    static testMethod void testUpdateMassiveInUdoCallout() {
        Account acc = Test_Util.createAccount('Account Test');
        insert acc;
        Case c = Test_Util.createCaseForUdo('Service', 'Activation','Open', acc.id, 'Subject', '2','Standard','Email','High','abc@abc.com', 'udo description, testing characther length, twenty');
        insert c;
        c.UDo_Ticket_created__c = true;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UDOUtilsHTTPFakeCalloutsImpl());
        UDOUtils.UpdateMassiveInUDo(c.Id);
        Test.stopTest();
    }
    
    /*static testMethod void testSendFileInUdoCallout() {
        Account acc = Test_Util.createAccount('Account Test');
        insert acc;
        Case c = Test_Util.createCaseForUdo('Service', 'Activation','Open', acc.id, 'Subject', '2','Standard','Email','High','abc@abc.com', 'udo description, testing characther length, twenty');
        insert c;
        c.UDo_Ticket_created__c = true;
        Attachment att = Test_Util.createAttachment('test name', Blob.valueof('stringtoblob'), 'test desc', c.Id);
        insert att;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UDOUtilsHTTPFakeCalloutsImpl());
        UDOUtils.SendFileInUDo(att.Id);
        Test.stopTest();
    }*/
}