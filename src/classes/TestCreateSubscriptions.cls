//------------------------------------------------------------------------------
// Test creating of subscriptions
//------------------------------------------------------------------------------
@isTest
private class TestCreateSubscriptions
{

    //--------------------------------------------------------------------------
    // Create subscriptions
    //--------------------------------------------------------------------------
    static testMethod void createSubscriptions()
    {
        //
        // Create test order
        //
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers;

        TestHelper.createOrder();
        TestHelper.createJournalMappings();

        notriggers.Flag__c = false;
        update notriggers;

        csord__Order__c order =
                [ SELECT Id, Order_Request__c, Order_Request__r.Name,
                  Order_Request__r.Billing_Account__c,
                  Order_Request__r.Frame_Contract__r.Customer__c
                  FROM csord__Order__c ];

        //
        // Create subscriptions
        //
        Test.startTest();
        CreateSubscriptions createSubscriptions = new CreateSubscriptions(order.Id, new InsertHelper());
        createSubscriptions.create();
        Test.stopTest();

        //
        // Get product configuration orders
        //
        Product_Configuration_Order__c[] pcos =
                [ SELECT Id, Product_Configuration__c
                  FROM Product_Configuration_Order__c
                  WHERE Product_Configuration__r.Name != 'SIM & Tariff' ];
        Journal_Mapping__c journalMapping =
                [ SELECT Id
                  FROM Journal_Mapping__c
                  WHERE Name = 'Recuring Fee'
                  AND Country__c = 'Non-DE' ];
        //
        // Get created subscriptions and services
        //
        List<csord__Subscription__c> subscriptions =
                [ SELECT Name, Billing_Account__c, csord__Account__c, csord__Order__c,
                  RecordType.Name
                  FROM csord__Subscription__c
                  ORDER BY RecordType.Name DESC ];
        List<csord__Service__c> services =
                [ SELECT csord__Subscription__c, csord__Activation_Date__c,
                  Activation_Date__c, csord__Deactivation_Date__c, Billing_Rate_Model__c,
                  Product_Configuration_Order__r.Product_Order__r.Quantity__c,
                  ( SELECT Name, Total_Price__c, Journal_Mapping__c, csord__Is_Recurring__c
                    FROM csord__Service_Line_Items__r)
                  FROM csord__Service__c ];

        //
        // Test subscriptions
        //
        System.assertEquals(2, subscriptions.size());
        System.assertEquals('Subscription for ' + order.Order_Request__r.Name, subscriptions[0].Name);
        System.assertEquals('Hardware & Support', subscriptions[0].RecordType.Name);
        System.assertEquals(order.Id, subscriptions[0].csord__Order__c);
        System.assertEquals(order.Order_Request__r.Billing_Account__c, subscriptions[0].Billing_Account__c);
        System.assertEquals(order.Order_Request__r.Frame_Contract__r.Customer__c, subscriptions[0].csord__Account__c);
        System.assertEquals('Connectivity for ' + order.Order_Request__r.Name, subscriptions[1].Name);
        System.assertEquals('Connectivity', subscriptions[1].RecordType.Name);
        System.assertEquals(order.Id, subscriptions[1].csord__Order__c);
        System.assertEquals(order.Order_Request__r.Billing_Account__c, subscriptions[1].Billing_Account__c);
        System.assertEquals(order.Order_Request__r.Frame_Contract__r.Customer__c, subscriptions[1].csord__Account__c);
        //
        // Test services
        //
        System.assertEquals(1, services.size());
        System.assertEquals(subscriptions[0].Id, services[0].csord__Subscription__c);
        System.assertEquals(DateTime.now().date(), services[0].csord__Activation_Date__c);
        System.assertEquals(DateTime.now().date().addMonths(24), services[0].csord__Deactivation_Date__c);
        System.assertEquals(pcos[0].Id, services[0].Product_Configuration_Order__c);
        System.assertEquals('Rate Payment', services[0].Billing_Rate_Model__c);
        //
        // Test service line items
        //
        System.assertEquals(1, services[0].csord__Service_Line_Items__r.size());
        System.assertEquals('Recurring Target Fee Total', services[0].csord__Service_Line_Items__r[0].Name);
        System.assertEquals(120, services[0].csord__Service_Line_Items__r[0].Total_Price__c);
        System.assertEquals(journalMapping.Id, services[0].csord__Service_Line_Items__r[0].Journal_Mapping__c);
        System.assertEquals(true, services[0].csord__Service_Line_Items__r[0].csord__Is_Recurring__c);
    }

    //--------------------------------------------------------------------------
    // Create subscriptions without journal mappings
    //--------------------------------------------------------------------------
    static testMethod void createSubscriptionsWihoutJournalMappings()
    {
        //
        // Create test date
        //
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers;

        TestHelper.createOrder();

        notriggers.Flag__c = false;
        update notriggers;

        csord__Order__c order =
                [ SELECT Id, Order_Request__c, Order_Request__r.Name,
                  Order_Request__r.Billing_Account__c,
                  Order_Request__r.Frame_Contract__r.Customer__c
                  FROM csord__Order__c ];

        Test.startTest();
        try
        {
            CreateSubscriptions createSubscriptions = new CreateSubscriptions(order.Id, new InsertHelper());
            createSubscriptions.create(); // This should throw exception
            System.assert(false);
        }
        catch (JournalMappingException e) {}
        Test.stopTest();
        //
        // Tests
        //
        System.assert(true);
    }

    //--------------------------------------------------------------------------
    // Create subscriptions without billing rate model
    //--------------------------------------------------------------------------
    static testMethod void createSubscriptionsWithoutRateModel()
    {
        //
        // Create test order
        //
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers;

        TestHelper.createOrder();
        TestHelper.createJournalMappings();

        //
        // Clear billing rate model
        //
        cscfga__Product_Configuration__c[] configs =
                [ SELECT Billing_Rate_Model__c
                  FROM cscfga__Product_Configuration__c ];
        for (cscfga__Product_Configuration__c config : configs)
        {
            config.Billing_Rate_Model__c = null;
        }
        update configs;

        notriggers.Flag__c = false;
        update notriggers;

        csord__Order__c order =
                [ SELECT Id, Order_Request__c, Order_Request__r.Name,
                  Order_Request__r.Billing_Account__c,
                  Order_Request__r.Frame_Contract__r.Customer__c
                  FROM csord__Order__c ];

        //
        // Create subscriptions
        //
        Test.startTest();
        CreateSubscriptions createSubscriptions = new CreateSubscriptions(order.Id, new InsertHelper());
        createSubscriptions.create();
        Test.stopTest();

        //
        // Get product configuration orders
        //
        Product_Configuration_Order__c[] pcos =
                [ SELECT Id, Product_Configuration__c
                  FROM Product_Configuration_Order__c
                  WHERE Product_Configuration__r.Name != 'SIM & Tariff' ];
        Journal_Mapping__c journalMapping =
                [ SELECT Id
                  FROM Journal_Mapping__c
                  WHERE Name = 'Recuring Fee'
                  AND Country__c = 'Non-DE' ];
        //
        // Get created subscriptions and services
        //
        List<csord__Subscription__c> subscriptions =
                [ SELECT Name, Billing_Account__c, csord__Account__c, csord__Order__c,
                  RecordType.Name
                  FROM csord__Subscription__c
                  ORDER BY RecordType.Name DESC ];
        List<csord__Service__c> services =
                [ SELECT csord__Subscription__c, csord__Activation_Date__c,
                  Activation_Date__c, csord__Deactivation_Date__c, Billing_Rate_Model__c,
                  Product_Configuration_Order__r.Product_Order__r.Quantity__c,
                  ( SELECT Name, Total_Price__c, Journal_Mapping__c, csord__Is_Recurring__c
                    FROM csord__Service_Line_Items__r)
                  FROM csord__Service__c ];

        //
        // Test subscriptions
        //
        System.assertEquals(2, subscriptions.size());
        System.assertEquals('Subscription for ' + order.Order_Request__r.Name, subscriptions[0].Name);
        System.assertEquals('Hardware & Support', subscriptions[0].RecordType.Name);
        System.assertEquals(order.Id, subscriptions[0].csord__Order__c);
        System.assertEquals(order.Order_Request__r.Billing_Account__c, subscriptions[0].Billing_Account__c);
        System.assertEquals(order.Order_Request__r.Frame_Contract__r.Customer__c, subscriptions[0].csord__Account__c);
        System.assertEquals('Connectivity for ' + order.Order_Request__r.Name, subscriptions[1].Name);
        System.assertEquals('Connectivity', subscriptions[1].RecordType.Name);
        System.assertEquals(order.Id, subscriptions[1].csord__Order__c);
        System.assertEquals(order.Order_Request__r.Billing_Account__c, subscriptions[1].Billing_Account__c);
        System.assertEquals(order.Order_Request__r.Frame_Contract__r.Customer__c, subscriptions[1].csord__Account__c);
        //
        // Test services
        //
        System.assertEquals(1, services.size());
        System.assertEquals(subscriptions[0].Id, services[0].csord__Subscription__c);
        System.assertEquals(DateTime.now().date(), services[0].csord__Activation_Date__c);
        System.assertEquals(DateTime.now().date().addMonths(24), services[0].csord__Deactivation_Date__c);
        System.assertEquals(pcos[0].Id, services[0].Product_Configuration_Order__c);
        System.assertEquals('One Off', services[0].Billing_Rate_Model__c);
        //
        // Test service line items
        //
        System.assertEquals(1, services[0].csord__Service_Line_Items__r.size());
        System.assertEquals('Recurring Target Fee Total', services[0].csord__Service_Line_Items__r[0].Name);
        System.assertEquals(120, services[0].csord__Service_Line_Items__r[0].Total_Price__c);
        System.assertEquals(journalMapping.Id, services[0].csord__Service_Line_Items__r[0].Journal_Mapping__c);
        System.assertEquals(true, services[0].csord__Service_Line_Items__r[0].csord__Is_Recurring__c);
    }

    //--------------------------------------------------------------------------
    // Create subscriptions with connectivity only this shouldn't fail
    //--------------------------------------------------------------------------
    static testMethod void createSubscriptionsWithoutHardware()
    {
        //
        // Create test order
        //
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers;

        TestHelper.createOrder();
        TestHelper.createJournalMappings();

        //
        // Remove hardware order
        //
        Product_Order__c productOrder = [ SELECT Id FROM Product_Order__c WHERE Name = 'SmartMeter' ];
        cscfga__Product_Configuration__c pc =
        	[ SELECT Id
        	  FROM cscfga__Product_Configuration__c
        	  WHERE Name = 'SIM & Tariff' ];

        delete productOrder;
        cscfga__Attribute__c att = new cscfga__Attribute__c();
        att.Name = 'Total Expected Deta Over-Usage';
        att.cscfga__Product_Configuration__c = pc.Id;
        att.cscfga__Is_Line_Item__c = true;
        att.cscfga__Recurring__c = true;
        att.cscfga__Line_Item_Sequence__c = 50;
        att.cscfga__Line_Item_Description__c = 'Monthly Expected Data Over-Usage';
        att.cscfga__Price__c = 6000.00;
        att.cscfga__Value__c = '6000';

        insert att;

        notriggers.Flag__c = false;
        update notriggers;

        csord__Order__c order =
                [ SELECT Id, Order_Request__c, Order_Request__r.Name,
                  Order_Request__r.Billing_Account__c,
                  Order_Request__r.Frame_Contract__r.Customer__c
                  FROM csord__Order__c ];

        //
        // Create subscriptions
        //
        Test.startTest();
        CreateSubscriptions createSubscriptions = new CreateSubscriptions(order.Id, new InsertHelper());
        createSubscriptions.create();
    }
}