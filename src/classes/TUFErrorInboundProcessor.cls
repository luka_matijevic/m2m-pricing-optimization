global class TUFErrorInboundProcessor implements csam_t1.InboundMessageProcessor {
	public Boolean isMessageProcessible(csam_t1__Incoming_Message__c incomingMessage) {
		// change this condition later on with callout handler name
		csam_t1__ObjectGraph_Callout_Handler__c ogch = [SELECT Id, Name FROM csam_t1__ObjectGraph_Callout_Handler__c WHERE Id=:incomingMessage.csam_t1__Outgoing_Message__r.csam_t1__ObjectGraph_Callout_Handler__c];
		return incomingMessage.csam_t1__Incoming_URL_Path__c.endsWith('error') && ogch.Name.equalsIgnoreCase('Upload TUF');
	}

	public void processMessage(csam_t1__Incoming_Message__c incomingMessage, Map<Id, Id> oldToNewIdsMap) {
		incomingMessage.csam_t1__Status__c = 'Integration Error';
		incomingMessage.csam_t1__Processed_At__c = System.now();

		update incomingMessage;

		try{
			csam_t1__Outgoing_Message_Record__c outAttachRecord = [Select csam_t1__Object_Record_Id__c From csam_t1__Outgoing_Message_Record__c WHERE csam_t1__Outgoing_Message__c = :incomingMessage.csam_t1__Outgoing_Message__r.Id LIMIT 1];

			// query for TUF record
			Attachment att = [SELECT Id, ParentId FROM Attachment WHERE Id = :outAttachRecord.csam_t1__Object_Record_Id__c];

			SM2MErrorMessageParser errorParser = new SM2MErrorMessageParser(incomingMessage);

			String error = 'Integration Error';
	        if (errorParser.hasUnrecoverableException()) {
	            error = errorParser.getUnrecoverableException();
	        } else if (errorParser.hasSM2MError()) {
	            error = errorParser.getSM2MError();
	        }

			integer fieldLength = Schema.SObjectType.Third_Upload_File__c.fields.Upload_to_SM2M_Status__c.getLength();
			Third_Upload_File__c tuf = new Third_Upload_File__c(Id = att.ParentId, Upload_to_SM2M_Status__c = error.abbreviate(fieldLength));
			update tuf;
		} catch (Exception e){
			// to see this log, debug the M2M Site Guest User
			System.debug('Exception occured while extracting SM2M error message: ' + e);
		}
	}
}