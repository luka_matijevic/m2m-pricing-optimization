public class TariffDeepCloneController {

    Id tariffId;

    public TariffDeepCloneController(ApexPages.StandardController stdCtrl) {
        tariffId = stdCtrl.getId();
    }
    
    public PageReference doClone() {
        string tariffSoql = 'select ' + SObjectHelper.getFieldListForSOQL('Tariff__c', null, null) + 
                                ', (select ' + SObjectHelper.getFieldListForSOQL('Tariff_Option__c', null, null) + ' from Tariff_Options__r) ' +
                            ' from Tariff__c where Id = \'' + tariffId + '\'';
                            
        system.debug('>1>' + tariffSoql);
        
        Tariff__c tariff = database.query(tariffSoql);
        
        Tariff__c clTariff = tariff.clone();
        clTariff.Id = null;
        clTariff.Name += ' (New)';
        if (clTariff.Name.length() > 80) {
            clTariff.Name = clTariff.Name.substring(0, 80);
        }
        clTariff.Status__c = 'Draft';
        clTariff.OwnerId = Userinfo.getUserId();
        insert clTariff;
        
        Tariff_Option__c[] clTariffOptions = new Tariff_Option__c[]{};
        
        map<Id, Tariff_Option__c> oldIdToNewTariffOption = new map<Id, Tariff_Option__c>();
        for (Tariff_Option__c to : tariff.Tariff_Options__r) {
            Tariff_Option__c clTo = to.clone();
            clTo.Id = null;
            clTo.Tariff__c = clTariff.Id;
            clTariffOptions.add(clTo);
            oldIdToNewTariffOption.put(to.Id, clTo);
        }
        
        insert clTariffOptions;
        
        set<Id> tariffOptionsIdsSet = oldIdToNewTariffOption.keyset();
        if (tariffOptionsIdsSet.size() > 0) {
            id[] tariffOptionsIds = new List<Id>(tariffOptionsIdsSet);
            string tarifOptionsIdsString = '(\'' + string.join(tariffOptionsIds , '\',\'') + '\')';
            Rate_Card__c[] rateCards = database.query('select ' + SObjectHelper.getFieldListForSOQL('Rate_Card__c', null, null) +
                                                        ' from Rate_Card__c where Tariff_Option__c IN ' + tarifOptionsIdsString  );
            Service_Fee__c[] serviceFees = database.query('select ' + SObjectHelper.getFieldListForSOQL('Service_Fee__c', null, null) +
                                                        ' from Service_Fee__c where Tariff_Option__c IN ' + tarifOptionsIdsString  );
            
            SObject[] clTariffOptionChildren = new SObject[] {};
            
            for (Rate_Card__c rc : rateCards) {
                Rate_Card__c clRc = rc.clone();
                clRc.Id = null;
                clRc.Tariff_Option__c = oldIdToNewTariffOption.get(clRc.Tariff_Option__c).Id;
                clTariffOptionChildren.add(clRc);
            }
            
            for (Service_Fee__c sf : serviceFees ) {
                Service_Fee__c sfRc = sf.clone();
                sfRc.Id = null;
                sfRc.Tariff_Option__c = oldIdToNewTariffOption.get(sfRc.Tariff_Option__c).Id;
                clTariffOptionChildren.add(sfRc);
            }
            
            insert clTariffOptionChildren;
        }
        
        return (new ApexPages.StandardController(clTariff)).view();
    }
}