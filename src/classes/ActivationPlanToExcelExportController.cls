public class ActivationPlanToExcelExportController {

    final static String NWLN = '\r\n';
    final static String DBLNWLN = NWLN + NWLN;
    final static String IND = '  ';
    final static String IND2 = IND + IND;
    final static String IND3 = IND2 + IND;
    final static String IND4 = IND3 + IND;

    static String CSV_SEPARATOR = ';';

    final static String YEAR = 'Year';
    final static String MONTH_JANUARY = 'January';
    final static String MONTH_FEBRUARY = 'February';
    final static String MONTH_MARCH = 'March';
    final static String MONTH_APRIL = 'April';
    final static String MONTH_MAY = 'May';
    final static String MONTH_JUNE = 'June';
    final static String MONTH_JULY = 'July';
    final static String MONTH_AUGUST = 'August';
    final static String MONTH_SEPTEMBER = 'September';
    final static String MONTH_OCTOBER = 'October';
    final static String MONTH_NOVEMBER = 'November';
    final static String MONTH_DECEMBER = 'December';

    Id oppId;
    Id bundleId;

    //--------------------------------------------------------------------------
    // Constructor
    //--------------------------------------------------------------------------
    public ActivationPlanToExcelExportController(ApexPages.StandardController stdCtrl)
    {
        this.oppId = stdCtrl.getId();
        this.bundleId = ApexPages.currentpage().getparameters().get('bundleId');

        ExportBundleToExcelSettings__c settings = ExportBundleToExcelSettings__c.getInstance(UserInfo.getUserId());
        if (settings != null)
        {
            if (settings.CSV_Delimiter__c != null)
            {
                CSV_SEPARATOR = settings.CSV_Delimiter__c;
            }
        }
    }

    //--------------------------------------------------------------------------
    // Generates CSV data
    //--------------------------------------------------------------------------
    public String getExportData()
    {
        ExportData[] data = getExportDataInternal();
        string result = '';
        for (ExportData d : data)
        {
            result += toCsvLine(d.getColumns());
        }
        
        return result;
    }
        
    //--------------------------------------------------------------------------
    // Generates Export data
    //--------------------------------------------------------------------------
    private ExportData[] getExportDataInternal()
    {
        ExportData[] result = new ExportData[] {};
        
        result.add(new ExportData(rowArray(new String[]{ YEAR, MONTH_JANUARY, MONTH_FEBRUARY, MONTH_MARCH, MONTH_APRIL, MONTH_MAY, MONTH_JUNE, MONTH_JULY, MONTH_AUGUST, MONTH_SEPTEMBER, MONTH_OCTOBER, MONTH_NOVEMBER, MONTH_DECEMBER })));
        
        List<Activation_plan__c> actPlanList = [SELECT Id, Year__c, Opportunity__c, Connectivity_Sims_January__c, Connectivity_Sims_February__c, Connectivity_Sims_March__c, Connectivity_Sims_April__c, Connectivity_Sims_May__c, Connectivity_Sims_June__c, Connectivity_Sims_July__c,
                                Connectivity_Sims_August__c, Connectivity_Sims_September__c, Connectivity_Sims_October__c, Connectivity_Sims_November__c, Connectivity_Sims_December__c FROM Activation_plan__c WHERE Product_bundle__c = :bundleId];
                                
        if(actPlanList != null && actPlanList.size() > 0) {
            for(Activation_plan__c ap : actPlanList) {
                result.add(new ExportData(decimalValuesToString(ap.Year__c, ap.Connectivity_Sims_January__c, ap.Connectivity_Sims_February__c, ap.Connectivity_Sims_March__c, ap.Connectivity_Sims_April__c, ap.Connectivity_Sims_May__c,
                                ap.Connectivity_Sims_June__c, ap.Connectivity_Sims_July__c, ap.Connectivity_Sims_August__c, ap.Connectivity_Sims_September__c, ap.Connectivity_Sims_October__c, ap.Connectivity_Sims_November__c, ap.Connectivity_Sims_December__c)));
            }
        }

        return result;
    }

    //--------------------------------------------------------------------------
    // Utils
    //--------------------------------------------------------------------------
    
    private static String toCsvLine(String col1, String col2, String col3, String col4, String col5, String col6, String col7, String col8, String col9, String col10, String col11, String col12, String col13)
    {
        return toCsvLine(new String[] { col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13 });
    }

    //--------------------------------------------------------------------------
    // Creates CSV line from values in the list
    //--------------------------------------------------------------------------
    private static String toCsvLine(String[] columns)
    {
        if (columns != null && columns.size() > 0)
        {
            String res = '';
            boolean first = true;
            for (String col : columns)
            {
                if (first)
                {
                    res += toCsvValue(col);
                    first = false;
                }
                else
                {
                    res += CSV_SEPARATOR + toCsvValue(col);
                }
            }
            return res + NWLN;
        }
        else
        {
            return NWLN;
        }
    }

    //--------------------------------------------------------------------------
    // Replaces " with "". If text is null return blank
    //--------------------------------------------------------------------------
    private static String toCsvValue(String text)
    {
        if (text != null && text != '')
        {
            return '"' + text.replaceAll('"', '""') + '"';
        }
        else
        {
            return '';
        }
    }

    //--------------------------------------------------------------------------
    // Convert decimal to string. If null return blank
    //--------------------------------------------------------------------------
    private static String decimalToString(Decimal numb)
    {
        if (numb == null)
        {
            return '';
        }
        else
        {
            return numb.format();
        }
    }

    private static String[] decimalValuesToString(String yr, Decimal jan, Decimal feb, Decimal mar, Decimal apr, Decimal may, Decimal jun, Decimal jul, Decimal aug, Decimal sep, Decimal oct, Decimal nov, Decimal dec)
    {
        //String yearString = decimalToString(yr);
        String janString = decimalToString(jan);
        String febString = decimalToString(feb);
        String marString = decimalToString(mar);
        String aprString = decimalToString(apr);
        String mayString = decimalToString(may);
        String junString = decimalToString(jun);
        String julString = decimalToString(jul);
        String augString = decimalToString(aug);
        String sepString = decimalToString(sep);
        String octString = decimalToString(oct);
        String novString = decimalToString(nov);
        String decString = decimalToString(dec);
        
        return new String[] { yr, janString, febString, marString, aprString, mayString, junString, julString, augString, sepString, octString, novString, decString };
    }

    //--------------------------------------------------------------------------
    // Creates array of strings for CSV row
    //--------------------------------------------------------------------------
    private static String[] rowArray(String[] values)
    {
        String[] returnValue = new List<String>();
        returnValue.addAll(values);
        return returnValue;
    }

    class ExportData {
        public string Col1 { get; set; }
        public string Col2 { get; set; }
        public string Col3 { get; set; }
        public string Col4 { get; set; }
        public string Col5 { get; set; }
        public string Col6 { get; set; }
        public string Col7 { get; set; }
        public string Col8 { get; set; }
        public string Col9 { get; set; }
        public string Col10 { get; set; }
        public string Col11 { get; set; }
        public string Col12 { get; set; }
        public string Col13 { get; set; }
        
        public ExportData() {}
        
        public ExportData(string[] cols) {
            this.Col1 = cols[0];
            this.Col2 = cols[1];
            this.Col3 = cols[2];
            this.Col4 = cols[3];
            this.Col5 = cols[4];
            this.Col6 = cols[5];
            this.Col7 = cols[6];
            this.Col8 = cols[7];
            this.Col9 = cols[8];
            this.Col10 = cols[9];
            this.Col11 = cols[10];
            this.Col12 = cols[11];
            this.Col13 = cols[12];
        }
        
        public string[] getColumns() {
            string[] result = new string[] {};
            
            if (this.Col1 != null) {
                result.add(this.Col1);
            }
            
            if (this.Col2 != null) {
                result.add(this.Col2);
            }
            
            if (this.Col3 != null) {
                result.add(this.Col3);
            }
            
            if (this.Col4 != null) {
                result.add(this.Col4);
            }
            
            if (this.Col5 != null) {
                result.add(this.Col5);
            }
            
            if (this.Col6 != null) {
                result.add(this.Col6);
            }
            
            if (this.Col7 != null) {
                result.add(this.Col7);
            }
            
            if (this.Col8 != null) {
                result.add(this.Col8);
            }
            
            if (this.Col9 != null) {
                result.add(this.Col9);
            }
            
            if (this.Col10 != null) {
                result.add(this.Col10);
            }
            
            if (this.Col11 != null) {
                result.add(this.Col11);
            }
            
            if (this.Col12 != null) {
                result.add(this.Col12);
            }
            
            if (this.Col13 != null) {
                result.add(this.Col13);
            }
            
            return result;
        }
    }
}