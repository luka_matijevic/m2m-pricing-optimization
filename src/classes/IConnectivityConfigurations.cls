//------------------------------------------------------------------------------
// Interface for handling with connectivity product configurations
//------------------------------------------------------------------------------
public interface IConnectivityConfigurations
{
	//--------------------------------------------------------------------------
	// Set quantities from configuration to tariff options on the tariff
	//--------------------------------------------------------------------------
	void setQuantities(ITariff tariff);

	//--------------------------------------------------------------------------
	// Delete product current product configurations
	//--------------------------------------------------------------------------
	void deleteProductConfigurations();

	//--------------------------------------------------------------------------
	// Create product configurations from tariff
	//--------------------------------------------------------------------------
	String createProductConfigurations(ITariff tariff);
}