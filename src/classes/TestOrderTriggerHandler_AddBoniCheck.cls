@isTest
private class TestOrderTriggerHandler_AddBoniCheck
{

    static testMethod void testBoniCheckCreation()
    {
    	//
    	// Create test order request
    	//
		No_Trigger__c notriggers = new No_Trigger__c();
		notriggers.Flag__c = true;
		insert notriggers;

		TestHelper.createOrderRequest();
		TestHelper.createJournalMappings();

		notriggers.Flag__c = false;
		update notriggers;

		Boni_Check__c[] boniChecks =
			[ SELECT Id FROM Boni_Check__c ];
		delete boniChecks;

		Test.startTest();
		//
		// Simulate the creation of the boni check
		//
		Order_Request__c orderRequest =
				[ SELECT Id, Name, Frame_Contract__r.Opportunity__r.AccountId, Status__c, Boni_Check__c
				  FROM Order_Request__c
				  LIMIT 1 ];

		orderRequest.Status__c = 'Sent to Boni';
		update orderRequest;
		Test.stopTest();

		//
		// Check boni check
		//
		boniChecks =
			[ SELECT Id, Number_Requested_SMS__c, Requested_Units_HW__c,
			  Number_Requested_VAS__c, Fix_Revenue_SMS__c, Fix_Revenue_HW__c,
			  Fix_Revenue_VAS__c, Variable_Revenue_SMS__c, Variable_Revenue_HW__c,
			  Variable_Revenue_VAS__c
			  FROM Boni_Check__c ];

		orderRequest =
			[ SELECT Id, Boni_Check__c
			  FROM Order_Request__c
			  LIMIT 1 ];

		System.assertEquals(1, boniChecks.size());
		System.assertEquals(boniChecks[0].Id, orderRequest.Boni_Check__c);
		//
		// Check for calculations
		//
		System.assertEquals(100, boniChecks[0].Number_Requested_SMS__c);
		System.assertEquals(50, boniChecks[0].Requested_Units_HW__c);
		System.assertEquals(0, boniChecks[0].Number_Requested_VAS__c);
		System.assertEquals(33.5, boniChecks[0].Fix_Revenue_SMS__c);
		System.assertEquals(890, boniChecks[0].Fix_Revenue_HW__c);
		System.assertEquals(0, boniChecks[0].Fix_Revenue_VAS__c);
		System.assertEquals(12.1268, boniChecks[0].Variable_Revenue_SMS__c);
		System.assertEquals(10, boniChecks[0].Variable_Revenue_HW__c);
		System.assertEquals(0, boniChecks[0].Variable_Revenue_VAS__c);
    }
}