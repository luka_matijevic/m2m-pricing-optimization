/**
 * Covers functionality that is used by multiple inbound processors.
 */ 
public class InboundProcessorHelper {
	
	private InboundProcessorHelper() {
	}

	/**
	 * @description Returns the first attachment related to the message with the given ID.
	 */ 
	public static Attachment getMessageAttachment(Id messageId) {
		return [
			SELECT Id, Name, Body
			FROM Attachment
			WHERE ParentId = :messageId
			AND	Name like 'Message%'
			LIMIT 1
		];
	}

	public static Object getDeserializedMessageAttachment(Id messageId, System.Type clazz) {
		Attachment msgAttach = InboundProcessorHelper.getMessageAttachment(messageId);
		return JSON.deserialize(msgAttach.Body.toString(), clazz);
	}
	
	public static CallbackMessage getCallbackMessage(Id messageId) {
		return (CallbackMessage) getDeserializedMessageAttachment(messageId, CallbackMessage.class);
	}

	/**
	 * Returns a list of Outgoing message records (records that were picked up by the object graph) for outgoing message related
	 * to the current incoming message
	 *
	 * Salesforce -(picked up objects)-> Heroku
	 *						^
	 *    described by outgoing message records
	 */ 
	public static List<csam_t1__Outgoing_Message_Record__c> outgoingMessageRecords(csam_t1__Incoming_Message__c incomingMessage) {
		return [
			SELECT csam_t1__Object_Name__c, csam_t1__Object_Record_Id__c
			FROM csam_t1__Outgoing_Message_Record__c
			WHERE csam_t1__Outgoing_Message__c = :incomingMessage.csam_t1__Outgoing_Message__c
		];
	}

	public static csam_t1__Outgoing_Message__c outgoingMessage(csam_t1__Incoming_Message__c incomingMessage) {
		return 
			[SELECT 
				csam_t1__Bulk_Outgoing_Message__c, 
				csam_t1__Callback_Host__c, 
				csam_t1__Content_Type__c,
				csam_t1__Created_At__c,
				csam_t1__Delivery_Tag__c,
				csam_t1__HTTP_Method__c,
				csam_t1__HTTP_Response_Status__c,
				csam_t1__HTTP_Response_Status_Code__c,
				csam_t1__Is_Bulked__c,
				csam_t1__ObjectGraph_Callout_Handler__c,
				csam_t1__Sent_At__c,
				csam_t1__Status__c,
				csam_t1__URL_File__c,
				csam_t1__URL_Host__c
			FROM
				csam_t1__Outgoing_Message__c
			WHERE
				Id = :incomingMessage.csam_t1__Outgoing_Message__c
			LIMIT 1
		];
	}

	/**
	 * Updates status fields
	 */ 
	public static void updateStatuses(csam_t1__Incoming_Message__c incomingMessage, String status) {
		
		String statusField = incomingMessage.csam_t1__Outgoing_Message__r.csam_t1__ObjectGraph_Callout_Handler__r.csam_t1__Status_Field__c;
					
		if (!String.isEmpty(statusField)) {
			
			List<csam_t1__Outgoing_Message_Record__c> outgoingMessageRecords = InboundProcessorHelper.outgoingMessageRecords(incomingMessage);
							
			List<Id> outgoingRecordIds = new List<Id>();
			
			for (csam_t1__Outgoing_Message_Record__c outgoingMessageRecord : outgoingMessageRecords) {
				outgoingRecordIds.add(outgoingMessageRecord.csam_t1__Object_Record_Id__c);
			}

			String queryString = 'SELECT Id, ' + statusField
				+ ' FROM ' + incomingMessage.csam_t1__Outgoing_Message__r.csam_t1__ObjectGraph_Callout_Handler__r.csam_t1__Startpoint_Type_Name__c
				+ ' WHERE Id in (\''
				+ String.join(outgoingRecordIds, '\',\'') + '\')';
			
			List<sObject> statusUpdateList = Database.query(queryString);
			
			// TODO: check the actual lenght of the field and abbreviate to that length
			for (sObject o : statusUpdateList) {
				o.put(statusField, status.abbreviate(255));
			}
			update statusUpdateList;
		}
	}

	public static void insertErrorAttachment(csam_t1__Incoming_Message__c incomingMessage, Exception e) {

		// TODO: check the actual lenght of the field and abbreviate to that length
		Attachment errorAttach = new Attachment(
			Body = Blob.valueOf(e.getStackTraceString()),
			parentId = incomingMessage.Id,
			Name = ('Error_' + e.getMessage()).abbreviate(255)
		);
				
		insert errorAttach;
	}


}