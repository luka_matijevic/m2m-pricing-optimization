public class JournalMappingHelper {
	public static Prebill_Line_Item__c addInformation(Prebill_Line_Item__c pbli, Journal_Mapping__c journalMapping) {
		if (pbli == null) {
			system.debug(LoggingLevel.ERROR, 'Prebill line item is null when mapping from journal mapping.');	
		} else if (journalMapping == null) {
			system.debug(LoggingLevel.ERROR, 'Journal mapping information for prebill is missing. Prebill: ' + JSON.serializePretty(pbli));
		} else {
			try {
				pbli.Konto__c = journalMapping.Konto__c;
				pbli.KoSt__c = journalMapping.KoSt__c;
				pbli.Prod__c = journalMapping.Prod__c;
				pbli.Projekt__c = journalMapping.Projekt__c;
				pbli.Tax_Code__c = journalMapping.Revenue__c;
				pbli.I_CO__c = journalMapping.I_CO__c;
				pbli.Unique_Identifier__c = journalMapping.Unique_Identifier__c;
			} catch (System.SObjectException e) {
				system.debug(LoggingLevel.ERROR, 'Some fields have not been mapped. ' + e.getMessage() + ' Prebill: ' + JSON.serializePretty(pbli));
				// swallowed intentionally
			}
		}
		return pbli;
	}
}