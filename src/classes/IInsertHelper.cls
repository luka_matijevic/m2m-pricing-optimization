//------------------------------------------------------------------------------
// Interface for inserting objects in bulk
//------------------------------------------------------------------------------
public interface IInsertHelper 
{
	//--------------------------------------------------------------------------
	// Add single object
	//--------------------------------------------------------------------------
	void add(SObject obj);

	//--------------------------------------------------------------------------
	// Add related objects
	// child       Child SObject
	// fieldName   Field name on child object for lookup to parent object
	// parent      Parent SObject
	//--------------------------------------------------------------------------
	void add(SObject child, String fieldName, SObject parent);

	//--------------------------------------------------------------------------
	// Save all objects to the database with relationships
	//--------------------------------------------------------------------------
	void store();
	void storeUpdate();

	//--------------------------------------------------------------------------
	// Used for clonning
	//--------------------------------------------------------------------------
	IInsertHelper deepClone();
}