public class BICReportSettingsHelper {
	
	public static Integer getBicReportPollerDelaySeconds() {
		return (Integer) BIC_Report_Settings__c.getOrgDefaults().BIC_Report_Poller_Delay_Seconds__c;
	}
	
	public static Boolean isDisableBICReportPollerReschedule() {
		return BIC_Report_Settings__c.getOrgDefaults().Disable_BIC_Report_Poller_Reschedule__c == true;
	}
}