@IsTest
public class Pricing_Report_Test {

    @testSetup
    static void perpareData(){
        
        Profile pfl = [select id from Profile where name='System Administrator' limit 1];
        
        User usr = Test_Util.createUser(pfl.id,'test@testasas.com','Name','Name');
        insert usr;
        
        Account acc = Test_Util.CreateAccount('Account Test');
        acc.Global__c = true;
        insert acc;
        
        Contact con = Test_Util.createContact(acc.id);
        insert con;
        
        Opportunity opp = Test_Util.createOpportynity('Opportunity Test',acc.id,system.today(),'New','Price',10,10);
        opp.Description = 'Test description 20 characters';
        insert opp;
        
        cscfga__Product_Bundle__c prodBundle = Test_Util.createProdBundle(opp.id);
        prodBundle.Billing_Approval_Status__c = 'Required';
        insert prodBundle;
        
        system.runAs(usr){
            list<FeedItem> fd = new list<FeedItem>();
            fd.add(new FeedItem(body = 'body',parentId = prodBundle.id));
            fd.add(new FeedItem(body = 'body',parentId = prodBundle.id));
            insert fd;
            
            list<feedComment> fcmnt = new list<feedComment>();
            fcmnt.add(new feedComment(CommentBody='CommentBody',FeedItemId = fd[0].id));
            fcmnt.add(new feedComment(CommentBody='CommentBody',FeedItemId = fd[0].id));
            insert fcmnt;
        }
        
         
        list<Business_comment__c> bcmnt = new list<Business_comment__c>();
        bcmnt.add(Test_Util.createBusinessComments(prodBundle.id, 'Comment'));
        bcmnt.add(Test_Util.createBusinessComments(prodBundle.id, 'Comment'));
        insert bcmnt;
        
        Frame_Contract__c fc = Test_Util.createFC(prodBundle.id);
        fc.Customer__c = acc.id;
        insert fc;
        
        Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
        approvalRequest.setComments('Submitting request for approval.');
        approvalRequest.setObjectId(prodBundle.id);
        approvalRequest.setSkipEntryCriteria(false);
        Approval.ProcessResult apprRequestResult = Approval.process(approvalRequest);
    }
    
    static testMethod void runReportTest(){
        Pricing_Report pr = new Pricing_Report();
        
        ApexPages.StandardSetController con = pr.con;
        
        pr.runReport();
        pr.startDate.Synchronization_Date__c = System.today() -200;
        pr.Todate.Synchronization_Date__c = System.today();
        pr.runReport();
        test.startTest();
        list<Pricing_Report.Wrapper> wrp = pr.recordsLst;
        Boolean b = pr.HasNext;
        b = pr.HasPrevious;
        pr.next();
        pr.Previous();
        pr.last();
        pr.first();
        
        test.stopTest();
    }
    
    static testMethod void export(){
        Pricing_Report pr = new Pricing_Report();
        ApexPages.StandardSetController con = pr.con;
        pr.runReport();
        pr.startDate.Synchronization_Date__c = System.today() -10;
        pr.Todate.Synchronization_Date__c = System.today();
        test.startTest();
        list<selectOption> recordsSize = pr.recordsSize;
        recordsSize = pr.recordsSize;
        list<account> acc = [select id from account limit 1];
        pr.opp.accountId = acc[0].id;
        string queryString = pr.queryString();
        pr.export();
        pr.cancel();
        Integer pageNumber = pr.pageNumber;
        test.stopTest();
    }

}