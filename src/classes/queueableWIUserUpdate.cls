public class queueableWIUserUpdate implements Queueable {
    String billingAccountID;
    public queueableWIUserUpdate(String billingAccountID){
        this.billingAccountID = billingAccountID;
    }
    
    public void execute(QueueableContext context){
        //csam_t1.ObjectGraphCalloutHandler.createAndSend('Update Billing Account', billingAccountID) ;
        csam_t1.ObjectGraphCalloutHandler.createAndSend('Send ETL User', billingAccountID) ;
        }

    
}