/**
 * The object model for items that are processed looks like this:
 * csord__Service_Line_Item__c --> csord__Order_Request__c (required by managed package, but not used)
 *                             --> Journal_Mapping__c (used for mapping billing information)
 *                             -->
 */

@isTest
public class BillingProcessorTest {

	static testMethod void testNoQuantityItems() {

		Date testDate = Date.newInstance(2014,2,10);

		Account acc = new Account(
			Name = 'Test account',
			BillingCity = 'Bremen',
			BillingCountry = 'Germany',
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße',
			Type = 'Customer',
			Billing_Cycle__c = String.valueOf(testDate.day()),
			VAT__c = 1,
            Global__c = true
		);
		insert acc;

		Billing_Account__c billingAccount = new Billing_Account__c(
			Payer__c = acc.Id,
			VAT__c = '1'
		);
		insert billingAccount;

		csord__Order_Request__c orderRequest = new csord__Order_Request__c(
			csord__Module_Name__c = 'test',
			csord__Module_Version__c = 'test'
           
		);
		insert orderRequest;

		csord__Subscription__c subscription = new csord__Subscription__c(
			Billing_Account__c = billingAccount.Id,
			csord__Identification__c = 'Id',
			csord__Order_Request__c = orderRequest.Id,
			csord__Status__c = 'Test'
		);
		insert subscription;

		csord__Service__c
		service = new csord__Service__c(
			csord__Subscription__c = subscription.Id,
			Billing_Rate_Model__c = 'One Off',
			csord__Activation_Date__c = System.today().addMonths(-3).addDays(-3),
			csord__Deactivation_Date__c = System.today().addMonths(9),
			csord__Identification__c = 'Test',
			csord__Order_Request__c = orderRequest.Id,
			csord__Status__c = 'Test'
		);
		insert service;

		Journal_Mapping__c jm = new Journal_Mapping__c();
		insert jm;

		csord__Service_Line_Item__c sli = new csord__Service_Line_Item__c(
			csord__Service__c = service.Id,
			csord__Identification__c = 'Test',
			csord__Order_Request__c = orderRequest.Id,
			Journal_Mapping__c = jm.Id
		);
		insert sli;

		csord__Order__c ord = new csord__Order__c(
			csord__Identification__c = 'Test',
			csord__Order_Request__c = orderRequest.Id,
			csord__Status2__c = 'Test'
		);
		insert ord;

		csord__Order_Line_Item__c oli = new csord__Order_Line_Item__c(
			csord__Order__c = ord.Id,
			csord__Discount_Type__c = 'Amount',
			csord__Identification__c = 'Test',
			csord__Order_Request__c = orderRequest.Id,
			Journal_Mapping__c = jm.Id,
			// required for prebill creation
			csord__Total_Price__c = 1000.0,
			Name = 'test'
		);

		insert oli;

		Daily_Bill__c dailyBill = new Daily_Bill__c(
			Date__c = testDate
		);

		insert dailyBill;

		BillingProcessor processor = new BillingProcessor(testDate);

		// there should only be one billing account for testDate
		system.assertEquals(1, processor.getBillingAccounts().size());
		system.assertEquals(0, processor.getNewPrebillLineItemsCount());
		system.assertEquals(0, processor.getNewPrebillsCount());
		system.assertEquals(0, processor.getProcessedCount());
		system.assertEquals(0, processor.getSuccessfulCount());

		processor.queue(oli);
		processor.processQueued();
		system.assertEquals(0, processor.getNewPrebillLineItemsCount());
		system.assertEquals(0, processor.getNewPrebillsCount());
		system.assertEquals(1, processor.getProcessedCount());
		system.assertEquals(1, processor.getSuccessfulCount());

		processor.queue(sli);
		processor.processQueued();
		system.assertEquals(0, processor.getNewPrebillLineItemsCount());
		system.assertEquals(0, processor.getNewPrebillsCount());
		system.assertEquals(2, processor.getProcessedCount());
		system.assertEquals(2, processor.getSuccessfulCount());
	}

	static testMethod void testQuantityItems() {
		Date testDate = Date.newInstance(2014,2,10);

		Account acc = new Account(
			Name = 'Test account',
			BillingCity = 'Bremen',
			BillingCountry = 'Germany',
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße',
			Type = 'Customer',
			Billing_Cycle__c = String.valueOf(testDate.day()),
			VAT__c = 1,
            Global__c = true
		);
		insert acc;

		Billing_Account__c billingAccount = new Billing_Account__c(
			Payer__c = acc.Id,
			VAT__c = '1'
		);
		insert billingAccount;

		csord__Order_Request__c csordRequest = new csord__Order_Request__c(
			csord__Module_Name__c = 'test',
			csord__Module_Version__c = 'test'
		);
		insert csordRequest;

		csord__Subscription__c subscription = new csord__Subscription__c(
			Billing_Account__c = billingAccount.Id,
			csord__Identification__c = 'Id',
			csord__Order_Request__c = csordRequest.Id,
			csord__Status__c = 'Test'
		);
		insert subscription;

		Product_Order__c productOrder = new Product_Order__c(
			Quantity__c = 1
		);
		insert productOrder;

		cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c(
		);
		insert productConfiguration;

		Product_Configuration_Order__c productConfigurationOrder = new Product_Configuration_Order__c(
			Product_Order__c = productOrder.Id,
			Product_Configuration__c = productConfiguration.Id
		);
		insert productConfigurationOrder;

		List<csord__Service__c> services = new List<csord__Service__c>();
		csord__Service__c service = new csord__Service__c(
			csord__Subscription__c = subscription.Id,
			Billing_Rate_Model__c = BillingRateModels.toString(BillingRateModel.ONEOFF),
			csord__Activation_Date__c = System.today().addMonths(-3).addDays(-3),
			csord__Deactivation_Date__c = System.today().addMonths(9),
			csord__Identification__c = 'Test',
			csord__Order_Request__c = csordRequest.Id,
			csord__Status__c = 'Test',
			Product_Configuration_Order__c = productConfigurationOrder.Id
		);
		services.add(service);
		service = new csord__Service__c(
			csord__Subscription__c = subscription.Id,
			Billing_Rate_Model__c = BillingRateModels.toString(BillingRateModel.ONEOFF),
			csord__Activation_Date__c = System.today().addMonths(-1).addDays(20),
			csord__Deactivation_Date__c = System.today().addMonths(12),
			csord__Identification__c = 'Test',
			csord__Order_Request__c = csordRequest.Id,
			csord__Status__c = 'Test',
			Product_Configuration_Order__c = productConfigurationOrder.Id
		);
		services.add(service);
		insert services;

		Journal_Mapping__c jm = new Journal_Mapping__c();
		insert jm;

		List<csord__Service_Line_Item__c> slis = new List<csord__Service_Line_Item__c>();
		csord__Service_Line_Item__c sli = new csord__Service_Line_Item__c(
			csord__Service__c = services.get(0).Id,
			csord__Identification__c = 'Test',
			csord__Order_Request__c = csordRequest.Id,
			Journal_Mapping__c = jm.Id,
			// required for prebill creation
			Name = 'test',
			Total_Price__c = 500.0
		);
		slis.add(sli);
		sli = new csord__Service_Line_Item__c(
			csord__Service__c = services.get(1).Id,
			csord__Identification__c = 'Test',
			csord__Order_Request__c = csordRequest.Id,
			Journal_Mapping__c = jm.Id,
			// required for prebill creation
			Name = 'test',
			Total_Price__c = 500.0
		);
		slis.add(sli);

		insert slis;

		slis = [
			SELECT
				Id, Name, csord__Service__r.csord__Subscription__r.Billing_Account__c, csord__Is_Recurring__c, csord__Service__r.csord__Activation_Date__c,
				csord__Service__r.csord__Deactivation_Date__c, Journal_Mapping__c, Quantity__c, Total_Price__c, csord__Service__r.Billing_Rate_Model__c
			FROM
				csord__Service_Line_Item__c
			WHERE
				Is_Billed__c = false
		];

		// csord__Service__r.Product_Configuration_Order__r.Product_Order__r.Quantity__c


		Order_Request__c orderRequest = new Order_Request__c(
			Billing_Account__c = billingAccount.Id
            ,Shipping_City__c='Test City', Shipping_Country__c = 'Test Country', Shipping_Postal_Code__c='10000', Shipping_Street__c='Test shipping street');

		insert orderRequest;

		csord__Order__c ord = new csord__Order__c(
			csord__Identification__c = 'Test',
			csord__Order_Request__c = csordRequest.Id,
			csord__Status2__c = 'Test',
			Order_Request__c = orderRequest.Id
		);
		insert ord;


		csord__Order_Line_Item__c oli = new csord__Order_Line_Item__c(
			csord__Order__c = ord.Id,
			csord__Discount_Type__c = 'Amount',
			csord__Identification__c = 'Test',
			csord__Order_Request__c = csordRequest.Id,
			Journal_Mapping__c = jm.Id,
			Quantity__c = 1,
			// required for prebill creation
			csord__Total_Price__c = 1000.0,
			Name = 'test',
			Billing_Rate_Model__c = BillingRateModels.toString(BillingRateModel.ONEOFF)
		);

		insert oli;

		oli = [
			SELECT
				Id, Name, csord__Order__r.Order_Request__r.Billing_Account__c, csord__Total_Price__c,
                Quantity__c, Journal_Mapping__c, Billing_Rate_Model__c, billing_Account_id__c,
                csord__Order__r.Order_Request__r.Name, csord__Order__r.Order_Request__r.Id,
                csord__Order__r.Order_Request__r.RecordTypeId
			FROM
				csord__Order_Line_Item__c
			limit 1
		];

		Daily_Bill__c dailyBill = new Daily_Bill__c(
			Date__c = testDate
		);

		insert dailyBill;

		BillingProcessor processor = new BillingProcessor(testDate);
		System.debug(oli);
		// there should only be one billing account for testDate
		system.assertEquals(1, processor.getBillingAccounts().size());
		system.assertEquals(0, processor.getNewPrebillLineItemsCount());
		system.assertEquals(0, processor.getNewPrebillsCount());
		system.assertEquals(0, processor.getProcessedCount());
		system.assertEquals(0, processor.getSuccessfulCount());

		processor.queue(oli);
		processor.processQueued();
		system.assertEquals(1, processor.getNewPrebillLineItemsCount());
		system.assertEquals(1, processor.getNewPrebillsCount());
		system.assertEquals(1, processor.getProcessedCount());
		system.assertEquals(1, processor.getSuccessfulCount());

		processor.queue(slis.get(0));
		processor.processQueued();
		system.assertEquals(1, processor.getNewPrebillLineItemsCount());
		system.assertEquals(1, processor.getNewPrebillsCount());
		system.assertEquals(2, processor.getProcessedCount());
		system.assertEquals(1, processor.getSuccessfulCount());

		processor.queue(slis.get(1));
		processor.processQueued();
		system.assertEquals(1, processor.getNewPrebillLineItemsCount());
		system.assertEquals(1, processor.getNewPrebillsCount());
		system.assertEquals(3, processor.getProcessedCount());
		system.assertEquals(1, processor.getSuccessfulCount());

	}

	static testMethod void testDifferentBillingRateModels() {
		Date testDate = Date.newInstance(2014,2,10);

		Account acc = new Account(
			Name = 'Test account',
			BillingCity = 'Bremen',
			BillingCountry = 'Germany',
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße',
			Type = 'Customer',
			Billing_Cycle__c = String.valueOf(testDate.day()),
			VAT__c = 1,
            Global__c = true
		);
		insert acc;

		Billing_Account__c billingAccount = new Billing_Account__c(
			Payer__c = acc.Id,
			VAT__c = '1'
		);
		insert billingAccount;

		csord__Order_Request__c csordRequest = new csord__Order_Request__c(
			csord__Module_Name__c = 'test',
			csord__Module_Version__c = 'test'
		);
		insert csordRequest;

		csord__Subscription__c subscription = new csord__Subscription__c(
			Billing_Account__c = billingAccount.Id,
			csord__Identification__c = 'Id',
			csord__Order_Request__c = csordRequest.Id,
			csord__Status__c = 'Test'
		);
		insert subscription;

		Product_Order__c productOrder = new Product_Order__c(
			Quantity__c = 1
		);
		insert productOrder;

		cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c(
		);
		insert productConfiguration;

		Product_Configuration_Order__c productConfigurationOrder = new Product_Configuration_Order__c(
			Product_Order__c = productOrder.Id,
			Product_Configuration__c = productConfiguration.Id
		);
		insert productConfigurationOrder;

		csord__Service__c service = new csord__Service__c(
			csord__Subscription__c = subscription.Id,
			Billing_Rate_Model__c = BillingRateModels.toString(BillingRateModel.ONEOFF),
			csord__Activation_Date__c = System.today().addMonths(-3).addDays(-3),
			csord__Deactivation_Date__c = System.today().addMonths(9),
			csord__Identification__c = 'Test',
			csord__Order_Request__c = csordRequest.Id,
			csord__Status__c = 'Test',
			Product_Configuration_Order__c = productConfigurationOrder.Id
		);
		insert service;

		Journal_Mapping__c jm = new Journal_Mapping__c();
		insert jm;

		csord__Service_Line_Item__c sli = new csord__Service_Line_Item__c(
			csord__Service__c = service.Id,
			csord__Identification__c = 'Test',
			csord__Order_Request__c = csordRequest.Id,
			Journal_Mapping__c = jm.Id,
			// required for prebill creation
			Name = 'test',
			Total_Price__c = 500.0
		);
		insert sli;

		sli = [
			SELECT
				Id, Name,
                csord__Service__r.csord__Subscription__r.Billing_Account__c,
                csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__r.Name,
                csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__r.RecordTypeId,
                csord__Service__r.csord__Activation_Date__c,
                csord__Service__r.csord__Deactivation_Date__c,
                csord__Service__r.Billing_Rate_Model__c,
                csord__Is_Recurring__c, Journal_Mapping__c, Quantity__c, Total_Price__c,
                csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__r.Id
			FROM
				csord__Service_Line_Item__c
			WHERE
				Is_Billed__c = false
			limit 1
		];

		// csord__Service__r.Product_Configuration_Order__r.Product_Order__r.Quantity__c


		Order_Request__c orderRequest = new Order_Request__c(
			Billing_Account__c = billingAccount.Id
            ,  Shipping_City__c='Test City', Shipping_Country__c = 'Test Country', Shipping_Postal_Code__c='10000', Shipping_Street__c='Test shipping street');

		insert orderRequest;

		csord__Order__c ord = new csord__Order__c(
			csord__Identification__c = 'Test',
			csord__Order_Request__c = csordRequest.Id,
			csord__Status2__c = 'Test',
			Order_Request__c = orderRequest.Id
		);
		insert ord;


		csord__Order_Line_Item__c oli = new csord__Order_Line_Item__c(
			csord__Order__c = ord.Id,
			csord__Discount_Type__c = 'Amount',
			csord__Identification__c = 'Test',
			csord__Order_Request__c = csordRequest.Id,
			Journal_Mapping__c = jm.Id,
			Quantity__c = 1,
			// required for prebill creation
			csord__Total_Price__c = 1000.0,
			Name = 'test',
			Billing_Rate_Model__c = BillingRateModels.toString(BillingRateModel.INSTALLMENTS)
		);

		insert oli;

		oli = [
			SELECT
				Id, Name, csord__Order__r.Order_Request__r.Billing_Account__c, csord__Total_Price__c,
                Quantity__c, Journal_Mapping__c, Billing_Rate_Model__c, billing_Account_id__c,
                csord__Order__r.Order_Request__r.Name, csord__Order__r.Order_Request__r.Id,
                csord__Order__r.Order_Request__r.RecordTypeId
			FROM
				csord__Order_Line_Item__c
			limit 1
		];

		Daily_Bill__c dailyBill = new Daily_Bill__c(
			Date__c = testDate
		);

		insert dailyBill;

		BillingProcessor processor = new BillingProcessor(testDate);

		// there should only be one billing account for testDate
		system.assertEquals(1, processor.getBillingAccounts().size());
		system.assertEquals(0, processor.getNewPrebillLineItemsCount());
		system.assertEquals(0, processor.getNewPrebillsCount());
		system.assertEquals(0, processor.getProcessedCount());
		system.assertEquals(0, processor.getSuccessfulCount());

		processor.queue(oli);
		processor.processQueued();
		system.assertEquals(1, processor.getNewPrebillLineItemsCount());
		system.assertEquals(1, processor.getNewPrebillsCount());
		system.assertEquals(1, processor.getProcessedCount());
		system.assertEquals(1, processor.getSuccessfulCount());

		/* processor.queue(sli);
		processor.processQueued();
		system.assertEquals(2, processor.getNewPrebillLineItemsCount());
		system.assertEquals(2, processor.getNewPrebillsCount());
		system.assertEquals(2, processor.getProcessedCount());
		system.assertEquals(2, processor.getSuccessfulCount()); */

	}
}