public with sharing class IoTConnectAdvancedPricingController {
	/***
	PROPERTIES
	***/
	String bundleIdQueryString = ApexPages.currentPage().getParameters().get('idBundle');
	String bundleId;
	public List<SimType> simTypeList {get;set;}
	public List<cscfga.Attribute> screenAttributes {get;set;}
	Cscfga__product_bundle__c productBundle;
	ApexPages.StandardController standardController;
	cscfga.Api_1.ApiSession apiSession;
	List<cscfga__product_configuration__c> productConfigurationList;
	cscfga__Product_Definition__c productDefinitionDefault;
	cscfga__Product_Definition__c productDefinitionRootDefault;
	private cscfga__product_configuration__c productConfigurationRoot;
	private final string RootDefinitionId = 'a0p7E000001OXks';
	private final string SimTypeDefinitionId = 'a0p7E000001OWyS';
	private boolean noTriggerUser = false;
    private No_Trigger__c notrigger = null;
    public List<cscfga.Attribute> rootConfigurationAttributeList {get;set;}
    public List<SimTypeConfigurationDisplay> simTypeConfigurationList {get;set;}
	/***
	END OF PROPERTIES
	***/

	public IoTConnectAdvancedPricingController(ApexPages.StandardController controller) {
		standardController = controller;
		if (ApexPages.currentPage().getParameters().get('idBundle') != null){
			bundleId = ApexPages.currentPage().getParameters().get('idBundle');
		}
	}

	public void init(){
		if (bundleIdQueryString != null || bundleId != null){
			productConfigurationList = getProductConfigurationsFromBundle(bundleIdQueryString != null ? bundleIdQueryString : bundleId);
			productConfigurationRoot = getProductConfigurationFromList(RootDefinitionId, productConfigurationList);
			//simTypeList = getSimTypeListForConfigurations(productConfigurationList);
		}else{
			createBundleAndAddNewConfiguration();
		}
		populateScreenAttributes();
	}

	public PageReference addNewSimType(){
		disableTriggers();
		addNewSimTypeConfiguration();
		enableTriggers();
		//removeRootAndParentFromConfigurations();
		return returnThisPageReference();
	}

	public PageReference saveAll(){
		disableTriggers();
		
		enableTriggers();
		return returnThisPageReference();
	}

	private void createBundleAndAddNewConfiguration(){
		//TO DO- implement
		productBundle =(cscfga__Product_Bundle__c) standardController.getRecord();
		productBundle.cscfga__Opportunity__c = '0067E000002tklh';

		disableTriggers();
		upsert productBundle;
		bundleId = productBundle.Id;
		addNewRootConfiguration();
		addNewSimTypeConfiguration();
		enableTriggers();
	}

	private void addNewRootConfiguration(){
		string query = 'select id,name from cscfga__Product_Definition__c where id = \'' + RootDefinitionId + '\'';
        if (productDefinitionRootDefault == null)
        	productDefinitionRootDefault = Database.query(query);

        productDefinitionRootDefault.Name = productDefinitionRootDefault.Name + productBundle.Id;
		apiSession = cscfga.Api_1.getApiSession();
		apiSession.setProductToConfigure(productDefinitionRootDefault);

		apiSession.setBundle(productBundle);
		apiSession.persistConfiguration(true);
	}

	private void addNewSimTypeConfiguration(){
		/*
		TO DO - instantiate default configuration and put into custom label so use only clone configuration
		for test using a0n7E000000tdSR
		*/
		string query = 'select id,name from cscfga__Product_configuration__c where id = \'a0n7E000000tfBc\'';
        cscfga__Product_configuration__c confToCopy = Database.query(query);

		apiSession = cscfga.Api_1.getApiSession();
		productBundle =(cscfga__Product_Bundle__c) standardController.getRecord();
		Id clonedConf = cscfga.Api_1.cloneConfiguration(confToCopy, new cscfga__product_bundle__c(Id = bundleId), null);
		setParentConfigurationForSimType(clonedConf);
		addConfigurationToRootConfiguration(clonedConf);
	}

	private void addConfigurationToRootConfiguration(Id idConfig){
		if (productConfigurationRoot == null){
			if (productConfigurationList == null && !String.isEmpty(bundleId)){
				productConfigurationList = getProductConfigurationsFromBundle(bundleId);
			}
			productConfigurationRoot = getProductConfigurationFromList(RootDefinitionId, productConfigurationList);
		}

		if (productConfigurationRoot != null){
			addRelatedConfigurationToRoot(idConfig, productConfigurationRoot.Id);
		}
	}

	private void setParentConfigurationForSimType(Id newConfiguration){
		if (productConfigurationRoot == null){
			if (productConfigurationList == null && !String.isEmpty(bundleId)){
				productConfigurationList = getProductConfigurationsFromBundle(bundleId);
			}
			productConfigurationRoot = getProductConfigurationFromList(RootDefinitionId, productConfigurationList);
		}

		cscfga__product_configuration__c pcTmp = [SELECT id, cscfga__Parent_Configuration__c, cscfga__Attribute_Name__c
												FROM cscfga__product_configuration__c where id = :newConfiguration];

		pcTmp.cscfga__Parent_Configuration__c = productConfigurationRoot.Id;
		pcTmp.cscfga__Attribute_Name__c = 'SimType';

		update pcTmp;

	}

	private cscfga__product_configuration__c getProductConfigurationFromList(Id definitionId, List<cscfga__product_configuration__c> productConfigurationListIn){
		cscfga__product_configuration__c configReturn = null;
		if (productConfigurationListIn != null){
			for (cscfga__product_configuration__c pc : productConfigurationListIn){
				if (pc.cscfga__Product_Definition__c == definitionId){
					configReturn = pc;
					break;
				}
			}
		}
		return configReturn;
	}

	private List<cscfga__product_configuration__c> getProductConfigurationsFromBundle(Id idBundleIn){
		String query = 'SELECT ' + SObjectHelper.getFieldListForSOQL('cscfga__product_configuration__c',null,null) + ',' +SObjectHelper.getFieldListForSOQL('cscfga__Screen_Flow__c','cscfga__Screen_Flow__r.',null) + 
        ' FROM cscfga__product_configuration__c where cscfga__product_bundle__c=\'' + idBundleIn + '\''; 
        List<cscfga__product_configuration__c> pcList = new List<cscfga__product_configuration__c>();
		pcList = Database.Query(query);

		return pcList;
	}

	private Map<Id, List<cscfga__attribute__c>> getProductConfigurationsAttributesMap(List<cscfga__product_configuration__c> productConfigurationListIn){
		String query = 'SELECT ' +  SObjectHelper.getFieldListForSOQL('cscfga__attribute__c',null,null) + ', cscfga__product_configuration__r.cscfga__Product_Definition__c'
					+ ' FROM cscfga__attribute__c where cscfga__product_configuration__c in :productConfigurationListIn';
		
		List<cscfga__attribute__c> attributeList = new List<cscfga__attribute__c>();
		attributeList = Database.Query(query);

		Map<Id, List<cscfga__attribute__c>> mapTmp = new Map<Id, List<cscfga__attribute__c>>();
		
		for (cscfga__attribute__c attr : attributeList){
			List<cscfga__attribute__c> attrTmpList;
			if (mapTmp.get(attr.cscfga__product_configuration__c) == null){
				attrTmpList = new List<cscfga__attribute__c>();
			}
			else{
				attrTmpList = mapTmp.get(attr.cscfga__product_configuration__c);
			}
			attrTmpList.add(attr);

			mapTmp.put(attr.cscfga__product_configuration__c, attrTmpList);
		}

		return mapTmp;
	}


	private void populateScreenAttributes(){
		if (productConfigurationList == null && !String.isEmpty(bundleId)){
			productConfigurationList = getProductConfigurationsFromBundle(bundleId);
		}

		screenAttributes = new List<cscfga.Attribute>();
		if (productConfigurationList != null){
			simTypeConfigurationList = new List<SimTypeConfigurationDisplay>();
			rootConfigurationAttributeList = new List<cscfga.Attribute>();
			for (cscfga__product_configuration__c pcTmp : productConfigurationList){

				if (pcTmp.cscfga__Product_Definition__c == SimTypeDefinitionId){
					cscfga.Api_1.ApiSession apiSessionTmp = cscfga.Api_1.getApiSession(pcTmp);
					cscfga.ProductConfiguration productConfigurationTmp = apiSessionTmp.getConfiguration();
					SimTypeConfigurationDisplay sTypeConfDisplay = new SimTypeConfigurationDisplay();
					sTypeConfDisplay.attributeList = new cscfga.Attribute[3];
					for (cscfga.Attribute anAttribute : productConfigurationTmp.getAttributes()){
						if (anAttribute.getName().equalsIgnoreCase('Article Info Set')){
							sTypeConfDisplay.attributeList[0] = anAttribute;
						}else if (anAttribute.getName().equalsIgnoreCase('Quantity')){
							sTypeConfDisplay.attributeList[1] = anAttribute;
						}else if (anAttribute.getName().equalsIgnoreCase('Activation Fee')){
							sTypeConfDisplay.attributeList[2] = anAttribute;
						}
					}
					simTypeConfigurationList.add(sTypeConfDisplay);
				}
				else if (pcTmp.cscfga__Product_Definition__c == RootDefinitionId){
					cscfga.Api_1.ApiSession apiSessionTmp = cscfga.Api_1.getApiSession(pcTmp);
					cscfga.ProductConfiguration productConfigurationTmp = apiSessionTmp.getConfiguration();
					for (cscfga.Attribute anAttribute : productConfigurationTmp.getAttributes()){
						if (anAttribute.getName().equalsIgnoreCase('Discount')){
							rootConfigurationAttributeList.add(anAttribute);
						}
					}
				}
			}
		}
	}

	private void disableTriggers(){
		List<No_Trigger__c> noList = [Select Id, SetupOwnerId, Flag__c from No_Trigger__c where SetupOwnerId =: UserInfo.getUserId() limit 1];
        if (noList.isEmpty()) {
            noTriggerUser=true;
            notrigger = new No_Trigger__c();
            notrigger.Flag__c = true;
            notrigger.SetupOwnerId = UserInfo.getUserId();
            upsert notrigger;
        } else {
            noList[0].Flag__c = true;
            upsert noList[0];
            notrigger = noList[0];
        }
	}

	private void enableTriggers(){
		notrigger.Flag__c = false;
        upsert notrigger; 
        if (noTriggerUser) {
            delete notrigger;
        }
	}

	private void setRootConfigurationInSession(){
		apiSession = cscfga.Api_1.getApiSession(getProductConfigurationFromList(RootDefinitionId, productConfigurationList));
	}

	private PageReference returnThisPageReference(){
		PageReference page = new PageReference('/apex/IoTConnectAdvancedPricing?idBundle=' + bundleId);
		page.setRedirect(true);
        return page;
	}

	private void removeRootAndParentFromConfigurations(){
		List<cscfga__product_configuration__c> pcTmpList = [SELECT id, cscfga__Parent_Configuration__c, cscfga__Root_Configuration__c 
												from cscfga__product_configuration__c 
												where cscfga__product_bundle__c = :bundleId];


		if (pcTmpList != null){
			disableTriggers();
			for (cscfga__product_configuration__c pcTmp : pcTmpList){
				pcTmp.cscfga__Parent_Configuration__c = null;
				pcTmp.cscfga__Root_Configuration__c = null;
			}
			update pcTmpList;
			enableTriggers();
		}
	}

	private void addRelatedConfigurationToRoot(Id configIdToAdd, Id rootConfigId){
		List<cscfga__attribute__c> attList = [SELECT id, name, cscfga__value__c from cscfga__attribute__c where cscfga__product_configuration__c = :rootConfigId and name = 'SimType'];
		
		if (attList != null && attList.size() > 0){
			cscfga__attribute__c att = attList[0];
			String attValue = att.cscfga__value__c;
			if (!String.isEmpty(attValue)){
				attValue = attValue + ',' + String.valueOf(configIdToAdd);
			}else{
				attValue = String.valueOf(configIdToAdd);
			}
			att.cscfga__value__c = attValue;
			update att;
		}
	}


	/********************************************
	HELPER CLASSES
	*********************************************/
	public class SimTypeConfigurationDisplay{
		public List<cscfga.Attribute> attributeList {get; set;}
	}
}