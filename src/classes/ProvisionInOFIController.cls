public with sharing class ProvisionInOFIController
{
	//--------------------------------------------------------------------------
	// Constants
	//--------------------------------------------------------------------------
	private final String[] MANDATORY_FIELDS_ACCOUNT_ADDRESS = new String[]
	{
		'Name',
		'Account_Street__c',
		'Account_City__c',
		'Account_Postcode__c',
		'Payment_Term__c',
		'Payment_Method__c'
	};

	private final String[] MANDATORY_FIELDS_BILLING_ADDRESS = new String[]
	{
		'Name',
		'Billing_Street__c',
		'Billing_City__c',
		'Billing_Postcode__c',
		'Payment_Term__c',
		'Payment_Method__c'
	};

	//--------------------------------------------------------------------------
	// Members
	//--------------------------------------------------------------------------
	ApexPages.StandardController controller;

	//--------------------------------------------------------------------------
	// Getters / setters
	//--------------------------------------------------------------------------
	public String[] errorMessages { get; set; }

	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	public ProvisionInOFIController(ApexPages.StandardController controller)
	{
		this.controller = controller;
	}

	//--------------------------------------------------------------------------
	// Change OFI provisioning status
	//--------------------------------------------------------------------------
	public PageReference provision()
	{
		//
		// Check for mandatory fields
		//
		Billing_Account__c billingAccount =
				[ SELECT Name, Payment_Term__c, Payment_Method__c, Override_Account_Address__c,
						Account_Street__c, Account_City__c, Account_Postcode__c, Account_Country__c,
						Billing_Street__c, Billing_City__c, Billing_Postcode__c, Billing_Country__c,
						OFI_Provisioning_Status__c
				  FROM Billing_Account__c
				  WHERE Id = :controller.getId() ];

		String[] mandatoryFields = billingAccount.Override_Account_Address__c ?
				MANDATORY_FIELDS_BILLING_ADDRESS : MANDATORY_FIELDS_ACCOUNT_ADDRESS;
		errorMessages = SObjectHelper.validatePresence(billingAccount, mandatoryFields);

		if (errorMessages.isEmpty())
		{
			//
			// No errors provision in OFI
			//
			errorMessages = null;
			billingAccount.OFI_Provisioning_Status__c = Constants.PENDING;
			update billingAccount;
		}
		return null;
	}

	//--------------------------------------------------------------------------
	// Return to billing account page
	//--------------------------------------------------------------------------
	public PageReference gotoBillingAccount()
	{
		return new PageReference('/' + controller.getId());
	}
}