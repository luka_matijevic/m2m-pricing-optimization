global with sharing class BillingAccountDiscountController {
    
    String[] discountTypes = new String[]{};
    
    public String[] getDiscountTypes() {
        return discountTypes;
    }
        
    public void setDiscountTypes(String[] discountTypes) {
        this.discountTypes = discountTypes;
    }

     public List<SelectOption> getItems() {
        
        List<Discount_Types__c> discountTypeList = Discount_Types__c.getall().values();

        List<SelectOption> options = new List<SelectOption>();
        for (Discount_Types__c dt : discountTypeList) {
            options.add(new SelectOption(dt.name,dt.name));
        }
        
        return options;
    }

    public BillingAccountDiscountController(ApexPages.StandardController cntrl) {
        
	}
}