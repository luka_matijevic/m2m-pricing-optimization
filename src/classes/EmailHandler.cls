public class EmailHandler {
	
	public interface Emailable {
		Messaging.SingleEmailMessage[] emails();
	}

	public EmailHandler() {}

	public Messaging.SendEmailResult[] send(Emailable e) {
		return this.send(e, true);
	}

	public virtual Messaging.SendEmailResult[] send(Emailable e, Boolean allOrNothing) {
		return Messaging.sendEmail(e.emails(), allOrNothing);
	}
}