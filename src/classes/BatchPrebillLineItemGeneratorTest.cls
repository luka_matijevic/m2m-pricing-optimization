@isTest
public class BatchPrebillLineItemGeneratorTest {


	static testMethod void testAddingOrderLineItemExistingOneOffPrebillSame() {

		
		addOrderLineItemExistingPrebill(BillingRateModel.ONEOFF, BillingRateModel.ONEOFF,true);
		
		List<Prebill__c> prebillList = [Select Id From Prebill__c];
		System.assertEquals(1,prebillList.size());
		Prebill__c prebill = prebillList[0];
		
		System.assertEquals(0, [Select Id from Prebill_Line_Item__c where Prebill__c = :prebill.Id].size());
	}
	
	static testMethod void testAddingOrderLineItemExistingOneOffPrebillDifferent() {

		
	/*	addOrderLineItemExistingPrebill(BillingRateModel.ONEOFF, BillingRateModel.INSTALLMENTS,true);
		String brm = BillingRateModels.toString(BillingRateModel.INSTALLMENTS);
		List<Prebill__c> prebillList = [Select Id,Billing_Rate_Model__c,Daily_Bill__c From Prebill__c WHERE Billing_Rate_Model__c = :brm];
		System.assertEquals(1,prebillList.size());
		Prebill__c prebill = prebillList[0];
		
		System.assertEquals(1,[Select Id from Prebill_Line_Item__c].size());
		System.assertEquals(1, [Select Id from Prebill_Line_Item__c where Prebill__c = :prebill.Id].size());*/
	}
	
	static testMethod void testAddingOrderLineItemExistingInstallmentsPrebillSame() {

		
		addOrderLineItemExistingPrebill(BillingRateModel.INSTALLMENTS,BillingRateModel.INSTALLMENTS,true);
		
		List<Prebill__c> prebillList = [Select Id From Prebill__c];
		System.assertEquals(1,prebillList.size());
		Prebill__c prebill = prebillList[0];
		
		System.assertEquals(0, [Select Id from Prebill_Line_Item__c where Prebill__c = :prebill.Id].size());
	}
	
	static testMethod void testAddingOrderLineItemExistingInstallmentsPrebillDifferent() {

		
	/*	addOrderLineItemExistingPrebill(BillingRateModel.INSTALLMENTS,BillingRateModel.ONEOFF,true);
		String brm = BillingRateModels.toString(BillingRateModel.ONEOFF);
		List<Prebill__c> prebillList = [Select Id,Billing_Rate_Model__c,Daily_Bill__c From Prebill__c WHERE Billing_Rate_Model__c = :brm];
		System.assertEquals(1,prebillList.size());
		Prebill__c prebill = prebillList[0];
		
		System.assertEquals(1,[Select Id from Prebill_Line_Item__c].size());
		System.assertEquals(1, [Select Id from Prebill_Line_Item__c where Prebill__c = :prebill.Id].size());*/
	}
	
	static testMethod void testAddingServiceLineItemExistingOneOffPrebillSame() {

		
		addServiceLineItemExistingPrebill(BillingRateModel.ONEOFF,BillingRateModel.ONEOFF,true);
		
		List<Prebill__c> prebillList = [Select Id From Prebill__c];
		System.assertEquals(1,prebillList.size());
		Prebill__c prebill = prebillList[0];
		
		System.assertEquals(1, [Select Id from Prebill_Line_Item__c where Prebill__c = :prebill.Id].size());
	}
	
	static testMethod void testAddingServiceLineItemOneOffJournalMappingCheck() {

		
		addServiceLineItemExistingPrebill(BillingRateModel.ONEOFF,BillingRateModel.ONEOFF,true);
		
		List<Prebill__c> prebillList = [Select Id From Prebill__c];
		System.assertEquals(1,prebillList.size());
		Prebill__c prebill = prebillList[0];
		
		List<Prebill_Line_Item__c> prebillLineItems = [
				Select 
					Id,
					Name,
					I_CO__c,
					KoSt__c,	
					Projekt__c,
					Tax_Code__c,
					Tax_Amount__c,
					Konto__c
				from 
					Prebill_Line_Item__c 
				where 
					Prebill__c = :prebill.Id
			];
		
		System.assertEquals('test2', prebillLineItems[0].Projekt__c);
	}
	
	static testMethod void testAddingServiceLineItemExistingOneOffPrebillDifferent() {

		/*
		addServiceLineItemExistingPrebill(BillingRateModel.ONEOFF,BillingRateModel.INSTALLMENTS,true);
		String brm = BillingRateModels.toString(BillingRateModel.INSTALlMENTS);
		List<Prebill__c> prebillList = [Select Id,Billing_Rate_Model__c,Daily_Bill__c From Prebill__c WHERE Billing_Rate_Model__c = :brm];
		System.assertEquals(1,prebillList.size());
		Prebill__c prebill = prebillList[0];
		
		System.assertEquals(1,[Select Id from Prebill_Line_Item__c].size());
		System.assertEquals(1, [Select Id from Prebill_Line_Item__c where Prebill__c = :prebill.Id].size()); */
	}
	static testMethod void testAddingServiceLineItemExistingInstallmentsPrebillSame() {
		 Invoice_Number__c i = new Invoice_Number__c();
	        i.Name='Currrent';
	        i.Value__c=100;
	        
	        insert i;
	        
		addServiceLineItemExistingPrebill(BillingRateModel.INSTALLMENTS,BillingRateModel.INSTALLMENTS,true);
		
		List<Prebill__c> prebillList = [Select Id,Billing_Rate_Model__c,Daily_Bill__c From Prebill__c];
		System.debug('** multiplePrebills: ' + prebillList);
		System.assertEquals(1,prebillList.size());
		Prebill__c prebill = prebillList[0];
		
		System.assertEquals(1, [Select Id from Prebill_Line_Item__c where Prebill__c = :prebill.Id].size());
	}
	
	static testMethod void testAddingServiceLineItemExistingInstallmentsPrebillDifferent() {

		/*
		addServiceLineItemExistingPrebill(BillingRateModel.INSTALLMENTS,BillingRateModel.ONEOFF,true);
		String brm = BillingRateModels.toString(BillingRateModel.ONEOFF);
		List<Prebill__c> prebillList = [Select Id,Billing_Rate_Model__c,Daily_Bill__c From Prebill__c WHERE Billing_Rate_Model__c = :brm];
		System.assertEquals(1,prebillList.size());
		Prebill__c prebill = prebillList[0];
		
		System.assertEquals(1, [Select Id from Prebill_Line_Item__c where Prebill__c = :prebill.Id].size()); */
	}
	
	static testMethod void testCreatingNewPrebillForOrderLineItemOneOff() {

		
		addOrderLineItemExistingPrebill(BillingRateModel.ONEOFF,BillingRateModel.ONEOFF,false);
		
		Daily_Bill__c dailyBill = [Select Id From Daily_Bill__c LIMIT 1];
		List<Prebill__c> prebillList = [Select Id,Daily_Bill__c From Prebill__c];
		System.assertEquals(0,prebillList.size());
		/*Prebill__c prebill = prebillList[0];
		
		System.assertEquals(1, [Select Id from Prebill_Line_Item__c where Prebill__c = :prebill.Id].size());
		System.assertEquals(dailyBill.Id,prebill.Daily_Bill__c);*/
	}
	
	static testMethod void testCreatingNewPrebillForOrderLineItemInstallments() {

		
		addOrderLineItemExistingPrebill(BillingRateModel.INSTALLMENTS, BillingRateModel.INSTALLMENTS,false);
		
		Daily_Bill__c dailyBill = [Select Id From Daily_Bill__c LIMIT 1];
		List<Prebill__c> prebillList = [Select Id,Daily_Bill__c From Prebill__c];
		System.assertEquals(0,prebillList.size());
		/*Prebill__c prebill = prebillList[0];
		
		System.assertEquals(1, [Select Id from Prebill_Line_Item__c where Prebill__c = :prebill.Id].size());
		System.assertEquals(dailyBill.Id,prebill.Daily_Bill__c);*/
	}
	
	static testMethod void testCreatingNewPrebillForServiceLineItemOneOff() {

	        
		addServiceLineItemExistingPrebill(BillingRateModel.ONEOFF,BillingRateModel.ONEOFF,false);
		
		Daily_Bill__c dailyBill = [Select Id From Daily_Bill__c LIMIT 1];
		List<Prebill__c> prebillList = [Select Id,Daily_Bill__c From Prebill__c];
		System.assertEquals(1,prebillList.size());
		Prebill__c prebill = prebillList[0];
		
		System.assertEquals(1, [Select Id from Prebill_Line_Item__c where Prebill__c = :prebill.Id].size());
		System.assertEquals(dailyBill.Id,prebill.Daily_Bill__c);
		
	}
	
	static testMethod void testCreatingNewPrebillForServiceLineItemInstallments() {

		
		addServiceLineItemExistingPrebill(BillingRateModel.INSTALLMENTS,BillingRateModel.INSTALLMENTS,false);
		
		Daily_Bill__c dailyBill = [Select Id From Daily_Bill__c LIMIT 1];
		List<Prebill__c> prebillList = [Select Id,Daily_Bill__c From Prebill__c];
		System.assertEquals(1,prebillList.size());
		Prebill__c prebill = prebillList[0];
		
		System.assertEquals(1, [Select Id from Prebill_Line_Item__c where Prebill__c = :prebill.Id].size());
		System.assertEquals(dailyBill.Id,prebill.Daily_Bill__c);
		
	}
	
	private static void addServiceLineItemExistingPrebill(BillingRateModel billingRateModelPrebill,BillingRateModel billingRateModelService, boolean createPrebill){

		
		Date billingDate = Date.newInstance(2014, 2, 1);
		
		Billing_Settings__c billingSettings = new Billing_Settings__c(
			OFI_email_address__c = 'kristijan.kosutic@cloudsense.com',
			Status_reporting_email__c = 'kristijan.kosutic@cloudsense.com',
			Billing_Cycle_Offset__c = 0.0,
			Prebill_Poller_Delay_Seconds__c = 2.0
		);
		insert billingSettings;
		
		Daily_Bill__c dailyBill = new Daily_Bill__c(
			Date__c = billingDate
		);
		insert dailyBill;
		
		Account acc = new Account(
			Name = 'Test account',
			BillingCity = 'Bremen',
			BillingCountry = 'Germany',
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße',
			Type = 'Customer',
            Global__c = true,
			Billing_Cycle__c = String.valueOf(billingDate.day()),
			VAT__c = 1
		);
		insert acc;
		
		Billing_Account__c billingAccount = new Billing_Account__c(
			Billing_Country__c = 'Deutschland',
			Payer__c = acc.Id,
			VAT__c = '19%',
			OFI_ID__c = '0001'
		);
		insert billingAccount;
		
		if(createPrebill){
			Prebill__c prebill = new Prebill__c(
				Daily_Bill__c = dailyBill.Id,
				Billing_Rate_Model__c = BillingRateModels.toString(billingRateModelPrebill),
				Billing_Account__c = billingAccount.Id
			);
			insert prebill;
		}
		
		Product_Order__c productOrder = new Product_Order__c(
			Quantity__c = 1
		);
		insert productOrder;

		cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c(
		);
		insert productConfiguration;

		Product_Configuration_Order__c productConfigurationOrder = new Product_Configuration_Order__c(
			Product_Order__c = productOrder.Id,
			Product_Configuration__c = productConfiguration.Id
		);
		insert productConfigurationOrder;
		
		Order_Request__c oRequest = new Order_Request__c(
			Billing_Account__c = billingAccount.Id,
			Account__c = acc.Id, Shipping_City__c='Test City', Shipping_Country__c = 'Test Country', Shipping_Postal_Code__c='10000', Shipping_Street__c='Test shipping street');

		insert oRequest;
		
		csord__Order_Request__c orderRequest = new csord__Order_Request__c(
			csord__Module_Name__c = 'test',
			csord__Module_Version__c = 'test'
		);
		insert orderRequest;
		
		
		List<Journal_Mapping__c> journalMappings = new List<Journal_Mapping__c>();
			
		
		journalMappings.add(new Journal_Mapping__c(
			Name = 'Aktivierunggebühr',
			Country__c = 'DE',
			Revenue__c = '19%',
			Konto__c = '4120100',
			KoSt__c = '465605',
			Projekt__c = 'test2' 
		));
		
		insert journalMappings;
		
		csord__Subscription__c subscription = new csord__Subscription__c(
			Billing_Account__c = billingAccount.Id,
			csord__Identification__c = 'Id',
			csord__Order_Request__c = orderRequest.Id,
			csord__Status__c = 'Test'
		);
		insert subscription;

		csord__Service__c
		service = new csord__Service__c(
			csord__Subscription__c = subscription.Id,
			Billing_Rate_Model__c = BillingRateModels.toString(billingRateModelService),
			csord__Activation_Date__c = billingDate.addMonths(-3).addDays(-3),
			csord__Deactivation_Date__c = billingDate.addMonths(9),
			csord__Identification__c = 'Test',
			csord__Order_Request__c = orderRequest.Id,
			csord__Status__c = 'Test',
			Product_Configuration_Order__c = productConfigurationOrder.Id
			
		);
		insert service;
		
		csord__Service_Line_Item__c sli = new csord__Service_Line_Item__c(
			csord__Service__c = service.Id,
			csord__Identification__c = 'Test',
			csord__Order_Request__c = orderRequest.Id,
			Journal_Mapping__c = journalMappings[0].Id,
			Is_Billed__c = false,
			Total_Price__c = 10.0
			
		);
		insert sli;
		
		Test.startTest();
		Database.executeBatch(new BatchPrebillLineItemGenerator(billingDate), 1);
		Test.stopTest();
	}
	
	private static void addOrderLineItemExistingPrebill(BillingRateModel billingRateModelPrebill,BillingRateModel billingRateModelOrder, boolean createPrebill ){

		Date billingDate = Date.newInstance(2014, 2, 1);
		
		Billing_Settings__c billingSettings = new Billing_Settings__c(
			OFI_email_address__c = 'kristijan.kosutic@cloudsense.com',
			Status_reporting_email__c = 'kristijan.kosutic@cloudsense.com',
			Billing_Cycle_Offset__c = 2,
			Prebill_Poller_Delay_Seconds__c = 20
		);
		insert billingSettings;
		
		Daily_Bill__c dailyBill = new Daily_Bill__c(
			Date__c = billingDate
		);
		insert dailyBill;

		Account acc = new Account(
			Name = 'Test account',
			BillingCity = 'Bremen',
			BillingCountry = 'Germany',
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße',
			Type = 'Customer',
            Global__c = true,
			Billing_Cycle__c = String.valueOf(billingDate.day()),
			VAT__c = 1
		);
		insert acc;
		
		Billing_Account__c billingAccount = new Billing_Account__c(
			Billing_Country__c = 'Deutschland',
			Payer__c = acc.Id,
			VAT__c = '19%',
			OFI_ID__c = '0001'
		);
		insert billingAccount;
		
		if(createPrebill){
			Prebill__c prebill = new Prebill__c(
				Daily_Bill__c = dailyBill.Id,
				Billing_Rate_Model__c = BillingRateModels.toString(billingRateModelPrebill),
				Billing_Account__c = billingAccount.Id
			);
			insert prebill;
		}
		
		Order_Request__c oRequest = new Order_Request__c(
			Billing_Account__c = billingAccount.Id,
			Account__c = acc.Id, Shipping_City__c='Test City', Shipping_Country__c = 'Test Country', Shipping_Postal_Code__c='10000', Shipping_Street__c='Test shipping street');

		insert oRequest;
		
		csord__Order_Request__c orderRequest = new csord__Order_Request__c(
			csord__Module_Name__c = 'test',
			csord__Module_Version__c = 'test'
		);
		insert orderRequest;
		
		Journal_Mapping__c jm = new Journal_Mapping__c();
		insert jm;
		
		csord__Order__c ord = new csord__Order__c(
			csord__Identification__c = 'Test',
			csord__Order_Request__c = orderRequest.Id,
			csord__Status2__c = 'Test',
			Order_Request__c = oRequest.Id
		);
		insert ord;

		csord__Order_Line_Item__c oli = new csord__Order_Line_Item__c(
			csord__Order__c = ord.Id,
			csord__Discount_Type__c = 'Amount',
			csord__Identification__c = 'Test',
			csord__Order_Request__c = orderRequest.Id,
			Journal_Mapping__c = jm.Id,
			// required for prebill creation
			csord__Total_Price__c = 1000.0,
			Name = 'test',
			Is_Billed__c = false,
			Quantity__c = 1,
			Billing_Rate_Model__c = BillingRateModels.toString(billingRateModelOrder)
		);
		insert oli;
		
		Test.startTest();
		Database.executeBatch(new BatchPrebillLineItemGenerator(billingDate), 1);
		Test.stopTest();
		
	}
}