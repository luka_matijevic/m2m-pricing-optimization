/*
* SIM report generation batch class. Creates Non Trial & Commercial SIM reports as well.
*/
public class BatchSIMReportGeneration implements Database.Batchable<sObject>, Database.Stateful {
	
	private static final String query = 'Select Id, Account__c, Account__r.Name, SRF__c, Third_Upload_File__c, Account__r.TEF_DE_Sales_Channel__c, Activation_Date__c, Deactivation_Date__c, Commercial_Date__c, HistoryTracking__c, Status__c, SM2M_Status__c, State_Change_Date__c, Trial__c from Stock__c';
	
	private SIMReportBuilder reportBuilder;
	private SIMReportBuilder trialReportBuilder;
	private SIMReportBuilder commercialReportBuilder;
	private SIMReportBuilder newCBReportBuilder;
	private SIMReportBuilder accountBuilder;
	private Integer toYear;
	
	/*
	* Constructor call.
	*/
	public BatchSIMReportGeneration(Integer toYear) {
		this.toYear = toYear; 
	}
		
	/*
	* Query execution.
	*/
	public Database.QueryLocator start(Database.BatchableContext BC) {
		reportBuilder = new SIMReportBuilder(toYear, false);
		accountBuilder = new SIMReportBuilder(toYear,true,true,true);
		trialReportBuilder = new SIMReportBuilder(toYear, false);
		commercialReportBuilder = new SIMReportBuilder(toYear, true); // T-16972 changes to include Commercial dates
		newCBReportBuilder = new SIMReportBuilder(toYear, true, true);
		deleteExistingReports();

		return Database.getQueryLocator(query);  
	}
	
	/*
	* Execute call.
	*/
	public void execute(Database.BatchableContext BC, List<SObject> scope) {
		List<Stock__c> sims = (List<Stock__c>) scope;
		reportBuilder.addSims(sims);
		trialReportBuilder.addSims(StockServiceHelper.filterTrialSObjects(sims)); 
		commercialReportBuilder.addSims(StockServiceHelper.filterTrialSObjects(sims));
		newCBReportBuilder.addSims(StockServiceHelper.filterTrialSObjects(sims));
		accountBuilder.addSims(StockServiceHelper.filterTrialSObjects(sims));
	}
	
	/*
	* Finish call.
	*/
	public void finish(Database.BatchableContext BC) {
		Date now = Date.today(); 
		List<SIM_Report__c> reports = reportBuilder.buildReports();
		System.debug(LoggingLevel.DEBUG, 'reports size ' + reports.size());
		List<SIM_Report__c> trialReports = trialReportBuilder.buildReports();
		System.debug(LoggingLevel.DEBUG, 'trialReports size ' + trialReports.size());
		List<SIM_Report__c> comReports = commercialReportBuilder.buildReports(); // T-16972 changes to include Commercial dates
		System.debug(LoggingLevel.DEBUG, 'comReports size ' + comReports.size());
		List<SIM_Report__c> newCBReports = newCBReportBuilder.buildReports();
		System.debug(LoggingLevel.DEBUG, 'newCBReports size ' + newCBReports.size());
		
		if(reports!=null && !reports.isEmpty()){
			insert reports; 
		}
		
		// Trial Flag update
		for(SIM_Report__c sr : trialReports){
			sr.trial__c=true;
		}
		if(trialReports!=null && !trialReports.isEmpty()){
			insert trialReports; 
		}
		
		// Commercial Flag update
		for(SIM_Report__c sRep : comReports){
			sRep.commercial__c=true;
		}
		if(comReports!=null && !comReports.isEmpty()){
			insert comReports;
		}
		
		for (SIM_Report__c nsr : newCBReports) {
			nsr.newCBCalculation__c = true;
		}
		
		if (newCBReports != null && !newCBReports.isEmpty()) {
			insert newCBReports;
		}

		Database.executeBatch(new BatchSIMReportGenerationExtended(accountBuilder), 50);
	}

	/**
	 * Deletes all existing SIM reports and returns the number of deleted records
	 */ 
	private Integer deleteExistingReports() {
		List<SIM_Report__c> existingSIMReports = [Select Id from SIM_Report__c];
		Integer size = existingSIMReports.size();
		if (existingSIMReports != null && existingSIMReports.size() > 0) {
			delete existingSIMReports;
		}
		return size;
	}
}