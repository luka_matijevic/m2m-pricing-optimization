global class SimSwapModifierPoller implements Schedulable {
    public static void startPolling() {
		SimSwapModifierPoller.scheduleTick(System.now().addSeconds(10));
	}
	
	global void execute(SchedulableContext sc) {
		List<Stock__c> stocks = [SELECT ID FROM Stock__c WHERE Status__c in ('Pending activation', 'Pending retirement')];
		
		if(stocks != null && stocks.size() > 0) {
		    csam_t1.ObjectGraphCalloutHandler.createAndSend('Sim Swap Modifier', Label.SIM_SWAP_TUF); // CloudSense Sim Swap TUF
		}
		
		Datetime nextTick = System.now().addMinutes(5);
		SimSwapModifierPoller.scheduleTick(nextTick);
		System.abortJob(sc.getTriggerId());
	}
	
	private static void scheduleTick(Datetime dt) {
		String chron_exp = dt.format('ss mm HH dd MM ? yyyy');
		SimSwapModifierPoller poller = new SimSwapModifierPoller();
		// Schedule the next job, and give it the system time so name is unique
		System.schedule(jobName(dt), chron_exp, poller);
	}
	
	private static String jobName(Datetime dt) {
		return 'SimSwapModifierPoller_' + dt.getTime();
	}
}