global class UpdateSimStatusesInboundProcessor implements csam_t1.InboundMessageProcessor {

	/**
	* Accepts all update sim status messages (should be put before the standard Inbound Message Handler)
	* @param incomingMessage message to be processed
	* @return Boolean returns true
	*/
	public Boolean isMessageProcessible(csam_t1__Incoming_Message__c incomingMessage) {
	    if (incomingMessage.csam_t1__Incoming_URL_Path__c == '/csam_t1/callback/sims/update-statuses' || incomingMessage.csam_t1__Incoming_URL_Path__c == '/csam_t1/callback/modify/simswap')
		    return true;
		else return false;
	}

	/**
	* Assumes that the message payload is a status message from integration platform
	* 
	* @param incomingMessage message to be processed with corresponding payload attachment
	* @param oldToNewIdsMap map of old-to-new ids used when inserting in chunks to pass the references between messages
	*/
	public void processMessage(csam_t1__Incoming_Message__c incomingMessage, Map<Id, Id> oldToNewIdsMap) {
		
		try {
			
			Account originatingAccount = originatingAccount(incomingMessage);
			
			Attachment attachment = [SELECT Id, Name, Body
										FROM Attachment
										WHERE ParentId = :incomingMessage.Id
										AND Name like 'Message%' LIMIT 1];

			CallbackMessage cm = (CallbackMessage) JSON.deserialize(attachment.Body.toString(), CallbackMessage.class);
			
			if (cm.objectsToUpdate != null && cm.objectsToUpdate.size() > 0) {
				processUpdates(cm);
			}
			
			if (cm.objectsToInsert != null && cm.objectsToInsert.size() > 0) {
				
				List<Stock__c> unsynchronizedSims = (List<Stock__c>) SObjectHelper.filterSObjects(cm.objectsToInsert, Stock__c.sObjectType);			
				
				StockSynchronizationResult syncResult = new StockSynchronizer().synchronize(unsynchronizedSims);
				
				for (Stock__c sim : syncResult.StocksToInsert) {
					sim.Account__c = originatingAccount.Id;
					sim.Id = null;
				}
				insert syncResult.StocksToInsert;
				
				for (Stock__c sim : syncResult.StocksToUpdate) {
					sim.Account__c = originatingAccount.Id;
				}
				update syncResult.StocksToUpdate;
			}
			
			incomingMessage.csam_t1__Status__c = 'Processed';
			
		} catch (Exception e) {
			incomingMessage.csam_t1__Status__c = 'Process Error';

			Attachment errorAttach = new Attachment();
			errorAttach.Body = Blob.valueOf(e.getStackTraceString());
			errorAttach.parentId = incomingMessage.Id;

			String fullName = 'Error_' + e.getMessage();
			errorAttach.Name = fullName.abbreviate(255);
			
			insert errorAttach;
		}
		
		incomingMessage.csam_t1__Processed_At__c = System.now();
		update incomingMessage;
	}
	
	private Account originatingAccount(csam_t1__Incoming_Message__c incomingMessage) {
		
		List<csam_t1__Outgoing_Message_Record__c> outgoingMessageRecords = [
			SELECT
				csam_t1__Object_Name__c, csam_t1__Object_Record_Id__c
			FROM
				csam_t1__Outgoing_Message_Record__c
			WHERE
				csam_t1__Outgoing_Message__c = :incomingMessage.csam_t1__Outgoing_Message__c
		];
		
		for (csam_t1__Outgoing_Message_Record__c record : outgoingMessageRecords) {
			if (record.csam_t1__Object_Name__c == 'Account') {
				List<Account> accounts = [Select Id, Name from Account where Id = :record.csam_t1__Object_Record_Id__c];
				if (accounts.size() > 0) {
					return accounts.get(0);
				}
			}
		}
		
		return null;
	}
	
	/**
	 * @description Processes all updates defined by CallbackMessage.objectsToUpdate. First objects are separated based
	 * on sObject type. Then, to prevent different sObject types in single collection and corresponding chunking limits,
	 * each object type collection is separately updated.
	 * @param cm CallbackMessage
	 */
	private void processUpdates(CallbackMessage cm) {
		Map<String, List<sObject>> objectsToUpdateMap = new Map<String, List<sObject>>();
		for (sObject objectToUpdate : cm.objectsToUpdate) {
			String objectToUpdateName = objectToUpdate.getSObjectType().getDescribe().getName();
			if (!objectsToUpdateMap.containsKey(objectToUpdateName)) {
				objectsToUpdateMap.put(objectToUpdateName, new List<sObject>());
			}
			objectsToUpdateMap.get(objectToUpdateName).add(objectToUpdate);
		}

		// avoid updates in chunks with different sObject types in a collection
		for (String key : objectsToUpdateMap.keySet()) {
			update objectsToUpdateMap.get(key);
		}
	}
}