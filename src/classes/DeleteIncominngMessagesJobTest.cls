@isTest
public class DeleteIncominngMessagesJobTest {

    @testSetup
    static void prepareData(){
        
        list<csam_t1__Incoming_Message__c> incomingMesgs = new list<csam_t1__Incoming_Message__c>();
        for(integer i = 0; i<200; i++){
            csam_t1__Incoming_Message__c obj = new csam_t1__Incoming_Message__c();
            obj.Name = 'Name';
            obj.csam_t1__HTTP_Method__c = 'post';
            obj.csam_t1__Mac_Signature__c = 'csam_t1__Mac_Signature__c';
            obj.csam_t1__Chunk_Number__c = i;
            incomingMesgs.add(obj);
        }
        
        insert incomingMesgs;
    }
    
    static testMethod void testJobRun(){
        
        test.startTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        System.schedule('ScheduleApexClassTest', CRON_EXP, new DeleteIncominngMessagesJob());

        
        test.stopTest();
        
    }
}