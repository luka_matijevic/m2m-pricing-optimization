/*
* Name: ServiceTriggerTest 
* Description: Test class for ServiceTrigger.
* Author: petar matkovic
*/
@isTest
private class ServiceTriggerTest {
	
	@isTest static void testMailSend() {
		Csord__Order_Request__c orderRequest = new Csord__Order_Request__c(
			csord__Module_Name__c = 'test module name',
			csord__Module_Version__c = 'v1.1'
		);
		upsert orderRequest;
		Csord__order__c order = new Csord__order__c(
			name = 'Test Order',
			csord__Start_Date__c = System.today(),
			csord__Identification__c = 'Test Ident',
			csord__Order_Request__c = orderRequest.Id,
			csord__Status2__c = 'test status'
		);
		upsert order;
		Csord__subscription__c subscription = new Csord__subscription__c(
			csord__Identification__c = 'Test',
			csord__Order__c = order.Id,
			csord__Order_Request__c = orderRequest.Id,
			csord__Status__c = 'Open'
		);
		upsert subscription;

		Test.startTest();
        csord__Service__c service = new csord__Service__c();
        service.Csord__subscription__c = subscription.Id;
        service.Csord__Identification__c = 'CS-APN / VPN';
        service.Csord__Order_Request__c = orderRequest.Id;
        service.Csord__Status__c = 'Open';
        insert service;
        Test.stopTest();
	}
	
}