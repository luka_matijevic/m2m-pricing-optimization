public class AsyncCustomerBillUpload implements Queueable {
	    public void execute(QueueableContext context) {
            List<Customer_Monthly_Bill__c> cmbListToUpload = [SELECT Id, Uploaded__c, PDFBlob__c, Billing_Account__r.Name, Invoice_Number__c,Billing_Account__r.SM2M_Type__c, Billing_Account__r.SM2M_ID__c, Billing_Account__r.OFI_ID__c, Daily_Bill__r.Date__c FROM Customer_Monthly_Bill__c WHERE Uploaded__c = false LIMIT 50];
            
            for(Customer_Monthly_Bill__c cmb : cmbListToUpload) {
                Blob b = EncodingUtil.base64Decode(cmb.PDFBlob__c);
                String tmpBillingAcctAddition, tmpLocalOrGlobal;
                String tmp = cmb.Billing_Account__r.OFI_ID__c;//cmb.Billing_Account__r.Name.replace(' ', '_');
                String tmpDate = cmb.Daily_Bill__r.Date__c.format().replace('.', '_');
                String sm2mType = cmb.Billing_Account__r.SM2M_Type__c;
                tmpLocalOrGlobal = sm2mType != null && !sm2mType.equalsIgnoreCase('null') ? sm2mType.equalsIgnoreCase('Local') ? 'TEFDEL' : 'TEFDEG' : '';
                String sm2mId = cmb.Billing_Account__r.SM2M_ID__c != null ? String.valueOf(cmb.Billing_Account__r.SM2M_ID__c.intValue()) : '';
                tmpBillingAcctAddition = String.format('{0}{1}', new String[] {sm2mId, !String.isEmpty(tmpLocalOrGlobal) ? '_' + tmpLocalOrGlobal : ''});
                
                //String tmpLink = 'https://m2m-etl-uat.com/services/uploads/customerBills/' + tmp + '_' + tmpDate + '_' + tmpBillingAcctAddition;
                //String tmpLink = 'https://m2m-etl-prod.com/services/uploads/customerBills/' + tmp + '_' + tmpDate + '_' + tmpBillingAcctAddition;
                String tmpLink = Label.CustomerBillUploadLink + tmp + '_' + tmpDate + '_' + tmpBillingAcctAddition + '_' + cmb.Invoice_Number__c;
                
                CalloutIntegration.executeFileRequest(tmpLink, 'POST', tmp + '_' + tmpDate + '_' + tmpBillingAcctAddition + '_' + cmb.Invoice_Number__c, b, 32);
                
                cmb.Uploaded__c = true;
                update cmb;
            }
            
            cmbListToUpload = [SELECT Id, Uploaded__c, PDFBlob__c, Billing_Account__r.Name, Billing_Account__r.OFI_ID__c, Daily_Bill__r.Date__c FROM Customer_Monthly_Bill__c WHERE Uploaded__c = false LIMIT 50];
            
            if(cmbListToUpload != null && cmbListToUpload.size() > 0) {
                System.enqueueJob(new AsyncCustomerBillUpload());
            }
	    }
	}