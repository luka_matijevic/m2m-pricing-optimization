public class TerminationConsoleNewController {
    //Private variables
    private static final Integer pageSize = 100;
    private boolean clearAllSims;
    private ApexPages.StandardSetController stdController;
    
    /*PROPERTIES*/
	public String caseId {
		get{return ApexPages.currentPage().getParameters().get('Id');}
		set;
	}
	public String searchFC {get; set;}
	public String icicdFilter {get; set;}
	public List<stock__c> AllSims {get{
        numOfConsoleSims = String.valueOf(stdController.getResultSize());
        return (List<stock__c>)stdController.getRecords();
        }
        set;}
	public Case ticketCase {get;set;}
    public String dateOfReceipt{get;set;}
    public String numOfAccountSims {get;set;}
    public String numOfConsoleSims{get;set;}
	public List<selectOption> AccountFrameContracts{
        get{
            List<selectOption> options = new List<selectOption>();
            options.add(new selectOption('','All'));
            if(ticketCase != null && ticketCase.AccountId != null){
                //Add conditions to check for "Commerical" record type and "Archived Contract And Sent To Customer" status after checking with business
                for(Frame_Contract__c fc : [Select Id, Name from Frame_Contract__c where Customer__c = :ticketCase.AccountId]){
                    options.add(new selectOption(fc.Name, fc.Name));
                }
            }
            return options;
        }
        set;
    }
	public String simsToRemove{get;set;}
    public String selectedSims{get;set;}
    public string errorMsgs {get; set;}
    public String fileName {get; set;}
    public Blob fileBody {get; set;}
    public Set<String> iccIds = new Set<String>();
    public String hdnOrderBy {get; set;}
    public String hdnOrderDirection {get; set;}
    public Boolean showSpecialTermination{get;set;}
    public Date specialTerminationDate{get;set;}
    public Case helperCase{get;set;}
    public Boolean allSelected{get;set;}
    /*Properties END*/
    
	public TerminationConsoleNewController() {
        clearAllSims = false;
        allSelected = false;
        hdnOrderBy = 'Potential_Termination_Date__c';
        hdnOrderDirection ='ASC';
		if (!String.isEmpty(caseId))
			ticketCase = getCaseById(caseId);

		if (ticketCase != null){
            numOfAccountSims = getAccountNumOfSims(ticketCase.AccountId);
			loadDataInController();
            ticketCase.Sixt_Termination_date__c = date.newinstance(ticketCase.CreatedDate.year(), ticketCase.CreatedDate.month(), ticketCase.CreatedDate.day());
		}

        showSpecialTermination = false;
        helperCase = new Case();
        helperCase.Sixt_Termination_date__c = ticketCase != null ? ticketCase.Sixt_Termination_date__c : Date.today();
        UserRole userRole = getRoleByRoleId(UserInfo.getUserRoleId());
        if (userRole != null){
            if (userRole.Name == 'System Admin' || isUserAllowedForSpecialTermination(UserInfo.getUserId()))
                showSpecialTermination = true;
        }
	}

	private Case getCaseById(string caseId){
		List<Case> tmpCases = new list<case>([Select Id, CaseNumber, Frame_Contract__c, CreatedDate, Sixt_Termination_date__c, AccountId, Account.Name, Subject, Status, UDo_Ticket_created__c from Case where Id = :caseId LIMIT 1]);
		Case c = null;
		if (tmpCases[0] != null)
			c = tmpCases[0];
		return c;
	}

    private String getAccountNumOfSims(String accId){
        String numOfSims = '0';
        List<AggregateResult> results=[SELECT count(ID) from stock__c where account__c = :accId];
        numOfSims = string.valueOf(results[0].get('expr0'));
        return numOfSims;
    }

	/*
	 * Fetch SIMs into the StandardSetController
	 */
	private void loadDataInController(){
        String query = 'Select Id,Name,ICCID__c,MSISDN__c,IMSI__c,Activation_Date__c,Runtime__c,End_date__c,Termination_Status__c,'
                        +'BIC_Framework_Contract_No__c,Account__c, Commercial_Date__c, Potential_Termination_Date__c, SM2M_Status__c '
                        +'from stock__c '
                        +'where Account__c = \'' + ticketCase.AccountId + '\'' 
                                +'AND SM2M_Status__c IN (\'ACTIVE\', \'DEACTIVATED\')';

        if (iccIds != null && iccIds.size() > 0){
            String iccidsTmp = '';
            for (String iccid : iccids){
                if (String.isEmpty(iccidsTmp) || iccidsTmp == '')
                    iccidsTmp = '\'' + iccid + '\'';
                else
                    iccidsTmp += ', \'' + iccid + '\'';
            }
            query += ' AND (ICCID__c in (' + iccidsTmp + ') OR MSISDN__c in (' +  iccidsTmp + '))';
        }

        if (!String.isEmpty(searchFC))
            query += ' AND BIC_Framework_Contract_No__c = \'' + searchFC + '\'';

        if (!String.isEmpty(icicdFilter)){
            String tmpIds = '';
            for (String s : icicdFilter.split(',')){
                if (String.isEmpty(tmpIds)){
                    tmpIds = '\'' + s.trim() + '\'';
                }
                else{
                    tmpIds += ', \'' + s.trim() + '\'';
                }
            }
            query += ' AND (ICCID__c in (' + tmpIds + ') OR MSISDN__c in (' +  tmpIds + '))';
        }


        if (!String.isEmpty(simsToRemove)){
            String removedTmp = '';
            for (String s : simsToRemove.split(',')){
                if (String.isEmpty(removedTmp)){
                    removedTmp = '\'' + s + '\'';
                }
                else{
                    removedTmp += ', \'' + s + '\'';
                }
            }
            query += ' AND IMSI__c NOT IN ('+ removedTmp +')';
        }

        if (!String.isEmpty(hdnOrderBy) && !String.isEmpty(hdnOrderDirection)){
            query += ' ORDER BY ' + hdnOrderBy + ' ' + hdnOrderDirection;
        }

        query += ' LIMIT 9999';

        System.debug(query);

        stdController = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        stdController.setPageSize(pageSize);
        //AllSims = (List<stock__c>)stdController.getRecords();
	}

    public void addAllAccountSims(){
        simsToRemove = '';
        searchFC = '';
        icicdFilter = '';
        selectedSims = '';
        fileBody = null;
        iccIds.clear();
        allSelected = false;
        loadDataInController();
    }

    public void filterByFrameContract(){
        allSelected = false;
        loadDataInController();
    }

    public void removeSelectedSims(){
        try{
            allSelected = false;
            //icicdFilter = '';
            selectedSims = '';
            loadDataInController();
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, ex.getMessage()));
        }
    }

    public void terminateSelectedSims(){
        try{
            allSelected = false;
            if (selectedSims == null || String.isEmpty(selectedSims)){
                errorMsgs = 'Please add the sims before terminating';
            }
            else{
                errorMsgs = '';
                String selectedSimsTmp = '';
                for (String s : selectedSims.split(',')){
                    if (String.isEmpty(selectedSimsTmp)){
                        selectedSimsTmp = '\'' + s + '\'';
                    }
                    else{
                        selectedSimsTmp += ', \'' + s + '\'';
                    }
                }
                String queryRequest = 'Select Id,Name,ICCID__c,MSISDN__c,IMSI__c,Activation_Date__c,Runtime__c,End_date__c,Termination_Status__c,'
                        +'BIC_Framework_Contract_No__c,Account__c, Commercial_Date__c, Potential_Termination_Date__c, IsProlonged__c, Special_Termination__c '
                        +'from stock__c '
                        +'where Account__c = \'' + ticketCase.AccountId + '\'' 
                                //Activation ready SIMS cannot be requested
                                //+'AND SM2M_Status__c IN (\'ACTIVE\', \'ACTIVATION_READY\')';
                                +'AND SM2M_Status__c IN (\'ACTIVE\', \'DEACTIVATED\')';
                queryRequest += ' AND IMSI__c IN ('+ selectedSimsTmp +')';
                queryRequest += ' LIMIT 9999';
                List<stock__c> simsToBeRequested = Database.query(queryRequest);
                if (simsToBeRequested != null && simsToBeRequested.size() > 0){
                    for (stock__c stock : simsToBeRequested){
                        Date endDate = stock.Potential_Termination_Date__c;
                        if (stock.IsProlonged__c){
                            //calculate if end date is before real Potential End Date
                            Date receiptDate = ticketCase.Sixt_Termination_date__c;
                            if ((receiptDate.monthsBetween(stock.Potential_Termination_Date__c) >= 15) && receiptDate.day() <= stock.Potential_Termination_Date__c.day()){
                                endDate = stock.Potential_Termination_Date__c.addMonths(-12);
                            }
                        }
                        Date endDateReal = endDate.addDays(-1);
                        stock.End_date__c = endDateReal;
                        stock.Potential_Termination_Date__c = endDateReal;
                        stock.Termination_Status__c = 'REQUESTED';
                        stock.Termination_Requested_Ticket__c = ticketCase.Id;
                        stock.Special_Termination__c = false;
                    }
                    update simsToBeRequested;
                }
                //icicdFilter = '';
                selectedSims = '';
                loadDataInController();
            }
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, ex.getMessage()));
        }
    }

    public void stopTerminationOfSelectedSims(){
        try{
            allSelected = false;
            if (selectedSims == null || String.isEmpty(selectedSims)){
                errorMsgs = 'Please add the sims before termination stopping';
            }
            else{
                errorMsgs = '';
                String selectedSimsTmp = '';
                for (String s : selectedSims.split(',')){
                    if (String.isEmpty(selectedSimsTmp)){
                        selectedSimsTmp = '\'' + s + '\'';
                    }
                    else{
                        selectedSimsTmp += ', \'' + s + '\'';
                    }
                }
                String queryRequest = 'Select Id,Name,ICCID__c,MSISDN__c,IMSI__c,Activation_Date__c,Runtime__c,End_date__c,Termination_Status__c,'
                        +'BIC_Framework_Contract_No__c,Account__c, Commercial_Date__c, Potential_Termination_Date__c, Special_Termination__c '
                        +'from stock__c '
                        +'where Account__c = \'' + ticketCase.AccountId + '\'' 
                                +'AND SM2M_Status__c IN (\'ACTIVE\', \'DEACTIVATED\')';
                queryRequest += ' AND IMSI__c IN ('+ selectedSimsTmp +')';
                queryRequest += ' LIMIT 9999';
                List<stock__c> simsToBeStopped = Database.query(queryRequest);
                if (simsToBeStopped != null && simsToBeStopped.size() > 0){
                    for (stock__c stock : simsToBeStopped){
                        stock.Termination_Status__c = '';
                        stock.Termination_Requested_Ticket__c = null;
                        stock.End_date__c = null;
                        stock.Special_Termination__c = false;
                    }
                    update simsToBeStopped;
                }
                //icicdFilter = '';
                selectedSims = '';
                loadDataInController();
            }
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, ex.getMessage()));
        }
    }

    public void specialTermination(){
        try{
            allSelected = false;
            if (selectedSims == null || String.isEmpty(selectedSims)){
                errorMsgs = 'Please add the sims before terminating';
            }
            else{
                errorMsgs = '';
                String selectedSimsTmp = '';
                for (String s : selectedSims.split(',')){
                    if (String.isEmpty(selectedSimsTmp)){
                        selectedSimsTmp = '\'' + s + '\'';
                    }
                    else{
                        selectedSimsTmp += ', \'' + s + '\'';
                    }
                }
                System.debug(selectedSimsTmp);
                String queryRequest = 'Select Id,Name,ICCID__c,MSISDN__c,IMSI__c,Activation_Date__c,Runtime__c,End_date__c,Termination_Status__c,'
                        +'BIC_Framework_Contract_No__c,Account__c, Commercial_Date__c, Potential_Termination_Date__c, IsProlonged__c, Special_Termination__c '
                        +'from stock__c '
                        +'where Account__c = \'' + ticketCase.AccountId + '\'' 
                                +'AND SM2M_Status__c IN (\'ACTIVE\', \'DEACTIVATED\')';
                queryRequest += ' AND IMSI__c IN ('+ selectedSimsTmp +')';
                queryRequest += ' LIMIT 9999';
                Date specialTerminationDate = helperCase.Sixt_Termination_date__c;

                List<stock__c> simsToBeSpecialTerminated = Database.query(queryRequest);
                if (simsToBeSpecialTerminated != null && simsToBeSpecialTerminated.size() > 0){
                    for (stock__c stock : simsToBeSpecialTerminated){
                        stock.End_date__c = specialTerminationDate;
                        stock.Special_Termination__c = true;
                        stock.Termination_Status__c = 'REQUESTED';
                        stock.Termination_Requested_Ticket__c = ticketCase.Id;
                    }
                    update simsToBeSpecialTerminated;
                }
                //icicdFilter = '';
                selectedSims = '';
                loadDataInController();
            }
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, ex.getMessage()));
        }
    }

    public void selectDeselectAllSims(){
        system.debug('selectAll ' + allSelected);
        if (allSelected){
            allSelected = false;
            selectedSims = '';
        }
        else{
            allSelected = true;
            String query = 'Select IMSI__c '
                        +'from stock__c '
                        +'where Account__c = \'' + ticketCase.AccountId + '\'' 
                                +'AND SM2M_Status__c IN (\'ACTIVE\', \'DEACTIVATED\')';

            if (iccIds != null && iccIds.size() > 0){
                String iccidsTmp = '';
                for (String iccid : iccids){
                    if (String.isEmpty(iccidsTmp) || iccidsTmp == '')
                        iccidsTmp = '\'' + iccid + '\'';
                    else
                        iccidsTmp += ', \'' + iccid + '\'';
                }
                query += ' AND (ICCID__c in (' + iccidsTmp + ') OR MSISDN__c in (' +  iccidsTmp + '))';
            }

            if (!String.isEmpty(searchFC))
                query += ' AND BIC_Framework_Contract_No__c = \'' + searchFC + '\'';

            if (!String.isEmpty(icicdFilter)){
                String tmpIds = '';
                for (String s : icicdFilter.split(',')){
                    if (String.isEmpty(tmpIds)){
                        tmpIds = '\'' + s.trim() + '\'';
                    }
                    else{
                        tmpIds += ', \'' + s.trim() + '\'';
                    }
                }
                query += ' AND (ICCID__c in (' + tmpIds + ') OR MSISDN__c in (' +  tmpIds + '))';
            }


            if (!String.isEmpty(simsToRemove)){
                String removedTmp = '';
                for (String s : simsToRemove.split(',')){
                    if (String.isEmpty(removedTmp)){
                        removedTmp = '\'' + s + '\'';
                    }
                    else{
                        removedTmp += ', \'' + s + '\'';
                    }
                }
                query += ' AND IMSI__c NOT IN ('+ removedTmp +')';
            }

            query += ' LIMIT 9999';

            List<Stock__c> stocks = Database.query(query);
            for (Stock__c stock : stocks){
                //simsToRemove = simsToRemove + ',' + stock.IMSI__c;
                if(String.isEmpty(selectedSims)){
                    selectedSims = stock.IMSI__c;
                }
                else
                    selectedSims = selectedSims + ',' + stock.IMSI__c;
            }

            Decimal noPages = (stdController.getResultSize() / stdController.getPageSize());
            noPages = Math.floor(noPages) + ((Math.mod(stdController.getResultSize(), stdController.getPageSize()) > 0) ? 1 : 0);
        }
    }

    public void filterSims(){
        try{
            allSelected = false;
            selectedSims = '';
            loadDataInController();
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, ex.getMessage()));
        }
    }

    /*****************************************
    *               Pagination               *
    ******************************************/
    public Boolean hasNext {
        get {
            return stdController.getHasNext();
        }
        set;
    }
    
    public Boolean hasPrevious {
        get {
            return stdController.getHasPrevious();
        }
        set;
    }

    public void pNext() {
        stdController.next();
    }

    public void pPrevious() {
        stdController.previous();
    }

    public void pFirst() {
        stdController.first();
    }
    
    public void pLast() {
        stdController.last();
    }

    public String pPageNumber{
        get{
            return String.valueOf(stdController.getPageNumber());
        }
        set;
    }
    
    public String pTotalNumberPages{
        get{
            Decimal noPages = (stdController.getResultSize() / stdController.getPageSize());
            noPages = Math.floor(noPages) + ((Math.mod(stdController.getResultSize(), stdController.getPageSize()) > 0) ? 1 : 0);
            return String.valueOf((Integer.valueOf(noPages)));
        }
        set;
    }
    /*****************************
    **      END OF Pagination    *
    *****************************/
    public void sortByEndDate(){
        hdnOrderBy = 'End_date__c';
        sort();
    }

    public void sortByRunTime(){
        hdnOrderBy = 'Runtime__c';
        sort();
    }

    public void sortByFc(){
        hdnOrderBy = 'BIC_Framework_Contract_No__c';
        sort();
    }

    public void sortByTerminationStatus(){
        hdnOrderBy = 'Termination_Status__c';
        sort();
    }

    public void sortByPotentialTerminationDate(){
        hdnOrderBy = 'Potential_Termination_Date__c';
        sort();
    }

    public void sortBySM2MStatus(){
        hdnOrderBy = 'SM2M_Status__c';
        sort();
    }

    private void sort(){
        String orderDirection = hdnOrderDirection;
        if (orderDirection == 'ASC')
            orderDirection = 'DESC';
        else
            orderDirection = 'ASC';
        hdnOrderDirection = orderDirection;
        //simsToRemove = '';
        loadDataInController();
    }


    /***EXPORT***/
    public String csvFileData {get; set;}
    public PageReference exportToExcel(){
        PageReference pageRef;
        try{
            if(stdController.getResultSize() > 0){
                prepareCSVFileData();
                pageRef = Page.SimsTobeTerminatedExcelNewPage;
                pageRef.setRedirect(false);
            }
            else{
                errorMsgs = 'Please add the sims before exporting';
                pageRef = null;
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
            pageRef = null;
        }
        return pageRef;
    }

    public void prepareCSVFileData(){
        string headerName = 'ICCID;MSISDN;IMSI;Activation Date;Commercial date;Runtime;End Date;Frame Contract;Termination Status;Potential Termination Date; SM2M Status';
        string rows;
        
        String query = 'Select Id,Name,ICCID__c,MSISDN__c,IMSI__c,Activation_Date__c,Runtime__c,End_date__c,Termination_Status__c,'
                        +'BIC_Framework_Contract_No__c,Account__c, Commercial_Date__c, Potential_Termination_Date__c, SM2M_Status__c '
                        +'from stock__c '
                        +'where Account__c = \'' + ticketCase.AccountId + '\'' 
                                +'AND SM2M_Status__c IN (\'ACTIVE\', \'DEACTIVATED\')';

        if (iccIds != null && iccIds.size() > 0){
            String iccidsTmp = '';
            for (String iccid : iccids){
                if (String.isEmpty(iccidsTmp) || iccidsTmp == '')
                    iccidsTmp = '\'' + iccid + '\'';
                else
                    iccidsTmp += ', \'' + iccid + '\'';
            }
            query += ' AND (ICCID__c in (' + iccidsTmp + ') OR MSISDN__c in (' +  iccidsTmp + '))';
        }

        if (!String.isEmpty(searchFC))
            query += ' AND BIC_Framework_Contract_No__c = \'' + searchFC + '\'';

        if (!String.isEmpty(icicdFilter)){
            String tmpIds = '';
            for (String s : icicdFilter.split(',')){
                if (String.isEmpty(tmpIds)){
                    tmpIds = '\'' + s.trim() + '\'';
                }
                else{
                    tmpIds += ', \'' + s.trim() + '\'';
                }
            }
            query += ' AND (ICCID__c in (' + tmpIds + ') OR MSISDN__c in (' +  tmpIds + '))';
        }


        if (!String.isEmpty(simsToRemove)){
            String removedTmp = '';
            for (String s : simsToRemove.split(',')){
                if (String.isEmpty(removedTmp)){
                    removedTmp = '\'' + s + '\'';
                }
                else{
                    removedTmp += ', \'' + s + '\'';
                }
            }
            query += ' AND IMSI__c NOT IN ('+ removedTmp +')';
        }
        
        query += ' LIMIT 9999';

        if(query != null && !String.isEmpty(query)){
            for(Stock__C sim : Database.query(query)){
                if(rows != null){
                    rows = rows + '\n';
                } 
                rows =  rows+ '="' + checkNullString(sim.ICCID__c)+'";'; //ICCID__c
                rows =  rows+ '="' + checkNullString(sim.MSISDN__c)+'";';//
                rows =  rows+ '="' + checkNullString(sim.IMSI__c)+'";';
                rows =  rows+ String.valueOf(formatDateTime(sim.Activation_Date__c))+';';
                rows =  rows+ String.valueOf(formatDateTime(sim.Commercial_Date__c))+';';
                rows =  rows+ checkNullString(string.valueOf(sim.Runtime__c))+';';
                rows =  rows+ String.valueOf(formatDateTime(sim.End_date__c))+';';
                rows =  rows+ String.valueOf(sim.BIC_Framework_Contract_No__c)+';';
                rows =  rows+ String.valueOf(sim.Termination_Status__c)+';';
                rows =  rows+ String.valueOf(formatDateTime(sim.Potential_Termination_Date__c))+';';
                rows =  rows+ String.valueOf(sim.SM2M_Status__c)+';';
            }
        }
        
        
        if(rows == null){
            rows = '';
        }
        
        csvFileData = headerName + '\n' + rows;
        
        csvFileData = csvFileData.replace('null','');
    }

    public static string formatDateTime(DateTime dt){
         string tmp = '';
         if(dt != null){
             tmp = dt.format('dd.MM.yyyy');
         }
         return tmp;
    }

    public string checkNullString(string obj){
        if(obj == null){
            obj = '';
        }
        return obj;
    }

    public void uploadSims(){
        try{
            iccIds = SIMDispatchFileParser.parseIccidAndMSISDNExportFile(fileBody,true);
            if(!iccIds.isEmpty()){
                System.debug(iccIds);
                /*for (String iccid : iccIds){
                    System.debug(iccid);
                    allSelected = false;
                }*/
                allSelected = false;
                loadDataInController();
            }
            else{
                System.debug('empty');
            }
        }catch(Exception e){
            if(e.getMessage().contains('Argument cannot be null')){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, 'Please select a file'));
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
            }
            System.debug(e.getMessage());
        }
    }

    /****TERMINATION LETTER****/
    public String frameContract {get;set;}
    public static final Map<Integer, String> germanMonthNames = new Map<Integer, String>{1 => 'Januar', 2 => 'Februar', 3 => 'März', 4 => 'April', 5 => 'Mai', 6 => 'Juni', 7 => 'Juli', 8 => 'August', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Dezember'};
    public Account acc {get;set;}
    public String terminationDate {get; set;}
    public PageReference downloadTerminationConfirmLetter(){
        PageReference pageRef = null;
        try{
            prepareTerminationConfirmationLetter();
            pageRef = Page.TerminationConfirmLetterInterNewPage;
            pageRef.setRedirect(false);
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
        return pageRef;
    }
    
    public void prepareTerminationConfirmationLetter(){
        try{
            if(ticketCase != null && ticketCase.AccountId != null){
                acc = [Select Id,Name,BillingCity,BillingCountry,BillingCountryCode,BillingPostalCode,BillingState,BillingStateCode,BillingStreet from Account where Id = :ticketCase.AccountId];
                terminationDate = convertDateToGermanFormat(System.today());
                dateOfReceipt = convertDateToGermanFormat(ticketCase.Sixt_Termination_date__c);
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
    }

    public List<List<SimWrapperForLetter>> getTerminatedSIMs(){
        List<List<SimWrapperForLetter>> terminatedSims = new List<List<SimWrapperForLetter>>();
        List<SimWrapperForLetter> tSims = new List<SimWrapperForLetter>();
        Set<String> frameContractSet = new Set<String>();
        String queryForLetter;
        try{
            if(ticketCase != null && ticketCase.AccountId != null){
                /*queryForLetter = 'Select BIC_Framework_Contract_No__c,ICCID__c,MSISDN__c,End_date__c from stock__c '
                +'where Account__c = \'' + ticketCase.AccountId  + '\' AND '
                +'Termination_Status__c = \'REQUESTED\' AND SM2M_Status__c IN (\'ACTIVE\', \'ACTIVATION_READY\')'
                +' AND Termination_Requested_Ticket__c = \'' + ticketCase.Id + '\'';*/
                queryForLetter = 'Select BIC_Framework_Contract_No__c,ICCID__c,MSISDN__c,End_date__c, SM2M_Status__c, '
                +' Commercial_Date__c, Activation_Date__c from stock__c '
                +'where Account__c = \'' + ticketCase.AccountId + '\'' 
                                +'AND SM2M_Status__c IN (\'ACTIVE\', \'ACTIVATION_READY\', \'DEACTIVATED\')';

                if (iccIds != null && iccIds.size() > 0){
                    String iccidsTmp = '';
                    for (String iccid : iccids){
                        if (String.isEmpty(iccidsTmp) || iccidsTmp == '')
                            iccidsTmp = '\'' + iccid + '\'';
                        else
                            iccidsTmp += ', \'' + iccid + '\'';
                    }
                    queryForLetter += ' AND (ICCID__c in (' + iccidsTmp + ') OR MSISDN__c in (' +  iccidsTmp + '))';
                }

                if (!String.isEmpty(searchFC))
                    queryForLetter += ' AND BIC_Framework_Contract_No__c = \'' + searchFC + '\'';

                if (!String.isEmpty(icicdFilter)){
                    String tmpIds = '';
                    for (String s : icicdFilter.split(',')){
                        if (String.isEmpty(tmpIds)){
                            tmpIds = '\'' + s.trim() + '\'';
                        }
                        else{
                            tmpIds += ', \'' + s.trim() + '\'';
                        }
                    }
                    queryForLetter += ' AND (ICCID__c in (' + tmpIds + ') OR MSISDN__c in (' +  tmpIds + '))';
                }


                if (!String.isEmpty(simsToRemove)){
                    String removedTmp = '';
                    for (String s : simsToRemove.split(',')){
                        if (String.isEmpty(removedTmp)){
                            removedTmp = '\'' + s + '\'';
                        }
                        else{
                            removedTmp += ', \'' + s + '\'';
                        }
                    }
                    queryForLetter += ' AND IMSI__c NOT IN ('+ removedTmp +')';
                }

                if(!String.isEmpty(queryForLetter)){
                    for(Stock__c sim : Database.query(queryForLetter)){
                        frameContractSet.add(sim.BIC_Framework_Contract_No__c);

                        String simTerminationDateText = 'Nicht bekannt, da Karte noch nicht aktviert.';
                        if (sim.SM2M_Status__c != 'ACTIVATION_READY')
                            simTerminationDateText = convertDateToGermanFormat(sim.End_date__c);

                        tSims.add(new SimWrapperForLetter(sim, simTerminationDateText));
                        if(tSims.size() == 1000){
                            terminatedSims.add(tSims);
                            tSims = new List<SimWrapperForLetter>();
                        }
                    }
                    terminatedSims.add(tSims);
                    System.debug(frameContractSet);
                    for (String fc : frameContractSet){
                        if (!String.isBlank(fc)){ 
                            if (String.isBlank(frameContract))
                                frameContract = fc;
                            else
                                frameContract += ', ' + fc;
                        }
                    }
                    System.debug('frame contract: ' + frameContract);
                }
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
        return terminatedSims;
    }

    public String convertDateToGermanFormat(Date reqDate){
        Integer day, month, year;
        String germanDate = '';
        try{
            if(reqDate != null){
                day = reqDate.day();
                month = reqDate.month();
                year = reqDate.year();
                if(!germanMonthNames.isEmpty() && germanMonthNames.containsKey(month)){
                    germanDate = day + '. ' + germanMonthNames.get(month) + ' ' + year;
                }
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
        return germanDate;
    }

    private UserRole getRoleByRoleId(Id roleId){
        UserRole role = [SELECT id, name from UserRole where id = :roleId];
        return role;
    }

    @TestVisible
    private Boolean isUserAllowedForSpecialTermination(Id idUser){
        Boolean isAllowed = false;
        Set<String> allowedUserAliasSet = new Set<String>();
        allowedUserAliasSet.add('dblug');
        allowedUserAliasSet.add('pdros');
        allowedUserAliasSet.add('czips');
        allowedUserAliasSet.add('buyan');
        allowedUserAliasSet.add('mbahc');
        List<User> userLst = [SELECT ID, NAME, ALIAS FROM USER WHERE ID = :idUser];
        if (userLst != null && userLst.size() > 0){
            User user = userLst[0];
            if (user != null && user.Alias != null)
            isAllowed = allowedUserAliasSet.contains(user.Alias);
        }

        //Petar Matkovic 16.3.2017. Added check if user is in 2nd level support
        //no need to check if user has rights already
        if (!isAllowed){
            List<Group> lstGroup = [select Id, Developername, Type, Name 
                                    from Group 
                                    where Name = 'M2M Service 2nd Level'];

            if (lstGroup != null && lstGroup.size()>0){
                List<Groupmember> lstGroupMember = [select UserOrGroupId, GroupId 
                                                    from groupmember 
                                                    where groupId in :lstGroup];

                if (lstGroupMember != null && lstGroupMember.size() > 0){
                    for (Groupmember tmpGroupMember : lstGroupMember){
                        if (tmpGroupMember.UserOrGroupId == idUser){
                            isAllowed = true;
                            break;
                        }
                    }
                }
            }
        }

        return isAllowed;
    }
}