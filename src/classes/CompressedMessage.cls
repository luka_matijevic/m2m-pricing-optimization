/**
 * @description Integration server can send a compressed result back. This class serves for JSON deserialization
 * of sucha a message
 * @author Ilija Pavlic
 */ 
public class CompressedMessage {
	
	// used for any naming purpose
	public String name {get; set;}

	// base64 encoded content
	public String content {get; set;}

	// media type of the result (e.g. zip)
	public String mediaType {get; set;}
}