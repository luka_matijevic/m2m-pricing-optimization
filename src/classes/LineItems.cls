public class LineItems {
	
	public static Id billingAccountId(csord__Service_Line_Item__c sli) {
		return sli.csord__Service__r.csord__Subscription__r.Billing_Account__c;
	}

	public static Id billingAccountId(csord__Order_Line_Item__c oli) {
		if (oli.billing_Account_id__c!=null)
	         return oli.billing_Account_id__c;
		else return oli.csord__Order__r.Order_Request__r.Billing_Account__c;
	
	}
	
	public static Boolean hasQuantity(csord__Order_Line_Item__c oli) {
		return oli.Quantity__c != null && oli.Quantity__c > 0;
	}

	public static Boolean hasQuantity(csord__Service_Line_Item__c sli) {
		return sli.Quantity__c != null && sli.Quantity__c > 0;
	}
	
	public static Boolean isFromOrderLineItem(Prebill_Line_Item__c pbli) {
		return pbli.Order_Line_Item__c != null;
	}
	
	public static Boolean isFromServiceLineItem(Prebill_Line_Item__c pbli) {
		return pbli.Service_Line_Item__c != null;
	}

	public static Id getM2MOrderRequest (Prebill_Line_Item__c pbli) {
		Id ordReqId = null;

		if (pbli != null)
		{
			if (pbli.Order_Line_Item__c != null)
			{
				if (pbli.Order_Line_Item__r.csord__Order__r.Order_Request__c != null)
				{
					ordReqId = pbli.Order_Line_Item__r.csord__Order__r.Order_Request__c;
				}
			} else if (pbli.Service_Line_Item__c != null) {
				
				if (pbli.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c != null)
				{
					ordReqId = pbli.Service_Line_Item__r.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c;
				}
			}
		}

		return ordReqId;
	}

	public static Boolean isConnectivitySubscription(Prebill_Line_Item__c pbli) {
		Boolean isConnectivity = false;

		if (pbli != null)
		{
			if (pbli.Service_Line_Item__c != null) {
				
				if (pbli.Service_Line_Item__r.csord__Service__r.csord__Subscription__c != null)
				{
					//csord__Identification__c
					csord__Subscription__c subs = pbli.Service_Line_Item__r.csord__Service__r.csord__Subscription__r;
					
					if (subs != null && subs.csord__Identification__c == Constants.PRODUCT_TYPE_CONNECTIVITY)
					{
						isConnectivity = true;
					}
				}
			}
		}

		return isConnectivity;
	}

	public static String returnM2MOrderRequestName (Prebill_Line_Item__c pbli) {
		String orName = '';

		if (pbli != null)
		{
			if (pbli.Service_Line_Item__c != null) {
				
				if (pbli.Service_Line_Item__r.csord__Order_Request__c != null)
				{
					csord__Order_Request__c orReq = pbli.Service_Line_Item__r.csord__Order_Request__r;

					orName = orReq.Name;
				}
			}
		}

		return orName;
	}
}