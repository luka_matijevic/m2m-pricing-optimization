/*------------------------------------------------------------------------------
    Attachments to frame contract
  ------------------------------------------------------------------------------*/
public class FrameContractPDFAttachmentsListCtrl 
{
	/*----------------------------------------------------------------------
	    PDF attachment wrapper
	  ----------------------------------------------------------------------*/
	public class PDFAttachment
	{
		public Attachment attachment { get; set; }
		public Boolean isActive { get; set; }
	}

	/*----------------------------------------------------------------------
	    Members
	  ----------------------------------------------------------------------*/
	Id recordId;
	transient PDFAttachment[] pdfAttachments = null;

	/*----------------------------------------------------------------------
	    Constructor
	  ----------------------------------------------------------------------*/
	public FrameContractPDFAttachmentsListCtrl(ApexPages.StandardController stdCtrl) 
	{
		this.recordId = stdCtrl.getId();
	}

	/*----------------------------------------------------------------------
	    Get pdf attachments
	  ----------------------------------------------------------------------*/
	public PDFAttachment[] getPDFAttachments()
	{
		if (pdfAttachments == null) 
		{
			Attachment[] attachments = 
				[SELECT Id, Name
				 FROM Attachment
				 WHERE ParentId = :this.recordId
				 AND ContentType LIKE '%pdf'
				 ORDER BY CreatedDate ASC];
			pdfAttachments = new List<PDFAttachment>();
			/*
			   Add attachments to wrapper
			*/
			PDFAttachment pdfAttachment = null;
			for (Attachment att : attachments)
			{
				pdfAttachment = new PDFAttachment();
				pdfAttachment.attachment = att;
				pdfAttachment.isActive = false;
				pdfAttachments.add(pdfAttachment);
			}
			/*
			   Set last to active
			*/
			if (pdfAttachment != null)
			{
				pdfAttachment.isActive = true;
			}
		}
		return pdfAttachments;
	}

	/*----------------------------------------------------------------------
	    Is empty attachment list
	  ----------------------------------------------------------------------*/
	public boolean getIsEmptyList() 
	{
		return getPDFAttachments().isEmpty();
	}
}