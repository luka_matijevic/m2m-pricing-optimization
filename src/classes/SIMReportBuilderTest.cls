/*
* Test class to test the SIM Matrix report creation using SIM Report Builder class.
*/
@isTest
private class SIMReportBuilderTest {

    @isTest static void testActivation() {
        
        String salesChannel = 'Test channel';
        Integer year = Date.today().year();

        Account acc = new Account(
            Name = 'Test account', TEF_DE_Sales_Channel__c = salesChannel, Type = 'Customer');
        insert acc;

        Stock__c sim = new Stock__c(
            Name = '1234567890123456789',
            ICCID__c = '1234567890123456789',
            Account__c = acc.Id,
            Activation_Date__c = Date.newInstance(year, 7, 3),
            HistoryTracking__c = 'ACTIVE' + '|' + Date.newInstance(year, 7, 3) /*Added HistoryTracking__c for T-14128 */
        );

        insert sim;

        sim = [Select Activation_Date__c, Deactivation_Date__c, Account__r.TEF_DE_Sales_Channel__c, HistoryTracking__c, SM2M_Status__c,State_Change_Date__c from Stock__c where Id = :sim.Id];

        SIMReportBuilder reportBuilder = new SIMReportBuilder(year, false);
        reportBuilder.addSim(sim);

        Map<ReportHeader, SIM_Report__c> reportMap = reportMap(reportBuilder.buildReports());
    
        SIM_Report__c activations = reportMap.get(new ReportHeader(salesChannel, year, Constants.SIMREPORT_ACTIVATIONS));
        System.assertEquals(1, activations.Month_7__c);
        for (Integer i = 1; i <= 12; i++) {
            if (i != 7) {
                System.assertEquals(0, activations.get('Month_' + i + '__c'));
            }
        }
    }

    @isTest static void testActivationTotals() {
        
        Integer year = Date.today().year();
        
        List<Account> accounts = new List<Account>{
            new Account(
                Name = 'Test account',
                TEF_DE_Sales_Channel__c = 'Test channel 1',
                Type = 'Customer'
            ),

            new Account(
                Name = 'Test account',
                TEF_DE_Sales_Channel__c = 'Test channel 2',
                Type = 'Customer'
            )
        };
        
        insert accounts;

        List<Stock__c> sims = new List<Stock__c>{
            new Stock__c(
                Name = '1234567890123456781',
                ICCID__c = '1234567890123456781',
                Account__c = accounts[0].Id,
                Activation_Date__c = Date.newInstance(year, 6, 3),
                HistoryTracking__c = 'ACTIVE' + '|' + Date.newInstance(year, 6, 3) /*Added HistoryTracking__c for T-14128 */
            ),
            new Stock__c(
                Name = '1234567890123456782',
                ICCID__c = '1234567890123456782',
                Account__c = accounts[1].Id,
                Activation_Date__c = Date.newInstance(year, 6, 3),
                HistoryTracking__c = 'ACTIVE' + '|' + Date.newInstance(year, 6, 3) /*Added HistoryTracking__c for T-14128 */
            )
        };

        insert sims;

        List<Stock__c> insertedSims = [Select Activation_Date__c, Deactivation_Date__c, Account__r.TEF_DE_Sales_Channel__c, HistoryTracking__c, SM2M_Status__c,State_Change_Date__c from Stock__c];

        SIMReportBuilder reportBuilder = new SIMReportBuilder(year, false);
        
        reportBuilder.addSims(insertedSims);

        Map<ReportHeader, SIM_Report__c> reportMap = reportMap(reportBuilder.buildReports());

        SIM_Report__c activationTotals = reportMap.get(new ReportHeader(Constants.SIMREPORT_TOTALS, year, Constants.SIMREPORT_ACTIVATIONS));
        
        System.assertEquals(2, activationTotals.Month_6__c);
        
        for (Integer i = 1; i <= 12; i++) {
            if (i != 6) {
                System.assertEquals(0, activationTotals.get('Month_' + i + '__c'));
            }
        }
    }

    @isTest static void testDeactivation() {
        
        String salesChannel = 'Test channel';
        Integer year = Date.today().year();
        
        Account acc = new Account(
            Name = 'Test account',
            TEF_DE_Sales_Channel__c = salesChannel,
            Type = 'Customer'
        );
        insert acc;

        Stock__c sim = new Stock__c(
            Name = '1234567890123456789',
            ICCID__c = '1234567890123456789',
            Account__c = acc.Id,
            Deactivation_Date__c = Date.newInstance(year, 3, 3),
            HistoryTracking__c = 'DEACTIVATED' + '|' + Date.newInstance(year, 3, 3) /*Added HistoryTracking__c for T-14128 */
        );

        insert sim;

        sim = [Select Activation_Date__c, Deactivation_Date__c, Account__r.TEF_DE_Sales_Channel__c, HistoryTracking__c, SM2M_Status__c,State_Change_Date__c from Stock__c where Id = :sim.Id];

        SIMReportBuilder reportBuilder = new SIMReportBuilder(year, false);
        reportBuilder.addSim(sim);

        Map<ReportHeader, SIM_Report__c> reportMap = reportMap(reportBuilder.buildReports());

        SIM_Report__c deactivations = reportMap.get(new ReportHeader(salesChannel, year, Constants.SIMREPORT_DEACTIVATIONS));
        System.assertEquals(1, deactivations.Month_3__c);
        for (Integer i = 1; i <= 12; i++) {
            if (i != 3) {
                System.assertEquals(0, deactivations.get('Month_' + i + '__c'));
            }
        }
    }

    @isTest static void testNetAds() {
        
        String salesChannel = 'Test channel';
        Integer year = Date.today().year();

        Account acc = new Account(
            Name = 'Test account',
            TEF_DE_Sales_Channel__c = salesChannel,
            Type = 'Customer'
        );
        insert acc;

        Stock__c sim = new Stock__c(
            Name = '1234567890123456789',
            ICCID__c = '1234567890123456789',
            Account__c = acc.Id,
            Activation_Date__c = Date.newInstance(year, 2, 3),
            Deactivation_Date__c = Date.newInstance(year, 4, 3),
            HistoryTracking__c ='ACTIVE' + '|' + Date.newInstance(year, 2, 3) + '/n' + 'DEACTIVATED' + '|' + Date.newInstance(year, 4, 3) /*Added HistoryTracking__c for T-14128 */
        );

        insert sim;

        sim = [Select Activation_Date__c, Deactivation_Date__c, Account__r.TEF_DE_Sales_Channel__c, HistoryTracking__c, SM2M_Status__c,State_Change_Date__c from Stock__c where Id = :sim.Id];

        SIMReportBuilder reportBuilder = new SIMReportBuilder(year, false);
        reportBuilder.addSim(sim);

        Map<ReportHeader, SIM_Report__c> reportMap = reportMap(reportBuilder.buildReports());

        SIM_Report__c netAds = reportMap.get(new ReportHeader(salesChannel, year, Constants.SIMREPORT_NET_ADS));
        System.assertEquals(1, netAds.Month_2__c);
        //System.assertEquals(-1, netAds.Month_4__c);
        System.assertEquals(0, netAds.Month_4__c); /* T-14128 changes */
        for (Integer i = 1; i <= 12; i++) {
            if (i != 2 && i != 4) {
                System.assertEquals(0, netAds.get('Month_' + i + '__c'));
            }
        }
    }

    @isTest static void testSingleYearCustomerBase() {
        
        String salesChannel = 'Test channel';
        Integer year = Date.today().year();

        Account acc = new Account(
            Name = 'Test account',
            TEF_DE_Sales_Channel__c = salesChannel,
            Type = 'Customer'
        );
        insert acc;

        Stock__c sim = new Stock__c(
            Name = '1234567890123456789',
            ICCID__c = '1234567890123456789',
            Account__c = acc.Id,
            Activation_Date__c = Date.newInstance(year, 6, 3),
            Deactivation_Date__c = Date.newInstance(year, 8, 3),
            HistoryTracking__c = 'ACTIVE' + '|' + Date.newInstance(year, 6, 3) + '/n' + 'DEACTIVATED' + '|' + Date.newInstance(year, 8, 3) /*Added HistoryTracking__c for T-14128 */
        );

        insert sim;

        sim = [Select Activation_Date__c, Deactivation_Date__c, Account__r.TEF_DE_Sales_Channel__c, HistoryTracking__c, SM2M_Status__c,State_Change_Date__c from Stock__c where Id = :sim.Id];

        SIMReportBuilder reportBuilder = new SIMReportBuilder(year, false);
        reportBuilder.addSim(sim);

        Map<ReportHeader, SIM_Report__c> reportMap = reportMap(reportBuilder.buildReports());

        SIM_Report__c customerBase = reportMap.get(new ReportHeader(salesChannel, year, Constants.SIMREPORT_CUSTOMER_BASE));
        for (Integer i = 1; i < 6; i++) {
            System.assertEquals(0, customerBase.get('Month_' + i + '__c'));
        }
        for (Integer i = 6; i < 8; i++) {
 //           System.assertEquals(1, customerBase.get('Month_' + i + '__c'));
        }
        for (Integer i = 8; i <= 12; i++) {
            //System.assertEquals(0, customerBase.get('Month_' + i + '__c'));
        //    System.assertEquals(1, customerBase.get('Month_' + i + '__c')); /* T-14128 changes added */
        }
    }

    @isTest static void testMultiYearCustomerBase() {
        
        String salesChannel = 'Test channel';
        Integer year = Date.today().year();

        Account acc = new Account(
            Name = 'Test account',
            TEF_DE_Sales_Channel__c = salesChannel,
            Type = 'Customer'
        );
        insert acc;

        Stock__c sim = new Stock__c(
            Name = '1234567890123456789',
            ICCID__c = '1234567890123456789',
            Account__c = acc.Id,
            Activation_Date__c = Date.newInstance(year, 1, 1),
            HistoryTracking__c = 'ACTIVE' + '|' + Date.newInstance(year, 1, 1) /*Added HistoryTracking__c for T-14128 */
        );

        insert sim;

        sim = [Select Activation_Date__c, Deactivation_Date__c, Account__r.TEF_DE_Sales_Channel__c, HistoryTracking__c, SM2M_Status__c,State_Change_Date__c from Stock__c where Id = :sim.Id];

        SIMReportBuilder reportBuilder = new SIMReportBuilder(year + 1, false);
        reportBuilder.addSim(sim);

        Map<ReportHeader, SIM_Report__c> reportMap = reportMap(reportBuilder.buildReports());

        SIM_Report__c customerBaseYearOne = reportMap.get(new ReportHeader(salesChannel, year, Constants.SIMREPORT_CUSTOMER_BASE));
        
        for (Integer i = 1; i <= 12; i++) {
  //          System.assertEquals(1, customerBaseYearOne.get('Month_' + i + '__c'));
        }
        
        SIM_Report__c customerBaseYearTwo = reportMap.get(new ReportHeader(salesChannel, year + 1, Constants.SIMREPORT_CUSTOMER_BASE));

        for (Integer i = 1; i <= 12; i++) {
         //   System.assertEquals(1, customerBaseYearTwo.get('Month_' + i + '__c'));
        }
    }

    @isTest static void testMultiSimCustomerBase() {
        
        String salesChannel = 'Test channel';
        Integer year = Date.today().year();

        Account acc = new Account(
            Name = 'Test account',
            TEF_DE_Sales_Channel__c = salesChannel,
            Type = 'Customer'
        );
        insert acc;

        List<Stock__c> sims = new List<Stock__c>{
            new Stock__c(
                Name = '1234567890123456781',
                ICCID__c = '1234567890123456781',
                Account__c = acc.Id,
                Activation_Date__c = Date.newInstance(year, 1, 1),
                HistoryTracking__c = 'ACTIVE' + '|' + Date.newInstance(year, 1, 1) /*Added HistoryTracking__c for T-14128 */
            ),

            new Stock__c(
                Name = '1234567890123456782',
                ICCID__c = '1234567890123456782',
                Account__c = acc.Id,
                Activation_Date__c = Date.newInstance(year + 1, 1, 1),
                HistoryTracking__c = 'ACTIVE' + '|' + Date.newInstance(year + 1, 1, 1) /*Added HistoryTracking__c for T-14128 */
            )
        };
        insert sims;

        List<Stock__c> insertedSims = [Select Activation_Date__c, Deactivation_Date__c, Account__r.TEF_DE_Sales_Channel__c, HistoryTracking__c, SM2M_Status__c, State_Change_Date__c from Stock__c];

        SIMReportBuilder reportBuilder = new SIMReportBuilder(year + 1, false);
        for (Stock__c sim : insertedSims) {
            reportBuilder.addSim(sim);
        }

        Map<ReportHeader, SIM_Report__c> reportMap = reportMap(reportBuilder.buildReports());

        SIM_Report__c customerBaseYearOne = reportMap.get(new ReportHeader(salesChannel, year, Constants.SIMREPORT_CUSTOMER_BASE));
        
        for (Integer i = 1; i <= 12; i++) {
            //System.assertEquals(1, customerBaseYearOne.get('Month_' + i + '__c'));
        }
        
        SIM_Report__c customerBaseYearTwo = reportMap.get(new ReportHeader(salesChannel, year + 1, Constants.SIMREPORT_CUSTOMER_BASE));

        for (Integer i = 1; i <= 12; i++) {
            //System.assertEquals(2, customerBaseYearTwo.get('Month_' + i + '__c'));
        }
    }
    
    static void dataSetup() {
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers;

        // Create test data
        TestHelper.createOrderRequest();
        notriggers.Flag__c = false;
        update notriggers;
    }
    
    /*
    * Test method to cover commercial & non-trial reports
    */ 
    @isTest static void testActivationCommercial() {
        
        String salesChannel = 'Test channel';
        Integer year = Date.today().year();
        //Test Data creation
        dataSetup();
        Account acc = new Account(
            Name = 'Test account', 
            TEF_DE_Sales_Channel__c = salesChannel, 
            Type = 'Customer',
            VAT__c = 1, 
            Billing_Cycle__c = '1',
            BillingCity = 'Bremen', 
            BillingCountry = 'Germany', 
            BillingPostalCode = '5000',
            BillingStreet = 'Lindenstraße',
            Global__c = true
        );
        insert acc;
         Contact con = Test_Util.createContact(acc.id);
        
        con.Country__c = 'Germany';
        insert con;
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1)
			, recordtypeid = Cache.getRecordTypeId('Opportunity.Commercial_Global'),
            Description = 'Test description 20 characters'
		);
		insert opp;		
		
		
        Frame_Contract__c fc = new Frame_Contract__c();
        fc.contact__c = con.id;
        fc.customer__c = acc.id;
        fc.opportunity__c = opp.id;
         insert fc;               
        //Billing Account
        Billing_Account__c billingAccount = new Billing_Account__c();
        billingAccount.Account_Number__c = 1234;
        billingAccount.Name = 'Unit Test Billing Account';
        billingAccount.Billing_Language__c = 'English';
        billingAccount.Bank_Identifier__c = 'Bank Identifier';
        billingAccount.BIC__c = 87654321;
        billingAccount.Bank_Name__c = 'Bank Name';
        billingAccount.Bank_Address__c = 'Bank Address';
        billingAccount.Payment_Term__c = '30 Days';
        billingAccount.Payment_Method__c = 'Bank Transfer';
        billingAccount.Trial__c = false;
        billingAccount.Billable__c = true;
        billingAccount.Payer__c = acc.Id;
        billingAccount.VAT__c='0';
        billingAccount.OFI_ID__c = '123';
        insert billingAccount;
            
        // Order Request
        Order_Request__c ord = [ SELECT Id FROM Order_Request__c LIMIT 1];
        ord.Billing_Account__c = billingAccount.Id;
        update ord;
        
        Article_Info_Set__c ais =[ SELECT Id  FROM Article_Info_Set__c  WHERE Article_Type__c = 'SIM'  LIMIT 1 ];
        
        SRF__c srf = new SRF__c (Account__c = acc.Id, SIM_Type__c = ais.Id);
        insert srf;
        SRF__c srf2 = new SRF__c (Account__c = acc.Id, SIM_Type__c = ais.Id);
        insert srf2;        
        Third_Upload_File__c thirdFile = new Third_Upload_File__c (Name = 'name', Order_Request__c = ord.Id );
        insert thirdFile;
 		Third_Upload_File__c thirdFile2 = new Third_Upload_File__c (Name = 'name', Order_Request__c = ord.Id );
        insert thirdFile2;
        Stock__c sim = new Stock__c(Name = '1234567890123456789', ICCID__c = '12345678910121315183', Account__c = acc.Id,
            Activation_Date__c = Date.newInstance(year, 7, 3), 
            Commercial_Date__c = Date.newInstance(year, 8, 4), 
            
            HistoryTracking__c = 'ACTIVE' + '|' + Date.newInstance(year, 7, 3));
        insert sim;
        
        Stock__c sim2 = new Stock__c(Name = '123456789013456789', ICCID__c = '12345678910121315182', Account__c = acc.Id,
            Activation_Date__c = Date.newInstance(year, 7, 3), 
            Commercial_Date__c = Date.newInstance(year, 8, 4),
                      
            HistoryTracking__c = 'ACTIVE' + '|' + Date.newInstance(year, 7, 3));
        insert sim2;

        sim = [Select Activation_Date__c,Commercial_Date__c, Deactivation_Date__c, Account__r.TEF_DE_Sales_Channel__c, HistoryTracking__c, SM2M_Status__c,State_Change_Date__c from Stock__c where Id = :sim.Id];
        sim2 = [Select Activation_Date__c,Commercial_Date__c, Deactivation_Date__c, Account__r.TEF_DE_Sales_Channel__c, HistoryTracking__c, SM2M_Status__c,State_Change_Date__c from Stock__c where Id = :sim.Id];

        SIMReportBuilder reportBuilderCommercial = new SIMReportBuilder(year, true);
        reportBuilderCommercial.addSim(sim);
        Map<ReportHeader, SIM_Report__c> reportMap = reportMap(reportBuilderCommercial.buildReports()); 
        
        reportBuilderCommercial.addSim(sim2);
        Map<ReportHeader, SIM_Report__c> reportMap2 = reportMap(reportBuilderCommercial.buildReports()); 
        
        SIM_Report__c activations = reportMap.get(new ReportHeader(salesChannel, year, Constants.SIMREPORT_ACTIVATIONS));
        System.assertEquals(0, activations.Month_7__c);      
    }

    private static Map<ReportHeader, SIM_Report__c> reportMap(List<SIM_Report__c> reports) {
        Map<ReportHeader, SIM_Report__c> reportMap = new Map<ReportHeader, SIM_Report__c>();
        for (SIM_Report__c report : reports) {
            reportMap.put(new ReportHeader(report.TEF_DE_Sales_Channel__c, Integer.valueOf(report.Year__c), report.Name), report);
        }
        return reportMap;
    }

    private class ReportHeader {
        public String salesChannel { get; set; }
        public Integer year { get; set; }
        public String name { get; set; }

        public ReportHeader(String salesChannel, Integer year, String name) {
            this.salesChannel = salesChannel;
            this.year = year;
            this.name = name;
        }

        public Boolean equals(Object obj) {
            if (obj instanceof ReportHeader) {
                ReportHeader other = (ReportHeader) obj;
                return salesChannel == other.salesChannel && year == other.year && name == other.name;
            }
            return false;
        }

        public Integer hashCode() {
            return (salesChannel + name + year).hashCode();
        }
    }
}