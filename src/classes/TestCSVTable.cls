//------------------------------------------------------------------------------
// CSV Table
//------------------------------------------------------------------------------
@isTest
private class TestCSVTable {

    static testMethod void createCSVString()
    {
        List<IRow> rows = new List<IRow>();
        rows.add(
        		new StringRow(
        				new List<String>{'First; Name', 'Last\nName'}));
        rows.add(
        		new StringRow(
        				new List<String>{'Jur"o', 'Mecko'}));
        rows.add(
        		new StringRow(
        				new List<String>{null, ''}));
        CSVTable csvTable = new CSVTable(rows, ';', '\n');
        System.assertEquals(AttachmentUtils.csvBOM() + '"First; Name";"Last\nName"\n"Jur""o";Mecko\n;\n', csvTable.generate());
        csvTable = new CSVTable(rows, ',', '\r');
        System.assertEquals(AttachmentUtils.csvBOM() + 'First; Name,"Last\nName"\r"Jur""o",Mecko\r,\r', csvTable.generate());
    }
}