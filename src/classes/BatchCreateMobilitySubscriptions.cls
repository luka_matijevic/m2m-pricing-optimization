/*
	Batch class that creates subscriptions for today activated sims on orders of type mobility
	Author: P. Matkovic
*/
global class BatchCreateMobilitySubscriptions implements Database.Batchable<sObject> {
	String query;
	List<Order_Request__c> orderRequestList;
	List<Stock__c> todayActivatedStocks;
	Map<Id, List<Stock__c>> ordReqStockMap;
	Map<Id, List<Cscfga__Attribute__c>> ordReqIdAttMap;
	Map<Id, List<csord__service__c>> orderReqServiceMap;
	Map<Id, List<csord__Service_Line_Item__c>> serviceServiceLineItemMap;
	Set<Id> ordReqIds;
	Map<Id, csord__Subscription__c> ordReqSubscriptionMapMain;
	Map<Id, csord__Order__c> ordReqOrderMap;
	Set<Id> ordReqNewOrder = new Set<Id>();
	GEOTAB_Settings__c geotabSettings = GEOTAB_Settings__c.getInstance();
	Date dateforQuery = null;
	Map<Id, Csord__service__c> serviceMap;

	List<Csord__service__c> servicesToUpdate = new List<Csord__Service__c>();
	List<Csord__Order_Line_Item__c> hardwareOneTimeFeeToInsert = new List<Csord__Order_Line_Item__c>();
	List<csord__service_Line_Item__c> serviceLineItemsToUpdate = new List<csord__service_Line_Item__c>();
	Map<csord__Service__c, List<csord__Service_Line_Item__c>> newServiceServiceLineItemMap = new Map<csord__Service__c, List<csord__Service_Line_Item__c>>();
	global BatchCreateMobilitySubscriptions() {
		
	}

	global BatchCreateMobilitySubscriptions(Date dateforQueryIn){
		dateforQuery = dateforQueryIn;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		String oppMobilityType = 'SIXT';
		query = 'SELECT id, Account__c, activation_date__c, iccid__c, sm2m_status__c, order_request__c, order_request__r.Opportunity__c '
				+'FROM stock__c '
				+'WHERE order_request__r.RecordType.Id = \'' + geotabSettings.Order_Request_Type_Id__c + '\''
				+' AND GEOTAB_Processed__c = false';
		if (dateforQuery == null){
			query = query + ' AND DAY_ONLY(first_activation_date__c) = TODAY';
		}
		else{
			query = query + ' AND DAY_ONLY(first_activation_date__c) = ' + String.valueOf(dateforQuery);
		}
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		List<Stock__c> stockList = (List<Stock__c>)scope;
   		processTodaysStocks(stockList);
   		//extractOrderRequestIdsFromStocks(todayActivatedStocks);
		getOrderRequestsAndAttributes(ordReqIds);
		getSubscriptionsOrCreateNewForOrderRequests(ordReqIds);

		createOrUpdateServicesForOrderRequests();

		List<ServiceServiceLineItemHelper> insertHelperList = new List<ServiceServiceLineItemHelper>();
		for (Csord__service__c service : newServiceServiceLineItemMap.keySet()){
			insertHelperList.add(new ServiceServiceLineItemHelper(service, newServiceServiceLineItemMap.get(service)));
		}

		List<Csord__service__c> serviceToInsertList = new List<Csord__service__c>();

		for (ServiceServiceLineItemHelper sslih : insertHelperList){
			serviceToInsertList.add(sslih.service);
		}

		upsert serviceToInsertList;

		List<Csord__service_Line_Item__c> serviceLineItemToInsertList = new List<Csord__service_Line_Item__c>();
		Integer counter = 0;
		for (Csord__service__c service : serviceToInsertList){
			ServiceServiceLineItemHelper sslihTmp = insertHelperList[counter];
			for (Csord__service_Line_Item__c serviceLineItemhelperTmp : sslihTmp.serviceLineItemList){
				serviceLineItemhelperTmp.Csord__service__c = service.Id;
			}
			serviceLineItemToInsertList.addAll(sslihTmp.serviceLineItemList);
			counter++;
		}
		upsert serviceLineItemToInsertList;

		upsert serviceLineItemsToUpdate;

		upsert hardwareOneTimeFeeToInsert;

		upsert stockList;
	}
	
	global void finish(Database.BatchableContext BC) {

	}

	private void processTodaysStocks(List<Stock__c> stockList){
		Map<Id, List<Stock__c>> ordReqStockMapTmp = new Map<Id, List<Stock__c>>();
		ordReqIds = new Set<Id>();
		for (Stock__c stock : stockList){
			List<Stock__c> stockListTmp;
			if (ordReqStockMapTmp.get(stock.order_request__c) == null){
				stockListTmp = new List<Stock__c>();
			}
			else{
				stockListTmp = ordReqStockMapTmp.get(stock.order_request__c);
			}
			ordReqIds.add(stock.Order_Request__c);
			stock.GEOTAB_Processed__c = true;
			stockListTmp.add(stock);
			ordReqStockMapTmp.put(stock.order_request__c, stockListTmp);
		}
		todayActivatedStocks = stockList;
		ordReqStockMap = ordReqStockMapTmp;
	}

	private void extractOrderRequestIdsFromStocks(List<Stock__c> stocks){
		ordReqIds = new Set<Id>();
		for (Stock__c stock : stocks){
			ordReqIds.add(stock.Order_Request__c);
		}
	}

	private void getOrderRequestsAndAttributes(Set<Id> orderRequestIds){
		List<Order_Request__c> orderRequestListTmp = [SELECT id, name, order_quantity__c, Runtime__c
													from order_request__c where id in :orderRequestIds];

		List<Product_Configuration_Order__c> productConfOrderList = [SELECT id, Product_Order__r.Order_Request__c, Product_Configuration__c 
																from Product_Configuration_Order__c
																where Product_Order__r.Order_Request__c in :orderRequestIds];
		Map<Id, Id> confOrdReqIdMap = new Map<Id, Id>();
		List<Id> confIdList = new List<Id>();
		for (Product_Configuration_Order__c pco : productConfOrderList){
			confIdList.add(pco.Product_Configuration__c);
			confOrdReqIdMap.put(pco.Product_Configuration__c, pco.Product_Order__r.Order_Request__c);
		}

		List<Cscfga__Attribute__c> attributes = [SELECT id, name, cscfga__value__c, cscfga__Price__c, cscfga__Product_Configuration__c
												from Cscfga__Attribute__c
												where cscfga__Product_Configuration__c in :confIdList];

		Map<Id, List<Cscfga__Attribute__c>> ordReqAttributeMap = new Map<Id, List<Cscfga__Attribute__c>>();
		for (Cscfga__Attribute__c att : attributes){
			Id attOrdReqId = confOrdReqIdMap.get(att.cscfga__Product_Configuration__c);
			List<Cscfga__Attribute__c> attList;
			if (ordReqAttributeMap.get(attOrdReqId) == null){
				//no previous entries
				attList = new List<Cscfga__Attribute__c>();
			}
			else{
				//entry exists
				attList = ordReqAttributeMap.get(attOrdReqId);
			}
			attList.add(att);
			ordReqAttributeMap.put(attOrdReqId, attList);
		}
		orderRequestList = orderRequestListTmp;
		ordReqIdAttMap = ordReqAttributeMap;
	}

	private void getSubscriptionsOrCreateNewForOrderRequests(Set<Id> orderRequestIds){
		List<csord__Subscription__c> subscriptionList = [SELECT id, name, csord__Order__r.Order_Request__c 
														from csord__Subscription__c 
														where csord__Order__r.Order_Request__c in :orderRequestIds];
		Set<Id> subscriptionIds = new Set<Id>();
		Map<Id, csord__Subscription__c> ordReqSubscriptionMap = new Map<Id, csord__Subscription__c>();
		for (csord__Subscription__c sub : subscriptionList){
			ordReqSubscriptionMap.put(sub.csord__Order__r.Order_Request__c, sub);
			subscriptionIds.add(sub.Id);
		}

		//check if there is order for each order request. If not, create order and subscription from factory code
		for (Id ordReqId : orderRequestIds){
			if (ordReqSubscriptionMap.get(ordReqId) == null){
				Factory.createCreateSubscriptions(Factory.createCreateOrder(ordReqId).create()).create();
				ordReqNewOrder.add(ordReqId);
			}
		}

		//Fetch all again
		subscriptionList = [SELECT id, name, csord__Order__r.Order_Request__c, csord__order_request__c, csord__Order__c
							from csord__Subscription__c 
							where csord__Order__r.Order_Request__c in :orderRequestIds];

		subscriptionIds = new Set<Id>();
		ordReqSubscriptionMap = new Map<Id, csord__Subscription__c>();
		for (csord__Subscription__c sub : subscriptionList){
			ordReqSubscriptionMap.put(sub.csord__Order__r.Order_Request__c, sub);
			subscriptionIds.add(sub.Id);
		}

		//at this point ordReqSubscriptionMap contains subscription for every Order REquest
		ordReqSubscriptionMapMain = ordReqSubscriptionMap;

		//Fetch all orders
		List<csord__Order__c> orderList = [SELECT ID, Order_Request__c
											FROM csord__Order__c
											WHERE Order_Request__c in :orderRequestIds];
		ordReqOrderMap = new Map<Id, csord__Order__c>();
		for (csord__Order__c order : orderList){
			ordReqOrderMap.put(order.order_request__c, order);
		}

		List<csord__service__c> serviceList = [SELECT id, csord__Activation_Date__c, 
														csord__subscription__c, 
														csord__subscription__r.csord__Order__r.Order_Request__c,
														GEOTAB_Quantity__c
												FROM csord__service__c
												WHERE csord__subscription__c in :subscriptionIds];
		Map<Id, List<csord__service__c>> orderReqServiceMapTmp = new Map<Id, List<csord__service__c>>();
		for (csord__service__c service : serviceList){
			serviceMap.put(service.Id, service);
			List<csord__service__c> services;
			if (orderReqServiceMapTmp.get(service.csord__subscription__r.csord__Order__r.Order_Request__c) == null){
				services = new List<csord__service__c>();
			}
			else{
				services = orderReqServiceMapTmp.get(service.csord__subscription__r.csord__Order__r.Order_Request__c);
			}
			services.add(service);
			orderReqServiceMapTmp.put(service.csord__subscription__r.csord__Order__r.Order_Request__c, services);
		}

		orderReqServiceMap = orderReqServiceMapTmp;

		List<csord__Service_Line_Item__c> serviceLineItemList = [SELECT id, csord__Service__c, Is_Billed__c, Total_Price__c, Csord__Identification__c
																FROM csord__Service_Line_Item__c
																WHERE csord__Service__c in :serviceList];
		Map<Id, List<csord__Service_Line_Item__c>> serviceServiceLineItemMapTmp = new Map<Id, List<csord__Service_Line_Item__c>>();
		for (csord__Service_Line_Item__c sli : serviceLineItemList){
			List<csord__Service_Line_Item__c> listTmp;
			if (serviceServiceLineItemMapTmp.get(sli.csord__Service__c) == null){
				listTmp = new List<csord__Service_Line_Item__c>();
			}
			else{
				listTmp = serviceServiceLineItemMapTmp.get(sli.csord__Service__c);
			}
			listTmp.add(sli);
			serviceServiceLineItemMapTmp.put(sli.csord__Service__c, listTmp);
		}

		serviceServiceLineItemMap = serviceServiceLineItemMapTmp;
	}

	private void createOrUpdateServicesForOrderRequests(){
		for (Order_Request__c orderRequest : orderRequestList){
			createOrUpdateServicesForOrderRequest(orderRequest);
		}
	}

	private void createOrUpdateServicesForOrderRequest(Order_Request__c orderRequest){
		List<Stock__c> orderRequestStocks = ordReqStockMap.get(orderRequest.Id);
		Integer orderRequestQuantity = Integer.valueOf(orderRequest.Order_quantity__c);
		Decimal orderRequestVasMonthlyPrice;
		Decimal orderRequestHardwareMonthlyPrice;
		Decimal orderRequestHardwareOneTimeFee;
		Integer orderRequestRuntime = Integer.valueOf(orderRequest.Runtime__c);
		Boolean isHardwareRecurring = false;
		for(Cscfga__Attribute__c att : ordReqIdAttMap.get(orderRequest.Id)){
			if (att.Name == 'VAS Monthly Fee'){
				orderRequestVasMonthlyPrice = Decimal.valueOf(String.ValueOf(att.cscfga__Value__c));
			}
			if (att.Name == 'Hardware Monthly Fee'){
				orderRequestHardwareMonthlyPrice = Decimal.valueOf(String.ValueOf(att.cscfga__Value__c));
			}
			if (att.Name == 'Hardware One Time Fee TCV'){
				orderRequestHardwareOneTimeFee = Decimal.valueOf(String.ValueOf(att.cscfga__Value__c));
			}
			if (att.Name == 'Recurring Hardware Fee'){
				if (String.ValueOf(att.cscfga__Value__c) == 'Yes'){
					isHardwareRecurring = true;
				}
			}
		}

		//create map of dates and sims for that Order Req
		Map<Date, List<Stock__c>> dateStockMap = new Map<Date, List<Stock__c>>();
		for (Stock__c stock : orderRequestStocks){
			Date dateTmp = Date.newInstance(stock.Activation_Date__c.year(), stock.Activation_Date__c.month(), stock.Activation_Date__c.day());
			List<Stock__c> stocksTmp;
			if (dateStockMap.get(dateTmp) == null){
				stocksTmp = new List<Stock__c>();
			}
			else{
				stocksTmp = dateStockMap.get(dateTmp);
			}
			stocksTmp.add(stock);
			dateStockMap.put(dateTmp, stocksTmp);
		}


		//Check if serviceLineItem for service with activation date exists and update it
		//Else create new service and service line item
		for (Date key : dateStockMap.keySet()){
			List<Csord__service_Line_Item__c> serLineItemList = getServiceLineItemForOrderRequestAndActivationDate(orderRequest, key);
			if (serLineItemList == null){
				serLineItemList = new List<Csord__service_Line_Item__c>();
				//create new Service and ServiceLineItem
				//Check if recurring Hardware and create those.
				Csord__service__c newService = createNewServiceForOrderReq(orderRequest, key, dateStockMap.get(key).size());
				if (newService != null){
					//Create service line item
					Csord__service_Line_Item__c sLineItemVAS = createNewVASServiceLineItem(orderRequest, dateStockMap.get(key).size(), orderRequestVasMonthlyPrice);
					serLineItemList.add(sLineItemVAS);
					if (isHardwareRecurring){
						Csord__service_Line_Item__c sLineItemHardware = createNewHardwareServiceLineItem(orderRequest, dateStockMap.get(key).size(), orderRequestHardwareMonthlyPrice);
						serLineItemList.add(sLineItemHardware);
					}
				}
				newServiceServiceLineItemMap.put(newService, serLineItemList);
			}
			else{
				//update existing - add quantity * monthly fee to serviceLineItem Amount and for hardware
				Id parentServiceId = null;
				for (Csord__service_Line_Item__c serLineItem : serLineItemList){
					updateServiceLineItem(serLineItem, dateStockMap.get(key).size(), orderRequestVasMonthlyPrice, orderRequestHardwareMonthlyPrice);
					serviceLineItemsToUpdate.add(serLineItem);
					parentServiceId = serLineItem.csord__service__c;
				}
				if (parentServiceId != null){
					updateServiceQuantity(serviceMap.get(parentServiceId), dateStockMap.get(key).size());
					servicesToUpdate.add(serviceMap.get(parentServiceId));
				}		
			}
		}

		if (!isHardwareRecurring && ordReqNewOrder.contains(orderRequest.Id)){
			//Create hardware one time fee
			Csord__Order_Line_Item__c hardwareOneTime = createHardwareOneTimeFee(orderRequest, orderRequestHardwareOneTimeFee);
			hardwareOneTimeFeeToInsert.add(hardwareOneTime);
		}
	}

	private List<Csord__service_Line_Item__c> getServiceLineItemForOrderRequestAndActivationDate(Order_Request__c ordReq, Date activationDateIn){
		List<Csord__service_Line_Item__c> serviceLineItemList = null;
		List<Csord__service__c> services = orderReqServiceMap.get(ordReq.Id);
		if (services != null){
			for (Csord__service__c service : services){
				if (service.csord__Activation_Date__c == activationDateIn){
					serviceLineItemList = serviceServiceLineItemMap.get(service.Id);
					break;
				}
			}
		}

		return serviceLineItemList;
	}

	private Csord__service__c createNewServiceForOrderReq(Order_Request__c orderRequest, Date activationDate, Integer quantity){
		//get Order's Subscription
		Csord__service__c newService = null;
		csord__subscription__c subscription= ordReqSubscriptionMapMain.get(orderRequest.Id);
		if (subscription != null){
			newService = new Csord__service__c();
			newService.Name = 'GEOTAB Service ' + activationDate;
			newService.Csord__Subscription__c = subscription.Id;
			newService.Csord__Identification__c = 'GEOTAB Service';
			newService.Csord__Status__c = 'Active';
			newService.Csord__Activation_Date__c = activationDate;
			newService.Csord__Deactivation_Date__c = activationDate.addMonths(Integer.valueOf(orderRequest.Runtime__c));
			newService.GEOTAB_Quantity__c = quantity;
			Csord__Order__c csOrder = ordReqOrderMap.get(orderRequest.Id);
			newService.Csord__Order__c = csOrder.Id;
			newService.csord__Order_Request__c = subscription.csord__Order_Request__c;
		}
		return newService;
	}

	private Csord__service_Line_Item__c createNewVASServiceLineItem(Order_Request__c orderRequest, Integer lineItemQuantity, Decimal pricePerOne){
		Csord__service_Line_Item__c serviceLineItem = new Csord__service_Line_Item__c();
		serviceLineItem.Csord__Identification__c = 'GEOTAB VAS Recurring Charge';
		serviceLineItem.Csord__Is_Recurring__c = true;
		serviceLineItem.Total_Price__c = pricePerOne * lineItemQuantity;
		serviceLineItem.Journal_Mapping__c = geotabSettings.Recurring_Charge_Journal_Mapping__c;

		Csord__subscription__c subscription= ordReqSubscriptionMapMain.get(orderRequest.Id);
		serviceLineItem.Csord__Order_Request__c = subscription.csord__Order_Request__c;
		return serviceLineItem;
	}

	private Csord__service_Line_Item__c createNewHardwareServiceLineItem(Order_Request__c orderRequest, Integer lineItemQuantity, Decimal pricePerOne){
		Csord__service_Line_Item__c serviceLineItem = new Csord__service_Line_Item__c();
		serviceLineItem.Csord__Identification__c = 'GEOTAB Hardware Recurring Charge';
		serviceLineItem.Csord__Is_Recurring__c = true;
		serviceLineItem.Total_Price__c = pricePerOne * lineItemQuantity;
		serviceLineItem.Journal_Mapping__c = geotabSettings.Recurring_Charge_Journal_Mapping__c;

		Csord__subscription__c subscription= ordReqSubscriptionMapMain.get(orderRequest.Id);
		serviceLineItem.Csord__Order_Request__c = subscription.csord__Order_Request__c;
		return serviceLineItem;
	}

	private void updateServiceQuantity(Csord__service__c service, Integer quantityToAdd){
		service.GEOTAB_quantity__C = service.GEOTAB_Quantity__C + quantityToAdd;
	} 

	private void updateServiceLineItem(Csord__service_Line_Item__c serviceLineItem, Integer lineItemQuantity, Decimal pricePerOne, Decimal hardwareMonthlyFee){
		if (serviceLineItem.Csord__Identification__c == 'GEOTAB VAS Recurring Charge'){
			serviceLineItem.Total_Price__c = serviceLineItem.Total_Price__c + (pricePerOne * lineItemQuantity);
		}
		else if (serviceLineItem.Csord__Identification__c == ''){
			serviceLineItem.Total_Price__c = serviceLineItem.Total_Price__c + (hardwareMonthlyFee * lineItemQuantity);
		}
	}

	private Csord__Order_Line_Item__c createHardwareOneTimeFee(Order_Request__c orderRequest, Decimal hardwareOneTimeFee){
		Csord__Order_Line_Item__c orderLineItem = new Csord__Order_Line_Item__c();
		Csord__subscription__c subscription= ordReqSubscriptionMapMain.get(orderRequest.Id);
		orderLineItem.Csord__Order_Request__c = subscription.csord__Order_Request__c;
		orderLineItem.Csord__Order__c = subscription.csord__Order__c;
		orderLineItem.Name = 'GEOTAB Hardware One Time Fee';
		orderLineItem.Csord__Discount_Type__c = 'Amount';
		orderLineItem.Csord__Total_Price__c = hardwareOneTimeFee;
		orderLineItem.Csord__Identification__c = 'GEOTAB Hardware One Time';
		orderLineItem.Journal_Mapping__c = geotabSettings.Hardware_One_Time_Journal_Mapping__c;
		return orderLineItem;
	}

	private class ServiceServiceLineItemHelper{
		Csord__service__c service {get;set;}
		List<Csord__service_Line_Item__c> serviceLineItemList{get;set;}

		ServiceServiceLineItemHelper(Csord__Service__c serviceIn, List<Csord__service_Line_Item__c> lineItemListIn){
			serviceLineItemList = new List<Csord__service_Line_Item__c>();
			service = serviceIn;
			serviceLineItemList.addAll(lineItemListIn);
		}
	}
}

/*
BatchCreateMobilitySubscriptions FLOW

1.		Get all Stocks Activated today and attached on M2M Order Request of type SIXT

2.		Process Stocks and get M2M Order Request Id list
2.1.	Set GEOTAB_Processed__c to true on each stock (if batch runs twice or more times a day it shouldn't take already processed stocks)
2.2.	Create Map {M2M Order Request - List of stocks on request}

3.		Create Map {M2M Order Request - Related product configuration's attribute list}
3.1.	Get Order Requests
3.2.	Get Product Configurations
3.3.	Create Map {M2M Order Request - Product Configuration}
3.4.	Get Attributes for all Product Configurations
3.5.	Create Map {M2M Order Request Id - List of related product configuration's attribute}
3.6.	Put Order Request data (3.1) and Map (3.5) into class fields

4.		Get subscriptions for M2M Order Requests or create new if needed (when no subscription exists on Order Request)
4.1		Get already created subscriptions for M2M Order Requests and map them to Order Requests
4.2.	Create new Orders and subscriptions for Order Requests that do not have them
4.3.	Create map {Order Request Id - Subscription} and put that into class field
4.4.	Get Orders and map them {Order Request ID - Order}
4.5.	Get Services and map them into {Order Request - service}
4.6. 	Get Service Line Items and map them into {Service - List of Service Line Items}

5.		Create or update services for Order Requests
5.1.	for each Order Request
5.1.1.	Extract needed configuration attributes for Order Request
5.1.2.	Create map {date - list of stocks activated with that date (m2m activation)}
5.2.	For each Date
5.2.1.	get service and update; in no service-create new
5.2.2.	If hardware recurring fee - update that service too or create new
		else if not recurring - check if order is created on this run - if true -> create new one time fee
																		-else do nothing -> order exists and one time fee is created on order creation
*/