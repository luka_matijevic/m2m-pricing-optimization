public with sharing class M2MYearlyTotalCalculator {
	
	private static Decimal onetimeMonthlyTotal(Integer monthAmount, Decimal revenuePerUnit) {
		if (monthAmount == null || revenuePerUnit == null) {
			return 0.0;
		}
		return monthAmount * revenuePerUnit;
	}
	
	public static Double calculateServiceRevenue(Id opportunityId, Integer year) {
		
		Date rangeStart	= Date.newinstance(year, 1, 1);
		Date rangeEnd 	= Date.newinstance(year + 1, 1, 1);
		
		List<Unit_Schedule__c> schedules = [
			Select
				u.Year__c,
				u.Opportunity__r.Contract_Term_Per_Subscription__c,
				u.Opportunity__r.Monthly_Revenue_Per_Service__c,
				u.Opportunity__r.One_Time_Revenue_Per_Service__c,
				u.VAS_Units_January__c, 
				u.VAS_Units_February__c, 
				u.VAS_Units_March__c, 
				u.VAS_Units_April__c, 
				u.VAS_Units_May__c, 
				u.VAS_Units_June__c, 
				u.VAS_Units_July__c, 
				u.VAS_Units_August__c, 
				u.VAS_Units_September__c, 
				u.VAS_Units_October__c, 
				u.VAS_Units_November__c, 
				u.VAS_Units_December__c
			From 
				Unit_Schedule__c u
			Where
				u.Opportunity__c = :opportunityId
			And
				u.VAS_Start_Date__c <> null
			And 
				u.VAS_Guaranteed_End_Date__c >= :rangeStart
			And
				u.VAS_Start_Date__c < :rangeEnd
		];
		
		Decimal totalRevenue = 0.0;
		
		for (Unit_Schedule__c schedule : schedules) {

			Integer contractDurationMonths = (Integer) schedule.Opportunity__r.Contract_Term_Per_Subscription__c;
			Integer contractYear = Integer.valueOf(schedule.Year__c);
			
			for (Integer month = 1; month <= 12; month++) {
				String fieldName = 'VAS_Units_' + Utils.getMonthName(month) + '__c';
				Integer monthAmount = Integer.valueOf(schedule.get(fieldName));
				
				totalRevenue += revenue(monthYearlyTotal( monthAmount, Date.newInstance(contractYear, month, 1), contractDurationMonths, year), (Double) schedule.Opportunity__r.Monthly_Revenue_Per_Service__c);
				totalRevenue += onetimeMonthlyTotal(monthAmount, schedule.Opportunity__r.One_Time_Revenue_Per_Service__c);
			}
		}
		
		return totalRevenue;
	}
	
	public static Double calculateConnectivityRevenue(Id opportunityId, Integer year) {
		
		Date rangeStart	= Date.newinstance(year, 1, 1);
		Date rangeEnd 	= Date.newinstance(year + 1, 1, 1);
		
		List<Unit_Schedule__c> schedules = [
			Select
				u.Year__c,
				u.Opportunity__r.Contract_Term_Per_SIM__c,
				u.Opportunity__r.Monthly_Revenue_Per_SIM__c,
				u.Opportunity__r.One_Time_Revenue_Per_SIM__c,
				u.Connectivity_Sims_January__c, 
				u.Connectivity_Sims_February__c, 
				u.Connectivity_Sims_March__c, 
				u.Connectivity_Sims_April__c, 
				u.Connectivity_Sims_May__c, 
				u.Connectivity_Sims_June__c, 
				u.Connectivity_Sims_July__c, 
				u.Connectivity_Sims_August__c, 
				u.Connectivity_Sims_September__c, 
				u.Connectivity_Sims_October__c, 
				u.Connectivity_Sims_November__c, 
				u.Connectivity_Sims_December__c
			From 
				Unit_Schedule__c u
			Where
				u.Opportunity__c = :opportunityId
			And 
				u.Connectivity_Start_Date__c <> null				
			And 
				u.Connectivity_Guaranteed_End_Date__c >= :rangeStart
			And
				u.Connectivity_Start_Date__c < :rangeEnd
		];
		
		Decimal totalRevenue = 0.0;
		
		for (Unit_Schedule__c schedule : schedules) {

			Integer contractDurationMonths = (Integer) schedule.Opportunity__r.Contract_Term_Per_SIM__c;
			Integer contractYear = Integer.valueOf(schedule.Year__c);
			
			for (Integer month = 1; month <= 12; month++) {
				String fieldName = 'Connectivity_Sims_' + Utils.getMonthName(month) + '__c';
				Integer monthAmount = Integer.valueOf(schedule.get(fieldName));
				
				totalRevenue += revenue(monthYearlyTotal( monthAmount, Date.newInstance(contractYear, month, 1), contractDurationMonths, year), (Double) schedule.Opportunity__r.Monthly_Revenue_Per_SIM__c);
				totalRevenue += onetimeMonthlyTotal(monthAmount, schedule.Opportunity__r.One_Time_Revenue_Per_SIM__c);
			}
		}
		
		return totalRevenue;
	}
	
	public static Double calculateHardwareRevenue(Id opportunityId, Integer year) {
		
		Date rangeStart	= Date.newinstance(year, 1, 1);
		Date rangeEnd 	= Date.newinstance(year + 1, 1, 1);
		
		List<Unit_Schedule__c> schedules = [
			Select
				u.Year__c,
				u.Opportunity__r.Contract_Term_Per_Hardware__c,
				u.Opportunity__r.Monthly_Revenue_Per_Hardware__c,
				u.Opportunity__r.Revenue_Per_Unit__c,
				u.Hardware_Units_January__c, 
				u.Hardware_Units_February__c, 
				u.Hardware_Units_March__c,
				u.Hardware_Units_April__c, 
				u.Hardware_Units_May__c, 
				u.Hardware_Units_June__c, 
				u.Hardware_Units_July__c, 
				u.Hardware_Units_August__c, 
				u.Hardware_Units_September__c, 
				u.Hardware_Units_October__c, 
				u.Hardware_Units_November__c, 
				u.Hardware_Units_December__c
			From 
				Unit_Schedule__c u
			Where
				u.Opportunity__c = :opportunityId
			And 
				u.Hardware_Start_Date__c <> null				
			And 
				u.Hardware_Guaranteed_End_Date__c >= :rangeStart
			And
				u.Hardware_Start_Date__c < :rangeEnd
		];
		
		Decimal totalRevenue = 0.0;
		
		for (Unit_Schedule__c schedule : schedules) {

			Integer contractDurationMonths = (Integer) schedule.Opportunity__r.Contract_Term_Per_Hardware__c;
			Integer contractYear = Integer.valueOf(schedule.Year__c);
			
			for (Integer month = 1; month <= 12; month++) {
				String fieldName = 'Hardware_Units_' + Utils.getMonthName(month) + '__c';
				Integer monthAmount = Integer.valueOf(schedule.get(fieldName));
				
				totalRevenue += revenue(monthYearlyTotal( monthAmount, Date.newInstance(contractYear, month, 1), contractDurationMonths, year), (Double) schedule.Opportunity__r.Monthly_Revenue_Per_Hardware__c);
				totalRevenue += onetimeMonthlyTotal(monthAmount, schedule.Opportunity__r.Revenue_Per_Unit__c);
			}
		}
		
		return totalRevenue;
	}
	
	/**
	 * Performs a simple calculation revenue = months * monthly amount. Returns 0.0 if either input 
	 * value is null, revenue otherwise.
	 *
	 * @param months Number of months
	 * @param monthlyAmount Monthly revenue amount
	 */
	private static Double revenue(Integer months, Double monthlyAmount) {
		if (monthlyAmount == null || months == null) {
			return 0.0;
		}
		else {
			return months * monthlyAmount;
		}
	}
	
	/**
	 * The method determines the number of months a contract with a contractStart start date and 
	 * contractDuractionMonths months of duration overlaps with the given year. 
	 * For a contract start of '2013-04-01' and the contract duration of 12 months, 
	 * the method will return 9 for year 2013 (there are 9 months of the contract in 2013).
	 *
	 * @param contractStart Start date of the contract
	 * @param contractDurationMonths Duration of contract expressed in months
	 * @param year Year for which the contract months overlap is checked
	 */
	private static Integer contractMonths(Date contractStart, Integer contractDurationMonths, Integer year) {
		
		if (contractStart == null || contractDurationMonths == null || year == null) {
			return 0;
		}
		
		Date yearStart = Date.newInstance(year, 1, 1);
		Date rangeStart = yearStart > contractStart ? yearStart : contractStart;
		
		Date contractEnd = contractStart.addMonths((Integer)contractDurationMonths);	
		Date yearEnd = Date.newInstance(year + 1, 1, 1);
		Date rangeEnd = yearEnd < contractEnd ? yearEnd : contractEnd;
		
		return rangeStart.monthsBetween(rangeEnd);
	}
	
	/**
	 * Calculates yearly total of a single month component as a product of monthly amount and the number of
	 * months of a contract which overlap with the given year.
	 * If the contract with a duration of 12 months starts on '2013-04-01', and the monthly amount is 10,
	 * for year 2013 the method will return 9 * 10 = 90 (there are 9 months of the contract in 2013).
	 * 
	 * @param monthlyAmount Amount in the starting month (SIMs, Services)
	 * @param contractStart Date of the contract start
	 * @param contractDurationMonths Duration of contract expressed in months
	 * @param year Year for which the contract months overlap is checked 
	 */
	private static Integer monthYearlyTotal(Integer monthlyAmount, Date contractStart, Integer contractDurationMonths, Integer year) {
		if (null == monthlyAmount || monthlyAmount == 0) {
			return 0;
		}
		else {
			return monthlyAmount * contractMonths(contractStart, contractDurationMonths, year);
		}
	}
}