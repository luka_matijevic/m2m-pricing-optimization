@isTest
public class AssignAPNsToSRFControllerTest {
    static testMethod void initPageTest(){
         Account account = new Account(
        	Name = 'Test3',
            Type = 'Business'
        	);
        	
        insert account;
        
        id simRecrdtype = Schema.SObjectType.Article_Info_Set__c.getRecordTypeInfosByName().get('SIM').getRecordTypeId();    
        
         Article_Info_Set__c articleInfo = new Article_Info_Set__c(recordtypeId=simRecrdtype,name='Artical Info set',Global_Local__c = 'Global',Status__c='Draft',One_Off_Journal_Mapping__c='Reccurring Fee',
                                             Journal_Mapping__c = 'SMS Usage Fee',Contract_Term__c ='12',One_Time_Fee__c = 100,Recurring_Fee__c = 100);
        insert articleInfo;
        
        SRF__c testSrf = new SRF__c (Name = 'test', Account__c = account.Id, SIM_Type__c = articleInfo.Id);
        insert testSrf;
        
        APN__c testApn = new APN__c(Name = 'testApn', Account__c = testSrf.Account__c, Is_Default__c = true);
        insert testApn;
        
        APN_SRF__c savedSrfApns = new APN_SRF__c (Name = 'test', APN__c = testApn.Id, SRF__c = testSrf.Id);
        insert savedSrfApns;
        
        List<APN_SRF__c> listApnSrf = new List<APN_SRF__c>();
        listApnSrf.add(savedSrfApns);
        
        Test.startTest();
        PageReference pageRef = Page.AssignAPNsToSRF;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', testSrf.Id);
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listApnSrf);
        sc.save();
        AssignAPNsToSRFController avc = new AssignAPNsToSRFController(sc);
        PageReference pageRefConfirm = avc.confirm();
        PageReference pageRefCancel = avc.cancel();
        Test.stopTest();
	}
}