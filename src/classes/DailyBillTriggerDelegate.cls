/**
 * @author Ilija Pavlic
 * This trigger should execute the Service Line Item / Order Line Item Prebill generation when all prebill
 * request have been processed. 
 */ 
public class DailyBillTriggerDelegate extends TriggerHandler.DelegateBase {
	
	public override void prepareBefore() {
		
	}
	
	public override void prepareAfter() {
	}
	
	 public override void beforeInsert(sObject o) {
       Daily_Bill__c db =  (Daily_Bill__c)o;
        if(!test.isrunningtest())
        {
       List<Invoice_Number__c> ll = [select id, name, value__c from invoice_number__c where name='Current' FOR UPDATE];
        Invoice_Number__c invoiceNumber = ll[0];
       String text=String.valueOf(invoiceNumber.Value__c.setScale(0)); 
        while (text.length() < 10)  
            { 
            text = '0' + text; 
            }
       
       db.Maximum_Invoice_Number__c = text;
       //Added by Mahaboob on 4th Jan 2016
       if(db != null && db.Date__c != null && db.Date__c.day() == 1){
           db.Real_Daily_Bill__c = true;
       }
       //Done by Mahaboob
        }
  }
  
  public override void beforeUpdate(sObject old, sObject o) {
      Daily_Bill__c newdb =  (Daily_Bill__c)o;
      Daily_Bill__c olddb =  (Daily_Bill__c)old;
            if(!test.isrunningtest())
        {
       List<Invoice_Number__c> ll = [select id, name, value__c from invoice_number__c where name='Current' FOR UPDATE];
        Invoice_Number__c invoiceNumber = ll[0];
       
       String text=String.valueOf(invoiceNumber.Value__c.setScale(0)); 
        while (text.length() < 10)  
            { 
            text = '0' + text; 
            }
       
       String text_1=String.valueOf(invoiceNumber.Value__c.setScale(0)-1); 
       while (text_1.length() < 10)  
            { 
            text_1 = '0' + text_1; 
            }
       
           if (newdb.Maximum_Invoice_Number__c!=text && newdb.Maximum_Invoice_Number__c!=null && newdb.Maximum_Invoice_Number__c!='0000000000') 
           {
               if (Decimal.valueOf(newdb.Maximum_Invoice_Number__c)>Decimal.valueOf(text)) {
                invoiceNumber.Value__c=Decimal.valueOf(newdb.Maximum_Invoice_Number__c);
                update invoiceNumber;
                }
                else {
                    newdb.Maximum_Invoice_Number__c=text_1;
                }
           }
           //Added by Mahaboob on 4th Jan 2016
           if(newdb != null && newdb.Date__c != null && newdb.Date__c.day() == 1 && !newdb.Real_Daily_Bill__c){
               newdb.Real_Daily_Bill__c = true;
           }
           else if(newdb != null && newdb.Date__c != null && newdb.Date__c.day() != 1 && newdb.Real_Daily_Bill__c){
               newdb.Real_Daily_Bill__c = false;
           }
           //Done by Mahaboob
        }
       
    }
	
	public override void finish() {
	}
}