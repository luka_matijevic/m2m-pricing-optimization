global class RefreshForecastReportScheduler implements Schedulable {

    global void execute(SchedulableContext SC) {
    
    Date startOfMonth = system.today().toStartOfMonth();
    Date endOfNMonth = startOfMonth.addMonths(2);
    endOfNMonth = endOfNMonth.addDays(-1);

//=============== Delete the records to refresh
     List<billing_report__c> billingReportsForCleanupAll = [select id, Billing_Date__c from billing_report__c where Billing_Date__c  >=:startOfMonth AND Billing_Date__c <=:endOfNMonth];

    if(billingReportsForCleanupAll != null && billingReportsForCleanupAll .size() > 0){
        delete billingReportsForCleanupAll ;
    }

//============================Order Line Item
    
    List<csord__Order_Line_Item__c> orderLineItems = [SELECT Id, Name, csord__Order__r.CreatedDate,csord__Order__r.csord__Status__c,csord__Order__r.Order_Request__r.Account__r.name, csord__Total_Price__c,Quantity__c, 
    Journal_Mapping__r.description__c, csord__Order__r.Order_Request__r.Billing_account__r.OFI_id__c, csord__Order__r.Order_Request__r.Billing_account__r.SM2M_Type__c, csord__Order__r.Order_Request__r.Billing_account__r.Id  FROM csord__Order_Line_Item__c where csord__Order__r.CreatedDate >=:startOfMonth AND csord__Order__r.CreatedDate <=:endOfNMonth];

    List<Billing_Report__c> billingReportsFromORL = new List<Billing_Report__c>();

    for (csord__Order_Line_Item__c orl :orderLineItems){
        Billing_Report__c billingReport = new Billing_Report__c();
        billingReport.Account__c=orl.csord__Order__r.Order_Request__r.Account__c;
        billingReport.Amount__c= orl.csord__Total_Price__c;
        billingReport.Billing_Date__c=orl.csord__Order__r.CreatedDate.date().addMonths(1).toStartOfMonth();
        billingReport.Description__c=orl.Journal_Mapping__r.description__c;
        billingReport.isBilled__c=false;
        billingReport.isForecast__c=true;
        billingReport.OFI_ID__c=orl.csord__Order__r.Order_Request__r.Billing_account__r.OFI_id__c;
        billingReport.Quantity__c=orl.Quantity__c;
        billingReport.Billing_account_sm2m_type__c = orl.csord__Order__r.Order_Request__r.Billing_account__r.SM2M_Type__c;
        //12.2.2017. Petar Matkovic Added lookup relationship to billing account
        billingReport.Billing_Account__c = orl.csord__Order__r.Order_Request__r.Billing_account__r.Id;
        
        billingReportsFromORL.add(billingReport);
    }

    insert billingReportsFromORL;


//============================Daily Bill

    List<Daily_Bill_Item__c> dailyBillItems = [SELECT id,name,VO_Nr__c,Einzel_Nettobetrag__c,Re_Datum__c,Beschreibung__c,Menge__c, Daily_Bill__r.date__c, billing_account__c from Daily_Bill_Item__c where Daily_Bill__r.date__c >=:startOfMonth AND Daily_Bill__r.date__c <=:endOfNMonth];


    List<String> ofiid = new List<string>();
    for (Daily_Bill_Item__c orl :dailyBillItems) {
        ofiid.add(orl.VO_Nr__c);
    }
    List<billing_account__c> billingAccounts = [Select payer__c,ofi_id__c, SM2M_Type__c from billing_account__c where ofi_id__c in :ofiid];

    Map<Id,billing_account__c> accountmap = new Map<Id,billing_account__c>();

    for (Daily_Bill_Item__c orl :dailyBillItems){
        for(billing_account__c ba :billingAccounts){
            if(ba.ofi_id__c==orl.VO_Nr__c){ 
                accountmap.put(orl.id,ba);
            }
        }
    }
    
    List<Billing_Report__c> billingReportsFromDB = new List<Billing_Report__c>();

    for (Daily_Bill_Item__c orl :dailyBillItems){
        Billing_Report__c billingReport = new Billing_Report__c();

    if (!billingAccounts.isEmpty()){
        billingReport.Account__c=accountmap.get(orl.id).payer__c;
        billingReport.Billing_account_sm2m_type__c = accountmap.get(orl.id).SM2M_Type__c;
    }

        billingReport.Amount__c= orl.Einzel_Nettobetrag__c;
        billingReport.Billing_Date__c=orl.Daily_Bill__r.date__c.toStartOfMonth();
        billingReport.Description__c=orl.Beschreibung__c;
        billingReport.isBilled__c=true;
        billingReport.isForecast__c=false;
        billingReport.OFI_ID__c=orl.VO_Nr__c;
        billingReport.Quantity__c=orl.Menge__c;
        //12.2.2017. Petar Matkovic Added lookup relationship to billing account
        billingReport.Billing_account__c = orl.Billing_account__c;  
        //23.2.2017. Petar Matkovic Added lookup relationship to daily bill item
        billingReport.Daily_Bill_Item__c = orl.Id;       
                        
        billingReportsFromDB.add(billingReport);
    }

    insert billingReportsFromDB;

//========================================= 

    List<Daily_Bill_Item__c> ll = [SELECT id,name,VO_Nr__c,Einzel_Nettobetrag__c,Re_Datum__c,Beschreibung__c,Menge__c, Daily_Bill__r.date__c, billing_account__c from Daily_Bill_Item__c where Daily_Bill__r.date__c >=:startOfMonth AND Daily_Bill__r.date__c <=:endOfNMonth];
    
    
    List<String> ofiid2= new List<string>();
    for (Daily_Bill_Item__c orl :ll)
        ofiid2.add(orl.VO_Nr__c);
    
    List<billing_account__c> al = [Select payer__c,ofi_id__c, SM2M_Type__c from billing_account__c where ofi_id__c in :ofiid2];
    
    Map<Id,billing_account__c> accountmap2 = new Map<Id,billing_account__c>();
    for (Daily_Bill_Item__c orl :ll)
    for(billing_account__c ba :al)
    if(ba.ofi_id__c==orl.VO_Nr__c) accountmap2.put(orl.id,ba);
    
    
    List<Billing_Report__c> brl = new List<Billing_Report__c>();
    
    for (Daily_Bill_Item__c orl :ll){
        Billing_Report__c brc = new Billing_Report__c();
    
    if (!al.isEmpty()){
        brc.Account__c=accountmap2.get(orl.id).payer__c;
        brc.Billing_account_sm2m_type__c=accountmap2.get(orl.id).SM2M_Type__c;
    }
    
        brc.Amount__c= orl.Einzel_Nettobetrag__c;
        brc.Billing_Date__c=orl.Daily_Bill__r.date__c.toStartOfMonth().addMonths(1);
        brc.Description__c=orl.Beschreibung__c;
        brc.isBilled__c=false;
        brc.isForecast__c=true;
        brc.OFI_ID__c=orl.VO_Nr__c;
        brc.Quantity__c=orl.Menge__c;
        //12.2.2017. Petar Matkovic Added lookup relationship to billing account
        brc.Billing_account__c = orl.Billing_account__c;  
        //23.2.2017. Petar Matkovic Added lookup relationship to daily bill item
        brc.Daily_Bill_Item__c = orl.Id;
        
        brl.add(brc);
    }
    
        insert brl;
//======================================== Cleanup
//select records for delete
   List<billing_report__c> billingReportsForCleanup = [select id, Billing_Date__c from billing_report__c where isForecast__c=true and ( description__c='Aktivierungsgebühr' OR description__c like 'Sim Gebühr%' 
            				OR description__c like 'GS%' OR description__c like 'Connect Kit %') 
            				AND Billing_Date__c  >=:startOfMonth AND Billing_Date__c <=:endOfNMonth];
            				
    if(billingReportsForCleanup != null && billingReportsForCleanup.size() > 0){
        delete billingReportsForCleanup;
    }
   
    }
}