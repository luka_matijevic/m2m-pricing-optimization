global class PublishSubscriptionPoller implements Schedulable {
    public static void startPolling() {
		PublishSubscriptionPoller.scheduleTick(System.now().addSeconds(10));
	}
	
	global void execute(SchedulableContext sc) {
		
		List<Third_Upload_File__c> tufs = [
			Select
				Id, Name, Upload_to_SM2M_Status__c, Order_Request__c, Order_Request__r.Billing_Account__c, Order_Request__r.Billing_Account__r.Billing_Email__c
			from
				Third_Upload_File__c
			where 
				Upload_to_SM2M_Status__c = 'Uploaded'
			Limit 9
		];

		List<Activation_Centre__c> actCenterList = getActivationCentreList(tufs);
		for (Third_Upload_File__c jobData : tufs) {
		    jobData.Upload_to_SM2M_Status__c = 'Published';
		    //P.M. Optimization
		    //update jobData;
		    for (Activation_Centre__c ac : actCenterList){
				if(jobData.Order_Request__c == ac.M2M_Order_Request__c){
					ac.Status__c = 'TUF Published';
					break;
				}
			}
			csam_t1.ObjectGraphCalloutHandler.createAndSend('Publish Subscriptions', jobData.Id);
			/*if (jobData.Order_Request__r.Billing_Account__r.Billing_Email__c != null)
            {
                String[] addresses = new String[]{};
                String body = '\n SIM Swap successfully finished.\n';
                addresses.add(jobData.Order_Request__r.Billing_Account__r.Billing_Email__c);
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setToAddresses(addresses);
                email.setPlainTextBody(body);

                if (addresses.Size() > 0)
                {
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
                }
            }*/
		}	
		
		update tufs;
		update actCenterList;
		if (!BICReportSettingsHelper.isDisableBICReportPollerReschedule()) {
			Datetime nextTick = System.now().addSeconds(BICReportSettingsHelper.getBICReportPollerDelaySeconds());
			PublishSubscriptionPoller.scheduleTick(nextTick);
		}
		System.abortJob(sc.getTriggerId());
	}
	
	private static void scheduleTick(Datetime dt) {
		String chron_exp = dt.format('ss mm HH dd MM ? yyyy');
		PublishSubscriptionPoller poller = new PublishSubscriptionPoller();
		// Schedule the next job, and give it the system time so name is unique
		System.schedule(jobName(dt), chron_exp, poller);
	}
	
	private static String jobName(Datetime dt) {
		return 'PublishSubscriptionPoller_' + dt.getTime();
	}

	private List<Activation_Centre__c> getActivationCentreList(List<Third_Upload_File__c> tufs){
		List<Id> orderRequestIds = new List<Id>();

		for (Third_Upload_File__c tuf : tufs){
			orderRequestIds.add(tuf.Order_Request__c);
		}

		List<Activation_Centre__c> actCenterList = [SELECT Id, Name, Status__c
													FROM Activation_Centre__c
													WHERE M2M_Order_Request__c in :orderRequestIds];

		return actCenterList;
	}
}