public class TicketAssignmentHelper
{
    public static boolean assignTicketToHigherLevel(Id ticketId)
    {
        Boolean isUpdated = false;
        Set<Id> firstLevelOwners = new Set<Id>();
        Set<Id> secondLevelOwners = new Set<Id>();
        Set<Id> thirdLevelOwners = new Set<Id>();
        Set<Id> allQueueIds = new Set<Id>();
        Id firstLevelQId, secondLevelQId, thirdLevelQId;
        Id oldOwnerId;
        List<Group> allLevelQueues = [select id, name, developerName, type from Group where Type = 'Queue' and (developerName = 'DSC_Queue' OR developerName = 'CS_NUE' OR developerName = 'Operations_Management')]; 
        for(Group g : allLevelQueues)
        {
            allQueueIds.add(g.Id);
            if(g.developerName.equalsIgnoreCase('DSC_Queue'))
            {
                firstLevelQId = g.Id;
                firstLevelOwners.add(g.Id);
            }
            else if(g.developerName.equalsIgnoreCase('CS_NUE'))
            {
                secondLevelQId = g.Id;
                secondLevelOwners.add(g.Id);
            }
            else if(g.developerName.equalsIgnoreCase('Operations_Management'))
            {
                thirdLevelQId = g.Id;
                thirdLevelOwners.add(g.Id);
            }
        }
        List<GroupMember> queueMembers = [Select UserOrGroupId, GroupId from GroupMember where GroupId IN :allQueueIds];
        for(GroupMember gm : queueMembers)
        {
            if(firstLevelOwners.contains(gm.GroupId))
            {
            	firstLevelOwners.add(gm.UserorGroupId);
        	}
            else if(secondLevelOwners.contains(gm.GroupId))
            {
            	secondLevelOwners.add(gm.UserorGroupId);
        	}
            else if(thirdLevelOwners.contains(gm.GroupId))
            {
            	thirdLevelOwners.add(gm.UserorGroupId);
        	}
        }
        Case c = [Select Id, CaseNumber, OwnerId, Owner.Name from Case where Id = :ticketId];
        oldOwnerId = c.ownerId;
        if(firstLevelOwners.contains(c.ownerId))
        {
            c.ownerId = secondLevelQId;
            // Adam added, T-21053
            c.Sent_to_higher_level__c = true;
        }  
        else if(secondLevelOwners.contains(c.ownerId))
        {
            c.ownerId = thirdLevelQId;
        }
        if(oldOwnerId != c.ownerId)
        {
            c.Status = 'Open';
			Database.SaveResult sr =  Database.update(c);
            if(sr.isSuccess())
            {
                isUpdated = true;
            }
        }
        return isUpdated;
    }
    
    public static boolean assignTicketToLowerLevel(Id ticketId)
    {
        Boolean isUpdated = false;
        Set<Id> firstLevelOwners = new Set<Id>();
        Set<Id> secondLevelOwners = new Set<Id>();
        Set<Id> thirdLevelOwners = new Set<Id>();
        Set<Id> allQueueIds = new Set<Id>();
        Id firstLevelQId, secondLevelQId, thirdLevelQId;
        Id oldOwnerId;
        List<Group> allLevelQueues = [select id, name, developerName, type from Group where Type = 'Queue' and (developerName = 'DSC_Queue' OR developerName = 'CS_NUE' OR developerName = 'Operations_Management')]; 
        for(Group g : allLevelQueues)
        {
            allQueueIds.add(g.Id);
            if(g.developerName.equalsIgnoreCase('DSC_Queue'))
            {
                firstLevelQId = g.Id;
                firstLevelOwners.add(g.Id);
            }
            else if(g.developerName.equalsIgnoreCase('CS_NUE'))
            {
                secondLevelQId = g.Id;
                secondLevelOwners.add(g.Id);
            }
            else if(g.developerName.equalsIgnoreCase('Operations_Management'))
            {
                thirdLevelQId = g.Id;
                thirdLevelOwners.add(g.Id);
            }
        }
        List<GroupMember> queueMembers = [Select UserOrGroupId, GroupId from GroupMember where GroupId IN :allQueueIds];
        for(GroupMember gm : queueMembers)
        {
            if(firstLevelOwners.contains(gm.GroupId))
            {
            	firstLevelOwners.add(gm.UserorGroupId);
        	}
            else if(secondLevelOwners.contains(gm.GroupId))
            {
            	secondLevelOwners.add(gm.UserorGroupId);
        	}
            else if(thirdLevelOwners.contains(gm.GroupId))
            {
            	thirdLevelOwners.add(gm.UserorGroupId);
        	}
        }
        Case c = [Select Id, CaseNumber, OwnerId, Owner.Name from Case where Id = :ticketId];
        oldOwnerId = c.ownerId;
        if(thirdLevelOwners.contains(c.ownerId))
        {
            c.ownerId = secondLevelQId;
        }  
        else if(secondLevelOwners.contains(c.ownerId))
        {
            c.ownerId = firstLevelQId;
        }
        if(oldOwnerId != c.ownerId)
        {
            c.Status = 'Open';
			Database.SaveResult sr =  Database.update(c);
            if(sr.isSuccess())
            {
                isUpdated = true;
            }
        }
        return isUpdated;
    }
    
    /*public static void assignTicketToHigherLevel(Id ticketId){
    	string nextQueue = 'DSC_Queue';
     	Case caseObject = [Select id, ownerId, caseNumber, account.Name, Owner.Name from Case where id =: ticketId];
      	List<GroupMember> groupNames = [select id,  Group.developerName
      										from GroupMember where Group.Type = 'Queue' 
      										and UserOrGroupId = :caseObject.ownerid and (Group.developerName = 'DSC_Queue' OR Group.developerName = 'CS_NUE' OR Group.developerName = 'Operations_Management')];
          
      	if (!groupNames.isEmpty()) {
        	system.debug(groupNames[0].Group.developerName);
           	if (groupNames[0].Group.developerName == 'DSC_Queue') nextQueue='CS_NUE';
            if (groupNames[0].Group.developerName == 'CS_NUE') nextQueue='Operations_Management';
      	}
      	List<Group> newQueue = [select id, name from Group where Type = 'Queue' and developerName = :nextQueue];
      	system.debug(nextQueue);
      	if (!newQueue.isEmpty())
      	{
        	system.debug(newQueue[0].id);
      		caseObject.ownerid = newQueue[0].id;
      		update caseObject;
      	}
    }*/
    
    /*public static void assignTicketToLowerLevel(Id ticketId){
        string nextQueue = 'Operations_Management';
     	Case caseObject = [Select id, ownerId, caseNumber, account.Name, Owner.Name from Case where id = :ticketId];
        System.debug('Case Owner : ' + caseObject.owner.Name);
      	List<GroupMember> groupNames = [select id,  Group.developerName
      										from GroupMember where Group.Type = 'Queue' 
      										and UserOrGroupId = :caseObject.ownerid and (Group.developerName = 'DSC_Queue' OR Group.developerName = 'CS_NUE' OR Group.developerName = 'Operations_Management')];
          
      	if (!groupNames.isEmpty()) {
        	system.debug(groupNames[0].Group.developerName);
           	if (groupNames[0].Group.developerName == 'CS_NUE') nextQueue='DSC_Queue';
            if (groupNames[0].Group.developerName == 'Operations_Management') nextQueue='CS_NUE';
      	}
      	List<Group> newQueue = [select id, name from Group where Type = 'Queue' and developerName = :nextQueue];
      	system.debug(nextQueue);
      	if (!newQueue.isEmpty())
      	{
        	system.debug(newQueue[0].id);
      		caseObject.ownerid = newQueue[0].id;
      		update caseObject;
      	}
    }*/
}