/**
 *  @author Kristijan K
 *  @description Controller for VisualForce page to display conga document templates 
 */

public class DocumentTemplateController {

	public List<Attachment> congaTempalteList {get;set;}
	public String selectedGroup {get;set;}
	public SelectOption[] listViews = new SelectOption[]{
			new SelectOption('All','All'),
			new SelectOption('Frame Contract','Frame Contract'),
			new SelectOption('Order Form','Order Form'),
			new SelectOption('Einzelauftrag SMART Mobile','Einzelauftrag SMART Mobile'),
			new SelectOption('Order','Order'),
			new SelectOption('Quote','Quote'),
			new SelectOption('SEPA Mandat Business','SEPA Mandat Business'),
			new SelectOption('Service Cloud','Service Cloud')
		};
	private List<Attachment> filteredAttachments;
	private Map<APXTConga4__Conga_Template__c,Attachment> congaTemplateToAttachmentMap;
	private Map<Id,APXTConga4__Conga_Template__c> congaTemplateWrapperMap;
	
	/*
	 * Controller constructor
	 */
	public DocumentTemplateController() {
		congaTemplateWrapperMap = new Map<Id,APXTConga4__Conga_Template__c>();
		List<APXTConga4__Conga_Template__c> congaTemplateWrapperList = [Select Id, Name,APXTConga4__Name__c,APXTConga4__Template_Group__c from APXTConga4__Conga_Template__c];
		for (APXTConga4__Conga_Template__c ct : congaTemplateWrapperList) {
			if (ct.APXTConga4__Template_Group__c != null && ct.APXTConga4__Template_Group__c != '') {
				congaTemplateWrapperMap.put(ct.Id,ct);
			}
		}
		List<Attachment> unfilteredCongaTempalteList = [select Id, Name, BodyLength, CreatedDate, LastModifiedDate, ParentId  from Attachment where parentId IN:congaTemplateWrapperMap.keySet() ORDER BY LastModifiedDate DESC NULLS FIRST];
		selectedGroup = 'All';
		if (unfilteredCongaTempalteList != null) {
			// Utils.sortList() method to sort the passed sObject list by fieldName and specified order
			congaTempalteList = (List<Attachment>)Utils.sortList(filterAttachmentsByGroup(filterAttachments(unfilteredCongaTempalteList)), 'LastModifiedDate', 'DESC');
		}

	}

	/*
	 * conga template getter method
	 */
	public List<Attachment> getCongaTemplateList() {
		return congaTempalteList; 
	} 

	/**
	 * @description Method filters all conga templates and returns only most recent template
	 * @param unfilteredCongaTempalteList - unfiltered attachment list
	 * @return filtered attachment list
	 */
	private List<Attachment> filterAttachments(List<Attachment> unfilteredCongaTempalteList) {
		Map<Id,Attachment> idToAttachmentMap = new Map<Id,Attachment>();
		for (Attachment att : unfilteredCongaTempalteList) {
			if (idToAttachmentMap.get(att.ParentId) == null) {
				idToAttachmentMap.put(att.ParentId,att);
			} else if (idToAttachmentMap.get(att.ParentId) != null && idToAttachmentMap.get(att.ParentId).CreatedDate < att.CreatedDate) {
				idToAttachmentMap.put(att.ParentId, att);		
			} 
		}
		filteredAttachments = idToAttachmentMap.values();
		return idToAttachmentMap.values();

	}

	public SelectOption[] getListViews() {
		return listViews;
	}


	public void refreshTemplates() {
		congaTempalteList = (List<Attachment>)Utils.sortList(filterAttachmentsByGroup(filteredAttachments), 'LastModifiedDate', 'DESC');
	}


	public Map<Id,APXTConga4__Conga_Template__c> getCongaTemplateWrapperMap() {
		return congaTemplateWrapperMap;
	}
	/**
	 * @description method returns different filtering based on template group
	 */
	private List<Attachment> filterAttachmentsByGroup(List<Attachment> attachmentList) {
		if (selectedGroup == 'All') {
			return attachmentList;
		}
		else if (selectedGroup == 'Frame Contract') {
			List<Attachment> returnList = new List<Attachment>();
			for (Attachment att : attachmentList) {
				if (Integer.valueOf(congaTemplateWrapperMap.get(att.ParentId).APXTConga4__Template_Group__c) == 5) {
					returnList.add(att);
				}
			}
			return returnList;
		}
		else if (selectedGroup == 'Service Cloud') {
			List<Attachment> returnList = new List<Attachment>();
			for (Attachment att : attachmentList) {
				if (Integer.valueOf(congaTemplateWrapperMap.get(att.ParentId).APXTConga4__Template_Group__c) == 6) {
					returnList.add(att);
				}
			}
			return returnList;
		}
		else if (selectedGroup == 'Order Form') {
			List<Attachment> returnList = new List<Attachment>();
			for (Attachment att : attachmentList) {
				if (Integer.valueOf(congaTemplateWrapperMap.get(att.ParentId).APXTConga4__Template_Group__c) == 4) {
					returnList.add(att);
				}
			}
			return returnList;
		}
		else if (selectedGroup == 'Einzelauftrag SMART Mobile') {
			List<Attachment> returnList = new List<Attachment>();
			for (Attachment att : attachmentList) {
				if (congaTemplateWrapperMap.get(att.ParentId).APXTConga4__Name__c.contains('Einzelauftrag SMART Mobile')) {
					returnList.add(att);
				}
			}
			return returnList;
		}
		else if (selectedGroup == 'SEPA Mandat Business') {
			List<Attachment> returnList = new List<Attachment>();
			for (Attachment att : attachmentList) {
				if (congaTemplateWrapperMap.get(att.ParentId).APXTConga4__Name__c.contains('SEPA Mandat Business')) {
					returnList.add(att);
				}
			}
			return returnList;
		}
		else if (selectedGroup == 'Order') {
			List<Attachment> returnList = new List<Attachment>();
			for (Attachment att : attachmentList) {
				if (congaTemplateWrapperMap.get(att.ParentId).APXTConga4__Name__c.equals('Order')) {
					returnList.add(att);
				}
			}
			return returnList;
		}
		else if (selectedGroup == 'Quote') {
			List<Attachment> returnList = new List<Attachment>();
			for (Attachment att : attachmentList) {
				if (congaTemplateWrapperMap.get(att.ParentId).APXTConga4__Name__c.equals('Quote')) {
					returnList.add(att);
				}
			}
			return returnList;
		}
		else {
			return new List<Attachment>();
		}
	}
}