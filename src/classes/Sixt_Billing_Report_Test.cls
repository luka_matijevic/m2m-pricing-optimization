@isTest
public class Sixt_Billing_Report_Test {
    
    static testMethod void Sixt_Billing_Report(){
        Case cs = new Case(Status= 'Open',Type='Standard',Category__c='Sixt',
                           Origin='Phone',Sixt_LVID__c='1234',
                           Sixt_SIM_activation_date__c = DateTime.valueOf(system.now()),
                           Subject = 'Test Subject',
                           Description = 'Test Description Description');   
        insert cs;
        Account acc = new Account(
			Name = 'Test account111', 
			TEF_DE_Sales_Channel__c = 'Test channel', 
			Type = 'Customer',
			VAT__c = 1,
			BillingCity = 'Bremen', 
			BillingCountry = 'Germany', 
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße',
            Global__c = true
		);
		insert acc;
        ID sixtRTOnFC=Schema.SObjectType.Frame_Contract__c.getRecordTypeInfosByName().get('Sixt').getRecordTypeID();
        
        Frame_Contract__c fc = new Frame_Contract__c(recordtypeId = sixtRTOnFC, Customer__c=acc.id);
        insert fc;
        
        Order_Request__c m2mOrderRequest = new Order_Request__c(Frame_Contract__c = fc.id,
                                                                Ticket__c = cs.id, Shipping_City__c='Test City', Shipping_Country__c = 'Test Country', Shipping_Postal_Code__c='10000', Shipping_Street__c='Test shipping street');
        
        
        insert m2mOrderRequest;
         
        csord__Order_Request__c ordReq = new csord__Order_Request__c(Name='Ord Req',
                                                                     csord__Module_Name__c= 'Module Name',
                                                                     csord__Module_Version__c = 'v1.1');
        insert ordReq;
        
        csord__Order__c orderRequest = new csord__Order__c(Name = 'Test Order Request',
                                                           csord__Status2__c ='Opened',
                                                           Order_Request__c = m2mOrderRequest.id,
                                                           csord__Identification__c = '12222',
                                                           csord__Order_Request__c = ordReq.id);
        insert orderRequest;
        
        
        Journal_Mapping__c jmapping = new Journal_Mapping__c(name = 'Journal Mapping');
        insert jmapping;
        
        csord__Order_Line_Item__c ordLi = new csord__Order_Line_Item__c(name= 'Ord Line Item',
                                                                        Journal_Mapping__c = jmapping.id,
                                                                        csord__Identification__c = '2112312',
                                                                        csord__Order__c =orderRequest.id,
                                                                        csord__Order_Request__c =ordReq.id ,
                                                                        csord__Discount_Type__c = 'Amount');
        insert ordLi;
        
        csord__Subscription__c sub = new csord__Subscription__c(name='Subscription',
                                                                csord__Status__c = 'Status',
                                                                csord__Identification__c = '2qweqweq',
                                                                csord__Order_Request__c = ordReq.id,
                                                                csord__Order__c = orderRequest.id);
        insert sub;
        
        csord__Service__c servc = new csord__Service__c(name = 'service',
                                                        csord__Subscription__c = sub.id,
                                                        csord__Identification__c = '2e2312341',
                                                        csord__Order_Request__c = ordReq.id,
                                                        csord__Status__c = 'Status',
                                                        csord__Deactivation_Date__c = date.valueOf(system.today()));
        insert servc;
        
        csord__Service_Line_Item__c sli = new csord__Service_Line_Item__c(name = 'Service Line Item',
                                                                          csord__Service__c = servc.id,
                                                                          Journal_Mapping__c = jmapping.id,
                                                                          csord__Identification__c = 'asdasdasd',
                                                                          csord__Order_Request__c = ordReq.id);
        insert sli;
        
        Daily_Bill__c dailyBill = new Daily_Bill__c(Official_Invoice_Date__c = Date.valueOf(system.today()));
        insert dailyBill;
        
        Prebill__c preBill = new Prebill__c(name='Pre Bill',Daily_Bill__c = dailyBill.id);
        insert preBill;
        
        Prebill_Line_Item__c preBillLi = new Prebill_Line_Item__c(name= 'Pre Bill Line Item',
                                                                  Line_Item_Description__c = 'Description',
                                                                  Line_Item_Quantity__c = 10,
                                                                  Line_Item_Amount__c = 10,
                                                                  Prebill__c = preBill.id,
                                                                  Service_Line_Item__c =sli.id,
                                                                  Order_Line_Item__c = ordLi.id);
        insert preBillLi;
        
        test.startTest();
        
        PageReference pf = Page.Sixt_Billing_Report;
        test.setCurrentPage(pf);
        pf.getParameters().put('id',dailyBill.id);
        
        ApexPages.StandardController sc = new apexPages.StandardController(dailyBill);
        Sixt_Billing_Report billingReport = new Sixt_Billing_Report(sc);
        billingReport.checkNullString(null);
        
        test.stopTest();
        
        
    }

}