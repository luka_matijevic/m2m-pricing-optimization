public class SimWrapper {
    public String iccId {get; set;}
    public String msisdn {get; set;}
    
    public SimWrapper(String iccId, String msisdn){
        this.iccId = iccId;
        this.msisdn = msisdn;
    }
}