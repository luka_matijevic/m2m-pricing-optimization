@isTest
global with sharing class MockSendEmail implements ISendEmail
{
	private String attachmentName;
	private String attachment;

	//--------------------------------------------------------------------------
	// Set attachment
	//--------------------------------------------------------------------------
	global void setAttachment(String attachmentName, String attachment)
	{
		this.attachmentName = attachmentName;
		this.attachment = attachment;
	}

	//--------------------------------------------------------------------------
	// Send email
	//--------------------------------------------------------------------------
	global void send()
	{
		//
		// Do nothing
		//
	}

	//--------------------------------------------------------------------------
	// Get attachment name
	//--------------------------------------------------------------------------
	public String getAttachmentName()
	{
		return attachmentName;
	}

	//--------------------------------------------------------------------------
	// Get attachment
	//--------------------------------------------------------------------------
	public String getAttachment()
	{
		return attachment;
	}

}