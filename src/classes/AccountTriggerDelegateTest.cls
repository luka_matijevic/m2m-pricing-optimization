@isTest
private class AccountTriggerDelegateTest {

	private static testMethod void testAccount() {
       	Account acc = new Account(
			Name = 'Test account',
			BillingCity = 'Bremen',
			BillingCountry = 'Germany',
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße',
			Type = 'Customer',
			Billing_Cycle__c = String.valueOf(Date.today().day()),
			VAT__c = 1,
			TEF_DE_Sales_Channel__c='M2M__'
		);
		insert acc;
		
		system.assert(acc.id!=null,'Account insert erorr');
        
        acc.VAT__c=2;
        
        system.assert(acc.VAT__c==2,'Account update error');
        
        /*Luka - code coverage fix*/
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
      	User u = new User(Alias = '123sta', Email='standarduser@testorg.com', 
      	EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      	LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles',
      	UserName='123o2spin-salesforce@syzygy.de');
      	
      	System.runAs(u){
      		acc.TEF_DE_Sales_Channel__c='M2M';
      		acc.BillingCountry ='Croatia';
      		update acc;
      		
      	}
      	      	
        
	}

}