@isTest
public class TestMSISDNRangeTriggerDelegate { 


	static testMethod void testInsertingMSISDNRange()
	{
		Test.startTest();
		MSISDN_Range__c range = new MSISDN_Range__c();
		range.Range_start__c = '100000000000';
		range.Range_end__c = '100000000002';
		insert range;
		
		range = new MSISDN_Range__c();
		range.Range_start__c = '100000000001';
		range.Range_end__c ='100000000004';
		try 
		{	        
			insert range;
		}
		catch (Exception e)
		{
			Boolean expectedException = e.getMessage().contains('Range overlaps range:') ;
			System.assertEquals(expectedException, true);
		}	
		range = new MSISDN_Range__c();
		range.Range_start__c = 'A';
		range.Range_end__c = '1';
		try 
		{	        
			insert range;
		}
		catch (Exception e)
		{
            system.debug(e.getMessage());
			Boolean expectedException = e.getMessage().contains('has to be a number.') ;
			System.assertEquals(expectedException, true);
		}	
		range = new MSISDN_Range__c();
		range.Range_start__c = '2';
		range.Range_end__c = '1';
		try 
		{	        
			insert range;
		}
		catch (Exception e)
		{
			Boolean expectedException = e.getMessage().contains('Range end has to be greater than range start.') ;
			System.assertEquals(expectedException, true);
		}
	
	
	}
}