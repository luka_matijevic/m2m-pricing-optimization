//-------------------------------------------------------------------------------
// 
//-------------------------------------------------------------------------------
global class CreateOrderForAccountBatchableStepFirst implements Database.Batchable<SObject>
{ 
    Id accountId = null;
    Opportunity opportunityObj = null;
    List<Opportunity> opportunityObjList = null;
    String labelConfigCopyRequestId = /*PROD 'a0nb000000IV4Ny'; // UAT  'a0n7E00000054WVQAY'; // */ Label.Config_Copy_Request_Id; // standard product with a simple configuration (just for migration)
    cscfga__Product_Configuration__c pc = null;
    String bundleForCopy = /* UAT'a0l7E000000DZM3QAO'; */ /* PROD */ 'a0lb000000DBvrsAAD';
    static Integer bundleFieldLength = cscfga__Product_Bundle__c.Name.getDescribe().getLength();
    cscfga__Product_Basket__c basket = null;
    static List<cscfga__Product_Bundle__c> pbList = null;
        
    //---------------------------------------------------------------------------
    // Start of batch apex
    //---------------------------------------------------------------------------
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        //
        // Filter records
        //
        String query = 'SELECT Id, Eplus_Customer__c FROM Account WHERE Eplus_Customer__c = true and id in (select accountid from opportunity where RecordType.Name = \'Global Local Commercial K2\'  and StageName != \'Closed Won\')';// AND Migration_process_finished__c = false LIMIT 5';
        return Database.getQueryLocator(query);
    }

    //--------------------------------------------------------------------------
    // Execute of batch apex
    //--------------------------------------------------------------------------
    global void execute(Database.BatchableContext bc, List<SObject> scope)
    {
        for (Account acct : (List<Account>)scope)
        {
            accountId = acct.Id;
            createProcess();
        }
    }

    //--------------------------------------------------------------------------
    // Finish of batch apex
    //--------------------------------------------------------------------------
    global void finish(Database.BatchableContext bc)
    {
        
    }
    
    private void createProcess() {
        
        string opquery = 'SELECT Id, AccountId, HasSynchedBundle__c, CloseDate, Synched_Bundle_Id__c, RecordTypeId, RecordType.Name, Contract_Term_Per_SIM__c, Contract_Term_Per_Subscription__c  FROM Opportunity WHERE AccountId = :accountId AND RecordType.Name = \'Global Local Commercial K2\' and id not in (select cscfga__Opportunity__c from cscfga__Product_Bundle__c) LIMIT 1'; //   
        opportunityObjList = database.query(opquery);
        
        if(opportunityObjList != null && opportunityObjList.size() > 0) {
            opportunityObj = opportunityObjList[0];
            System.debug(LoggingLevel.INFO, 'accId: ' + accountId + ', oppId: ' + opportunityObj.Id);
            if(opportunityObj != null) {
                
                /* LM - From hardcoded bundle to custom setting
                pbList = [SELECT Id, Name FROM cscfga__Product_Bundle__c WHERE Id = :bundleForCopy];
				*/
				Migration_Bundles__c migrationBundle = Migration_Bundles__c.getValues(String.valueOf(accountId).substring(0,15));
                System.debug('accountId, migrationBundle: ' + accountId + ', '+ migrationBundle);
                
                if (migrationBundle != null ){ 
                    pbList = [SELECT Id, Name FROM cscfga__Product_Bundle__c WHERE Id = :migrationBundle.Bundle_Id__c];
                }
                else
                    pbList = [SELECT Id, Name FROM cscfga__Product_Bundle__c WHERE Id = :bundleForCopy];
                System.debug('pbList: ' + pbList);

                //cscfga__Product_Bundle__c bundle = insertBundle(opportunityObj.Id);
            
                //cscfga__Product_Basket__c basket = insertBasket(opportunityObj.Id);
                
                //opportunityObj.Account.Migration_process_finished__c = true;
                //update opportunityObj.Account;
                
                cscfga__Product_Bundle__c targetBundle = null;
                
                targetBundle = new cscfga__Product_Bundle__c();
                String targetName = 'Copy of ' + pbList[0].Name;
                if (targetName.length() > bundleFieldLength)
                {
                    targetName = targetName.substring(0, bundleFieldLength);
                }
                targetBundle.Name = targetName;
                targetBundle.cscfga__Opportunity__c = opportunityObj.Id;
                
                basket = new cscfga__Product_Basket__c();
                basket.Name = 'Basket for Opportunity: ' + opportunityObj.Id;   // use the same opp id
                // targetBasket.cscfga__Opportunity__c is set by the page automatically (on the press of the clone button setter is called by the page)
                basket.cscfga__Basket_Status__c = 'Valid';
                
                //
                // Create bundle and basket
                //
                insert targetBundle;
                insert basket;
                System.debug(LoggingLevel.INFO, 'targetBundle: ' + targetBundle + ', basket: ' + basket);
                //
                // Copy configurations to product bundle
                //
                List<cscfga__Product_Configuration__c> configurations =
                        [ SELECT Id
                          FROM cscfga__Product_Configuration__c
                          WHERE cscfga__Product_Bundle__c = :pbList[0].Id
                          ORDER BY CreatedDate ASC ];
        
                List<Id> childIdList = cscfga.API_1.cloneConfigurations(configurations, targetBundle, basket, false);
                
                List<cscfga__Attribute__c> attributesProdConfigList = new List<cscfga__Attribute__c>();
                Set<String> nameSet = new Set<String>();
                nameSet.add('Account Id');
                nameSet.add('Bundle Id');
                nameSet.add('Config Id');
                for(cscfga__Attribute__c att : [SELECT cscfga__Value__c, Name, cscfga__Product_Configuration__c, Id,
                                            cscfga__Product_Configuration__r.cscfga__Product_Bundle__c, 
                                            cscfga__Product_Configuration__r.cscfga__Product_Bundle__r.cscfga__Opportunity__r.AccountId
                                            FROM cscfga__Attribute__c where cscfga__Product_Configuration__c IN: childIdList AND Name IN: nameSet]){
                                            
                    if(att.Name.equalsIgnoreCase('Account Id')){
                        att.cscfga__Value__c = att.cscfga__Product_Configuration__r.cscfga__Product_Bundle__r.cscfga__Opportunity__r.AccountId;
                    }
                    if(att.Name.equalsIgnoreCase('Bundle Id')){
                        att.cscfga__Value__c = att.cscfga__Product_Configuration__r.cscfga__Product_Bundle__c;
                    }
                    if(att.Name.equalsIgnoreCase('Config Id')){
                        att.cscfga__Value__c = att.cscfga__Product_Configuration__c;
                    }
                    attributesProdConfigList.add(att);
                }
                
                if(!attributesProdConfigList.isEmpty()){
                    update attributesProdConfigList;
                }
                
                PostCopyConnectivityConfigurations connector = new PostCopyConnectivityConfigurations(targetBundle.Id);
                connector.doExecute();
            }
        }
        
        /*Set<Id> setColl = new Set<Id>();
        setColl.add(labelConfigCopyRequestId);
        
        cscfga.ProductConfigurationBulkActions.copyProductConfigsAsync(setColl, bundle.Id, basket.Id);*/
        
        /*
        string query_pc = 'SELECT ' +  SObjectHelper.getFieldListForSOQL('cscfga__product_configuration__c', null, null) + ' FROM cscfga__product_configuration__c where cscfga__product_bundle__c=\'' + bundle.Id + '\'';    
        List<cscfga__Product_Configuration__c> pclist = database.query(query_pc);
        
        pc = pclist[0];
    
        Frame_Contract__c fc = insertFrameContract(opportunityObj);
        
        bundle = upsertBundle(bundle);
            
        opportunityObj.Contract_Term_Per_SIM__c = 24;
        opportunityObj.Contract_Term_Per_Subscription__c = 0;
    
        update opportunityObj;
        
        //sync bundle
        cscfga.Api_1.syncBundleToOpportunity(bundle.Id);
            
        fc.Opportunity__c = opportunityObj.Id;
        
        update fc;
        
        string BusinessModelDescription = '';
        
        Order_Request__c oc = insertOrderRequest(opportunityObj, fc, BusinessModelDescription);
        
        //create other related products
        Product_Order__c pcor = insertProductOrder(BusinessModelDescription, oc);
        
        Product_Configuration_Order__c cor = insertProductConfigOrder(pcor, pc);
        
        Third_Upload_File__c tuf = insertTUF(oc, opportunityObj);
        
        // Create order
        Id orderId = Factory.createCreateOrder(oc.Id).create();
        // Create subscriptions
        Factory.createCreateSubscriptions(orderId).create();

*/
    }

    private cscfga__Product_Bundle__c insertBundle(Id oppId) {
        cscfga__Product_Bundle__c theBundle = new cscfga__Product_Bundle__c();
        theBundle.cscfga__Opportunity__c = oppId;
        
        insert theBundle;
        
        return theBundle;
    }

    private cscfga__Product_Basket__c insertBasket(Id oppId) {
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
        basket.cscfga__Opportunity__c = oppId;
        
        insert basket;
        
        return basket;
    }
    
    private Frame_Contract__c insertFrameContract(Opportunity opportunityObj) {
        Frame_Contract__c fc = new Frame_Contract__c(Opportunity__c = opportunityObj.Id,
             Valid_From__c = opportunityObj.CloseDate,
             Product_Bundle__c = opportunityObj.Synched_Bundle_Id__c,
             Customer__c = opportunityObj.AccountId,
             Status__c = 'Frame Contract Requested',
             Send_Email_to_BSS_Inbox__c = false);
             
        if(opportunityObj.RecordTypeId == Cache.RecordTypeId('Opportunity.Trial Local') || opportunityObj.RecordTypeId == Cache.RecordTypeId('Opportunity.Trial Global')){
            fc.RecordTypeId = Cache.RecordTypeId('Frame_Contract__c.Trial');
        }
        else if(opportunityObj.RecordTypeId == Cache.RecordTypeId('Opportunity.Commercial Local') || opportunityObj.RecordTypeId == Cache.RecordTypeId('Opportunity.Commercial Global')){
            fc.RecordTypeId = Cache.RecordTypeId('Frame_Contract__c.Commercial');
        }
        
        insert fc;
        
        return fc;
    }

    private cscfga__Product_Bundle__c upsertBundle(cscfga__Product_Bundle__c bundle) {
        if (bundle.cscfga__Opportunity__c == null)  
        {
            bundle.cscfga__Synchronised_with_Opportunity__c = true;
            bundle.cscfga__Opportunity__c = opportunityObj.Id;
        }
        
        upsert bundle;
        
        return bundle;
    }
    
    private Order_Request__c insertOrderRequest(Opportunity opportunityObj, Frame_Contract__c fc, string BusinessModelDescription) {
        //create order request and connect it with opportunity and its attributes
        Order_Request__c oc = new Order_Request__c(Frame_Contract__c = fc.Id);
        
        oc.Business_Model_Description__c = BusinessModelDescription;
        oc.Account__c = opportunityObj.AccountId;
        
        oc.Opportunity__c = opportunityObj.Id;
        
        List<Billing_Account__c> bics = [select id,name from Billing_account__c where Payer__c = :opportunityObj.Accountid limit 1];
        Billing_Account__c bic = new Billing_Account__c();
        
        if (bics != null && bics.size() > 0) {
            bic = bics[0];
            oc.Billing_Account__c = bic.id;
        }
        
        List<Boni_Check__c> accountBoniChecks = [SELECT Id, CreatedDate FROM Boni_Check__c WHERE Account__c = :oc.Account__c ORDER BY CreatedDate DESC];
        if (accountBoniChecks != null && accountBoniChecks.size() > 0)
        {
            oc.Boni_Check__c = accountBoniChecks[0].Id;
        }
        
        insert oc;
        
        return oc;
    }
    
    private Product_Order__c insertProductOrder(string BusinessModelDescription, Order_Request__c oc) {
        Product_Order__c pcor = new Product_Order__c();
        pcor.Comment__c = BusinessModelDescription;
        pcor.Order_Request__c = oc.id;       
            
        upsert pcor;
        
        return pcor;
    }
    
    private Product_Configuration_Order__c insertProductConfigOrder(Product_Order__c pcor, cscfga__Product_Configuration__c pc) {
        Product_Configuration_Order__c cor = new Product_Configuration_Order__c();
        
        cor.Product_Configuration__c = pc.id;
        cor.Product_Order__c = pcor.id;
        
        upsert cor;
        
        return cor;
    }
    
    private Third_Upload_File__c insertTUF(Order_Request__c oc, Opportunity opportunityObj) {
        Third_Upload_File__c tuf = new Third_Upload_File__c();
        tuf.SIM_Article_Type__c  = 'a15b0000005l3ML';/*PROD // UAT =  'a157E000000Fa4v';*/
        tuf.Order_Request__c = oc.id; 
        tuf.Account__c = opportunityObj.AccountId ;
        
        upsert tuf;
        
        return tuf;
    }
}