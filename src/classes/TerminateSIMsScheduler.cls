/**
	* @author: Petar Matkovic
*/
global class TerminateSIMsScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		//MyBatchClass b = new MyBatchClass();
		//database.executebatch(b);
		Database.executeBatch(new TerminateSIMsBatch());
	}
}