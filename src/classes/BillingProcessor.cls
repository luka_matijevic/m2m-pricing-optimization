/**
 * This class should process Service line items and Order line items data to produce
 * prebill line items.
 *
 * @author Ilija Pavlic
 */
public class BillingProcessor {

	// contains billing accounts for the date of the billing
	private List<Billing_Account__c> billingAccounts = new List<Billing_Account__c>();
	// map of all journal mappings
	private Map<Id, Journal_Mapping__c> journalMappings = new Map<Id, Journal_Mapping__c>();
	// distributor of prebill line items to appropriate prebills
	private PrebillLineItemDistributor pbliDistributor;

	private Integer successesCount = 0;
	private Integer processedCount = 0;

	private List<Billing_Account__c> failedBillingAccounts = new List<Billing_Account__c>();

	private Date billingDate;

	private List<sObject> queue = new List<sObject>();

	GEOTAB_Settings__c geotabSettings = GEOTAB_Settings__c.getInstance();

	public BillingProcessor(Date billingDate) {
		this.billingDate = billingDate;
		this.billingAccounts = selectBillingAccounts(billingDate);
		this.journalMappings = selectJournalMappingsMap();
		this.pbliDistributor = new PrebillLineItemDistributor(billingDate);
	}

	public Integer getSuccessfulCount() {
		return Integer.valueOf(this.successesCount);
	}

	/**
	 * Delegates to prebill distributor
	 */
	public Integer getNewPrebillLineItemsCount() {
		return this.pbliDistributor.getNewPrebillLineItemsCount();
	}

	/**
	 * Delegates to prebill distributor
	 */
	public Integer getNewPrebillsCount() {
		return this.pbliDistributor.getNewPrebillsCount();
	}

	public Integer getProcessedCount() {
		return Integer.valueOf(this.processedCount);
	}

	public Date getBillingDate() {
		return Date.valueOf(this.billingDate);
	}

	public Boolean hasFailedBillingAccounts() {
		return failedBillingAccounts.size() > 0;
	}

	public List<Billing_Account__c> getBillingAccounts() {
		return billingAccounts;
	}

	public List<Billing_Account__c> getFailedBillingAccounts() {
		return failedBillingAccounts;
	}

	public void queue(csord__Order_Line_Item__c oli) {
		queue.add(oli);
	}

	public void queue(csord__Service_Line_Item__c sli) {
		queue.add(sli);
	}

	private Prebill_Line_Item__c process(csord__Service_Line_Item__c sli) {

		this.pbliDistributor.addLineItem(sli);

		Id billingAccountId = LineItems.billingAccountId(sli);
		BillingRateModel brm = BillingRateModels.billingRateModel(sli);

		if (geotabSettings == null){
			geotabSettings = GEOTAB_Settings__c.getInstance();
		}
		
		Prebill_Line_Item__c pbli = new Prebill_Line_Item__c(
			Line_Item_Quantity__c = sli.Quantity__c,
			Line_Item_Amount__c = lineItemAmount(sli, billingDate),
			Line_Item_Description__c = journalMappings.get(sli.Journal_Mapping__c).Name,
			Name = sli.Name,
			Service_Line_Item__c = sli.Id,
			M2M_Order_Request__c = sli.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__r.Id,
			M2M_Order_Request_Name__c = sli.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__r.Name
		);

		if(sli.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__c != null &&
			sli.csord__Service__r.csord__Subscription__r.csord__Order__r.Order_Request__r.RecordTypeId == geotabsettings.Order_Request_Type_Id__c) {
			pbli.Is_Geotab__c = true;
		}
		else{
			pbli.Is_Geotab__c = false;
		}

		Prebill__c parentPrebill = this.pbliDistributor.findPrebill(sli);

		if (parentPrebill != null) {
			pbli.Prebill__c = parentPrebill.Id;
		}

		JournalMappingHelper.addInformation(pbli, journalMappings.get(sli.Journal_Mapping__c));

		// if this is service line item's last billing cycle, deactivate it
		if (sli.csord__Is_Recurring__c == false || sli.csord__Service__r.csord__Deactivation_Date__c < this.billingDate){
			sli.Is_Billed__c = true;
		}

		return pbli;
	}
	
	private Decimal lineItemAmount(csord__Service_Line_Item__c sli, Date billingDate) {
		
		Decimal lineItemAmount = null;
		
		Date activationDate = sli.csord__Service__r.csord__Activation_Date__c;
		Date deactivationDate = sli.csord__Service__r.csord__Deactivation_Date__c;
		
		Double monthlyFee = sli.Total_Price__c / sli.Quantity__c;
		
		Date billingStart = billingDate.addMonths(-1);

		if (this.billingDate >= activationDate && this.billingDate < activationDate.addMonths(1)){
			// if current billing date is for the first billing period
			BillingPeriod billingPeriod = new BillingPeriod(activationDate, this.billingDate);
			lineItemAmount = billingPeriod.weightedAmount(monthlyFee);
		} else if (this.billingDate >= deactivationDate && this.billingDate <= deactivationDate.addMonths(1)){
			// if current billing date is for the last billing period
			BillingPeriod billingPeriod = new BillingPeriod(billingStart, deactivationDate);
			lineItemAmount = billingPeriod.weightedAmount(monthlyFee);
		} else if (this.billingDate >= activationDate.addMonths(1) && this.billingDate <= deactivationDate){
			lineItemAmount = monthlyFee;
		}
		
		return lineItemAmount;
	}
	
	private Prebill_Line_Item__c process(csord__Order_Line_Item__c oli) {

		system.debug('**IP** processing oli '+ JSON.serializePretty(oli));
		this.pbliDistributor.addLineItem(oli);

		Id billingAccountId = LineItems.billingAccountId(oli);
		BillingRateModel brm = BillingRateModels.billingRateModel(oli);

		if (geotabSettings == null){
			geotabSettings = GEOTAB_Settings__c.getInstance();
		}
		// net and gross amounts are formulas
		// all order line items are one-time charges.
		// on sucessfull creation of prebill line item, set the
		// is billed flag to true unconditionally
        String line_item_desc = '';
        if (oli.name=='Rabatt') {
			line_item_desc = oli.csord__Line_Description__c;
			}
			else {
			line_item_desc = journalMappings.get(oli.Journal_Mapping__c).Name;
			}
		Prebill_Line_Item__c pbli = new Prebill_Line_Item__c(
			Line_Item_Quantity__c = oli.Quantity__c,
			Line_Item_Amount__c = oli.csord__Total_Price__c / oli.Quantity__c,
			Line_Item_Description__c = line_item_desc,
			Name = oli.Name,
			Order_Line_Item__c = oli.Id,
			M2M_Order_Request__c = oli.csord__Order__r.Order_Request__r.Id,
			M2M_Order_Request_Name__c = oli.csord__Order__r.Order_Request__r.Name
		);


		if(oli.csord__Order__r.Order_Request__c != null && oli.csord__Order__r.Order_Request__r.RecordTypeId == geotabsettings.Order_Request_Type_Id__c) {
			pbli.Is_Geotab__c = true;
		}
		else{
			pbli.Is_Geotab__c = false;
		}

		Prebill__c parentPrebill = this.pbliDistributor.findPrebill(oli);
		if (parentPrebill != null) {
			pbli.Prebill__c = parentPrebill.Id;
		}

		JournalMappingHelper.addInformation(pbli, journalMappings.get(oli.Journal_Mapping__c));

		oli.Is_Billed__c = true;
		system.debug('**IP** returning pbli '+ JSON.serializePretty(pbli));
		return pbli;
	}

	public void processQueued() {

		List<Prebill_Line_Item__c> prebillLineItems = new List<Prebill_Line_Item__c>();

		system.debug('**IP** processing queue '+ JSON.serializePretty(queue));

		for (sObject obj : queue) {
			try {
				if (obj instanceof csord__Service_Line_Item__c) {
					csord__Service_Line_Item__c sli = (csord__Service_Line_Item__c) obj;
					if (LineItems.hasQuantity(sli)) {
						prebillLineItems.add(process(sli));
					}
				} else if (obj instanceof csord__Order_Line_Item__c) {
					csord__Order_Line_Item__c oli = (csord__Order_Line_Item__c) obj;
					if (LineItems.hasQuantity(oli)) {
						prebillLineItems.add(process(oli));
					}
				}
				successesCount++;
			} catch (Exception e) {
				// catch all exceptions, and just add billing account to the list of failed billings and don't add to the success counter
				System.debug(e);
				System.debug('Error on line: ' + e.getLineNumber() + '\n' + e.getStackTraceString());
				Id billingAccountId;
				if (obj instanceof csord__Order_Line_Item__c) {
					csord__Order_Line_Item__c oli = (csord__Order_Line_Item__c) obj;
					billingAccountId = LineItems.billingAccountId(oli);
				} else if (obj instanceof csord__Service_Line_Item__c) {
					csord__Service_Line_Item__c sli = (csord__Service_Line_Item__c) obj;
					billingAccountId = LineItems.billingAccountId(sli);
				}
				failedBillingAccounts.add(new Billing_Account__c(Id = billingAccountId));
			}
			processedCount++;
		}
		system.debug('**IP** prebillLineItems: '+ JSON.serializePretty(prebillLineItems));
		pbliDistributor.linkParentsAndInsert(prebillLineItems);
		updateAndClear(queue);
	}

	private void updateAndClear(List<sObject> queue) {
		
		Integer j = 0;
        while (j < queue.size())
        {
          if(queue.get(j).id == null)
            {
                queue.remove(j);
            }
            else
                {
            j++;
                }
        }
		
		update queue;
		queue.clear();
		system.debug('**IP** queue after clear '+ JSON.serializePretty(queue));
	}

	private List<Billing_Account__c> selectBillingAccounts(Date billingDate) {
		return [
			Select
				id
			from
				Billing_Account__c
			where
				Payer__r.Billing_Cycle__c = :String.valueOf(billingDate.day())
		];
	}

	private Map<Id, Journal_Mapping__c> selectJournalMappingsMap() {
		return new Map<Id, Journal_Mapping__c>([
			SELECT
				Id, Name, Konto__c, KoSt__c, Prod__c, Unique_Identifier__c, I_CO__c, Projekt__c, Revenue__c
			FROM
				Journal_Mapping__c
		]);
	}
}