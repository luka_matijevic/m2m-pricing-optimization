global class GeotabUtils {
	/*
	* https://myadmin.geotab.com/sdk/#/api
	* Parameters - apiKey, apiOrderHeader, sessionId
	*/

	WebService static String PostOrder(Id orderRequestId) {
		GeotabCompleteOrderError error = null;
		String requestTmp;
		String response = 'response';
		List<String> responseOnCompleteOrder;
		Configurable_Sixt_helper__c csh = null;
		GEOTAB_Settings__c geotabSettings = GEOTAB_Settings__c.getInstance();
		String userInfoEndPoint = geotabSettings.UserInfoEndPoint__c;
		String postOrderEndPoint = geotabSettings.PostOrderEndPoint__c;
		String getUserContactsEndPoint = geotabSettings.GetUserContactsEndPoint__c;
		String getShippingFeesEndPoint = geotabSettings.GetShippingFeesEndPoint__c;
		String getWarrantyOptionsEndPoint = geotabSettings.GetWarrantyOptionsEndPoint__c;
		String getEditUserContactEndPoint = geotabSettings.EditUserContactEndPoint__c;
		String getOnlineOrderStatusEndPoint = geotabSettings.GetOnlineOrderStatusEndPoint__c;
		Map<String, AuthenticationResponseClass> authorizationResultMap = null;
		Map<String, List<GeotabUserContact>> geotabUserContactsResultMap = null;
		Map<String, List<GeotabShippingFee>> geotabShippingFeesResultMap = null;
		Map<String, List<GeotabWarrantyOption>> geotabWarrantyOptionResultMap = null;
		List<Product_Order__c> orReqProductOrders = null;
		List<Product_Configuration_Order__c> orReqProductConfigurationOrders = null;

		try {
            Order_Request__c orderRequest = [SELECT Id, Name, Boni_Check__c, Geotab_Contact__c, Geotab_Contact__r.Name,
            					Geotab_Contact__r.Geotab_Id__c, Geotab_Warranty_Option__c, Geotab_Warranty_Option__r.Name,
            					Geotab_Warranty_Option__r.Geotab_Id__c,
            					Geotab_Purchase_Order_Number__c, Boni_Check_Status__c, 
            					Opportunity__c, Frame_Contract__r.Contact__r.Name, Geotab_Reseller_Reference__c, 
            					Order_Posted_In_GEOTAB__c, Geotab_Order_Number__c,
            					Geotab_Device_Plan__c,
            					Geotab_Device_Plan__r.Geotab_Code__c
            					 FROM Order_Request__c WHERE Id = :orderRequestId];

            if(orderRequest != null) {
            	// if geotab contact empty order can not be created
            	if(orderRequest.Boni_Check_Status__c != null && orderRequest.Boni_Check_Status__c.equalsIgnoreCase('ACCEPTED') &&
            			orderRequest.Geotab_Contact__c != null && orderRequest.Geotab_Warranty_Option__c != null && orderRequest.Geotab_Device_Plan__c != null) {

            		if (!orderRequest.Order_Posted_In_GEOTAB__c){
            			List<Configurable_Sixt_helper__c> customConfig = [SELECT Id, Name,  Geotab_username__c, Geotab_password__c, Sixt_frame_contract__c, Sixt_account__c, Sixt_billing_account__c, Sixt_Opportunity_RecordType__c FROM Configurable_Sixt_helper__c WHERE Name = 'Configurable records' ORDER BY CreatedDate ASC];

	            		if (customConfig != null && customConfig.size() > 0)
						{
							csh = customConfig[0];
						}
						
						if (csh != null)
						{
							userInfoEndPoint = userInfoEndPoint + csh.Geotab_username__c + '&password=' + csh.Geotab_password__c;
						}
						requestTmp = userInfoEndPoint;
						HttpResponse resp = CaseUtils.executeRequest(userInfoEndPoint, 'GET', null, 32);

						if(resp != null && resp.getStatusCode() == 200) {
							authorizationResultMap = (Map<String, AuthenticationResponseClass>)JSON.deserialize(resp.getBody(), Map<String, AuthenticationResponseClass>.class);
							
							if (authorizationResultMap != null) {
								String userIdResult, sessionIdResult;
								List<GeotabAccount> geotabAccounts = null;
								Integer shipToUserContactId = 0;

								userIdResult = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').userId : null;
								sessionIdResult = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').sessionId : null;
								geotabAccounts = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').accounts : null;

								if(geotabAccounts != null && geotabAccounts.size() > 0) {
										
									getShippingFeesEndPoint = getShippingFeesEndPoint + 'apiKey="' + userIdResult + '"&sessionId="' + 
										sessionIdResult + '"';
									requestTmp = getShippingFeesEndPoint;
									resp = CaseUtils.executeRequest(getShippingFeesEndPoint, 'GET', null, 32);

									if(resp != null && resp.getStatusCode() == 200) {
										geotabShippingFeesResultMap = (Map<String, List<GeotabShippingFee>>)JSON.deserialize(resp.getBody(), Map<String, List<GeotabShippingFee>>.class);
										
										orReqProductOrders = [SELECT Id, Order_Request__c FROM Product_Order__c WHERE Order_Request__c = :orderRequest.Id];

										if(orReqProductOrders != null && orReqProductOrders.size() > 0) {
											orReqProductConfigurationOrders = [SELECT Id, Product_Configuration__c, Product_Order__c, Quantity__c FROM Product_Configuration_Order__c WHERE Product_Order__c = :orReqProductOrders];
											
											Map<String, Integer> prodConfOrderQuantityMap = new Map<String, Integer>();

											if(orReqProductConfigurationOrders != null && orReqProductConfigurationOrders.size() > 0) {
												for (Product_Configuration_Order__c pco : orReqProductConfigurationOrders){
													prodConfOrderQuantityMap.put(pco.Product_Configuration__c, Integer.valueOf(pco.Quantity__c));
												}

												/*List<cscfga__Attribute__c> attributes = [SELECT Id, Name, cscfga__Value__c 
								            		FROM cscfga__Attribute__c  
								            		WHERE cscfga__Product_Configuration__c = :orReqProductConfigurationOrders[0].Product_Configuration__c];*/

								            	List<cscfga__Attribute__c> attributes = [SELECT Id, Name, cscfga__Value__c, cscfga__Product_Configuration__c 
								            		FROM cscfga__Attribute__c  
								            		WHERE cscfga__Product_Configuration__c = :prodConfOrderQuantityMap.keyset() and Name in ('Article Info Set', 'Quantity')];

								            	List<ApiOrderItem> orderItems = new List<ApiOrderItem>();
												

												Map<String, String> articleInfoSetQuantityMap = new Map<String, String>();
												for (String prodConfId : prodConfOrderQuantityMap.keyset()){
													String articleInfoSetValue;
													String quantityConf;
													for (cscfga__Attribute__c attr : attributes) {
														if (attr.cscfga__Product_Configuration__c == prodConfId){
															if(attr.Name.equalsIgnoreCase('Article Info Set')) {
																articleInfoSetValue = String.valueOf(attr.cscfga__Value__c);
															}
															quantityConf = String.valueOf(prodConfOrderQuantityMap.get(prodConfId));
														}
													}

													if (!String.isEmpty(articleInfoSetValue) && !String.isEmpty(quantityConf)){
														articleInfoSetQuantityMap.put(articleInfoSetValue, quantityConf);
													}
												}

												List<Article_Info_Set__c> articleInfoList = [SELECT Id, Article_Code__c, Geotab_Hardware_Code__c, Name FROM Article_Info_Set__c WHERE Id = :articleInfoSetQuantityMap.keyset() and Geotab_Hardware_Code__c <> null];

												for (Article_Info_Set__c ais : articleInfoList){
													ApiOrderItem orderItem = new ApiOrderItem();
													orderItem.ProductCode = ais.Geotab_Hardware_Code__c;
													orderItem.Quantity = Integer.valueOf(articleInfoSetQuantityMap.get(ais.Id));
													orderItems.add(orderItem);
												}
												//System.debug(orderItems);
												if (orderItems != null && orderItems.size() > 0){
													List<GeotabShippingFee> shippingFeesList = geotabShippingFeesResultMap.get('result');
													responseOnCompleteOrder = CompleteOrder(orderItems, orderRequest, geotabAccounts, userIdResult,
														sessionIdResult, postOrderEndPoint, error, getOnlineOrderStatusEndPoint);
												}
												else{
													response = 'Product Configuration is missing Attributes!';
												}

						            			/*if(attributes != null && attributes.size() > 0) {
						            				for(cscfga__Attribute__c attr : attributes) {
						            					if(attr.Name != null) {
						            						if(attr.Name.equalsIgnoreCase('Article Info Set')) {
						            							string articleId = attr.cscfga__Value__c;

						            							List<Article_Info_Set__c> articleInfo = [SELECT Id, Article_Code__c, Geotab_Hardware_Code__c, Name FROM Article_Info_Set__c WHERE Id = :articleId];

						            							if(articleInfo != null && articleInfo.size() > 0) {
						            								Article_Info_Set__c ais = articleInfo[0];

						            								if(ais != null) {
						            									orderItem.ProductCode = ais.Geotab_Hardware_Code__c;
						            								}
						            							}
						            						} else if (attr.Name.equalsIgnoreCase('Quantity')) {
																orderItem.Quantity = Integer.valueOf(attr.cscfga__Value__c);
						            						}
						            					}
						            				}

													orderItems.add(orderItem);

													List<GeotabShippingFee> shippingFeesList = geotabShippingFeesResultMap.get('result');

													response = CompleteOrder(orderItems, orderRequest, geotabAccounts, userIdResult,
														sessionIdResult, postOrderEndPoint);
					            				} else {
													response = 'Product Configuration is missing Attributes!';
					            				}*/
											} else {
												response = 'Order Request ' + orderRequest.Name + ' is missing Product Configuration Orders!';
											}
										} else {
											response = 'Order Request ' + orderRequest.Name + ' is missing Product Orders!';
										}
									} else {
										response = 'Fetching Shipping Fees from Geotab was unsuccessful!';
									}								
								} else {
									response = 'Fetching Accounts from Geotab was unsuccessful!';
								}
							}
	    				} else {
	    					response = 'User authorization failed!';
	    				}
            		}
            		else{
            			response = 'Order already created in GEOTAB! GEOTAB Order Number: ' + orderRequest.Geotab_Order_Number__c;
            		}
            	} else {
            		response = 'Order in Geotab was not created. Boni Check is not accepted or check Geotab Contact, Geotab Warrant Option or Geotab Device Plan field!';
            	}

            	if (responseOnCompleteOrder != null && responseOnCompleteOrder.size() == 4){
            		InsertGeotabOrderLog(orderRequest.Id, responseOnCompleteOrder, responseOnCompleteOrder[3]);
            		if (error != null){
            			response = 'Error: +\n' + error.error.message;
            		}
            		else{
            			response = responseOnCompleteOrder[0];
            		}
            	}
            	else{
            		InsertGeotabOrderLog(orderRequest.Id, response, requestTmp);
            	}
            }
    	} catch(Exception e){
    		response = e.getMessage();
    	}

    	return response;
	}

	public static List<String> CompleteOrder(List<ApiOrderItem> orderItems, Order_Request__c orderRequest, List<GeotabAccount> geotabAccounts,
			String userIdResult, String sessionIdResult, String postOrderEndPoint, GeotabCompleteOrderError error, String getOrderStatusEndPoint) {

		List<String> responseValues = new List<String>();
		ApiOrderHeader geotabOrderHeader = new ApiOrderHeader();
		geotabOrderHeader.OrderItems = orderItems;
		geotabOrderHeader.PurchaseOrderNo = orderRequest.Geotab_Purchase_Order_Number__c;
		geotabOrderHeader.DevicePlanLevel = Integer.valueOf(orderRequest.Geotab_device_plan__r.Geotab_Code__c);
		geotabOrderHeader.ForAccount = geotabAccounts[0].AccountId;
		geotabOrderHeader.ShipToId = Integer.valueOf(orderRequest.Geotab_Contact__r.Geotab_Id__c);
		geotabOrderHeader.ShippingFeeId = orderRequest.Geotab_Shipping__c == null ? 1 : Integer.valueOf(orderRequest.Geotab_Shipping__c);//shippingFeesList[0].Id;
		geotabOrderHeader.WarrantyOptionId = Integer.valueOf(orderRequest.Geotab_Warranty_Option__r.Geotab_Id__c);
		geotabOrderHeader.ResellerReference = orderRequest.Geotab_Reseller_Reference__c;
		geotabOrderHeader.PromoCode = '';
		geotabOrderHeader.LeadId = '';
		geotabOrderHeader.LeadIdZip = '';
		geotabOrderHeader.Comments = '';
		geotabOrderHeader.InternalComments = '';
	        
	    String jsonBody = JSON.serialize(geotabOrderHeader);

	    String tmpString = 'apiKey="' + userIdResult + '"&apiOrderHeader=' + jsonBody + '&sessionId="' + 
			sessionIdResult + '"';

	    tmpString = EncodingUtil.urlEncode(tmpString, 'UTF-8');
		tmpString = tmpString.replace('+', '%20'); // Salesforce bug

		postOrderEndPoint = postOrderEndPoint + tmpString;

		HttpResponse resp = CaseUtils.executeRequest(postOrderEndPoint, 'GET', null, 32);

		if(resp != null && resp.getStatusCode() == 200) {
			responseValues.add('Order successfully posted in Geotab, Purchase Order Number is ' + orderRequest.Geotab_Purchase_Order_Number__c);
			responseValues.add(String.valueOf(resp.getStatusCode()));
			responseValues.add(String.ValueOf(resp.getBody()));
			if (!String.isEmpty(resp.getBody()) && String.valueOf(resp.getBody()) == '{}'){
				//body is empty, get order geotab id and set checkbox and geotab id on request.
				if (orderRequest.Order_Posted_In_GEOTAB__c == false){
					orderRequest.Order_Posted_In_GEOTAB_Date__c = System.now();
				}
				orderRequest.Order_Posted_In_GEOTAB__c = true;
				update orderRequest;
			}
			else if (!String.isEmpty(resp.getBody())){
				try{
					error = (GeotabCompleteOrderError)JSON.deserialize(resp.getBody(), GeotabCompleteOrderError.class);
				}
				catch(Exception ex){

				}
			}
			//Adam Mehtic
			//return 'Order successfully posted in Geotab, Purchase Order Number is ' + orderRequest.Geotab_Purchase_Order_Number__c;
		} else {
			responseValues.add('Posting order in Geotab failed! Exception: ' + resp.getBody());
			responseValues.add(String.valueOf(resp.getStatusCode()));
			responseValues.add(String.ValueOf(resp.getBody()));
			//Adam Mehtic
			//return 'Posting order in Geotab failed! Exception: ' + resp.getBody();
		}
		responseValues.add(postOrderEndPoint);
		return responseValues;
	}

	
	public static void InsertGeotabOrderLog(Id orderRequestId, String message) {		
		Geotab_orders_log__c gol = new Geotab_orders_log__c();

		gol.Order_Request__c = orderRequestId;
		gol.Response_message__c = message;

		insert gol;
	}

	public static void InsertGeotabOrderLog(Id orderRequestId, String message, String requestIn) {		
		Geotab_orders_log__c gol = new Geotab_orders_log__c();

		gol.Order_Request__c = orderRequestId;
		gol.Response_message__c = message;
		gol.Request__c = requestIn;

		insert gol;
	}

	public static void InsertGeotabOrderLog(Id orderRequestId, String[] responseValuesIn, String sfRequestIn) {		
		Geotab_orders_log__c gol = new Geotab_orders_log__c();

		gol.Order_Request__c = orderRequestId;
		gol.Response_message__c = responseValuesIn[0];
		gol.Response_Status_Code__c = responseValuesIn[1];
		gol.Original_response__c = responseValuesIn[2];
		gol.Request__c = sfRequestIn;

		insert gol;
	}

	public class ApiOrderHeader {
		public String Comments { get; set; }
		public Integer DevicePlanLevel { get; set; } //required
		public String ForAccount { get; set; } //required
		public String InternalComments { get; set; }
		public String LeadId { get; set; }
		public String LeadIdZip { get; set; }
		public List<ApiOrderItem> OrderItems { get; set; } //required
		public String PromoCode { get; set; }
		public String PurchaseOrderNo { get; set; } //required
		public String ResellerReference { get; set; }
		public Integer ShipToId { get; set; } //required
		public Integer ShippingFeeId { get; set; } //required
		public Integer WarrantyOptionId { get; set; } //required
	}

	public class ApiOrderItem {
		public String ProductCode { get; set; }
		public Integer Quantity { get; set; }
	}

	public class AuthenticationResponseClass {
		public String userId { get; set; }
		public String sessionId { get; set; }
		public Datetime lastLogonDate { get; set; }
		public List<GeotabAccount> accounts { get; set; }
		public String name { get; set; }
		public List<AuthenticationRoles> roles { get; set; }
	}

	public class GeotabOnlineOrder {
		public GeotabAccount Account { get; set; }
		public String Comments { get; set; }
		public String CurrentStatus { get; set; }
		public List<ApiOrderItem> OnlineOrderItems  { get; set; }
		public String OrderDate { get; set; }
		public Decimal OrderMonthlyTotalUsd { get; set; }
		public String OrderNo { get; set; }
		public Decimal OrderTotalUsd { get; set; }
		public String OrderedBy { get; set; }
		public String PurchaseOrderNo { get; set; }
		public String RateCode { get; set; }
		public String ResellerReference { get; set; }
	}

	public class GeotabDeviceOrderEntry {
		public GeotabAccount Account { get; set; }
		public String Comments { get; set; }
		public GeotabDevice device { get; set; }
		public Integer DevicePlanLevel { get; set; }
		public String PurchaseOrderNo { get; set; }
		public String RateCode { get; set; }
	}

	public class GeotabDevice {
		public Integer Id { get; set; }
		public String SerialNumber { get; set; }
	}

	global class GeotabUserContact {
		public Boolean Active { get; set; }
		public String Address { get; set; }
		public String City { get; set; }
		public String ContactEmail { get; set; }
		public String ContactName { get; set; }
		public String Country { get; set; }
		public String DisplayName { get; set; }
		public Integer Id { get; set; }
		public String State { get; set; }
		public String Street1 { get; set; }
		public String Street2 { get; set; }
		public String Telephone1 { get; set; }
		public String Telephone2 { get; set; }
		public GeotabUserCompany UserCompany { get; set; }
		public String ZipCode { get; set; }
	}

	public class GeotabUserCompany {
		public GeotabAccount Account { get; set; }
		public Boolean Active { get; set; }
		public Integer Id { get; set; }
	}

	global class GeotabAccount {
		public String AccountId { get; set; }
		//public GeotabCurrency currency { get; set; }
	}

	public class GeotabShippingFee {
		public String Name { get; set; }
		public String Code { get; set; } // Possible values - SHIP-STD, SHIP-PRI or LOCAL
		public Integer Id { get; set; }
	}

	global class GeotabWarrantyOption {
		public String Name { get; set; }
		public Integer Id { get; set; }
	}

	public class GeotabCurrency {
		public String Code { get; set; }
	}

	public class AuthenticationRoles {
		public String comments { get; set; }
		public String name { get; set; }
	}

	public class InstallLogsClass {
		public InstallLogsRequest request { get; set; }
		public Datetime resultDateUtc { get; set; }
		public Datetime lastServerCommunication { get; set; }
		public String simNumber { get; set; }
		public Boolean simActive { get; set; }
		public String comments { get; set; }
		public String possibleIssues { get; set; }
		public String firmwareVersion { get; set; }

	}

	public class InstallLogsRequest {
		public InstallLogsRequestDevice device { get; set; }
		public String installerName { get; set; }
		public String installerCompany { get; set; }
		public String asset { get; set; }
	}

	public class InstallLogsRequestDevice {
		public String id { get; set; }
		public String serialNumber { get; set; }
		public InstallLogsRequestDeviceType deviceType { get; set; }
	}

	public class InstallLogsRequestDeviceType {
		public String name {get;set;}
	}

	/***********************************************************
	CLASSES GENERATED FROM JSON
	Petar Matkovic
	*************************************************************/
	public class ErrorInner
	{
	    public string name { get; set; }
	    public string message { get; set; }
	}

	public class Error
	{
	    public string name { get; set; }
	    public string message { get; set; }
	    public List<ErrorInner> errors { get; set; }
	}

	public class GeotabCompleteOrderError
	{
	    public Error error { get; set; }
	}

	public class Account
	{
	    public string accountId { get; set; }
	    //public Currency currency { get; set; }
	}

	public class Currency2
	{
	    public string code { get; set; }
	    public string name { get; set; }
	}

	public class Account2
	{
	    public string accountId { get; set; }
	    //public Currency2 currency { get; set; }
	}

	public class UserCompany
	{
	    public Integer id { get; set; }
	    public Account2 account { get; set; }
	    public boolean active { get; set; }
	    public string name { get; set; }
	}

	public class ShippingContact
	{
	    public Integer id { get; set; }
	    public UserCompany userCompany { get; set; }
	    public string contactName { get; set; }
	    public string contactEmail { get; set; }
	    public string telephone1 { get; set; }
	    public string telephone2 { get; set; }
	    public string street1 { get; set; }
	    public string city { get; set; }
	    public string country { get; set; }
	    public string zipCode { get; set; }
	    public boolean active { get; set; }
	    public string displayName { get; set; }
	    public string address { get; set; }
	    public string state { get; set; }
	}

	public class Currency3
	{
	    public string code { get; set; }
	    public string name { get; set; }
	}

	public class ActiveCurrencyRate
	{
	    //public Currency3 currency { get; set; }
	    public double factor { get; set; }
	    public string effectiveFrom { get; set; }
	}

	public class DevicePlan
	{
	    public Integer id { get; set; }
	    public Integer level { get; set; }
	    public boolean validForOrder { get; set; }
	    public string name { get; set; }
	}

	public class OnlineOrderItem
	{
	    public string productCode { get; set; }
	    public string productDescription { get; set; }
	    public Integer quantity { get; set; }
	    public double unitCost { get; set; }
	    public double monthlyCost { get; set; }
	    public string itemStatus { get; set; }
	}

	public class Currency4
	{
	    public string code { get; set; }
	    public string name { get; set; }
	}

	public class Account3
	{
	    public string accountId { get; set; }
	   // public Currency4 currency { get; set; }
	}

	public class Currency5
	{
	    public string code { get; set; }
	    public string name { get; set; }
	}

	public class Account4
	{
	    public string accountId { get; set; }
	    //public Currency5 currency { get; set; }
	}

	public class UserCompany2
	{
	    public Integer id { get; set; }
	    public Account4 account { get; set; }
	    public boolean active { get; set; }
	    public string name { get; set; }
	}

	public class UserContact
	{
	    public Integer id { get; set; }
	    public UserCompany2 userCompany { get; set; }
	    public string contactName { get; set; }
	    public string contactEmail { get; set; }
	    public string telephone1 { get; set; }
	    public string telephone2 { get; set; }
	    public string street1 { get; set; }
	    public string city { get; set; }
	    public string country { get; set; }
	    public string zipCode { get; set; }
	    public boolean active { get; set; }
	    public string displayName { get; set; }
	    public string address { get; set; }
	    public string state { get; set; }
	}

	public class ShipItem
	{
	    public Account3 account { get; set; }
	    public string shipDate { get; set; }
	    public string purchaseOrderNo { get; set; }
	    public string erpReference { get; set; }
	    public UserContact userContact { get; set; }
	    public string trackingNo { get; set; }
	    public string trackingURL { get; set; }
	}

	public class Result
	{
	    public Account account { get; set; }
	    public boolean onHold { get; set; }
	    public string purchaseOrderNo { get; set; }
	    public string orderNo { get; set; }
	    public string orderDate { get; set; }
	    public string estimatedCompletionDate { get; set; }
	    public ShippingContact shippingContact { get; set; }
	    public ActiveCurrencyRate activeCurrencyRate { get; set; }
	    public string comments { get; set; }
	    public string orderedBy { get; set; }
	    public double orderTotalUsd { get; set; }
	    public double orderMonthlyTotalUsd { get; set; }
	    public DevicePlan devicePlan { get; set; }
	    public List<OnlineOrderItem> onlineOrderItems { get; set; }
	    public List<ShipItem> shipItems { get; set; }
	    public string currentStatus { get; set; }
	    public string resellerReference { get; set; }
	    public string rateCode { get; set; }
	}

	public class RootResponse
	{
	    public List<Result> result { get; set; }
	}
}