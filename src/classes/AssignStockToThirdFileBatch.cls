public class AssignStockToThirdFileBatch {
	private class TypeException extends Exception{}
	
	public static void assignStockToThirdFile(Set<String> dispatchedSIMCards, Id thirdUploadFileId, String tufType)
	{
		Savepoint sp = Database.setSavepoint();

		Integer countOfRecords = 0;
		boolean hasError = false;

		Set<String> controlIccidsSet = new Set<String>();
		controlIccidsSet.addAll(dispatchedSIMCards);
		try
		{
			List<String> errorIccidList = new List<String>();
			List<Stock__c> stocks = [SELECT ICCID__c, Third_Upload_File__c, Status__c, SIM_Range_Origin__c FROM Stock__c WHERE ICCID__c IN :dispatchedSIMCards AND Status__c = 'On stock'];
			for (Stock__c stock : stocks){
				countOfRecords++;
				if ((stock.SIM_Range_Origin__c == tufType ) || (tufType == 'Local' && (stock.SIM_Range_Origin__c == 'Blue' || stock.SIM_Range_Origin__c == 'Green'))){
        			stock.Third_Upload_File__c = thirdUploadFileId;
	        		stock.Status__c = 'Reserved';	
	        		controlIccidsSet.remove(stock.ICCID__c);
	        	}else{
	        		hasError = true;
	        		errorIccidList.add(stock.ICCID__c);
        			/*try {
        				String error = 'Check TUF and Stock types. Global TUF can have only Global Stocks. Local TUF can have Blue or Green Stocks.';
        				throw new TypeException (error);	
        			}
        			catch (TypeException e){
        				Database.rollback(sp);
        				throw e;
        			}*/
        		}
			}

			if (hasError){
				try{
    				String error = 'Check TUF and Stock types. Global TUF can have only Global Stocks. Local TUF can have Blue or Green Stocks.';
    				for (String iccid : errorIccidList){
    					error = error + '<br/>' + iccid;
    				}
    				throw new TypeException (error);	
    			}
    			catch (TypeException e){
    				Database.rollback(sp);
    				throw e;
    			}
			}

			update stocks;
			/*
			for (List<Stock__c> stock: [SELECT Third_Upload_File__c, Status__c, SIM_Range_Origin__c FROM Stock__c WHERE ICCID__c IN :dispatchedSIMCards AND Status__c = 'On stock'])
			{
	        	for(Stock__c item : stock)
	        	{
	        		countOfRecords++;
	        		if ( (item.SIM_Range_Origin__c == tufType ) || (tufType == 'Local' && (item.SIM_Range_Origin__c == 'Blue' || item.SIM_Range_Origin__c == 'Green'))){
	        			item.Third_Upload_File__c = thirdUploadFileId;
		        		item.Status__c = 'Reserved';	
	        		}
	        		else
	        		{
	        			try {
	        				String error = 'Check TUF and Stock types. Global TUF can have only Global Stocks. Local TUF can have Blue or Green Stocks.';
	        				throw new TypeException (error);	
	        			}
	        			catch (TypeException e){
	        				Database.rollback(sp);
	        				throw e;
	        			}
	        			
	        		}
		        	
	    	    }
	        	update stock;
        	}*/
		}
		catch(Exception ex)
		{
			Database.rollback(sp);
			throw ex;
		}

        if(countOfRecords != dispatchedSIMCards.size())
        {
        	Database.rollback(sp);
        	//controlIccidsSet containst all iccids that weren't fetched from database
        	String message = 'Can\'t load all ICCIDs, check stock status (should be \'On stock\') and account defined in the sim range (has to be the same as TUF account)!';
        	for (String iccid : controlIccidsSet){
        		message = message + '<br/>' + iccid;
        	}
        	throw new DispatchedAmountsDoNotMatchException(message);
        }
    }

    public class DispatchedAmountsDoNotMatchException extends Exception{}

}