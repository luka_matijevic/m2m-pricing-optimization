global class RetrieveMissingPrebillsBatch implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts {

	private List<Billing_Account__c> billingAccountsList;

	public RetrieveMissingPrebillsBatch() {
		
	}

	public RetrieveMissingPrebillsBatch(List<Billing_Account__c> billingAccountsList) {
		this.billingAccountsList = billingAccountsList;
	}

	global Iterable<sObject> start(Database.BatchableContext bc)
    {
        return billingAccountsList;
    }

    global void execute(Database.BatchableContext bc, List<SObject> scope)
    {
        for (Billing_Account__c ba : (List<Billing_Account__c>)scope)
        {
        	csam_t1.ObjectGraphCalloutHandler.queueMessageFromId('Retrieve Prebill', ba.Id);
        }
    }

    global void finish(Database.BatchableContext bc)
    {
        
    }
}