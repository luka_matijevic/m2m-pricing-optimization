global class BatchHardActivateIoTMobilitySIMCards implements Database.Batchable<sObject> {
	
	Date dateForQuery;
	String query;
	GEOTAB_Settings__c geotabSettins = GEOTAB_Settings__c.getInstance();
	List<Stock__c> stockList;
	String pendingActivation = 'Pending activation';
	String recordTypeForQuery;

	global BatchHardActivateIoTMobilitySIMCards() {
		dateForQuery = System.today();
		dateForQuery = dateForQuery.addDays(-90);
		recordTypeForQuery = geotabSettins.Order_Request_Type_Id__c;
	}

	global BatchHardActivateIoTMobilitySIMCards(Date dateIn) {
		dateForQuery = dateIn;
		recordTypeForQuery = geotabSettins.Order_Request_Type_Id__c;
	}

	global BatchHardActivateIoTMobilitySIMCards(Id recordTypeIdIn) {
		recordTypeForQuery = recordTypeIdIn;
		dateForQuery = System.today();
	}

	global BatchHardActivateIoTMobilitySIMCards(Date dateIn, Id recordTypeIdIn) {
		dateForQuery = dateIn;
		recordTypeForQuery = recordTypeIdIn;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		query = 'SELECT ID, SM2M_Status__c, Status__c '+
				'FROM Stock__c '
				+'WHERE Order_request__r.RecordTypeId = \'' + recordTypeForQuery +'\' '
				+'AND SM2M_Status__c in (\'INACTIVE_NEW\', \'ACTIVATION_READY\') '
				+'AND DAY_ONLY(CREATEDDATE) = ' + String.valueOf(dateForQuery);
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		stockList = (List<Stock__c>)scope;
		if (stockList != null){
			for (Stock__c stock : stockList){
				stock.Status__c = pendingActivation;
			}
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		if (stockList != null && stockList.size() > 0){
			update stockList;
		}
	}
}