/**
 *  Batch job that cleans up the generated export data for the given Product Bundles.
 */
public class ProductBundleExportDataCleanupJob implements Database.Batchable<Id> {

    Id[] bundleIds;

    public ProductBundleExportDataCleanupJob (Id bundleId) {
        this.bundleIds = new Id[] { bundleId };
    }
    public ProductBundleExportDataCleanupJob (Id[] bundleIds) {
        this.bundleIds = bundleIds;
    }

    public Id[] start(Database.BatchableContext bc) {
        return this.bundleIds;
    }
    
    public void execute(Database.BatchableContext BC, Id[] bundleIds) {
        delete [select Id 
                from Product_Bundle_Export_Data__c
                where Product_Bundle__c IN :bundleIds];
    }
    
    public void finish(Database.BatchableContext BC) {
    }
}