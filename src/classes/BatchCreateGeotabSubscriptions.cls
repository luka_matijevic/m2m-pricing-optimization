/*
	Petar Matkovic 23.11.2016.
	1. Get Today Activated Stocks
	2. Get Stock's Order Request
	3. Create Order and Subscriptions with services etc
*/
global class BatchCreateGeotabSubscriptions implements Database.Batchable<sObject>, Database.Stateful {
	String recurringHardwareServiceLineItemName = 'Recurring Target Fee Total';
	String query;
	GEOTAB_Settings__c geotabSettings = GEOTAB_Settings__c.getInstance();
	List<Order_Request__c> orderRequestsForProcess = new List<Order_Request__c>();
	List<Stock__c> stocksForProcess = new List<Stock__c>();

	global BatchCreateGeotabSubscriptions() {

	}
	
	global List<Order_Request__c> start(Database.BatchableContext BC) {
		getOrdersAndStocksForProcess();
		//system.debug('finish with start method');
		return orderRequestsForProcess;
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		//system.debug('enter execute method');
		List<Order_request__c> hybridPaymentTypeOrderRequestList = extractOrderRequestsByPaymentType(orderRequestsForProcess, 'Hybrid');
		List<Order_request__c> standardPaymentTypeOrderRequestList = extractOrderRequestsByPaymentType(orderRequestsForProcess, 'Standard');
		List<Csord__Order__c> existingOrders = getOrdersForOrderRequests(orderRequestsForProcess);
		//System.debug(standardPaymentTypeOrderRequestList);
		//System.debug(hybridPaymentTypeOrderRequestList);
		//System.debug(existingOrders);
		processStandardPaymentOrderRequests(standardPaymentTypeOrderRequestList, existingOrders);
		processHybridPaymentOrderRequests(hybridPaymentTypeOrderRequestList, existingOrders);
		//system.debug('finish execute method');
	}
	
	global void finish(Database.BatchableContext BC) {
		update stocksForProcess;
	}


	//fetch all today activated stocks and their Order Requests
	private void getOrdersAndStocksForProcess(){
		stocksForProcess = [SELECT 	Id, GEOTAB_Processed__c, Account__c, 
									Activation_date__c, Iccid__c, Sm2m_status__c, Order_request__c, Order_request__r.Opportunity__c
							FROM 	Stock__c 
							WHERE 	order_request__r.RecordTypeId = :geotabSettings.Order_Request_Type_Id__c
									AND GEOTAB_Processed__c = false
									AND Sm2m_status__c = 'ACTIVE'];

		Set<Id> ordReqIdSet = new Set<Id>();
		for (Stock__c stock : stocksForProcess){
			ordReqIdSet.add(stock.order_request__c);
			stock.GEOTAB_Processed__c = true;
		}

		orderRequestsForProcess = [SELECT 	Id, Name, Order_quantity__c, Runtime__c, Geotab_Payment_Type__c, Boni_Check__c,
											Boni_Check__r.Status_of_CC__c, Billing_Account__c, Billing_Account__r.Trial__c,
											Billing_Account__r.OFI_ID__c, Billing_Account__r.Payer__c, Billing_Account__r.Payer__r.BillingCountry
									FROM	Order_Request__c
									WHERE Id in :ordReqIdSet and RecordTypeId = :geotabSettings.Order_Request_Type_Id__c];
	}

	private List<Order_request__c> extractOrderRequestsByPaymentType(List<Order_Request__c> listIn, String paymentTypeIn){
		List<Order_Request__c> retList = new List<Order_Request__c>();
		for (Order_request__c ordReq : listIn){
			if (String.valueOf(ordReq.Geotab_Payment_Type__c) == paymentTypeIn){
				retList.add(ordReq);
			}
		}
		return retList;
	}

	private List<Csord__Order__c> getOrdersForOrderRequests(List<Order_Request__c> ordReqs){
		List<Csord__Order__c> orderList = [SELECT Id, Name, Order_request__c
											FROM Csord__Order__c 
											WHERE Order_request__c in :ordReqs];
		return orderList;
	}

	/*For each standard payment type Order Request
		check if order is created
			if order is created - hardware line items are added to order
			if order is not created - create new and add hardware line items to order

		if subscription is created for order, add new service with activation date today and quantity of today activated stock quantity
		if subscription is not created, create new one and add service with activation date today and quantity of today activated stock quantity
	*/
	private void processStandardPaymentOrderRequests(List<Order_request__c> orderRequestListIn, List<Csord__Order__c> existingOrderListIn){
		for (Order_request__c orderRequest : orderRequestListIn){
			Boolean orderCreated = false;
			Id orderId = null;
			//check if order is created
			for (Csord__Order__c order : existingOrderListIn){
				if (order.Order_request__c == orderRequest.Id){
					orderCreated = true;
					orderId = order.Id;
					break;
				}
			}

			//if order not created, create new order with order line items
			if (!orderCreated){
				orderId = Factory.createCreateOrder(orderRequest.Id).create();
			}

			//get number of activated stocks for order request
			Integer serviceQuantity = 0;
			for (Stock__c stock : stocksForProcess){
				if (stock.order_request__c == orderRequest.Id){
					serviceQuantity++;
				}
			}
			System.debug(serviceQuantity);
			createSubscriptionsForStandardPaymentType(orderId, serviceQuantity);
		}
	}

	private void processHybridPaymentOrderRequests(List<Order_request__c> orderRequestListIn, List<csord__Order__c> existingOrderListIn){
		for (Order_request__c orderRequest : orderRequestListIn){
			//if order is created, at least one sim had already been activated and hardware recurring billing had been started
			//if order is not created, create subscription for all hardwares as recurring charge and subscription for service with quantity of activated stock quantity
			Boolean orderCreated = false;
			Id orderId = null;
			//check if order is created
			for (Csord__Order__c order : existingOrderListIn){
				if (order.Order_request__c == orderRequest.Id){
					orderCreated = true;
					orderId = order.Id;
					break;
				}
			}

			//if order not created, create new order without order line items (hardwares)
			//hardware will be billed recurring
			if (!orderCreated){
				orderId = createOrderForHybridPayment(orderRequest);
				//create subscription and service for all hardware in order request
				//TO-DO
			}

			//create service for software(service) with quantity of today activated sim number
			//get number of activated stocks for order request
			Integer serviceQuantity = 0;
			for (Stock__c stock : stocksForProcess){
				if (stock.order_request__c == orderRequest.Id){
					serviceQuantity++;
				}
			}
			createSubscriptionForHybridPayment(orderId, serviceQuantity, !orderCreated);
		}
	}

	/**********************************************************************************
	Factory code with some changes onwards
	**********************************************************************************/
	private void createSubscriptionsForStandardPaymentType(Id orderIdIn, Integer quantityIn){
		IInsertHelper insertHelper = new InsertHelper();
		//
		// Retrieve order
		//
		csord__Order__c order =
				[ SELECT Id, Order_Request__c, csord__Order_Request__c,
				  Order_Request__r.Name, Order_Request__r.Billing_Account__c,
				  Order_Request__r.Billing_Account__r.Name,
				  Order_Request__r.Billing_Account__r.Payer__r.BillingCountry,
				  Order_Request__r.Frame_Contract__r.Customer__c
				  FROM csord__Order__c
				  WHERE Id = :orderIdIn];
		//
		// Retrieve product orders
		//
		Map<Id, Product_Order__c> productOrderMap = new Map<Id, Product_Order__c>(
				[ SELECT Id, Quantity__c
				  FROM Product_Order__c
				  WHERE Order_Request__c = :order.Order_Request__c ]);
		//
		// Retrieve product configuration orders
		//
		Product_Configuration_Order__c[] productConfigurationOrders =
				[ SELECT Id, Product_Configuration__c, Quantity__c
				  FROM Product_Configuration_Order__c
				  WHERE Product_Order__c IN :productOrderMap.keySet() ];
		//
		// Create map of product configuration order regarding product configuration id
		//
		Map<Id, Product_Configuration_Order__c> productConfigurationOrderMap =
			new Map<Id, Product_Configuration_Order__c>();
		for (Product_Configuration_Order__c pco : productConfigurationOrders)
		{
			productConfigurationOrderMap.put(pco.Product_Configuration__c, pco);
		}

		//
		// Retrieve product configuration names
		//
		cscfga__Product_Configuration__c[] configs =
				[ SELECT Id, Name, Original_Product_Name__c,
				  cscfga__Contract_Term__c, parent_product_configuration__c,
				  Billing_Rate_Model__c,
				  ( SELECT Id, Name, cscfga__Is_Line_Item__c, cscfga__Recurring__c,
				    cscfga__Price__c, cscfga__Value__c, cscfga__Product_Configuration__c,
				    cscfga__Line_Item_Description__c
				    FROM cscfga__Attributes__r
				    WHERE ( ( cscfga__Is_Line_Item__c = true
							AND cscfga__Recurring__c = true
							AND cscfga__Product_Configuration__r.Original_Product_Name__c != 'Connectivity')
						  OR Name = :Constants.ATTRIBUTE_NAME_QUANTITY
						  OR Name = :Constants.ATTRIBUTE_NAME_ARTICLE_INFO_SET) )
				  FROM cscfga__Product_Configuration__c
				  WHERE parent_product_configuration__c = null
				  AND Id IN :productConfigurationOrderMap.keySet() ];

		//
		// Put quantities to products
		//
		Map<Id, Integer> configurationQuantities = new Map<Id, Integer>();
		Map<Id, Id> configurationArticleInfoSets = new Map<Id, Id>();
		for (cscfga__Product_Configuration__c config : configs)
		{
			System.debug('Configuration: ' + config);
			for (cscfga__Attribute__c att : config.cscfga__Attributes__r)
			{
				System.debug('Attribute: ' + att);
				if (att.Name == Constants.ATTRIBUTE_NAME_QUANTITY)
				{
					configurationQuantities.put(att.cscfga__Product_Configuration__c, Integer.valueOf(att.cscfga__Value__c));
				}
				else if (att.Name == Constants.ATTRIBUTE_NAME_ARTICLE_INFO_SET)
				{
					try
					{
						System.debug('Putting article info set: ' + att);
						configurationArticleInfoSets.put(att.cscfga__Product_Configuration__c, att.cscfga__Value__c);
					}
					catch(Exception e)
					{
						System.debug('Wrong Id in attribute: ' + att.cscfga__Value__c);
						throw e;
					}
				}
			}
		}
		//
		// Get journal mappings
		//
		Map<Id, Article_Info_Set__c> articleInfoSets = new Map<Id, Article_Info_Set__c>(
				[ SELECT Id, Journal_Mapping__c
				  FROM Article_Info_Set__c
				  WHERE Id IN :configurationArticleInfoSets.values() ]);
		Set<String> journalMappingNames = new Set<String>();
		for (Article_Info_Set__c ais : articleInfoSets.values())
		{
			journalMappingNames.add(ais.Journal_Mapping__c);
		}

		String country = getCountry(order.Order_Request__r.Billing_Account__r.Payer__r.BillingCountry);

		Journal_Mapping__c[] journalMappings =
				[ SELECT Id, Name
				  FROM Journal_Mapping__c
				  WHERE Name IN :journalMappingNames
				  AND Country__c = :country ];
		Map<String, Id> journalMappingMap = new Map<String, Id>();
		for (Journal_Mapping__c jm : journalMappings)
		{
			journalMappingMap.put(jm.Name, jm.Id);
		}
		//
		// Retrieve created subscriptions
		//
		csord__Subscription__c[] subscriptions =
				[ SELECT Id, RecordType.DeveloperName
				  FROM csord__Subscription__c
				  WHERE csord__Order__c = :order.Id ];
		csord__Subscription__c connectivitySubscription;
		csord__Subscription__c hardwareSupportSubscription;
		for (csord__Subscription__c subs : subscriptions)
		{
			if (subs.RecordType.DeveloperName == 'Hardware_Support'
				&& hardwareSupportSubscription == null)
			{
				hardwareSupportSubscription = subs;
			}
			if (subs.RecordType.DeveloperName == 'Connectivity'
				&& connectivitySubscription == null)
			{
				connectivitySubscription = subs;
			}
		}

		Set<Id> aisWithoutJournalMapping = new Set<Id>();

		//
		// Create subscriptions
		//
		for (cscfga__Product_Configuration__c config : configs)
		{
			//
			// If this is connectivity
			//
			if (config.Original_Product_Name__c == Constants.PRODUCT_TYPE_CONNECTIVITY
				&& connectivitySubscription == null)
			{
				//
				// Create connectivity subscription
				//
				connectivitySubscription = new csord__Subscription__c();
				connectivitySubscription.Name = 'Connectivity for ' + order.Order_Request__r.Name;
				connectivitySubscription.RecordTypeId =
					Cache.getRecordTypeId('csord__Subscription__c.Connectivity');
				connectivitySubscription.csord__Identification__c = 'Connectivity';
				connectivitySubscription.csord__Status__c = 'Active';
				connectivitySubscription.csord__Order_Request__c = order.csord__Order_Request__c;
				connectivitySubscription.csord__Order__c = order.Id;
				connectivitySubscription.Billing_Account__c = order.Order_Request__r.Billing_Account__c;
				connectivitySubscription.csord__Account__c = order.Order_Request__r.Frame_Contract__r.Customer__c;
				insertHelper.add(connectivitySubscription);
			}
			//
			// Not connectivity product configuration
			//
			else
			{
				//
				// Create hardware & support subscription
				//
				if (hardwareSupportSubscription == null)
				{
					hardwareSupportSubscription = new csord__Subscription__c();
					hardwareSupportSubscription.Name = 'Subscription for ' + order.Order_Request__r.Name;
					hardwareSupportSubscription.RecordTypeId =
						Cache.getRecordTypeId('csord__Subscription__c.Hardware_Support');
					hardwareSupportSubscription.csord__Identification__c = 'Hardware & Support';
					hardwareSupportSubscription.csord__Status__c = 'Active';
					hardwareSupportSubscription.csord__Order_Request__c = order.csord__Order_Request__c;
					hardwareSupportSubscription.csord__Order__c = order.Id;
					hardwareSupportSubscription.Billing_Account__c = order.Order_Request__r.Billing_Account__c;
					hardwareSupportSubscription.csord__Account__c = order.Order_Request__r.Frame_Contract__r.Customer__c;
					insertHelper.add(hardwareSupportSubscription);
				}
			}
		}
		insertHelper.store();

		//
		// Create services if there are service line items
		//
		for (cscfga__Product_Configuration__c config : configs)
		{
			csord__Service__c service = null;
			for (cscfga__Attribute__c att : config.cscfga__Attributes__r)
			{
				if (att.cscfga__Is_Line_Item__c == true
					&& att.cscfga__Price__c != null
					&& att.cscfga__Price__c != 0)
				{
					//
					// Create service
					//
					if (service == null)
					{
						service = new csord__Service__c();
						service.csord__Identification__c = config.Name;
						service.csord__Status__c = 'Active';
						service.csord__Order_Request__c = order.csord__Order_Request__c;
						service.csord__Subscription__c = hardwareSupportSubscription.Id;
						service.csord__Activation_Date__c = DateTime.now().date();
						service.csord__Deactivation_Date__c =
							DateTime.now().date().addMonths(Integer.valueOf(config.cscfga__Contract_Term__c));
						service.Product_Configuration_Order__c =
							productConfigurationOrderMap.get(config.Id).Id;
						String billingRateModel = config.Billing_Rate_Model__c;
						service.Billing_Rate_Model__c =
								billingRateModel != null ? billingRateModel : Constants.ONE_OFF;
						insertHelper.add(service);
					}
					//
					// Calculate price
					//
					Decimal price = att.cscfga__Price__c;
					price *= quantityIn;
					//price /= configurationQuantities.get(att.cscfga__Product_Configuration__c);
					Id articleInfoSet = configurationArticleInfoSets.get(att.cscfga__Product_Configuration__c);
					Id journalMapping = null;
					if (articleInfoSet != null)
					{
						String journalMappingName = articleInfoSets.get(articleInfoSet).Journal_Mapping__c;
						if (journalMappingName != null)
						{
							journalMapping = journalMappingMap.get(journalMappingName);
						}
					}
					//
					// Check if journal mapping exists
					//
					if (journalMapping == null)
					{
						if (articleInfoSet == null)
						{
							aisWithoutJournalMapping.add(att.cscfga__Product_Configuration__c);
						}
						else
						{
							aisWithoutJournalMapping.add(articleInfoSet);
						}
					}
					//
					// Service line item
					//
					csord__Service_Line_Item__c serviceLineItem = new csord__Service_Line_Item__c();
					serviceLineItem.Name = att.Name;
					serviceLineItem.csord__Identification__c = att.Name;
					serviceLineItem.csord__Order_Request__c = order.csord__Order_Request__c;
					serviceLineItem.Journal_Mapping__c = journalMapping;
					serviceLineItem.csord__Is_Recurring__c = true;
					serviceLineItem.Total_Price__c = price;
					serviceLineItem.Article_Info_Set__c = articleInfoSet;

					insertHelper.add(serviceLineItem, 'csord__Service__c', service);
				}
			}
		}
		//
		// If there is no journal mapping throw exception
		// because line items without journal mapping cannot be billed
		//
		if (!aisWithoutJournalMapping.isEmpty())
		{
			String message = '';
			Iterator<Id> it = aisWithoutJournalMapping.iterator();
			while (it.hasNext())
			{
				Id aisId = it.next();
				message += aisId;
				if (it.hasNext())
				{
					message += ', ';
				}
			}
			throw new JournalMappingException(message);
		}
		insertHelper.store();
	}

	//--------------------------------------------------------------------------
	// Returns DE or Non-DE depending on country name
	//--------------------------------------------------------------------------
	private static String getCountry(String country)
	{
		System.debug('Country: ' + country);
		if (country == 'Germany'
			|| country == 'Deutschland')
		{
			return 'DE';
		}
		else
		{
			return 'Non-DE';
		}
	}

	private Id createOrderForHybridPayment(Order_request__c orderReq){
		IInsertHelper insertHelper = new InsertHelper();
		//
		// Create order request
		//
		csord__Order_Request__c orderRequest = new csord__Order_Request__c();
		orderRequest.Name = orderReq.Name;
		orderRequest.csord__Module_Name__c = Constants.ORDER_MODULE_NAME;
		orderRequest.csord__Module_Version__c = Constants.ORDER_MODULE_VERSION;
		insert orderRequest;
		//
		// Create order
		//
		csord__Order__c order = new csord__Order__c();
		order.Name = 'Order for ' + orderReq.Name;
		order.Order_Request__c = orderReq.Id;
		order.csord__Order_Request__c = orderRequest.Id;
		order.csord__Account__c = orderReq.Billing_Account__r.Payer__c;
		order.csord__Identification__c = 'Order';
		order.csord__Status2__c = 'Opened';
		insertHelper.add(order);

		insertHelper.store();
		return order.Id;
	}

	private void createSubscriptionForHybridPayment(Id orderIdIn, Integer quantityIn, Boolean isCreatingForFirstTime){
		IInsertHelper insertHelper = new InsertHelper();
		//
		// Retrieve order
		//
		csord__Order__c order =
				[ SELECT Id, Order_Request__c, csord__Order_Request__c,
				  Order_Request__r.Name, Order_Request__r.Billing_Account__c,
				  Order_Request__r.Billing_Account__r.Name,
				  Order_Request__r.Billing_Account__r.Payer__r.BillingCountry,
				  Order_Request__r.Frame_Contract__r.Customer__c
				  FROM csord__Order__c
				  WHERE Id = :orderIdIn];

		//
		// Retrieve product orders
		//
		Map<Id, Product_Order__c> productOrderMap = new Map<Id, Product_Order__c>(
				[ SELECT Id, Quantity__c
				  FROM Product_Order__c
				  WHERE Order_Request__c = :order.Order_Request__c ]);
		//
		// Retrieve product configuration orders
		//
		Product_Configuration_Order__c[] productConfigurationOrders =
				[ SELECT Id, Product_Configuration__c, Quantity__c
				  FROM Product_Configuration_Order__c
				  WHERE Product_Order__c IN :productOrderMap.keySet() ];
		//
		// Create map of product configuration order regarding product configuration id
		//
		Map<Id, Product_Configuration_Order__c> productConfigurationOrderMap =
			new Map<Id, Product_Configuration_Order__c>();
		for (Product_Configuration_Order__c pco : productConfigurationOrders)
		{
			productConfigurationOrderMap.put(pco.Product_Configuration__c, pco);
		}

		//
		// Retrieve product configuration names
		//
		cscfga__Product_Configuration__c[] configs =
				[ SELECT Id, Name, Original_Product_Name__c,
				  cscfga__Contract_Term__c, parent_product_configuration__c,
				  Billing_Rate_Model__c,
				  ( SELECT Id, Name, cscfga__Is_Line_Item__c, cscfga__Recurring__c,
				    cscfga__Price__c, cscfga__Value__c, cscfga__Product_Configuration__c,
				    cscfga__Line_Item_Description__c
				    FROM cscfga__Attributes__r
				    WHERE ( ( cscfga__Is_Line_Item__c = true
							//AND cscfga__Recurring__c = true
							AND cscfga__Product_Configuration__r.Original_Product_Name__c != 'Connectivity')
						  OR Name = :Constants.ATTRIBUTE_NAME_QUANTITY
						  OR Name = :Constants.ATTRIBUTE_NAME_ARTICLE_INFO_SET) )
				  FROM cscfga__Product_Configuration__c
				  WHERE parent_product_configuration__c = null
				  AND Id IN :productConfigurationOrderMap.keySet() ];

		//
		// Put quantities to products
		//
		Map<Id, Integer> configurationQuantities = new Map<Id, Integer>();
		Map<Id, Id> configurationArticleInfoSets = new Map<Id, Id>();
		for (cscfga__Product_Configuration__c config : configs)
		{
			System.debug('Configuration: ' + config);
			for (cscfga__Attribute__c att : config.cscfga__Attributes__r)
			{
				System.debug('Attribute: ' + att);
				if (att.Name == Constants.ATTRIBUTE_NAME_QUANTITY)
				{
					configurationQuantities.put(att.cscfga__Product_Configuration__c, Integer.valueOf(att.cscfga__Value__c));
				}
				else if (att.Name == Constants.ATTRIBUTE_NAME_ARTICLE_INFO_SET)
				{
					try
					{
						System.debug('Putting article info set: ' + att);
						configurationArticleInfoSets.put(att.cscfga__Product_Configuration__c, att.cscfga__Value__c);
					}
					catch(Exception e)
					{
						System.debug('Wrong Id in attribute: ' + att.cscfga__Value__c);
						throw e;
					}
				}
			}
		}

		//
		// Get journal mappings
		//
		Map<Id, Article_Info_Set__c> articleInfoSets = new Map<Id, Article_Info_Set__c>(
				[ SELECT Id, Journal_Mapping__c
				  FROM Article_Info_Set__c
				  WHERE Id IN :configurationArticleInfoSets.values() ]);
		Set<String> journalMappingNames = new Set<String>();
		for (Article_Info_Set__c ais : articleInfoSets.values())
		{
			journalMappingNames.add(ais.Journal_Mapping__c);
		}

		String country = getCountry(order.Order_Request__r.Billing_Account__r.Payer__r.BillingCountry);

		Journal_Mapping__c[] journalMappings =
				[ SELECT Id, Name
				  FROM Journal_Mapping__c
				  WHERE Name IN :journalMappingNames
				  AND Country__c = :country ];
		Map<String, Id> journalMappingMap = new Map<String, Id>();
		for (Journal_Mapping__c jm : journalMappings)
		{
			journalMappingMap.put(jm.Name, jm.Id);
		}
		Set<Id> aisWithoutJournalMapping = new Set<Id>();
		Csord__Subscription__c hardwareSupportSubscription;
		if (isCreatingForFirstTime){
			//no previous subscription created.
			//create new and put hardware as recurring service
			hardwareSupportSubscription = new Csord__Subscription__c();
			hardwareSupportSubscription.Name = 'Subscription for ' + order.Order_Request__r.Name;
			hardwareSupportSubscription.RecordTypeId =
			Cache.getRecordTypeId('csord__Subscription__c.Hardware_Support');
			hardwareSupportSubscription.csord__Identification__c = 'Hardware & Support';
			hardwareSupportSubscription.csord__Status__c = 'Active';
			hardwareSupportSubscription.csord__Order_Request__c = order.csord__Order_Request__c;
			hardwareSupportSubscription.csord__Order__c = order.Id;
			hardwareSupportSubscription.Billing_Account__c = order.Order_Request__r.Billing_Account__c;
			hardwareSupportSubscription.csord__Account__c = order.Order_Request__r.Frame_Contract__r.Customer__c;
			insertHelper.add(hardwareSupportSubscription);

			//add hardware service
			insertHelper.store();

			for (cscfga__Product_Configuration__c config : configs)
			{
				if (config.Original_Product_Name__c == 'Hardware'){
					csord__Service__c service = null;
					for (cscfga__Attribute__c att : config.cscfga__Attributes__r)
					{
						if (att.cscfga__Is_Line_Item__c == true
							&& att.cscfga__Price__c != null
							&& att.cscfga__Price__c != 0)
						{
							//
							// Create service
							//
							if (service == null)
							{
								service = new csord__Service__c();
								service.csord__Identification__c = config.Name;
								service.csord__Status__c = 'Active';
								service.csord__Order_Request__c = order.csord__Order_Request__c;
								service.csord__Subscription__c = hardwareSupportSubscription.Id;
								service.csord__Activation_Date__c = DateTime.now().date();
								service.csord__Deactivation_Date__c =
									DateTime.now().date().addMonths(Integer.valueOf(config.cscfga__Contract_Term__c));
								service.Product_Configuration_Order__c =
									productConfigurationOrderMap.get(config.Id).Id;
								String billingRateModel = config.Billing_Rate_Model__c;
								service.Billing_Rate_Model__c =
										billingRateModel != null ? billingRateModel : Constants.ONE_OFF;
								insertHelper.add(service);
							}
							//
							// Calculate price
							//
							Decimal price = att.cscfga__Price__c;
							price *= productConfigurationOrderMap.get(att.cscfga__Product_Configuration__c).Quantity__c;
							price /= Integer.valueOf(config.cscfga__Contract_Term__c);
							Id articleInfoSet = configurationArticleInfoSets.get(att.cscfga__Product_Configuration__c);
							Id journalMapping = null;
							if (articleInfoSet != null)
							{
								String journalMappingName = articleInfoSets.get(articleInfoSet).Journal_Mapping__c;
								if (journalMappingName != null)
								{
									journalMapping = journalMappingMap.get(journalMappingName);
								}
							}
							//
							// Check if journal mapping exists
							//
							if (journalMapping == null)
							{
								if (articleInfoSet == null)
								{
									aisWithoutJournalMapping.add(att.cscfga__Product_Configuration__c);
								}
								else
								{
									aisWithoutJournalMapping.add(articleInfoSet);
								}
							}
							//
							// Service line item
							//
							csord__Service_Line_Item__c serviceLineItem = new csord__Service_Line_Item__c();
							serviceLineItem.Name = recurringHardwareServiceLineItemName;
							serviceLineItem.csord__Identification__c = att.Name;
							serviceLineItem.csord__Order_Request__c = order.csord__Order_Request__c;
							serviceLineItem.Journal_Mapping__c = journalMapping;
							serviceLineItem.csord__Is_Recurring__c = true;
							serviceLineItem.Total_Price__c = price;
							serviceLineItem.Article_Info_Set__c = articleInfoSet;

							insertHelper.add(serviceLineItem, 'csord__Service__c', service);
						}
					}
				}
			}
		}
		else{
			//
			// Retrieve created subscriptions
			//
			csord__Subscription__c[] subscriptions =
					[ SELECT Id, RecordType.DeveloperName
					  FROM csord__Subscription__c
					  WHERE csord__Order__c = :order.Id ];

			for (csord__Subscription__c subs : subscriptions){
				if (subs.RecordType.DeveloperName == 'Hardware_Support'
					&& hardwareSupportSubscription == null){
					hardwareSupportSubscription = subs;
				}
			}
		}

		if (hardwareSupportSubscription != null){
			//add service for support with today activated sim quantity (quantity In)
			for (cscfga__Product_Configuration__c config : configs)
			{
				if (config.Original_Product_Name__c != 'Hardware'){
					csord__Service__c service = null;
					for (cscfga__Attribute__c att : config.cscfga__Attributes__r)
					{
						if (att.cscfga__Is_Line_Item__c == true
							&& att.cscfga__Price__c != null
							&& att.cscfga__Price__c != 0)
						{
							//
							// Create service
							//
							if (service == null)
							{
								service = new csord__Service__c();
								service.csord__Identification__c = config.Name;
								service.csord__Status__c = 'Active';
								service.csord__Order_Request__c = order.csord__Order_Request__c;
								service.csord__Subscription__c = hardwareSupportSubscription.Id;
								service.csord__Activation_Date__c = DateTime.now().date();
								service.csord__Deactivation_Date__c =
									DateTime.now().date().addMonths(Integer.valueOf(config.cscfga__Contract_Term__c));
								service.Product_Configuration_Order__c =
									productConfigurationOrderMap.get(config.Id).Id;
								String billingRateModel = config.Billing_Rate_Model__c;
								service.Billing_Rate_Model__c =
										billingRateModel != null ? billingRateModel : Constants.ONE_OFF;
								insertHelper.add(service);
							}
							//
							// Calculate price
							//
							Decimal price = att.cscfga__Price__c;
							price *= quantityIn;
							//price /= configurationQuantities.get(att.cscfga__Product_Configuration__c);
							Id articleInfoSet = configurationArticleInfoSets.get(att.cscfga__Product_Configuration__c);
							Id journalMapping = null;
							if (articleInfoSet != null)
							{
								String journalMappingName = articleInfoSets.get(articleInfoSet).Journal_Mapping__c;
								if (journalMappingName != null)
								{
									journalMapping = journalMappingMap.get(journalMappingName);
								}
							}
							//
							// Check if journal mapping exists
							//
							if (journalMapping == null)
							{
								if (articleInfoSet == null)
								{
									aisWithoutJournalMapping.add(att.cscfga__Product_Configuration__c);
								}
								else
								{
									aisWithoutJournalMapping.add(articleInfoSet);
								}
							}
							//
							// Service line item
							//
							csord__Service_Line_Item__c serviceLineItem = new csord__Service_Line_Item__c();
							serviceLineItem.Name = att.Name;
							serviceLineItem.csord__Identification__c = att.Name;
							serviceLineItem.csord__Order_Request__c = order.csord__Order_Request__c;
							serviceLineItem.Journal_Mapping__c = journalMapping;
							serviceLineItem.csord__Is_Recurring__c = true;
							serviceLineItem.Total_Price__c = price;
							serviceLineItem.Article_Info_Set__c = articleInfoSet;

							insertHelper.add(serviceLineItem, 'csord__Service__c', service);
						}
					}
				}
			}
		}

		//
		// If there is no journal mapping throw exception
		// because line items without journal mapping cannot be billed
		//
		if (!aisWithoutJournalMapping.isEmpty())
		{
			String message = '';
			Iterator<Id> it = aisWithoutJournalMapping.iterator();
			while (it.hasNext())
			{
				Id aisId = it.next();
				message += aisId;
				if (it.hasNext())
				{
					message += ', ';
				}
			}
			throw new JournalMappingException(message);
		}
		insertHelper.store();
	}
}