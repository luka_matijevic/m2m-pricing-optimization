global with sharing class customConfigurationController {
    
    public static string bundleId {get; set;}
   // public static string basketId {get; set;}
    
    public customConfigurationController(ApexPages.StandardController cntrl) {
		
        
	
	}
	
	    @RemoteAction
        global static cscfga__Product_Configuration__c syncWithOpp(String basketId) {
    	    //PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
            cscfga__product_basket__c basket = [select id, Related_Bundle_Id__c from cscfga__product_basket__c where id=:basketId];
           
            cscfga__Product_Configuration__c pc = [SELECT id, cscfga__product_bundle__c from cscfga__Product_Configuration__c where cscfga__product_basket__c=:basketId and cscfga__Parent_Configuration__c=null limit 1];
            
            pc.cscfga__product_bundle__c=basket.Related_Bundle_Id__c;
            pc.Original_Product_Name__c='Connectivity';
            update pc;
            
            List<cscfga__Product_Configuration__c> otherPcs = [SELECT id, cscfga__product_bundle__c from cscfga__Product_Configuration__c where cscfga__product_basket__c=:basketId and cscfga__Parent_Configuration__c<>null];
            
            for (cscfga__Product_Configuration__c otherPc : otherPcs) {
            otherPc.cscfga__product_bundle__c=basket.Related_Bundle_Id__c;
            otherPc.Original_Product_Name__c='Tariff Supplementary Service';
            }
            update otherPcs;
            
            cscfga.Api_1.syncBundleToOpportunity(basket.Related_Bundle_Id__c);
            
            return pc;
	    }
	    
	    @RemoteAction
	    global static void SubmitApproval(String configId) {

            cscfga__Attribute__c att = [SELECT Id, cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c=:configId and Name='Approval Required' ];
            
            att.cscfga__Value__c='Yes';
            
            update att;
            
            Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
            approvalRequest.setComments('Submitting request for approval.');
            approvalRequest.setObjectId(configId);
            
            Id currentUser = UserInfo.getUserId();
            approvalRequest.setSubmitterId(currentUser);
            
            // Submit the record to specific process and skip the criteria evaluation
            approvalRequest.setProcessDefinitionNameOrId(Label.Configuration_Approval_Process_Id); // Approval Process ID
            approvalRequest.setSkipEntryCriteria(false);
            
            // Submit the approval request for the account
            Approval.ProcessResult apprRequestResult = Approval.process(approvalRequest);
	    }
	    
	    
	    @RemoteAction
	    global static void ApprovePricing(String configId) {
	        string url = null;
	        cscfga__Product_Configuration__c pc = [SELECT id, cscfga__product_bundle__c,OwnerId,Sales_Confirmation_Needed__c from cscfga__Product_Configuration__c where Id=:configId limit 1];
	        if(pc.Sales_Confirmation_Needed__c = true) {
	            cscfga__Attribute__c att = [SELECT Id, cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c=:configId and Name='Store Revised Price' ];
                att.cscfga__Value__c='Yes';
            
                update att;
	            
	            pc.Sales_Confirmation_Needed__c = false;
	            update pc;
	        }
	        
	        List<ProcessInstance> approvalProcess = [SELECT Id, Status, TargetObjectId, CreatedDate FROM ProcessInstance WHERE TargetObjectId = :configId ORDER BY CreatedDate DESC];
            Id approvalProcessId = approvalProcess[0].Id;

            Id currentUser = UserInfo.getUserId();
            List<ProcessInstanceWorkitem> workitem = [SELECT Id, ActorId, CreatedDate FROM ProcessInstanceWorkitem WHERE ProcessInstanceId = :approvalProcessId AND ActorId = :currentUser];
            Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
            req2.setComments('Pricing accepted');
            req2.setAction('Approve');
            req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
            // Use the ID from the newly created item to specify the item to be worked
            req2.setWorkitemId(workitem[0].Id);
            // Submit the request for approval
            Approval.ProcessResult result2 =  Approval.process(req2);
	    
        }
        
        @RemoteAction
	    global static void RejectPricing(String configId) {
	        string url = null;
	        List<ProcessInstance> approvalProcess = [SELECT Id, Status, TargetObjectId, CreatedDate FROM ProcessInstance WHERE TargetObjectId = :configId ORDER BY CreatedDate DESC];
            Id approvalProcessId = approvalProcess[0].Id;
            Id currentUser = UserInfo.getUserId();
            List<ProcessInstanceWorkitem> workitem = [SELECT Id, ActorId, CreatedDate FROM ProcessInstanceWorkitem WHERE ProcessInstanceId = :approvalProcessId AND ActorId = :currentUser];
            if(workitem != null && workitem.size() > 0) {
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments('Rejected request.');
                req2.setAction('Reject');
                req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
                // Use the ID from the newly created item to specify the item to be worked
                req2.setWorkitemId(workitem[0].Id);
                // Submit the request for approval
                Approval.ProcessResult result2 =  Approval.process(req2);
        	   }
        }
        
        @RemoteAction
	    global static void SendReviewedPricing(String configId) {
	         cscfga__Product_Configuration__c pc = [SELECT id, cscfga__product_bundle__c,OwnerId,Sales_Confirmation_Needed__c from cscfga__Product_Configuration__c where Id=:configId limit 1];
	        
	        string url = null;
	        Id approvalProcessId = Label.Configuration_Approval_Process_Id;
            Id currentUser = UserInfo.getUserId();
            List<ProcessInstanceWorkitem> workitem = [SELECT Id, ActorId, CreatedDate FROM ProcessInstanceWorkitem WHERE ProcessInstanceId = :approvalProcessId AND ActorId = :currentUser];
            if(workitem != null && workitem.size() > 0) {
                pc.Sales_Confirmation_Needed__c = true;
                update pc;
           
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments('Reviewed version sent to sales.');
                req2.setAction('Approve');
                req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
                // Use the ID from the newly created item to specify the item to be worked
                req2.setWorkitemId(workitem[0].Id);
                
                List<Id> lit = new List<Id>();
                lit.add(pc.OwnerId);
                
                req2.setNextApproverIds(lit);
                
                // Submit the request for approval
                Approval.ProcessResult result2 =  Approval.process(req2);
              
        	   }
        }        
}