global class UploadTUFAcInboundProcessor implements csam_t1.InboundMessageProcessor {

    /**
    * Accepts all archiving messages (should be put before the standard Inbound Message Handler)
    * @param incomingMessage message to be processed
    * @return Boolean returns true
    */
    public Boolean isMessageProcessible(csam_t1__Incoming_Message__c incomingMessage) {     
        return incomingMessage.csam_t1__Incoming_URL_Path__c == '/csam_t1/callback/thirduploadfileAc';

    }
      /**
   * The constructor shouldn't do any processing, because the class is initialized for the isProcessible check
   */ 
    public UploadTUFAcInboundProcessor() {
  
     }

    /**
    * Assumes that the message payload is a status message from integration platform
    * 
    * @param incomingMessage message to be processed with corresponding payload attachment
    * @param oldToNewIdsMap map of old-to-new ids used when inserting in chunks to pass the references between messages
    */
    public void processMessage(csam_t1__Incoming_Message__c incomingMessage, Map<Id, Id> oldToNewIdsMap) {

    System.debug('UploadTUFActivationCentreInboundProcessor processMessage');
    Id tufId;	
        try {           
            Attachment attachment = [SELECT Id, Name, Body
                                        FROM Attachment
                                        WHERE ParentId = :incomingMessage.Id
                                        AND Name like 'Message%' LIMIT 1];
			String body = attachment.Body.toString();
							//find outgoing message record (object record id =attachment, att.parentId= tuf)
				//tuf has m2m_order_request
				//find activation centre for that tuf				

			csam_t1__Outgoing_Message_Record__c outgoingRecord = [select csam_t1__Object_Record_Id__c from csam_t1__Outgoing_Message_Record__c where csam_t1__Outgoing_Message__c = :incomingMessage.csam_t1__Outgoing_Message__c];
			Attachment att = [select parentId from Attachment  where id = :outgoingRecord.csam_t1__Object_Record_Id__c];						
			Third_Upload_File__c tuf = [select id, Order_Request__c, Upload_to_SM2M_Status__c from Third_Upload_File__c where id = :att.parentId];			
			Activation_Centre__c activationCentre = [select id, Status__c from Activation_Centre__c where M2M_Order_Request__c = :tuf.Order_Request__c];			
			if (body.contains('description')){
			//uploadError - set Activation Centre Status as TUF UPLOAD ERROR
				
				activationCentre.Status__c = 'TUF Upload Error';
				update activationCentre;
				
				tuf.Upload_to_SM2M_Status__c = 'Error';
				update tuf;

			}	
			else
			{
			//AC TUF UPLOAD OK
				activationCentre.Status__c = 'TUF Upload Success';
				update activationCentre;
				
				tuf.Upload_to_SM2M_Status__c = 'Uploaded';
				update tuf;
			}		
           CallbackMessage cm = (CallbackMessage) JSON.deserialize(attachment.Body.toString(), CallbackMessage.class);

			if (cm.objectsToUpdate != null && cm.objectsToUpdate.size() > 0) {
				processUpdates(cm);	
				// TUF with 'uploaded' should be inside the toUpdate list of the callback message
				List<Third_Upload_File__c> tufs = (List<Third_Upload_File__c>) SObjectHelper.filterSObjects(cm.objectsToUpdate, Third_Upload_File__c.sObjectType);
				if (tufs.size() > 0) {
				    tufId = tufs.get(0).Id;
					updateSimParentAccounts(tufs.get(0));
				}				
			}
			incomingMessage.csam_t1__Status__c = 'Processed';

		} catch (Exception e) {
			incomingMessage.csam_t1__Status__c = 'Process Error';

			Attachment errorAttach = new Attachment();
			errorAttach.Body = Blob.valueOf(e.getStackTraceString());
			errorAttach.parentId = incomingMessage.Id;
			
			String fullName = 'Error_' + e.getMessage();
			errorAttach.Name = fullName.abbreviate(255);
			
			insert errorAttach;
		}
		
		incomingMessage.csam_t1__Processed_At__c = System.now();
		update incomingMessage;
	}

	private void updateSimParentAccounts(Third_Upload_File__c tuf) {
		List<Third_Upload_File__c> tufsWithAccount = [
			Select Id, Account__c 
			from Third_Upload_File__c
			where Id = :tuf.Id
		];
		if (!tufsWithAccount.isEmpty()) {
			List<Stock__c> sims = [Select Id from Stock__c where Third_Upload_File__c = :tuf.Id];
			for (Stock__c sim : sims) {
				sim.Account__c = tufsWithAccount.get(0).Account__c;
			}	
			update sims;
		}
	}

	/**
	 * @description Processes all updates defined by CallbackMessage.objectsToUpdate. First objects are separated based
	 * on sObject type. Then, to prevent different sObject types in single collection and corresponding chunking limits,
	 * each object type collection is separately updated.
	 * @param cm CallbackMessage
	 */
	private void processUpdates(CallbackMessage cm) {
		Map<String, List<sObject>> objectsToUpdateMap = new Map<String, List<sObject>>();
		for (sObject objectToUpdate : cm.objectsToUpdate) {
			String objectToUpdateName = objectToUpdate.getSObjectType().getDescribe().getName();
			if (!objectsToUpdateMap.containsKey(objectToUpdateName)) {
				objectsToUpdateMap.put(objectToUpdateName, new List<sObject>());
			}
			objectsToUpdateMap.get(objectToUpdateName).add(objectToUpdate);
		}

		// avoid updates in chunks with different sObject types in a collection
		for (String key : objectsToUpdateMap.keySet()) {
			update objectsToUpdateMap.get(key);
		}
	}
}