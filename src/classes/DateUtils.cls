public class DateUtils {
	public static String formatDate(Date d, String format) {
		return DateTime.newInstance(d, Time.newInstance(0,0,0,0)).format(format);
	}

	public static Datetime datetimeOrNow(Datetime dt) {
		return dt != null ? dt : Datetime.now();
	}
}