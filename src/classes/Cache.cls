public class Cache
{
	private static Map<String,String> mRecordTypes;

	private static Map<String,Id> recordTypeIds;

    static
    {
    	String key;
    	String value;

    	if (mRecordTypes == null)
    	{
    		List<RecordType>  rtlist = [ SELECT Id, Name, DeveloperName, SObjectType FROM RecordType ];

    		mRecordTypes = new Map<String,String>();
    		recordTypeIds = new Map<String, Id>();

    		for (RecordType rt : rtlist)
    		{
    			key = rt.SObjectType + '.' + rt.Name;
    			value = rt.Id;

    			mRecordTypes.put(key, value );

    			key = rt.SObjectType + '.' + rt.DeveloperName;
    			value = rt.Id;
    			recordTypeIds.put(key, value);
    		}
    	}
    }

    //
    // This method is deprecated. Use getRecordTypeId instead
    //
    public static String recordTypeId(String RecordTypeName)
    {
    	String result = '';

    	if (mRecordTypes.containsKey(RecordTypeName))
    	{
    		result = mRecordTypes.get(RecordTypeName);
    	}

    	return 	result;
    }

    public static Id getRecordTypeId(String recordTypeName)
    {
    	return recordTypeIds.get(recordTypeName);
    }
}