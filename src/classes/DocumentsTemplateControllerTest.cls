/**
 * @author Kristijan K
 * @description Test class for DocumentTemplateController
 */

@isTest
private class DocumentsTemplateControllerTest {
	
	@isTest static void testControllerFunctionality() {
		APXTConga4__Conga_Template__c congaTemp  = new APXTConga4__Conga_Template__c(
			APXTConga4__Name__c = 'Test',
			APXTConga4__Template_Type__c = 'Document'
		);
		insert congaTemp;

		Attachment att = new Attachment(
			ParentId = congaTemp.Id,
			Name = 'Test',
			Body = Blob.valueOf('Test Data')
		);
		insert att;

		DocumentTemplateController dc = new DocumentTemplateController();
		//System.assertEquals(dc.congaTempalteList.size(),1);

	}
	
	
	
}