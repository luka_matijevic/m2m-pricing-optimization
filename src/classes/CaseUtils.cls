global class CaseUtils {

	private static Id gutschriftRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Gutschrift Case').getRecordTypeId();
	private static String GS_NUMMER_PREFIX = 'GS_';
	
	webService static String refreshRelatedBillingAccounts(Id caseId) {
	    Id accountId;
	    List<Billing_Account__c> relatedBillingAccounts = null;
	    String result = 'Related Billing Accounts refreshed.';
	    String billingAccountsLinks = '';
	    
	    Case selectedCase = [SELECT Id, AccountId, Related_Billing_Accounts__c FROM Case WHERE Id = :caseId];
	    
	    if(selectedCase != null && selectedCase.AccountId != null) {
	        List<Account> relatedAccount = [SELECT Id FROM Account WHERE Id = :selectedCase.AccountId];
            
            if(relatedAccount != null && relatedAccount.size() > 0)
            {
                relatedBillingAccounts = [SELECT Id, Name, Payer__c FROM Billing_Account__c WHERE Payer__c = :relatedAccount[0].Id];
	        
    	        if(relatedBillingAccounts != null && relatedBillingAccounts.size() > 0) {
    	            for(Billing_Account__c ba : relatedBillingAccounts) {
    	                billingAccountsLinks += '<a href=/' + ba.Id + '>' + ba.Name + '</a>' + '<br></br>'; //URL.getSalesforceBaseUrl().toExternalForm() + '/' + 
    	            }
    	            
    	            selectedCase.Related_Billing_Accounts__c = billingAccountsLinks;
    	            update selectedCase;
    	        }
            }
	    }
	    
	    return result;
	}
	
	webService static String createGutschriftCase(Id caseId) {
		String result = '';
		if (caseId != null) {
			Case c = [select id, Origin, Reason, Description, Subject, Subcategory__c, Category__c, AccountId, ContactId, RecordTypeId, Status, Type, Gutschrift_count__c, CaseNumber from Case where Id = :caseId];
			if (c.AccountId != null) {
				Case newCase = c.clone();
				newCase.RecordTypeId = gutschriftRecordType;
				newCase.Type = 'Gutschrift';
				newCase.Parent_Ticket__c = caseId;
				newCase.Parent = c;
				newCase.Id = null;
				newCase.Rechnungsempfanger_formula__c = c.AccountId;
				newCase.Approval_Needed__c = true;
				newCase.Responded__c = true;
				// Preparing and populating GS_NUMMER_ID field
				Integer currentYear = System.Today().year();
				if (c.Gutschrift_count__c == null){
					c.Gutschrift_count__c = 1;
				} else {
					c.Gutschrift_count__c = c.Gutschrift_count__c + 1;
				}
				newCase.GS_Nummer_id__c = GS_NUMMER_PREFIX + currentYear + '_' + c.CaseNumber + '_' + c.Gutschrift_count__c;

				try {
					insert newCase;
					result = '/' + newCase.Id;
					update c;
				} catch (Exception e) {
					result = 'Error ' + e.getMessage();
				}
			} else {
				result = 'Error - Ticket needs to be associated to Account before Gutschrift can be created.';
			}

		} else {
			result = 'Error - Please provide case Id.';
		}

		return result;
	}
	
	webService static String startGutschriftApprovalProcess(Id caseId) {
		String result = '';
		Case c = [select id, Ausf_hren_als__c, Bescreibung_m__c, Nettobetrag_in__c, Verursacher__c, Account_number__c from Case where Id = :caseId];
		if (c.Nettobetrag_in__c != null && c.Bescreibung_m__c != null && c.Nettobetrag_in__c != null && c.Verursacher__c != null && c.Account_number__c != null) {
			Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
			req.setComments('Submitted for approval. Please approve.');
			req.setObjectId(caseId);
			try {
				Approval.ProcessResult apResult = Approval.Process(req);
				result = '/' + caseId;
			} catch(Exception e) {
				result = 'Error -' + e.getMessage();
			}
		}
		else {
			result = 'Error - Please enter all mandatory fields: Ausführen als, Verursacher, Beschreibung, Nettobetrag and Account Number.';
		}
		
		return result;
	}
	
	webService static String sendBuildabilityStatus(Id caseId)
	{
	    Date todaysDate = DateTime.now().date();
	    //Integer daysDifference;
	    String result = '';
	    String jsonBody = '';
	    String httpMethod = 'POST';
	    String endPoint = 'https://logbook.sixt.com/buildability';
	    //String endPoint = 'https://logbook-acceptance.sixt.com/buildability';
	    Case c = [select Id, Sixt_buildability_status__c, Sixt_LVID__c, Sixt_Vehicle_manufacturer__c, Sixt_Vehicle_model__c, Sixt_Vehicle_production_year__c, Sixt_Contract_duration__c, Sixt_Additional_details__c,
	                           CreatedDate, Sixt_Cable_type__c, CaseNumber from Case where Id = :caseId];
	    
	    if(c != null)
	    {
	        //daysDifference = c.CreatedDate.date().daysBetween(todaysDate);
	        
	        //if(daysDifference > 5) // in 5 days it is possible to send buildability status to SIXT
	        //{
	            //result = 'Checking buildable status is available only within 5 days from opening a ticket.' ;
	        //}
	        //else
	        //{
    	    if(c.Sixt_buildability_status__c.equals('Positive') || c.Sixt_buildability_status__c.equals('Positiv'))
    	    {
    	        c.Sixt_buildability_status__c = 'BUILDABILITY_POSSIBLE';
    	    }
    	    else if(c.Sixt_buildability_status__c.equals('Pending'))
    	    {
    	        c.Sixt_buildability_status__c = 'BUILDABILITY_PENDING';
    	    }
    	    else if(c.Sixt_buildability_status__c.equals('Negative') || c.Sixt_buildability_status__c.equals('Negativ'))
    	    {
    	        c.Sixt_buildability_status__c = 'BUILDABILITY_IMPOSSIBLE';
    	            
    	        if(c.Sixt_Cable_type__c == null || !c.Sixt_Cable_type__c.equals('Nonexistent'))
    	        {
    	            result = 'Nonexistent value has to be selected as a Cable type.';
    	            return result;
    	        }
    	    }
    	        
    	    Map<String, String> returnJSONMap = new Map<String, String>();
    	    returnJSONMap.put('Sixt_LVID__c', c.Sixt_LVID__c);
    	    returnJSONMap.put('Sixt_Cable_type__c', c.Sixt_Cable_type__c);
    	    returnJSONMap.put('Sixt_buildability_status__c', c.Sixt_buildability_status__c);
    	    returnJSONMap.put('CaseNumber', c.CaseNumber);
    	    returnJSONMap.put('Sixt_Vehicle_manufacturer__c', c.Sixt_Vehicle_manufacturer__c);
    	    returnJSONMap.put('Sixt_Vehicle_model__c', c.Sixt_Vehicle_model__c);
    	    returnJSONMap.put('Sixt_Vehicle_production_year__c', c.Sixt_Vehicle_production_year__c);
    	    returnJSONMap.put('Sixt_Contract_duration__c', c.Sixt_Contract_duration__c);
    	    returnJSONMap.put('Sixt_Additional_details__c', c.Sixt_Additional_details__c);
    	        
    	    jsonBody = JSON.serializePretty(returnJSONMap);
    	        
    	    //System.debug(LoggingLevel.INFO, jsonBody);
    	    //result = jsonBody;
    	        
    	    HttpResponse resp = CalloutIntegration.executeRequest(endPoint, httpMethod, jsonBody, 32);
    	        
    	    if(resp != null)
    	    {
    	        result = resp.getBody();
    	        if(resp.getStatusCode() == 200) {
    	            if(c.Sixt_buildability_status__c.equals('BUILDABILITY_POSSIBLE'))
            	    {
            	        c.Sixt_buildability_status__c = 'Positive';
            	    }
            	    else if(c.Sixt_buildability_status__c.equals('BUILDABILITY_PENDING'))
            	    {
            	        c.Sixt_buildability_status__c = 'Pending';
            	    }
            	    else if(c.Sixt_buildability_status__c.equals('BUILDABILITY_IMPOSSIBLE'))
            	    {
            	        c.Sixt_buildability_status__c = 'Negative';
            	    }
            	        
    	            c.Sixt_Buildability_status_sent__c = true;
    	                
    	            update c;
    	        }
    	    }
	        //}
	    }
	    
	    return result;
	}

    webservice static String createOrderForSixt(Id caseId) {
        String result = 'NOTHING CHANGED';
		List<Case> casesForCheck;

		casesForCheck = [SELECT Id, Status, Sixt_Waiting_for_ICCID_activation__c FROM Case WHERE Id = :caseId];

		
		if (casesForCheck != null && casesForCheck.size() > 0)
		{
			Case cs = casesForCheck[0];
			if (cs != null)
			{
				cs.Sixt_Waiting_for_ICCID_activation__c = true;
				cs.Status = 'Sent to installation';				
				update cs;
				result = 'Order is sent to installation!';
			}
		}
		
        return result;
    }

	private static HttpRequest createRequest(string endPoint, string httpMethod, string body, integer timeout)
	{
		HttpRequest req = new HttpRequest();
		req.setEndpoint(endPoint);
		req.setHeader('content-type', 'application/json');
		req.setmethod(httpMethod);
		if (httpMethod != null && httpMethod.equalsIgnoreCase('POST'))
			req.setbody(body);
		req.setTimeout(timeout * 1000);
		return req;
	}
	
	public static HttpResponse executeRequest(string endPoint, string httpMethod, string body, integer timeout)
	{
		HttpRequest req = createRequest(endPoint, httpMethod, body, timeout);
		Http httpCall = new Http();
		HttpResponse resp = httpCall.send(req);
		return resp;
	}
}