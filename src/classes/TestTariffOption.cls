//------------------------------------------------------------------------------
// Test for tariff option cloning and equals methods
//------------------------------------------------------------------------------
@isTest
private class TestTariffOption
{
	//--------------------------------------------------------------------------
	// Test if cloning and equals methods return true
	//--------------------------------------------------------------------------
    static testMethod void testTariffOptionCloneAndEquals()
    {
		ITariffOption tariffOption = createTariffOption();
		ITariffOption clonedTariffOption = tariffOption.deepClone();
		System.assertEquals(true, tariffOption.equals(clonedTariffOption));
		System.assertEquals(true, tariffOption.equalsWithoutQuantity(clonedTariffOption));
		System.assertEquals(true, tariffOption.equalsWithoutConfiguration(clonedTariffOption));

		// code coverage for getters and setters
		tariffOption.getName();
		tariffOption.getTariffOption();
		tariffOption.getRecurringBaseFee();
		tariffOption.getRecurringTargetFee();
		tariffOption.getShowOnQuote();
		tariffOption.getUnit();
		tariffOption.getCanDiscount();
		tariffOption.getPerSim();
		tariffOption.setName('test');
		tariffOption.setRecurringBaseFee(0.12);
		tariffOption.setRecurringTargetFee(0.13);
		tariffOption.setShowOnQuote(true);
		tariffOption.setUnit('');
		tariffOption.setCanDiscount(true);
		tariffOption.setPerSim(true);
    }

	//---------------------------------------------------------------------------
	// Test when tariff option base is different
	//---------------------------------------------------------------------------
	static testMethod void testTariffOptionBaseChanged()
	{
		ITariffOption tariffOption = createTariffOption();
		ITariffOption clonedTariffOption = tariffOption.deepClone();
		clonedTariffOption.getTariffOption().Recurring_Base_Fee__c = 40.43;
		System.assertEquals(false, tariffOption.equals(clonedTariffOption));
		System.assertEquals(false, tariffOption.equalsWithoutQuantity(clonedTariffOption));
		System.assertEquals(false, tariffOption.equalsWithoutConfiguration(clonedTariffOption));
	}

	//---------------------------------------------------------------------------
	// Test when tariff option configuration is different
	//---------------------------------------------------------------------------
	static testMethod void testTariffOptionConfigurationChanged()
	{
		ITariffOption tariffOption = createTariffOption();
		ITariffOption clonedTariffOption = tariffOption.deepClone();
		clonedTariffOption.getTariffOption().Recurring_Target_Fee__c = 38.65;
		System.assertEquals(false, tariffOption.equals(clonedTariffOption));
		System.assertEquals(false, tariffOption.equalsWithoutQuantity(clonedTariffOption));
		System.assertEquals(true, tariffOption.equalsWithoutConfiguration(clonedTariffOption));
	}

	//---------------------------------------------------------------------------
	// Test when rate card configuration is different
	//---------------------------------------------------------------------------
	static testMethod void testRateCardConfigurationChanged()
	{
		ITariffOption tariffOption = createTariffOption();
		ITariffOption clonedTariffOption = tariffOption.deepClone();
		clonedTariffOption.getRateCards()[0].getRateCard().Expected_Usage__c = null;
		System.assertEquals(false, tariffOption.equals(clonedTariffOption));
		System.assertEquals(false, tariffOption.equalsWithoutQuantity(clonedTariffOption));
		System.assertEquals(true, tariffOption.equalsWithoutConfiguration(clonedTariffOption));

		//
		// Test RateCard getters and setters
		//
		clonedTariffOption.getRateCards()[0].getShowOnQuote();
		clonedTariffOption.getRateCards()[0].setShowOnQuote(true);
		clonedTariffOption.getRateCards()[0].getName();
		clonedTariffOption.getRateCards()[0].setName('Test Name');
		clonedTariffOption.getRateCards()[0].getClockRate();
		clonedTariffOption.getRateCards()[0].setClockRate('Clock Rate');
		clonedTariffOption.getRateCards()[0].getBaseUsageFee();
		clonedTariffOption.getRateCards()[0].setBaseUsageFee(7.23);
		clonedTariffOption.getRateCards()[0].getTargetUsageFee();
		clonedTariffOption.getRateCards()[0].setTargetUsageFee(7.23);
		clonedTariffOption.getRateCards()[0].getExpectedUsage();
		clonedTariffOption.getRateCards()[0].setExpectedUsage(7.23);
		clonedTariffOption.getRateCards()[0].getCanDiscount();
		clonedTariffOption.getRateCards()[0].setCanDiscount(true);

	}

	//---------------------------------------------------------------------------
	// Test when rate card base is different
	//---------------------------------------------------------------------------
	static testMethod void testRateCardBaseChanged()
	{
		ITariffOption tariffOption = createTariffOption();
		ITariffOption clonedTariffOption = tariffOption.deepClone();
		clonedTariffOption.getRateCards()[0].getRateCard().Base_Usage_Fee__c = 49.10;
		System.assertEquals(false, tariffOption.equals(clonedTariffOption));
		System.assertEquals(false, tariffOption.equalsWithoutQuantity(clonedTariffOption));
		System.assertEquals(false, tariffOption.equalsWithoutConfiguration(clonedTariffOption));

		//
		// Test ServiceFee getters and setters
		//
		clonedTariffOption.getServiceFees()[0].getChargedPer();
		clonedTariffOption.getServiceFees()[0].setChargedPer('Test Charger Per');
		clonedTariffOption.getServiceFees()[0].getShowOnQuote();
		clonedTariffOption.getServiceFees()[0].setShowOnQuote(true);
		clonedTariffOption.getServiceFees()[0].getName();
		clonedTariffOption.getServiceFees()[0].setName('Test Name');
		clonedTariffOption.getServiceFees()[0].getOneTimeBaseFee();
		clonedTariffOption.getServiceFees()[0].setOneTimeBaseFee(7.23);
		clonedTariffOption.getServiceFees()[0].getOneTimeTargetFee();
		clonedTariffOption.getServiceFees()[0].setOneTimeTargetFee(7.23);
		clonedTariffOption.getServiceFees()[0].getRecurringBaseFee();
		clonedTariffOption.getServiceFees()[0].setRecurringBaseFee(7.23);
		clonedTariffOption.getServiceFees()[0].getRecurringTargetFee();
		clonedTariffOption.getServiceFees()[0].setRecurringTargetFee(7.23);
		clonedTariffOption.getServiceFees()[0].getCanDiscount();
		clonedTariffOption.getServiceFees()[0].setCanDiscount(true);

	}

	//---------------------------------------------------------------------------
	// Test when service fee configuration is different
	//---------------------------------------------------------------------------
	static testMethod void testServiceFeeConfigurationChanged()
	{
		ITariffOption tariffOption = createTariffOption();
		ITariffOption clonedTariffOption = tariffOption.deepClone();
		clonedTariffOption.getServiceFees()[0].getServiceFee().One_Time_Target_Fee__c = 23.39;
		System.assertEquals(false, tariffOption.equals(clonedTariffOption));
		System.assertEquals(false, tariffOption.equalsWithoutQuantity(clonedTariffOption));
		System.assertEquals(true, tariffOption.equalsWithoutConfiguration(clonedTariffOption));
	}

	//---------------------------------------------------------------------------
	// Test when service fee base is different
	//---------------------------------------------------------------------------
	static testMethod void testServiceFeeBaseChanged()
	{
		ITariffOption tariffOption = createTariffOption();
		ITariffOption clonedTariffOption = tariffOption.deepClone();
		clonedTariffOption.getServiceFees()[0].getServiceFee().One_Time_Base_Fee__c = 43.56;
		System.assertEquals(false, tariffOption.equals(clonedTariffOption));
		System.assertEquals(false, tariffOption.equalsWithoutQuantity(clonedTariffOption));
		System.assertEquals(false, tariffOption.equalsWithoutConfiguration(clonedTariffOption));
	}

	//--------------------------------------------------------------------------
	// Test when tariff option has different quantity
	//--------------------------------------------------------------------------
	static testMethod void testTariffOptionQuantityChanged()
	{
		ITariffOption tariffOption = createTariffOption();
		ITariffOption clonedTariffOption = tariffOption.deepClone();
		clonedTariffOption.setQuantity(5);
		System.assertEquals(false, tariffOption.equals(clonedTariffOption));
		System.assertEquals(true, tariffOption.equalsWithoutQuantity(clonedTariffOption));
		System.assertEquals(true, tariffOption.equalsWithoutConfiguration(clonedTariffOption));
	}

	//--------------------------------------------------------------------------
	// Test when show on quote is changed
	//--------------------------------------------------------------------------
	static testMethod void testTariffOptionShowOnQuoteChanged()
	{
		ITariffOption tariffOption = createTariffOption();
		ITariffOption clonedTariffOption = tariffOption.deepClone();
		clonedTariffOption.getTariffOption().Show_on_Quote__c = false;
		System.assertEquals(false, tariffOption.equals(clonedTariffOption));
		System.assertEquals(false, tariffOption.equalsWithoutQuantity(clonedTariffOption));
		System.assertEquals(true, tariffOption.equalsWithoutConfiguration(clonedTariffOption));
	}

	//--------------------------------------------------------------------------
	// Test when is selected is changed
	//--------------------------------------------------------------------------
	static testMethod void testTariffOptionIsSelectedChanged()
	{
		ITariffOption tariffOption = createTariffOption();
		ITariffOption clonedTariffOption = tariffOption.deepClone();
		clonedTariffOption.setIsSelected(false);
		System.assertEquals(false, tariffOption.equals(clonedTariffOption));
		System.assertEquals(false, tariffOption.equalsWithoutQuantity(clonedTariffOption));
		System.assertEquals(true, tariffOption.equalsWithoutConfiguration(clonedTariffOption));
	}

	//---------------------------------------------------------------------------
	// Test when rate card show on quote is different
	//---------------------------------------------------------------------------
	static testMethod void testRateCardShowOnQuoteChanged()
	{
		ITariffOption tariffOption = createTariffOption();
		ITariffOption clonedTariffOption = tariffOption.deepClone();
		clonedTariffOption.getRateCards()[0].getRateCard().Show_on_Quote__c = false;
		System.assertEquals(false, tariffOption.equals(clonedTariffOption));
		System.assertEquals(false, tariffOption.equalsWithoutQuantity(clonedTariffOption));
		System.assertEquals(true, tariffOption.equalsWithoutConfiguration(clonedTariffOption));
	}

	//---------------------------------------------------------------------------
	// Test when service fee show on quote is different
	//---------------------------------------------------------------------------
	static testMethod void testServiceFeeShowOnQuoteChanged()
	{
		ITariffOption tariffOption = createTariffOption();
		ITariffOption clonedTariffOption = tariffOption.deepClone();
		clonedTariffOption.getServiceFees()[0].getServiceFee().Show_on_Quote__c = false;
		System.assertEquals(false, tariffOption.equals(clonedTariffOption));
		System.assertEquals(false, tariffOption.equalsWithoutQuantity(clonedTariffOption));
		System.assertEquals(true, tariffOption.equalsWithoutConfiguration(clonedTariffOption));
	}

	//--------------------------------------------------------------------------
	// Create tariff option
	//--------------------------------------------------------------------------
	static ITariffOption createTariffOption()
	{
		Tariff_Option__c to = new Tariff_Option__c();
		to.Name = 'Test tariff option';
		to.Recurring_Base_Fee__c = 12.39;
		to.Recurring_Target_Fee__c = 43.50;
		to.Show_on_Quote__c = true;

		Rate_Card__c rc = new Rate_Card__c();
		rc.Name = 'Test Rate Card';
		rc.Clock_Rate__c = 'MB';
		rc.Base_Usage_Fee__c = 12.43;
		rc.Target_Usage_Fee__c = 10.49;
		rc.Expected_Usage__c = 102.43;
		rc.Show_on_Quote__c = true;

		Service_Fee__c sf = new Service_Fee__c();
		sf.Name = 'Test Service Fee';
		sf.Charged_Per__c = 'Volume';
		sf.One_Time_Base_Fee__c = 21.39;
		sf.One_Time_Target_Fee__c = 43.54;
		sf.Show_on_Quote__c = true;

		ITariffOption tariffOption =
			new TariffOption(
					to,
					new List<IRateCard>{ new RateCard(rc,new List<IRateCardCountryManagement>(),new List<SelectOption>()) },
					new List<IServiceFee>{ new ServiceFee(sf) },
					true,
					1);
		return tariffOption;
	}
}