//-------------------------------------------------------------------------------
// Job for updating MSISDN__c ready for use field to true if quarantine end date < today
//-------------------------------------------------------------------------------
global class UpdateMSISDNIsReadyForUse implements Database.Batchable<SObject>
{ 
	//---------------------------------------------------------------------------
	// Start of batch apex
	//---------------------------------------------------------------------------
	global Database.QueryLocator start(Database.BatchableContext bc)
	{
		//
		// Filter records
		//
		String query = 'SELECT Id, isReadyForUse__c FROM MSISDN__c WHERE Date_quarantine_end__c < TODAY and isReadyForUse__c = false';
		return Database.getQueryLocator(query);
	}

	//--------------------------------------------------------------------------
	// Execute of batch apex
	//--------------------------------------------------------------------------
	global void execute(Database.BatchableContext bc, List<SObject> scope)
	{
		for (MSISDN__c msisdn : (List<MSISDN__c>)scope)
		{
			msisdn.isReadyForUse__c = true;
		}
		update scope;
	}

	//--------------------------------------------------------------------------
	// Finish of batch apex
	//--------------------------------------------------------------------------
	global void finish(Database.BatchableContext bc)
	{
	}

}