@isTest
public class OfiUtilsTest {
	
	static testMethod void testHasOneCentOrMore() {
		system.assertEquals(true, OfiUtils.hasOneCentOrMore(new Prebill_Line_Item__c(Line_Item_Amount__c = 0.01)));
		system.assertEquals(true, OfiUtils.hasOneCentOrMore(new Prebill_Line_Item__c(Line_Item_Amount__c = 0.013)));
		system.assertEquals(true, OfiUtils.hasOneCentOrMore(new Prebill_Line_Item__c(Line_Item_Amount__c = 1)));
		
		system.assertEquals(false, OfiUtils.hasOneCentOrMore(new Prebill_Line_Item__c(Line_Item_Amount__c = -1)));
		system.assertEquals(false, OfiUtils.hasOneCentOrMore(new Prebill_Line_Item__c(Line_Item_Amount__c = -0.01)));
		system.assertEquals(false, OfiUtils.hasOneCentOrMore(new Prebill_Line_Item__c(Line_Item_Amount__c = 0.001)));
		system.assertEquals(false, OfiUtils.hasOneCentOrMore(new Prebill_Line_Item__c(Line_Item_Amount__c = 0.00999999)));
	}
	
	static testMethod void testIsProjektZero() {
		system.assertEquals(true, OfiUtils.isProjektZero(new Prebill_Line_Item__c()));		
		system.assertEquals(true, OfiUtils.isProjektZero(new Prebill_Line_Item__c(Projekt__c = '00000000')));		
		system.assertEquals(true, OfiUtils.isProjektZero(new Prebill_Line_Item__c(Projekt__c = '0')));		
		system.assertEquals(false, OfiUtils.isProjektZero(new Prebill_Line_Item__c(Projekt__c = '0abc')));		
		system.assertEquals(false, OfiUtils.isProjektZero(new Prebill_Line_Item__c(Projekt__c = 'abc0')));		
		system.assertEquals(false, OfiUtils.isProjektZero(new Prebill_Line_Item__c(Projekt__c = '01')));		
		system.assertEquals(false, OfiUtils.isProjektZero(new Prebill_Line_Item__c(Projekt__c = '123')));		
	}
}