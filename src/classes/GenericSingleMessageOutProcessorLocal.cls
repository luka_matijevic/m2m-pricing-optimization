/**
* @description Generic outbound message processor implementation for single messages that will send attached payload to
* destination enpoint using HTTPS with all necessary HTTP header properties. The class should be configured inside
* Outbound Message Handler custom settings to be processed.
* @author Hrvoje Tutman
*/
global class GenericSingleMessageOutProcessorLocal implements csam_t1.OutboundMessageProcessor {

    /**
    * @description Accepts all messages (should be put as the last Inbound Message Handler)
    * @param incomingMessage message to be processed
    * @return Boolean returns true
    */
    global Boolean isMessageProcessible(csam_t1__Outgoing_Message__c outgoingMessage) {
        return true;
    }

    /**
    * @description Sends the payload message over HTTP to defined outbound endpoint. Adds <em>CS-Signature</em>,
    * <em>CS-Certificate</em>, <em>CS-Callback-Host</em> and <em>CS-Correlation-Id</em> HTTP header parameters. The
    * implementation expects acknowledgment and based on HTTP response status code sets the Outgoing Message record
    * status: HTTP status code &gt;= 200 OR &lt; 300 - <em>Waiting for Response</em>, HTTP status code = 403 -
    * <em>Security Error</em>, other value <em>Recoverable Error</em>. Acknowledgment message payload is stored as
    * attachment with name starting with <em>Response_</em>. On resending (when status is <em>Recoverable Error</em>)
    * Delivery Tag value increases.
    * @param incomingMessage message to be processed with corresponding payload attachment
    * @return objects to be updated and inserted inside OutboundMessageProcessingResponse
    */
    global csam_t1.OutboundMessageProcessingResponse processMessage(csam_t1__Outgoing_Message__c outgoingMessage) {
        URL url = new URL('https', outgoingMessage.csam_t1__URL_Host__c, outgoingMessage.csam_t1__URL_File__c);

        Attachment[] attachments = [SELECT Id, Name, Body
                                    FROM Attachment
                                    WHERE ParentId = :outgoingMessage.Id
                                    AND Name like 'Message%' LIMIT 1];

        Attachment attachment = null;
        if (attachments.size() == 1) {
            attachment = attachments[0];
        } else {
            // do nothing
            // this should never happen
            return null;
        }

        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();

        req.setClientCertificateName('tomcat');
        req.setTimeout(30000);
        system.debug('test');
        req.setEndpoint(url.toExternalForm());
        req.setHeader('Content-Type', outgoingMessage.csam_t1__Content_Type__c);
        req.setMethod(outgoingMessage.csam_t1__HTTP_Method__c);
        req.setBodyAsBlob(attachment.Body);
        req.setCompressed(true); // otherwise we hit a limit of 32000

        String signature = csam_t1.SecurityUtils.generateCalloutSignature(attachment.Body);

        req.setHeader('CS-Signature', signature);
        req.setHeader('CS-Certificate', UserInfo.getOrganizationId().substring(0, 15));
        req.setHeader('CS-Callback-Host', outgoingMessage.csam_t1__Callback_Host__c);
        req.setHeader('CS-Correlation-Id', outgoingMessage.Name);

        csam_t1.OutboundMessageProcessingResponse outResponse = new csam_t1.OutboundMessageProcessingResponse();

        try {
            res = http.send(req);
            Integer statusCode = res.getStatusCode();

            if (statusCode >= 200 && statusCode < 300){
                outgoingMessage.csam_t1__Status__c = 'Waiting for Response';
            } else if (statusCode == 403) {
                outgoingMessage.csam_t1__Status__c = 'Security Error';
            } else {
                outgoingMessage.csam_t1__Status__c = 'Recoverable Error';
            }

            outgoingMessage.csam_t1__HTTP_Response_Status_Code__c = statusCode;
            outgoingMessage.csam_t1__HTTP_Response_Status__c = res.getStatus();
        } catch (Exception e) {
            outgoingMessage.csam_t1__Status__c = 'Recoverable Error';

            outgoingMessage.csam_t1__HTTP_Response_Status_Code__c = 0;
            outgoingMessage.csam_t1__HTTP_Response_Status__c = '<exception>';
        }

        outgoingMessage.csam_t1__Sent_At__c = System.now();
        outgoingMessage.csam_t1__Delivery_Tag__c = outgoingMessage.csam_t1__Delivery_Tag__c + 1;

        outResponse.objectsToUpdate.add(outgoingMessage);

        if (res.getBodyAsBlob() != null) {
            Attachment responseAttach = new Attachment();
            responseAttach.Body = res.getBodyAsBlob();
            responseAttach.Name = 'Response_' + outgoingMessage.csam_t1__Delivery_Tag__c;
            responseAttach.parentId = outgoingMessage.Id;

            outResponse.objectsToInsert.add(responseAttach);
        }

        return outResponse;
    }
}