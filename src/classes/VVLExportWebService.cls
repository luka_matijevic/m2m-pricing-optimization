global class VVLExportWebService {
    WebService static String sendToHeroku(Id vvlFileId) {
    	try {
    	    List<BIC_Export_Job_Data__c> reportGeneratedJobs = [
			Select
				Id, CSV_Generation_Start_Time__c
			from
				BIC_Export_Job_Data__c
			where 
				Id = :vvlFileId
		    ];
		    
		    csam_t1.ObjectGraphCalloutHandler.AdditionalObjects addObjects = new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects();
    		addObjects.typeName = 'BIC_Export_Job_Data__c';
    		addObjects.ids = new Id[]{reportGeneratedJobs[0].id};
    
    		String response = csam_t1.ObjectGraphCalloutHandler.createAndSendExtended('Upload VVL File', vvlFileId, new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects[]{addObjects});
		    
    		//String response = csam_t1.ObjectGraphCalloutHandler.createAndSend('Upload PQT File', pqtFileId);
    		return response;
    	} catch(Exception e){
    		return e.getMessage();
    	}
    }
}