@isTest
public class ArvatoFileUploadControllerTest {
    
    static testMethod void testMethod1(){
        Order_Request__c m2mOrdReq = new Order_Request__c( Shipping_City__c='Test City', Shipping_Country__c = 'Test Country', Shipping_Postal_Code__c='10000', Shipping_Street__c='Test shipping street');

        insert m2mOrdReq;
        
        id simRecrdtype = Schema.SObjectType.Article_Info_Set__c.getRecordTypeInfosByName().get('SIM').getRecordTypeId();
        
        Article_Info_Set__c articleInfo = new Article_Info_Set__c(recordtypeId=simRecrdtype,name='Artical Info set',Global_Local__c = 'Global',Status__c='Draft',One_Off_Journal_Mapping__c='Reccurring Fee',
                                            Journal_Mapping__c = 'SMS Usage Fee',Contract_Term__c ='12',One_Time_Fee__c = 100,Recurring_Fee__c = 100);
        insert articleInfo;
        Third_Upload_File__c tuf = new Third_Upload_File__c(name = 'Third upload file',SIM_Article_Type__c=articleInfo.id );
        insert tuf;
        
        Stock__c st = new Stock__c(name ='Stock',Article_Info_Set__c =articleInfo.id,Status__c = 'On stock', ICCID__c = '1111111111111111111',Third_Upload_File__c=tuf.id);
        insert st;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(tuf);
        
        string str = '\"1111111111111111111\"';
        Attachment attach = new Attachment();
        attach.Body = blob.valueOf(str);
        attach.name ='Test';
        
        test.startTest();
        
        ArvatoFileUploadController afp = new ArvatoFileUploadController(sc);
        afp.attach = attach;
        afp.upload();
        
        
        pageReference pf = page.ArvatoFileUploadPage;
        Test.setCurrentPage(pf);
        pf.getParameters().put('excludeQuotes','true');
        
        ArvatoFileUploadController afp1 = new ArvatoFileUploadController(sc);
        afp1.attach = attach;
        afp1.upload();
        
        test.stopTest();
        
    }

}