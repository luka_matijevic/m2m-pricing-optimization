/*
@@Description : Batch job to provide case read permisison to users
                

*/

global class CaseSharing_Job implements Database.Batchable<sObject>{

    map<id,id> accIds = new map<id,id>();
    global CaseSharing_Job(map<id,id> accIds){
        this.accIds = accIds;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        try{
            if(accIds.size() > 0){
                set<id> accountIds = new set<id>();
                accountIds.addAll(accIds.keySet());
                string qry = 'select id, accountId from case where accountId in:accountIds order by createdDate desc';
                return Database.getQueryLocator(qry);
            }else{
                return null;
            }
            
        }catch(exception ex){
            system.debug('exception while giving case share permissions '+ex.getMessage());
            return null;
        }
    }
    
    global void execute(Database.BatchableContext BC, List<Case> scope){
        try{
        if(scope != null && scope.size() > 0 ){
            List<caseShare> caseshareToInsert = new list<caseShare>();
            for(Case cs: scope){
	            if(cs.accountId != null && accIds.get(cs.accountId) != null){
	                caseshareToInsert.add(new caseShare(caseId = cs.id,CaseAccessLevel = 'Read',UserOrGroupId = accIds.get(cs.accountId)));
	            }
	        }
            
            if(caseshareToInsert.size() > 0){
    	        database.insert(caseshareToInsert,false);
    	    }
            
        }
        }catch(exception ex){
            system.debug('exception while giving case share permissions '+ex.getMessage());
        }
    }
    
    global void finish(Database.BatchableContext BC){

    }
}