@isTest
private class ThirdUploadFileCSVGenerator_Test {
	
	static void dataSetup()
	{
		No_Trigger__c notriggers = new No_Trigger__c();
    	notriggers.Flag__c = true;
    	insert notriggers;

		//
    	// Create test data
    	//
    	TestHelper.createOrderRequest();

    	

		notriggers.Flag__c = false;
    	update notriggers;
	}

	@isTest static void test_method_one() {
		// Implement test code
		dataSetup();
		Order_Request__c ord = [ SELECT Id, Account__c FROM Order_Request__c LIMIT 1 ];

		Third_Upload_File__c thirdFile = new Third_Upload_File__c(
			Name = 'name',
			Order_Request__c = ord.Id
		);
		insert thirdFile;
		Test.startTest();
		ThirdUploadFileCSVGenerator.createThirdUploadFileCSV(thirdFile.Id);
		Test.stopTest();


	}
}