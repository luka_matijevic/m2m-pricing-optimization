public class MSISDNTriggerDelegate  extends TriggerHandler.DelegateBase{
	
	//find MSISDN Range
	public override void prepareBefore(){
		if (Trigger.isInsert || Trigger.isUpdate ){
			List<MSISDN__c> msisdn = (List<MSISDN__c>) Trigger.new;
			Map<Id, MSISDN_Range__c> rangeMap = new Map<Id, MSISDN_Range__c>([select Id, Range_start__c, Range_end__c from MSISDN_Range__c]);
			List<MSISDN_Range__c> rangeList = rangeMap.Values();
			for (MSISDN__c m :  msisdn)
			{
				for (MSISDN_Range__c mr : rangeList)
				{
					if (AdditionalUtils.compareBigNumbers(mr.Range_start__c, m.MSISDN__c)<=0 && 
						AdditionalUtils.compareBigNumbers(mr.Range_end__c, m.MSISDN__c)>=0
						)
						{
							m.MSISDN_Range__c = mr.Id;
							break;
						}
				}
			}
		
		}


	}
	//create MSISDN-Stock after MSISDN (not isreadyforuse) is created
	
	List<MSISDN_Stock__c> msisdnStockList = new List<MSISDN_Stock__c>();
	Set<Id> msisdnStockForUpdate = new Set<Id>();
	Set<Id> msisdnStockForUpdate2 = new Set<Id>();
	Set<Id> msisdnStockDoNothing = new Set<Id>();
	List<MSISDN_Stock__c> msisdnStockForInsert = new List<MSISDN_Stock__c>();
    List<Stock__C> stockRemoveMSISDN = new List<Stock__c>();
	
	Set<Id> msisdnSet = new Set<Id>();
	Set<Id> stockSet = new Set<Id>();
	List<MSISDN_Stock__c> msisdnStockListForUpdateAll = new List<MSISDN_Stock__c>();
	
	
	public override void prepareAfter(){
		if (Trigger.isInsert || Trigger.isUpdate){
			Map< String, MSISDN__c> msisdnMap= new Map<String, MSISDN__c>();
			for (MSISDN__c m : [select id, MSISDN__c, isReadyForUse__c from MSISDN__c where id in :Trigger.new]){
                if (m.MSISDN__c != null) {
                    msisdnMap.put( m.MSISDN__c, m);
					msisdnSet.add(m.id);
                }
			}

			Map< String, Stock__c> stockMap = new Map<String, Stock__c>();
			system.debug('msisdnMap: ' + msisdnMap);
			for (Stock__c s : [select id, MSISDN__c, SM2M_Status__c, status__c from Stock__c  where /*status__c != 'On Stock' and*/  MSISDN__c in :msisdnMap.keySet()]){
                if (s.status__c != 'On Stock'){
				stockMap.put( s.MSISDN__c, s);
				stockSet.add(s.id);
                }    
                if (s.SM2M_Status__c == Constants.RETIRED) stockRemoveMSISDN.add(s);   
			}
            for (Stock__c s : stockRemoveMSISDN) s.MSISDN__c = '';
			
			Map<String, MSISDN_Stock__c> msisdnStockMap = new Map<String, MSISDN_Stock__c>();

			List<MSISDN_Stock__c> msisdnStockList = [select id, MSISDN__c, isCurrentSIM__c, MSISDN__r.MSISDN__c from MSISDN_Stock__c where MSISDN__c in :msisdnSet ];

			for (MSISDN_Stock__c ms : msisdnStockList) msisdnStockMap.put(ms.MSISDN__r.MSISDN__c, ms);
			system.debug('msisdnMap: ' + msisdnMap);
			system.debug('msisdnSet: ' + msisdnSet);
			system.debug('stockMap ' + stockMap);
			system.debug('msisdnStockMap: ' + msisdnStockMap);
			system.debug('msisdnStockList: ' + msisdnStockList);
			
			for (String msisdn : msisdnMap.keySet()){
				if(	msisdnMap.containsKey(msisdn) &&  !msisdnMap.get(msisdn).isReadyForUse__c 
					&& msisdnStockMap.containsKey(msisdn) &&  msisdnStockMap.get(msisdn).isCurrentSIM__c) 
					msisdnStockForUpdate.add(msisdnStockMap.get(msisdn).Id);
				else if (	msisdnMap.containsKey(msisdn) &&  msisdnMap.get(msisdn).isReadyForUse__c 
					&& msisdnStockMap.containsKey(msisdn) &&  !msisdnStockMap.get(msisdn).isCurrentSIM__c) 
					msisdnStockForUpdate2.add(msisdnStockMap.get(msisdn).Id);
				else if (	msisdnMap.containsKey(msisdn) &&  msisdnMap.get(msisdn).isReadyForUse__c 
					&& msisdnStockMap.containsKey(msisdn) &&  msisdnStockMap.get(msisdn).isCurrentSIM__c){
						msisdnStockDoNothing.add(msisdnStockMap.get(msisdn).Id);
					}
				else if (stockMap.containsKey(msisdn))					
				msisdnStockForInsert.add(
					new MSISDN_Stock__c (
					Stock__c = stockMap.get(msisdn).Id,
					MSISDN__c = msisdnMap.get(msisdn).Id,
					isCurrentSIM__c = msisdnMap.get(msisdn).isReadyForUse__c
					)
				);								
			}
			/*Optimization P. Matkovic*/
			msisdnStockListForUpdateAll = [SELECT Id, isCurrentSIM__c, stock__c 
											FROM MSISDN_Stock__c 
											WHERE (Id in :msisdnStockForUpdate and isCurrentSim__c = true)
												OR (Id in :msisdnStockForUpdate2 and isCurrentSim__c = false)
												OR (stock__c in :stockSet and id not in :msisdnStockForUpdate2 and id not in :msisdnStockForUpdate and id not in :msisdnStockDoNothing)
											];
											
			if (msisdnStockListForUpdateAll != null){
				for (MSISDN_Stock__c ms : msisdnStockListForUpdateAll){
					if (msisdnStockForUpdate.contains(ms.Id) && ms.isCurrentSim__c == true){
						ms.isCurrentSim__c = false;
					}
					else if (msisdnStockForUpdate2.contains(ms.Id) && ms.isCurrentSim__c == false){
						ms.isCurrentSim__c = true;
					}
					else if (stockSet.contains(ms.stock__c) && !msisdnStockForUpdate.contains(ms.Id) && !msisdnStockForUpdate2.contains(ms.Id) && !msisdnStockDoNothing.contains(ms.Id)){
						ms.isCurrentSim__c = false;
					}
				}
			}
			/*End of P. Matkovic optimization*/
		} 
	}
	
	public override void finish()
    {
		try 
		{
			if (msisdnStockListForUpdateAll != null)
				update msisdnStockListForUpdateAll;
			if (msisdnStockForInsert != null)
				insert msisdnStockForInsert;
            if (stockRemoveMSISDN != null)
            	update stockRemoveMSISDN;
		}
		catch ( DmlException e)
		{
			System.debug( e.getMessage());
		}
    }
}