public with sharing class ActivationPlanTriggerDelegate extends TriggerHandler.DelegateBase
{
    Map<Id, Opportunity> parentOpportunities = new Map<Id, Opportunity>();
    
    public override void prepareBefore()
    {
        // do any preparation here – bulk loading of data etc
        /*if (Trigger.isInsert)
        {
            for (SObject obj : Trigger.new) {
                Activation_plan__c actPlan = (Activation_plan__c) obj;
                parentOpportunities.put(actPlan.Opportunity__c, new Opportunity(Id = actPlan.Opportunity__c));
            }
        }*/
    }
    
    public override void prepareAfter()
    {
        // do any preparation here – bulk loading of data etc
        /*if (Trigger.isInsert)
        {
            for (SObject obj : Trigger.new)
            {
                Activation_plan__c actPlan = (Activation_plan__c) obj;
                parentOpportunities.put(actPlan.Opportunity__c, new Opportunity(Id = actPlan.Opportunity__c));
            }
        }
        else if (Trigger.isUpdate || Trigger.isDelete)
        {
            for (SObject obj : Trigger.old)
            {
                Activation_plan__c actPlan = (Activation_plan__c) obj;
                parentOpportunities.put(actPlan.Opportunity__c, new Opportunity(Id = actPlan.Opportunity__c));
            }
        }*/
    }
    
    public override void beforeInsert(sObject o)
    {
    }
    
    public override void beforeUpdate(sObject old, sObject o)
    {
    }
    
    public override void beforeDelete(sObject o)
    {
    }
    
    public override void afterInsert(sObject o)
    {
    }
    
    public override void afterUpdate(sObject old, sObject o)
    {
    }
    
    public override void afterDelete(sObject o)
    {
    }
    
    public override void afterUndelete(sObject o)
    {
    }
    
    public override void finish()
    {
        /*if (trigger.isBefore && trigger.isInsert)
        {
            List<cscfga__Product_Bundle__c> synchronizedBundles = [Select Id, cscfga__Opportunity__c from cscfga__Product_Bundle__c where cscfga__Opportunity__c in :parentOpportunities.keySet() and cscfga__Synchronised_with_Opportunity__c = true];
            
            Set<Id> legacyOpportunityIds = new Map<Id, Opportunity>([Select Id from Opportunity where Id in :parentOpportunities.keySet() and RecordTypeId in (Select Id from RecordType where DeveloperName = 'Legacy')]).keySet(); 
            
            Map<Id, Boolean> opportunityHasSynchronizedBundle = new Map<Id, Boolean>();
            
            for (cscfga__Product_Bundle__c bundle : synchronizedBundles)
            {
                opportunityHasSynchronizedBundle.put(bundle.cscfga__Opportunity__c, true);
            }
        }
        
        if (trigger.isAfter)
        {
            for (Opportunity opportunity : parentOpportunities.values())
            {
                opportunity.Total_Connectivity_Rev_in_current_year__c = M2MYearlyTotalCalculator.calculateConnectivityRevenue(opportunity.Id, Date.today().year());
            }
            update parentOpportunities.values();
        }*/
    }
}