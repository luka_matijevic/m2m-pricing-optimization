public with sharing class StornoTriggerDelegate extends TriggerHandler.DelegateBase  {
    
    Storno__c storno = new Storno__c();
    Daily_Bill_Item__c db = new Daily_Bill_Item__c();
    Daily_Bill__c dailyBill;
     
    public override void prepareBefore() {

    }

    public override void prepareAfter() {

    }
 public override void afterInsert(sObject o) {
      storno = (Storno__c)o;
     

       //db.Anweisung__c= 'Abrechnungszeitraum ' + DateUtils.formatDate(dailyBill.Date__c.addMonths(-1), 'dd.MM.yyyy') + ' bis ' + DateUtils.formatDate(dailyBill.Date__c.addDays(-1), 'dd.MM.yyyy');
  
        db.Art__c=integer.valueOf(storno.Art__c);
        db.Beschreibung__c=storno.name+ ' ' + storno.reason__c;
        db.Vo_NR__c=storno.VO_NR__c;
        db.Betrag__c=storno.Einzel_Nettobetrag__c;
        db.Daily_Bill__c=storno.Daily_Bill__c;
        db.Einzel_Bruttobetrag__c=storno.Einzel_Bruttobetrag__c;
        db.Einzel_Nettobetrag__c=storno.Einzel_Nettobetrag__c;
        db.Einzel_Taxbetrag__c=storno.Einzel_Taxbetrag__c;
        db.Gesamt_Bruttobetrag__c=storno.Gesamt_Bruttobetrag__c;
        db.Gesamt_Nettobetrag__c=storno.Gesamt_Nettobetrag__c;
        db.Gesamt_Taxbetrag__c=storno.Gesamt_Taxbetrag__c;
        db.I_CO__c='';
        db.Konto__c='4120100';
        db.kost__c='465605';
        db.prod__c='387654';
        db.menge__c=1;
        db.Rechnungsnummer__c=storno.Invoice_Number__c;
        db.Steuersatz__c=storno.vat__c;
        db.Waehrung__c='EUR';

    
 }
     public override void beforeInsert(sObject o) {
             storno = (Storno__c)o;

    
    System.debug('***MJ***'+  JSON.serializePretty(o));
    
    List<Prebill__c> prebills = [Select id,     Total_Net_Amount__c,
                    Total_Tax_Amount__c,Invoice_Number__c,Daily_bill__c,
                    Total_Gross_Amount__c from Prebill__c where id= :storno.Prebill__c];
    System.debug('***MJ***'+ storno.Prebill__r.Id);
    
    if (!prebills.isEmpty())
    {
        
    Prebill__c prebill = prebills[0];
    System.debug('***MJ***'+prebill.Total_Gross_Amount__c);
    storno.Invoice_Number__c = prebill.Invoice_Number__c;
    }
    else
    {
        List<Prebill__c> prebills2 = [Select id,     Total_Net_Amount__c,
                    Total_Tax_Amount__c,Invoice_Number__c,Daily_bill__c,
                    Total_Gross_Amount__c from Prebill__c where Invoice_Number__c= :storno.Invoice_Number__c];
                    if (!prebills2.isEmpty())
    {
        
    Prebill__c prebill = prebills2[0];
    storno.Prebill__c = prebill.id;
    }
    }
    
     
     }


    
    public override void finish() {
        
    if (db.Rechnungsnummer__c!=null) {
        List<Daily_Bill__c> dailyBillList= [SELECT Date__c, name, id from Daily_Bill__c where id=:storno.Daily_Bill__c];
        dailyBill = dailyBillList[0];
        db.Anweisung__c= 'Abrechnungszeitraum ' + DateUtils.formatDate(dailyBill.Date__c.addMonths(-1), 'dd.MM.yyyy') + ' bis ' + DateUtils.formatDate(dailyBill.Date__c.addDays(-1), 'dd.MM.yyyy');
        insert db;   
        }
    }
}