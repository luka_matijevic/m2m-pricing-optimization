public class ScheduleSIMReportGeneration implements Schedulable
{
	public void execute(SchedulableContext sc) {
		Date now = Date.today();
		Database.executeBatch(new BatchSIMReportGeneration(now.year()), 100);
	}
}