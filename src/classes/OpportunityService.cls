/**
 * This class is used for calculations of fields on Opportunity object.
 * The service is not static by design, to allow mockups for testing.
 */

public with sharing class OpportunityService {
	/**
	 * Empty constructor. The service is not static by design, to allow mockups for testing.
	 */
	public OpportunityService() {
	}

	public List<Opportunity> prepareSynchronizationState(List<Opportunity> opportunities) {
		
		List<Opportunity> oppsWithState = [
			SELECT 
				Id, HasSynchedBundle__c, Synched_Bundle_Id__c,
				( SELECT cscfga__Synchronised_with_Opportunity__c FROM cscfga__Product_Bundles__r)
			FROM 
				Opportunity
			WHERE 
				Id 
			IN 
				:opportunities
		];
		
		for (Opportunity opp : oppsWithState) {
			
			boolean hasSyncedBundle = false;
			opp.Synched_Bundle_Id__c = null;

			for (cscfga__Product_Bundle__c bundle : opp.cscfga__Product_Bundles__r) {
				if (bundle.cscfga__Synchronised_with_Opportunity__c) {
					hasSyncedBundle = true;
					opp.Synched_Bundle_Id__c = bundle.Id;
					break;
				}
			}
			opp.HasSynchedBundle__c = hasSyncedBundle;
		}
		return oppsWithState;
	}

	public List<Opportunity> extractRelatedOpportunities(List<cscfga__Product_Bundle__c> productBundles) {
		
		List<Opportunity> opps = new List<Opportunity>();
		for (cscfga__Product_Bundle__c bundle : productBundles) {
			opps.add(new Opportunity(Id = bundle.cscfga__Opportunity__c));
		}
		return opps;
	}
	
	public Map<Id, Opportunity> extractRelatedOpportunitiesMap(List<cscfga__Product_Bundle__c> productBundles) {
		Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>();
		
		for (cscfga__Product_Bundle__c bundle : productBundles) {
			if (bundle.cscfga__Opportunity__c != null) {
				opportunityMap.put(bundle.cscfga__Opportunity__c, new Opportunity(Id = bundle.cscfga__Opportunity__c));
			}
		}
		return opportunityMap;
	}
	
}