public /* with sharing */ class CreateFrameContractController {

	private Id oppId;
	public Opportunity opp {get; private set;}
	
	public CreateFrameContractController(ApexPages.StandardController stdController)
	{
		this.oppId = stdController.getId();
		system.debug('++++ oppId: ' + oppId);
		//system.debug('++++ url: ' + oppId);
		opp = [select Id, Name, Amended_From__c, AccountId, CloseDate, RecordTypeId, Synched_Bundle_Id__c from Opportunity where Id = :oppId];
		
	}
	
	
	public pageReference createFrameContract(){
		
		//validate that there are no frame contracts already related to this opportunity
		list<Frame_Contract__c> fcs = [select Id, Opportunity__c from Frame_Contract__c where Opportunity__c = :oppId];
		
		if(!fcs.isEmpty()){
			/*ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,
				'You can only have one Frame Contract per Opportunity.'));*/
			return Page.CreateFrameContractError;
		}
		else{
			//create frame contract
			Frame_Contract__c fc = new Frame_Contract__c(Opportunity__c = opp.Id,
														 Valid_From__c = opp.CloseDate,
														 Product_Bundle__c = opp.Synched_Bundle_Id__c,
														 Customer__c = opp.AccountId,
														 Status__c = 'Frame Contract Requested',
														 Send_Email_to_BSS_Inbox__c = true);
			
			//check which record type should be assigned
			/*
			if(opp.RecordTypeId == Cache.RecordTypeId('Opportunity.Trial Local') || opp.RecordTypeId == Cache.RecordTypeId('Opportunity.Trial Global')){
				fc.RecordTypeId = Cache.RecordTypeId('Frame_Contract__c.Trial');
			}
			else if(opp.RecordTypeId == Cache.RecordTypeId('Opportunity.Commercial Local') || opp.RecordTypeId == Cache.RecordTypeId('Opportunity.Commercial Global')){
				fc.RecordTypeId = Cache.RecordTypeId('Frame_Contract__c.Commercial');
			}
			Luka - record name change Commercial --> Commercial Global, new record type Commercial Local
			*/
			if(opp.RecordTypeId == Cache.RecordTypeId('Opportunity.Trial Local') || opp.RecordTypeId == Cache.RecordTypeId('Opportunity.Trial Global')){
				fc.RecordTypeId = Cache.RecordTypeId('Frame_Contract__c.Trial');
			}
			else if(opp.RecordTypeId == Cache.RecordTypeId('Opportunity.Commercial Local')){
				fc.RecordTypeId = Cache.RecordTypeId('Frame_Contract__c.Commercial Local');
			}
			else if (opp.RecordTypeId == Cache.RecordTypeId('Opportunity.Commercial Global')){
				fc.RecordTypeId = Cache.RecordTypeId('Frame_Contract__c.Commercial');
			}
			
			 OpportunityContactRole oppContactRole = [SELECT ContactId  FROM OpportunityContactRole WHERE OpportunityId = :opp.Id];

			fc.Contact__c = oppContactRole.ContactId;
		

			if (opp.Amended_From__c != null) {
				Frame_Contract__c oldFC = [select Id, Name, Opportunity__r.Name from Frame_Contract__c where Opportunity__c = :opp.Amended_From__c];
				fc.Repriced_From__c = oldFC.id;
				
				if (opp.Name.contains('VVL')){
					fc.ContractExtensionStatus__c = 'Contract Extended successfully';
				} else if (opp.Name.contains('Repricing') && opp.Type =='Repricing'){
					fc.ContractExtensionStatus__c = 'Repricing successfull';					
				}
  			}
			insert fc;



			No_Trigger__c noTrigger = null;
            List<No_Trigger__c> noList = [Select Id, SetupOwnerId, Flag__c FROM No_Trigger__c WHERE SetupOwnerId =: UserInfo.getUserId() LIMIT 1];
            boolean noTriggerUser = false;
            if(noList.isEmpty()){
            	system.debug('1');
                noTriggerUser = true;
                noTrigger = new No_Trigger__c();
                noTrigger.Flag__c = true;
                noTrigger.SetupOwnerId = UserInfo.getUserId();
                system.debug(noTrigger);
                upsert noTrigger;
                system.debug(noTrigger);
                }
            else {
            	system.debug('2');
                noList[0].Flag__c = true;
                upsert noList[0];
                noTrigger = noList[0];
            }

            cscfga__Product_Bundle__c bundle;
            List<cscfga__Product_Bundle__c> pbList = [select Id, cscfga__Opportunity__c, cscfga__Synchronised_with_Opportunity__c 
            							FROM cscfga__Product_Bundle__c WHERE cscfga__Opportunity__c = :opp.id];

            for (cscfga__Product_Bundle__c pbTmp : pbList){
            	if(pbTmp.cscfga__Synchronised_with_Opportunity__c){
            		bundle = pbTmp;
            	}
            }
            if (bundle == null && pbList != null && pbList.size() > 0){
            	bundle = pbList[0];
            }
			//bundle = [select Id, cscfga__Opportunity__c FROM cscfga__Product_Bundle__c WHERE cscfga__Opportunity__c = :opp.id];


			List<cscfga__Product_Configuration__c> pcList = [SELECT Id, Name, Frame_Contract__c FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Bundle__c = :bundle.Id ];
			for(cscfga__Product_Configuration__c pc : pcList){
				pc.Frame_Contract__c = fc.Id;
			}

			update pcList;

			noTrigger.Flag__c = false;
            upsert noTrigger;
            if (noTriggerUser){
                delete noTrigger;
            }


			
			List<Activation_Plan__c> actPlans = [SELECT ID, Frame_Contract__c FROM Activation_Plan__c WHERE Opportunity__c = :oppId];
			for(Activation_Plan__c a : actPlans){
				a.Frame_Contract__c = fc.Id;
			}
			update actPlans;


			//sendEmailToBSS(fc);
			
			//Create billing account and populate account fields
			Account acc = [select id, name, BillingCountry, Vat__c, Language__c, Billing_Cycle__c, Currency__c from account where id = :opp.accountid];
			AccountHelper.PopulateAccountSM2MInformation(acc);
			AccountHelper.CreateBillingAccount(fc);
			
			//return created frame contract
			pageReference frameContractPage = new pageReference ('/' + fc.Id);
			frameContractPage.setredirect(true);
			return frameContractPage;
		}
		
	}
	
/*	public void sendEmailToBSS(Frame_Contract__c fc){
		
		BSS_Inbox__c bssCustomSetting = BSS_Inbox__c.getValues('BSSInboxSettings');
		
		
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		
		mail.setSubject(bssCustomSetting.Email_Subject__c);
		mail.setHtmlBody(bssCustomSetting.Email_Body__c + + '\n' +'<a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + (String) fc.Id + '">' + 'Frame Contract' + '</a>');
		//mail.setPlainTextBody(bssCustomSetting.Email_Body__c + '\n' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + (String) fc.Id);
		
		String[] toAddresses = new String[] {}; 
		toAddresses.addAll(bssCustomSetting.BSS_Email_Adresses__c.split(';'));
		mail.setToAddresses(toAddresses);
		
		//send the email
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}*/
}