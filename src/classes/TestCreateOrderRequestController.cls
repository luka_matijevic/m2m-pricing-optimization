@isTest
private class TestCreateOrderRequestController
{

	//--------------------------------------------------------------------------
	// Test create order request
	//--------------------------------------------------------------------------
    static testMethod void createOrderRequest()
    {
		//
		// Create frame contract with product bundle
		//
    	No_Trigger__c notriggers = new No_Trigger__c();
    	notriggers.Flag__c = true;
    	insert notriggers;

		TestHelper.createFrameContract();
		Frame_Contract__c frameContract = [ SELECT Id FROM Frame_Contract__c LIMIT 1];

    	notriggers.Flag__c = false;
    	update notriggers;

		Test.startTest();
		Test.setCurrentPage(new PageReference('Test?frameContractId=' + frameContract.Id));
		CreateOrderRequestController createOrderRequestController =
				new CreateOrderRequestController(new ApexPages.StandardController(frameContract));
		SelectOption[] billingAccounts = createOrderRequestController.getBillingAccounts();
		createOrderRequestController.selectedBillingAccount = billingAccounts[0].getValue();
		ProductOrderContainer[] pcos = createOrderRequestController.getProductOrders();
		pcos[0].quantity = 1;
		pcos[0].comment = 'Test';
		createOrderRequestController.orderRequest.Business_Model_Description__c = 'Test';
		PageReference pr = createOrderRequestController.create();
		Test.stopTest();

		//
		// Test
		//
		Id billingAccountId = [ SELECT Id FROM Billing_Account__c LIMIT 1 ].Id;

		System.assertEquals(2, billingAccounts.size());
		/*System.assertNotEquals(null, pr);
		pr = createOrderRequestController.cancel();
		System.assertNotEquals(null, pr);*/
    }

    //--------------------------------------------------------------------------
    // Test create order request without comments
    //--------------------------------------------------------------------------
    static testMethod void createOrderRequestWithoutComments()
    {
		//
		// Create frame contract with product bundle
		//
    	No_Trigger__c notriggers = new No_Trigger__c();
    	notriggers.Flag__c = true;
    	insert notriggers;

		TestHelper.createFrameContract();
		Frame_Contract__c frameContract = [ SELECT Id FROM Frame_Contract__c LIMIT 1];

    	notriggers.Flag__c = false;
    	update notriggers;

    	Test.startTest();
		Test.setCurrentPage(new PageReference('Test?frameContractId=' + frameContract.Id));
		CreateOrderRequestController createOrderRequestController =
				new CreateOrderRequestController(new ApexPages.StandardController(frameContract));
		ProductOrderContainer[] pcos = createOrderRequestController.getProductOrders();
		pcos[0].quantity = 1;
		PageReference pr = createOrderRequestController.create();
		System.assertEquals(null, pr);
		System.assertNotEquals(null, createOrderRequestController.errorMessage);
    }

    //--------------------------------------------------------------------------
    // Test create order request without quantities
    //--------------------------------------------------------------------------
    static testMethod void createOrderRequestWithoutQuantity()
    {
		//
		// Create frame contract with product bundle
		//
    	No_Trigger__c notriggers = new No_Trigger__c();
    	notriggers.Flag__c = true;
    	insert notriggers;

		TestHelper.createFrameContract();
		Frame_Contract__c frameContract = [ SELECT Id FROM Frame_Contract__c LIMIT 1];

    	notriggers.Flag__c = false;
    	update notriggers;

    	Test.startTest();
		Test.setCurrentPage(new PageReference('Test?frameContractId=' + frameContract.Id));
		CreateOrderRequestController createOrderRequestController =
				new CreateOrderRequestController(new ApexPages.StandardController(frameContract));
		ProductOrderContainer[] pcos = createOrderRequestController.getProductOrders();
		pcos[0].quantity = 0;
		PageReference pr = createOrderRequestController.create();
		System.assertEquals(null, pr);
		System.assertNotEquals(null, createOrderRequestController.errorMessage);
    }
}