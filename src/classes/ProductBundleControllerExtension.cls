/**
* Product Bundle controller extension used for enablement of submit for approval buttons and related lists on Product Bundle records
*/
global class ProductBundleControllerExtension extends cscfga.ProductBundleController{

    public final cscfga__Product_Bundle__c productBundle {get;set;}
    
    public ProcessInstance currentApprovalProcess { get; set; }
    
     public List<cscfga__Attribute__c> technicalRequests {get;set;}
     
     public boolean technicalRequestsEmpty {get;set;}
     
    //Added by Mahaboob as per T-10908
    public List<cscfga__Attribute__c> pricingRequests {get;set;}
     
    public boolean pricingRequestsEmpty {get;set;}
    //Done by Mahaboob as per T-10908
    
    /**
    * constructor for extension
    */
    public ProductBundleControllerExtension(ApexPages.StandardController stdController) {
        super(stdController);
        
        this.productBundle= [SELECT Technical_Approval_Status__c, Commercial_Approval_Status__c from cscfga__Product_Bundle__c where id = :((cscfga__Product_Bundle__c )stdController.getRecord()).Id];

        //Commented few lines and added new lines by Mahaboob to implement solution for T-10908
        /*List<cscfga__Attribute__c> technicalRequestAttributes = [select cscfga__Value__c, cscfga__Product_Configuration__r.Name from cscfga__Attribute__c 
                                                                  where cscfga__Product_Configuration__r.cscfga__Product_Bundle__c = : productBundle.Id 
                                                                  and Name = 'Request Technical Change'];
        
        technicalRequests = new List<cscfga__Attribute__c>();
        for(cscfga__Attribute__c att : technicalRequestAttributes){
            if(!String.isBlank(att.cscfga__Value__c)){
                technicalRequests.add(att);
            }
        }
        technicalRequestsEmpty = technicalRequests.isEmpty();*/
        
        List<ProcessInstance> approvalProcess = [SELECT Id, Status, TargetObjectId, CreatedDate FROM ProcessInstance WHERE TargetObjectId = :this.productBundle.id ORDER BY CreatedDate DESC];
	        if(approvalProcess != null && approvalProcess.size() > 0) {
	            currentApprovalProcess = approvalProcess[0];
	        }
        
        List<cscfga__Attribute__c> techAndPriceAttributes = [select Name, cscfga__Value__c, cscfga__Product_Configuration__r.Name from cscfga__Attribute__c 
                                                                  where cscfga__Product_Configuration__r.cscfga__Product_Bundle__c = : productBundle.Id 
                                                                  and Name IN ('Request Technical Change', 'Reason for Price Change')];  
        technicalRequests = new List<cscfga__Attribute__c>();
        pricingRequests = new List<cscfga__Attribute__c>();
        for(cscfga__Attribute__c att : techAndPriceAttributes){
            if(att.Name == 'Request Technical Change' && !String.isBlank(att.cscfga__Value__c)){
                technicalRequests.add(att);
            }
            if(att.Name == 'Reason for Price Change' && !String.isBlank(att.cscfga__Value__c)){
                pricingRequests.add(att);
            }
        }
        technicalRequestsEmpty = technicalRequests.isEmpty();
        pricingRequestsEmpty = pricingRequests.isEmpty();
        
        //Done by Mahaboob as per T-10908
        
   }
    
    global override PageReference deleteConfiguration() {
        PageReference redir = super.deleteConfiguration();
        PageReference ret = Page.Configuration_Bundle_Detail_Override;
        ret.getParameters().putAll(redir.getParameters());
        ret.setRedirect(true);
        return ret;
    }
    
    public PageReference submitApprovalWorkaround() {
    
        PageReference pageRef = new PageReference('/p/process/Submit?retURL=%2F' + productBundle.Id + '&id=' + productBundle.Id);
        
        Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
        approvalRequest.setComments('Submitting request for approval.');
        approvalRequest.setObjectId(productBundle.Id);
        
        Id currentUser = UserInfo.getUserId();
        approvalRequest.setSubmitterId(currentUser);
        
        approvalRequest.setProcessDefinitionNameOrId('04aZ0000000ChDW'); // Approval Process ID
        approvalRequest.setSkipEntryCriteria(false);
        
        Approval.ProcessResult apprRequestResult = Approval.process(approvalRequest);
        
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    /* //existed in the previos version of the class but seems not to be used anywhere
    public PageReference submitForApprovalCustom()
    {
        Approval.ProcessSubmitRequest approvalRequest= 
        new Approval.ProcessSubmitRequest();
        approvalRequest.setComments('Approval requested');
        approvalRequest.setObjectId(productBundle.id);
        
        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(approvalRequest);
        
        // Verify the result
        System.assert(result.isSuccess());
        return null;
    }
    */
}