/*
 * Name: CloneUtil.cls 
 * Desc: Utility Class to clone Parent & child records
 * Created By: Ritesh Saxena
 * Date: 07-Jan-2015
*/
public without sharing class CloneUtil {
    
    /*Get sObject name from Id*/
    public static Map<String, Schema.SobjectType> schemaMap = Schema.getGlobalDescribe();
    
    /*
    * createCloneParentRecord Static Method
    * @params - String, String , String 
    */  
    public static Database.SaveResult createCloneParentRecord(String sourceId, String fieldName, String value) {
        
        Savepoint sp = Database.setSavepoint();//For possible rollback if there are errors present in DML
        String objectName = CloneUtil.getObjectNameBasedOnPrefix(sourceId);//Get object name
        String query = CloneUtil.generateSelectAllQuery(objectName, 'Id', sourceId);//Generate query
        List<SObject> currentParentRecord = Database.query(query);//Query child records on child sObject     
        List<SObject> newParentRecord = currentParentRecord.deepClone(false);//Clone queried records
    
        if(fieldName!=null && fieldName!='' && value!=null && value!=''){
            for(SObject s : newParentRecord ) {
                s.put(fieldName, value);//Set new value
                s.put('OwnerId', UserInfo.getUserId());//Set new value
            }
        }
        Database.SaveResult[] newData = Database.Insert(newParentRecord, false);//Insert newly cloned records
        if(!newData.get(0).isSuccess()) {
            Database.rollback(sp);//Rollback if errors present
        }
        return newData.get(0);
    }

    /*
    * createCloneParentRecord Static Method
    * @params - String, String , String 
    */  
    public static Database.SaveResult createCloneParentRecordOpportunity(String sourceId, String fieldName, String value, String oppDescription) {
        
        Savepoint sp = Database.setSavepoint();//For possible rollback if there are errors present in DML
        String objectName = CloneUtil.getObjectNameBasedOnPrefix(sourceId);//Get object name
        String query = CloneUtil.generateSelectAllQuery(objectName, 'Id', sourceId);//Generate query
        List<SObject> currentParentRecord = Database.query(query);//Query child records on child sObject     
        List<SObject> newParentRecord = currentParentRecord.deepClone(false);//Clone queried records
    
        if(fieldName!=null && fieldName!='' && value!=null && value!=''){
            for(SObject s : newParentRecord ) {
                s.put(fieldName, value);//Set new value
                s.put('OwnerId', UserInfo.getUserId());//Set new value
                s.put('Description', 'This is a repriced opportunity from the old ' + oppDescription);
            }
        }
        Database.SaveResult[] newData = Database.Insert(newParentRecord, false);//Insert newly cloned records
        if(!newData.get(0).isSuccess()) {
            Database.rollback(sp);//Rollback if errors present
        }
        return newData.get(0);
    }
    
    /*
    * getObjectNameBasedOnPrefix Static Method
    * @params - String
    * Get the object name of the source Id based on its prefix(first three characters of sObject)
    */  
    public static String getObjectNameBasedOnPrefix(String id) {
        String objectName = '';
        if(id.length() == 15 || id.length() == 18) {//Standard Id size of 15 or 18
            for(Schema.SobjectType schema : schemaMap.values()) {//Loop through Metadata to get object name via prefix
                if(schema.getDescribe().getKeyPrefix() != null && id.startsWith(schema.getDescribe().getKeyPrefix())) {
                    objectName = schema.getDescribe().getName();
                }
            }
        }
        return objectName;
    }
    
    /*
    * generateSelectAllQuery Static Method
    * @params - String, String, String
    * Generate query based for child sObject via the source Id. Verify that fields are accessible, 
    * creatable, not unique, not auto number, not calculated, not formula
    */  
    public static String generateSelectAllQuery(String objectName, String fieldName, String sourceId) {
        String query = 'select Id';
        for(Schema.SObjectField field : schemaMap.get(objectName).getDescribe().fields.getMap().values()) {
            if(field.getDescribe().isAccessible() && field.getDescribe().isCreateable() && !field.getDescribe().isUnique() &&
                !field.getDescribe().isAutoNumber() && !field.getDescribe().isCalculated() /*&& !field.getDescribe().isDefaultedOnCreate()*/) {
                query += ',' + field.getDescribe().getName();//Add field names to query
            }
        }       
        query += ' from ' + objectName + ' where ' + fieldName + '= \'' + sourceId + '\'';              
        return query;
    }
    
    
    /*
    * createCloneChildRecords Static Method
    * @params - String, String, String , String      
    */  
    public static List<Database.Error> createCloneChildRecords(String sourceId, String destinationId, String objectName, String fieldName) {
        Savepoint sp = Database.setSavepoint();//For possible rollback if there are errors present in DML   
        String query = CloneUtil.generateSelectAllQuery(objectName, fieldName, sourceId);//Generate query
        List<SObject> currentRecords = Database.query(query);//Query child records
        List<SObject> newRecords = currentRecords.deepClone(false);//Clone queried records
        for(SObject s : newRecords) {
            s.put(fieldName, destinationId);//Set new parent Id
        }
        Database.SaveResult[] databaseResults = Database.Insert(newRecords, false);//Insert newly cloned records.            
        List<Database.Error> errorsFromSave = new List<Database.Error>();
        for(Database.SaveResult dbResult : databaseResults) {
            if(!dbResult.isSuccess()) {//Validate if there were any errors on the insert database process
                errorsFromSave.addAll(dbResult.getErrors());
            }
        }      
        if(!errorsFromSave.isEmpty()) {//If there were errors, rollback entire process
            Database.rollback(sp);
        }
        return errorsFromSave;
    }
}