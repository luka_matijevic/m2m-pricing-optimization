@isTest
public class BatchPrebillFetcherTest {
    static testMethod void testBatchPrebillFetcher() {
        Daily_bill__c dBill = new Daily_Bill__c(Date__c = Date.newInstance(System.now().year(), System.now().month(), 1), Prebill_Requests_Sent__c = 0);
        insert dBill;
        
        Account acc = Test_Util.createAccount('Account Test');
        acc.Global__c = true;
        //Address addr = new Address(BillingAddress = 'Test', BillingCity = 'Zagreb', BillingCountry = 'Croatia', BillingCountryCode = 'DE', BillingPostalCode = '10000', BillingState = 'Zagreb', BillingStreet = 'Empty 46');
        acc.Billing_Cycle__c = '1';
        acc.VAT__c = 19;
        acc.BillingCity = 'Troisdorf';
        acc.BillingCountry = 'Germany';
        acc.BillingPostalCode = '53844';
        acc.BillingStreet = 'Junkersring 57';
        insert acc;
        Billing_Account__c bac = new Billing_Account__c(Payer__c = acc.Id, Name = 'Test Billing Acc', 
                                                        SM2M_Type__c = 'Global', VAT__c = '19', 
                                                        Activation_token__c = 'NotNull', Trial__c = false);
        insert bac;
        
        csord__Order_Request__c orq = new csord__Order_Request__c(Name = 'test', csord__Module_Name__c = 'test module', csord__Module_Version__c = '2f2f2f');
        insert orq;
        
        csord__Subscription__c connSubscriptions = new csord__Subscription__c(Billing_Account__c = bac.Id, 
                                                                              csord__Identification__c = Constants.PRODUCT_TYPE_CONNECTIVITY,
                                                                              csord__Order_Request__c = orq.Id, 
                                                                              csord__Status__c = 'Test');
        insert connSubscriptions;
        
        Database.executeBatch(new BatchPrebillFetcher(dBill.Date__c));
    }
}