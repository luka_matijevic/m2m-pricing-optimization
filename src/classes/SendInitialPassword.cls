global class SendInitialPassword {
    
    webService static String updateBillingAccountAndSendInitialPassword(Id billingAccountId) {
        try {
            string returnMessage='';
            Billing_Account__c ba = [SELECT Id, OFI_ID__c, Billing_Contact__c, Activation_token__c, Billing_Email__c FROM Billing_Account__c WHERE Id = :billingAccountId];
            
            if (ba.Billing_Email__c != null && ba.Activation_token__c == null)
            {
                ba.Activation_token__c = generateRandomString(32);
                update ba;
                String[] addresses = new String[]{};
                    addresses.add(ba.Billing_Email__c);
                sendTemplatedEmail(addresses, 'Send_Initial_Password_Template', ba.Activation_token__c, ba.OFI_ID__c);
                
                returnMessage = 'Billing Account is successfully updated and initial password successfully created!';
            }
            if (ba.Billing_Email__c == null)  returnMessage += 'Billing Account is missing Contact email. Please populate it and then try again!';
            /* Additional contacts */            
            List<WI_Additional_Contacts__c> acList = [SELECT Id, Name, email_address__c, Activation_token__c  FROM WI_Additional_Contacts__c WHERE Billing_Account__c = :billingAccountId];
			
            for (WI_Additional_Contacts__c ac : acList){
                if (ac.email_address__c != null && ac.Activation_token__c == null){
                    ac.Activation_token__c = generateRandomString(32);
                    update ac;
                    String[] addresses = new String[]{};
                    addresses.add(ac.email_address__c);
                sendTemplatedEmail(addresses, 'Send_Initial_Password_Template', ac.Activation_token__c, ac.Name);
                
                returnMessage += '\n\rWI Additional Contact is successfully updated and initial password successfully created!';
                }
            }
            return returnMessage;
        }
        catch(exception ex) {
            return ex.getMessage();
        }
    }

    webService static String updateAdditionalContactAndSendInitialPassword(Id additionalContactId){
          try {
            string returnMessage='';
        	List<WI_Additional_Contacts__c> acList = [SELECT Id, Name, email_address__c, Activation_token__c  FROM WI_Additional_Contacts__c WHERE id = :additionalContactId];			
            for (WI_Additional_Contacts__c ac : acList){
                if (ac.email_address__c != null && ac.Activation_token__c == null){
                    ac.Activation_token__c = generateRandomString(32);
                    update ac;
                    String[] addresses = new String[]{};
                    addresses.add(ac.email_address__c);
                sendTemplatedEmail(addresses, 'Send_Initial_Password_Template', ac.Activation_token__c, ac.Name);
                
               returnMessage += '\n\rWI Additional Contact is successfully updated and initial password successfully created!';
                }
            }
            return returnMessage;
        }
        catch(exception ex) {
            return ex.getMessage();
        }
    }

    
    private static String getRandomNumber(Integer size) {
        Double d = Math.random() * size;
        Integer intValue = d.intValue();
        String returnValue = ('0000' + String.valueOf(intValue)).substring(String.valueOf(intValue).length());
        return returnValue;
    }
    
    public static String generateRandomString(Integer len) {
        String alphanumeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), alphanumeric.length());
            randStr += alphanumeric.substring(idx, idx+1);
        }
        return randStr; 
    }
    
    public static void sendTemplatedEmail(String[] toRecipients, String templateApiName, String activationToken, String ofiID) {
        //  templateId   must be ID of an Email template
        //  targetObjId must be a Contact, User, Lead Id -- also used in merge fields of template recipient.xxxx
        //  whatId    must be an SObject that is used in the merge fields of the template relatedTo.xxxx
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        String templateHtmlValue;
        
        try {
            templateHtmlValue = [select id, name, HtmlValue from EmailTemplate where developername = :templateApiName].HtmlValue;
        }
        catch (Exception e) { }
        
        
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'servicereply@m2m.telefonica.de'];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        if ( owea.size() > 0 ) {
            email.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        
        
        
        templateHtmlValue = templateHtmlValue.replace('Activation_token__c', activationToken);
        templateHtmlValue = templateHtmlValue.replace('OFI_ID__c', ofiID);
        
        email.setToAddresses(toRecipients);
        email.setHtmlBody(templateHtmlValue);
        email.setSubject('Aktivierung my.m2m Portal');
        
        try {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
            return;
        }
        catch (EmailException e) {}
    }
}