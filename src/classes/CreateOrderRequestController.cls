public with sharing class CreateOrderRequestController
{
	//--------------------------------------------------------------------------
	// Private members
	//--------------------------------------------------------------------------
	private Id frameContractId;
	private CreateOrderRequest createOrderRequest;
	Id[] billingAccounts;
	private Geotab_settings__c geotabSettings;

	//--------------------------------------------------------------------------
	// Getters / setters
	//--------------------------------------------------------------------------
	public Order_Request__c orderRequest { get; set; }
	public Frame_Contract__c frameContract { get; set; }
	public String errorMessage { get; set; }
	public String selectedBillingAccount { get; set; }
	public String selectedBillingAccountSubGroup { get{
														if(selectedBillingAccountSubGroup == null)
															selectedBillingAccountSubGroup ='No Tariff';
														return selectedBillingAccountSubGroup;
													}
								 					set{
								 						if(selectedBillingAccountSubGroup == null)
															selectedBillingAccountSubGroup ='No Tariff';
								 						} }
	public Order_Request__c ord { get; set; }
	public List<Billing_Account_Subscription_Group__c> billingAccountSubscriptionGroups;
	

	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	public CreateOrderRequestController(ApexPages.StandardController controller)
	{
		frameContractId = ApexPages.currentPage().getParameters().get('frameContractId');

		FrameContract =
				[ SELECT Name, Customer__c, Customer__r.ShippingStreet,
				  Customer__r.ShippingCity, Customer__r.ShippingPostalCode,
				  Customer__r.ShippingState, Customer__r.ShippingCountry,
				  Opportunity__r.Product_Family__c, Opportunity__r.Record_Type_Name__c,
				  Opportunity__r.Id
				  FROM Frame_Contract__c
				  WHERE Id = :frameContractId ];

		createOrderRequest = Factory.createCreateOrderRequest(frameContractId);

		//
		// Pre-populate account and biling rate on load
		//
		orderRequest = new Order_Request__c();
		orderRequest.Account__c = FrameContract.Customer__c;
		orderRequest.Contact__c = getContactFromOpportunityContactRole(FrameContract.Opportunity__r.Id);
		if (FrameContract.Opportunity__r.Product_Family__c == 'Mobility' && FrameContract.Opportunity__r.Record_Type_Name__c == 'Commercial Global'){
			orderRequest.RecordTypeId = getGeotabCustomSettings().Order_Request_Type_Id__c;
		}
		//orderRequest.Shipping_Street__c = FrameContract.Customer__r.ShippingStreet;
		//orderRequest.Shipping_City__c = FrameContract.Customer__r.ShippingCity;
		//orderRequest.Shipping_Postal_Code__c = FrameContract.Customer__r.ShippingPostalCode;
		//orderRequest.Shipping_State__c = FrameContract.Customer__r.ShippingState;
		//orderRequest.Shipping_Country__c = FrameContract.Customer__r.ShippingCountry;
		//System.debug(selectedBillingAccount);
	}


	public List<SelectOption> getBillingAccountSubscriptionGroups(){
		List<Billing_Account_Subscription_Group__c>  billingAccountSubscriptionGroups = [SELECT Id, Name, Service_Pack_Name__c FROM Billing_Account_Subscription_Group__c WHERE Billing_Account__c = :selectedBillingAccount];
		System.debug('sub gorupew ' + billingAccountSubscriptionGroups);
		List<SelectOption> lista = new List<SelectOption>();
		lista.add(new SelectOption('0', 'No Tariff'));
		lista.add(new SelectOption('1', 'More than 1 Tariff'));
		for(Billing_Account_Subscription_Group__c b : billingAccountSubscriptionGroups){
			lista.add(new SelectOption(b.Id, b.Service_Pack_Name__c));
		}
		return lista;
	}

	public void gotSelectedAccount(){
		System.debug('Selected account ' + selectedBillingAccount);
	}

	//--------------------------------------------------------------------------
	// Billing accounts that belong to order request account
	//--------------------------------------------------------------------------
	public List<SelectOption> getBillingAccounts()
	{
		Billing_Account__c[] billingAccounts =
				[ SELECT Id, Name
				  FROM Billing_Account__c
				  WHERE Payer__c = :orderRequest.Account__c ];
		List<SelectOption> returnValue = new List<SelectOption>{new SelectOption('','None')};
		for (Billing_Account__c billingAccount : billingAccounts)
		{
			returnValue.add(new SelectOption(billingAccount.Id, billingAccount.Name));
		}

		return returnValue;
	}

	//--------------------------------------------------------------------------
	// Get product orders to enter quantities
	//--------------------------------------------------------------------------
	public List<ProductOrderContainer> getProductOrders()
	{
		return createOrderRequest.getProductOrders();
	}

	//--------------------------------------------------------------------------
	// Cancel changes
	//--------------------------------------------------------------------------
	public PageReference cancel()
	{
		return new PageReference('/' + frameContractId);
	}

	//--------------------------------------------------------------------------
	// Submit changes
	//--------------------------------------------------------------------------
	public PageReference create()
	{
		//
		// If there is no quantities
		//
		errorMessage = null;
		// #Dev_207 - Business model on order request shall be mandatory
		if(string.isBlank(orderRequest.Business_Model_Description__c))
		{
			errorMessage = 'Business Model on Order Request is mandatory!';
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage) );
		}
		if(!validateComments())
		{
			errorMessage = 'Comments are mandatory for product orders with quantity greater than zero!';
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage) );
		}
		if (!checkQuantities())
		{
			errorMessage = 'At least one product order quantity must be greater than zero!';
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage) );
		}
		if (!validateShippingAddress())
		{
			errorMessage = 'Shipping Address must be provided!';
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage) );
		}

		if(!string.isBlank(errorMessage))
			return null;



		orderRequest.Opportunity__c = frameContract.Opportunity__r.Id;
		orderRequest.Billing_Account__c = selectedBillingAccount;
		if(selectedBillingAccountSubGroup != 'No Tariff' && selectedBillingAccountSubGroup != 'More than 1 Tariff'){
			orderRequest.Billing_Account_Subscription_Group__c = selectedBillingAccountSubGroup;
		}
		System.debug(orderRequest);
		Id orderRequestId = createOrderRequest.create(orderRequest);

		if (orderRequest.RecordTypeId == getGeotabCustomSettings().Order_Request_Type_Id__c){
			if (!validateGeotabQuantities()){
				errorMessage = 'Geotab orders must have 1 service and sum of dongle quantities must be the same as service quantity!';
				ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage) );
			}
			if(!string.isBlank(errorMessage)){
				delete orderRequest;
				return null;
			}
			else{
				setExpectedNumberOfStocks(orderRequest);
				setDefaultGeotabOptions(orderRequest);
				System.debug(orderRequest);
				upsert orderRequest;

		
			}
		}

		List<Activation_Plan__c> actPlans = [SELECT ID, M2M_Order_Request__c FROM Activation_Plan__c WHERE Opportunity__c = :orderRequest.Opportunity__c];
		for(Activation_Plan__c a : actPlans){
			a.M2M_Order_Request__c = orderRequestId;
		}
		update actPlans;		

		return new PageReference('/' + orderRequestId);
	}

	//--------------------------------------------------------------------------
	//getAddressFromAccount
	//--------------------------------------------------------------------------
	public PageReference getAddressFromAccount(){
		if (FrameContract == null){
			FrameContract =
				[ SELECT Name, Customer__c, Customer__r.ShippingStreet,
				  Customer__r.ShippingCity, Customer__r.ShippingPostalCode,
				  Customer__r.ShippingState, Customer__r.ShippingCountry
				  FROM Frame_Contract__c
				  WHERE Id = :frameContractId ];
		}
		orderRequest.Shipping_Street__c = FrameContract.Customer__r.ShippingStreet;
		orderRequest.Shipping_City__c = FrameContract.Customer__r.ShippingCity;
		orderRequest.Shipping_Postal_Code__c = FrameContract.Customer__r.ShippingPostalCode;
		orderRequest.Shipping_State__c = FrameContract.Customer__r.ShippingState;
		orderRequest.Shipping_Country__c = FrameContract.Customer__r.ShippingCountry;
		return null;
	}
	//--------------------------------------------------------------------------
	// Check quantities. Negative values are not allowed and at least one
	// must be greater than zero
	//--------------------------------------------------------------------------
	private Boolean checkQuantities()
	{
		Boolean quantityOk = false;
		List<ProductOrderContainer> productOrderContainers =
				createOrderRequest.getProductOrders();
		for (ProductOrderContainer pco : productOrderContainers)
		{
			if (pco.quantity != null && pco.quantity > 0)
			{
				quantityOk = true;
			}
		}
		return quantityOk;
	}

	//--------------------------------------------------------------------------
	// Validate comments. All comments have to be entered
	// #Dev_200 - On order request comment shall be only mandatory in case there is also an amount specified
	//--------------------------------------------------------------------------
	private Boolean validateComments()
	{
		List<ProductOrderContainer> productOrderContainers = createOrderRequest.getProductOrders();
		for (ProductOrderContainer pco : productOrderContainers)
		{
			if (pco.quantity != null && pco.quantity > 0)
			{
				if (string.isBlank(pco.comment))
					return false;
			}
		}
		return true;
	}

	//--------------------------------------------------------------------------
	// Validate Shipping Address. All fields have to be entered
	//--------------------------------------------------------------------------
	private Boolean validateShippingAddress()
	{
		Boolean isOk = true;
		if (String.isBlank(OrderRequest.Shipping_Street__c))
			isOk = false;
		if (String.isBlank(OrderRequest.Shipping_City__c))
			isOk = false;
		if (String.isBlank(OrderRequest.Shipping_Postal_Code__c))
			isOk = false;
		/*if (String.isBlank(OrderRequest.Shipping_State__c))
			isOk = false;*/
		if (String.isBlank(OrderRequest.Shipping_Country__c))
			isOk = false;

		return isOk;
	}

	public PageReference ProductOrderQuantityChanged() {
		return null;
	}

	private GEOTAB_Settings__c getGeotabCustomSettings(){
		if (geotabSettings == null){
			geotabSettings = GEOTAB_Settings__c.getInstance();
		}
		return geotabSettings;
	}

	//--------------------------------------------------------------------------
	// Validate Geotab Quantities. Number of service quantity must be the same as the sum of dongle quantities
	//--------------------------------------------------------------------------
	private Boolean validateGeotabQuantities(){
		Boolean quantityOk = false;

		List<product_order__c> productOrderList = [SELECT name, id, Quantity__c from product_order__c where order_request__c = :orderRequest.Id];
		List<product_configuration_order__c> pconfList = [select id, name, product_configuration__c, product_order__c from product_configuration_order__c where product_order__c in :productOrderList];
		List<cscfga__attribute__c> myList = [select name, id, cscfga__value__c, cscfga__product_configuration__c from cscfga__attribute__c where cscfga__product_configuration__c in (
		select product_configuration__c from product_configuration_order__c where product_order__c in :productOrderList) and name = 'Article Info Set'];
		Set<String> aisIdList = new Set<String>();

		Map<String, product_order__c> articleInfoSetProductOrderMap = new Map<String, product_order__c>();
		for (cscfga__attribute__c att : myList){
		    if (!String.isEmpty(att.cscfga__value__c)){
		        aisIdList.add(att.cscfga__value__c);
		        for (product_configuration_order__c pconf : pconfList){
		        	for (product_order__c po : productOrderList){
		        		if (po.id == pconf.product_order__c){
		        			articleInfoSetProductOrderMap.put(att.cscfga__value__c, po);
		        		}
		        	}
		        }
		    }
		}

		//List<Article_Info_Set__c> aisList = [select id, name, geotab_hardware_code__c, geotab_dongle__c from Article_Info_Set__c where id in :aisIdList and geotab_hardware_code__c = null];
		List<Article_Info_Set__c> aisList = [select id, name, geotab_hardware_code__c, geotab_dongle__c from Article_Info_Set__c where id in :aisIdList];
		

		Integer serviceQuantity = 0;
		Integer differentServiceQuantity = 0;
		Integer dongleQuantity = 0;
		//for each article info set find its product configuration order and check if it is dongle or service and count number
		for (Article_Info_Set__c ais : aisList){
			if (ais.geotab_dongle__c == true){
				if (articleInfoSetProductOrderMap.get(ais.Id) != null){
					dongleQuantity = dongleQuantity + Integer.valueOf(articleInfoSetProductOrderMap.get(ais.Id).Quantity__c);
				}
			}
			else if (String.isEmpty(ais.geotab_hardware_code__c)){
				differentServiceQuantity++;
				serviceQuantity = serviceQuantity + Integer.valueOf(articleInfoSetProductOrderMap.get(ais.Id).Quantity__c);
			}
		}

		if (serviceQuantity != dongleQuantity || differentServiceQuantity != 1){
			quantityOk = false;
		}
		else{
			quantityOk = true;
		}

		System.debug(serviceQuantity);
		System.debug(differentServiceQuantity);
		System.debug(dongleQuantity);
		return quantityOk;
	}

	private void setExpectedNumberOfStocks(Order_Request__c orderRequest){
		List<product_order__c> productOrderList = [SELECT name, id, Quantity__c from product_order__c where order_request__c = :orderRequest.Id];
		List<cscfga__attribute__c> myList = [select name, id, cscfga__value__c from cscfga__attribute__c where cscfga__product_configuration__c in (
		select product_configuration__c from product_configuration_order__c where product_order__c in :productOrderList) and name = 'Article Info Set'];
		Set<String> aisIdList = new Set<String>();
		for (cscfga__attribute__c att : myList){
		    if (!String.isEmpty(att.cscfga__value__c)){
		        aisIdList.add(att.cscfga__value__c);
		    }
		}

		List<Article_Info_Set__c> aisList = [select id, name, geotab_hardware_code__c, Geotab_Dongle__c from Article_Info_Set__c where id in :aisIdList and geotab_hardware_code__c = null];

		Integer numOfStocksInHardware = 0;
		for (product_order__c po : productOrderList){
		    for (Article_Info_Set__c ais : aisList){
		        if (ais.name.equalsIgnoreCase(po.name)){
		            numOfStocksInHardware += Integer.valueOf(po.Quantity__c);
		        }
		    }    
		}

		orderRequest.Order_Quantity__c=numOfStocksInHardware;
	}

	private void setDefaultGeotabOptions(Order_Request__c orderRequest){
		orderRequest.Geotab_Warranty_Option__c = getGeotabCustomSettings().DefaultGeotabWarrantyOptionId__c;
		String poNumber = null;
		if (String.isEmpty(orderRequest.Name)){
			List<Order_Request__c> orList = [SELECT id, Name from Order_Request__c where id = :orderRequest.Id];
			if (orList != null && orList.size()>0){
				poNumber = orList[0].Name;
			}
		}
		orderRequest.Geotab_Purchase_Order_Number__c = poNumber;
	}

	private String getContactFromOpportunityContactRole(String idOpportunity){
		List<OpportunityContactRole> oppContactRoleList = [SELECT Id, Contactid FROM OpportunityContactRole WHERE OpportunityId = :idOpportunity LIMIT 1];
		String idContact = null;
		if (oppContactRoleList != null && oppContactRoleList.size()>0){
			idContact = oppContactRoleList[0].Contactid;
		}
		return idContact;
	}
}