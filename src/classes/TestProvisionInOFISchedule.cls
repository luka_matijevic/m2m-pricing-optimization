@isTest
private class TestProvisionInOFISchedule
{
    //--------------------------------------------------------------------------
    // Schedule job
    //--------------------------------------------------------------------------
    static testMethod void schedule()
    {
    	Test.startTest();
		System.schedule('TestProvisionInOFI', '0 0 0 3 9 ? 2022', new ProvisionInOFISchedule());
    	Test.stopTest();
    }

}