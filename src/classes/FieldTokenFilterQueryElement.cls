/**
 * A field token filtering query element. Represents an intermediate state in query 
 * construction that has to be completed with a field token operation
 */ 
public class FieldTokenFilterQueryElement {

	FieldTokenFilterQuery query;
	Schema.SObjectField field;
	
	/**
	 * Constructor. Takes a valid query and a field name to append to the query
	 * @param query Field filtering query
	 * @param field Field to be appended to the query along with an operation
	 */ 
	public FieldTokenFilterQueryElement(FieldTokenFilterQuery query, Schema.SObjectField field) {
		this.query = query;
		this.field = field;
	}

	/**
	 * Helper method. Adds a filtering criterium to the query that consists of
	 * a field with which the element was constructed, a comparison criterium and
	 * a value to compare with
	 * @param criterium Comparison criterium
	 * @param value Value to compare field with
	 */ 
	private FieldTokenFilterQuery filterWith(Comparison criterium, Object value) {
		return query.addCriterium(field, criterium, value);
	}

	/**
	 * Equality comparison
	 */ 
	public FieldTokenFilterQuery equals(Object value) {
		return filterWith(Comparison.EQUALS, value);
	}

	/**
	 * equals alias method
	 */ 
	public FieldTokenFilterQuery eq(Object value) {
		return equals(value);
	}

	/**
	 * Inequality comparison
	 */ 
	public FieldTokenFilterQuery notEquals(Object value) {
		return filterWith(Comparison.NOT_EQUALS, value);
	}

	/**
	 * notEquals alias method
	 */ 
	public FieldTokenFilterQuery neq(Object value) {
		return notEquals(value);
	}
 
	public FieldTokenFilterQuery lessThan(Object value) {
		return filterWith(Comparison.LESS_THAN, value);
	}

	public FieldTokenFilterQuery lt(Object value) {
		return lessThan(value);
	}

	public FieldTokenFilterQuery lessThanOrEquals(Object value) {
		return filterWith(Comparison.LESS_THAN_OR_EQUALS, value);
	}

	public FieldTokenFilterQuery leq(Object value) {
		return lessThanOrEquals(value);
	}

	public FieldTokenFilterQuery greaterThan(Object value) {
		return filterWith(Comparison.GREATER_THAN, value);
	}

	public FieldTokenFilterQuery gt(Object value) {
		return greaterThan(value);
	}

	public FieldTokenFilterQuery greaterThanOrEquals(Object value) {
		return filterWith(Comparison.GREATER_THAN_OR_EQUALS, value);
	} 

	public FieldTokenFilterQuery geq(Object value) {
		return greaterThanOrEquals(value);
	}
}