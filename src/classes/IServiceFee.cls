public interface IServiceFee
{
	//--------------------------------------------------------------------------
	// Getters / setters
	//--------------------------------------------------------------------------
	Service_Fee__c getServiceFee();

	String getChargedPer();

	void setChargedPer(String chargedPer);

	Boolean getShowOnQuote();

	void setShowOnQuote(Boolean showOnQuote);

	String getName();

	void setName(String name);

	Decimal getOneTimeBaseFee();

	void setOneTimeBaseFee(Decimal oneTimeBaseFee);

	Decimal getOneTimeTargetFee();

	void setOneTimeTargetFee(Decimal oneTimeTargetFee);

	Decimal getRecurringBaseFee();

	void setRecurringBaseFee(Decimal RecurringBaseFee);

	Decimal getRecurringTargetFee();

	void setRecurringTargetFee(Decimal RecurringTargetFee);

	Boolean getCanDiscount();

	void setCanDiscount(Boolean canDiscount);

	Boolean getIsTargetFeeDiscounted();

	Boolean isOneTimeTargetFeeDiscounted();

	Boolean isRecurringTargetFeeDiscounted();

	//--------------------------------------------------------------------------
	// Clone service fee
	//--------------------------------------------------------------------------
	IServiceFee deepClone();

	//--------------------------------------------------------------------------
	// Compares two service fees
	//--------------------------------------------------------------------------
	Boolean equals(IServiceFee serviceFee);

	//--------------------------------------------------------------------------
	// Compare two service fees without configuration changes
	//--------------------------------------------------------------------------
	Boolean equalsWithoutConfiguration(IServiceFee serviceFee);
}