public class ProductOrder
{
	//--------------------------------------------------------------------------
	// Inner line item
	//--------------------------------------------------------------------------
	public class LineItem
	{
		public String name;
		public String description;
		public Boolean recurring;
		public Boolean sm2m;
		public Decimal price;

		//----------------------------------------------------------------------
		// Constructor
		//----------------------------------------------------------------------
		public LineItem() {}

		public LineItem(LineItem li)
		{
			name = li.name;
			description = li.description;
			recurring = li.recurring;
			sm2m = li.sm2m;
			price = li.price;
		}
	}

	//--------------------------------------------------------------------------
	// Inner product configuration class
	//--------------------------------------------------------------------------
	public class ProductConfiguration
	{
		public String name;
		public String originalProductName;
		public Decimal contractTerm;
		public String billingRateModel;
		public String aisRecordType;
		public id productConfigurationId;
		public Id productConfigurationOrderId;
		public Id oneTimeJournalMappingId;
		public Id recurringJournalMappingId;
		public List<LineItem> lineItems;
		public List<ProductOrder> productOrders;

		//----------------------------------------------------------------------
		// Constructor
		//----------------------------------------------------------------------
		public ProductConfiguration() {}

		public ProductConfiguration(ProductConfiguration pc)
		{
			name = pc.name;
			originalProductName = pc.originalProductName;
			contractTerm = pc.contractTerm;
			billingRateModel = pc.billingRateModel;
			aisRecordType = pc.aisRecordType;
			productConfigurationId = pc.productConfigurationId;
			productConfigurationOrderId = pc.productConfigurationOrderId;
			oneTimeJournalMappingId = pc.oneTimeJournalMappingId;
			recurringJournalMappingId = pc.recurringJournalMappingId;
			lineItems = pc.lineItems;
			productOrders = pc.productOrders;
		}
	}

	//--------------------------------------------------------------------------
	// Members
	//--------------------------------------------------------------------------
	public String name;
	public Decimal quantity;
	public String comment;
	public List<ProductConfiguration> productConfigurations;

	//----------------------------------------------------------------------
	// Constructor
	//----------------------------------------------------------------------
	public ProductOrder() {}

	public ProductOrder(ProductOrder po)
	{
		name = po.name;
		quantity = po.quantity;
		comment = po.comment;
		productConfigurations = po.productConfigurations;
	}
}