@isTest
private class CaseUtilsTest {
	
	public static testMethod void testCaseUtils() {
        Account account = new Account(
        	Name = 'Test3',
            Type = 'Business'
        	);
        
        account.Billing_Cycle__c = '2';
        account.VAT__c = 19;
        account.BillingCity = 'Troisdorf';
        account.BillingCountry = 'Germany';
        account.BillingPostalCode = '53844';
        account.BillingStreet = 'Junkersring 57';
        account.Global__c = true;
        insert account;
        
        Billing_Account__c bac = new Billing_Account__c(Payer__c = account.Id, Name = 'Test Billing Acc', SM2M_Type__c = 'Global', VAT__c = '19', Activation_token__c = 'NotNull');
        insert bac;
        
        Case c = Test_Util.createCase('Service','Sixt','Open', account.id, 'Test Demo111', '24', 'Standard', 'Email','High', 'test@t.com');
        c.Nettobetrag_in__c = 200;
        c.Bescreibung_m__c = 'Beschreibung';
        c.Verursacher__c = 'verursacher';
        c.Account_number__c = '22';
        c.Sixt_buildability_status__c = 'Negative';
        c.Sixt_Cable_type__c = null;
        c.Sixt_LVID__c = '123456789';
    	c.Sixt_Vehicle_manufacturer__c = 'Opel';
    	c.Sixt_Vehicle_model__c = 'Nothing';
    	c.Sixt_Vehicle_production_year__c = '2004';
    	c.Sixt_Contract_duration__c = '36';
    	c.Sixt_Additional_details__c = '2222';
        insert c;
        
        test.startTest();
		CaseUtils.refreshRelatedBillingAccounts(c.Id);
        CaseUtils.createGutschriftCase(c.Id);
        CaseUtils.startGutschriftApprovalProcess(c.Id);
        CaseUtils.sendBuildabilityStatus(c.Id);
        CaseUtils.createOrderForSixt(c.Id);
        test.stopTest();
	}
}