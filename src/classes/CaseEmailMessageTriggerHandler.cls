/**
 * This class is used as a trigger handler delegate which handles all the triggering events related to
 * Email Message object related to Case
 * */
public with sharing class CaseEmailMessageTriggerHandler extends TriggerHandler.DelegateBase {

	private String NEW_EMAIL_CHATTER_NOTIFICATION = 'A new email has arrived on the case.';
	private String CASE_NOTIFICATION_SUBJECT = 'Email Case Notification';
	private List<Id> caseIds = new List<Id>();
	private List<Case> caseList = new List<Case>();
	private Map<Id, Case> casesMap = new Map<Id, Case>();

	private List <BackUp_Person__c> backupPersonsList = new List <BackUp_Person__c>();
	private List <Id> ownerIds = new List <Id>();
	private Map <Id, BackUp_Person__c> backUpPersonsMap = new Map <Id, BackUp_Person__c>();

	private List <Task> tasksList = new List<Task>();
	private List <FeedItem> feedItemsList = new List<FeedItem>();


		public override void prepareBefore() {

			if (Trigger.isInsert){

				for (EmailMessage mail : (List<EmailMessage>)Trigger.new){

						if(mail.Incoming && mail.ParentId != null){
							caseIds.add(mail.ParentId);
						}
				fetchRelatedCasesAndBackupPersons();
				}
			}
		}

		public override void prepareAfter() {
				// do any preparation here – bulk loading of data etc
		}

		public override void beforeInsert(sObject o) {

			EmailMessage newEmail = (EmailMessage)o;

			if(newEmail.Incoming){

				if (newEmail.ParentId != null){

					FeedItem OppFeed = new FeedItem();
					OppFeed.body = NEW_EMAIL_CHATTER_NOTIFICATION;
					oppFeed.parentID = newEmail.ParentId;
					oppFeed.title = CASE_NOTIFICATION_SUBJECT;
					feedItemsList.add(oppFeed);

					Case parentCase = casesMap.get(newEmail.ParentId);
					if (parentCase != null && parentCase.IsClosed){
						createTask(parentCase);
					}

				}
			}
		}

		public override void beforeUpdate(sObject old, sObject o) {


		}

		public override void beforeDelete(sObject o) {
				// Apply before delete logic to this sObject. DO NOT do any SOQL
				// or DML here – store records to be modified in an instance variable
				// which can be processed by the finish() method
		}

		public override void afterInsert(sObject o) {
				// Apply after insert logic to this sObject. DO NOT do any SOQL
				// or DML here – store records to be modified in an instance variable
				// which can be processed by the finish() method

		}

		public override void afterUpdate(sObject old, sObject o) {
				// Apply after update logic to this sObject. DO NOT do any SOQL
				// or DML here – store records to be modified in an instance variable
				// which can be processed by the finish() method
		}

		public override void afterDelete(sObject o) {
				// Apply after delete logic to this sObject. DO NOT do any SOQL
				// or DML here – store records to be modified in an instance variable
				// which can be processed by the finish() method
		}

		public override void afterUndelete(sObject o) {
				// Apply after undelete logic to this sObject. DO NOT do any SOQL
				// or DML here – store records to be modified in an instance variable
				// which can be processed by the finish() method
		}

		public override void finish() {
			if(!feedItemsList.isEmpty()){
				insert feedItemsList;
			}

			if(!tasksList.isEmpty()){
				insert tasksList;
			}

		}

		private void fetchRelatedCasesAndBackupPersons() {

		if(!this.caseIds.isEmpty()){
				this.caseList = [Select c.Subject, c.OwnerId, c.IsClosed, c.Id, c.CaseNumber, c.Status, c.Priority__c, c.ClosedDate, c.ContactId, c.AccountId From Case c where c.Id IN :caseIds];
				} else{
				return;
				}

				for (Case caseTemp : this.caseList){
				this.casesMap.put(caseTemp.Id, caseTemp);

				if(caseTemp.OwnerId != null){
					ownerIds.add(caseTemp.OwnerId);
				}
			}

			if(!ownerIds.isEmpty()){
				backupPersonsList = [Select b.Id, b.Current_assignee__c, b.BackUp_assignee__c From BackUp_Person__c b where b.Current_assignee__c IN : ownerIds ];
			}

				for (BackUp_Person__c backUp : backupPersonsList){
				backUpPersonsMap.put(backUp.Current_assignee__c, backUp);
			}
		}

		private void createTask (Case closedCase){
			/*	Id currentAssignee;
			Id backUpAssignee;
			Boolean backupExists = false;
			BackUp_Person__c backUp = backUpPersonsMap.get(closedCase.OwnerId);

			if (backUp != null){
				currentAssignee = backUp.Current_assignee__c;
				backUpAssignee = backUp.BackUp_assignee__c;
				backupExists = true;
			} else {
				currentAssignee = closedCase.OwnerId;
			}
			Database.DMLOptions dmlo = new Database.DMLOptions(); 
			dmlo.EmailHeader.triggerUserEmail = false; 
			dmlo.EmailHeader.triggerAutoResponseEmail = false;
			Task newTask = new Task ();
			newTask.Description = 'A closed Case received an Email message.';
			newTask.OwnerId = currentAssignee;
			newTask.Priority = closedCase.Priority__c;
			newTask.Status = 'In Progress';
			newTask.WhatId = closedCase.Id;
			newTask.WhoId = closedCase.ContactId;
			newTask.subject = 'Closed Case - Email arrived';
			newTask.IsReminderSet = true;
			newTask.ReminderDateTime = System.now();
			newTask.setOptions(dmlo);
			tasksList.add(newTask);

			if(backupExists){
				Task forBackup = newTask.clone(false, true, false, false);
				forBackup.OwnerId = backUpAssignee;
				forBackup.setOptions(dmlo);
				tasksList.add(forBackup);
			}
        */
		}
}