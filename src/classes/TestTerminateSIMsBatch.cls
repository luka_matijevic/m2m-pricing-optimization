@isTest (SeeAllData=false)
private class TestTerminateSIMsBatch {
    
    static testmethod void terminateSingleSIMTest(){
        List<Stock__c> sims = TestDataGeneratorForTerminateSIMProcess.createSIMs(1);
        TestDataGeneratorForTerminateSIMProcess.terminateSIMs(sims);
        Test.startTest();
            Database.executeBatch(new TerminateSIMsBatch());
        Test.stopTest();
        List<Stock__c> fetchedSIMs = [Select End_Date__c, Termination_Status__c, SM2M_Status__c from Stock__c];
        System.assert(fetchedSIMs != null);
        System.assert(fetchedSIMs.size() == 1);
        System.debug('End Date : ' + fetchedSIMs[0].End_Date__c);
        //System.assertEquals('PENDING', fetchedSIMs[0].Termination_Status__c);
        //System.assertEquals('SUSPENDED', fetchedSIMs[0].SM2M_Status__c);
    }
    
    static testmethod void terminateBulkSIMsTest(){
        List<Stock__c> sims = TestDataGeneratorForTerminateSIMProcess.createSIMs(200);
        TestDataGeneratorForTerminateSIMProcess.terminateSIMs(sims);
        Test.startTest();
            Database.executeBatch(new TerminateSIMsBatch());
        Test.stopTest();
        List<Stock__c> fetchedSIMs = [Select Termination_Status__c, SM2M_Status__c from Stock__c];
        System.assert(fetchedSIMs != null);
        System.assert(fetchedSIMs.size() == 200);
        //System.assertEquals('PENDING', fetchedSIMs[0].Termination_Status__c);
        //System.assertEquals('SUSPENDED', fetchedSIMs[0].SM2M_Status__c);
    }
}