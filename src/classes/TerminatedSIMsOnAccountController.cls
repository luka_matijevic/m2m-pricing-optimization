public class TerminatedSIMsOnAccountController {

    public ApexPages.StandardSetController sController;
    private String query;
    private Static final String limitClause = ' limit ' + (Limits.getLimitQueryRows()-Limits.getQueryRows());
    private Static final Integer pageSize = 100;
    private static final String TERMINATION_REQUESTED = 'REQUESTED';
    public List<Stock__c> sims = new List<Stock__c>();
    public Boolean showSrchData {get; set;}
    private String accountId;
    public Account acc {get; set;}
    public String searchFC {get; set;}
    public String csvFileData {get; set;}

    public TerminatedSIMsonAccountController(ApexPages.standardController stdCtrl){
        try{
            accountId = ApexPages.currentPage().getParameters().get('id');
            acc = [Select Id, Name from Account where Id = :accountId];
            putDataIntoSetCtrl();
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
    }
    
    private void putDataIntoSetCtrl(){
        try{
            if(!String.isEmpty(accountId)){
                query = 'Select Id,Name,ICCID__c,MSISDN__c,IMSI__c,Activation_Date__c,Runtime__c,End_date__c,Termination_Status__c,BIC_Framework_Contract_No__c, Account__c from stock__C where Account__c = \'' + accountId + '\' AND Termination_Status__c = \'' + TERMINATION_REQUESTED + '\'';
                sController = new ApexPages.StandardSetController(Database.getQueryLocator(query + limitClause));
                sController.setPageSize(pageSize);
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
    }

    public List<selectOption> getAvailableFCs(){
        List<selectOption> options = new List<selectOption>();
        try{
            options.add(new selectOption('','--None--'));
            if(!String.isEmpty(accountId)){
                for(Frame_Contract__c fc : [Select Id, Name from Frame_Contract__c where Customer__c = :accountId]){
                    options.add(new selectOption(fc.Name, fc.Name));
                }
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
        return options;
    }
    
    public List<Stock__c> getAllSims(){
        try{
            sims = new List<Stock__c>();
            if(sController != null){
                for(Stock__c stock : (List<Stock__c>)sController.getRecords()){
                    if(searchFC != null && searchFC != '' && searchFC.length() > 3){
                        if(stock.BIC_Framework_Contract_No__c != null && stock.BIC_Framework_Contract_No__c == searchFC){
                            sims.add(stock);
                        }
                    }
                    else{
                        sims.add(stock);
                    }
                }
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
        return sims;
    }
    
    //==================================================================================================================================================
    //Pagination login starts
    public Boolean hasNext {
        get {
            return sController.getHasNext();
        }
        set;
    }
    
    public Boolean hasPrevious {
        get {
            return sController.getHasPrevious();
        }
        set;
    }
    
    public Integer getPageNumber(){
        return sController.getPageNumber();
    }
    
    public Integer getTotalPages(){
        Decimal noPages = (sController.getResultSize() / sController.getPageSize());
        noPages = Math.floor(noPages) + ((Math.mod(sController.getResultSize(), sController.getPageSize()) > 0) ? 1 : 0);
        return (Integer.valueOf(noPages));
    }
    
    public void first() {
        sController.first();
    }
    
    public void last() {
        sController.last();
    }
    
    public void previous() {
        sController.previous();
    }
    
    public void next() {
        sController.next();
    }
    //Pagination login Ends
    //==================================================================================================================================================

    public Integer getNoOfSIMs(){
        String queryForCount = 'Select Count() from stock__C where Account__c = \'' + accountId + '\' AND Termination_Status__c = \'' + TERMINATION_REQUESTED + '\'';
        Integer i = 0;
        try{
            if(searchFC != null && searchFC != ''){
                queryForCount += ' AND BIC_Framework_Contract_No__c = \'' + searchFC + '\'';
            }
            i = Database.countQuery(queryForCount);
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
        return i;
    }
    
    public PageReference performSearchbyFC(){
        showSrchData = true;
        return null;
    }
    
    public PageReference backToAccount(){
        PageReference pageRef;
        try{
            if(!String.isEmpty(accountId)){
                pageRef = new PageReference('/' + accountId);
            }else{
                pageRef = null;
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
        return pageRef;
    }
    
    public PageReference exportToExcel(){
        PageReference pageRef;
        try{
            if(!sims.isEmpty()){
                prepareCSVFileData();
                pageRef = Page.SIMsTobeTerminatedOnAccountExcelPage;
                pageRef.setRedirect(false);
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, 'No Sims requested for Termination'));
                pageRef = null;
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
            pageRef = null;
        }
        return pageRef;
    }
    
    private void prepareCSVFileData(){
        String headerName = 'ICCID,MSISDN,IMSI,Activation Date,Runtime,Potential End Date';
        String rows;
        String querytoExport = query;
        try{
            if(searchFC != null && searchFC != ''){
                querytoExport += ' AND BIC_Framework_Contract_No__c = \'' + searchFC + '\'';
            }
            for(Stock__C sim : Database.query(querytoExport)){
                if(rows != null){
                    rows = rows + '\n';
                } 
                rows =  rows+ checkNullString(sim.ICCID__c)+','; //ICCID__c
                rows =  rows+ checkNullString(sim.MSISDN__c)+',';//
                rows =  rows+ checkNullString(sim.IMSI__c)+',';
                rows =  rows+ formatDateTime(sim.Activation_Date__c)+',';
                rows =  rows+ checkNullString(string.valueOf(sim.Runtime__c))+',';
                rows =  rows+ string.valueOf(sim.End_date__c);
            }
            if(rows == null){
                rows = '';
            }
            csvFileData = headerName + '\n' + rows;
            csvFileData = csvFileData.replace('null','');
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
    }
    
    private String checkNullString(String obj){
        if(obj == null){
            obj = '';
        }
        return obj;
    }
    
    private static string formatDateTime(DateTime dt){
         String reqDt = '';
         if(dt != null){
             reqDt = dt.format('dd.MM.yyyy HH:mm');
         }
         return reqDt;
    }
    
    public PageReference exportToPDF(){
        PageReference pageRef;
        try{
            if(!sims.isEmpty()){
                pageRef = Page.SIMsTobeTerminatedOnAccountPDFInterPage;
                pageRef.setRedirect(false);
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, 'No Sims requested for Termination'));
                pageRef = null;
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
            pageRef = null;
        }
        return pageRef;
    }
    
    public List<Stock__c> getAllTerminatedSIMs(){
        List<Stock__c> terminatedSims = new List<Stock__c>();
        String querytoExport = query;
        try{
            if(searchFC != null && searchFC != ''){
                querytoExport += ' AND BIC_Framework_Contract_No__c = \'' + searchFC + '\'';
            }
            terminatedSims = Database.query(querytoExport);
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
        return terminatedSims;
    }
    
    public PageReference stopTermination(){
        List<Stock__c> simsTobeStopped = new List<Stock__c>();
        try{
            if(!String.isEmpty(query)){
                for(Stock__c stock : Database.query(query)){
                    if(stock.Termination_Status__c == 'REQUESTED'){
                        stock.Termination_Status__c = '';
                        stock.Termination_Requested_Ticket__c = null;
                        simsTobeStopped.add(stock);
                    }
                }
                if(!simsTobeStopped.isEmpty()){
                    update simsTobeStopped;
                }
                putDataIntoSetCtrl();
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
        return null;
    }
}