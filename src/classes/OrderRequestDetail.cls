//------------------------------------------------------------------------------
// Class for retrieving details for order request
//------------------------------------------------------------------------------
public class OrderRequestDetail
{
	//--------------------------------------------------------------------------
	// Virtual filter
	//--------------------------------------------------------------------------
	public virtual class Filter
	{
		virtual Boolean filter(ProductOrder.LineItem lineItem)
		{
			return true;
		}
	}

	//--------------------------------------------------------------------------
	// One time filter
	//--------------------------------------------------------------------------
	public class FilterOneTime extends Filter
	{
		override Boolean filter(ProductOrder.LineItem lineItem)
		{
			return !lineItem.recurring;
		}
	}

	//--------------------------------------------------------------------------
	// Recurring filter
	//--------------------------------------------------------------------------
	public class FilterRecurring extends Filter
	{
		override Boolean filter(ProductOrder.LineItem lineItem)
		{
			return lineItem.recurring;
		}
	}

	//--------------------------------------------------------------------------
	// With price filter
	//--------------------------------------------------------------------------
	public class FilterWithPrice extends Filter
	{
		override Boolean filter(ProductOrder.LineItem lineItem)
		{
			return (lineItem.price != null && lineItem.price != 0);
		}
	}

	//--------------------------------------------------------------------------
	// Abstract filter configurations
	//--------------------------------------------------------------------------
	public virtual class FilterConfiguration
	{
		virtual Boolean filter(cscfga__Product_Configuration__c pc)
		{
			return true;
		}
	}

	//--------------------------------------------------------------------------
	// Filter parent configuraitons
	//--------------------------------------------------------------------------
	public class FilterParentConfiguration extends FilterConfiguration
	{
		override Boolean filter(cscfga__Product_Configuration__c pc)
		{
			return pc.parent_product_configuration__c == null;
		}
	}

	//--------------------------------------------------------------------------
	// Filter child configurations
	//--------------------------------------------------------------------------
	public class FilterChildConfiguration extends FilterConfiguration
	{
		override Boolean filter(cscfga__Product_Configuration__c pc)
		{
			return pc.parent_product_configuration__c != null;
		}
	}

    //--------------------------------------------------------------------------
    // Members
    //--------------------------------------------------------------------------
    List<ProductOrder> productOrders;

    //--------------------------------------------------------------------------
    // Private constructors
    //--------------------------------------------------------------------------
    private OrderRequestDetail() {}

    private OrderRequestDetail(List<ProductOrder> productOrders)
    {
    	this.productOrders = productOrders;
    }

    //--------------------------------------------------------------------------
    // Constructor
    //--------------------------------------------------------------------------
    public OrderRequestDetail(Id orderRequestId)
    {
    	//
    	// Retrieve order reqeust
    	//
    	Order_Request__c orderRequest =
    			[ SELECT Billing_Account__r.Payer__r.BillingCountry
    			  FROM Order_Request__c
    			  WHERE Id = :orderRequestId ];
        //
        // Retrieve product orders
        //
        Map<Id, Product_Order__c> productOrderMap = new Map<Id, Product_Order__c>(
                [ SELECT Id, Name, Quantity__c, Comment__c
                  FROM Product_Order__c
                  WHERE Order_Request__c = :orderRequestId ]);
        //
        // Retrieve product configuration orders
        //
        Product_Configuration_Order__c[] productConfigurationOrders =
                [ SELECT Id, Product_Order__c,
                  Product_Configuration__c, Quantity__c
                  FROM Product_Configuration_Order__c
                  WHERE Product_Order__c IN :productOrderMap.keySet() ];
        //
        // Create map of product confgiuration orders regarding product configuration id
        //
        Map<Id, Product_Configuration_Order__c> productConfigurationOrderMap =
                new Map<Id, Product_Configuration_Order__c>();
        for (Product_Configuration_Order__c pco : productConfigurationOrders)
        {
            productConfigurationOrderMap.put(pco.Product_Configuration__c, pco);
        }

        //
        // Retrieve product configuration names
        //
        cscfga__Product_Configuration__c[] configs =
                [ SELECT Id, Name, Original_Product_Name__c,
                  cscfga__Contract_Term__c, parent_product_configuration__c,
                  Billing_Rate_Model__c,
                  ( SELECT Id, Name, cscfga__Is_Line_Item__c, cscfga__Recurring__c,
                    cscfga__Line_Item_Description__c, cscfga__Value__c, cscfga__Price__c
                    FROM cscfga__Attributes__r
                    WHERE cscfga__Is_Line_Item__c = true
                    OR Name = :Constants.ATTRIBUTE_NAME_QUANTITY
                    OR Name = :Constants.ATTRIBUTE_NAME_ARTICLE_INFO_SET
					// ORDER BY cscfga__Is_Line_Item__c ASC
					ORDER BY cscfga__Line_Item_Sequence__c ASC)
                  FROM cscfga__Product_Configuration__c
                  WHERE Id IN :productConfigurationOrderMap.keySet() ];

        //
        // Put quantities to products
        //
        Map<Id, Integer> configurationQuantities = new Map<Id, Integer>();
        Map<Id, Id> configurationArticleInfoSets = new Map<Id, Id>();
        for (cscfga__Product_Configuration__c config : configs)
        {
            System.debug('Configuration: ' + config);
            for (cscfga__Attribute__c att : config.cscfga__Attributes__r)
            {
                System.debug('Attribute: ' + att);
				//
				// Quantity
				//
                if (att.Name == Constants.ATTRIBUTE_NAME_QUANTITY)
                {
                    configurationQuantities.put(config.Id, Integer.valueOf(att.cscfga__Value__c));
                }
				//
				// Article Info Set
				//
                else if (att.Name == Constants.ATTRIBUTE_NAME_ARTICLE_INFO_SET)
                {
                    try
                    {
                        System.debug('Putting article info set: ' + att);
                        configurationArticleInfoSets.put(config.Id, att.cscfga__Value__c);
                    }
                    catch (Exception e)
                    {
                        System.debug('Wrong Id in attribute: ' + att.cscfga__Value__c);
                        throw e;
                    }
                }
            }
        }
        //
        // Get journal mappings
        //
        Map<Id, Article_Info_Set__c> articleInfoSets = new Map<Id, Article_Info_Set__c>(
                [ SELECT Id, One_Off_Journal_Mapping__c,
                  Journal_Mapping__c, RecordType.Name
                  FROM Article_Info_Set__c
                  WHERE Id IN :configurationArticleInfoSets.values() ]);
        Set<String> journalMappingNames = new Set<String>();
        for (Article_Info_Set__c ais : articleInfoSets.values())
        {
            journalMappingNames.add(ais.Journal_Mapping__c);
            journalMappingNames.add(ais.One_Off_Journal_Mapping__c);
        }

        String country = getCountry(orderRequest.Billing_Account__r.Payer__r.BillingCountry);

        Journal_Mapping__c[] journalMappings =
                [ SELECT Id, Name
                  FROM Journal_Mapping__c
                  WHERE Name IN :journalMappingNames
                  AND Country__c = :country ];
        Map<String, Id> journalMappingMap = new Map<String, Id>();
        for (Journal_Mapping__c jm : journalMappings)
        {
            journalMappingMap.put(jm.Name, jm.Id);
        }

		//
		// Map parent configurations
		//
        cscfga__Product_Configuration__c[] parentConfigs =
        		filterConfigurations(configs, new FilterParentConfiguration());
    	productOrders =  mapConfigurations
    			( parentConfigs
    			, productConfigurationOrderMap
    			, configurationQuantities
    			, configurationArticleInfoSets
    			, articleInfoSets
    			, journalMappingMap
    			, productOrderMap );
    	//
    	// Map child configuraitons and connect them to parents
    	//
    	cscfga__Product_Configuration__c[] childConfigs =
    			filterConfigurations(configs, new FilterChildConfiguration());
    	List<cscfga__Product_Configuration__c[]> groupedConfigs =
    			groupConfigurations(childConfigs);

    	for (cscfga__Product_Configuration__c[] pcs : groupedConfigs)
    	{
            System.debug('Configurations: ' + pcs);
    		ProductOrder[] pos = mapConfigurations
        			( pcs
        	    	, productConfigurationOrderMap
        	    	, configurationQuantities
        	    	, configurationArticleInfoSets
        	    	, articleInfoSets
        	    	, journalMappingMap
        	    	, productOrderMap );
    		attachToParent(productOrders, pcs[0].parent_product_configuration__c, pos);
    	}
    }

    //--------------------------------------------------------------------------
    // Retrieve product orders
    //--------------------------------------------------------------------------
    public List<ProductOrder> getProductOrders()
    {
        return productOrders;
    }

    //--------------------------------------------------------------------------
    // Filter one time line items
    //--------------------------------------------------------------------------
    public OrderRequestDetail filterOneTime()
    {
        return new OrderRequestDetail(filter(productOrders, new FilterOneTime()));
    }

    //--------------------------------------------------------------------------
    // Filter recurring line items
    //--------------------------------------------------------------------------
    public OrderRequestDetail filterRecurring()
    {
        return new OrderRequestDetail(filter(productOrders, new FilterRecurring()));
    }

    //--------------------------------------------------------------------------
    // Filter line items with price
    //--------------------------------------------------------------------------
    public OrderRequestDetail filterWithPrice()
    {
    	return new OrderRequestDetail(filter(productOrders, new FilterWithPrice()));
    }

    //--------------------------------------------------------------------------
    // Joins child line items to parent configuration
    //--------------------------------------------------------------------------
	public OrderRequestDetail join()
	{
		return new OrderRequestDetail(join(productOrders));
	}

    //--------------------------------------------------------------------------
    // Returns DE or Non-DE depending on country name
    //--------------------------------------------------------------------------
    private static String getCountry(String country)
    {
        System.debug('Country: ' + country);
        if (country == 'Germany'
            || country == 'Deutschland')
        {
            return 'DE';
        }
        else
        {
            return 'Non-DE';
        }
    }

    //--------------------------------------------------------------------------
    // Create product order
    //--------------------------------------------------------------------------
    private static ProductOrder createProductOrder
    		( Product_Order__c po
    		, ProductOrder.ProductConfiguration pc )
    {
        ProductOrder returnValue = new ProductOrder();
        returnValue.name = po.Name;
        returnValue.quantity = po.Quantity__c;
        returnValue.comment = po.Comment__c;
        returnValue.productConfigurations = new List<ProductOrder.ProductConfiguration>();
        returnValue.productConfigurations.add(pc);
        return returnValue;
    }

	//--------------------------------------------------------------------------
	// Filter line items
	//--------------------------------------------------------------------------
	private static ProductOrder[] filter(List<ProductOrder> productOrders, Filter filter)
	{
		ProductOrder[] returnValue = new List<ProductOrder>();
		for (ProductOrder po : productOrders)
		{
			ProductOrder.ProductConfiguration[] productConfigurations =
				new List<ProductOrder.ProductConfiguration>();
			for (ProductOrder.ProductConfiguration pc : po.productConfigurations)
			{
				//
				// Create line items
				//
				ProductOrder.LineItem[] lineItems = filter(pc.lineItems, filter);
				//
				// Create child product orders using recursion
				//
				ProductOrder[] childProductOrders = filter(pc.productOrders, filter);

				if (!lineItems.isEmpty()
					|| !childProductOrders.isEmpty())
				{
					//
					// Create new product configuration with filtered line items
					//
					ProductOrder.ProductConfiguration tempPc =
						new ProductOrder.ProductConfiguration(pc);
					tempPc.lineItems = lineItems;
					tempPc.productOrders = childProductOrders;

					productConfigurations.add(tempPc);
				}
			}

			if (!productConfigurations.isEmpty())
			{
				//
				// Create new product order with filtered line items
				//
				ProductOrder productOrder = new ProductOrder(po);
				productOrder.productConfigurations = productConfigurations;

				returnValue.add(productOrder);
			}
		}
		return returnValue;
	}

	//--------------------------------------------------------------------------
	// Filter line items dependent if it is recurring
	//--------------------------------------------------------------------------
	public static ProductOrder.LineItem[] filter(ProductOrder.LineItem[] lineItems, Filter filter)
	{
		ProductOrder.LineItem[] returnValue = new List<ProductOrder.LineItem>();
		for (ProductOrder.LineItem li : lineItems)
		{
			if (filter.filter(li))
			{
				returnValue.add(new ProductOrder.LineItem(li));
			}
		}
		return returnValue;
	}

	//-------------------------------------------------------------------------
	// Joins child line items to parent configuration
	//-------------------------------------------------------------------------
	public static ProductOrder[] join(ProductOrder[] productOrders)
	{
		ProductOrder[] returnValue = new List<ProductOrder>();
		for (ProductOrder po : productOrders)
		{
			ProductOrder.ProductConfiguration[] productConfigurations =
				new List<ProductOrder.ProductConfiguration>();
			for (ProductOrder.ProductConfiguration pc : po.productConfigurations)
			{
				//
				// Create new product configuration with joined elements
				//
				ProductOrder.ProductConfiguration tempPc =
					new ProductOrder.ProductConfiguration(pc);
				tempPc.lineItems = filter(pc.lineItems, new Filter());
				//
				// Add childLineItems
				//
				tempPc.lineItems.addAll(joinChildLineItems(pc.productOrders));
				//
				// Remove child product orders
				//
				tempPc.productOrders = new List<ProductOrder>();

				productConfigurations.add(tempPc);
			}
			//
			// Create new product order
			//
			ProductOrder ProductOrder = new ProductOrder(po);
			productOrder.productConfigurations = productConfigurations;

			returnValue.add(productOrder);
		}
		return returnValue;
	}

	//--------------------------------------------------------------------------
	// Joins all child line items
	//--------------------------------------------------------------------------
	private static ProductOrder.LineItem[] joinChildLineItems(
			ProductOrder[] productOrders)
	{
		ProductOrder.LineItem[] returnValue = new List<ProductOrder.LineItem>();
		for (ProductOrder po : productOrders)
		{
			for (ProductOrder.ProductConfiguration pc : po.productConfigurations)
			{
				for (ProductOrder.LineItem li : pc.lineItems)
				{
					returnValue.add(new ProductOrder.LineItem(li));
				}
				returnValue.addAll(joinChildLineItems(pc.productOrders));
			}
		}
		return returnValue;
	}

	//--------------------------------------------------------------------------
	// Check if it is SM2M line item
	//--------------------------------------------------------------------------
	private static boolean isSM2M(cscfga__Product_Configuration__c config, cscfga__Attribute__c att)
	{
		return ((config.Original_Product_Name__c == Constants.PRODUCT_TYPE_CONNECTIVITY
				 && att.Name != 'SIM One-Time Target Fee Total')
				|| config.parent_product_configuration__c != null);
	}

	//--------------------------------------------------------------------------
	// Filter configurations
	//--------------------------------------------------------------------------
	private static cscfga__Product_Configuration__c[] filterConfigurations(
			cscfga__Product_Configuration__c[] configs,
			FilterConfiguration filter)
	{
		List<cscfga__Product_Configuration__c> returnValue =
			new List<cscfga__Product_Configuration__c>();
		for (cscfga__Product_Configuration__c config : configs)
		{
			if (Filter.filter(config))
			{
				returnValue.add(config);
			}
		}
		return returnValue;
	}

	//--------------------------------------------------------------------------
	// Map configurations to list of product orders
	//--------------------------------------------------------------------------
	private static ProductOrder[] mapConfigurations
			( cscfga__Product_Configuration__c[] configs
			, Map<Id, Product_Configuration_Order__c> productConfigurationOrderMap
			, Map<Id, Integer> configurationQuantities
			, Map<Id, Id> configurationArticleInfoSets
			, Map<Id, Article_Info_Set__c> articleInfoSets
			, Map<String, Id> journalMappingMap
			, Map<Id, Product_Order__c> productOrderMap	)
	{
        ProductOrder[] productOrders = new List<ProductOrder>();
        ProductOrder productOrder = null;

        for (cscfga__Product_Configuration__c config : configs)
        {
            List<ProductOrder.LineItem> lineItems = new List<ProductOrder.LineItem>();
            for (cscfga__Attribute__c att : config.cscfga__Attributes__r)
            {
                if (att.cscfga__Is_Line_Item__c == true
                    && att.cscfga__Price__c != null
                    && att.cscfga__Price__c != 0)
                {
                    //
                    // Calculate price
                    //
                    Decimal price = att.cscfga__Price__c;
                    price *= productConfigurationOrderMap.get(config.Id).Quantity__c;
                    price /= configurationQuantities.get(config.Id);
                    ProductOrder.LineItem lineItem = new ProductOrder.LineItem();
                    lineItem.name = att.Name;
                    lineItem.description = att.cscfga__Line_Item_Description__c;
                    lineItem.recurring = att.cscfga__Recurring__c;
                    lineItem.sm2m = isSM2M(config, att);
                    lineItem.price = price;
                    lineItems.add(lineItem);
                }
            }
            //
            // Craete product confguration
            //
            ProductOrder.ProductConfiguration pc = new ProductOrder.ProductConfiguration();
            pc.name = config.Name;
            pc.originalProductName = config.Original_Product_Name__c;
            pc.contractTerm = config.cscfga__Contract_Term__c;
            String billingRateModel = config.Billing_Rate_Model__c;
            pc.billingRateModel =
                billingRateModel != null ? billingRateModel : Constants.ONE_OFF;
            pc.productConfigurationId = config.Id;
            pc.productConfigurationOrderId = productConfigurationOrderMap.get(config.Id).Id;
            //
            // Journal mapping
            //
            Id articleInfoSet = configurationArticleInfoSets.get(config.Id);
            Id oneTimeJournalMappingId = null;
            Id recurringJournalMappingId = null;
			String aisRecordType = null;
            if (articleInfoSet != null)
            {
                Article_Info_Set__c ais = articleInfoSets.get(articleInfoSet);
                String oneTimeJournalMappingName = ais.One_Off_Journal_Mapping__c;
                String recurringJournalMappingName = ais.Journal_Mapping__c;
				aisRecordType = ais.RecordType.Name;

                if (oneTimeJournalMappingName != null)
                {
                    oneTimeJournalMappingId =
                        journalMappingMap.get(oneTimeJournalMappingName);
                }

                if (recurringJournalMappingName != null)
                {
                    recurringJournalMappingId =
                        journalMappingMap.get(recurringJournalMappingName);
                }
            }
            pc.oneTimeJournalMappingId = oneTimeJournalMappingId;
            pc.recurringJournalMappingId = recurringJournalMappingId;
			pc.aisRecordType = aisRecordType;
            pc.lineItems = lineItems;
            pc.productOrders = new List<ProductOrder>();
            //
            // Create product order if it doesn't exist
            //
            Id productOrderId =
                productConfigurationOrderMap.get(config.Id).Product_Order__c;
            Product_Order__c po = productOrderMap.get(productOrderId);
            if (productOrder == null)
            {
                //
                // Create new product order
                //
                productOrder = createProductOrder(po, pc);
            }
            else if (productOrder.name != po.Name)
            {
                //
                // Add current product order and create new
                //
                productOrders.add(productOrder);
                productOrder = createProductOrder(po, pc);
            }
            else
            {
                //
                // Add product configuration to product order
                //
                productOrder.productConfigurations.add(pc);
            }
        }
    	//
    	// Add finaly last product order
    	//
        if (productOrder != null)
        {
        	productOrders.add(productOrder);
        }

        return productOrders;
	}

	//--------------------------------------------------------------------------
	// Group configurations by parent configuration id
	//--------------------------------------------------------------------------
	private static List<cscfga__Product_Configuration__c []> groupConfigurations(
			cscfga__Product_Configuration__c[] configs)
	{
		System.debug('Configurations: ' + configs);
		Map<Id, List<cscfga__Product_Configuration__c>> groupedMap =
				new Map<Id, List<cscfga__Product_Configuration__c>>();
		for (cscfga__Product_Configuration__c config : configs)
		{
			attachConfigToMap(groupedMap, config);
		}
		return groupedMap.values();
	}

	//--------------------------------------------------------------------------
	// Attach configuration to map
	//--------------------------------------------------------------------------
	private static void attachConfigToMap(
			Map<Id, List<cscfga__Product_Configuration__c>> groupedMap,
			cscfga__Product_Configuration__c config)
	{
		//
		// Check if we have that parent id
		//
		cscfga__Product_Configuration__c[] configs = groupedMap.get(config.parent_product_configuration__c);
		//
		// If configuration list exist add current configuration
		// else create new list and put it to map
		//
		if (configs != null)
		{
			configs.add(config);
		}
		else
		{
			configs = new List<cscfga__Product_Configuration__c>();
			configs.add(config);
			groupedMap.put(config.parent_product_configuration__c, configs);
		}
	}

	//--------------------------------------------------------------------------
	// Attach to child product orders to parent product order
	//--------------------------------------------------------------------------
	private static void attachToParent(
			ProductOrder[] productOrders,
			Id configId,
			ProductOrder[] childProductOrders)
	{
        System.debug('Product orders: ' + productOrders);
        System.debug('ConfigId: ' + configId);
        System.debug('Child product orders: ' + childProductOrders);
		ProductOrder.ProductConfiguration config =
				findConfiguration
					( productOrders
					, configId);
		if (config != null)
		{
            System.debug('Attached to parent');
			config.productOrders = childProductOrders;
		}
	}

	//--------------------------------------------------------------------------
	// Find configuration in product orders
	//--------------------------------------------------------------------------
	private static ProductOrder.ProductConfiguration findConfiguration(
			ProductOrder[] productOrders,
			Id configId)
	{
		for (ProductOrder po : productOrders)
		{
			for (ProductOrder.ProductConfiguration pc : po.productConfigurations)
			{
				if (pc.productConfigurationId == configId)
				{
					return pc;
				}
			}
		}
		return null;
	}
}