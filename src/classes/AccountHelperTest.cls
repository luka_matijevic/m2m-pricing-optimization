@isTest
private class AccountHelperTest {

    static testMethod void myUnitTest() {
    	    	
        Account acc = Test_Util.createAccount('Test');
        acc.BillingCountry = 'Germany'; 
        acc.BillingStreet = 'Street';
        acc.BillingCity = 'City';
        acc.BillingPostalCode = 'Postal code';
        acc.Global__c = true;
        
     	insert acc;

        Contact con = Test_Util.createContact(acc.id);
        
        con.Country__c = 'Germany';
        insert con;
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1)
			, recordtypeid = Cache.getRecordTypeId('Opportunity.Commercial_Global')
            , accountId = acc.id,
            Description = 'Test description 20 characters'
         
		);
		insert opp;		
		
		
        Frame_Contract__c fc = new Frame_Contract__c();
        fc.contact__c = con.id;
        fc.customer__c = acc.id;
        fc.opportunity__c = opp.id;
        insert fc;
        Test.startTest();

        AccountHelper.PopulateAccountSM2MInformation(acc);
        AccountHelper.CreateBillingAccount(opp.Id);
        AccountHelper.CreateBillingAccount(fc);
        //Assertions
        //account has address
        Billing_Account__c ba = [select id from Billing_Account__c where payer__c = :acc.id limit 1];
     
        Test.stopTest();
     
    }
    
    static testMethod void myUnitTest2() {
        Account acc2 = Test_Util.createAccount('Test2');
        acc2.BillingCountry = 'Switzerland'; 
        acc2.BillingStreet = 'Street';
        acc2.BillingCity = 'City';
        acc2.Language__c = 'German';
        acc2.VAT__c = 20;
        acc2.BillingPostalCode = 'Postal code';
        acc2.Global__c = true;
        
     	insert acc2;
        
        //Billing_Account__c bac = new Billing_Account__c(Payer__c = acc2.Id, Name = 'Test Billing Acc', SM2M_Type__c = 'Global', VAT__c = '19');
        //insert bac;

        Contact con = Test_Util.createContact(acc2.id);
        
        con.Country__c = 'Germany';
        insert con;
        
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1)
			, recordtypeid = Cache.getRecordTypeId('Opportunity.Commercial_Global')
            , accountId = acc2.id,
            Description = 'Test description 20 characters'
         
		);
		insert opp;		
		
		
        Frame_Contract__c fc = new Frame_Contract__c();
        fc.contact__c = con.id;
        fc.customer__c = acc2.id;
        fc.opportunity__c = opp.id;
        insert fc;
        Test.startTest();

        AccountHelper.PopulateAccountSM2MInformation(acc2);
        AccountHelper.CreateBillingAccount(fc);
        AccountHelper.CreateBillingAccount(opp.Id);
     
        Test.stopTest();
     
    }
}