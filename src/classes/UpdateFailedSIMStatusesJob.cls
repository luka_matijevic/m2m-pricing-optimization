global class UpdateFailedSIMStatusesJob implements Database.Batchable<SObject> {
	//---------------------------------------------------------------------------
	// Start of batch apex
	//---------------------------------------------------------------------------
	global List<SObject> start(Database.BatchableContext bc)
	{		
		String result = 'There is nothing to update.';
        Set<Id> accountIds = new Set<Id>();
        Set<Id> accountProcessedIds = new Set<Id>();
        List<Account> finalAccounts = new List<Account>();
        
        Set<Id> outgoingMessagesIds = new Set<Id>();
        Set<Id> outgoingMessagesProcessedIds = new Set<Id>();
        
        List<csam_t1__Outgoing_Message__c> outgoingMessagesStuckList = [SELECT Id, csam_t1__Status__c FROM csam_t1__Outgoing_Message__c WHERE 
        				csam_t1__ObjectGraph_Callout_Handler__r.Name = 'Update SIM Statuses' AND csam_t1__Status__c in ('Waiting for Response', 'Pending')];
								
		if(outgoingMessagesStuckList != null && outgoingMessagesStuckList.size() > 0) {
			List<csam_t1__Outgoing_Message_Record__c> stuckMessagesRecordsList = [SELECT Id, csam_t1__Object_Record_Id__c, csam_t1__Outgoing_Message__c FROM csam_t1__Outgoing_Message_Record__c WHERE CreatedDate = TODAY AND 
				csam_t1__Outgoing_Message__r.csam_t1__ObjectGraph_Callout_Handler__r.Name = 'Update SIM Statuses' AND csam_t1__Outgoing_Message__c in :outgoingMessagesStuckList];
	        
	        for(csam_t1__Outgoing_Message__c om : outgoingMessagesStuckList) {
	        	om.csam_t1__Status__c = 'Unrecoverable Error';
	        }
	        
	        update outgoingMessagesStuckList;
	        
	        for(csam_t1__Outgoing_Message_Record__c omr : stuckMessagesRecordsList) {
	        	accountIds.add(omr.csam_t1__Object_Record_Id__c);
	        }
		}
          
        List<csam_t1__Incoming_Message__c> incomingMessagesList = [SELECT Id, Name, csam_t1__Outgoing_Message__c, csam_t1__Status__c FROM csam_t1__Incoming_Message__c where 
                                                                    csam_t1__Status__c = 'Integration Error' AND CreatedDate = TODAY AND csam_t1__Outgoing_Message__r.csam_t1__ObjectGraph_Callout_Handler__r.Name = 'Update SIM Statuses'];
                                                                    
        List<csam_t1__Incoming_Message__c> incomingMessagesProcessedList = [SELECT Id, csam_t1__Outgoing_Message__c, csam_t1__Status__c FROM csam_t1__Incoming_Message__c where csam_t1__Status__c = 'Processed' AND CreatedDate = TODAY AND 
        															csam_t1__Outgoing_Message__r.csam_t1__ObjectGraph_Callout_Handler__r.Name = 'Update SIM Statuses'];
        
        if(incomingMessagesProcessedList != null && incomingMessagesProcessedList.size() > 0) {
        	for(csam_t1__Incoming_Message__c inc : incomingMessagesProcessedList) {
                outgoingMessagesProcessedIds.add(inc.csam_t1__Outgoing_Message__c);
            }
            
            if(outgoingMessagesProcessedIds.size() > 0) {
                List<csam_t1__Outgoing_Message_Record__c> outgoingMessagesRecordsProcessedList = [SELECT Id, csam_t1__Object_Record_Id__c, csam_t1__Outgoing_Message__c FROM csam_t1__Outgoing_Message_Record__c WHERE csam_t1__Outgoing_Message__c in :outgoingMessagesProcessedIds];
                
                if(outgoingMessagesRecordsProcessedList != null && outgoingMessagesRecordsProcessedList.size() > 0) {
                    for(csam_t1__Outgoing_Message_Record__c msgRec : outgoingMessagesRecordsProcessedList) {
                    	accountProcessedIds.add(msgRec.csam_t1__Object_Record_Id__c);
                    }
                }
            }
        }
        
        if(incomingMessagesList != null && incomingMessagesList.size() > 0) {
        	
            for(csam_t1__Incoming_Message__c inc : incomingMessagesList) {
                outgoingMessagesIds.add(inc.csam_t1__Outgoing_Message__c);
            }
            
            if(outgoingMessagesIds.size() > 0) {
                List<csam_t1__Outgoing_Message_Record__c> outgoingMessagesRecordsList = [SELECT Id, csam_t1__Object_Record_Id__c, csam_t1__Outgoing_Message__c FROM csam_t1__Outgoing_Message_Record__c WHERE csam_t1__Outgoing_Message__c in :outgoingMessagesIds];
                
                if(outgoingMessagesRecordsList != null && outgoingMessagesRecordsList.size() > 0) {
                    for(csam_t1__Outgoing_Message_Record__c msgRec : outgoingMessagesRecordsList) {
                    	accountIds.add(msgRec.csam_t1__Object_Record_Id__c);
                    }
                    
					if(accountProcessedIds.size() > 0) {
						accountIds.removeAll(accountProcessedIds);
					}
                    
                    if(accountIds.size() > 0) {
                    	finalAccounts = [SELECT Id FROM Account WHERE Id in :accountIds];
                    	//result = 'Update of SIM statuses is successfully started!';
                    }
                }
            }
        }
        
        if(finalAccounts == null)
        	finalAccounts = new List<Account>();
        	
        return finalAccounts;
	}

	//--------------------------------------------------------------------------
	// Execute of batch apex
	//--------------------------------------------------------------------------
	global void execute(Database.BatchableContext bc, List<SObject> scope)
	{
		Set<Id> accIds = new Set<Id>();
		for (Account account : (List<Account>)scope)
		{
			accIds.add(account.Id);
		}
		csam_t1.ObjectGraphCalloutHandler.queueMultipleMessages('Update SIM Statuses', new List<Id>(accIds));
	}

	//--------------------------------------------------------------------------
	// Finish of batch apex
	//--------------------------------------------------------------------------
	global void finish(Database.BatchableContext bc)
	{
		
	}
}