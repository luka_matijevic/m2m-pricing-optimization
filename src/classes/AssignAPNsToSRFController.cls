public with sharing class AssignAPNsToSRFController {

	private ApexPages.StandardSetController stdSetController;
	
	public SRF__c SRF { get; set; }								// Current SRF object to associate APNs with
	public List<SrfApnRow> srfApnRowItems { get; set; }			// Wrapper object list for presentation
	private Map<Id, APN_SRF__c> currentSrfApns { get; set; }	// APN.Id-based map of currently selected APNs for the current TUF
	
	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	public AssignAPNsToSRFController(ApexPages.StandardSetController controller)
	{
		this.stdSetController = controller;
		this.stdSetController.setPageSize(25);
		
		string recordId = ApexPages.currentPage().getParameters().get('id');
		SRF = [SELECT Id, Name, Account__c FROM SRF__c WHERE Id = :recordId];
		
		// Instantiate collections
		srfApnRowItems = new List<SrfApnRow>();
		currentSrfApns = new Map<Id, APN_SRF__c>();
		
		
		// Saved APNs for the current SRF
		Set<APN_SRF__c> savedSrfApns = new Set<APN_SRF__c>([SELECT Id, Name, APN__c FROM APN_SRF__c WHERE SRF__c = :recordId]);
		for(APN_SRF__c apnsrf : savedSrfApns) {
			currentSrfApns.put(apnsrf.APN__c, apnsrf);
		}
		
		
		// Available APNs (Default and associated with Account)
		List<APN__c> availableAPNs = [SELECT Id, Name, Account__c, Domain__c, Filter_Type__c, IP_Mask__c, Port_or_Port_Range__c, Protocol_Number__c, Is_Default__c 
										FROM APN__c 
										WHERE Is_Default__c = true OR Account__c = :SRF.Account__c];
		for(APN__c apn : availableAPNs) {
			boolean isSaved = currentSrfApns.containsKey(apn.Id);
			srfApnRowItems.add(new SrfApnRow(apn, SRF, isSaved, isSaved));
		}
	}
	
	public PageReference confirm()
	{
		List<APN_SRF__c> unselectedSrfApns = new List<APN_SRF__c>(); 
		for(SrfApnRow apnRowItem : srfApnRowItems) {
			
			// Skip if association with SRF and current apnRowItem already exists
			if(currentSrfApns.containsKey(apnRowItem.apn.Id) && apnRowItem.IsChecked)
				continue;
				
			// If current row item exists in the map of associated APNs and has been unchecked 
			if(currentSrfApns.containsKey(apnRowItem.apn.Id) && !apnRowItem.IsChecked) {
				// Add item to collection for deletion, only if it was previously saved
				// ...otherwise just remove it from the map of associated APNs
				if(apnRowItem.IsSaved) 
					unselectedSrfApns.add(currentSrfApns.remove(apnRowItem.apn.Id));
				else
					currentSrfApns.remove(apnRowItem.apn.Id);
			}
			
			if(!apnRowItem.IsChecked)
				continue;
				
			APN_SRF__c newSrfApn = new APN_SRF__c(Name=apnRowItem.apn.Name, APN__c=apnRowItem.apn.Id, SRF__c=apnRowItem.srf.Id);
			currentSrfApns.put(apnRowItem.apn.Id, newSrfApn);
		}
		
		// If selected items size is larger than 10, inform user that only up to 10 items can be selected
		if(currentSrfApns.values().size() > 10) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Only up to 10 APNs can be selected per SRF!'));
			return null;
		}
		
		// Delete unassociated APN-SRF associations
		if(unselectedSrfApns.size() > 0)
			delete unselectedSrfApns;
		
		// Update/Insert fresh APN-SRF associations
		if(currentSrfApns.values().size() > 0)
			upsert currentSrfApns.values();
		
		PageReference pageRef = new PageReference('/' + SRF.Id);
		return pageRef;
	}
	
	public PageReference cancel()
	{
		PageReference pageRef = new PageReference('/' + SRF.Id);
		return pageRef;
	}
	
	public class SrfApnRow {
		public APN__c apn { get; set; }
		public SRF__c srf { get; set; }
		public APN_SRF__c apnsrf { get; set; }
		public boolean IsChecked { get; set; }
		public boolean IsSaved { get; set; }
		
		public SrfApnRow(APN__c apn, SRF__c srf, boolean isChecked, boolean isSaved) {
			this.apn = apn;
			this.srf = srf;
			this.IsChecked = isChecked;
			this.IsSaved = isSaved;
		}
	}
}