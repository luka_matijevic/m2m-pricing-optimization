public class DailyBillFileUploadController {
    
     public DailyBillFileUploadController(ApexPages.StandardController controller) {
		// initialization is done through visual force page action

	}
    
    public void uploadDailyBill() {
        
        String dailybillId = ApexPages.currentPage().getParameters().get('id');
        
        List<Attachment> alist = [SELECT Id, parentid  FROM Attachment WHERE ParentId = :dailybillId and (not name like '%RATE_PAYMENT%') and (not name like 'Sixt%') ORDER BY createddate DESC LIMIT 1]; 
        Attachment attachment = new Attachment();
        if (!alist.isEmpty()){
            attachment = alist[0];
            } 
            
        csam_t1.ObjectGraphCalloutHandler.AdditionalObjects addObjects = new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects();
        addObjects.typeName = 'Attachment';
        addObjects.ids = new Id[]{attachment.id};
    
        csam_t1.ObjectGraphCalloutHandler.createAndSendExtended('Upload Daily Bill', attachment.ParentId, new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects[]{addObjects});
        boolean createTicketReports = true;
        /*Luka - separate button for Bills*/
		//CustomerMonthlyBillService.createBills(dailybillId, false, '', createTicketReports); 
		}	
}