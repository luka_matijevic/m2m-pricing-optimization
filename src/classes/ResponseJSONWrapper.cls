public class ResponseJSONWrapper {

	public class AnnotationsWrapper {
		public String operator { get; set; }
		public String value { get; set; }
	}
	
	
	public class AttachmentWrapper {
		public String name { get; set; }
		public String uri { get; set; }
		public String date1 { get; set; }
	}

	public String status;
	public Integer severity;
	public String description;
	public String subject;
	public List<AnnotationsWrapper> annotations;
	public List<AttachmentWrapper> attachments;
}