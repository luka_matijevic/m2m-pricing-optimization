@isTest (SeeAllData=false)
private class TestTaskForSIMsTerminationBatch {
    
    static testmethod void taskForSingleSIMTest(){
        List<Stock__c> sims = TestDataGeneratorForTerminateSIMProcess.createSIMsforTaskCreation(1);
        System.debug('Inserted SIMs : ' + sims);
        Test.startTest();
            Database.executeBatch(new TaskForSIMsTerminationBatch());
        Test.stopTest();
        List<Task> fetchedTasks = [Select Id from Task];
        System.assert(fetchedTasks != null, 'Task List is null');
        //System.assert(fetchedTasks.size() > 0, 'No Tasks created');
    }
    
    /*static testmethod void terminateBulkSIMstest(){
        List<Stock__c> sims = TestDataGeneratorForTerminateSIMProcess.createSIMs(200);
        sims = TestDataGeneratorForTerminateSIMProcess.updateSIMsforTaskCreation(sims);
        TestDataGeneratorForTerminateSIMProcess.terminateSIMs(sims);
        Test.startTest();
            Database.executeBatch(new TaskForSIMsTerminationBatch());
        Test.stopTest();
        List<Task> fetchedTasks = [Select Id from Task];
        System.assert(fetchedTasks != null, 'Task List is null');
        System.assert(fetchedTasks.size() > 0, 'No Tasks created');
    }*/
}