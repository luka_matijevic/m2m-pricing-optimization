public with sharing class ProductConfigurationService {
	
	public ProductConfigurationService() {
		
	}
	
	public Decimal productConfigurationPrice(cscfga__Product_Configuration__c prodConfig, List<cscfga__Attribute__c> priceAttributes) {
	
		Decimal price = 0.0;
		
		for (cscfga__Attribute__c priceAttr : priceAttributes) {
			if (belongsTo(priceAttr, prodConfig)) {
				if (priceAttr.cscfga__Price__c != null) {
					price += priceAttr.cscfga__Price__c;
				}
			}
		}
		return price;	
	}
	
	private static Set<String> productNames = new Set<String>{
		Constants.PRODUCT_TYPE_CONNECTIVITY, Constants.PRODUCT_TYPE_SERVICE, Constants.PRODUCT_TYPE_HARDWARE
	};
	
	/**
	 * Returns the sum of values of quantity attributes of provided product Configuration
	 */
	public Decimal productConfigurationQuantity(cscfga__Product_Configuration__c prodConfig, List<cscfga__Attribute__c> quantityAttributes) {
		Decimal quantity = 0.0;

		for (cscfga__Attribute__c qtyAttr : quantityAttributes) {
			if (belongsTo(qtyAttr, prodConfig) && !String.isEmpty(qtyAttr.cscfga__Value__c)) {
				try  {
					quantity += Decimal.valueOf(qtyAttr.cscfga__Value__c);
				}
				catch (TypeException e)  {
					// swallowed intentionally
				}
			}
		}
		return quantity;
	}
		/**
	 * Total revenue is calculated as a sum of all recurring price attribute prices multiplied by contract length in months
	 * and one time prices for particular configuarions where configurations are either:
	 * - has productName original product name or
	 * - are children of such configurations
	 *
	 */
	private Decimal calculateTotalRevenue(
			List<cscfga__Product_Configuration__c> productConfigurations,
			List<cscfga__Attribute__c> recurringPriceAttributes,
			List<cscfga__Attribute__c> oneTimePriceAttributes,
			List<cscfga__Attribute__c> quantityAttributes,
			Decimal contractTermLength,
			String productName) 
	{

		Decimal totalPrice = 0.0;
		Decimal totalQuantity = 0.0;

		for (cscfga__Product_Configuration__c prodConfig : productConfigurations) {
			// the product configuration could be of productName type or can lookup to productName type
			if (prodConfig.Original_Product_Name__c == productName 
				|| prodConfig.parent_product_configuration__r.Original_Product_Name__c == productName) {
				if (contractTermLength != null)	{
					for (cscfga__Attribute__c priceAttr : recurringPriceAttributes) {
   						if (belongsTo(priceAttr, prodConfig) && hasPrice(priceAttr)) {
   							totalPrice += priceAttr.cscfga__Price__c * contractTermLength;
						}
					}
				}
				for (cscfga__Attribute__c priceAttr : oneTimePriceAttributes) {
					if (belongsTo(priceAttr, prodConfig) && hasPrice(priceAttr)) {
						totalPrice += priceAttr.cscfga__Price__c;
					}
				}
			}

			if (prodConfig.Original_Product_Name__c == productName) 
			{

				for (cscfga__Attribute__c qtyAttr : quantityAttributes) {
					if (qtyAttr.cscfga__Product_Configuration__c == prodConfig.Id 
						&& !String.isEmpty(qtyAttr.cscfga__Value__c)) {
						try {
							totalQuantity += Decimal.valueOf(qtyAttr.cscfga__Value__c);
						}
					   	catch (TypeException e) {
							// swallowed intentionally
						}
					}
				}
			}
		}
		return totalQuantity == 0.0 ? 0.0 : totalPrice / totalQuantity;
	}

	/**
	 * Calculates monthly revenues for Opportunities from prefetched lists of product configurations,
	 * recurring price attributes and quantity attributes.
	 * Returns a Map of Opportunity Id -> Opportunity pairs. The Opportunity will have just the Id
	 * and revenue fields filled.
	 * Uses @see calculateMonthlyRevenue method
	 *
	 * @param productConfigurations List of product configurations for which the monthly revenue information is extracted
	 * @param recurringPriceAttributes List of recurring price attributes from which the revenue information is extracted
	 * @param quantityAttributes List of quantity attributes which are used for calculating the monthly revenue
	 */
	public Map<Id, Opportunity> calculateRevenue(
			List<cscfga__Product_Configuration__c> productConfigurations,
			List<cscfga__Attribute__c> recurringPriceAttributes,
			List<cscfga__Attribute__c> onetimePriceAttributes,
			List<cscfga__Attribute__c> quantityAttributes) 
	{

		Map<Id, Opportunity> opportunitiesMap = new Map<Id, Opportunity>();

		Map<Id, List<cscfga__Product_Configuration__c>> oppSplitConfigurations = splitConfigurationsByOpportunity(productConfigurations);

		for (Id oppId : oppSplitConfigurations.keySet()) {
			Opportunity opp = new Opportunity(Id = oppId);

			List<cscfga__Product_Configuration__c> oppProdConfigs = oppSplitConfigurations.get(oppId);

			System.debug('Petar testing uslo ' + oppId);
			System.debug('oppProdConfigs ' + oppProdConfigs);
			System.debug('onetimePriceAttributes ' + onetimePriceAttributes);
			System.debug('quantityAttributes ' + quantityAttributes);
			
			opp.Monthly_Revenue_Per_SIM__c = calculateMonthlyRevenue(oppProdConfigs, recurringPriceAttributes, quantityAttributes, Constants.PRODUCT_TYPE_CONNECTIVITY);
			opp.Monthly_Revenue_Per_Service__c = calculateMonthlyRevenue(oppProdConfigs, recurringPriceAttributes, quantityAttributes, Constants.PRODUCT_TYPE_SERVICE);
			opp.Monthly_Revenue_Per_Hardware__c = calculateMonthlyRevenue(oppProdConfigs, recurringPriceAttributes, quantityAttributes, Constants.PRODUCT_TYPE_HARDWARE);
			
			opp.One_Time_Revenue_Per_SIM__c = calculateOnetimeRevenue(oppProdConfigs, onetimePriceAttributes, quantityAttributes, Constants.PRODUCT_TYPE_CONNECTIVITY);
			opp.One_Time_Revenue_Per_Service__c = calculateOnetimeRevenue(oppProdConfigs, onetimePriceAttributes, quantityAttributes, Constants.PRODUCT_TYPE_SERVICE);
			opp.Revenue_Per_Unit__c = calculateOnetimeRevenue(oppProdConfigs, onetimePriceAttributes, quantityAttributes, Constants.PRODUCT_TYPE_HARDWARE);
			
			opportunitiesMap.put(oppId, opp);
		}
		return opportunitiesMap;
	}

	/**
	 * Calculates Opportunity contract terms. Returns a Map of Opportunity Id -> Opportunity pairs. The Opportunity will
	 * have just the Id and contract term fields filled. The contract term of a single product type is the maximum of
	 * available contract terms for that product type.
	 *
	 * @param productConfigurations List of product configurations for which the contract term information is extracted
	 */
	public Map<Id, Opportunity> calculateContractTerms(List<cscfga__Product_Configuration__c> productConfigurations) 
	{

		Map<Id, Opportunity> opportunitiesMap = new Map<Id, Opportunity>();

		Map<Id, List<cscfga__Product_Configuration__c>> oppSplitConfigurations = splitConfigurationsByOpportunity(productConfigurations);

		for (Id oppId : oppSplitConfigurations.keySet()) {
			Opportunity opp = new Opportunity(Id = oppId);

			List<cscfga__Product_Configuration__c> oppProdConfigs = oppSplitConfigurations.get(oppId);

			// Contract_Term_Per_Subscription__c is labeled as Contract Term Per Service
			opp.Contract_Term_Per_Subscription__c = calculateContractTerm(oppProdConfigs, Constants.PRODUCT_TYPE_SERVICE);
			opp.Contract_Term_Per_SIM__c = calculateContractTerm(oppProdConfigs, Constants.PRODUCT_TYPE_CONNECTIVITY);
			opp.Contract_Term_Per_Hardware__c = calculateContractTerm(oppProdConfigs, Constants.PRODUCT_TYPE_HARDWARE);

			opportunitiesMap.put(oppId, opp);
		}
		return opportunitiesMap;
	}

	/**
	 * Splits product configurations by distinct Opportunities into a map of Opportunity Id -> List<cscfga__Product_Configuration__c> product configurations
	 */
	public Map<Id, List<cscfga__Product_Configuration__c>> splitConfigurationsByOpportunity(List<cscfga__Product_Configuration__c> productConfigurations) 
	{

		Map<Id, List<cscfga__Product_Configuration__c>> oppSplitConfigurations = new Map<Id, List<cscfga__Product_Configuration__c>>();

		for (cscfga__Product_Configuration__c prodConfig : productConfigurations) {

			Id oppId = prodConfig.cscfga__Product_Bundle__r.cscfga__Opportunity__c;

			if (oppId != null && oppSplitConfigurations.get(oppId) == null) {
				oppSplitConfigurations.put(oppId, new List<cscfga__Product_Configuration__c>());
			}
			oppSplitConfigurations.get(oppId).add(prodConfig);
		}

		return oppSplitConfigurations;
	}
	
	
	private Decimal calculateOnetimeRevenue(List<cscfga__Product_Configuration__c> productConfigurations,
		List<cscfga__Attribute__c> onetimePriceAttributes, List<cscfga__Attribute__c> quantityAttributes, String productName) {
		
		Decimal totalPrice = 0.0;
		Decimal totalQty = 0.0;
		
		for (cscfga__Product_Configuration__c prodConfig : productConfigurations)  {
			if (prodConfig.Original_Product_Name__c == productName || prodConfig.parent_product_configuration__r.Original_Product_Name__c == productName || prodConfig.Original_Product_Name__c=='Fast Lane Opportunity') {
				totalPrice += productConfigurationPrice(prodConfig, onetimePriceAttributes);
			}
			if (prodConfig.Original_Product_Name__c == productName || prodConfig.Original_Product_Name__c=='Fast Lane Opportunity') {
				totalQty += productConfigurationQuantity(prodConfig, quantityAttributes);
			}
		}
		return totalQty == 0.0 ? 0.0 : totalPrice / totalQty;
	}
	
	/**
	 * Monthly revenue is calculated as a sum of all recurring price attribute prices of configurations that either:
	 * - has productName original product name or
	 * - are children of such configurations
	 *
	 * That sum is divided by the sum of values of quantity attributes of configurations that:
	 * - have productName original product name
	 *
	 * As an example, we could have two 'Connectivity' product configurations (for base rates) and one child
	 * product configuration that looks up to the 'Connectivity' product configuration (for data package).
	 * The monthly revenue is sum of product configuration 1, product configuration 2 and product configuration 3
	 * prices divided by the sum of product configuration 1 and product configuration 2 quantities.
	 */
	private Decimal calculateMonthlyRevenue(
			List<cscfga__Product_Configuration__c> productConfigurations,
		   	List<cscfga__Attribute__c> recurringPriceAttributes,
		   	List<cscfga__Attribute__c> quantityAttributes,
		   	String productName) 
	{

		Decimal totalQuantity = 0.0;
		Decimal totalRevenue = 0.0;
		Decimal maxContractTerm = 0.0;

		for (cscfga__Product_Configuration__c prodConfig : productConfigurations)  {
			
			Decimal monthlyTotalPrice = 0.0;
			Decimal configurationQuantity = 0.0;
			Decimal contractTerm = prodConfig.cscfga__Contract_Term__c;

			if (prodConfig.Original_Product_Name__c == productName || prodConfig.parent_product_configuration__r.Original_Product_Name__c == productName || prodConfig.Original_Product_Name__c=='Fast Lane Opportunity') {
				monthlyTotalPrice = productConfigurationPrice(prodConfig, recurringPriceAttributes);
			}

			if (prodConfig.Original_Product_Name__c == productName || prodConfig.Original_Product_Name__c=='Fast Lane Opportunity') {
				if (contractTerm != null && contractTerm > maxContractTerm) {
					maxContractTerm = contractTerm;
				} 
				totalQuantity += productConfigurationQuantity(prodConfig, quantityAttributes); 
			}
			  
			totalRevenue += monthlyTotalPrice * contractTerm;
		}
		
		return totalQuantity == 0.0 ? 0.0 : totalRevenue / (totalQuantity * maxContractTerm);
	}


	/**
	 * Calculates a maximum contract term in a list of productConfigurations for a product type (cscfga__Contract_Term__c has
	 * to be queried). Returns null if none is found.
	 */
	private Decimal calculateContractTerm(List<cscfga__Product_Configuration__c> productConfigurations, String productName) {
		Decimal contractTerm = null;
		for (cscfga__Product_Configuration__c prodConfig : productConfigurations) {
			if (prodConfig.Original_Product_Name__c == productName && prodConfig.cscfga__Contract_Term__c != null) {
				if (contractTerm == null || (contractTerm != null && contractTerm < prodConfig.cscfga__Contract_Term__c)) {
					contractTerm = prodConfig.cscfga__Contract_Term__c;
				}
			}
		}
		return contractTerm;
	}
	
	/**
	 * Checks whether attribute belongs to a product configuration (cscfga__Product_Configuration__c has to be queried)
	 */
	Boolean belongsTo(cscfga__Attribute__c attribute, cscfga__Product_Configuration__c productConfiguration) {
		return attribute.cscfga__Product_Configuration__c == productConfiguration.Id;
	}


	/**
	 * Checks whether attribute has a price set (cscfga__Price__c has to be queried)
	 */
	Boolean hasPrice(cscfga__Attribute__c attribute) {
		return attribute.cscfga__Price__c != null;
	}
	
	/**
	 * Returns product bundle related configurations
	 */ 
	public List<cscfga__Product_Configuration__c> getProductConfigurations(List<cscfga__Product_Bundle__c> productBundles) {
		
		return [
			SELECT
				cscfga__Contract_Term__c, Original_Product_Name__c,
				cscfga__Product_Bundle__c, cscfga__Product_Bundle__r.cscfga__Opportunity__c,
				parent_product_configuration__r.Original_Product_Name__c, parent_product_configuration__r.cscfga__Product_Bundle__c
			FROM
				cscfga__Product_Configuration__c
			WHERE
				cscfga__Product_Bundle__c IN :productBundles
		];
	}
}