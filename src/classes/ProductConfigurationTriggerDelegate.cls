public with sharing class ProductConfigurationTriggerDelegate extends TriggerHandler.DelegateBase
{
	//--------------------------------------------------------------------------
	// Members
	//--------------------------------------------------------------------------
	List<cscfga__Product_Configuration__c> insertedConfigurations = new List<cscfga__Product_Configuration__c>();
	Map<Id, cscfga__Product_Configuration__c> insertedConfigurationsWithDefinitionsMap = new Map<Id, cscfga__Product_Configuration__c>();

	List<cscfga__Product_Configuration__c> connectivityConfigurations = new List<cscfga__Product_Configuration__c>();
	List<cscfga__Product_Configuration__c> deletedConfigurations = new List<cscfga__Product_Configuration__c>();

	//--------------------------------------------------------------------------
	// Prepares before trigger
	//--------------------------------------------------------------------------
	public override void prepareBefore()
	{
		// prevent changes for configurations related to product bundles that are synchronized to an opportunity which has a frame contract in progress
		List<cscfga__Product_Configuration__c> changedProductConfigurations = (trigger.isDelete) ? trigger.old : trigger.new;

		preventChangesWithFrameContract(changedProductConfigurations);

		if (trigger.isDelete) {
			connectivityConfigurations = filterConnectivityProductConfigurations(trigger.old);
			deletedConfigurations = trigger.old;
		}

		if (trigger.isInsert) {
			copyOfferNameToField(trigger.new);
		}
	}

	//--------------------------------------------------------------------------
	// Before insert functionality
	//--------------------------------------------------------------------------
	public override void beforeInsert(sObject o)
	{
		copyConfigurationOfferToFromOffer((cscfga__Product_Configuration__c) o);
	}

	//--------------------------------------------------------------------------
	// After insert functionality
	//--------------------------------------------------------------------------
	public override void afterInsert(sObject o)
	{
		insertedConfigurations.add((cscfga__Product_Configuration__c)o);
	}

	//--------------------------------------------------------------------------
	// Finish with trigger processing
	//--------------------------------------------------------------------------
	public override void finish()
	{
		if (!insertedConfigurations.isEmpty()) {
			updateOriginalProductName(insertedConfigurations);
		}

		if (!connectivityConfigurations.isEmpty())
		{
			deleteChildConfigurations(connectivityConfigurations, deletedConfigurations);
			deleteTariffs(connectivityConfigurations);
		}
	}

	private void updateOriginalProductName(List<cscfga__Product_Configuration__c> productConfigurations) {

		Map<Id, cscfga__Product_Configuration__c> configsWithProductDefinitions = new Map<Id, cscfga__Product_Configuration__c>([
			SELECT Id, cscfga__Product_Definition__c, cscfga__Product_Definition__r.Name
			FROM cscfga__Product_Configuration__c
			WHERE Id IN :Trigger.new ]);

		List<cscfga__Product_Configuration__c> configurationsToUpdate = new List<cscfga__Product_Configuration__c>();

		for (cscfga__Product_Configuration__c prodConfig : insertedConfigurations) {

			cscfga__Product_Configuration__c updatedConfiguration =	new cscfga__Product_Configuration__c(Id = prodConfig.Id);
			updatedConfiguration.Original_Product_Name__c = configsWithProductDefinitions.get(updatedConfiguration.Id).cscfga__Product_Definition__r.Name;
			configurationsToUpdate.add(updatedConfiguration);
		}

		update configurationsToUpdate;
	}

	private void deleteTariffs(List<cscfga__Product_Configuration__c> productConfigurations) {

		List<cscfga__Attribute__c> tariffAttributes = [
			SELECT Id, Name, cscfga__Value__c,
				cscfga__Product_Configuration__r.cscfga__Product_Bundle__r.cscfga__Opportunity__r.AccountId
			FROM cscfga__Attribute__c
			WHERE Name = :Constants.ATTRIBUTE_NAME_TARIFF
			AND cscfga__Product_Configuration__c IN : connectivityConfigurations ];

		Set<Id> tariffIds = new Set<Id>();
		for (cscfga__Attribute__c att : tariffAttributes)
		{
			tariffIds.add((Id) att.cscfga__Value__c);
		}

		List<Tariff__c> tariffsToDelete = [
			SELECT Id
			FROM Tariff__c
			WHERE Id IN :tariffIds
			AND Parent_Tariff__c != null ];

		if (!tariffsToDelete.isEmpty())
		{
			delete tariffsToDelete;
		}
	}

	//--------------------------------------------------------------------------
	// Delete child configurations if they are not deleted before
	//--------------------------------------------------------------------------
	private void deleteChildConfigurations(List<cscfga__Product_Configuration__c> parentConfigurations, List<cscfga__Product_Configuration__c> deletedConfigurations)
	{
		List<cscfga__Product_Configuration__c> childConfigurations = [
			SELECT parent_product_configuration__c, Id
			FROM cscfga__Product_Configuration__c
			WHERE parent_product_configuration__c IN :parentConfigurations
			AND Id NOT IN :deletedConfigurations ];

		if (!childConfigurations.isEmpty())
		{
			delete childConfigurations;
		}
	}

	private void copyConfigurationOfferToFromOffer(cscfga__Product_Configuration__c productConfiguration) {
		if (productConfiguration.cscfga__Configuration_Offer__c != null) {
			productConfiguration.From_Offer__c = productConfiguration.cscfga__Configuration_Offer__c;
		}
	}

	private List<cscfga__Product_Configuration__c> filterOfferProductConfigurations(List<cscfga__Product_Configuration__c> productConfigurations) {

		List<cscfga__Product_Configuration__c> configsWithOffers = new List<cscfga__Product_Configuration__c>();

		for (cscfga__Product_Configuration__c config : productConfigurations) {
			if (config.cscfga__Product_Bundle__c != null && config.From_Offer__c != null) {
				configsWithOffers.add(config);
			}
		}
		return configsWithOffers;
	}

	private void copyOfferNameToField(List<cscfga__Product_Configuration__c> productConfigurations) {

		List<cscfga__Product_Configuration__c> configsWithOffers = filterOfferProductConfigurations(productConfigurations);

		Set<Id> offerIds = new Set<Id>();
		for (cscfga__Product_Configuration__c config : configsWithOffers) {
			offerIds.add(config.From_Offer__c);
		}

		Map<Id, cscfga__Configuration_Offer__c> offerMap = new Map<Id, cscfga__Configuration_Offer__c>([
			SELECT Name, Billing_Rate_Model__c
			FROM cscfga__Configuration_Offer__c
			WHERE Id IN :offerIds ]);

		// Copy name from offer to offer name field
		for (cscfga__Product_Configuration__c configuration : productConfigurations)
		{
			if (configuration.From_Offer__c != null)
			{
				configuration.Offer_Name__c = offerMap.get(configuration.From_Offer__c).Name;
				configuration.Billing_Rate_Model__c = offerMap.get(configuration.From_Offer__c).Billing_Rate_Model__c;
			}
		}
	}

	private List<cscfga__Product_Configuration__c> filterConnectivityProductConfigurations(List<cscfga__Product_Configuration__c> productConfigurations) {
		List<cscfga__Product_Configuration__c> filteredConfigurations = new List<cscfga__Product_Configuration__c>();

		for (cscfga__Product_Configuration__c config : productConfigurations) {
			if (config.Original_Product_Name__c == Constants.PRODUCT_TYPE_CONNECTIVITY) {
				filteredConfigurations.add(config);
			}
		}
		return filteredConfigurations;
	}

	private void preventChangesWithFrameContract(List<cscfga__Product_Configuration__c> changedConfigurations) {

		List<cscfga__Product_Configuration__c> configurationsWithOpps = [
			SELECT Id, cscfga__Product_Bundle__r.cscfga__Opportunity__c
			FROM cscfga__Product_Configuration__c
			WHERE Id in :changedConfigurations ];

		Map<Id, Id> configToOppIdMap = new Map<Id, Id>();

		for (cscfga__Product_Configuration__c prodConfig : configurationsWithOpps) {
			if (prodConfig.cscfga__Product_Bundle__r != null && prodConfig.cscfga__Product_Bundle__r.cscfga__Opportunity__c != null) {
				configToOppIdMap.put(prodConfig.Id, prodConfig.cscfga__Product_Bundle__r.cscfga__Opportunity__c);
			}
		}

		Map<Id, Opportunity> relatedOpportunities = new Map<Id, Opportunity>([
			SELECT HasSynchedBundle__c,
			( SELECT Id
			  FROM Frame_Contracts__r)
			FROM Opportunity
			WHERE Id in :configToOppIdMap.values() ]);

		for (cscfga__Product_Configuration__c config : changedConfigurations) {

			if (configToOppIdMap.get(config.Id) != null) {

				Opportunity relatedOpportunity = relatedOpportunities.get(configToOppIdMap.get(config.Id));

				// finally, the check
				if (relatedOpportunity != null && relatedOpportunity.HasSynchedBundle__c == true && relatedOpportunity.Frame_Contracts__r.size() > 0) {
					config.addError('The product configuration cannot be changed once a Frame Contract was requested on the related Product Bundle.');
				}
			}
		}
	}
}