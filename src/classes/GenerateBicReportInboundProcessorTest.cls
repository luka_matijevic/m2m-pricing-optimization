@isTest
private class GenerateBicReportInboundProcessorTest {
	
	@isTest static void testBicReportProcessing() {
		
		BIC_Export_Job_Data__c exportJob = new BIC_Export_Job_Data__c(
			Name = 'testJob'
		);

		insert exportJob;

		csam_t1__Outgoing_Message__c outMsg = new csam_t1__Outgoing_Message__c(
			Name = 'outgoing message',
			csam_t1__Callback_Host__c = 'test host', 
			csam_t1__Content_Type__c = 'test type', 
			csam_t1__URL_Host__c = 'test url'
		);

		insert outMsg;

		csam_t1__Outgoing_Message_Record__c record = new csam_t1__Outgoing_Message_Record__c(
			csam_t1__Object_Name__c = 'BIC_Export_Job_Data__c',
			csam_t1__Object_Record_Id__c = exportJob.Id,
			csam_t1__Outgoing_Message__c = outMsg.Id
		);

		insert record;

		csam_t1__Incoming_Message__c inMsg = new csam_t1__Incoming_Message__c(
			csam_t1__Outgoing_Message__c = outMsg.Id,
			csam_t1__HTTP_Method__c = 'PUT',
			csam_t1__Mac_Signature__c = 'mac'
		);

		insert inMsg;

		CompressedMessage cm = new CompressedMessage();
		cm.name = 'test';
		cm.content = 'Y29udGVudA=='; // == base64('content')
		cm.mediaType = 'application/zip';

		// Name has to start with 'Message' as implemented in the 
		// async messaging package functionality 
		Attachment msgAtt = new Attachment(
			Name = 'Message attachment',
			Body = Blob.valueOf(JSON.serialize(cm)),
			ParentId = inMsg.Id
		);

		insert msgAtt;

		GenerateBicReportInboundProcessor processor = new GenerateBicReportInboundProcessor();

		processor.processMessage(inMsg, new Map<Id, Id>());

		List<Attachment> attachments = [
			Select Id, Name, Body, ParentId, ContentType
			from Attachment 
			where ParentId = :exportJob.Id
		];
		/*
		System.assertEquals(1, attachments.size());

		Attachment result = attachments.get(0);
		System.assertEquals('test', result.Name);
		System.assertEquals('content', result.Body.toString());
		System.assertEquals('application/zip', result.ContentType);
		*/
	}	
}