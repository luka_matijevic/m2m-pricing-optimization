public class SelectBasketController {

    private string oppId;
    public string productBasketId { get; set; }
    public List<cscfga__Product_Basket__c> productBasketList {get; set;}
    public ProcessInstance currentApprovalProcess;
    public Opportunity opp {get; set;}
    
    
          // ALWAYS CHECK THIS BEFORE PRODUCTION DEPLOY !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    /*********************************************************************************************************/
    private String labelConfigCopyRequestId = Label.Config_Copy_Request_GLP_Id;
    private String labelTariffId = Label.Tariff_Id;
    private String labelDummyTariffId = Label.Dummy_Tariff_Id;
    private String labelApprovalProcessId = Label.Approval_Process_Id;
    /*************************************************************************************************************/
    
    public SelectBasketController(ApexPages.StandardController cntrl) {
		initiatePage();
	}
	
	public void initiatePage() {
	        
       string message = ApexPages.currentPage().getParameters().get('msg');
             
       if (message!=null) { 
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,message);
            ApexPages.addMessage(myMsg);
       }
	    
       oppId  = ApexPages.currentPage().getParameters().get('id');
       
       string opquery = 'SELECT ' + SobjectHelper.getFieldListForSOQL('Opportunity', null, null) + ' FROM Opportunity where id = :oppId'; 
       opp = database.query(opquery);
       
       productBasketList =  new List<cscfga__Product_Basket__c>();
       
       string pbquery = 'SELECT ' + SobjectHelper.getFieldListForSOQL('cscfga__Product_Basket__c', null, null) + ' from cscfga__Product_Basket__c where cscfga__Opportunity__c=\'' + oppId + '\' ORDER BY CreatedDate DESC';
       productBasketList = Database.query(pbquery);
       
       
       if(productBasketList.size() == 0) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Product basket list is empty!');
            ApexPages.addMessage(myMsg);
       }
       
       
	}


    public boolean getShowTable(){
        boolean show = true;
        if(productBasketList.size() == 0){
            show = false;
        }
        return show;
    }
    
    public PageReference addNewPricing() {
        
        cscfga__Product_Bundle__c theBundle  = new cscfga__Product_Bundle__c();
        theBundle.cscfga__Opportunity__c=oppId;
    
        insert theBundle;
    
        cscfga__Product_Basket__c basket= new cscfga__Product_Basket__c();
        basket.cscfga__Opportunity__c=oppId;
        basket.Related_Bundle_Id__c=theBundle.id;
       
        insert basket;
        
        
        cscfga__Product_Basket__c updateBasket = [SELECT name,pricing_reference_number__c from  cscfga__Product_Basket__c where id=:basket.id limit 1];
        updateBasket.name=updateBasket.pricing_reference_number__c;
        update updateBasket;
       
        
        if(opp.Product_Family__c=='Horizontals') {
        
        List<cscfga.ProductConfigurationHelper.ConfigurationCopyRequest> lista = new List<cscfga.ProductConfigurationHelper.ConfigurationCopyRequest>();
        lista.add(new cscfga.ProductConfigurationHelper.ConfigurationCopyRequest('a0n7E000000cWV2QAM', 1));

        cscfga.ProductConfigurationHelper pchelper = new cscfga.ProductConfigurationHelper();

        cscfga.ProductConfigurationHelper.copyConfigurations(lista, null,basket.id);
        
        string query_pc = 'SELECT ' +  SObjectHelper.getFieldListForSOQL('cscfga__product_configuration__c',null,null) + ' FROM cscfga__product_configuration__c where cscfga__Product_Basket__c=\''+basket.id+'\'';    
		List<cscfga__Product_Configuration__c> pclist = database.query(query_pc);   
        
        PageReference newocp = new PageReference('/apex/customConfiguration?id=' + basket.id + '&configId=' +  pclist[0].id + '&definitionId=a0p7E000000J3yM&retURL=/'+opp.id + '&linkedId=a0p7E000000J3yO');
        //PageReference newocp = new PageReference('/apex/customConfiguration?id=' + basket.id + '&definitionId=a0p7E000000J3yM&retURL=/'+opp.id + '&linkedId=a0p7E000000J3yO');
        
        newocp.setRedirect(true);

        return newocp;
            
        }
        else {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Global Local Pricing allows only Horizontal product family');
                ApexPages.addMessage(myMsg);
                 PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
                 pageRef.setRedirect(true);
                return pageRef;
        }
    }
    
    public PageReference editExistingBundle() {
        string productConfigurationId;
        List<cscfga__Product_Configuration__c> productConfigurations = [SELECT Id, cscfga__Product_Basket__c, CreatedDate FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c = :productBasketId ORDER BY CreatedDate ASC];
        
        if(productConfigurations != null && productConfigurations.size() > 0) {
            productConfigurationId = productConfigurations[0].Id;
        }
        
        PageReference newocp = new PageReference('/apex/customConfiguration?id=' + productBasketId + '&configId=' +  productConfigurationId + '&definitionId=a0p7E000000J3yM&retURL=/'+opp.id + '&linkedId=a0p7E000000J3yO');
        newocp.setRedirect(true);

        return newocp;
    }
    
    public PageReference deleteExistingBundle() {
        PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
        
        cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c WHERE Id = :productBasketId];
        
        delete basket;
        
        pageRef.setRedirect(true);
        return pageRef;
    }
}