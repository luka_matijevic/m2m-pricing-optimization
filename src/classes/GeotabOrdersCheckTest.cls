@isTest
private class GeotabOrdersCheckTest {
	
	@isTest static void test_method_one() {
		Account acc = Test_Util.CreateAccount('Account Test');
        acc.Global__c = true;
        insert acc;

		Opportunity opp = Test_Util.createOpportynity('Opportunity Test',acc.id,system.today(),'New','Price',10,10);
        opp.Description = 'Test description 20 characters';
        insert opp;

        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Order_Request__c;
        Map<String,Schema.RecordTypeInfo> OrReqRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
		Id rtId = OrReqRecordTypeInfo.get('GEOTAB').getRecordTypeId();


        Order_Request__c orQ = new Order_Request__c();
        orQ.Account__c = acc.Id;
        orQ.Opportunity__c = opp.Id;
        orQ.RecordTypeId = rtId;
        insert orQ;

		GEOTAB_Settings__c geotabSettings = new GEOTAB_Settings__c(Order_Request_Type_Id__c = rtId);
		insert geotabSettings;

		id simRecrdtype = Schema.SObjectType.Article_Info_Set__c.getRecordTypeInfosByName().get('SIM').getRecordTypeId();

		Article_Info_Set__c articleInfo = new Article_Info_Set__c(recordtypeId = simRecrdtype, name = 'Artical Info set', 
			Global_Local__c = 'Global', Status__c='Draft', One_Off_Journal_Mapping__c = 'Reccurring Fee',
            Journal_Mapping__c = 'SMS Usage Fee', Contract_Term__c ='12', One_Time_Fee__c = 100, Recurring_Fee__c = 100);
        insert articleInfo;

        Third_Upload_File__c tuf = new Third_Upload_File__c(name = 'Third upload file', SIM_Article_Type__c = articleInfo.id);
        insert tuf;

		Stock__c st = new Stock__c(name = 'Stock', Article_Info_Set__c = articleInfo.id, Status__c = 'On stock', 
			ICCID__c = '1111111111111111111', Third_Upload_File__c = tuf.id, Order_Request__c = orQ.Id);
        insert st;

        Test.startTest();
        GeotabOrdersCheck gos = new GeotabOrdersCheck();
		gos.execute(null);
		Test.stopTest();
	}
	
}