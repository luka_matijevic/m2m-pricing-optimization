public with sharing class ContractExtentionController {
	
	/**
	 * Custom exception
	 */
	public class CloneException extends Exception {} 
	
	public static final String OPP_ERROR = 'Opportunity should be Closed Won for Contract Extention';
	public static final String CONTRACT_EXTENDED_OPTION = 'VVL';
	public static final String REPRICING_OPTION = 'Repricing';
		
	/**
	 * Call to process Frame Contract related Opportunity
	 */
	public static Opportunity processFC(Id fcId, Id parentFcId, No_Trigger__c noTrigger, String option) {
		Savepoint sp = Database.setSavepoint(); // Savepoint to rollback all changes
		Id  destinationId = null;
		List<Opportunity> oppList = new List<Opportunity>();
		List<Frame_Contract__c> fcList = new List<Frame_Contract__c>();
		Opportunity destOpp = null;
		Frame_Contract__c parentFc;
		String prodBundleNamePrefix;
		
		try{
			Integer bundleFieldLength = cscfga__Product_Bundle__c.Name.getDescribe().getLength();
			fcList = [Select Id, Name, Extension_Counter__c,Product_Bundle__c, Repricing_Counter__c, Opportunity__c, Opportunity__r.Name, Opportunity__r.StageName from Frame_Contract__c where Id=: fcId limit 1];
			
			if (null != fcList[0].Opportunity__c && !fcList[0].Opportunity__r.StageName.equalsIgnoreCase('Closed Won')) {
				throw new CloneException (OPP_ERROR); 
				return null;
			}
			if (parentFcId != fcId) { 
				parentFc = [Select id, Name, Repricing_Counter__c, Extension_Counter__c, Opportunity__c, Opportunity__r.Name, Opportunity__r.StageName from Frame_Contract__c where id=:parentFcId];
			} else {
				parentFc = fcList[0];
			}
			Id sourceId = parentFc.Opportunity__c;
			//Database.SaveResult saveParentResult = CloneUtil.createCloneParentRecord(String.escapeSingleQuotes(sourceId), 'StageName', 'Prospecting');//clone parent record
			//21.3.2017. PM Added cloning of opportunity to input description.
			Database.SaveResult saveParentResult = CloneUtil.createCloneParentRecordOpportunity(String.escapeSingleQuotes(sourceId), 'StageName', 'Prospecting', parentFc.Name);//clone parent 
			if (saveParentResult.isSuccess()) {//If successful show Id, if not show error
				destinationId = saveParentResult.getId();//New destination Id
			} else {
				for (Database.Error dbError : saveParentResult.getErrors()) {//If there were errors, show to Visualforce page
					system.debug('@@@@Error In Clone Process>>>>' + dbError.getMessage());
					Database.rollback(sp); 
					throw new CloneException (dbError.getMessage()); 
					return null;
				}
			}
			system.debug('**KK + opp: ' + destinationId);
			// Cloning of ContactRoles
			List<Database.Error> dbErrors = CloneUtil.createCloneChildRecords(String.escapeSingleQuotes(sourceId), destinationId, 'OpportunityContactRole', 'OpportunityId');
			if (!dbErrors.isEmpty()) {//Verify if there were errors in the clone process
				for (Database.Error dbError : dbErrors) {//If there were errors, show to Visualforce page
					system.debug('@@@@Error In Clone Process----' + dbError.getMessage());
					Database.rollback(sp);
					throw new CloneException (dbError.getMessage());
				}
			}
			
			// set triggers back to active
			if (notrigger != null) {
				notrigger.Flag__c = false;
				upsert notrigger;
			}

			if (destinationId != null) {
				//Query New & Old Opportunity Data
				for (Opportunity opp : [Select Id, Name, Amended_From__c, Amended_To__c from Opportunity where Id=:destinationId OR Id=:sourceId]) {
					if (opp.Id == destinationId) {
						opp.Amended_From__c = sourceId;
						opp.Probability = 0;
						opp.Preliminary_TCV__c = 0;
						opp.Total_Connectivity_Rev_in_current_year__c = 0;
						opp.One_Time_Revenue_Per_Service__c = 0;
						opp.One_Time_Revenue_Per_SIM__c = 0;
						opp.Monthly_Revenue_Per_SIM__c = 0;
						opp.Amended_To__c = null;
						
						destOpp = opp;
						if (option.contains(CONTRACT_EXTENDED_OPTION)) {
							if (parentFc.Extension_Counter__c == null) {
								parentFc.Extension_Counter__c = 1;
							} else {
								parentFc.Extension_Counter__c = parentFc.Extension_Counter__c + 1;
							}
							opp.Name = opp.Name + '_' + option + '_' + parentFc.Extension_Counter__c;
							prodBundleNamePrefix = 'Contract_Extended_PB_';
							opp.Description = 'This is a contract extended opportunity from the old ' + parentFc.Name;
							
						} else if (option.contains(REPRICING_OPTION)) {
							if (parentFc.Repricing_Counter__c == null) {
								parentFc.Repricing_Counter__c = 1;
							} else {
								parentFc.Repricing_Counter__c = parentFc.Repricing_Counter__c + 1;
							}
							opp.Name = opp.Name + '_' + option + '_' + parentFc.Repricing_Counter__c;
							prodBundleNamePrefix = 'Repriced_PB_';
							opp.Type = 'Repricing';
							opp.Description = 'This is a repriced opportunity from the old ' + parentFc.Name;
						}
						
						// sbrtan added a check to right pad the name if it is too long. So that counter doesn't get lost.
						if((opp.Name != null) && (opp.Name.length()> 255)){
							opp.Name = opp.Name.right(254);
						}
						

						/*
						if (opp.Name.contains(option)) {
							String num = opp.Name.substring(opp.Name.lastIndexOf(option + '_') + stringLength);
							Integer newNum = Integer.valueOf(num) + 1;
							String replace = option + '_' + num;
							String replacement = option + '_' + newNum;
							opp.Name = opp.Name.replace(replace,replacement);
						} else {
							opp.Name = opp.Name + '_' + option + '_1';
						}
						*/
					}
					else if (opp.Id == sourceId) {
						//Set Old Opportunity Data
						//opp.Name = opp.Name+ ' - Amended';
						//opp.OldFrameContract__c = fcId;
						opp.Amended_To__c = destinationId;
						if (opp.Description == null || opp.Description.length() < 20)
							opp.Description = 'This opportunity is repriced/extended ' + System.Now();
					}
					oppList.add(opp);
				}
				
				// Clone Bundle & Configuration data
				cscfga__Product_Bundle__c sourceBundle = [SELECT Id, Name, cscfga__Opportunity__c FROM cscfga__Product_Bundle__c WHERE cscfga__Opportunity__c =:sourceId AND cscfga__Synchronised_with_Opportunity__c=true limit 1];
				// Set target bundle
				cscfga__Product_Bundle__c targetBundle = new cscfga__Product_Bundle__c();
				
				String targetName = prodBundleNamePrefix + sourceBundle.Name;
				
				if (targetName.length() > bundleFieldLength){
					targetName = targetName.substring(0, bundleFieldLength);
				}
				targetBundle.Name = targetName;
				targetBundle.cscfga__Opportunity__c = destinationId;
				targetBundle.Re_Price__c = true; // Pavan, To track budnle as repriced bundle

				// Set target basket (we don't need the original basket)
				cscfga__Product_Basket__c targetBasket = new cscfga__Product_Basket__c();
				targetBasket.Name = 'Basket for Opportunity: ' + oppList[0].Name;// use the new opp name
				targetBasket.cscfga__Basket_Status__c = 'Valid';
				
				// Create bundle and basket
				insert targetBundle;
				insert targetBasket;
				// Copy configurations to product bundle
				List<cscfga__Product_Configuration__c> configurations = [SELECT Id,Name FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Bundle__c =:sourceBundle.Id ORDER BY CreatedDate ASC ];
                
				List<Id> childIdList = cscfga.API_1.cloneConfigurations(configurations, targetBundle, targetBasket);
				List<cscfga__Attribute__c> attributesProdConfigList = new List<cscfga__Attribute__c>();
				cscfga__Attribute__c clonedBundleAtt;
				Set<String> nameSet = new Set<String>{'Account Id', 'Bundle Id', 'Config Id', 'Cloned Tariff'};
				for (cscfga__Attribute__c att : [SELECT cscfga__Value__c, Name, cscfga__Product_Configuration__c, Id,
												cscfga__Product_Configuration__r.cscfga__Product_Bundle__c, 
												cscfga__Product_Configuration__r.cscfga__Product_Bundle__r.cscfga__Opportunity__r.AccountId
												FROM cscfga__Attribute__c where cscfga__Product_Configuration__c IN: childIdList AND Name IN: nameSet]) {
												    
					if (att.Name.equalsIgnoreCase('Account Id')) {
						att.cscfga__Value__c = att.cscfga__Product_Configuration__r.cscfga__Product_Bundle__r.cscfga__Opportunity__r.AccountId;
					}
					if (att.Name.equalsIgnoreCase('Bundle Id')) {
						att.cscfga__Value__c = att.cscfga__Product_Configuration__r.cscfga__Product_Bundle__c;
					}
					if (att.Name.equalsIgnoreCase('Config Id')) {
						att.cscfga__Value__c = att.cscfga__Product_Configuration__c;
					}
					if(att.Name == 'Cloned Tariff'){
		                clonedBundleAtt = att;
		                continue;
		            }
					
					attributesProdConfigList.add(att);
				}
				System.debug(attributesProdConfigList);
				if (!attributesProdConfigList.isEmpty()) {
					update attributesProdConfigList;
				}
				
				//postCopyConnectivityConfig(targetBundle.Id);
				if (clonedBundleAtt != null && !String.isEmpty(clonedBundleAtt.cscfga__Value__c)){
		            CopyTariffHelper ctHelper = new CopyTariffHelper();
		            Id clonedTariffId = ctHelper.copyTariffWithAllOptions(clonedBundleAtt.cscfga__Value__c, targetBundle.Id);
		            if (clonedTariffId != null){
		                clonedBundleAtt.cscfga__Value__c = String.valueOf(clonedTariffId);
		                update clonedBundleAtt;
		            }
		        }
				// Set new Opportunity & bundle on Frame Contract
				// fcList[0].Opportunity__c = destinationId;
				// fcList[0].Product_Bundle__c = targetBundle.Id;
				
				// Set trigger to inactive
				if (notrigger != null) {
					notrigger.Flag__c = true;
					upsert notrigger;
				}
				// Update Opportunity data 
				if (!oppList.isEmpty()) {
					upsert oppList;
				}
				
				// set triggers back to active
				if (notrigger != null) {
					notrigger.Flag__c = false;
					upsert notrigger;
				} 
			}
		} catch(Exception e) {
			Database.rollback(sp);
			System.debug(e);
			System.debug(e.getMessage());
			System.debug(e.getLineNumber());
			throw e; 
			return null;
		}
		return destOpp;
	}
	
	/*
	 * PostCopyConnectivityConfigurations call. Exected as asynchronous to avoid Number of SOQL governor limit.
	 */
	@Future
	public static void postCopyConnectivityConfig(Id bundleId) {
		PostCopyConnectivityConfigurations connector = new PostCopyConnectivityConfigurations(bundleId);
		connector.doExecute(); 
	}
}