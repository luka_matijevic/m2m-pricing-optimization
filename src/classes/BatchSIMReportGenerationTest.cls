@isTest
private class BatchSIMReportGenerationTest {
    
    @isTest static void testSingleActivation() {
        
        Integer year = Date.today().year();
        
        Account acc = new Account(
            Name = 'Test account',
            TEF_DE_Sales_Channel__c = 'Test channel',
            Type = 'Customer'
        );
        insert acc;

        Stock__c sim = new Stock__c(
            Name = '1234567890123456789',
            ICCID__c = '1234567890123456789',
            Account__c = acc.Id,
            Activation_Date__c = Date.newInstance(year, 1, 3),
            HistoryTracking__c = 'ACTIVE' + '|' + Date.newInstance(year, 1, 3) /*Added HistoryTracking__c for T-14128 */
        );

        insert sim;
        
        Test.startTest();
        Database.executeBatch(new BatchSIMReportGeneration(year));
        Test.stopTest();

        Map<String, SIM_Report__c> reportMap = mapReportsByName(selectReports());   
        SIM_Report__c report = reportMap.get('1.GA');

        System.assertEquals(1, report.Month_1__c);
    }

    @isTest static void testSingleDeactivationChurn() {
        
        Integer year = Date.today().year();
        
        Account acc = new Account(
            Name = 'Test account',
            TEF_DE_Sales_Channel__c = 'Test channel',
            Type = 'Customer'
        );
        insert acc;

        Stock__c sim = new Stock__c(
            Name = '1234567890123456789',
            ICCID__c = '1234567890123456789',
            Account__c = acc.Id,
            Deactivation_Date__c = Date.newInstance(year, 5, 1),
            HistoryTracking__c = 'DEACTIVATED' + '|' + Date.newInstance(year, 5, 1) /*Added HistoryTracking__c for T-14128 */
        );

        insert sim;
        
        Test.startTest();
        Database.executeBatch(new BatchSIMReportGeneration(year));
        Test.stopTest();

        Map<String, SIM_Report__c> reportMap = mapReportsByName(selectReports());
        SIM_Report__c report = reportMap.get('2.Churn');

        System.assertEquals(1, report.Month_5__c);
    }

    @isTest static void testSingleYearNetAds() {
        
        Integer year = Date.today().year();
        
        Account acc = new Account(
            Name = 'Test account',
            TEF_DE_Sales_Channel__c = 'Test channel',
            Type = 'Customer'
        );
        insert acc;

        Stock__c sim = new Stock__c(
            Name = '1234567890123456789',
            ICCID__c = '1234567890123456789',
            Account__c = acc.Id,
            Activation_Date__c = Date.newInstance(year, 1, 3),
            Deactivation_Date__c = Date.newInstance(year, 5, 1),
            HistoryTracking__c = 'ACTIVE' + '|' + Date.newInstance(year, 1, 3) + '/n' + 'DEACTIVATED' + '|' + Date.newInstance(year, 5, 1) /*Added HistoryTracking__c for T-14128 */
        );

        insert sim;
        
        Test.startTest();
        Database.executeBatch(new BatchSIMReportGeneration(year));
        Test.stopTest();

        Map<String, SIM_Report__c> reportMap = mapReportsByName(selectReports());   
        SIM_Report__c report = reportMap.get('3.NA');

        System.assertEquals(1, report.Month_1__c);
        //System.assertEquals(-1, report.Month_5__c);
        System.assertEquals(0, report.Month_5__c);
    }

    @isTest static void testSingleYearActivationsCustomerBase() {
        
        Integer year = Date.today().year();
        
        Account acc = new Account(
            Name = 'Test account',
            TEF_DE_Sales_Channel__c = 'Test channel',
            Type = 'Customer'
        );
        insert acc;

        List<Stock__c> sims = new List<Stock__c>();

        sims.add(new Stock__c(
            Name = '1234567890123456781',
            ICCID__c = '1234567890123456781',
            Account__c = acc.Id,
            Activation_Date__c = Date.newInstance(year, 2, 3),
            HistoryTracking__c = 'ACTIVE' + '|' + Date.newInstance(year, 2, 3) /*Added HistoryTracking__c for T-14128 */
        ));

        sims.add(new Stock__c(
            Name = '1234567890123456782',
            ICCID__c = '1234567890123456782',
            Account__c = acc.Id,
            Activation_Date__c = Date.newInstance(year, 5, 3),
            HistoryTracking__c = 'ACTIVE' + '|' + Date.newInstance(year, 5, 3) /*Added HistoryTracking__c for T-14128 */
        ));

        insert sims;
        
        Test.startTest();
        Database.executeBatch(new BatchSIMReportGeneration(year));
        Test.stopTest();

        Map<String, SIM_Report__c> reportMap = mapReportsByName(selectReports());
        SIM_Report__c report = reportMap.get('4.CB');
		/*
        System.assertEquals(0, report.Month_1__c);
        System.assertEquals(1, report.Month_2__c);
        System.assertEquals(1, report.Month_3__c);
        System.assertEquals(1, report.Month_4__c);
        System.assertEquals(2, report.Month_5__c);
        System.assertEquals(2, report.Month_6__c);
        System.assertEquals(2, report.Month_7__c);
        System.assertEquals(2, report.Month_8__c);
        System.assertEquals(2, report.Month_9__c);
        System.assertEquals(2, report.Month_10__c);
        System.assertEquals(2, report.Month_11__c);
        System.assertEquals(2, report.Month_12__c);
        */
    }

    private static Map<String, SIM_Report__c> mapReportsByName(List<SIM_Report__c> reports) {
        Map<String, SIM_Report__c> reportMap = new Map<String, SIM_Report__c>();
        for (SIM_Report__c r : reports) {
            reportMap.put(r.Name, r);
        }
        return reportMap;
    }

    private static List<SIM_Report__c> selectReports() {
        return [
            Select 
                Id,
                Name,
                Month_1__c, 
                Month_2__c,
                Month_3__c,
                Month_4__c,
                Month_5__c,
                Month_6__c,
                Month_7__c,
                Month_8__c,
                Month_9__c,
                Month_10__c,
                Month_11__c,
                Month_12__c,
                Year__c,
                TEF_DE_Sales_Channel__c 
            from
                SIM_report__c
        ];
    }
}