/**
 * @author Ilija Pavlic, Adam Mehtic
 */ 
global class BatchPrebillFetcher implements Database.Batchable<Billing_Account__c>, Database.Stateful {
  
  global Daily_Bill__c dailyBill;
  
  /**
   * Constructs the prebill fetching batch. Inserts the daily bill so that asynchronously retrieved 
   * prebills can be attached to it.
   */ 
  global BatchPrebillFetcher(Date forDate) {
      //define next months dates
      Date nextMonth = forDate.addMonths(1);
      Date nextMonthBill = Date.newInstance(nextMonth.year(), nextMonth.month(), 1);
      
      //pull next month bill or insert the new one if there is not any
      List<Daily_Bill__c> nextList= [Select id from Daily_Bill__c where date__c=:nextMonthBill];
      
      if (nextList.isEmpty()) {
      Daily_Bill__c nextDailyBill = new Daily_Bill__c(Date__c = nextMonthBill, Prebill_Requests_Sent__c = 0);
      insert nextDailyBill;
      }
      
      //pull todays bill or insert the new one if there is not any
      List<Daily_Bill__c> nowList= [Select id, Date__c,Prebill_Requests_Sent__c from Daily_Bill__c where date__c=:forDate];
    
    if (nowList.isEmpty()) {
    this.dailyBill = new Daily_Bill__c(Date__c = forDate, Prebill_Requests_Sent__c = 0);
    insert dailyBill;
    }
    else this.dailyBill=nowlist[0];
  }

  global List<Billing_Account__c> start(Database.BatchableContext BC) {
      // Adam Mehtic made changes due to new filter inside SOQL query.
      Set<Id> baDistinct = new Set<Id>();
      List<Billing_Account__c> baReturn = [Select  Id,  Trial__c, Payer__c, Sm2m_Type__c, Exclude_from_Prebill_Fetch__c From Billing_Account__c where Payer__r.Billing_Cycle__c =: billingCycle(dailyBill.Date__c)];
      List<Billing_Account__c> baFullList = baReturn;

        
      if(baReturn != null && baReturn.size() > 0) {
        List<csord__Subscription__c> connSubscriptions = [Select Id, Billing_Account__c, Billing_Account__r.Sm2m_Type__c From csord__Subscription__c where csord__Identification__c = :Constants.PRODUCT_TYPE_CONNECTIVITY AND Billing_Account__c in :baReturn];
      
          
          if(connSubscriptions != null && connSubscriptions.size() > 0) {
            for(csord__Subscription__c sub : connSubscriptions) {
                baDistinct.add(sub.Billing_Account__c);
            }
          }
      }
    
    if(baDistinct != null && baDistinct.size() > 0) {
        baReturn = [Select Id, Payer__c, Sm2m_Type__c From Billing_Account__c where Id in :baDistinct];

        Map<Id, String> accountDistinct = new Map<Id, String>();
        Map<Id, String> duplicateAccountDistinct = new Map<Id, String>();
        Map<Id, String> finalMap = new Map<Id, String>();
      
        for(Billing_Account__c bac : baReturn) {
            if(accountDistinct.containsKey(bac.Payer__c)) {
                String sm2mType = accountDistinct.get(bac.Payer__c);
                
                if(sm2mType != null && !string.isEmpty(sm2mType) && !sm2mType.equalsIgnoreCase(bac.Sm2m_Type__c)) {
                    duplicateAccountDistinct.put(bac.Payer__c, bac.Sm2m_Type__c);
                }
            } else {
                accountDistinct.put(bac.Payer__c, bac.Sm2m_Type__c);
            }
        }
      
        baReturn = new List<Billing_Account__c>();
            
          for(Billing_Account__c baItem : baFullList) {
                if(accountDistinct.containsKey(baItem.Payer__c)) {
                    String mapValue = accountDistinct.get(baItem.Payer__c);
                    //Petar Matkovic 11.1.2017. Added: && baItem.Exclude_from_Prebill_Fetch__c == true
                    if(mapValue != null && mapValue.equalsIgnoreCase(baItem.Sm2m_Type__c) && baItem.Exclude_from_Prebill_Fetch__c == false) {
                        baReturn.add(baItem);
                    }
                }
            
                if(duplicateAccountDistinct.containsKey(baItem.Payer__c)) {
                    String mapValue = duplicateAccountDistinct.get(baItem.Payer__c);
                    //Petar Matkovic 11.1.2017. Added: && baItem.Exclude_from_Prebill_Fetch__c == true
                    if(mapValue != null && mapValue.equalsIgnoreCase(baItem.Sm2m_Type__c) && baItem.Exclude_from_Prebill_Fetch__c == false) {
                          baReturn.add(baItem);
                    }
                }
          }
    } else {
        baReturn = new List<Billing_Account__c>();
    }
    
    return baReturn;
  }

  /**
   * Increments the number of sent requests to update on finish of the batch.
   */ 
    global void execute(Database.BatchableContext BC, list<Billing_Account__c> scope) {
        for (Billing_Account__c billingAccount : scope) {
            csam_t1.ObjectGraphCalloutHandler.queueMessageFromId('Retrieve Prebill', billingAccount.Id);
            dailyBill.Prebill_Requests_Sent__c++;
        }
    }


  /**
   * The boolean All_Prebills_Requested__c helps to distinguish between 0/0 case as a result of processing and 0/0
   * as default values of Prebill_Requests_Sent__c and Prebill_Requests_Processed__c.
   */   
  global void finish(Database.BatchableContext BC) {
    dailyBill.All_Prebills_Requested__c = true;
    update dailyBill;
  }
  
  private String billingCycle(Date d) {
    return String.valueOf(d.day());
  }
}