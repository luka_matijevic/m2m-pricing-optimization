public class ContractTerm {

	Integer term;
	Date startDate;
	Date endDate;

	public ContractTerm(Date startDate, Decimal contractMonths) {
		this.startDate = startDate;
		this.term = (Integer) contractMonths;
		if (term == null) {
			term = 0;
		}
		this.endDate = startDate.addMonths(term);
	}

	public Integer getMonths() {
		return term;
	}

	public Integer getTotalYears() {
		return endDate.year() - startDate.year() + 1;
	}
	
	public Date getStartDate() {
		return startDate;
	}
}