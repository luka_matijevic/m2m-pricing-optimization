/**
 *
 * Helper class for SObject functionality.
 */
public class SObjectHelper
{
	// Local cache of SObjectField maps
	static Map<String, Map<String, Schema.SObjectField>> sObjectNameToFieldsMap =
		new Map<String, Map<String, Schema.SObjectField>>();

	// Local cache for field descriptions
	static Map<Schema.SObjectType, Map<String, Schema.DescribeFieldResult>> sObjectTypeToFieldDescriptionsMap =
		new Map<Schema.sObjectType, Map<String, Schema.DescribeFieldResult>>();

	// Local cache of SObjectField maps
	static Map<Schema.sObjectType, Map<String, Schema.SObjectField>> sObjectTypeToFieldsMap =
		new Map<Schema.sObjectType, Map<String, Schema.SObjectField>>();

	/**
	 * @description Returns a map with field names for keys and the related DescribeFieldResult for values.
	 */
	public static Map<String, Schema.DescribeFieldResult> getFieldDescriptions(String sObjectName)
	{
		Map<String, Schema.SObjectField> fieldsMap;

		if (!sObjectNameToFieldsMap.containsKey(sObjectName))
		{
			Map<String,Schema.SobjectType> describe = Schema.getGlobalDescribe();
			fieldsMap = describe.get(sObjectName).getDescribe().fields.getMap();
			sObjectNameToFieldsMap.put(sObjectName, fieldsMap);
		}
		else
		{
			fieldsMap = sObjectNameToFieldsMap.get(sObjectName);
		}

		Map<String, Schema.DescribeFieldResult> result = new Map<String, Schema.DescribeFieldResult>();
		for (Schema.SObjectField f : fieldsMap.values())
		{
			Schema.DescribeFieldResult d = f.getDescribe();
			result.put(d.getName(), d);
		}
		return result;
	}

	/**
	 * Gets field description
	 */
	public static Schema.DescribeFieldResult getFieldDescription(
			Schema.SObjectType objectType,
			String fieldName)
	{
		//
		// Try to get previous field describe
		//
		Map<String, Schema.DescribeFieldResult> describeFieldMap =
			sObjectTypeToFieldDescriptionsMap.get(objectType);
		//
		// If field map doesn't exist create one
		//
		if (describeFieldMap == null)
		{
			describeFieldMap = new Map<String, Schema.DescribeFieldResult>();
			sObjectTypeToFieldDescriptionsMap.put(objectType, describeFieldMap);
		}
		//
		// If describe result doesn't exist retrieve it and add to map
		//
		Schema.DescribeFieldResult describeField = describeFieldMap.get(fieldName);
		if (describeField == null) {
			//
			// Get field
			//
			Schema.SObjectField field = getFields(objectType).get(fieldName);

			if (field  == null)
			{
				return null;
			}
			describeField = field.getDescribe();
			describeFieldMap.put(fieldName, describeField);
		}

		return describeField;
	}

	/**
	 * Returns a comma separated list of fields of the SObject with the
	 * given name, which can be used in SOQL queries.
	 * Fields defined in ignoreFields will be ignored.
	 * The given prefix is added to all fields.
	 */
	public static String getFieldListForSOQL(String objectName, String prefix, set<String> ignoreFields)
	{

		set<String> objFields = getFieldDescriptions(objectName).keyset();

		String result = '';
		for (String fName : objFields)
		{
			if (ignoreFields == null || !ignoreFields.contains(fName))
			{
				result += (prefix != null ? prefix : '') + fName + ',';
			}
		}

		// Remove last comma
		if (result.length() > 0)
		{
			result = result.substring(0, result.length() - 1);
		}

		return result;
	}

	/**
	 * Returns a map with field types for keys and object fields for values.
	 */
	public static Map<String, Schema.SObjectField> getFields(Schema.sObjectType sObjectType)
	{

		Map<String, Schema.SObjectField> fieldsMap;

		if (!sObjectTypeToFieldsMap.containsKey(sObjectType))
		{
			fieldsMap = sObjectType.getDescribe().fields.getMap();
			sObjectTypeToFieldsMap.put(sObjectType, fieldsMap);
		}
		else
		{
			fieldsMap = sObjectTypeToFieldsMap.get(sObjectType);
		}

		return fieldsMap;
	}

	/**
	 * This method tries to merge field from a source sObject to a target
	 * sObject without overwriting already existing fields.
	 * The objects are not necessarily of the same type.
	 * The code will not work on fields that are not writeable,
	 * and will swallow SObjectExceptions on setting such fields.
	 */
	public static void mergeFields(sObject target, sObject source) {
		if (target != null && source != null) {
			mergeFields(target, source, false);
		}
	}

	/**
	 * This method tries to merge field from a source sObject to a target sObject with an optional
	 * overwrite of already existing fields. The Id field is never overwritten.
	 * The objects are not necessarily of the same type. The code will not work on fields that are
	 * not writeable, and will swallow SObjectExceptions on setting such fields.
	 * TODO: exception if fields are not retrieved
	 */
	public static void mergeFields(sObject target, sObject source, Boolean overwrite) {

		Map<String, Schema.SObjectField> sourceFieldMap = getFields(source.getSObjectType());
		Map<String, Schema.SObjectField> targetFieldMap = getFields(target.getSObjectType());

		for (String field : sourceFieldMap.keySet()) {
			try {
				// if the source field has content and there is a field named the same and writeable on the target
				if (field != 'Id' && targetFieldMap.get(field) != null && source.get(field) != null) {
					// if an overwrite should occur
					if (target.get(field) == null || overwrite) {
						target.put(field, source.get(field));
					}
				}
			}
			catch (System.SObjectException e) {
				// swallowed intentionally.
			}
		}
	}

	/**
	 * @author Ilija Pavlic
	 * @description Removes objects of sObject type from the list and returns the removed objects.
	 */ 
	public static List<sObject> extractSObjects(List<sObject> sObjects, Schema.SObjectType sObjectType) {
		
		List<sObject> filtered = new List<sObject>();
		List<sObject> remaining = new List<sObject>();
		 
		for (SObject obj : sObjects) {
			if (sObjectType == obj.getSObjectType()) {
				filtered.add(obj);
			} else {
				remaining.add(obj);
			}
		}
		
		sObjects.clear();
		sObjects.addAll(remaining);

		return filtered; 
	}

	
	/**
	 * @author Ilija Pavlic
	 * @description Filters an sObject list to return the objects of sObjectType type
	 */ 
	public static List<sObject> filterSObjects(List<sObject> sObjects, Schema.SObjectType sObjectType) {
		List<SObject> filtered = new List<sObject>();
		for (SObject obj : sObjects) {
			if (sObjectType == obj.getSObjectType()) {
				filtered.add(obj);
			}
		}
		return filtered;
	}

	/**
	 * @author Ilija Pavlic
	 * Validate mandatory fields
	 * Return list of formated errors. List is empty if no errors.
	 */
	public static List<String> validatePresence(SObject obj, List<String> mandatoryFields)
	{
		List<String> validationErrors =
			new List<String>();

		Map<String, String> fieldNameToLabelMap =
			new Map<String, String>();

		Map<String, Schema.SObjectField> fields =
			obj.getsObjectType().getDescribe().fields.getMap();

		for (Schema.SObjectField f : fields.values())
		{
			Schema.DescribeFieldResult describe = f.getDescribe();
			fieldNameToLabelMap.put(
					describe.getName().toLowerCase(),
					describe.getLabel());
		}

		for (String field : mandatoryFields)
		{
			String label =
				!String.isEmpty(fieldNameToLabelMap.get(field.toLowerCase())) ?
				fieldNameToLabelMap.get(field.toLowerCase()) : field;
			try
			{
				Object value = obj.get(field);
				if (value == null || String.isEmpty(String.valueOf(value)))
				{
					validationErrors.add('Mandatory field ' + label + ' is missing or empty.');
				}
			}
			catch (System.SObjectException e)
			{
				validationErrors.add('Mandatory field ' + label + ' is missing or empty.');
				System.debug('Field ' + label + ' was not available on ' + obj.getSObjectType());
			}
		}
		return validationErrors;
	}

}