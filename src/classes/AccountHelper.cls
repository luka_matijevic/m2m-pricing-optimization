public class AccountHelper {
    
    public static void PopulateAccountSM2MInformation(Account acc){
        //get contacts
        List<Contact> cList = new List<Contact>([select id, email from Contact where AccountId = :acc.id and  email != null]);
        
        if(null != acc.BillingCountry && acc.BillingCountry=='Germany' && 
           (acc.VAT__c != 19 || acc.Language__c !='German' || acc.Billing_Cycle__c != '1' || acc.Currency__c != 'Euro') 
          ){
              acc.VAT__c = 19;
              acc.Language__c = 'German';
              acc.Billing_Cycle__c = '1';
              acc.Currency__c = 'Euro';
              if (!cList.isEmpty()) acc.Company_Email__c = cList[0].email;        
              update acc;  
          }
        else if (null != acc.BillingCountry && acc.BillingCountry !='Germany' && 
                 (acc.VAT__c != 0 || acc.Language__c !='English' || acc.Billing_Cycle__c != '1' || acc.Currency__c != 'Euro') 
                ){
                    acc.VAT__c = 0;
                    acc.Language__c = 'English';
                    acc.Billing_Cycle__c = '1';
                    acc.Currency__c = 'Euro';
                    if (!cList.isEmpty()) acc.Company_Email__c = cList[0].email;        
                    update acc;  
                }
        
        
    }
    
    public static void CreateBillingAccount(Frame_Contract__c fc){
        //check if Billing_Account__c already exists
        Opportunity opp = [select id, recordtype.name, Account.name,AccountId, Account.BillingCountry, Account.BillingStreet, Account.BillingCity, Account.BillingPostalCode from Opportunity where id = :fc.opportunity__c limit 1];
        boolean globalBA = false;
        boolean trialBA = false;
        if (opp.recordtype.name == 'Commercial Global' || opp.recordtype.name == 'IoT Connect' || opp.recordtype.name == 'Trial Global' || opp.recordtype.Name == 'IoT Connect Advanced')
            globalBA = true;
        if (opp.recordtype.name == 'Trial Global')
            trialBA = true;
        if (globalBA){
            List<Billing_Account__c> baList = [select id from Billing_Account__c where payer__c = :opp.AccountId and SM2M_Type__c = 'Global'];
            if (baList.isEmpty()){
                //in case of missing or invalid account address, contact country and address will be used
                List<Contact> con = [select id, Country__c, MailingAddress, account.name from Contact where accountid = :opp.AccountId ];
                String dummyString = '*';
                Billing_Account__c ba = new Billing_Account__c();
                ba.payer__c = opp.AccountId ;
                ba.name = opp.Account.name.substring(0, opp.Account.name.length()<20 ? opp.Account.name.length() : 20) +  ' Global' ;
                ba.SM2M_Type__c = 'Global';           
                ba.Payment_Term__c = '30 Tage netto.';
                ba.Payment_Method__c = 'Bank Transfer';
                ba.OFI_ID__c = 'update_' + opp.Account.name;
                ba.Trial__c = trialBA;
                if (!trialBA) ba.Billable__c = true;
                if(null != opp.Account.BillingCountry && null != opp.Account.BillingStreet && null != opp.Account.BillingCity && null!=opp.Account.BillingPostalCode)
                    ba.Use_Account_Address__c = true;           
                else {
                    ba.Use_Account_Address__c = false;
                    ba.Billing_Country__c = !con.isEmpty() && null !=con[0].MailingAddress && null != con[0].MailingAddress.getCountry()   ? con[0].MailingAddress.getCountry() : dummyString;
                    ba.Billing_Street__c = !con.isEmpty() && null !=con[0].MailingAddress && null != con[0].MailingAddress.getStreet() ? con[0].MailingAddress.getStreet() : dummyString;
                    ba.Billing_City__c = !con.isEmpty() && null !=con[0].MailingAddress && null != con[0].MailingAddress.getCity() ? con[0].MailingAddress.getCity() : dummyString;
                    ba.Billing_Postcode__c = !con.isEmpty() && null !=con[0].MailingAddress && null != con[0].MailingAddress.getPostalCode()  ? con[0].MailingAddress.getPostalCode() : dummyString;
                    ba.Billing_Country__c = !con.isEmpty() && null !=con[0].MailingAddress && null != con[0].MailingAddress.getCountry()  ? con[0].MailingAddress.getCountry() : dummyString;
                    ba.Billing_State__c = !con.isEmpty() && null !=con[0].MailingAddress && null != con[0].MailingAddress.getState()  ? con[0].MailingAddress.getState() : dummyString;
                }
                if(opp.Account.BillingCountry == 'Germany' ||   
                   (!con.isEmpty() && con[0].Country__c == 'Germany') || 
                   (!con.isEmpty() && null !=con[0].MailingAddress && con[0].MailingAddress.getCountry()  == 'Germany')){
                       ba.vat__c = '19%';
                       ba.Billing_Language__c = 'German';
                   }
                else {
                    ba.vat__c = '0%';
                    ba.Billing_Language__c = 'English';
                }
                /*
if(ba.SM2M_Type__c == 'Local' && !ba.payer__r.Local__c){
//update account set Local__c
Account a = [select id, Local__c from Account where id = :ba.payer__c];
a.Local__c = true;
update a;
}
else */
                if (ba.SM2M_Type__c == 'Global' && !ba.payer__r.Global__c){
                    //update account set Global__c
                    Account a = [select id, Global__c from Account where id = :ba.payer__c];
                    a.Global__c = true;
                    update a;
                }   
                
                insert ba;  
                
                
            }
            
            
        }
    }

    public static void CreateBillingAccount(Id oppId){
        //check if Billing_Account__c already exists
        Opportunity opp = [select id, recordtype.name, Account.name,AccountId, Account.BillingCountry, Account.BillingStreet, Account.BillingCity, Account.BillingPostalCode from Opportunity where id = :oppId limit 1];
        boolean globalBA = false;
        boolean trialBA = false;
        if (opp.recordtype.name == 'Commercial Global' || opp.recordtype.name == 'IoT Connect' || opp.recordtype.name == 'Trial Global')
            globalBA = true;
        if (opp.recordtype.name == 'Trial Global')
            trialBA = true;
        if (globalBA){
            List<Billing_Account__c> baList = [select id from Billing_Account__c where payer__c = :opp.AccountId and SM2M_Type__c = 'Global'];
            if (baList.isEmpty()){
                //in case of missing or invalid account address, contact country and address will be used
                List<Contact> con = [select id, Country__c, MailingAddress, account.name from Contact where accountid = :opp.AccountId ];
                String dummyString = '*';
                Billing_Account__c ba = new Billing_Account__c();
                ba.payer__c = opp.AccountId ;
                ba.name = opp.Account.name.substring(0, opp.Account.name.length()<20 ? opp.Account.name.length() : 20) +  ' Global' ;
                ba.SM2M_Type__c = 'Global';           
                ba.Payment_Term__c = '30 Tage netto.';
                ba.Payment_Method__c = 'Bank Transfer';
                ba.OFI_ID__c = 'update_' + opp.Account.name;
                ba.Trial__c = trialBA;
                if (!trialBA) ba.Billable__c = true;
                if(null != opp.Account.BillingCountry && null != opp.Account.BillingStreet && null != opp.Account.BillingCity && null!=opp.Account.BillingPostalCode)
                    ba.Use_Account_Address__c = true;           
                else {
                    ba.Use_Account_Address__c = false;
                    ba.Billing_Country__c = !con.isEmpty() && null !=con[0].MailingAddress && null != con[0].MailingAddress.getCountry()   ? con[0].MailingAddress.getCountry() : dummyString;
                    ba.Billing_Street__c = !con.isEmpty() && null !=con[0].MailingAddress && null != con[0].MailingAddress.getStreet() ? con[0].MailingAddress.getStreet() : dummyString;
                    ba.Billing_City__c = !con.isEmpty() && null !=con[0].MailingAddress && null != con[0].MailingAddress.getCity() ? con[0].MailingAddress.getCity() : dummyString;
                    ba.Billing_Postcode__c = !con.isEmpty() && null !=con[0].MailingAddress && null != con[0].MailingAddress.getPostalCode()  ? con[0].MailingAddress.getPostalCode() : dummyString;
                    ba.Billing_Country__c = !con.isEmpty() && null !=con[0].MailingAddress && null != con[0].MailingAddress.getCountry()  ? con[0].MailingAddress.getCountry() : dummyString;
                    ba.Billing_State__c = !con.isEmpty() && null !=con[0].MailingAddress && null != con[0].MailingAddress.getState()  ? con[0].MailingAddress.getState() : dummyString;
                }
                if(opp.Account.BillingCountry == 'Germany' ||   
                   (!con.isEmpty() && con[0].Country__c == 'Germany') || 
                   (!con.isEmpty() && null !=con[0].MailingAddress && con[0].MailingAddress.getCountry()  == 'Germany')){
                       ba.vat__c = '19%';
                       ba.Billing_Language__c = 'German';
                   }
                else {
                    ba.vat__c = '0%';
                    ba.Billing_Language__c = 'English';
                }
                /*
if(ba.SM2M_Type__c == 'Local' && !ba.payer__r.Local__c){
//update account set Local__c
Account a = [select id, Local__c from Account where id = :ba.payer__c];
a.Local__c = true;
update a;
}
else */
                if (ba.SM2M_Type__c == 'Global' && !ba.payer__r.Global__c){
                    //update account set Global__c
                    Account a = [select id, Global__c from Account where id = :ba.payer__c];
                    a.Global__c = true;
                    update a;
                }   
                
                insert ba;  
                
                
            }
            
            
        }
    }


    
}