//------------------------------------------------------------------------------
// Class for configuring tariff options in connectivity product configuration
//------------------------------------------------------------------------------
public without sharing class ConnectivityTariffOptions implements IConnectivityTariffOptions
{
	//--------------------------------------------------------------------------
	// Private members
	//--------------------------------------------------------------------------
	private Id tariffId;
	private Id accountId;
	private String accountName;
	private Id configurationId;
	private ITariff originalTariff;
	private ITariff parentTariff;
	private ITariff clonedTariff;
	private IConnectivityConfigurations configurations;
	private Decimal totalExpectedDataOverUsage;
	private Decimal totalExpectedTextOverUsage;
	private Decimal totalExpectedVoiceOverUsage;

	//--------------------------------------------------------------------------
	// Public methods
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructor used in factory
	//--------------------------------------------------------------------------
	public ConnectivityTariffOptions(
			ITariff originalTariff,
			ITariff parentTariff,
			Id accountId,
			Id configurationId,
			IConnectivityConfigurations configurations)
	{
		this.originalTariff = originalTariff;
		this.parentTariff = parentTariff;
		this.accountId = accountId;
		this.configurationId = configurationId;
		this.configurations = configurations;
		prepareTariffs();
	}

	//--------------------------------------------------------------------------
	// Get all tariff options sorted by type and sequence
	//--------------------------------------------------------------------------
	public List<ITariffOption> getTariffOptions()
	{
		return clonedTariff.getTariffOptions();
	}

	//--------------------------------------------------------------------------
	// Get included tariff options sorted by sequence
	//--------------------------------------------------------------------------
	public List<ITariffOption> getIncludedTariffOptions()
	{
		return clonedTariff.getIncludedTariffOptions();
	}

	//--------------------------------------------------------------------------
	// Get additional tariff options sorted by sequence
	//--------------------------------------------------------------------------
	public List<ITariffOption> getAdditionalTariffOptions()
	{
		return clonedTariff.getAdditionalTariffOptions();
	}

	//--------------------------------------------------------------------------
	// Get supplementary service tariff options
	//--------------------------------------------------------------------------
	public List<ITariffOption> getSupplementaryServiceTariffOptions()
	{
		return clonedTariff.getSupplementaryServiceTariffOptions();
	}

	//--------------------------------------------------------------------------
	// Clone tariff with all selected tariff options and configurations
	//--------------------------------------------------------------------------
	public Id storeTariff() {
	        configurations.deleteProductConfigurations();
	        
	        //originalTariff=clonedTariff;
	        
	    	originalTariff.copyQuantities(clonedTariff);
			calculateExpectedOverUsages();
			originalTariff.storeUpdate();
			//configurations.createProductConfigurations(originalTariff);
			return originalTariff.getTariff().Id;
	}
	
	
	public Id cloneTariff(String contractTerm)
	{
		//
		// Remove configurations from database
		//
		system.debug(originalTariff);
		system.debug(clonedTariff);
		
		configurations.deleteProductConfigurations();

		system.debug(originalTariff);
		system.debug(clonedTariff);
		
		//
		// Prepare and cloned tariff and all selected tariff options
		//
		clonedTariff.prepareChild(parentTariff, accountId, accountName, contractTerm);
		
		
		system.debug(originalTariff);
		system.debug(clonedTariff);
		
		
		//
		// Remove original tariff if not needed
		//
		removeOriginalTariff();
        
		//
		// Do not clone tariff if original and cloned are the same
		// without quantities.
		// Quantities should be copied from clonedTariff to the original
		// because we use this for creating product configurations
		//
		system.debug(originalTariff);
		system.debug(clonedTariff);
		
		if (originalTariff.equalsWithoutQuantity(clonedTariff))
		{
			originalTariff.copyQuantities(clonedTariff);
			calculateExpectedOverUsages();
			originalTariff.storeUpdate();
			 system.debug(totalExpectedDataOverUsage);
			return originalTariff.getTariff().Id;
			
		}

		//
		// Store cloned tariff and all selected tariff options
		//
		clonedTariff.store();

		originalTariff = clonedTariff;
		clonedTariff = originalTariff.deepClone();
		//
		// Calculate expected overusages for getters later
		//
		calculateExpectedOverUsages();
		
        system.debug(totalExpectedDataOverUsage);
		
		return originalTariff.getTariff().Id;
	}

	//--------------------------------------------------------------------------
	// Create product configurations for additional and supplementary service
	// Returns:
	//		Unique key that is part of craeted product configurations
	//--------------------------------------------------------------------------
	public String createConfigurations()
	{
		System.debug('originalTariff ' + originalTariff);
		return configurations.createProductConfigurations(originalTariff);
	}

	//--------------------------------------------------------------------------
	// Get commercial approval required
	//--------------------------------------------------------------------------
	public Boolean getCommercialApprovalRequired()
	{
		return (clonedTariff.getTariff().Status__c != Constants.TARIFF_STATUS_APPROVED);
	}

	//--------------------------------------------------------------------------
	// Get total expected data over usage for included tariff options
	//--------------------------------------------------------------------------
	public Decimal getTotalExpectedDataOverUsage()
	{
		return totalExpectedDataOverUsage;
	}

	//--------------------------------------------------------------------------
	// Get total expected text over usage for included tariff options
	//--------------------------------------------------------------------------
	public Decimal getTotalExpectedTextOverUsage()
	{
		return totalExpectedTextOverUsage;
	}

	//--------------------------------------------------------------------------
	// Get total expected voice over usage for included tariff options
	//--------------------------------------------------------------------------
	public Decimal getTotalExpectedVoiceOverUsage()
	{
		return totalExpectedVoiceOverUsage;
	}

	//--------------------------------------------------------------------------
	// Private methods
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Prepare tariffs for user
	//--------------------------------------------------------------------------
	private void prepareTariffs()
	{
		//
		// Set account name
		//
		if (accountId != null)
		{
			accountName = '';
			List<Account> accounts = [ SELECT Name FROM Account WHERE Id = :accountId ];
			if (accounts.size() > 0)
			{
				accountName = accounts[0].Name;
			}
		}
		//
		// Deep merge original and parent tariff
		//
		if (parentTariff != originalTariff)
		{
			originalTariff.deepMerge(parentTariff);
		}

        //MJ 25_01_2016 - quantities should not be copied from configuration - Tariffoption has this data!
		//configurations.setQuantities(originalTariff);
		
		//
		// Clone tariff and set account Id
		//
		clonedTariff = originalTariff.deepClone();
		clonedTariff.getTariff().Account__c = accountId;
	}

	//--------------------------------------------------------------------------
	// Remove original tariff if not needed
	//--------------------------------------------------------------------------
	private void removeOriginalTariff()
	{
		if (originalTariff != parentTariff
				&& !originalTariff.equalsWithoutQuantity(clonedTariff))
		{
			Integer configurationsAtTariff =
					Utils.countConfigurationsOnTariff(originalTariff.getTariff().Id);
			//
			// Check if any other product configuration is looking at this tariff
			// If not delete tariff
			//
			if ((configurationsAtTariff == 0)
				|| (configurationId != null && configurationsAtTariff == 1))
			{
				delete originalTariff.getTariff();
			}
		}
	}

	//--------------------------------------------------------------------------
	// Calculate data over usages
	//--------------------------------------------------------------------------
	private void calculateExpectedOverUsages()
	{
		Decimal data = 0;
		Decimal text = 0;
		Decimal voice = 0;
		system.debug(clonedTariff);
		for (ITariffOption tariffOption : clonedTariff.getIncludedTariffOptions())
		{
			TariffOptionCalc calc = new TariffOptionCalc(tariffOption);
			system.debug(calc);
			data = Utils.addValues(data, calc.getDataOverUsage());
			text = Utils.addValues(text, calc.getTextOverUsage());
			voice = Utils.addValues(voice, calc.getVoiceOverUsage());
			system.debug(tariffOption);
			system.debug(data);
		}

		totalExpectedDataOverUsage = Utils.nullIfZero(data);
		totalExpectedTextOverUsage = Utils.nullIfZero(text);
		totalExpectedVoiceOverUsage = Utils.nullIfZero(voice);
	}
}