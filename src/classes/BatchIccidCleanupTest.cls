@isTest
private class BatchIccidCleanupTest {
	
	@isTest static void testIccidPrefixCleanup() {
		
		No_Validation__c noValidation = new No_Validation__c(
			Flag__c = true
		);
		insert noValidation;

		Stock__c sim = new Stock__c(ICCID__c = 'a1234567891234567891');
		insert sim;

		noValidation.Flag__c = false;
		update noValidation;

		Test.startTest();
		Database.executeBatch(new BatchIccidCleanup(), 100);
		Test.stopTest();

		Stock__c cleanedSim = [Select ICCID__c from Stock__c where Id = :sim.Id];
		System.assertEquals('1234567891234567891', cleanedSim.ICCID__c);
	}

	@isTest static void testIccidSuffixCleanup() {
		
		No_Validation__c noValidation = new No_Validation__c(
			Flag__c = true
		);
		insert noValidation;

		Stock__c sim = new Stock__c(ICCID__c = '1234567891234567891a');
		insert sim;

		noValidation.Flag__c = false;
		update noValidation;

		Test.startTest();
		Database.executeBatch(new BatchIccidCleanup(), 100);
		Test.stopTest();

		Stock__c cleanedSim = [Select ICCID__c from Stock__c where Id = :sim.Id];
		System.assertEquals('1234567891234567891', cleanedSim.ICCID__c);
	}
}