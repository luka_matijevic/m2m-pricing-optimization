global class OpportunityBatchUpdateYearlyTotal implements Database.Batchable<sObject> {

	public String query = 'SELECT Id FROM Opportunity';

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<Opportunity> opportunities = new List<Opportunity>();
		for (sObject obj : scope) {
			Opportunity opp = (Opportunity) obj;
			opp.Total_Connectivity_Rev_in_current_year__c = M2MYearlyTotalCalculator.calculateConnectivityRevenue(opp.Id, Date.today().year());
			opp.Total_Service_Revenue_in_current_year__c = M2MYearlyTotalCalculator.calculateServiceRevenue(opp.Id, Date.today().year());
			opp.Total_Service_Revenue_in_current_year__c = M2MYearlyTotalCalculator.calculateHardwareRevenue(opp.Id, Date.today().year());
			opportunities.add(opp);
		}
		update opportunities;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
}