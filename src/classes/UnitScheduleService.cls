/**
 * This class is used for calculations of Unit Schedules.
 * The service is not static by design, to allow mockups for testing.
 */
public with sharing class UnitScheduleService {

	private static Map<String, String> productFieldPrefixes = new Map<String, String>{
    		Constants.PRODUCT_TYPE_CONNECTIVITY => 'Connectivity_Sims_',
    		Constants.PRODUCT_TYPE_FASTLANE => 'Connectivity_Sims_',
    		'FLO Connectivity' => 'Connectivity_Sims_',
    		Constants.PRODUCT_TYPE_SERVICE => 'VAS_Units_',
    		Constants.PRODUCT_TYPE_HARDWARE => 'Hardware_Units_'
    };
	
	private ProductConfigurationService prodConfigService;
	
    /**
     * Empty constructor. The service is not static by design, to allow mockups for testing.
     */
    public UnitScheduleService() {
    	prodConfigService = new ProductConfigurationService();
    }

    public UnitScheduleService(ProductConfigurationService productConfigurationService) {
    	this.prodConfigService = productConfigurationService;
    }
    
    private Date determineStartDate(Date opportunityCloseDate) {
    	Date tdy = Date.today();
    	Date d = opportunityCloseDate < tdy ? tdy.addMonths(1) : opportunityCloseDate.addMonths(1);
    	return Date.newInstance(d.year(), d.month(), 1); 
    }
    

    /**
     * Builds unit schedules out of preselected relevant data. The opportunities in the map should have contract terms calculated.
     * Based on the maximum contract term, the required number of unit schedules (years) are created. Then, based on every individual
     * contract term, the unit schedules are filled with monthly amounts by dividing the total quantity of a single product type by
     * the contract term (in months) of that product type.
     * If the total amount in not evenly divisible, then the remainder is added to the first month.
     *
     * @param opportunitySplitProductConfigurations Map of Opportunity Id -> Product Configurations
     * @return List of build Unit_Schedule__c objects
     */
    public List<Unit_Schedule__c> buildUnitSchedules(List<Opportunity> opportunities, List<cscfga__Product_Configuration__c> productConfigurations, List<cscfga__Attribute__c> quantityAttributes) {

        List<Unit_Schedule__c> unitSchedules = new List<Unit_Schedule__c>();
        System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! uslo');
        //Marko Zulic, new logic implemented for building Unit Schedule, requested by huseyin
		
		/*Map<Id, Date> opportunityCloseDates = new Map<Id, Date>();
		
		if(){
	
		}

		for (Opportunity opp : [Select Id, CloseDate from Opportunity where Id in :opportunities]) {
			opportunityCloseDates.put(opp.Id, opp.CloseDate);
		}

		Map<Id, List<cscfga__Product_Configuration__c>> oppSplitConfigurations = prodConfigService.splitConfigurationsByOpportunity(productConfigurations);
			
        for (Opportunity opp : opportunities) {

			Date startDate = determineStartDate(opportunityCloseDates.get(opp.Id));
			
			List<Unit_Schedule__c> oppUnitSchedules = initializeUnitSchedules(opp, startDate);
			system.debug('**IP** opp '+ JSON.serializePretty(opp)); 
			system.debug('**IP** oppUnitSchedules '+ JSON.serializePretty(oppUnitSchedules)); 
			
			for (cscfga__Product_Configuration__c prodConfig : oppSplitConfigurations.get(opp.Id)) {

				String productName = prodConfig.Original_Product_Name__c;
				
				ContractTerm contractTerm = null;
				if (productName == Constants.PRODUCT_TYPE_CONNECTIVITY) {
					contractTerm = new ContractTerm(startDate, opp.Contract_Term_Per_SIM__c);
				} else if (productName == Constants.PRODUCT_TYPE_SERVICE) {
					contractTerm = new ContractTerm(startDate, opp.Contract_Term_Per_Subscription__c);
				} else if (productName == Constants.PRODUCT_TYPE_HARDWARE) {
					contractTerm = new ContractTerm(startDate, opp.Contract_Term_Per_Hardware__c);
				} else if (productName == Constants.PRODUCT_TYPE_FASTLANE || productName == 'FLO Connectivity') {
				    contractTerm = new ContractTerm(startDate, 24.0);
				}
				
				if (contractTerm != null) {
				
					Decimal qty = prodConfigService.productConfigurationQuantity(prodConfig, quantityAttributes);
					if (qty<10000) {
					while (qty > 0) {    	 
					    for (Integer month = startDate.month(); month < startDate.month() + contractTerm.getMonths() && qty > 0; month++) {		
						    
						    Unit_Schedule__c unitSchedule = new Unit_Schedule__c();
							
							if (productName == Constants.PRODUCT_TYPE_FASTLANE || productName == 'FLO Connectivity') unitSchedule = oppUnitSchedules.get(0);
							else  unitSchedule = oppUnitSchedules.get((month - 1) / 12);
						    
							String fieldName = productFieldPrefixes.get(productName) + Utils.getMonthName(Math.mod(month - 1, 12) + 1) + '__c';
							
							Integer currentQty = Integer.valueOf(unitSchedule.get(fieldName));
							unitSchedule.put(fieldName, currentQty != null ? currentQty + 1 : 1);
							qty--;
					    }
					}
					}
					else {
					    Unit_Schedule__c unitSchedule = new Unit_Schedule__c();
					    unitSchedule = oppUnitSchedules.get(0);
				    	
				    	String fieldName = productFieldPrefixes.get(productName) + Utils.getMonthName(Math.mod(1 - 1, 12) + 1) + '__c';
						
						Integer currentQty = Integer.valueOf(unitSchedule.get(fieldName));
						unitSchedule.put(fieldName, qty); 
					}
				}
			}*/


			List<Activation_plan__c> activationPlans = [SELECT Id, Connectivity_SIMs_January__c, Connectivity_SIMs_February__c, Connectivity_SIMs_March__c,
																Connectivity_SIMs_April__c, Connectivity_SIMs_May__c, Connectivity_SIMs_June__c,
																Connectivity_SIMs_July__c, Connectivity_SIMs_August__c, Connectivity_SIMs_September__c,
																Connectivity_SIMs_October__c, Connectivity_SIMs_November__c, Connectivity_SIMs_December__c,
																Opportunity__c, Year__c
																FROM Activation_plan__c 
																WHERE Opportunity__c in :opportunities];

			
			Map<Id, List<cscfga__Product_Configuration__c>> oppSplitConfigurations = prodConfigService.splitConfigurationsByOpportunity(productConfigurations);
			Map<Id, Date> opportunityCloseDates = new Map<Id, Date>();
			for (Opportunity opp : [Select Id, CloseDate from Opportunity where Id in :opportunities]) {
				opportunityCloseDates.put(opp.Id, opp.CloseDate);
			}

			for(Opportunity opp : opportunities){
				System.debug(opportunities);
				Date startDate = determineStartDate(opportunityCloseDates.get(opp.Id));
				if(opp.Record_Type_Name__c == 'IoT Connect'){
					List<Unit_Schedule__c> oppUnitSchedules = initializeUnitSchedules(opp, startDate);
					system.debug('**IP** opp '+ JSON.serializePretty(opp)); 
					system.debug('**IP** oppUnitSchedules '+ JSON.serializePretty(oppUnitSchedules)); 
					
					for (cscfga__Product_Configuration__c prodConfig : oppSplitConfigurations.get(opp.Id)) {

						String productName = prodConfig.Original_Product_Name__c;
						
						ContractTerm contractTerm = null;
						if (productName == Constants.PRODUCT_TYPE_CONNECTIVITY) {
							contractTerm = new ContractTerm(startDate, opp.Contract_Term_Per_SIM__c);
						} else if (productName == Constants.PRODUCT_TYPE_SERVICE) {
							contractTerm = new ContractTerm(startDate, opp.Contract_Term_Per_Subscription__c);
						} else if (productName == Constants.PRODUCT_TYPE_HARDWARE) {
							contractTerm = new ContractTerm(startDate, opp.Contract_Term_Per_Hardware__c);
						} else if (productName == Constants.PRODUCT_TYPE_FASTLANE || productName == 'FLO Connectivity') {
						    contractTerm = new ContractTerm(startDate, 24.0);
						}
						
						if (contractTerm != null) {
						
							Decimal qty = prodConfigService.productConfigurationQuantity(prodConfig, quantityAttributes);
							if (qty<10000) {
							while (qty > 0) {    	 
							    for (Integer month = startDate.month(); month < startDate.month() + contractTerm.getMonths() && qty > 0; month++) {		
								    
								    Unit_Schedule__c unitSchedule = new Unit_Schedule__c();
									
									if (productName == Constants.PRODUCT_TYPE_FASTLANE || productName == 'FLO Connectivity') unitSchedule = oppUnitSchedules.get(0);
									else  unitSchedule = oppUnitSchedules.get((month - 1) / 12);
								    
									String fieldName = productFieldPrefixes.get(productName) + Utils.getMonthName(Math.mod(month - 1, 12) + 1) + '__c';
									
									Integer currentQty = Integer.valueOf(unitSchedule.get(fieldName));
									unitSchedule.put(fieldName, currentQty != null ? currentQty + 1 : 1);
									qty--;
							    }
							}
							}
							else {
							    Unit_Schedule__c unitSchedule = new Unit_Schedule__c();
							    unitSchedule = oppUnitSchedules.get(0);
						    	
						    	String fieldName = productFieldPrefixes.get(productName) + Utils.getMonthName(Math.mod(1 - 1, 12) + 1) + '__c';
								
								Integer currentQty = Integer.valueOf(unitSchedule.get(fieldName));
								unitSchedule.put(fieldName, qty); 
							}
						}
					}
					}
					else{




				//List<Unit_Schedule__c> oppUnitSchedules = initializeUnitSchedules(opp, startDate);
				system.debug(oppSplitConfigurations.get(opp.Id));
				for (cscfga__Product_Configuration__c prodConfig : oppSplitConfigurations.get(opp.Id)) {
					
					String productName = prodConfig.Original_Product_Name__c;
					system.debug(activationPlans);


					for(Activation_plan__c a : activationPlans){

						Unit_Schedule__c unitSchedule;

						system.debug(productName + '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');

						if (productName == Constants.PRODUCT_TYPE_FASTLANE || productName == 'FLO Connectivity') {
							unitSchedule = new Unit_Schedule__c();
							unitSchedule.Opportunity__c = opp.Id;
							unitSchedule.Year__c = a.Year__c;

							unitSchedule.Connectivity_SIMs_January__c = a.Connectivity_SIMs_January__c;
							unitSchedule.Connectivity_SIMs_February__c = a.Connectivity_SIMs_February__c;
							unitSchedule.Connectivity_SIMs_March__c = a.Connectivity_SIMs_March__c;
							unitSchedule.Connectivity_SIMs_April__c = a.Connectivity_SIMs_April__c;
							unitSchedule.Connectivity_SIMs_May__c = a.Connectivity_SIMs_May__c;
							unitSchedule.Connectivity_SIMs_June__c = a.Connectivity_SIMs_June__c;
							unitSchedule.Connectivity_SIMs_July__c = a.Connectivity_SIMs_July__c;
							unitSchedule.Connectivity_SIMs_August__c = a.Connectivity_SIMs_August__c;
							unitSchedule.Connectivity_SIMs_September__c = a.Connectivity_SIMs_September__c;
							unitSchedule.Connectivity_SIMs_October__c = a.Connectivity_SIMs_October__c;
							unitSchedule.Connectivity_SIMs_November__c = a.Connectivity_SIMs_November__c;
							unitSchedule.Connectivity_SIMs_December__c = a.Connectivity_SIMs_December__c;

						} else if (productName == Constants.PRODUCT_TYPE_CONNECTIVITY) {
							Boolean usAlreadyExists = false;
							for (Unit_Schedule__c ucTmp : unitSchedules){
								if (ucTmp != null && ucTmp.Year__c != null && ucTmp.Year__c == a.Year__c){
									usAlreadyExists = true;
									break;
								}
							}
							if (!usAlreadyExists){
								unitSchedule = new Unit_Schedule__c();
								unitSchedule.Opportunity__c = opp.Id;
								unitSchedule.Year__c = a.Year__c;

								unitSchedule.Connectivity_SIMs_January__c = a.Connectivity_SIMs_January__c;
								unitSchedule.Connectivity_SIMs_February__c = a.Connectivity_SIMs_February__c;
								unitSchedule.Connectivity_SIMs_March__c = a.Connectivity_SIMs_March__c;
								unitSchedule.Connectivity_SIMs_April__c = a.Connectivity_SIMs_April__c;
								unitSchedule.Connectivity_SIMs_May__c = a.Connectivity_SIMs_May__c;
								unitSchedule.Connectivity_SIMs_June__c = a.Connectivity_SIMs_June__c;
								unitSchedule.Connectivity_SIMs_July__c = a.Connectivity_SIMs_July__c;
								unitSchedule.Connectivity_SIMs_August__c = a.Connectivity_SIMs_August__c;
								unitSchedule.Connectivity_SIMs_September__c = a.Connectivity_SIMs_September__c;
								unitSchedule.Connectivity_SIMs_October__c = a.Connectivity_SIMs_October__c;
								unitSchedule.Connectivity_SIMs_November__c = a.Connectivity_SIMs_November__c;
								unitSchedule.Connectivity_SIMs_December__c = a.Connectivity_SIMs_December__c;
							}

						}else if (productName == Constants.PRODUCT_TYPE_SERVICE) {
							unitSchedule = new Unit_Schedule__c();
							unitSchedule.Opportunity__c = opp.Id;
							unitSchedule.Year__c = a.Year__c;

							unitSchedule.VAS_Units_January__c = a.Connectivity_SIMs_January__c;
							unitSchedule.VAS_Units_February__c = a.Connectivity_SIMs_February__c;
							unitSchedule.VAS_Units_March__c = a.Connectivity_SIMs_March__c;
							unitSchedule.VAS_Units_April__c = a.Connectivity_SIMs_April__c;
							unitSchedule.VAS_Units_May__c = a.Connectivity_SIMs_May__c;
							unitSchedule.VAS_Units_June__c = a.Connectivity_SIMs_June__c;
							unitSchedule.VAS_Units_July__c = a.Connectivity_SIMs_July__c;
							unitSchedule.VAS_Units_August__c = a.Connectivity_SIMs_August__c;
							unitSchedule.VAS_Units_September__c = a.Connectivity_SIMs_September__c;
							unitSchedule.VAS_Units_October__c = a.Connectivity_SIMs_October__c;
							unitSchedule.VAS_Units_November__c = a.Connectivity_SIMs_November__c;
							unitSchedule.VAS_Units_December__c = a.Connectivity_SIMs_December__c;

						} else if (productName == Constants.PRODUCT_TYPE_HARDWARE) {
							unitSchedule = new Unit_Schedule__c();
							unitSchedule.Opportunity__c = opp.Id;
							unitSchedule.Year__c = a.Year__c;

							unitSchedule.Hardware_Units_January__c = a.Connectivity_SIMs_January__c;
							unitSchedule.Hardware_Units_February__c = a.Connectivity_SIMs_February__c;
							unitSchedule.Hardware_Units_March__c = a.Connectivity_SIMs_March__c;
							unitSchedule.Hardware_Units_April__c = a.Connectivity_SIMs_April__c;
							unitSchedule.Hardware_Units_May__c = a.Connectivity_SIMs_May__c;
							unitSchedule.Hardware_Units_June__c = a.Connectivity_SIMs_June__c;
							unitSchedule.Hardware_Units_July__c = a.Connectivity_SIMs_July__c;
							unitSchedule.Hardware_Units_August__c = a.Connectivity_SIMs_August__c;
							unitSchedule.Hardware_Units_September__c = a.Connectivity_SIMs_September__c;
							unitSchedule.Hardware_Units_October__c = a.Connectivity_SIMs_October__c;
							unitSchedule.Hardware_Units_November__c = a.Connectivity_SIMs_November__c;
							unitSchedule.Hardware_Units_December__c = a.Connectivity_SIMs_December__c;

						}else if (productName != null && (productName.equalsIgnoreCase(Constants.OPPORTUNITY_TYPE_ICAP) )){
							System.debug(a);
							Boolean icapExistsAlready = false;
							System.debug(unitSchedules);
							for (Unit_Schedule__c ucTmp : unitSchedules){
								if (ucTmp != null && ucTmp.Year__c != null && ucTmp.Year__c == a.Year__c){
									icapExistsAlready = true;
									break;
								}
							}
							System.debug(icapExistsAlready);
							if (!icapExistsAlready){
								unitSchedule = new Unit_Schedule__c();
								unitSchedule.Opportunity__c = opp.Id;
								unitSchedule.Year__c = a.Year__c;

								unitSchedule.Connectivity_SIMs_January__c = a.Connectivity_SIMs_January__c;
								unitSchedule.Connectivity_SIMs_February__c = a.Connectivity_SIMs_February__c;
								unitSchedule.Connectivity_SIMs_March__c = a.Connectivity_SIMs_March__c;
								unitSchedule.Connectivity_SIMs_April__c = a.Connectivity_SIMs_April__c;
								unitSchedule.Connectivity_SIMs_May__c = a.Connectivity_SIMs_May__c;
								unitSchedule.Connectivity_SIMs_June__c = a.Connectivity_SIMs_June__c;
								unitSchedule.Connectivity_SIMs_July__c = a.Connectivity_SIMs_July__c;
								unitSchedule.Connectivity_SIMs_August__c = a.Connectivity_SIMs_August__c;
								unitSchedule.Connectivity_SIMs_September__c = a.Connectivity_SIMs_September__c;
								unitSchedule.Connectivity_SIMs_October__c = a.Connectivity_SIMs_October__c;
								unitSchedule.Connectivity_SIMs_November__c = a.Connectivity_SIMs_November__c;
								unitSchedule.Connectivity_SIMs_December__c = a.Connectivity_SIMs_December__c;

								unitSchedule.VAS_Units_January__c = a.Connectivity_SIMs_January__c;
								unitSchedule.VAS_Units_February__c = a.Connectivity_SIMs_February__c;
								unitSchedule.VAS_Units_March__c = a.Connectivity_SIMs_March__c;
								unitSchedule.VAS_Units_April__c = a.Connectivity_SIMs_April__c;
								unitSchedule.VAS_Units_May__c = a.Connectivity_SIMs_May__c;
								unitSchedule.VAS_Units_June__c = a.Connectivity_SIMs_June__c;
								unitSchedule.VAS_Units_July__c = a.Connectivity_SIMs_July__c;
								unitSchedule.VAS_Units_August__c = a.Connectivity_SIMs_August__c;
								unitSchedule.VAS_Units_September__c = a.Connectivity_SIMs_September__c;
								unitSchedule.VAS_Units_October__c = a.Connectivity_SIMs_October__c;
								unitSchedule.VAS_Units_November__c = a.Connectivity_SIMs_November__c;
								unitSchedule.VAS_Units_December__c = a.Connectivity_SIMs_December__c;
							}
							
							/*
							if (unitSchedule.Connectivity_SIMs_January__c == null || unitSchedule.Connectivity_SIMs_January__c == 0)
									unitSchedule.Connectivity_SIMs_January__c = a.Connectivity_SIMs_January__c;
							else
								unitSchedule.Connectivity_SIMs_January__c += a.Connectivity_SIMs_January__c;

							if(unitSchedule.Connectivity_SIMs_February__c == null || unitSchedule.Connectivity_SIMs_February__c == 0)
								unitSchedule.Connectivity_SIMs_February__c = a.Connectivity_SIMs_February__c;
							else
								unitSchedule.Connectivity_SIMs_February__c += a.Connectivity_SIMs_February__c;

							if(unitSchedule.Connectivity_SIMs_March__c == null || unitSchedule.Connectivity_SIMs_March__c == 0) 
								unitSchedule.Connectivity_SIMs_March__c = a.Connectivity_SIMs_March__c;
							else
								unitSchedule.Connectivity_SIMs_March__c += a.Connectivity_SIMs_March__c;

							if(unitSchedule.Connectivity_SIMs_April__c == null || unitSchedule.Connectivity_SIMs_April__c == 0) 
								unitSchedule.Connectivity_SIMs_April__c = a.Connectivity_SIMs_April__c;
							else
								unitSchedule.Connectivity_SIMs_April__c += a.Connectivity_SIMs_April__c;

							if(unitSchedule.Connectivity_SIMs_May__c == null || unitSchedule.Connectivity_SIMs_May__c == 0) 
								unitSchedule.Connectivity_SIMs_May__c = a.Connectivity_SIMs_May__c;
							else
								unitSchedule.Connectivity_SIMs_May__c += a.Connectivity_SIMs_May__c;

							if(unitSchedule.Connectivity_SIMs_June__c == null || unitSchedule.Connectivity_SIMs_June__c == 0) 
								unitSchedule.Connectivity_SIMs_June__c = a.Connectivity_SIMs_June__c;
							else
								unitSchedule.Connectivity_SIMs_June__c += a.Connectivity_SIMs_June__c;

							if(unitSchedule.Connectivity_SIMs_July__c == null || unitSchedule.Connectivity_SIMs_July__c == 0) 
								unitSchedule.Connectivity_SIMs_July__c = a.Connectivity_SIMs_July__c;
							else
								unitSchedule.Connectivity_SIMs_July__c += a.Connectivity_SIMs_July__c;

							if(unitSchedule.Connectivity_SIMs_August__c == null || unitSchedule.Connectivity_SIMs_August__c == 0) 
								unitSchedule.Connectivity_SIMs_August__c = a.Connectivity_SIMs_August__c;
							else
								unitSchedule.Connectivity_SIMs_August__c += a.Connectivity_SIMs_August__c;

							if(unitSchedule.Connectivity_SIMs_September__c == null || unitSchedule.Connectivity_SIMs_September__c == 0) 
								unitSchedule.Connectivity_SIMs_September__c = a.Connectivity_SIMs_September__c;
							else
								unitSchedule.Connectivity_SIMs_September__c += a.Connectivity_SIMs_September__c;

							if(unitSchedule.Connectivity_SIMs_October__c == null || unitSchedule.Connectivity_SIMs_October__c == 0) 
								unitSchedule.Connectivity_SIMs_October__c = a.Connectivity_SIMs_October__c;
							else
								unitSchedule.Connectivity_SIMs_October__c += a.Connectivity_SIMs_October__c;

							if(unitSchedule.Connectivity_SIMs_November__c == null || unitSchedule.Connectivity_SIMs_November__c == 0)
								unitSchedule.Connectivity_SIMs_November__c = a.Connectivity_SIMs_November__c;
							else
								unitSchedule.Connectivity_SIMs_November__c += a.Connectivity_SIMs_November__c;

							if(unitSchedule.Connectivity_SIMs_December__c == null || unitSchedule.Connectivity_SIMs_December__c == 0)
								unitSchedule.Connectivity_SIMs_December__c = a.Connectivity_SIMs_December__c;
							else
								unitSchedule.Connectivity_SIMs_December__c += a.Connectivity_SIMs_December__c;*/

						}

						if (unitSchedule != null)
							unitSchedules.add(unitSchedule);
					}
					}
				}
		
			}


        return unitSchedules;
    }

    public List<Unit_Schedule__c> buildUnitSchedulesNew(List<Opportunity> opportunities, List<cscfga__Product_Configuration__c> productConfigurations, List<cscfga__Attribute__c> quantityAttributes) {
ProductConfigurationService prodConfigService = new ProductConfigurationService();
    List<Unit_Schedule__c> unitSchedules = new List<Unit_Schedule__c>();
    List<Activation_plan__c> activationPlans = [SELECT Id, Connectivity_SIMs_January__c, Connectivity_SIMs_February__c, Connectivity_SIMs_March__c,
																Connectivity_SIMs_April__c, Connectivity_SIMs_May__c, Connectivity_SIMs_June__c,
																Connectivity_SIMs_July__c, Connectivity_SIMs_August__c, Connectivity_SIMs_September__c,
																Connectivity_SIMs_October__c, Connectivity_SIMs_November__c, Connectivity_SIMs_December__c,
																Opportunity__c, Year__c
																FROM Activation_plan__c 
																WHERE Opportunity__c in :opportunities];
	Map<Id, List<cscfga__Product_Configuration__c>> oppSplitConfigurations = prodConfigService.splitConfigurationsByOpportunity(productConfigurations);
			Map<Id, Date> opportunityCloseDates = new Map<Id, Date>();
			for (Opportunity opp : [Select Id, CloseDate from Opportunity where Id in :opportunities]) {
				opportunityCloseDates.put(opp.Id, opp.CloseDate);
			}
    
    for(Opportunity opp : opportunities){
		Date startDate = determineStartDate(opportunityCloseDates.get(opp.Id));
		for(Activation_plan__c a : activationPlans){
		    System.debug('activation plan ' + a);
		    Unit_Schedule__c unitSchedule = new Unit_Schedule__c();
		    unitSchedule.Opportunity__c = opp.Id;
			unitSchedule.Year__c = a.Year__c;
			
			for (cscfga__Product_Configuration__c prodConfig : oppSplitConfigurations.get(opp.Id)) {
			    String productName = prodConfig.Original_Product_Name__c;
			    system.debug(productName + '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
                System.debug(prodConfig);
			    
			    if (productName == Constants.PRODUCT_TYPE_FASTLANE || productName == 'FLO Connectivity') {

					unitSchedule.Connectivity_SIMs_January__c = a.Connectivity_SIMs_January__c;
					unitSchedule.Connectivity_SIMs_February__c = a.Connectivity_SIMs_February__c;
					unitSchedule.Connectivity_SIMs_March__c = a.Connectivity_SIMs_March__c;
					unitSchedule.Connectivity_SIMs_April__c = a.Connectivity_SIMs_April__c;
					unitSchedule.Connectivity_SIMs_May__c = a.Connectivity_SIMs_May__c;
					unitSchedule.Connectivity_SIMs_June__c = a.Connectivity_SIMs_June__c;
					unitSchedule.Connectivity_SIMs_July__c = a.Connectivity_SIMs_July__c;
					unitSchedule.Connectivity_SIMs_August__c = a.Connectivity_SIMs_August__c;
					unitSchedule.Connectivity_SIMs_September__c = a.Connectivity_SIMs_September__c;
					unitSchedule.Connectivity_SIMs_October__c = a.Connectivity_SIMs_October__c;
					unitSchedule.Connectivity_SIMs_November__c = a.Connectivity_SIMs_November__c;
					unitSchedule.Connectivity_SIMs_December__c = a.Connectivity_SIMs_December__c;

				} else if (productName == Constants.PRODUCT_TYPE_CONNECTIVITY) {
					unitSchedule.Connectivity_SIMs_January__c = a.Connectivity_SIMs_January__c;
					unitSchedule.Connectivity_SIMs_February__c = a.Connectivity_SIMs_February__c;
					unitSchedule.Connectivity_SIMs_March__c = a.Connectivity_SIMs_March__c;
					unitSchedule.Connectivity_SIMs_April__c = a.Connectivity_SIMs_April__c;
					unitSchedule.Connectivity_SIMs_May__c = a.Connectivity_SIMs_May__c;
					unitSchedule.Connectivity_SIMs_June__c = a.Connectivity_SIMs_June__c;
					unitSchedule.Connectivity_SIMs_July__c = a.Connectivity_SIMs_July__c;
					unitSchedule.Connectivity_SIMs_August__c = a.Connectivity_SIMs_August__c;
					unitSchedule.Connectivity_SIMs_September__c = a.Connectivity_SIMs_September__c;
					unitSchedule.Connectivity_SIMs_October__c = a.Connectivity_SIMs_October__c;
					unitSchedule.Connectivity_SIMs_November__c = a.Connectivity_SIMs_November__c;
					unitSchedule.Connectivity_SIMs_December__c = a.Connectivity_SIMs_December__c;
				
				    
				}else if (productName == Constants.PRODUCT_TYPE_SERVICE) {
					unitSchedule.VAS_Units_January__c = a.Connectivity_SIMs_January__c;
					unitSchedule.VAS_Units_February__c = a.Connectivity_SIMs_February__c;
					unitSchedule.VAS_Units_March__c = a.Connectivity_SIMs_March__c;
					unitSchedule.VAS_Units_April__c = a.Connectivity_SIMs_April__c;
					unitSchedule.VAS_Units_May__c = a.Connectivity_SIMs_May__c;
					unitSchedule.VAS_Units_June__c = a.Connectivity_SIMs_June__c;
					unitSchedule.VAS_Units_July__c = a.Connectivity_SIMs_July__c;
					unitSchedule.VAS_Units_August__c = a.Connectivity_SIMs_August__c;
					unitSchedule.VAS_Units_September__c = a.Connectivity_SIMs_September__c;
					unitSchedule.VAS_Units_October__c = a.Connectivity_SIMs_October__c;
					unitSchedule.VAS_Units_November__c = a.Connectivity_SIMs_November__c;
					unitSchedule.VAS_Units_December__c = a.Connectivity_SIMs_December__c;
				
				    
				} else if (productName == Constants.PRODUCT_TYPE_HARDWARE) {
					unitSchedule.Hardware_Units_January__c = a.Connectivity_SIMs_January__c;
					unitSchedule.Hardware_Units_February__c = a.Connectivity_SIMs_February__c;
					unitSchedule.Hardware_Units_March__c = a.Connectivity_SIMs_March__c;
					unitSchedule.Hardware_Units_April__c = a.Connectivity_SIMs_April__c;
					unitSchedule.Hardware_Units_May__c = a.Connectivity_SIMs_May__c;
					unitSchedule.Hardware_Units_June__c = a.Connectivity_SIMs_June__c;
					unitSchedule.Hardware_Units_July__c = a.Connectivity_SIMs_July__c;
					unitSchedule.Hardware_Units_August__c = a.Connectivity_SIMs_August__c;
					unitSchedule.Hardware_Units_September__c = a.Connectivity_SIMs_September__c;
					unitSchedule.Hardware_Units_October__c = a.Connectivity_SIMs_October__c;
					unitSchedule.Hardware_Units_November__c = a.Connectivity_SIMs_November__c;
					unitSchedule.Hardware_Units_December__c = a.Connectivity_SIMs_December__c;
				}
			    
			}
			
		    unitSchedules.add(unitSchedule);
		}
    }
    
    return unitSchedules;
}
    
    private List<Unit_Schedule__c> initializeUnitSchedules(Opportunity opp, Date startDate) {
    	
    	List<Unit_Schedule__c> unitSchedules = new List<Unit_Schedule__c>();

        ContractTerm simContractTerm = new ContractTerm(startDate, opp.Contract_Term_Per_SIM__c);
        ContractTerm servicesContractTerm = new ContractTerm(startDate, opp.Contract_Term_Per_Subscription__c);
        ContractTerm hardwareContractTerm = new ContractTerm(startDate, opp.Contract_Term_Per_Hardware__c);
        
        Integer totalYears = max(new Set<Integer>{ simContractTerm.getTotalYears(), servicesContractTerm.getTotalYears(), hardwareContractTerm.getTotalYears()} );
        
        for (Integer year = 0; year < totalYears; year++) {
            unitSchedules.add(new Unit_Schedule__c(
                Year__c = String.valueOf(startDate.year() + year),
                Opportunity__c = opp.Id
            ));
        }
        return unitSchedules;
    }
    
    private Integer max(Set<Integer> numbers) {
    	Integer max = 0;
		for (Integer num : numbers) {
			if (num != null && num > max) {
				max = num;
			}
		}
		return max;
    }
    
    private class ContractTerm {

		Integer term;
		Date startDate;
		Date endDate;
	
		public ContractTerm(Date startDate, Decimal contractMonths) {
			this.startDate = startDate;
			this.term = (Integer) contractMonths;
			if (term == null) {
				term = 0;
			}
			this.endDate = startDate.addMonths(term);
		}
	
		public Integer getMonths() {
			return term;
		}
	
		public Integer getTotalYears() {
			return endDate.year() - startDate.year() + 1;
		}
		
		public Date getStartDate() {
			return startDate;
		}
    }
}