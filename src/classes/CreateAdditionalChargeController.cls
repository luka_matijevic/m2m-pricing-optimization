public with sharing class CreateAdditionalChargeController
{
    ApexPages.StandardController controller;
	public String errorMessage {get; set;}
    private String daily_bill_id;
    
	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	public CreateAdditionalChargeController(ApexPages.StandardController controller)
	{
		this.controller = controller;
		daily_bill_id = ApexPages.currentPage().getParameters().get('daily_bill_id');
		Daily_Bill_Item__c dbo = (Daily_Bill_Item__c)controller.getRecord();
        dbo.Anweisung__c = getAnweisungText();
	}
    
    public PageReference createDBFItem()
    { 
        
        try{
            Daily_Bill_Item__c dbo = (Daily_Bill_Item__c)controller.getRecord();
            
            if(dbo.Payment_Type__c == 'One Time' && dbo.Number_of_months__c != null){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'Please clear Number Of Months, it is not applicable if payemtn is One Time');
                ApexPages.addMessage(myMsg);
                return null;
            }else if(dbo.Payment_Type__c == 'Recurring' && dbo.Number_of_months__c == null){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'Please enter Number of months');
                ApexPages.addMessage(myMsg);
                return null;
            }
            
            if (dbo.Payment_Type__c == 'Recurring'){
                if(!createRecurringAdditionalCharge()){
                    return null;
                }
            }else{
                //One time
                dbo.Daily_Bill__c = daily_bill_id;
                List<Billing_Account__c> ba = [select id,OFI_ID__c from billing_account__c where Id = :dbo.Billing_Account__c];
                if(!ba.isEmpty() && dbo.Billing_Account__c!=null)
                {
                    dbo.VO_Nr__c =ba[0].OFI_ID__c;
                }
                
                dbo.I_CO__c='';
                dbo.Konto__c='4120100';
                dbo.kost__c='465605';
                dbo.prod__c='387654';
                dbo.Waehrung__c='EUR';
                 
                if (dbo.tax_code__c =='19%') dbo.Steuersatz__c=19; 
                else dbo.Steuersatz__c=0;
                
                dbo.Einzel_Nettobetrag__c = dbo.Betrag__c;
                
                
                    if (dbo.Steuersatz__c<>0) {
                    dbo.Einzel_Taxbetrag__c = (dbo.Betrag__c * (dbo.Steuersatz__c/100)).setScale(2,RoundingMode.HALF_UP); 
                    dbo.Einzel_Bruttobetrag__c = (dbo.Betrag__c * (1 + (dbo.Steuersatz__c/100))).setScale(2,RoundingMode.HALF_UP); 
                    }
                    else {
                        dbo.Einzel_Taxbetrag__c = dbo.Betrag__c * 0; 
                        dbo.Einzel_Bruttobetrag__c = dbo.Betrag__c ;
                    }
                    dbo.Gesamt_Bruttobetrag__c = (dbo.Einzel_Bruttobetrag__c * dbo.Menge__c).setScale(2,RoundingMode.HALF_UP);
                    dbo.Gesamt_Nettobetrag__c = (dbo.Einzel_Nettobetrag__c * dbo.Menge__c).setScale(2,RoundingMode.HALF_UP);
                    dbo.Gesamt_Taxbetrag__c = (dbo.Einzel_Taxbetrag__c * dbo.Menge__c).setScale(2,RoundingMode.HALF_UP);
                if(dbo.Payment_Type__c == 'Recurring'){
                    dbo.Number_of_months__c = null;
                }
                controller.save();
                system.debug(controller.getId());
            }
        }catch(exception ex){
            system.debug('Exception  '+ex.getMessage());
            return null;
        }

        PageReference newocp = new PageReference('/apex/billingconsole?daily_bill=' + daily_bill_id);
        newocp.setRedirect(true);
        
        return newocp;
    }
    
    public PageReference cancelItem() {
         PageReference newocp = new PageReference('/apex/billingconsole?daily_bill=' + daily_bill_id);
		newocp.setRedirect(true);
		
		return newocp;
    }
        public boolean showNoOfMonths {get; set;}
    public pagereference refreshNumberOfMonths(){
        Daily_Bill_Item__c dbo = (Daily_Bill_Item__c)controller.getRecord();
        system.debug('dbo.Payment_Type__c   '+dbo.Payment_Type__c);
        if(dbo.Payment_Type__c == 'Recurring'){
            showNoOfMonths = true;
        }else{
            showNoOfMonths = false;
        }
        return null;
    }

    //Create additional recurring charge
    private Boolean createRecurringAdditionalCharge(){
        Boolean recuringOk = true;
        Daily_Bill_Item__c dbo = (Daily_Bill_Item__c)controller.getRecord();
        if (dbo != null){
            CreateAdditionalRecurringCharge carc = new CreateAdditionalRecurringCharge();
            carc.CreateAdditionalRecurringChargeForDbi(dbo, daily_bill_id);

            if (!carc.insertOk){
                recuringOk = false;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, carc.message);
                ApexPages.addMessage(myMsg);
            }
        }
        else{
            recuringOk = false;
        }
        return recuringOk;
    }

    private String getAnweisungText(){
        String text = 'Abrechnungszeitraum ';// 01.06.2016 bis 30.06.2016
        Date dbDate = getDailyBillDate();
        Date firstDay = null;
        Date lastDay = null;
        if (dbDate != null){
            firstDay = Date.newInstance(dbDate.year(), dbDate.month(), 1);
            Integer numberOfDays = Date.daysInMonth(dbDate.year(), dbDate.month());
            lastDay = Date.newInstance(dbDate.year(), dbDate.month(), numberOfDays);
        }

        if (firstDay != null && lastDay != null)
            text = text + String.valueOf(firstDay.format()) + ' bis ' + String.valueOf(lastDay.format());

        return text;
    }

    private Date getDailyBillDate(){
        Date retDate = null;
        List<daily_bill__c> tmpList = [SELECT id, Date__c from daily_bill__c where id = :daily_bill_id];
        if (tmpList != null && tmpList.size() > 0)
            retDate = Date.newInstance(tmpList[0].Date__c.year(), tmpList[0].Date__c.month(), tmpList[0].Date__c.day());
        return retDate;
    }
}