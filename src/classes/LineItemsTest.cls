@isTest
public class LineItemsTest {

	static testMethod void testBuildingPrebills() {

		Account acc = new Account(
			Name = 'Test account',
			BillingCity = 'Bremen',
			BillingCountry = 'Germany',
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße',
			Type = 'Customer',
			VAT__c = 1,
            Global__c = true
		);
		insert acc;

		Billing_Account__c billingAccount = new Billing_Account__c(
			Payer__c = acc.Id,
			VAT__c = '1',
            SM2M_Type__c = 'Global'
		);
		insert billingAccount;

		csord__Order_Request__c orderRequest = new csord__Order_Request__c(
			csord__Module_Name__c = 'test',
			csord__Module_Version__c = 'test'
		);
		insert orderRequest;

		csord__Subscription__c subscription = new csord__Subscription__c(
			Billing_Account__c = billingAccount.Id,
			csord__Identification__c = 'Id',
			csord__Order_Request__c = orderRequest.Id,
			csord__Status__c = 'Test'
		);
		insert subscription;

		csord__Service__c
		service = new csord__Service__c(
			csord__Subscription__c = subscription.Id,
			Billing_Rate_Model__c = 'One Off',
			csord__Identification__c = 'Test',
			csord__Order_Request__c = orderRequest.Id,
			csord__Status__c = 'Test'
		);
		insert service;

		Journal_Mapping__c jm = new Journal_Mapping__c();
		insert jm;

		csord__Service_Line_Item__c sli = new csord__Service_Line_Item__c(
			csord__Service__c = service.Id,
			csord__Identification__c = 'Test',
			csord__Order_Request__c = orderRequest.Id,
			Journal_Mapping__c = jm.Id
		);
		insert sli;

		sli = [
			SELECT
				Id, Name, csord__Service__r.csord__Subscription__r.Billing_Account__c, csord__Is_Recurring__c,
				csord__Service__r.csord__Deactivation_Date__c, Journal_Mapping__c, Quantity__c, Total_Price__c, csord__Service__r.Billing_Rate_Model__c
			FROM
				csord__Service_Line_Item__c
			limit 1
		];

		/*Prebill__c prebill = LineItems.buildPrebill(sli, Date.newInstance(2013,2,1));
		system.assertEquals('One Off', prebill.Billing_Rate_Model__c);
		system.assertEquals('Prebill for 2013-02-01', prebill.Name);
		system.assertEquals('EUR', prebill.Currency_Code__c);
		system.assertEquals(billingAccount.Id, prebill.Billing_Account__c);*/
	}

	static testMethod void testBillingRateModel() {

		Account acc = new Account(
			Name = 'Test account',
			BillingCity = 'Bremen',
			BillingCountry = 'Germany',
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße',
			Type = 'Customer',
			VAT__c = 1,
            Global__c = true
		);
		insert acc;

		Billing_Account__c billingAccount = new Billing_Account__c(
			Payer__c = acc.Id,
			VAT__c  = '1',
            SM2M_Type__c = 'Global'
		);
		insert billingAccount;

		csord__Order_Request__c orderRequest = new csord__Order_Request__c(
			csord__Module_Name__c = 'test',
			csord__Module_Version__c = 'test'
		);
		insert orderRequest;

		csord__Subscription__c subscription = new csord__Subscription__c(
			Billing_Account__c = billingAccount.Id,
			csord__Identification__c = 'Id',
			csord__Order_Request__c = orderRequest.Id,
			csord__Status__c = 'Test'
		);
		insert subscription;

		csord__Service__c
		service = new csord__Service__c(
			csord__Subscription__c = subscription.Id,
			Billing_Rate_Model__c = 'One Off',
			csord__Identification__c = 'Test',
			csord__Order_Request__c = orderRequest.Id,
			csord__Status__c = 'Test'
		);
		insert service;

		Journal_Mapping__c jm = new Journal_Mapping__c();
		insert jm;

		csord__Service_Line_Item__c sli = new csord__Service_Line_Item__c(
			csord__Service__c = service.Id,
			csord__Identification__c = 'Test',
			csord__Order_Request__c = orderRequest.Id,
			Journal_Mapping__c = jm.Id
		);
		insert sli;

		sli = [
			SELECT
				Id, Name, csord__Service__r.csord__Subscription__r.Billing_Account__c, csord__Is_Recurring__c,
				csord__Service__r.csord__Deactivation_Date__c, Journal_Mapping__c, Quantity__c, Total_Price__c, csord__Service__r.Billing_Rate_Model__c
			FROM
				csord__Service_Line_Item__c
			limit 1
		];

		//system.assertEquals(BillingRateModel.ONEOFF, LineItems.billingRateModel(sli));
	}
}