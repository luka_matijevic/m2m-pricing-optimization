@isTest
public class StockSynchronizerTest {
	
	static testmethod void testUpdateSim() {
		
		Stock__c unsynchronizedStock = new Stock__c(
			ICCID__c = '1111111111111111111',
			IMSI__c = 'imsi',
			MSISDN__c = 'msisdn',
			SM2M_Status__c = 'status',
			State_Trans_Manual__c = true,
			State_Change_Date__c = Date.newInstance(2014,2,1),
			State_Change_Reason__c = 'reason'
		);
		
		Stock__c currentStock = new Stock__c(
			ICCID__c = '1111111111111111111',
			IMSI__c = 'old-imsi',
			MSISDN__c = 'old-msisdn',
			SM2M_Status__c = 'old-status',
			State_Trans_Manual__c = false,
			State_Change_Date__c = Date.newInstance(2014,1,1),
			State_Change_Reason__c = 'old-reason'
		);
		
		insert currentStock;
		
		StockSynchronizationResult result = new StockSynchronizer().synchronize(new List<Stock__c>{unsynchronizedStock});
		system.assertEquals(1, result.StocksToUpdate.size());
		system.assertEquals(0, result.StocksToInsert.size());
		
		Stock__c stock = result.StocksToUpdate.get(0);
		
		system.assertEquals('1111111111111111111', stock.ICCID__c);
		system.assertEquals('imsi', stock.IMSI__c);
		system.assertEquals('msisdn', stock.MSISDN__c);
		system.assertEquals('status', stock.SM2M_Status__c);
		system.assertEquals(true, stock.State_Trans_Manual__c);
		system.assertEquals(Date.newInstance(2014,2,1), stock.State_Change_Date__c);
		system.assertEquals('reason', stock.State_Change_Reason__c);
		
		system.assertEquals(currentStock.Id, stock.Id);
	}
	
	static testmethod void testInsertSim() {
		
		Stock__c unsynchronizedStock = new Stock__c(
			ICCID__c = '1111111111111111111',
			IMSI__c = 'imsi',
			MSISDN__c = 'msisdn',
			SM2M_Status__c = 'status',
			State_Trans_Manual__c = true,
			State_Change_Date__c = Date.newInstance(2014,2,1),
			State_Change_Reason__c = 'reason'
		);
		
		Stock__c currentStock = new Stock__c(
			ICCID__c = '2222222222222222222',
			IMSI__c = 'old-imsi',
			MSISDN__c = 'old-msisdn',
			SM2M_Status__c = 'old-status',
			State_Trans_Manual__c = false,
			State_Change_Date__c = Date.newInstance(2014,1,1),
			State_Change_Reason__c = 'old-reason'
		);
		
		insert currentStock;
		
		StockSynchronizationResult result = new StockSynchronizer().synchronize(new List<Stock__c>{unsynchronizedStock});
		system.assertEquals(1, result.StocksToInsert.size());
		system.assertEquals(0, result.StocksToUpdate.size());
		
		Stock__c stock = result.StocksToInsert.get(0);
		
		system.assertEquals('1111111111111111111', stock.ICCID__c);
		system.assertEquals('imsi', stock.IMSI__c);
		system.assertEquals('msisdn', stock.MSISDN__c);
		system.assertEquals('status', stock.SM2M_Status__c);
		system.assertEquals(true, stock.State_Trans_Manual__c);
		system.assertEquals(Date.newInstance(2014,2,1), stock.State_Change_Date__c);
		system.assertEquals('reason', stock.State_Change_Reason__c);
		
		insert result.StocksToInsert;
		
		List<Stock__c> updatedStock = [Select Id from Stock__c];
		
		system.assertEquals(2, updatedStock.size());
	}
	
}