public without sharing class RateCard implements IRateCard
{
    //--------------------------------------------------------------------------
    // Private members
    //--------------------------------------------------------------------------
    Rate_Card__c rc;
    List<IRateCardCountryManagement> rateCardCountriesManagement;
    List<SelectOption> availableCountries;

    //--------------------------------------------------------------------------
    // Constructor
    //--------------------------------------------------------------------------
    public RateCard(Rate_Card__c rc, List<IRateCardCountryManagement> rateCardCountriesManagement, List<SelectOption> availableCountries)
    {
        this.rc = rc;
        this.rateCardCountriesManagement = rateCardCountriesManagement;
        this.availableCountries = availableCountries;
        
        if (this.rateCardCountriesManagement == null)
		{
			this.rateCardCountriesManagement = new List<IRateCardCountryManagement>();
		}
		
		if (this.availableCountries == null)
		{
			this.availableCountries = new List<SelectOption>();
		}
    }
    
    /*public RateCard(Rate_Card__c rc)
    {
        this.rc = rc;
    }*/

    //--------------------------------------------------------------------------
    // Getters / setters
    //--------------------------------------------------------------------------
    public Rate_Card__c getRateCard()
    {
        return rc;
    }

    public List<IRateCardCountryManagement> getRateCardCountriesManagement()
    {
        return rateCardCountriesManagement;
    }
    
    public List<SelectOption> getAvailableCountries()
    {
        return availableCountries;
    }

    public Id getRateCardId()
    {
        return rc.Rate_Card_Id__c;
    }

    public Boolean getShowOnQuote()
    {
        return rc.Show_on_Quote__c;
    }

    public void setShowOnQuote(Boolean showOnQuote)
    {
        rc.Show_On_Quote__c = showOnQuote;
    }

    public String getName()
    {
    	return rc.Name;
    }

    public void setName(String name)
    {
    	rc.Name = name;
    }

    public String getClockRate()
    {
    	return rc.Clock_Rate__c;
    }

    public void setClockRate(String clockRate){
    	rc.Clock_Rate__c = clockRate;
    }

    public Decimal getBaseUsageFee()
    {
    	return rc.Base_Usage_Fee__c;
    }

    public void setBaseUsageFee(Decimal buf)
    {
        rc.Base_Usage_Fee__c = buf;
    }

    public Decimal getTargetUsageFee()
    {
    	return rc.Target_Usage_Fee__c;
    }

    public void setTargetUsageFee(Decimal tuf)
    {
    	rc.Target_Usage_Fee__c = tuf;
    }

    public Decimal getExpectedUsage()
    {
    	return rc.Expected_Usage__c;
    }

    public void setExpectedUsage(Decimal expectedUsage)
    {
    	rc.Expected_Usage__c = expectedUsage;
    }

    public Boolean getCanDiscount()
    {
    	return rc.Can_Discount__c;
    }

    public void setCanDiscount(Boolean canDiscount)
    {
        rc.Can_Discount__c = canDiscount;
    }

    public Boolean getIsTargetFeeDiscounted()
    {
    	return rc.Base_Usage_Fee__c != rc.Target_Usage_Fee__c;
    }

    public Decimal getEditedUsageFee()
    {
    	return rc.Edited_Fee__c;
    }

    public void setEditedUsageFee(Decimal buf)
    {
    	rc.Edited_Fee__c = buf;
    }

    //--------------------------------------------------------------------------
    // Clone rate card
    //--------------------------------------------------------------------------
    public IRateCard deepClone()
    {
        return new RateCard(rc.clone(), rateCardCountriesManagement, availableCountries);
    }

    //--------------------------------------------------------------------------
    // Compare two rate cards
    //--------------------------------------------------------------------------
    public Boolean equals(IRateCard rateCard)
    {
        //
        // Compare Rate__Card__c
        //
        if (getRateCard().Show_on_Quote__c != rateCard.getRateCard().Show_on_Quote__c
        	|| getRateCard().Expected_Usage__c != rateCard.getRateCard().Expected_Usage__c
        	|| getRateCard().Target_Usage_Fee__c != rateCard.getRateCard().Target_Usage_Fee__c)
        {
            return false;
        }
        return equalsWithoutConfiguration(rateCard);
    }

    //--------------------------------------------------------------------------
    // Compare two rate cards without configuration changes
    //--------------------------------------------------------------------------
    public Boolean equalsWithoutConfiguration(IRateCard rateCard)
    {
        //
        // Copare Rate_Card__c without configuration fields
        //
        if (getRateCard().Name != rateCard.getRateCard().Name
            || getRateCard().Base_Usage_Fee__c != rateCard.getRateCard().Base_Usage_Fee__c
            || getRateCard().Can_Discount__c != rateCard.getRateCard().Can_Discount__c
            || getRateCard().Clock_Rate__c != rateCard.getRateCard().Clock_Rate__c
            || getRateCard().Description_DE__c != rateCard.getRateCard().Description_DE__c
            || getRateCard().Description_ENG__c != rateCard.getRateCard().Description_ENG__c
            || getRateCard().Direction__c != rateCard.getRateCard().Direction__c
            || getRateCard().Sequence_No__c != rateCard.getRateCard().Sequence_No__c
            || getRateCard().Worldzone__c != rateCard.getRateCard().Worldzone__c)
        {
            return false;
        }
        return true;
    }
}