/*
* Description: Test class for  ManagerView Controller class
* Created: 28-Oct-2014
*/
@isTest(seeAllData=false)
public class ManagerViewControllerTest{

    /*
    *   Test method for CaseViews & chart
    */
    public static testMethod void testManagerCaseView() {
        
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers;
        
        Account acc = new Account(Company_Email__c = 'company@email.com',
        Unique_Business_Number__c = 'UBN 123',
        BillingCountry = 'Ireland',
        BillingStreet = 'Warwick street 15',
        BillingCity = 'Cork',
        Jurisdiction__c = 'Jurisdiction',
        VO_Number__c = '4567898',
        VAT__c = 12.50,
        Phone = '12345',
        Type = 'Customer',
        BillingPostalCode = '57000',
        Sales_Employee__c = 'John Voight',
        Frame_Contract__c = false,
        Website = 'www',
        Name = 'CS Service Test Acct'
        );
        insert acc;
        
        List<Case> caseList = new List<Case>();
        for(Integer i=0; i<30; i++){
            String priority = '';
            String sta ='';
            boolean flag = true;
            if(i<10){
                priority= 'High';
                sta = 'Open';
            } 
            if(i>=10 && i<20){
                priority= 'Medium';
                sta = 'In Progress';
            } 
            if(i>=20 && i<30){
                priority= 'Low';
                sta = 'Open';
            }
            if(i>=25 && i<30){
                flag=false;
            }
             caseList.add(new Case(Status=sta, Subject='Test Demo'+i, SLA_breached_checkbox__c=flag, SLA_Time__c='24', Type='Standard', Origin='Email', Priority=priority, AccountId=acc.Id));
        }     
        insert caseList;           
                
        notriggers.Flag__c = false;
        update notriggers;
        
        Test.startTest();
            PageReference pageRefr = Page.ManagerView;
            Test.setCurrentPageReference(pageRefr); 
            ManagerViewController ctrl = new ManagerViewController (new ApexPages.StandardController(new Case()));
            system.assert(ctrl.openCaseList.size()>0);
                       
            ctrl.selectedOption='Priority 1';
            ctrl.applyFilters();
            system.assert(ctrl.openCaseList.size()==10);
            
            ctrl.selectedOption='Priority 2';
            ctrl.applyFilters();
            system.assert(ctrl.openCaseList.size()==10);
            
            ctrl.selectedOption='Priority 3';
            ctrl.applyFilters();
            system.assert(ctrl.openCaseList.size()==10);
            
            ctrl.selectedOption='Unassigned';
            ctrl.applyFilters();
            system.assert(ctrl.openCaseList.size()==10);
            
            ctrl.selectedOption='24h-P1 breached';
            ctrl.applyFilters();
            system.assert(ctrl.openCaseList.size()>0);
            
            ctrl.selectedOption='24h-P2 or P3 breached';
            ctrl.applyFilters();
            system.assert(ctrl.openCaseList.size()>0);
            
            ctrl.selectedOption='24h-No SLA breached';
            ctrl.applyFilters();
            system.assert(ctrl.openCaseList.size()>0);
            
            ctrl.selectedOption='CW-P1 breached';
            ctrl.applyFilters();
            system.assert(ctrl.openCaseList.size()>0);
            
            ctrl.selectedOption='CW-P2 or P3 breached';
            ctrl.applyFilters();
            system.assert(ctrl.openCaseList.size()>0);
            
            ctrl.selectedOption='CW-No SLA breached';
            ctrl.applyFilters();
            system.assert(ctrl.openCaseList.size()>0);
            
            ctrl.selectedOption='SLA breach 24h';
            ctrl.applyFilters();
            system.assert(ctrl.openCaseList.size()>0);
            
            ctrl.selectedOption='SLA breach current week';
            ctrl.applyFilters();
            system.assert(ctrl.openCaseList.size()>0);
            
            ctrl.selectedOption='';
            ctrl.applyFilters();
            system.assert(ctrl.openCaseList.size()==30);
            
        Test.stopTest();    
        
    }
}