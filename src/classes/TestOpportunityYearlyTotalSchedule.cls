@isTest
private class TestOpportunityYearlyTotalSchedule {

	static CronTrigger fetchCronTrigger(String jobId) {
		CronTrigger cron = [
			SELECT
				Id, CronExpression, TimesTriggered, TimeZoneSidKey, NextFireTime
			FROM
				CronTrigger
			WHERE
				Id = :jobId
		];
		return cron;
	}
/*
    static testMethod void testOpportunityYearlyTotalSchedule() {

        list<Profile> profiles = new list<profile>([select id, name from profile where name = 'M2M Standard User']);
        
        User standardUsr = Test_Util.createUser(profiles[0].id, 'testuser@m2m.com', 'alias','lnameee');
        insert standardUsr;
        
        Id devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Fast Lane Standard Pricing').getRecordTypeId();
        
    	Account account = new Account(
    		Name = 'Test',
        	Type = 'Business'
        );

        insert account;
        
        list<case> csLst = new list<case>();
        csLst.add(Test_Util.createCase('Service','Complaints','Open',account.id, 'Test Demo111', '24', 'Standard', 'Email','High', 'test@t.com'));
        csLst.add(Test_Util.createCase('Service','Complaints','Open',account.id, 'Test Demo111', '24', 'Standard', 'Email','High', 'test@t.com'));
        
        insert csLst;
        

    	Double monthlyRevenue = 1.0;
        
        List<Opportunity> oppLst = new list<Opportunity>();
        
        Opportunity opp = Test_Util.createOpportynity('Test-1',account.Id,date.valueOf(system.today()),'Closed Won','Price',12,1);
        Opportunity opp1 = Test_Util.createOpportynity('Test-1',account.Id,date.valueOf(system.today()),'Closed Won','Price',12,1);
        Opportunity opp2 = Test_Util.createOpportynity('Test-1',account.Id,date.valueOf(system.today()),'New','Price',12,1);
        
        
        oppLst.add(opp);
        oppLst.add(opp1);
        oppLst.add(opp2);
        
        insert oppLst;
        
        Boni_Check__c boniCheck = new Boni_Check__c(Status_of_CC__c='Accepted');
        insert boniCheck;
        
        Order_Request__c oreq = new Order_Request__c(Boni_Check__c = boniCheck.id,Opportunity__c =opp2.id);
        insert oreq;
        
        opp2.ownerId = standardUsr.id;
    	opp2.stageName ='Closed Won';
    	
    	if(devRecordTypeId != null)
    	opp2.recordTypeId = devRecordTypeId;

        
        update opp2;
    
    	cscfga__Product_Bundle__c bundle = new cscfga__Product_Bundle__c(
    		Name = 'Test 1',
    		cscfga__Synchronised_with_Opportunity__c = true,
    		cscfga__Opportunity__c = opp.Id
    	);

    	insert bundle;

    	Integer monthlyAmount = 10;

    	Unit_Schedule__c schedule = new Unit_Schedule__c(
    		Opportunity__c = opp.Id,
    		Year__c = '2014',
    		Connectivity_Sims_January__c 	= monthlyAmount,
			Connectivity_Sims_February__c 	= monthlyAmount,
			Connectivity_Sims_March__c 		= monthlyAmount,
			Connectivity_Sims_April__c		= monthlyAmount,
			Connectivity_Sims_May__c 		= monthlyAmount,
			Connectivity_Sims_June__c 		= monthlyAmount,
			Connectivity_Sims_July__c 		= monthlyAmount,
			Connectivity_Sims_August__c 	= monthlyAmount,
			Connectivity_Sims_September__c 	= monthlyAmount,
			Connectivity_Sims_October__c 	= monthlyAmount,
			Connectivity_Sims_November__c 	= monthlyAmount,
			Connectivity_Sims_December__c 	= monthlyAmount
    	);

    	insert schedule;

    	opp.Total_Connectivity_Rev_in_current_year__c = 0.0;
    	opp.Total_Service_Revenue_in_current_year__c = 0.0;

    	update opp;

        Test.startTest();

        String jobId = System.schedule(
            'Unit test job',
            OpportunityYearlyTotalSchedule.CRON_EXP,
            new OpportunityYearlyTotalSchedule()
        );

        CronTrigger ct = fetchCronTrigger(jobId);

    
        OpportunityYearlyTotalSchedule.scheduleIt();


        // Sanity checks
        System.assertEquals(OpportunityYearlyTotalSchedule.CRON_EXP, ct.CronExpression, 'Did not get the same Cron Expression back');
        System.assertEquals(0, ct.TimesTriggered, 'The job has been run and should not have');

		// Is the fire time correct?
		Datetime nextFireTime = Datetime.newInstanceGmt(Date.today().year() + 1, 1, 1);
		System.assertEquals(nextFireTime.format('yyyy-MM-dd HH:mm:ss', 'GMT'), ct.NextFireTime.format('yyyy-MM-dd HH:mm:ss', ct.TimeZoneSidKey));

        List<Opportunity> opportunities = [SELECT Id, Total_Connectivity_Rev_in_current_year__c, Total_Service_Revenue_in_current_year__c FROM Opportunity];

        for (Opportunity opportunity : opportunities) {
        	//System.assertEquals(0.0, opportunity.Total_Connectivity_Rev_in_current_year__c);
        	//System.assertEquals(0.0, opportunity.Total_Service_Revenue_in_current_year__c);
        }

        SchedulableContext SC;
        OpportunityYearlyTotalSchedule yts = new OpportunityYearlyTotalSchedule();
        yts.execute(SC);
        Test.stopTest();
 
        Opportunity changedOpportunity = [SELECT Id, Total_Connectivity_Rev_in_current_year__c FROM Opportunity o WHERE o.Id = :opp.Id];

        System.assertEquals(0.0, opp.Total_Connectivity_Rev_in_current_year__c);
        System.assertNotEquals(0.0, changedOpportunity.Total_Connectivity_Rev_in_current_year__c);
        
        delete oppLst;
    } */
}