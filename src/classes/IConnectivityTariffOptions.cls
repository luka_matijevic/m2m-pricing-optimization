public interface IConnectivityTariffOptions
{
	//--------------------------------------------------------------------------
	// Get all tariff options sorted by type and sequence
	//--------------------------------------------------------------------------
	List<ITariffOption> getTariffOptions();

	//--------------------------------------------------------------------------
	// Get included tariff options sorted by sequence
	//--------------------------------------------------------------------------
	List<ITariffOption> getIncludedTariffOptions();

	//--------------------------------------------------------------------------
	// Get additional tariff options sorted by sequence
	//--------------------------------------------------------------------------
	List<ITariffOption> getAdditionalTariffOptions();

	//--------------------------------------------------------------------------
	// Get supplementary service tariff options sorted by sequence
	//--------------------------------------------------------------------------
	List<ITariffOption> getSupplementaryServiceTariffOptions();

    Id storeTariff();
	//--------------------------------------------------------------------------
	// Clone tariff with all selected tariff options and configurations
	//--------------------------------------------------------------------------
	Id cloneTariff(String contractTerm);

	//--------------------------------------------------------------------------
	// Create product configuratins for additiona and supplementary service
	// Returns:
	//		Unique key that is part of craeted product configurations
	//--------------------------------------------------------------------------
	String createConfigurations();

	//--------------------------------------------------------------------------
	// Get commercial approval required
	//--------------------------------------------------------------------------
	Boolean getCommercialApprovalRequired();

	//--------------------------------------------------------------------------
	// Get total expected data over usage for included tariff options
	//--------------------------------------------------------------------------
	Decimal getTotalExpectedDataOverUsage();

	//--------------------------------------------------------------------------
	// Get total expected text over usage for included tariff options
	//--------------------------------------------------------------------------
	Decimal getTotalExpectedTextOverUsage();

	//--------------------------------------------------------------------------
	// Get total expected voice over usage for included tariff options
	//--------------------------------------------------------------------------
	Decimal getTotalExpectedVoiceOverUsage();
}