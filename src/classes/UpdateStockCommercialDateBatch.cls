global class UpdateStockCommercialDateBatch implements Database.Batchable<sObject> {

    private Map<Id, DateTime> payerIdCommercialDateMap;
    public UpdateStockCommercialDateBatch(Map<Id, DateTime> inMap){
        payerIdCommercialDateMap = inMap;
    }
     global Database.QueryLocator start(Database.BatchableContext BC){
         Set<Id> ids = payerIdCommercialDateMap.keySet();
         String query = 'Select Id, Account__c, Commercial_Date__c from Stock__c where Account__c IN';
         query += ':ids';
         system.debug(query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> sims){
        List<Stock__c> simsToUpdate = new List<Stock__c>();
        for(Stock__c stock : (List<Stock__c>)sims){
            stock.Commercial_Date__c = payerIdCommercialDateMap.get(stock.Account__c);
            simsToUpdate.add(stock);
        }
        if(!simsToUpdate.isEmpty()){
            update simsToUpdate;
        } 
        
    }
    global void finish(Database.BatchableContext BC){
        
    }
        
}