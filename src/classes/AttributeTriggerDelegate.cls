public with sharing class AttributeTriggerDelegate extends TriggerHandler.DelegateBase
{
    
    
    //--------------------------------------------------------------------------
	// Prepare Before
	//--------------------------------------------------------------------------
	public override void prepareBefore(){
	    
	    /*start: Id attribute with name "Clone Tariff" is inserted/updated with null as value, then assiging parents tariff as cloned tarrif */
	    map<id,id> clonedTariffIds = new map<id,id>();
	    list<cscfga__Attribute__c> newAttr = new list<cscfga__Attribute__c>();
	    newAttr = (list<cscfga__Attribute__c>)trigger.new;
	    for(cscfga__Attribute__c attr : newAttr){
	        if(attr.Name =='Cloned Tariff' && attr.cscfga__Value__c == null){
	            clonedTariffIds.put(attr.cscfga__Product_Configuration__c,null);
	        }
	    }
	    system.debug('clonedTariffIds  '+clonedTariffIds);
	    if(clonedTariffIds.size() > 0){
	        list<cscfga__Attribute__c> clonedattrLst = new list<cscfga__Attribute__c>([SELECT cscfga__Value__c, Name, cscfga__Product_Configuration__c, Id,cscfga__Product_Configuration__r.cscfga__Product_Bundle__r.cscfga__Opportunity__c,cscfga__Product_Configuration__r.cscfga__Product_Bundle__r.cscfga__Opportunity__r.Amended_From__c	FROM cscfga__Attribute__c where cscfga__Product_Configuration__c In:clonedTariffIds.keySet()]);
	        map<id,string> parentOppIds = new map<id,string>();
	        for(cscfga__Attribute__c attr: clonedattrLst){
	            if(attr.cscfga__Product_Configuration__r.cscfga__Product_Bundle__r.cscfga__Opportunity__r.Amended_From__c != null){
	                parentOppIds.put(attr.cscfga__Product_Configuration__r.cscfga__Product_Bundle__r.cscfga__Opportunity__r.Amended_From__c,null);
	                clonedTariffIds.put(attr.cscfga__Product_Configuration__c,attr.cscfga__Product_Configuration__r.cscfga__Product_Bundle__r.cscfga__Opportunity__r.Amended_From__c);
	            }
	        }
	        system.debug('clonedTariffIds  '+clonedTariffIds);
	        system.debug('parentOppIds  '+parentOppIds);
	        if(parentOppIds.size() > 0){
	            for(cscfga__Attribute__c attr : [SELECT cscfga__Value__c, Name,Id,cscfga__Product_Configuration__r.cscfga__Product_Bundle__r.cscfga__Opportunity__c FROM cscfga__Attribute__c where cscfga__Product_Configuration__r.cscfga__Product_Bundle__r.cscfga__Opportunity__c in:parentOppIds.keyset() AND Name ='Cloned Tariff']){
	                if(attr.cscfga__Product_Configuration__r.cscfga__Product_Bundle__r.cscfga__Opportunity__c != null){
	                    parentOppIds.put(attr.cscfga__Product_Configuration__r.cscfga__Product_Bundle__r.cscfga__Opportunity__c , attr.cscfga__Value__c);
	                }
	            }    
	        }
	        system.debug('parentOppIds  '+parentOppIds);
	        for(cscfga__Attribute__c attr : newAttr){
    	        if(attr.Name =='Cloned Tariff' && attr.cscfga__Value__c == null){
    	            id clonedTariffId = attr.cscfga__Product_Configuration__c;
    	            id clonedTariffParent;
    	            if(clonedTariffIds.get(clonedTariffId) != null){
    	                
    	                clonedTariffParent = clonedTariffIds.get(clonedTariffId);
    	                system.debug('clonedTariffParent  '+clonedTariffParent);
    	                if(parentOppIds.get(clonedTariffParent) != null){
    	                    system.debug('parentOppIds.get(clonedTariffParent)  '+parentOppIds.get(clonedTariffParent));
    	                    attr.cscfga__Value__c = parentOppIds.get(clonedTariffParent);
    	                }
    	            }
    	        }
    	    }
	        
	    }
	    
	    /* Id attribute with name "Clone Tariff" is inserted/updated with null as value, then assiging parents tariff as cloned tarrif */
	    
	}
    
    
	//--------------------------------------------------------------------------
	// Members
	//--------------------------------------------------------------------------
	List<cscfga__Product_Configuration__c> productConfigurationsToUpdate =
		new List<cscfga__Product_Configuration__c>();

	//--------------------------------------------------------------------------
	// Prepare after
	//--------------------------------------------------------------------------
	public override void prepareAfter()
	{
		//
		// Do any preparation here  bulk loading of data etc
		//
		if (Trigger.isInsert || Trigger.isUpdate)
		{
			List<cscfga__Attribute__c> uniqueKeyAttributes = findUniqueKeyAttributes(Trigger.new);

			//
			// Map from unique key attributes to their parent product configurations
			//
			Map<String, Id> uniqueKeyProductConfigurationMap = new Map<String, ID>();
			Set<String> uniqueKeys = new Set<String>();
			Set<Id> parentConfigurationIds = new Set<Id>();

			for (cscfga__Attribute__c attr : uniqueKeyAttributes)
			{
				if (String.isNotEmpty(attr.cscfga__Value__c))
				{
					uniqueKeys.add(attr.cscfga__Value__c);
					uniqueKeyProductConfigurationMap.put(attr.cscfga__Value__c, attr.cscfga__Product_Configuration__c);
					parentConfigurationIds.add(attr.cscfga__Product_Configuration__c);
				}
			}

			//
			// Select information from attribute parent configurations
			//
			Map<Id, cscfga__Product_Configuration__c> parentConfigurations =
				new Map<Id, cscfga__Product_Configuration__c>([
					SELECT cscfga__Product_Bundle__c, cscfga__Product_Basket__c
					FROM cscfga__Product_Configuration__c
					WHERE Id IN :parentConfigurationIds
			]);

			//
			// Select product bundle ids
			//
			Set<Id> productBundles = new Set<Id>();
			productBundles.add(null);

			for (cscfga__Product_Configuration__c parentConf : parentConfigurations.values())
			{
				productBundles.add(parentConf.cscfga__Product_Bundle__c);
			}


			//
			// Find future child configurations,
			//those that have the same unique key as one of updated/inserted attributes
			//
			List<cscfga__Product_Configuration__c> childConfigurations = [
				SELECT Id, Unique_Key__c
				FROM cscfga__Product_Configuration__c
				WHERE Unique_Key__c IN :uniqueKeys
				AND cscfga__Product_Bundle__c IN :productBundles
			];

			//
			// Relink children to parents and their relationships (like Product Bundles)
			//
			for (cscfga__Product_Configuration__c childConf : childConfigurations)
			{
				Id parentConfigurationId = uniqueKeyProductConfigurationMap.get(childConf.Unique_Key__c);

				childConf.parent_product_configuration__c = parentConfigurationId;
				// merges fields overwriting existing data except Id
				SObjectHelper.mergeFields(childConf, parentConfigurations.get(parentConfigurationId), true);
			}
			productConfigurationsToUpdate.addAll(childConfigurations);
		}
	}

	//--------------------------------------------------------------------------
	// Finish the job (before, after)
	//--------------------------------------------------------------------------
	public override void finish()
	{
		if (!productConfigurationsToUpdate.isEmpty())
		{
			update productConfigurationsToUpdate;
		}
	}

	//--------------------------------------------------------------------------
	// Find unique key attributes
	//--------------------------------------------------------------------------
	List<cscfga__Attribute__c> findUniqueKeyAttributes(List<cscfga__Attribute__c> attributes)
	{
		List<cscfga__Attribute__c> uniqueKeyAttributes = new List<cscfga__Attribute__c>();
		for (cscfga__Attribute__c attr : attributes)
		{
			if (attr.Name == 'Unique Key')
			{
				uniqueKeyAttributes.add(attr);
			}
		}
		return uniqueKeyAttributes;
	}
}