public with sharing class CreateAdditionalRecurringCharge {
	/*****************************************
	Private fields
	******************************************/
	Order_Request__c m2mOrderRequest;
	csord__Order_Request__c orderRequest = null;
	csord__Order__c order = null;
	csord__Subscription__c subscription = null;
	csord__Service__c service = null;
	csord__Service_Line_Item__c serviceLineItem = null;
	Daily_Bill_Item__c newDailyBillItem = null;
	String dailyBill;
	String m2mOrderRequestName;
	/*****************************************
	Properties
	******************************************/
	public Boolean insertOk = true;
	public String message;
	/*****************************************
	Constructors
	******************************************/
	public CreateAdditionalRecurringCharge() {
		
	}

	/*****************************************
	Public Methods
	******************************************/
	public void CreateAdditionalRecurringChargeForDbi(Daily_Bill_Item__c dbi, String idDailyBill) {
		try{
			dailyBill = idDailyBill;
			//Create m2mOrderRequest
			m2mOrderRequest = createM2MOrderRequest(dbi.Billing_account__c);
			upsert m2mOrderRequest;

			orderRequest = createOrderRequest(m2mOrderRequest);
			upsert orderRequest;

			order = createOrder(m2mOrderRequest, orderRequest);
			upsert order;

			subscription = createSubscriptionForOrder(order, orderRequest, dbi);
			upsert subscription;

			service = createService(order, orderRequest, subscription, dbi);
			upsert service;

			serviceLineItem = createServiceLineItem(service, dbi, orderRequest);
			upsert serviceLineItem;

			Date todayDate = System.today();
			if (getDailyBillDate() <= todayDate){
				newDailyBillItem = createDBI(dbi);
				upsert newDailyBillItem;
			}
		}
		catch (Exception ex){
			insertOk = false;
			message = ex.getMessage() + ' Line number: ' + ex.getLineNumber();
			if (serviceLineItem != null)
				delete serviceLineItem;
			if (service != null)
				delete service;
			if (subscription != null)
				delete subscription;
			if (order != null)
				delete order;
			if (orderRequest != null)
				delete orderRequest;
			if (m2mOrderRequest != null)
				delete m2mOrderRequest;
			if (newDailyBillItem != null)
				delete newDailyBillItem;
		}
	}

	/*****************************************
	Methods
	******************************************/
	//Create M2M Order Request
	@TestVisible
	private Order_Request__c createM2MOrderRequest(Id billingAccountId){
		Order_Request__c orderRequest =  new Order_Request__c();
		//Order_request.Frame_Contract__c = 
		Billing_Account__c baTmp = [SELECT id, payer__c from Billing_Account__c where id = :billingAccountId];
		orderRequest.Account__c = baTmp.Payer__c;
		orderRequest.Billing_Account__c = billingAccountId;
		orderRequest.Boni_Check__c = getBoniCheckForAccount(baTmp.payer__c);
		orderRequest.Status__c = 'Closed';
		//orderRequest.Business_Model_Description__c = 
		//orderRequest.Opportunity__c = 
		//orderRequest.Business_Model_Description__c =
		return orderRequest;
	}

	//Create Order request
	private csord__Order_Request__c createOrderRequest(Order_Request__c m2mOrderRequest){
		csord__Order_Request__c orderRequest = new csord__Order_Request__c();
		orderRequest.Name = getM2mOrderRequestName(m2mOrderRequest.Id);
		orderRequest.csord__Module_Name__c = 'M2M';
		orderRequest.csord__Module_Version__c = 'v1.0';
		return orderRequest;
	}
	
	//Create Order
	private csord__Order__c createOrder(Order_Request__c m2mOrderRequest, csord__Order_Request__c orderRequest){
		csord__Order__c order = new csord__Order__c();
		order.Name = 'Order for ' + getM2mOrderRequestName(m2mOrderRequest.Id);
		order.csord__Account__c = m2mOrderRequest.Account__c;
		order.csord__Status2__c = 'Opened';
		order.Order_Request__c = m2mOrderRequest.Id;
		order.csord__Order_Request__c = orderRequest.Id;
		order.csord__Identification__c = 'Order';
		return order;
	}
	
	//Create subscription for order
	private csord__Subscription__c createSubscriptionForOrder(csord__Order__c order, csord__Order_Request__c orderRequest, daily_bill_Item__c dbi){
		csord__Subscription__c subscription = new csord__Subscription__c();
		subscription.csord__Account__c = order.csord__Account__c;
		subscription.Billing_Account__c = dbi.Billing_account__c;
		subscription.csord__Status__c = 'Active';
		subscription.csord__Identification__c = getJournalMapNameFromDBIProductType(dbi.Product_Type__c);
		subscription.csord__Order__c = order.Id;
		subscription.csord__Order_Request__c = orderRequest.Id;
		subscription.Name = 'Subscription for ' + getM2mOrderRequestName(m2mOrderRequest.Id);
		return subscription;
	}
	
	//Create service
	private csord__Service__c createService(csord__Order__c order, csord__Order_Request__c orderRequest, csord__Subscription__c subscription, Daily_Bill_Item__c dbi){
		csord__Service__c service = new csord__Service__c();
		service.Name = 'Additional recurring charge';
		service.csord__Order__c = order.Id;
		service.csord__Order_Request__c = orderRequest.Id;
		service.csord__Status__c = 'Active';
		service.csord__Subscription__c = subscription.Id;
		service.csord__Identification__c = getJournalMapNameFromDBIProductType(dbi.Product_Type__c);

		Date tmpDate = getDailyBillDate();
		service.csord__Activation_Date__c = tmpDate;
		Integer numOfMonths = Integer.valueOf(dbi.Number_of_months__c);
        tmpDate = tmpDate.addMonths(numOfMonths);
		service.Csord__Deactivation_Date__c = tmpDate;
		return service;
	}
	
	//Create service line item
	private csord__Service_Line_Item__c createServiceLineItem(csord__Service__c service, daily_bill_Item__c dbi, csord__Order_Request__c orderRequest){
		csord__Service_Line_Item__c serviceLineItem = new csord__Service_Line_Item__c();
		serviceLineItem.Name = 'Additional recurring charge';
		serviceLineItem.csord__Service__c = service.Id;
		serviceLineItem.Journal_Mapping__c = getJournalMappingFromDailyBillItem(dbi);
		serviceLineItem.Total_Price__c = dbi.Betrag__c * dbi.Menge__c;
		serviceLineItem.Csord__Is_Recurring__c = true;
		serviceLineItem.csord__Order_Request__c = orderRequest.Id;
		serviceLineItem.Csord__Identification__c = dbi.Product_Type__c;
		
		return serviceLineItem;
	}

	//Create Daily Bill Item For current Daily Bill
	public Daily_bill_Item__c createDBI(Daily_Bill_Item__c dbi){
		Daily_bill_Item__c dbiNew = new Daily_Bill_Item__c();
		dbiNew.Payment_Type__c = 'Recurring';
		dbiNew.Number_of_months__c = dbi.Number_of_months__c;
		dbiNew.Daily_Bill__c = dailyBill;
		List<Billing_Account__c> ba = [select id,OFI_ID__c from billing_account__c where Id = :dbi.Billing_Account__c];
        if(!ba.isEmpty() && dbi.Billing_Account__c !=null)
        {
            dbiNew.VO_Nr__c =ba[0].OFI_ID__c;
        }
		dbiNew.I_CO__c='';
        dbiNew.Konto__c='4120100';
        dbiNew.Kost__c='465605';
        dbiNew.Prod__c='387654';
        dbiNew.Waehrung__c='EUR';
        dbiNew.Betrag__c = dbi.Betrag__c;
        dbiNew.Einzel_Nettobetrag__c = dbiNew.Betrag__c;
        dbiNew.Menge__c = 1;
        dbiNew.Billing_Account__c = dbi.Billing_Account__c;
        if (dbi.Tax_code__c =='19%') 
        	dbiNew.Steuersatz__c=19; 
        else 
        	dbiNew.Steuersatz__c=0;
        
        if (dbiNew.Steuersatz__c<>0) {
	        dbiNew.Einzel_Taxbetrag__c = (dbiNew.Betrag__c * (dbiNew.Steuersatz__c/100)).setScale(2,RoundingMode.HALF_UP); 
	        dbiNew.Einzel_Bruttobetrag__c = (dbiNew.Betrag__c * (1 + (dbiNew.Steuersatz__c/100))).setScale(2,RoundingMode.HALF_UP); 
        }else{
            dbiNew.Einzel_Taxbetrag__c = dbiNew.Betrag__c * 0; 
            dbiNew.Einzel_Bruttobetrag__c = dbiNew.Betrag__c ;
        }

        dbiNew.Gesamt_Bruttobetrag__c = (dbiNew.Einzel_Bruttobetrag__c * dbi.Menge__c).setScale(2,RoundingMode.HALF_UP);
        dbiNew.Gesamt_Nettobetrag__c = (dbiNew.Einzel_Nettobetrag__c * dbi.Menge__c).setScale(2,RoundingMode.HALF_UP);
        dbiNew.Gesamt_Taxbetrag__c = (dbiNew.Einzel_Taxbetrag__c * dbi.Menge__c).setScale(2,RoundingMode.HALF_UP);

        dbiNew.Beschreibung__c = dbi.Beschreibung__c;
        dbiNew.Anweisung__c = dbi.Anweisung__c;

        return dbiNew;
	}

	/*****************************************
	Helper Methods
	******************************************/
	//Get first Billing Account for the given Account Id
	@TestVisible
	private Billing_Account__c getBillingAccountForAccount(Id accountId){
		Billing_Account__c bAcc = null;
		List<Billing_Account__c> bAccList = [SELECT id, ofi_id__c FROM Billing_Account__c WHERE Payer__c = :accountId];
		if (bAccList != null && bAccList.size()>0){
			bAcc = bAccList[0];
		}
		return bAcc;
	}
	
	//Get latest Boni Check for given Account
	@TestVisible
	private Id getBoniCheckForAccount(Id accountId){
		Id bCheck = null;
		List<Boni_Check__c> bCheckList = [SELECT id FROM Boni_Check__c WHERE Account__c = :accountId ORDER BY createddate DESC];
		if (bCheckList != null && bCheckList.size()>0){
			bCheck = bCheckList[0].Id;
		}
		return bCheck;
	}
	
    //Get Daily bill date for given daily Bill
    @TestVisible
    private Date getDailyBillDate(){
        Date retDate = null;
        List<daily_bill__c> tmpList = [SELECT id, Date__c from daily_bill__c where id = :dailyBill];
        if (tmpList != null && tmpList.size() > 0)
            retDate = Date.newInstance(tmpList[0].Date__c.year(), tmpList[0].Date__c.month(), tmpList[0].Date__c.day());
        return retDate;
    }
	
	//Get Journal Map from DailY bill Item
	@TestVisible
	private Id getJournalMappingFromDailyBillItem(Daily_Bill_Item__c dbItem){
        Id jMapId = null;
        String journalMapName = getJournalMapNameFromDBIProductType(dbItem.Product_Type__c);
        String revenueCode = dbItem.Tax_Code__c;
        List<Journal_Mapping__c> jmapList = [SELECT ID from Journal_Mapping__c 
                                            where Name = :journalMapName and
                                                 Revenue__c = :revenueCode];
        if (jmapList != null && jmapList.size() > 0)
            jMapId = jmapList[0].Id;

        return jMapId;
    }

    //Get Journal Map Name from dbi product
    @TestVisible
    private String getJournalMapNameFromDBIProductType(String dbiProduct){
        Map<String, String> nameMap = new Map<String, String>();
        nameMap.put('Data', 'Data Usage Fee');
        nameMap.put('SMS', 'SMS Usage Fee');
        nameMap.put('Voice', 'Voice Usage Fee');
        nameMap.put('Monthly fee', 'Grundgebühr');
        nameMap.put('Hardware', 'HW Revenue (Hardware Only)');
        nameMap.put('Service', 'Kosten Serviceeinsatz (einmalig & recurring)');

        String name = '';
        name = nameMap.get(dbiProduct);
        return name;
    }

    @TestVisible
    private String getM2mOrderRequestName(Id m2mOrderRequestId){
    	System.debug(m2mOrderRequestId);
    	if (String.isBlank(m2mOrderRequestName)){
    		List<Order_Request__c> ors = [SELECT Id, Name from Order_Request__c where id = :m2mOrderRequestId];
	    	if (ors != null && ors.size() > 0)
	    		m2mOrderRequestName = ors[0].Name;
    	}
    	System.debug(m2mOrderRequestName);
    	return m2mOrderRequestName;
    }
}