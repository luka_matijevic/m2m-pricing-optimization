global class RetireSIMsBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    
    private static final String TERMINATION_STATUS_TERMINATED = 'TERMINATED';
    private static final String TERMINATION_STATUS_RETIRED = 'TERMINATED-RETIRED';
    private static final String SM2M_STATUS_RETIRED = 'RETIRED';
    
    private Date reqDate = System.today().addMonths(-24).addDays(-42).addMonths(24);
    private String query = 'Select Id,Name,End_date__c,Termination_Status__c,SM2M_Status__c from stock__C where End_date__c <= :reqDate AND Termination_Status__c = \'' + TERMINATION_STATUS_TERMINATED + '\'';
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> sims){
        List<Stock__c> simsToUpdate = new List<Stock__c>();
        for(Stock__c stock : (List<Stock__c>)sims){
            //stock.SM2M_Status__c = SM2M_STATUS_RETIRED;
            stock.Termination_Status__c = TERMINATION_STATUS_RETIRED;
            stock.State_Change_Date__c = Date.today();
            simsToUpdate.add(stock);
        }
        if(!simsToUpdate.isEmpty()){
            update simsToUpdate;
        }

        Datetime now = Datetime.now();
        System.debug('2 test');
        //Call Heroku to update the status and on succesful responce set the Termination Status to Terminated
        SIM_Termination_Job__c jobData = new SIM_Termination_Job__c(
            Name = 'SIM Retire JOB @' + now.formatLong(),
            Status__c = Constants.RETIREMENT_PENDING
        );
        
        insert jobData;
        System.enqueueJob(new AsyncSIMTerminationSendMessage(jobData));
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
}