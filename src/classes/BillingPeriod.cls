/**
 * This class calculates the weighed amount for the billing period.
 */
public class BillingPeriod {

	/**
	 * [periodStart, periodEnd)
	 */
	Date periodStart, periodEnd, lastDay;
	Decimal unitAmount;
	DayDistribution dayDistribution;

	public BillingPeriod(Date periodStart, Date periodEnd) {
		this.periodStart = periodStart;
		this.periodEnd = periodEnd;
		this.lastDay = (this.periodEnd).addDays(-1);

		if (!isInTwoMonths()) {
			this.dayDistribution = new DayDistribution(this.length(), 0);
		} else {
			Date firstOfSecondMonth = periodEnd.toStartOfMonth();
			Integer firstMonthDays = periodStart.daysBetween(firstOfSecondMonth);
			Integer secondMonthDays = firstOfSecondMonth.daysBetween(periodEnd);
			this.dayDistribution = new DayDistribution(firstMonthDays, secondMonthDays);
		}
	}

	public Boolean isInTwoMonths() {
		return periodStart.month() != lastDay.month();
	}

	/**
	 * [periodStart, periodEnd)
	 */
	public Integer length() {
		return periodStart.daysBetween(periodEnd);
	}

	public Decimal weightedAmount(Decimal unitAmount) {
		return dayDistribution.firstMonth * unitAmount / daysInMonth(periodStart) + dayDistribution.secondMonth * unitAmount / daysInMonth(lastDay);
	}

	private Integer daysInMonth(Date d) {
		return Date.daysInMonth(d.year(), d.month());
	}

	private class DayDistribution {

		public DayDistribution(Integer firstMonth, Integer secondMonth) {
			this.firstMonth = firstMonth;
			this.secondMonth = secondMonth;
		}

		Integer firstMonth { get; set; }
		Integer secondMonth { get; set; }
	}
}