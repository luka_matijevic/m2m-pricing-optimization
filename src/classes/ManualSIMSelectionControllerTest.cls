@isTest
public class ManualSIMSelectionControllerTest {
    @TestSetup
    static void prepareTestData(){
        
    }
    
    static testMethod void testMethod1(){
        Order_Request__c m2mOrdReq = new Order_Request__c( Shipping_City__c='Test City', Shipping_Country__c = 'Test Country', Shipping_Postal_Code__c='10000', Shipping_Street__c='Test shipping street');

        insert m2mOrdReq;
        
        id simRecrdtype = Schema.SObjectType.Article_Info_Set__c.getRecordTypeInfosByName().get('SIM').getRecordTypeId();
        
        Article_Info_Set__c articleInfo = new Article_Info_Set__c(recordtypeId=simRecrdtype,name='Artical Info set',Global_Local__c = 'Global',Status__c='Draft',One_Off_Journal_Mapping__c='Reccurring Fee',
                                            Journal_Mapping__c = 'SMS Usage Fee',Contract_Term__c ='12',One_Time_Fee__c = 100,Recurring_Fee__c = 100);
        insert articleInfo;
        Third_Upload_File__c tuf = new Third_Upload_File__c(name = 'Third upload file',SIM_Article_Type__c=articleInfo.id );
        insert tuf;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(tuf);
        ManualSIMSelectionController ms1 = new ManualSIMSelectionController(sc);
        ms1.resetSelectedStockWrapper();
        
        Stock__c st = new Stock__c(name ='Stock',Article_Info_Set__c =articleInfo.id,Status__c = 'On stock', ICCID__c = '1111111111111111111',Third_Upload_File__c=tuf.id);
        insert st;
        
        test.startTest();  
        ManualSIMSelectionController ms = new ManualSIMSelectionController(sc);
        ms.iccidPart = '1111111111111111111'; 
        ms.setUpFirstCurrentPage();
        ms.setUpNextCurrentPage();
        ms.setUpPreviousCurrentPage();
        ms.setUpLastCurrentPage(); 
        ms.renderingButtons();
        ms.runSearch(); 
        ms.renderingSelectedButtons();
        ms.querySelectedStockPerPage();
        ms.getFirstRecordIndexSelected();
        ms.setUpFirstCurrentPageSelected();
        ms.setUpNextCurrentPageSelected();
        ms.setUpPreviousCurrentPageSelected();
        ms.toggleSort();
        ms.openArvatoUploadPage();
        ms.updateSIMCard();
        ms.setUpLastCurrentPageSelected();
        ms.queryStockPerPage();
        test.stopTest();
               
    }
    
   
}