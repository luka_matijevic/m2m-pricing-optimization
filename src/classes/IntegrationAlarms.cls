public class IntegrationAlarms {

	static Billing_Settings__c billingSettings;
	
	static {
		billingSettings = Billing_Settings__c.getOrgDefaults();
	}

	public class PrebillRetrieve implements EmailHandler.Emailable {
		
		private Messaging.SingleEmailMessage email;

		public PrebillRetrieve(csam_t1__Incoming_Message__c inMsg) {
			
			SM2MErrorMessageParser errorParser = new SM2MErrorMessageParser(inMsg);

			List<csam_t1__Outgoing_Message_Record__c> outMsgRecords = InboundProcessorHelper.outgoingMessageRecords(inMsg);
			List<Id> recordIds = recordIds(outMsgRecords);

			List<Billing_Account__c> billingAccounts = [
				SELECT Id, Name 
				FROM Billing_Account__c 
				WHERE Id in :recordIds
			];
			
			email = new Messaging.SingleEmailMessage();
			email.setSubject('Prebill retrieval failed');
			
			if (billingAccounts.isEmpty()) {
				email.setPlainTextBody('Prebill retrieval failed.');
			} else {
				Billing_Account__c billingAccount = billingAccounts.get(0);
				email.setPlainTextBody('Prebill retrieval failed for billing account "' + billingAccount.Name + '" (' + billingAccount.Id + ').');
			}
			
			if (errorParser.hasSocketTimeoutException()) {
				email.setPlainTextBody(email.getPlainTextBody() + ' ' + errorParser.getSocketTimeoutException());
			} else if (errorParser.hasUnrecoverableException()) {
				email.setPlainTextBody(email.getPlainTextBody() + ' ' + errorParser.getUnrecoverableException());
			}
			email.setToAddresses(new String[] {billingSettings.Status_reporting_email__c});
		}

		public Messaging.SingleEmailMessage[] emails() {
			return new Messaging.SingleEmailMessage[] { email };
		}

		private List<Id> recordIds(List<csam_t1__Outgoing_Message_Record__c> outgoingRecords) {
			List<Id> s = new List<Id>();
			for (csam_t1__Outgoing_Message_Record__c rec : outgoingRecords) {
				s.add(rec.csam_t1__Object_Record_Id__c);
			}
			return s;
		}
	}

	public class SimStatus implements EmailHandler.Emailable {
		
		private Messaging.SingleEmailMessage email;

		public SimStatus (csam_t1__Incoming_Message__c inMsg) {
			
			List<csam_t1__Outgoing_Message_Record__c> outMsgRecords = InboundProcessorHelper.outgoingMessageRecords(inMsg);
			List<Id> recordIds = recordIds(outMsgRecords);

			List<Account> accounts = [
				SELECT Id, Name 
				FROM Account 
				WHERE Id in :recordIds
			];

			email = new Messaging.SingleEmailMessage();
			email.setSubject('SIM Status Update failed');

			if (accounts.isEmpty()) {
				email.setPlainTextBody('SIM Status Update failed.');
			} else {
				Account acc = accounts.get(0);
				String orgURL = loadStaticResource('Organization_URL');
				email.setPlainTextBody('SIM Status Update failed for account "' + acc.Name + '" (' + orgURL + acc.Id + ').');
			}
			email.setToAddresses(new String[] {billingSettings.Status_reporting_email__c});
		}

		public Messaging.SingleEmailMessage[] emails() {
			return new Messaging.SingleEmailMessage[] { email };
		}

		private List<Id> recordIds(List<csam_t1__Outgoing_Message_Record__c> outgoingRecords) {
			List<Id> s = new List<Id>();
			for (csam_t1__Outgoing_Message_Record__c rec : outgoingRecords) {
				s.add(rec.csam_t1__Object_Record_Id__c);
			}
			return s;
		}
	}
	
	//--------------------------------------------------------------------------
    // Loading of the static resource
    //--------------------------------------------------------------------------
    public static String loadStaticResource(String resourceName)
    {
    	return [SELECT Description FROM StaticResource WHERE Name = :resourceName ].Description;
    }
}