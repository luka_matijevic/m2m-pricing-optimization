@isTest
private class TestUpdateMSISDNIsReadyForUse 
{  
	static testMethod void TestUpdateMSISDNIsReadyForUse() 
	{
		
	Test.startTest();
		List<MSISDN__c> msisdnList = new List<MSISDN__c>();
		msisdnList.add
		(
			new MSISDN__c
			(
				MSISDN__c = '1',
				Date_quarantine_start__c = Date.today(),
				Date_quarantine_end__c = Date.today().addDays(90),
				isReadyForUse__c = false
			)
		);
		msisdnList.add
		(
			new MSISDN__c
			(
				MSISDN__c = '2',
				Date_quarantine_start__c = Date.today().addDays(-100),
				Date_quarantine_end__c = Date.today().addDays(-10),
				isReadyForUse__c = false
			)
		);
		
		insert msisdnList;
		Database.executeBatch(new UpdateMSISDNIsReadyForUse());

		Test.stopTest();
		}

}