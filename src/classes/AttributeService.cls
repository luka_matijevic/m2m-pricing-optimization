public with sharing class AttributeService {

	/**
	 * Empty constructor. The service is not static by design, to allow mockups for testing.
	 */
	public AttributeService() {
	}

	/**
	 * Returns price attributes related to provided product configurations. The attributes can be recurring or one-off
	 */ 
	public List<cscfga__Attribute__c> getPriceAttributes(List<cscfga__Product_Configuration__c> productConfigurations, Boolean isRecurring) {
		return [
			SELECT
				cscfga__Product_Configuration__c, cscfga__Price__c
			FROM
				cscfga__Attribute__c
			WHERE
				cscfga__Recurring__c = :isRecurring
			AND
				cscfga__Is_Line_Item__c = true
			AND
				cscfga__Product_Configuration__c IN :productConfigurations
		];
	}
	
	/**
	 * Returns quantity attributes related to provided product configurations
	 */ 
	public List<cscfga__Attribute__c> getQuantityAttributes(List<cscfga__Product_Configuration__c> productConfigurations) {
		return [
			SELECT
				cscfga__Product_Configuration__c, cscfga__Value__c
			FROM
				cscfga__Attribute__c
			WHERE
				Name = 'Quantity'
			AND
				cscfga__Product_Configuration__c IN :productConfigurations
		];
	}
	
	public List<cscfga__Attribute__c> getRequiresApprovalAttribute(Set<Id> prodBundleIds, String[] attrName) {
	    List<cscfga__Attribute__c> attrValues = [SELECT Name, cscfga__Value__c, cscfga__Product_Configuration__r.cscfga__Product_Bundle__c 
	    	FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__r.cscfga__Product_Bundle__c in :prodBundleIds AND Name in :attrName];
	    	
	    return attrValues;
	}
}