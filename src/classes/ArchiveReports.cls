global class ArchiveReports implements Schedulable {
    /*Customer bills older than 6 months are archived*/
    /*Replaces existing link*/
    /*deletes other report links*/
    global void execute(SchedulableContext sc) {
        date d = system.today().toStartOfMonth().addMonths(-6);
        List<Customer_Monthly_Bill__c> cmbList = 
            [select id, PDF_Bill_Link__c, Sim_Card_Summary__c, EDIFACT__c, EVN_EDIFACT__c, EVN_Report_PDF__c, EVN_Report_CSV__c, archived__C
             from Customer_Monthly_Bill__c
             where billing_date__c = :d
             and archived__C = false
            ];
        
        for (Customer_Monthly_Bill__c cmb : cmbList){
            cmb.Sim_Card_Summary__c = '';
            cmb.EDIFACT__c = '';
            cmb.EVN_EDIFACT__c = '';
            cmb.EVN_Report_PDF__c = '';
            cmb.EVN_Report_CSV__c = '';
            cmb.PDF_Bill_Link__c = Label.ReportArchiveDownloadLink + 
                cmb.PDF_Bill_Link__c.right((cmb.PDF_Bill_Link__c.length()-cmb.PDF_Bill_Link__c.lastIndexOf('/'))-1); 
            cmb.Archived__c = true;
        }
        update cmbList;

        
    }
    public static String CRON_EXP = '0 0 0 1 * ? *';
    public static void scheduleTick() {
		ArchiveReports job = new ArchiveReports();
		System.schedule('ArchiveReports', CRON_EXP, job);

	}

}