public class MSISDNRangeTriggerDelegate  extends TriggerHandler.DelegateBase{ 


	/**
	* Check range start, end (numbers, start<end, ranges cant overlap )
	*/
	Map<Id, MSISDN_Range__c> msisdnRangeList = new Map<Id, MSISDN_Range__c>();
	public override void prepareBefore() {
        
		if (Trigger.isInsert || Trigger.isUpdate) {     
			
			Map<Id, MSISDN_Range__c> allRanges = new Map<Id, MSISDN_Range__c>([select Range_start__c, Range_end__c, Name, Range_Length__c from MSISDN_Range__c where id not in :Trigger.new ]);
			String paddingZero = '0';
			String paddingNine = '9';
			for (MSISDN_Range__c m : (List<MSISDN_Range__c>) Trigger.new){
				
				String errorText = '';
				m.Range_start__c = m.Range_start__c + paddingZero.repeat(/*MSISDN_Range__c.Range_start__c.getDescribe().getLength()*/ Integer.valueOf(m.Range_Length__c) - m.Range_start__c.length());
				m.Range_end__c = m.Range_end__c + paddingNine.repeat(/*MSISDN_Range__c.Range_start__c.getDescribe().getLength()*/ Integer.valueOf(m.Range_Length__c) - m.Range_end__c.length());

				
				if ( !m.Range_start__c.isNumeric() ) errorText += ' Range start has to be a number.';
				if ( !m.Range_end__c.isNumeric() ) errorText += ' Range end has to be a number.';
				
				if ( (m.Range_start__c.isNumeric() && m.Range_end__c.isNumeric()) && 
					(AdditionalUtils.compareBigNumbers( m.Range_start__c, m.Range_end__c ) >= 0 ) ) 
					errorText += ' Range end has to be greater than range start.';
                /*09-2016 added range limit 10000*/
                try{
					long rS = Long.valueOf(m.Range_Start__c);
                	long rE = Long.valueOf(m.Range_End__c);
                	if (rE-rS>10000) errorText+=' Range should not exceed 10000 MSISDNs.';
                }
                catch (Exception e){}
                
					//no errors, check overlap
				if (errorText.length()==0){
					for (MSISDN_Range__c range : allRanges.values()){
					
system.debug('m.Range_start__c: ' + m.Range_start__c + '>>>m.Range_end__c: '+ m.Range_end__c + '>>>range.Range_start__c: '+range.Range_start__c+'>>>:range.Range_end__c '+range.Range_end__c);
						if ((AdditionalUtils.compareBigNumbers(m.Range_start__c ,range.Range_start__c)>=0 &&
							 AdditionalUtils.compareBigNumbers(m.Range_start__c , range.Range_end__c)<=0)
							 ||
							 (
							 AdditionalUtils.compareBigNumbers(m.Range_end__c , range.Range_start__c)>=0 &&
							 AdditionalUtils.compareBigNumbers(m.Range_end__c , range.Range_end__c)<=0
							))
						errorText += ' Range overlaps range: ' + range.Name;
					}
				}


                if (errorText.length()>0) {
                    m.addError(errorText);
                    
                }
			}
		}
	}
    public override void prepareAfter() {
        // do any preparation here – bulk loading of data etc
             //populate MSISDN object with missing MSISDNs - exclude TC accounts
             
       	Set<Id> acctSet = new Set<Id>();
        for (MSISDN_Range__c m : (List<MSISDN_Range__c>) Trigger.new){
            acctSet.add(m.Account__c);            
        }    
        
        Map<Id, boolean> acctMap = new Map<Id, boolean>();
        List<Account> acctList = [select id, Data_security_customer__c from account where id in :acctSet];
        for (Account acct : acctList) acctMap.put(acct.Id, acct.Data_security_customer__c);
        
        for (MSISDN_Range__c m : (List<MSISDN_Range__c>) Trigger.new){
            if((acctMap.containsKey(m.Account__c) && !acctMap.get(m.Account__c)) || !acctMap.containsKey(m.Account__c))
        		try{ 
                	Database.executeBatch(new MsisdnBatch(Long.valueOf(m.Range_start__c), Long.valueOf(m.Range_end__c), m.Id));
				}                
            	catch (Exception e){
    	            m.addError(' Unable to insert MSISDNs, error: ' + e.getMessage());
	        	}
        }
    }
}