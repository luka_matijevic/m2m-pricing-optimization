/*
 * Description: Batch to clean HistoryTrack duplicate entries. One time execution.
 * MAKE SURE TO BYPASS TRIGGERS & VALIDATION USING CUSTOM SETTINGS DATA (AS REQUIRED).
 * Author: Ritesh Saxena
 * Date: 06/Jan/2015
*/
public class StockHistoryTrackFixBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    private static String query = 'Select Id, SM2M_Status__c, HistoryTracking__c, Activation_Date__c, Deactivation_Date__c, State_Change_Date__c from Stock__c';
    
    /*
    * Start call to process SIM record.
    */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        if (test.isRunningTest()) {
            query+= ' limit 25';
        }
        return Database.getQueryLocator(query);  
    }
    
    /*
    * Execute call to process SIM record.
    */
    public void execute(Database.BatchableContext BC, List<SObject> scope) {
        List<Stock__c> stockList = new List<Stock__c>();
        for (Stock__c sim: (List<Stock__c>) scope) {
            
            Set<String> statusHistorySet = new Set<String>();
            if(sim.HistoryTracking__c!=null && sim.HistoryTracking__c!=''){
                for(String str : sim.HistoryTracking__c.split('\n')){
                    if(str!=null && str!=''){
                        statusHistorySet.add(str);          
                    }
                }
                sim.HistoryTracking__c = '';
                for(String key: statusHistorySet){
                    sim.HistoryTracking__c =  '' + key + '\n' + (sim.HistoryTracking__c==null?'' : sim.HistoryTracking__c);
                }
                stockList.add(sim);
            }
        }
        if (!stockList.isEmpty()) {         
            update stockList;           
        }
    }
    
    /*
    * Finish call to process SIM record.
    */
    public void finish(Database.BatchableContext BC) {

        //Nothing to-do            
    }
}