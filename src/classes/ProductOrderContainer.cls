public with sharing class ProductOrderContainer
{
	public String name {get; set;}
	public Integer quantity {get; set;}
	public String comment {get; set;}
	public Boolean child {get; set;}
	public List<Id> productConfigurations;

	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	public ProductOrderContainer() {}

	//--------------------------------------------------------------------------
	// Copy constructor
	//--------------------------------------------------------------------------
	public ProductOrderContainer(ProductOrderContainer poc)
	{
		name = poc.name;
		quantity = poc.quantity;
		comment = poc.comment;
		child = poc.child;
		productConfigurations = poc.productConfigurations.clone();
	}
}