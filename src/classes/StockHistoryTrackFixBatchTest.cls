/*
*  Test StockHistoryTrackFixBatch execution. Test class to provide coverage to one time execution of StockHistoryTrackFixBatch .
*/
@isTest
private class StockHistoryTrackFixBatchTest {
    
    @isTest static void testHistoryTrackDuplicateFix() {
        
        Integer year = Date.today().year();
        Account acc = new Account(Name = 'Test account', TEF_DE_Sales_Channel__c = 'Test channel', Type = 'Customer');
        insert acc;     
        List<Stock__c> simList = new List<Stock__c>();

        Stock__c sim1 = new Stock__c(
            Name = '12345678901234567891',
            ICCID__c = '12345678901234567891',
            Account__c = acc.Id,
            Activation_Date__c = Date.newInstance(year, 1, 3),
            HistoryTracking__c = 'ACTIVE' + '|' + Date.newInstance(year, 1, 3) + '/n' + 'ACTIVE' + '|' + Date.newInstance(year, 1, 3)
        );
        simList.add(sim1);
        Stock__c sim2 = new Stock__c(
            Name = '12345678901234567892',
            ICCID__c = '12345678901234567892',
            Account__c = acc.Id,
            Deactivation_Date__c = Date.newInstance(year, 2, 6),
            HistoryTracking__c = 'DEACTIVATED' + '|' + Date.newInstance(year, 2, 6) + '/n' + 'DEACTIVATED' + '|' + Date.newInstance(year, 2, 6)
        );
        simList.add(sim2);
        insert simList;
        
        Test.startTest();
            Database.executeBatch(new StockHistoryTrackFixBatch());
        Test.stopTest();
        
        Set<String> statusHistorySet = new Set<String>();
        for(Stock__c sim : [Select Id, HistoryTracking__c from Stock__c where Id IN: simList]){          
            for(String str : sim.HistoryTracking__c.split('\n')){
                if(str!=null && str!=''){
                    statusHistorySet.add(str);          
                }
            }       
        }
        System.assert(statusHistorySet.size()==2);
    }
}