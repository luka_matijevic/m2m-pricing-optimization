@isTest
private class TestUpdateSIMStatusesSchedule 
{
	//--------------------------------------------------------------------------
	// Schedule job
	//--------------------------------------------------------------------------
	static testMethod void schedule()
	{
		Test.startTest();
		System.schedule('TestUpdateSIMStatuses', '0 0 0 3 9 ? 2022', new UpdateSIMStatusesSchedule());
		Test.stopTest();
	}
}