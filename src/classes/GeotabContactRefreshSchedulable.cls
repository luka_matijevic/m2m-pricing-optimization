global class GeotabContactRefreshSchedulable implements Schedulable, Database.AllowsCallouts {
	global void execute(SchedulableContext sc) {
		GEOTAB_Settings__c geotabSettings = GEOTAB_Settings__c.getInstance();
		String getUserContactsEndPoint = geotabSettings.GetUserContactsEndPoint__c;
		String userInfoEndPoint = geotabSettings.UserInfoEndPoint__c;
		Map<String, List<GeotabUtils.GeotabUserContact>> geotabUserContactsResultMap = null;
		Map<String, GeotabUtils.AuthenticationResponseClass> authorizationResultMap = null;
		List<Configurable_Sixt_helper__c> customConfig = [SELECT Id, Name,  Geotab_username__c, Geotab_password__c, Sixt_frame_contract__c, Sixt_account__c, Sixt_billing_account__c, Sixt_Opportunity_RecordType__c FROM Configurable_Sixt_helper__c WHERE Name = 'Configurable records' ORDER BY CreatedDate ASC];
		List<GeotabUtils.GeotabAccount> geotabAccounts = null;
		Configurable_Sixt_helper__c csh = null;

		if (customConfig != null && customConfig.size() > 0)
		{
			csh = customConfig[0];
		}
		
		if (csh != null)
		{
			userInfoEndPoint = userInfoEndPoint + csh.Geotab_username__c + '&password=' + csh.Geotab_password__c;
		}

		HttpResponse resp = CaseUtils.executeRequest(userInfoEndPoint, 'GET', null, 32);

		if(resp != null && resp.getStatusCode() == 200) {
			authorizationResultMap = (Map<String, GeotabUtils.AuthenticationResponseClass>)JSON.deserialize(resp.getBody(), Map<String, GeotabUtils.AuthenticationResponseClass>.class);
			
			if (authorizationResultMap != null) {
				String userIdResult, sessionIdResult;

				userIdResult = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').userId : null;
				sessionIdResult = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').sessionId : null;
				geotabAccounts = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').accounts : null;
				//System.debug(LoggingLevel.INFO, 'Geotab Accounts API ->' + geotabAccounts);
				if(geotabAccounts != null && geotabAccounts.size() > 0) {
					getUserContactsEndPoint = getUserContactsEndPoint + 'apiKey="' + userIdResult + '"&forAccount="' + geotabAccounts[0].AccountId + '"&sessionId="' + 
						sessionIdResult + '"';

					resp = CaseUtils.executeRequest(getUserContactsEndPoint, 'GET', null, 32);

					if(resp != null && resp.getStatusCode() == 200) {
						geotabUserContactsResultMap = (Map<String, List<GeotabUtils.GeotabUserContact>>)JSON.deserialize(resp.getBody(), Map<String, List<GeotabUtils.GeotabUserContact>>.class);
						//System.debug(LoggingLevel.INFO, 'geotabUserContactsResultMap API ->' + geotabUserContactsResultMap);
						List<GeotabUtils.GeotabUserContact> geotabContactsApi = geotabUserContactsResultMap.get('result');
						//System.debug(LoggingLevel.INFO, 'Geotab Contacts API ->' + geotabContactsApi);
						Database.executeBatch(new GeotabContactRefreshBatch(geotabContactsApi));
					}
				}
			}
		}
	}
}