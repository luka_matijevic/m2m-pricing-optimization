/*
@@Description : If Stock's End Date  <= Three Months from Today, and Status = 'Active'
                Then Extensinding end date to 12 Months
*/
global class Extend_Past_EndDate implements Database.Batchable<sObject>{

    static final Date tdy  = system.today();
    global Database.QueryLocator start(Database.BatchableContext BC){
        Date third_Month_from_Now  = tdy.addMonths(3);
        string active = 'ACTIVE';
        string query = 'select id,VVL_date__c, Order_Request__c,Start_Date__c,End_Date__c,SM2M_Status__c from Stock__c where SM2M_Status__c = :active AND End_date__c <=:third_Month_from_Now and End_date__c !=null';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Stock__c> scope){
        if(scope != null && scope.size() > 0 ){
            for(Stock__c sim : scope){
                sim.VVL_date__c = sim.Potential_Termination_Date__c.addMonths(-3);
                
                //PM 23.3.2017.
                sim.Contract_Cycle_Type__c = 'Implicit';
                //End Of PM Change 23.3.2017.

                date prolongedDate = sim.Potential_Termination_Date__c.addMonths(12);
                //sim.End_date__c = prolongedDate;
                sim.Potential_Termination_Date__c = prolongedDate;
            }
            update scope;
        }
    }
    
    global void finish(Database.BatchableContext BC){

    }
}