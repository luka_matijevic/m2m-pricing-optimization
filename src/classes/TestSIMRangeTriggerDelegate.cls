@isTest
public class TestSIMRangeTriggerDelegate { 



	static testMethod void testInsertingSIMRange()
	{
		Test.startTest();
		SIM_Range__c range = new SIM_Range__c();
		range.Range_start__c = '1';
		range.Range_end__c = '3';
		insert range;

		range = new SIM_Range__c();
		range.Range_start__c = '1';
		range.Range_end__c ='1';
		try 
		{	        
			insert range;
		}
		catch (Exception e)
		{
			Boolean expectedException = e.getMessage().contains('Range overlaps range:') ;
			System.assertEquals(expectedException, true);
		}	
		range = new SIM_Range__c();
		range.Range_start__c = 'A';
		range.Range_end__c = '1';
		try 
		{	        
			insert range;
		}
		catch (Exception e)
		{
			Boolean expectedException = e.getMessage().contains('has to be a number.') ;
			System.assertEquals(expectedException, true);
		}	
		range = new SIM_Range__c();
		range.Range_start__c = '2';
		range.Range_end__c = '1';
		try 
		{	        
			insert range;
		}
		catch (Exception e)
		{
			Boolean expectedException = e.getMessage().contains('Range end has to be greater than range start.') ;
			System.assertEquals(expectedException, true);
		}

		range = new SIM_Range__c();
		range.Range_start__c = '0';
		range.Range_end__c = '3';
		try 
		{	        
			insert range;
		}
		catch (Exception e)
		{
			Boolean expectedException = e.getMessage().contains('Range overlaps range:') ;
			System.assertEquals(expectedException, true);
		}		
	
	}
}