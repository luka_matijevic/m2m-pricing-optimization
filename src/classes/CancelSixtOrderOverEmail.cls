global class CancelSixtOrderOverEmail implements Messaging.InboundEmailHandler
{
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope)
    {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        
        //private static Id gutschriftRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Gutschrift Case').getRecordTypeId();
        String emailSubject = '';
        String emailBody = '';
        String lvid = '';
        List <RecordType> recTypeSixt = [SELECT Id FROM RecordType where Name = 'SIXT' AND sObjecttype = 'Case']; //need to correct, RecordType can be fetched from cache (Adam wrote for SIXT)
        Id sixtRecordTypeId = ((RecordType)recTypeSixt.get(0)).Id;
        Integer daysDifference;
        Date todaysDate = DateTime.now().date();
        Case caseToCancel;
        
        emailBody = email.plainTextBody;
        emailSubject = email.subject;
		lvid = returnLVIDFromTheSubject(emailSubject);

		if (isThisCancellationEmail(emailSubject))
		{        
			try
			{
				caseToCancel = [SELECT Id, Sixt_LVID__c, Status, Sixt_Additional_details__c, CreatedDate, Category__c, Subcategory__c FROM Case WHERE Sixt_LVID__c = :lvid AND RecordTypeId = :sixtRecordTypeId LIMIT 1];
            
				if(caseToCancel != null)
				{
					daysDifference = caseToCancel.CreatedDate.date().daysBetween(todaysDate);
                
					if(daysDifference > 14)
					{
						//maybe we need to send an email response
						result.message = 'Order can be cancelled only within the first 14 days period.';
						result.success = false;
						return result;
					}
                
					CaseComment newCommentForCase = new CaseComment();
					newCommentForCase.CommentBody = emailBody;
					newCommentForCase.ParentId = caseToCancel.Id;
                
					insert newCommentForCase;
                
					caseToCancel.Status = 'Closed';
					if(caseToCancel.Category__c != null && caseToCancel.Category__c.equalsIgnoreCase('Sixt'))
						caseToCancel.Subcategory__c = 'Order Cancelled';
					caseToCancel.Sixt_Additional_details__c = String.format('{0} \n \n {1}', new String[] { caseToCancel.Sixt_Additional_details__c, emailBody });
					update caseToCancel;
					result.success = true;
				}
			}
			catch (QueryException e)
			{
        
			}
        
			if(result.success) {
				sendCancellationStatus(lvid, emailBody, sixtRecordTypeId);
			}
		}
		else
		{
			try
			{
				Date dateOfTermination;
				String dateOfTerminationString = returnTerminationDate(emailBody, 'Termination date');
				caseToCancel = [SELECT Id, Sixt_LVID__c, Status, Sixt_Additional_details__c, CreatedDate, Category__c, Subcategory__c, Sixt_Terminated__c,
								Sixt_Termination_date__c FROM Case WHERE Sixt_LVID__c = :lvid AND RecordTypeId = :sixtRecordTypeId LIMIT 1];
				
				if(caseToCancel != null)
				{
					dateOfTermination = Date.parse(dateOfTerminationString);

					CaseComment newCommentForCase = new CaseComment();
					newCommentForCase.CommentBody = emailBody;
					newCommentForCase.ParentId = caseToCancel.Id;

					insert newCommentForCase;

					//caseToCancel.Status = 'Closed and cancelled';
					caseToCancel.Sixt_Terminated__c = true;
					caseToCancel.Sixt_Termination_date__c = dateOfTermination;
					if(caseToCancel.Category__c != null && caseToCancel.Category__c.equalsIgnoreCase('Sixt'))
						caseToCancel.Subcategory__c = 'Order Terminated';
					caseToCancel.Sixt_Additional_details__c = String.format('{0} \n \n {1}', new String[] { caseToCancel.Sixt_Additional_details__c, emailBody });

					update caseToCancel;

					result.success = true;
				}
			}
			catch (QueryException e)
			{
				
			}
		}
        
        return result;
    }
    
    String returnLVIDFromTheSubject(String emailSubject) {
        String lvidToReturn = '';
        
        List<String> splittedString = emailSubject.split(':');
        
        if(splittedString != null && splittedString.size() > 0)
        {
            // take the part after ':'
            
            lvidToReturn = splittedString[1].trim();
        }
        
        return lvidToReturn;
    }

	Boolean isThisCancellationEmail(String emailSubject) {
		if (emailSubject.contains('Cancellation') || emailSubject.contains('cancellation') || emailSubject.contains('CANCELLATION'))
		{
			return true;
		}
		return false;
	}

	String returnTerminationDate(String emailBody, String valueForSearch) {
		String valueForReturn = '';
        Integer sizeOfSearchingValue = 0;
        
        if(emailBody == null || valueForSearch == null)
            return valueForReturn;
            
            
        String[] parsedEmail = emailBody.split('\n');
        
        if(parsedEmail != null && parsedEmail.size() > 0)
        {
            for(String row : parsedEmail)
            {
                if(row != null)
                {
                    if(row.equalsIgnoreCase('END'))
                        return valueForReturn;
                        
                    sizeOfSearchingValue = String.format('{0}{1}', new String[] {valueForSearch, ':'}).length();
                    
                    if(row.toLowerCase().contains(valueForSearch.toLowerCase()))
                    {
                        valueForReturn = row.substring(sizeOfSearchingValue);
                        valueForReturn = valueForReturn.trim();
                        valueForReturn = valueForReturn.replace(':', '');
						valueForReturn = valueForReturn.trim();
                    }
                }
            }
        }
        
        return valueForReturn;
	}
    
    @Future(callout=true)
    private static void sendCancellationStatus(String lvid, String emailBody, Id sixtRecordTypeId)
	{
	    String result = '';
	    String jsonBody = '';
	    String httpMethod = 'POST';
	    //String endPoint = 'https://logbook-acceptance.sixt.com/cancellation';
	    String endPoint = 'https://logbook.sixt.com/cancellation';
	    
	    if(lvid != null && lvid.length() > 0) {
            
            Map<String, String> returnJSONMap = new Map<String, String>();
	        returnJSONMap.put('Sixt_LVID__c', lvid);
	        returnJSONMap.put('Sixt_Additional_details__c', emailBody);
            
            jsonBody = JSON.serializePretty(returnJSONMap);
            
            HttpResponse resp = CalloutIntegration.executeRequest(endPoint, httpMethod, jsonBody, 32);
            
            if(resp != null)
            {
                // if success
                //if(resp.getStatusCode() == 200) {
                List <RecordType> recTypeSixtCaseClosed = [SELECT Id FROM RecordType where Name = 'SIXT Case Closed']; //need to correct, RecordType can be fetched from cache (Adam wrote for SIXT)
                Id recTypeSixtCaseClosedId = ((RecordType)recTypeSixtCaseClosed.get(0)).Id;
                Case caseToCancel = [SELECT Id, Sixt_LVID__c, Status, Sixt_Additional_details__c, CreatedDate, Category__c, Subcategory__c FROM Case WHERE Sixt_LVID__c = :lvid AND RecordTypeId = :recTypeSixtCaseClosedId LIMIT 1];
                
                if(caseToCancel != null) {
                    caseToCancel.Sixt_cancellation_confirmation__c = true;
                    caseToCancel.Sixt_Additional_details__c = String.format('{0} \n \n {1}', new String[] { caseToCancel.Sixt_Additional_details__c, resp.getBody() });
                    update caseToCancel;
                }
                //}
                /*else {
                    Case caseToCancel = [SELECT Id, Sixt_LVID__c, Status, Sixt_Additional_details__c, CreatedDate, Category__c, Subcategory__c FROM Case WHERE Sixt_LVID__c = :lvid AND RecordTypeId = :sixtRecordTypeId LIMIT 1];
                    
                    if(caseToCancel != null) {
                        caseToCancel.Sixt_Additional_details__c = resp.getBody();
                        update caseToCancel;
                    }
                }*/
            }
	    }
	}
}