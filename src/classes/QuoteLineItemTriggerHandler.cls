public class QuoteLineItemTriggerHandler {

	public static void handleAfterInsert(map<Id, QuoteLineItem> newQlis) {
		
  		set<Id> attributeIds = new set<Id>();
  		
  		for (QuoteLineItem qli : newQlis.values()) {
  			attributeIds.add(qli.cscfga__Attribute__c);
  		}
  		
  		// Mapping of configurator values to QLI fields
  		doConfiguratorMapping(newQlis.values(), attributeIds);
  	}
  	
  	/**
  	 *	Map values from the configurator to fields with same names as attributes.
  	 */
  	private static void doConfiguratorMapping(QuoteLineItem[] newQlis, set<Id> lineItemAttributeIds) {
  		
  		cscfga__Attribute__c[] lineItemAtts = [select Id, cscfga__Product_Configuration__c
  												from cscfga__Attribute__c
  												where Id IN :lineItemAttributeIds];
 		set<Id> configIds = new set<Id>();
 		for (cscfga__Attribute__c att : lineItemAtts) {
 			configIds.add(att.cscfga__Product_Configuration__c);
 		}
 		
  		map<Id, cscfga__Attribute__c> allRelatedAtts = new map<Id, cscfga__Attribute__c>([select Id, Name, cscfga__Value__c, cscfga__Product_Configuration__c, cscfga__Is_Line_Item__c
  												from cscfga__Attribute__c
  												where cscfga__Product_Configuration__c IN :configIds]);
  		
  		map<Id, cscfga__Attribute__c[]> configIdToAttributes = new map<Id, cscfga__Attribute__c[]>();
  		for (cscfga__Attribute__c att : allRelatedAtts.values()) {
  			if (!configIdToAttributes.containsKey(att.cscfga__Product_Configuration__c)) {
  				configIdToAttributes.put(att.cscfga__Product_Configuration__c, new cscfga__Attribute__c[] { att });
  			} else {
  				configIdToAttributes.get(att.cscfga__Product_Configuration__c).add(att);
  			}
  		}
  		
  		map<string, Schema.DescribeFieldResult> qliFieldsMap = SObjectHelper.getFieldDescriptions('QuoteLineItem');
  		map<string, Schema.DescribeFieldResult> qlidFieldLabelToDescriptionMap = new map<string, Schema.DescribeFieldResult>();
  		for (string fName : qliFieldsMap.keyset()) {
  			Schema.DescribeFieldResult fDesc = qliFieldsMap.get(fName);
  			qlidFieldLabelToDescriptionMap.put(fDesc.getLabel(), fDesc);
  		}
  		
  		QuoteLineItem[] qlisToUpdate = new QuoteLineItem[] {};
  		
  		for (QuoteLineItem qli : newQlis) {
  			
  			cscfga__Attribute__c lineItemAtt = allRelatedAtts.get(qli.cscfga__Attribute__c);
  			
  			if (lineItemAtt != null) {
	  			cscfga__Attribute__c[] relatedAtts = configIdToAttributes.get(lineItemAtt.cscfga__Product_Configuration__c);

				QuoteLineItem updatedQli = new QuoteLineItem(Id = qli.Id);
				boolean isUpdated = false;
	  			
	  			for (cscfga__Attribute__c att : relatedAtts) {
	  				if (att.cscfga__Is_Line_Item__c == false && qlidFieldLabelToDescriptionMap.containsKey(att.Name) && att.cscfga__Value__c != null && att.cscfga__Value__c != '') {
	  					Schema.DescribeFieldResult fDesc = qlidFieldLabelToDescriptionMap.get(att.Name);
	  					Schema.DisplayType fType = fDesc.getType();
	  					string fName = fDesc.getName();
	  					
	  					if (fType == Schema.DisplayType.Double || fType == Schema.DisplayType.Currency || fType == Schema.DisplayType.Percent) {
	  						updatedQli.put(fName, decimal.valueOf(att.cscfga__Value__c));
	  					} else if (fType == Schema.DisplayType.Integer) {
	  						updatedQli.put(fName, integer.valueOf(att.cscfga__Value__c));
	  					} else {
	  						updatedQli.put(fName, att.cscfga__Value__c);
	  					}
	  					
	  					isUpdated = true;
	  				}
	  			}
	  			
	  			if (isUpdated) {
	  				qlisToUpdate.add(updatedQli);
	  			}
  			}
  		}
 
 		update qlisToUpdate;
  	}
	
}