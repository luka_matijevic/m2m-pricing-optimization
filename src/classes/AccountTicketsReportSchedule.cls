global class AccountTicketsReportSchedule implements Queueable {

	public void execute(QueueableContext context) {
	    //createSeventhReport();
	    List<Billing_Account__c> baList = [SELECT Id, Activation_token__c, AccountId__c FROM Billing_Account__c WHERE Activation_token__c != null];
	    Set<String> accountIds = new Set<String>();
	    
	    for(Billing_Account__c ba : baList) {
	        accountIds.add(ba.AccountId__c);
	    }
	    
	    for(String accId : accountIds) {
		    Date currentDate = Date.today();
			String tmpDate = currentDate.toStartOfMonth().format().replace('.', '_');
			//Date fromDate = currentDate.addMonths(-1).toStartOfMonth(); // beginning of the previous month
			Date toDate = currentDate.toStartOfMonth().addDays(-1); //end of previous month
			List<Case> accountCases = [SELECT Id, Account.Name, CaseNumber, AccountId, CreatedDate, Owner.Name, ClosedDate, Subject, CreatedBy.Name, Contact.Name, Status FROM Case WHERE CreatedDate < :toDate AND AccountId = :accId]; //CreatedDate >= :fromDate AND 
			String generatedCSV = createReport(accountCases);
			if(generatedCSV != null) {
			    String encodedReport = EncodingUtil.base64Encode(Blob.valueOf(generatedCSV));
			    Blob b = EncodingUtil.base64Decode(encodedReport);
    			String fileName = accId + '_' + tmpDate;
    			String tmpLink = Label.CustomerTicketReportUploadLink + fileName;
    			CalloutIntegration.executeFileRequestCSV(tmpLink, 'POST', fileName, b, 32);
			}
	    }
	}

	static String createReport(List<Case> accountCases) {
	    final string csvLineFormat = '{0};{1};{2};{3};{4};\n';
	    String response = 'account_name;ticket_status;ticket_number;ticket_created_date;ticket_closed_date\n';
	    
	    for(Case c : accountCases) {
	        response += String.format(csvLineFormat, new List<string> { c.Account.Name, c.Status, c.CaseNumber, String.valueOf(c.CreatedDate), String.valueOf(c.ClosedDate) });
	    }
	    
	    return response;
	}
}