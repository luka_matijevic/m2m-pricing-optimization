global class PublishSubscriptionsACInboundProcessor implements csam_t1.InboundMessageProcessor{
	/**
    * Accepts all archiving messages (should be put before the standard Inbound Message Handler)
    * @param incomingMessage message to be processed
    * @return Boolean returns true
    */
    public Boolean isMessageProcessible(csam_t1__Incoming_Message__c incomingMessage) {     
        return incomingMessage.csam_t1__Incoming_URL_Path__c == '/csam_t1/callback/publishAC';

    }

    /*
   	* The constructor shouldn't do any processing, because the class is initialized for the isProcessible check
   	*/ 
    public PublishSubscriptionsACInboundProcessor() {
		
	}

	/**
    * Assumes that the message payload is a status message from integration platform
    * 
    * @param incomingMessage message to be processed with corresponding payload attachment
    * @param oldToNewIdsMap map of old-to-new ids used when inserting in chunks to pass the references between messages
    */
    public void processMessage(csam_t1__Incoming_Message__c incomingMessage, Map<Id, Id> oldToNewIdsMap) {
	    try{           
	        Attachment attachment = [SELECT Id, Name, Body
	                                    FROM Attachment
	                                    WHERE ParentId = :incomingMessage.Id
	                                    AND Name like 'Message%' LIMIT 1];
			String body = attachment.Body.toString();
						

			csam_t1__Outgoing_Message_Record__c outgoingRecord = [select csam_t1__Object_Record_Id__c 
																from csam_t1__Outgoing_Message_Record__c 
																where csam_t1__Outgoing_Message__c = :incomingMessage.csam_t1__Outgoing_Message__c];
			
			/*Attachment att = [select parentId 
								from Attachment  
								where id = :outgoingRecord.csam_t1__Object_Record_Id__c];	*/

			Third_Upload_File__c tuf = [select id, Order_Request__c, Upload_to_SM2M_Status__c, Publish_Subscriptions_Status__c
										from Third_Upload_File__c 
										where id = :outgoingRecord.csam_t1__Object_Record_Id__c];	

			//deserialize response
	       	CallbackMessage cm = (CallbackMessage) JSON.deserialize(attachment.Body.toString(), CallbackMessage.class);

			if (cm.objectsToUpdate != null && cm.objectsToUpdate.size() > 0) {
				processUpdates(cm);	
				updateActivationCenterObject(cm, tuf);
			}
			incomingMessage.csam_t1__Status__c = 'Processed';

		}catch (Exception e) {
			incomingMessage.csam_t1__Status__c = 'Process Error';

			Attachment errorAttach = new Attachment();
			errorAttach.Body = Blob.valueOf(e.getStackTraceString());
			errorAttach.parentId = incomingMessage.Id;
			
			String fullName = 'Error_' + e.getMessage();
			errorAttach.Name = fullName.abbreviate(255);
			
			insert errorAttach;
		}
			
		incomingMessage.csam_t1__Processed_At__c = System.now();
		update incomingMessage;
	}

	/**
	 * @description Processes all updates defined by CallbackMessage.objectsToUpdate. First objects are separated based
	 * on sObject type. Then, to prevent different sObject types in single collection and corresponding chunking limits,
	 * each object type collection is separately updated.
	 * @param cm CallbackMessage
	 */
	private void processUpdates(CallbackMessage cm) {
		Map<String, List<sObject>> objectsToUpdateMap = new Map<String, List<sObject>>();
		for (sObject objectToUpdate : cm.objectsToUpdate) {
			String objectToUpdateName = objectToUpdate.getSObjectType().getDescribe().getName();
			if (!objectsToUpdateMap.containsKey(objectToUpdateName)) {
				objectsToUpdateMap.put(objectToUpdateName, new List<sObject>());
			}
			objectsToUpdateMap.get(objectToUpdateName).add(objectToUpdate);
		}

		// avoid updates in chunks with different sObject types in a collection
		for (String key : objectsToUpdateMap.keySet()) {
			update objectsToUpdateMap.get(key);
		}
	}

	private void updateActivationCenterObject(CallbackMessage cm, Third_Upload_File__c tuf){
		Map<String, List<sObject>> objectsToUpdateMap = new Map<String, List<sObject>>();
		Set<Id> tufIds = new Set<Id>();
		Boolean publishOk = true;

		for (sObject objectToUpdate : cm.objectsToUpdate) {
			String objectToUpdateName = objectToUpdate.getSObjectType().getDescribe().getName();
			if (!objectsToUpdateMap.containsKey(objectToUpdateName)) {
				objectsToUpdateMap.put(objectToUpdateName, new List<sObject>());
			}
			objectsToUpdateMap.get(objectToUpdateName).add(objectToUpdate);
		}

		List<Activation_Centre__c> actCenterList = [SELECT id, name, Status__c
											FROM Activation_Centre__c
											WHERE m2m_order_request__c =:tuf.Order_Request__c];

		Activation_Centre__c actCenter;
		if (actCenterList != null && actCenterList.size() > 0){
			actCenter = actCenterList[0];
		}

 		for(String key : objectsToUpdateMap.keySet()){
			List<Stock__c> stockList = (List<Stock__c>)objectsToUpdateMap.get(key);
			if (stockList != null && stockList.size()>0){
				for (Stock__c stock : stockList){
					if (publishOk && !stock.Publish_AC_Response__c.equalsIgnoreCase('PUBLISHED')){
						publishOk = false;
					}
				}
			}
		}

		if (publishOk){
			if (actCenter != null){
				actCenter.Status__c = 'Published';
			}
			tuf.Publish_Subscriptions_Status__c = 'Completed';
		}else{
			if (actCenter != null){
				actCenter.status__c = 'Publish Error';
			}
			tuf.Publish_Subscriptions_Status__c = 'Publish Error';
		}
		if (actCenter != null){
			update actCenter;
		}
		update tuf;
	}
}