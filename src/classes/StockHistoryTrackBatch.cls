/*
 * Description: Batch class to update Stock records HistoryTrack field (batch will be executed as one time script to sync existing records).
 * MAKE SURE TO BYPASS TRIGGERS & VALIDATION USING CUSTOM SETTINGS DATA (AS REQUIRED).
 * Implemented to update the existing records in system with History Track. If Activation Date != null -->Add row in HistoryTracking: ACTIVE|Activation Date 
 * and similarly for "DEACTIVATION/ Deactivation Date"
 * Author: Ritesh Saxena
 * Date: 13/11/2014
*/
public class StockHistoryTrackBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    private static String query = 'Select Id, SM2M_Status__c, HistoryTracking__c, Activation_Date__c, Deactivation_Date__c, State_Change_Date__c from Stock__c where (SM2M_Status__c=\'SUSPENDED\' OR Activation_Date__c!=null OR Deactivation_Date__c!=null)';
    
    /*
    * Start call to process SIM record.
    */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        if (test.isRunningTest()) {
            query+= ' limit 25';
        }
        return Database.getQueryLocator(query);  
    }
    
    /*
    * Execute call to process SIM record.
    */
    public void execute(Database.BatchableContext BC, List<SObject> scope) {
        List<Stock__c> stockList = new List<Stock__c>();
        for (Stock__c st: (List<Stock__c>) scope) {
            st.HistoryTracking__c = '';
            if (st.Activation_Date__c != null){
                st.HistoryTracking__c =  '' + 'ACTIVE' + '|' + st.Activation_Date__c 
                + '\n' + (st.HistoryTracking__c == null ?'' : st.HistoryTracking__c);
            }
            if (st.Deactivation_Date__c != null) {  
                st.HistoryTracking__c =  '' + 'DEACTIVATED' + '|' + st.Deactivation_Date__c 
                + '\n' + (st.HistoryTracking__c == null?'' : st.HistoryTracking__c);
            }
            if (st.SM2M_Status__c != null && st.SM2M_Status__c.equalsIgnoreCase('SUSPENDED')) {
                if (st.State_Change_Date__c != null) {
                    st.HistoryTracking__c =  '' + 'SUSPENDED' + '|' + st.State_Change_Date__c
                    + '\n' + (st.HistoryTracking__c == null?'' : st.HistoryTracking__c);
                }
            }
            stockList.add(st);
        }
        if (!stockList.isEmpty()) {           
            update stockList;
        }
    }
    
    /*
    * Finish call to process SIM record.
    */
    public void finish(Database.BatchableContext BC) {

        //Nothing to-do            
    }
}