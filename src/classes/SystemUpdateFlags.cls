public class SystemUpdateFlags {
	// Description      :   Holds the flags that prevent Self Reference from trigger errors or prevent trigger from getting executed
	
	public static Boolean NoOpportunityTrigger;
	
	static
	{
		NoOpportunityTrigger=false;
		
	}
	
}