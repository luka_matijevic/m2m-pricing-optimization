//------------------------------------------------------------------------------
//  Test ConnectivityConfigurations functionality
//------------------------------------------------------------------------------
@isTest
private class TestConnectivityConfigurations
{
	//--------------------------------------------------------------------------
	// Test of the removing product configurations
	//--------------------------------------------------------------------------
    static testMethod void testCreateAndDeleteProductConfigurations()
	{/* Commented whole method - pavan
		//
		// Prepare data
		//
		TestHelper.createTariffs();
		TestHelper.createProductDefinitions();
		Id clonedTariffId = [ SELECT Id FROM Tariff__c WHERE Parent_Tariff__c != null ].Id;

		//
		// Create product configurations from cloned tariff
		//
		ITariff tariff = Factory.createTariff(clonedTariffId);
		tariff.getAdditionalTariffOptions()[0].setQuantity(3);
		tariff.getSupplementaryServiceTariffOptions()[0].setQuantity(49);
		IConnectivityConfigurations configurations = new ConnectivityConfigurations(null);
		//String uniqueKey = configurations.createProductConfigurations(tariff);
		//
		// Test creating product configurations
		//
		//TestHelper.checkCreatedProductConfigurations(clonedTariffId, uniqueKey);

		//
		// Delete created product configurations
		//
		configurations.deleteProductConfigurations();
		//
		// Test deleting created product configurations
		//
		List<cscfga__Product_Configuration__c> productConfigurations =
			[ SELECT Id FROM cscfga__Product_Configuration__c ];
		system.assertEquals(0, productConfigurations.size());
	*/}

	//--------------------------------------------------------------------------
	// Test of the removing products
	//--------------------------------------------------------------------------
	static testMethod void testDeleteExistingProductConfigurations()
	{/* Commented whole method - pavan
		//
		// Create test product configurations
		//
		TestHelper.createProductConfigurations();
		Id configurationId =
			[ SELECT Id
			  FROM cscfga__Product_Configuration__c
			  WHERE Parent_Configuration__c = null].Id;
		//
		// Connectivity configurations
		//
		IConnectivityConfigurations configurations =
			new ConnectivityConfigurations(configurationId);
		configurations.deleteProductConfigurations();

		//
		// Test
		//
		List<cscfga__Product_Configuration__c> configs =
			[ SELECT Id, Name
			  FROM cscfga__Product_Configuration__c ];
		system.assertEquals(1, configs.size());
		system.assertEquals('SIM & Tariff', configs[0].Name);
	*/}

	//--------------------------------------------------------------------------
	// Test of the setting quantities to tariff options
	//--------------------------------------------------------------------------
	static testMethod void testSetTariffOptionsQuantities()
	{
		//
		// Create test product configurations
		//
		TestHelper.createProductConfigurations();
		Id configurationId =
			[ SELECT Id
			  FROM cscfga__Product_Configuration__c
			  WHERE parent_product_configuration__c = null ].Id;
		Id tariffId =
			[ SELECT Id
			  FROM Tariff__c
			  WHERE Parent_Tariff__c != null ].Id;
		Id parentTariffId =
			[ SELECT Id
			  FROM Tariff__c
			  WHERE Parent_Tariff__c = null ].Id;
		ITariff tariff = Factory.createTariff(tariffId);
		ITariff parentTariff = Factory.createTariff(parentTariffId);
		tariff.deepMerge(parentTariff);
		//
		// Connectivity configurations
		//
		IConnectivityConfigurations configurations =
			new ConnectivityConfigurations(configurationId);
		configurations.setQuantities(tariff);
		//
		// Test
		//
		system.assertEquals(4, tariff.getAdditionalTariffOptions()[0].getQuantity());
		system.assertEquals(2, tariff.getSupplementaryServiceTariffOptions()[0].getQuantity());
	}
}