global class ThirdUploadFileGeneratorWebService {

	webService static String createThirdUploadCSV(Id thirdUploadFileId) {
		try	{
			// generate TUF
			Attachment attach = new Attachment();
			attach.ParentId = thirdUploadFileId;
			attach.Name = 'ThirdUploadFile.csv';
			attach.Body = ThirdUploadFileCSVGenerator.createThirdUploadFileCSV(thirdUploadFileId);
			insert attach;
			return 'Third Upload File CSV was successfully generated';
		}
		catch(exception ex)	{
			return ex.getMessage();
		}
	}
	
	webService static void updateAccountOnStock(Id thirdUploadFileId) {
        try{
			
    		List<Third_Upload_File__c> currentTuf = [SELECT Id, Account__c FROM Third_Upload_File__c WHERE Id = :thirdUploadFileId];
    		
    		if(currentTuf != null && currentTuf.size() > 0) {
		        List<Stock__c> stocks = [SELECT Id, Account__c, Third_Upload_File__c FROM Stock__c WHERE Third_Upload_File__c =:thirdUploadFileId];
		
        		if(stocks != null && stocks.size() > 0)
        		{
        		    for(Stock__c stock : stocks) {
        		        stock.Account__c = currentTuf[0].Account__c;
        		    }
        		    
        		    update stocks;
        		}
		    }
		} catch(Exception e){
		}
    }
    
	webService static String sendToHeroku(Id thirdUploadFileId){
		try{
			List<Attachment> attach = [
				SELECT 
					Id, ParentId, Name, Description, CreatedDate, ContentType, Body, IsDeleted
				FROM 
					Attachment
				WHERE 
					isDeleted = false
				AND 
					ParentId = :thirdUploadFileId
				ORDER BY CreatedDate DESC
				LIMIT 1
			];
			
			if(attach == null || attach.size() == 0){
				return 'No attachment found related to this Third Upload File record. Please generate the CSV first!';
			}
			
			csam_t1.ObjectGraphCalloutHandler.AdditionalObjects addObjects = new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects();
    		addObjects.typeName = 'Third_Upload_File__c';
    		addObjects.ids = new Id[]{thirdUploadFileId};
    		
    		List<Third_Upload_File__c> currentTuf = [SELECT Id, SIM_swap_Ticket__c FROM Third_Upload_File__c WHERE Id = :thirdUploadFileId];
    		
    		if(currentTuf != null && currentTuf.size() > 0) {
    		    if(currentTuf[0].SIM_swap_Ticket__c != null) {
    		        List<Stock__c> stocks = [SELECT Id, Status__c, Third_Upload_File__c FROM Stock__c WHERE Third_Upload_File__c =:thirdUploadFileId];
    		
            		if(stocks != null && stocks.size() > 0)
            		{
            		    for(Stock__c stock : stocks) {
            		        stock.Status__c = 'Pending activation';
            		    }
            		    
            		    update stocks;
            		}
    		    }
    		}
    		
			String response = csam_t1.ObjectGraphCalloutHandler.createAndSendExtended('Upload TUF', attach.get(0).id, new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects[]{addObjects});//'Third Upload File CSV was successfully sent';
			
			Third_Upload_File__c tuf = [SELECT Id, Upload_to_SM2M_Status__c FROM Third_Upload_File__c WHERE Id =:thirdUploadFileId];
			tuf.Upload_to_SM2M_Status__c = 'Pending';
			update tuf;
			return response;
		} catch(Exception e){
			return e.getMessage();
		}
	}

	webService static String sendToHerokuActivationCentre(Id thirdUploadFileId){
		try{
			List<Attachment> attach = [
				SELECT 
					Id, ParentId, Name, Description, CreatedDate, ContentType, Body, IsDeleted
				FROM 
					Attachment
				WHERE 
					isDeleted = false
				AND 
					ParentId = :thirdUploadFileId
				ORDER BY CreatedDate DESC
				LIMIT 1 
			];
			
			if(attach == null || attach.size() == 0){
				return 'No attachment found related to this Third Upload File record. Please generate the CSV first!';
			}
			
			csam_t1.ObjectGraphCalloutHandler.AdditionalObjects addObjects = new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects();
    		addObjects.typeName = 'Third_Upload_File__c';
    		addObjects.ids = new Id[]{thirdUploadFileId};
    		
    		List<Third_Upload_File__c> currentTuf = [SELECT Id, SIM_swap_Ticket__c FROM Third_Upload_File__c WHERE Id = :thirdUploadFileId];
    		
    		if(currentTuf != null && currentTuf.size() > 0) {
    		    if(currentTuf[0].SIM_swap_Ticket__c != null) {
    		        List<Stock__c> stocks = [SELECT Id, Status__c, Third_Upload_File__c FROM Stock__c WHERE Third_Upload_File__c =:thirdUploadFileId];
    		
            		if(stocks != null && stocks.size() > 0)
            		{
            		    for(Stock__c stock : stocks) {
            		        stock.Status__c = 'Pending activation';
            		    }
            		    
            		    update stocks;
            		}
    		    }
    		}
    		
			String response = csam_t1.ObjectGraphCalloutHandler.createAndSendExtended('Upload TUF Activation Centre', attach.get(0).id, new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects[]{addObjects});//'Third Upload File CSV was successfully sent';
			
			Third_Upload_File__c tuf = [SELECT Id, Upload_to_SM2M_Status__c FROM Third_Upload_File__c WHERE Id =:thirdUploadFileId];
			tuf.Upload_to_SM2M_Status__c = 'Pending';
			update tuf;
			return response;
		} catch(Exception e){
			return e.getMessage();
		}
	}
	

}