@isTest
private class BillingPeriodTest {

    static testMethod void testPartial() {

    	Date startDate = Date.newInstance(2014, 3, 20);
    	Date endDate = Date.newInstance(2014, 4, 5);
        BillingPeriod bp = new BillingPeriod(startDate, endDate);
        System.assertEquals(true, bp.isInTwoMonths());
        System.assertEquals(bp.length(), 16);
        System.assertEquals(bp.weightedAmount(100).setScale(3, System.Roundingmode.FLOOR), Decimal.valueOf('52.043'));

    }

    static testMethod void testFull(){

    	Date startDate = Date.newInstance(2014, 4, 10);
    	Date endDate = Date.newInstance(2014, 4, 20);
        BillingPeriod bp = new BillingPeriod(startDate, endDate);
        System.assertEquals(false, bp.isInTwoMonths());
        System.assertEquals(10, bp.length());
        System.assertEquals(Decimal.valueOf('10'), bp.weightedAmount(30).setScale(3, System.Roundingmode.FLOOR));

    }

}