/**
 * @description REST service used to pull SFDC data
 * 
 * @author Hrvoje Tutman, modifications Ilija Pavlic
 */
@RestResource(urlMapping='/Stock/v1/*')
global class StockServiceV1 {

    private static final Integer SOBJECTS_PER_PAGE = 200;
    private static final String STOCK = 'Stock__c';
    
    /**
     * @description Method handles all GET request for data enhancements.
     */
    @HttpGet
    @RemoteAction
    @readOnly
    global static RestResponse doGet() {
        RestRequest req = RestContext.request;
        String reqUri = req.requestURI;
        
        String host = req.headers.get('Host');
        
        system.debug(RestContext.request.headers.get('CS-Mac'));
        system.debug(reqUri);
        
        boolean verified = csam_t1.SecurityUtils.verifyCallinSignature(
            Blob.valueOf(reqUri),
            RestContext.request.headers.get('CS-Mac')
        );

        if (verified) {
            // security check OK
            system.debug('OK');
            RestContext.response.statusCode = 200;
            
            Map<String, Schema.SObjectField> fieldMap = fieldMap(STOCK);
            Set<String> stockFields = fieldNames(fieldMap);
            
            List<String> urlQueryOptions = new List<String>();
            List<String> filteringOptions = new List<String>();
            Boolean trialFilter = true;
            for(String queryKey : req.params.keySet()){
                if(queryKey.equalsIgnoreCase('account__c') || queryKey.equalsIgnoreCase('account'))
                    trialFilter = false;
            }
            for (String queryKey : req.params.keySet()) {
                /* KK - will be enabled once new code is deployed to Heroku
                if(queryKey.equalsIgnoreCase('trial') || queryKey.equalsIgnoreCase('trial__c')){
                    String filteringValue = req.params.get(queryKey);
                    if(filteringValue.equalsIgnoreCase('yes') || filteringValue.equalsIgnoreCase('true')) trialFilter = true;
                    urlQueryOptions.add(queryKey + '=' + System.EncodingUtil.urlEncode(filteringValue,'UTF-8'));
                } 
                */
                if (stockFields.contains(queryKey)) {
                    trialFilter = false;
                    Schema.SObjectField sObjectField = fieldMap.get(queryKey);
                    String filteringValue = req.params.get(queryKey);
                    
                    urlQueryOptions.add(queryKey + '=' + System.EncodingUtil.urlEncode(filteringValue, 'UTF-8'));

                    if (isNumberType(sObjectField)) {
                        filteringOptions.add(queryKey + ' = ' + filteringValue);
                    } else {
                        if (filteringValue.contains('%')) {
                            filteringOptions.add(queryKey + ' LIKE \'' + filteringValue + '\'');
                        } else {
                            filteringOptions.add(queryKey + ' = \'' + filteringValue + '\'');
                        }
                    }
                }
            }
    
            Integer totalResults;
    
            if (filteringOptions.isEmpty()) {
                //totalResults = Database.countQuery('SELECT Count() FROM ' + STOCK);
                if(BIC_Report_Record_Size__c.getOrgDefaults().Number_Of_SIMs__c != null) {
                    totalResults = (Integer) BIC_Report_Record_Size__c.getOrgDefaults().Number_Of_SIMs__c;
                }
                else{
                    totalResults = Database.countQuery('SELECT Count() FROM ' + STOCK);
                }
            } else {
                totalResults = Database.countQuery(String.format('SELECT Count() FROM {0} WHERE {1}', new List<String> {STOCK, String.join(filteringOptions, ' AND ')}));
                //if (totalResults==40000) totalResults = 144000;
            }
            
            Integer totalPages = totalPages(totalResults);
            Integer currentPage = getPageNumber(req.params, totalPages);

            Integer offset = SOBJECTS_PER_PAGE*(currentPage-1);
    
            String soqlQuery;
            if (req.params.containsKey('theLastId') && isId(req.params.get('theLastId'))) {
                if (filteringOptions.isEmpty()) {
                    soqlQuery = String.format('SELECT {0} FROM {1} WHERE Id > {2} ORDER BY Id LIMIT {3}', new List<String> {String.join(new List<String>(stockFields), ', '), STOCK, '\'' + req.params.get('theLastId') + '\'', ''+SOBJECTS_PER_PAGE});
                } else {
                    filteringOptions.add('Id > \'' + req.params.get('theLastId') + '\'');
                    soqlQuery = String.format('SELECT {0} FROM {1} WHERE {2} ORDER BY Id LIMIT {3}', new List<String> {String.join(new List<String>(stockFields), ', '), STOCK, String.join(filteringOptions, ' AND '), ''+SOBJECTS_PER_PAGE});
                }
            } else {
                if (filteringOptions.isEmpty()) {
                    soqlQuery = String.format('SELECT {0} FROM {1} ORDER BY Id LIMIT {2} OFFSET {3}', new List<String> {String.join(new List<String>(stockFields), ', '), STOCK, ''+SOBJECTS_PER_PAGE, ''+(SOBJECTS_PER_PAGE*(currentPage-1))});
                } else {
                    soqlQuery = String.format('SELECT {0} FROM {1} WHERE {2} ORDER BY Id LIMIT {3} OFFSET {4}', new List<String> {String.join(new List<String>(stockFields), ', '), STOCK, String.join(filteringOptions, ' AND '), ''+SOBJECTS_PER_PAGE, ''+(SOBJECTS_PER_PAGE*(currentPage-1))});
                }
            }
        
            String queryAddon;
            if (urlQueryOptions.isEmpty()) {
                queryAddon = '';
            } else {
                queryAddon = '&' + String.join(urlQueryOptions, '&');
            }
    		system.debug(soqlQuery);
            List<sObject> dataResult = Database.query(soqlQuery);
            system.debug(dataResult.size());
            String theLastId = dataResult.isEmpty() ? null : (String) dataResult.get(dataResult.size() - 1).get('Id');
            
            
            if(trialFilter) {
                dataResult = StockServiceHelper.filterTrialSObjects(dataResult);
            }

            RestStockQueryResponse response = restStockQueryResponseFactory(
                new Header(totalResults, currentPage, totalPages, SOBJECTS_PER_PAGE), 
                    totalResults == 0 ? new Link() : new Link('https://' + host + '/services/apexrest' + reqUri + '?pageNumber=1' + queryAddon, 'GET'), 
                    totalResults == 0 ? new Link() : currentPage == 1 ? new Link() : new Link('https://' + host + '/services/apexrest' + reqUri + '?pageNumber=' + (currentPage-1) + queryAddon, 'GET'), 
                    totalResults == 0 ? new Link() : currentPage == totalPages ? new Link() : new Link('https://' + host + '/services/apexrest' + reqUri + '?pageNumber=' + (currentPage+1) + (theLastId == null ? '' : '&theLastId=' + theLastId) + queryAddon, 'GET'), 
                    totalResults == 0 ? new Link() : new Link('https://' + host + '/services/apexrest' + reqUri + '?pageNumber=' + totalPages + queryAddon, 'GET'),
                    dataResult);
            return response;

        } else {
            // the request is not signed properly
            RestContext.response.statusCode = 403;
            return null; 
        }
    }

    /**
     * Gets page number from request parameters. If no <em>pageNumber</em> key is found, 1 is returned.
     * @param reqParams request parameters as a Map
     * @param totalPages total number of pages
     * @return pageNumber value, or 1 if not found
     */
    private static Integer getPageNumber(Map<String, String> reqParams, Integer totalPages) {
        if (reqParams.containsKey('pageNumber')) {
            try {
                Integer currentPage = Integer.valueOf(reqParams.get('pageNumber'));

                if (currentPage < 1) {
                    currentPage = 1;
                }
        
                if (currentPage > totalPages) {
                    currentPage = totalPages;
                }
                
                return currentPage;
            } catch (System.TypeException e) {
                return 1;
            }
        } else {
            return 1;
        }
    }
    
    /**
     * Verifies whether the String parameter is valid Salesforce.com Id.
     * @param input String to verify
     * @return true if the input String is valid Salesforce.com Id
     */
    private static boolean isId(String input) {
        try {
            Id idTest = (Id) input;
            return true;
        } catch (System.StringException e) {
            return false;
        }
    }

    /**
     * Verifies whether the String parameter is valid sObject Type name.
     * @param input String to verify
     * @return true if the input String is valid sObject type name
     */
    private static boolean isSObjectTypeName(String input) {
        return Schema.getGlobalDescribe().containsKey(input);
    }

    global class RestStockQueryResponse extends RestResponse {
        public List<SObject> results {get; set;}
        public Navigation navigation {get; set;}
    }
    
    global abstract class RestResponse {
    }
    

    /**
     * Navigation data response with headers and links.
     */
    global class Navigation {
        public Header header {get; set;}
        public Links links {get; set;}
    }
    
    /**
     * Navigation header with pagination parameters.
     */
    global class Header {
        public Integer totalResults {get; set;}
        public Integer pageNumber {get; set;}
        public Integer totalPages {get; set;}
        public Integer resultsPerPage {get; set;}
        
        Header(Integer totalResults, Integer pageNumber, Integer totalPages, Integer resultsPerPage) {
            this.totalResults = totalResults;
            this.pageNumber = pageNumber;
            this.totalPages = totalPages;
            this.resultsPerPage = resultsPerPage;
        }
    }
    
    /**
     * Navigation Links (first, previous, next, last).
     */
    global class Links {
        public Link first {get; set;}
        public Link prev {get; set;}
        public Link next {get; set;}
        public Link last {get; set;}
    }
    
    /**
     * Navigation link with href and verb defined.
     */
    global class Link {
        public String href {get; set;}
        public String verb {get; set;}
        
        /**
         * Default constructor.
         */
        Link() {
        }
        
        /**
         * Constructor taking href and verb fields.
         * @param href URI of the link.
         * @param verb HTTP Method to use for the URI
         */
        Link(String href, String verb) {
            this.href = href;
            this.verb = verb;
        }
    }

    private static RestStockQueryResponse restStockQueryResponseFactory(Header header, Link firstLink, Link prevLink, 
            Link nextLink, Link lastLink, List<SObject> results) {
        RestStockQueryResponse response = new RestStockQueryResponse();
        response.navigation = new Navigation();
        response.results = results;
        
        response.navigation.header = header;
        response.navigation.links = new Links();

        response.navigation.links.first = firstLink;
        response.navigation.links.prev = prevLink;
        response.navigation.links.next = nextLink;
        response.navigation.links.last = lastLink;
        system.debug(response);
        return response;
    }

    private static Map<String, Schema.SObjectField> fieldMap(String objectName) {
        return Schema.getGlobalDescribe().get(STOCK).getDescribe().fields.getMap();
    }

    private static Set<String> fieldNames(Map<String, Schema.SObjectField> fieldMap) {
        Set<String> fieldNames = new Set<String>();
        for (Schema.SObjectField field : fieldMap.values()) {
                fieldNames.add(field.getDescribe().getName());
        }
        return fieldNames;
    }

    private static Boolean isNumberType(SObjectField field) {
        Schema.DisplayType t = field.getDescribe().getType();
        return t == Schema.DisplayType.Boolean || t == Schema.DisplayType.Currency || t == Schema.DisplayType.Double || t == Schema.DisplayType.Integer || t == Schema.DisplayType.Percent;
    }

    private static Integer totalPages(Integer totalResults) {
        Integer totalPages = (Integer) System.Math.ceil(1.0 * totalResults / SOBJECTS_PER_PAGE);
        return (totalPages == 0) ? 1 : totalPages;
    }     

}