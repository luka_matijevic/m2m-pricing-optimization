@isTest
private class TestCreateFrameContractController{
    //  Test for CreateFrameContractController
    static testMethod void testcreateFrameContractError(){
    	
    	
    	
        Account account = new Account(
        	Name = 'Test',
            Type = 'Business',
            BillingCountry ='Germany',
            Global__c = true
        	);
        insert account;
        Contact con = Test_Util.createContact(account.id);
        
        con.Country__c = 'Germany';
        insert con;
    	Double monthlyRevenue = 1.0;   
      
    	Opportunity opp  = new Opportunity(
        	Name = 'Test opp',
       	    AccountId = account.Id,
        	CloseDate = Date.newInstance(2013, 1, 1),
       	    StageName = 'Closed Won',
        	Win_Reason__c = 'Price',
        	Contract_Term_Per_SIM__c = 12,
        	Monthly_Revenue_Per_SIM__c = 1.0,
            Description ='Test Test Test Test Test Test Test Test '
        	
   		);
   		opp.RecordTypeId = [Select Id FROM RecordType WHERE SobjectType = 'Opportunity' AND Name = 'Commercial Global'][0].Id;
    	insert opp;
    	
        OpportunityContactRole oppContactRole = new OpportunityContactRole(
            ContactId = con.Id,
            OpportunityId = opp.Id,
            Role = 'Business User'
        );
        insert oppContactRole;

        cscfga__Product_Bundle__c bundle = new cscfga__Product_Bundle__c(
            Name = 'Test',
            cscfga__Synchronised_with_Opportunity__c = true,
            cscfga__Opportunity__c = opp.Id
        );

        insert bundle;
    	
        PageReference newPage = Page.CreateFrameContract;
        Test.setCurrentPage(newPage);
        System.currentPageReference().getParameters().put('id', opp.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        CreateFrameContractController cont = new CreateFrameContractController(sc);
        
        list<Frame_Contract__c> fcs = [select Id, Opportunity__c from Frame_Contract__c where Opportunity__c = :opp.Id];
        System.assertEquals(fcs.IsEmpty(),true);
        
        Frame_Contract__c fc = new Frame_Contract__c(Opportunity__c = opp.Id,
                             Valid_From__c = opp.CloseDate,
                             Product_Bundle__c = opp.Synched_Bundle_Id__c,
                             Customer__c = opp.AccountId);
		/*
        if(opp.RecordTypeId == Cache.RecordTypeId('Opportunity.Trial Local') || opp.RecordTypeId == Cache.RecordTypeId('Opportunity.Trial Global')){
				fc.RecordTypeId = Cache.RecordTypeId('Frame_Contract__c.Trial');
			}
			else if(opp.RecordTypeId == Cache.RecordTypeId('Opportunity.Commercial Local') || opp.RecordTypeId == Cache.RecordTypeId('Opportunity.Commercial Global')){
				fc.RecordTypeId = Cache.RecordTypeId('Frame_Contract__c.Commercial');
			}
		*/
		
		if(opp.RecordTypeId == Cache.RecordTypeId('Opportunity.Trial Local') || opp.RecordTypeId == Cache.RecordTypeId('Opportunity.Trial Global')){
				fc.RecordTypeId = Cache.RecordTypeId('Frame_Contract__c.Trial');
			}
			else if(opp.RecordTypeId == Cache.RecordTypeId('Opportunity.Commercial Local')){
				fc.RecordTypeId = Cache.RecordTypeId('Frame_Contract__c.Commercial Local');
			}
			else if (opp.RecordTypeId == Cache.RecordTypeId('Opportunity.Commercial Global')){
				fc.RecordTypeId = Cache.RecordTypeId('Frame_Contract__c.Commercial');
			}	
		
        insert fc;
        
        // Check if Error page is returned if opportunity already has related frame contract, 
        System.assertEquals(cont.createFrameContract().getUrl(),Page.CreateFrameContractError.getUrl());
              
        
        delete fc;
        
       
        
        // Testing if createFrameContract() and sendEmailToBSS() work as intended, RecordType = Record Type = Commercial Global'
        PageReference testPage = cont.createFrameContract();
        fc = [SELECT id,RecordTypeId FROM Frame_Contract__c WHERE Opportunity__c = :opp.id];
        System.assertEquals(fc.RecordTypeId,Cache.RecordTypeId('Frame_Contract__c.Commercial') );
        System.assertEquals(testPage.getUrl(), '/' + fc.Id);
        
   }      
   
}