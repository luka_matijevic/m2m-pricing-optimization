/**
	* @author: Petar Matkovic
*/
global class RetireSIMsBatchScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		//MyBatchClass b = new MyBatchClass();
		//database.executebatch(b);
		Database.executeBatch(new RetireSIMsBatch());
	}
}