/**
 * @description Error response class used when Integration error occurs. The integration server should send error
 * response formatted as ErroResponse in such case.
 * @author Hrvoje Tutman
 */
public class ErrorResponse {
    
    public ErrorResponse() {}
    
    public ErrorResponse(ErrorResponse errorResponse) {
        this();
        this.currentPayload = errorResponse.currentPayload;
        this.errorMessage = errorResponse.errorMessage;
        this.stackTrace = errorResponse.stackTrace;
    }
    /**
     * Integration server current payload (when an exception occured) serialized in JSON string
     */
    public String currentPayload {get; set;}

    /**
     * Integration server error message
     */
    public String errorMessage {get; set;}

    /**
     * Full exception stack trace from the Integration server
     */
    public String stackTrace {get; set;}
}