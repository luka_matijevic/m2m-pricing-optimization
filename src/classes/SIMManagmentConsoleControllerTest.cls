@isTest
public class SIMManagmentConsoleControllerTest {
    static testMethod void SIMManagmentConsole(){
    	
    insert new Case_Email_Status_Settings__c(
			Poller_Delay_Seconds__c = 20
		);
    insert new BIC_Report_Settings__c(BIC_Report_Poller_Delay_Seconds__c = 30);
    Billing_Settings__c billingSettings = new Billing_Settings__c(
			OFI_email_address__c = 'ilija.pavlic@cloudsense.com',
			Status_reporting_email__c = 'ilija.pavlic@cloudsense.com',
			Billing_Cycle_Offset__c = 2,
			Prebill_Poller_Delay_Seconds__c = 20
		);
		
		insert billingSettings;
    	
	Date d = Date.today();
	List<BIC_Export_Job_Data__c> bbb = new List<BIC_Export_Job_Data__c>();
	for (integer i = 0 ; i<10; i++) {
	BIC_Export_Job_Data__c  b = new BIC_Export_Job_Data__c ();
	b.name = 'BIC Export Job @' + d.addDays(-1 * i).format();
	b.CSV_Generation_Start_Time__c = d.addDays(-1 * i);
	b.Report_Generated__c = Math.mod(i, 2) == 0 ? false : true;
	b.Report_Upload_Started__c = Math.mod(i, 3) == 0 ? false : true;
	b.Status__c = Math.mod(i, 2) == 0 ? 'Failed' : 'CSV Generation Finished' ;
	b.Upload_to_BIC_Status__c =  Math.mod(i, 3) == 0 ? 'Auth cancel' : 'Completed' ; 
	bbb.add(b);
	}
	insert bbb;
	
	
    csam_t1__Integration_Endpoint__c ie = new csam_t1__Integration_Endpoint__c();
        ie.name = 'M2M Integration';
        ie.csam_t1__Callback_Host__c = 'callback';
        ie.csam_t1__Callout_Host__c = 'callout';
        insert ie;
    
    Account a = Test_Util.createAccount('Test');
    insert a;
    csam_t1__ObjectGraph_Callout_Handler__c handler = new csam_t1__ObjectGraph_Callout_Handler__c();
	handler.Name = 'Update SIM Statuses';
	handler.csam_t1__Integration_Endpoint_Name__c ='M2M Integration';
	handler.csam_t1__Startpoint_Type_Name__c = 'Account';
	insert handler;
	csam_t1__Outgoing_Message__c message = new csam_t1__Outgoing_Message__c();
	message.csam_t1__Status__c = 'Waiting for Response';
	message.csam_t1__ObjectGraph_Callout_Handler__c = handler.id;
	message.csam_t1__Callback_Host__c = 'uat-tfd-de-m2m-prod.cs86.force.com';
	message.csam_t1__Content_Type__c = 'application/json; charset=utf-8';
	message.csam_t1__URL_Host__c = 'm2m-aws-proxy.herokuapp.com';
	insert message;
    csam_t1__Outgoing_Message_Record__c record = new csam_t1__Outgoing_Message_Record__c();
	record.csam_t1__Outgoing_Message__c = message.id;
	record.csam_t1__Object_Record_Id__c = a.id;
	record.csam_t1__Object_Name__c = 'Account';
	insert record;    
    message = new csam_t1__Outgoing_Message__c();
	message.csam_t1__Status__c = 'Response Received';
	message.csam_t1__ObjectGraph_Callout_Handler__c = handler.id;
	message.csam_t1__Callback_Host__c = 'uat-tfd-de-m2m-prod.cs86.force.com';
	message.csam_t1__Content_Type__c = 'application/json; charset=utf-8';
	message.csam_t1__URL_Host__c = 'm2m-aws-proxy.herokuapp.com';
	insert message;  
    record = new csam_t1__Outgoing_Message_Record__c();
	record.csam_t1__Outgoing_Message__c = message.id;
	record.csam_t1__Object_Record_Id__c = a.id;
	record.csam_t1__Object_Name__c = 'Account';
	insert record; 
    csam_t1__Incoming_Message__c incoming = new csam_t1__Incoming_Message__c();
    incoming.csam_t1__Outgoing_Message__c = message.id;
    incoming.csam_t1__Status__c = 'Processed';
    incoming.csam_t1__HTTP_Method__c ='dfgd';
    incoming.csam_t1__Mac_Signature__c = 'fdgsdgf';
    insert incoming;
    
    Stock__c[] assets = new List<Stock__c>();
		assets.add(new Stock__c
			( Name = 'Stock1'
			, SIM_Type__c = 'LOCAL'
			 ));
		assets.add(new Stock__c
			( Name = 'Stock2'
			, SIM_Type__c = 'LOCAL'
			  ));
	insert assets;
   	MSISDN__c m = new MSISDN__c();
   	m.Date_quarantine_start__c = Date.today();
   	m.Date_quarantine_end__c = Date.today().addDays(10);
   	insert m;  
    Test.startTest();
    ApexPages.standardController sc = new ApexPages.standardController(new Stock__c());
    SIMManagmentConsoleController page = new  SIMManagmentConsoleController(sc);
        page.simDate = Date.today();
        page.refreshSimList();
        page.getQuarantineRanges();
        page.getStatuses();
        page.getsimList();
        page.getBarData();
        page.getFinalQuarantineList();
        page.retryAll();
        page.getPieData();
        page.getJobList();
        page.exportSIMtoExcel();
        page.exportQuarantinetoExcel();
        page.jobType = 'BICReportPoller';
        page.schedule();
        page.jobType = 'InMessageJob';
        page.schedule();
        page.jobType = 'OutMessageJob';
        page.schedule();
        page.jobType = 'PrebillFetchingPoller';
        page.schedule();
        page.jobType = 'CaseEmailStatusPoller';
        page.schedule();
        page.getTerminatedByStatus();
        page.getRequestedListViewURL();
        page.getTerminatedListViewURL();
        page.getRetiredListViewURL();
        page.getschedules();
        page.getcommercialOrders();
        page.gettrialOrders();
        page.getOrders();        
    Test.stopTest();
    }    
}


/*
system.debug([




List<Account> acc = new List<Account>([select id from account limit 1]);
csam_t1__Outgoing_Message__c message = new csam_t1__Outgoing_Message__c();
csam_t1__ObjectGraph_Callout_Handler__c handler = [select id from csam_t1__ObjectGraph_Callout_Handler__c limit 1];
message.csam_t1__Status__c = 'Response Received';
message.csam_t1__ObjectGraph_Callout_Handler__c = handler.id;
message.csam_t1__Callback_Host__c = 'uat-tfd-de-m2m-prod.cs86.force.com';
message.csam_t1__Content_Type__c = 'application/json; charset=utf-8';
message.csam_t1__URL_Host__c = 'm2m-aws-proxy.herokuapp.com';
insert message;

csam_t1__Outgoing_Message_Record__c record = new csam_t1__Outgoing_Message_Record__c();
record.csam_t1__Outgoing_Message__c = message.id;
record.csam_t1__Object_Record_Id__c = acc[0].id;
record.csam_t1__Object_Name__c = 'Account';
insert record;





List<BIC_Export_Job_Data__c> bb = [select id from BIC_Export_Job_Data__c];
delete bb;
Date d = Date.today();
List<BIC_Export_Job_Data__c> bbb = new List<BIC_Export_Job_Data__c>();
for (integer i = 0 ; i<10; i++) {
BIC_Export_Job_Data__c  b = new BIC_Export_Job_Data__c ();
b.name = 'BIC Export Job @' + d.addDays(-1 * i).format();
b.CSV_Generation_Start_Time__c = d.addDays(-1 * i);
b.Report_Generated__c = Math.mod(i, 2) == 0 ? false : true;
b.Report_Upload_Started__c = Math.mod(i, 3) == 0 ? false : true;
b.Status__c = Math.mod(i, 2) == 0 ? 'Failed' : 'CSV Generation Finished' ;
b.Upload_to_BIC_Status__c =  Math.mod(i, 3) == 0 ? 'Auth cancel' : 'Completed' ; 
bbb.add(b);
}
insert bbb;

*/