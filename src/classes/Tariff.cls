//------------------------------------------------------------------------------
// Container for tariff
//------------------------------------------------------------------------------
public without sharing class Tariff implements ITariff
{
	//--------------------------------------------------------------------------
	// Private members
	//--------------------------------------------------------------------------
	private Tariff__c tariff;
	private List<ITariffOption> tariffOptions;
	private List<ITariffOption> includedTariffOptions;
	private List<ITariffOption> additionalTariffOptions;
	private List<ITariffOption> supplementaryServiceTariffOptions;
	private IInsertHelper insertHelper;

	private static Integer fieldLength = Tariff__c.Name.getDescribe().getLength();

	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	public Tariff(Tariff__c tariff,
			List<ITariffOption> includedTariffOptions,
			List<ITariffOption> additionalTariffOptions,
			List<ITariffOption> supplementaryServiceTariffOptions,
			IInsertHelper insertHelper)
	{
		this.tariff = tariff;
		this.includedTariffOptions = includedTariffOptions;
		this.additionalTariffOptions = additionalTariffOptions;
		this.supplementaryServiceTariffOptions = supplementaryServiceTariffOptions;
		this.insertHelper = insertHelper;
		addAllTariffOptions();
	}

	//--------------------------------------------------------------------------
	// Public methods
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Get tariff custom object
	//--------------------------------------------------------------------------
	public  Tariff__c getTariff()
	{
		return tariff;
	}

	//--------------------------------------------------------------------------
	// Get all tariff options sorted by type and sequence
	//--------------------------------------------------------------------------
	public List<ITariffOption> getTariffOptions()
	{
		return tariffOptions;
	}

	//--------------------------------------------------------------------------
	// Get included tariff options sorted by sequence
	//--------------------------------------------------------------------------
	public List<ITariffOption> getIncludedTariffOptions()
	{
		return includedTariffOptions;
	}

	//--------------------------------------------------------------------------
	// Get additional tariff options sorted by sequence
	//--------------------------------------------------------------------------
	public List<ITariffOption> getAdditionalTariffOptions()
	{
		return additionalTariffOptions;
	}

	//--------------------------------------------------------------------------
	// Get supplementary service tariff options sorted by sequence
	//--------------------------------------------------------------------------
	public List<ITariffOption> getSupplementaryServiceTariffOptions()
	{
		return supplementaryServiceTariffOptions;
	}

	//--------------------------------------------------------------------------
	// Clone tariff with all selected tariff options and configurations
	//--------------------------------------------------------------------------
	public ITariff deepClone()
	{
		Tariff__c clonedTariff = tariff.clone();
		IInsertHelper clonedInsertHelper = null;
		if (insertHelper != null)
		{
			clonedInsertHelper = insertHelper.deepClone();
		}
		List<ITariffOption> clonedIncludedTariffOptions =
			new List<ITariffOption>();
		List<ITariffOption> clonedAdditionalTariffOptions =
			new List<ITariffOption>();
		List<ITariffOption> clonedSupplementaryServiceTariffOptions =
			new List<ITariffOption>();
		for (ITariffOption tariffOption : includedTariffOptions)
		{
			clonedIncludedTariffOptions.add(tariffOption.deepClone());
		}
		for (ITariffOption tariffOption : additionalTariffOptions)
		{
			clonedAdditionalTariffOptions.add(tariffOption.deepClone());
		}
		for (ITariffOption tariffOption : supplementaryServiceTariffOptions)
		{
			clonedSupplementaryServiceTariffOptions.add(tariffOption.deepClone());
		}
		return new Tariff(clonedTariff,
				clonedIncludedTariffOptions,
				clonedAdditionalTariffOptions,
				clonedSupplementaryServiceTariffOptions,
				clonedInsertHelper);
	}

	//--------------------------------------------------------------------------
	// Compare two tariffs
	//--------------------------------------------------------------------------
	public Boolean equals(ITariff tariff)
	{
		//
		// Compare tariff
		//
		if (!equalsTariffsOnly(tariff))
		{
			return false;
		}
		//
		// Compare tariff options
		//
		if (getTariffOptions().size() != tariff.getTariffOptions().size())
		{
			return false;
		}
		for (Integer i = 0; i < getTariffOptions().size(); i++)
		{
			if (!getTariffOptions()[i].equals(tariff.getTariffOptions()[i]))
			{
				return false;
			}
		}
		return true;
	}

	//--------------------------------------------------------------------------
	// Compare two tariffs without quantity
	//--------------------------------------------------------------------------
	public Boolean equalsWithoutQuantity(ITariff tariff)
	{
		//
		// Compare tariff
		//
		if (!equalsTariffsOnly(tariff))
		{
			system.debug('testDEBUU');
			return false;
		}
		//
		// Compare tariff options without quantities
		//
		if (getTariffOptions().size() != tariff.getTariffOptions().size())
		{
			return false;
		}
		for (Integer i = 0; i < getTariffOptions().size(); i++)
		{
			if (!getTariffOptions()[i].equalsWithoutQuantity(tariff.getTariffOptions()[i]))
			{
				return false;
			}
		}
		return true;
	}

	//--------------------------------------------------------------------------
	// Merges tariff options
	// Tariff options that are missing in this tariff are added
	// and unselected
	//--------------------------------------------------------------------------
	public void deepMerge(ITariff tariff)
	{
		mergeTariffOptions(includedTariffOptions,
				tariff.getIncludedTariffOptions());
		mergeTariffOptions(additionalTariffOptions,
				tariff.getAdditionalTariffOptions());
		mergeTariffOptions(supplementaryServiceTariffOptions,
				tariff.getSupplementaryServiceTariffOptions());
		addAllTariffOptions();
	}

	//--------------------------------------------------------------------------
	// Copy quantities from destination tariff
	//--------------------------------------------------------------------------
	public void copyQuantities(ITariff tariff)
	{
		Iterator<ITariffOption> it = tariff.getTariffOptions().iterator();
		for (ITariffOption tariffOption : tariffOptions)
		{
			if (!it.hasNext())
			{
				break;
			}
			tariffOption.setQuantity(it.next().getQuantity());
		}
	}

	//--------------------------------------------------------------------------
	// Store tariff to database
	//--------------------------------------------------------------------------
	public Id store()
	{
		store(insertHelper);
		//
		// Insert tariff, tariff options, rate cards and service fees
		//
		insertHelper.store();
		return getTariff().Id;
	}
	
	public Id storeUpdate()
	{
		List<Tariff_Option__c> lit = new List<Tariff_Option__c>();
		List<Rate_Card__c> rca = new List<Rate_Card__c>();
		
		for (ITariffOption tariffOption : getTariffOptions())
		{
		    Tariff_Option__c to = tariffOption.getTariffOption();
		    lit.add(to);
		    	for (IRateCard rateCard : tariffOption.getRateCards())
				{
					Rate_Card__c rc = rateCard.getRateCard();
					rca.add(rc);
				}
		}
		system.debug('tariffOption  ** lit'+lit);
		system.debug('tariffOption  ** rca'+rca);
		if(lit.size() > 0){
		    update lit;
		}
		
		if(rca.size() > 0){
		    update rca;
		}
		
		
		
		//store(insertHelper);
		//
		// Insert tariff, tariff options, rate cards and service fees
		//
		//insertHelper.storeUpdate();
		return getTariff().Id;
	}
	
	

	//--------------------------------------------------------------------------
	// Store tariff to insert helper to store it in bulk
	//--------------------------------------------------------------------------
	public void store(IInsertHelper insertHelper)
	{
		//
		// Insert tariff only
		//
		getTariff().OwnerId = UserInfo.getUserId();
		insertHelper.add(getTariff());
		for (ITariffOption tariffOption : getTariffOptions())
		{
			//
			// If tariff option is selected insert everything
			//
			if (tariffOption.getIsSelected())
			{
				Tariff_Option__c to = tariffOption.getTariffOption();
				//
				// Configure tariff option
				//
				if (to.Recurring_Target_Fee__c == null)
				{
					to.Recurring_Target_Fee__c =
							to.Recurring_Base_Fee__c;
				}
				insertHelper.add(to, 'Tariff__c', getTariff());
				//
				// Configure rate cards
				//
				for (IRateCard rateCard : tariffOption.getRateCards())
				{
					Rate_Card__c rc = rateCard.getRateCard();
					if (rc.Target_Usage_Fee__c == null)
					{
						rc.Target_Usage_Fee__c = rc.Base_Usage_Fee__c;
					}
					//
					// Set show on quote of the parent if rate card has it
					//
					if (rc.Show_on_Quote__c)
					{
						to.Show_on_Quote__c = true;
					}
					insertHelper.add(rc, 'Tariff_Option__c', to);
				}
				//
				// Configure service fees
				//
				for (IServiceFee serviceFee : tariffOption.getServiceFees())
				{
					Service_Fee__c sf = serviceFee.getServiceFee();
					if (sf.Recurring_Target_Fee__c == null)
					{
						sf.Recurring_Target_Fee__c = sf.Recurring_Base_Fee__c;
					}
					if (sf.One_Time_Target_Fee__c == null)
					{
						sf.One_Time_Target_Fee__c = sf.One_Time_Base_Fee__c;
					}
					//
					// Set show on quote of the parent if service fee has it
					//
					if (sf.Show_on_Quote__c)
					{
						to.Show_on_Quote__c = true;
					}
					insertHelper.add(sf, 'Tariff_Option__c', to);
				}
			}
		}
	}

	//--------------------------------------------------------------------------
	// Prepare child tariff before storing
	//--------------------------------------------------------------------------
	public void prepareChild(
			ITariff parentTariff,
		   	Id accountId,
		   	String accountName,
		   	String contractTerm)
	{
		//
		// Set status to Approved if configuration doesnt have discounts
		//
		if (!getIsAnyTargetFeeDiscounted())
		{
			getTariff().Status__c = Constants.TARIFF_STATUS_APPROVED;
		}
		else
		{
			getTariff().Status__c = Constants.TARIFF_STATUS_DRAFT;
		}
		//
		// Set parent id and account id
		//
		getTariff().Parent_Tariff__c = parentTariff.getTariff().Id;
		getTariff().Account__c = accountId;
		getTariff().Contract_Term__c = contractTerm;
		//
		// Set name of the cloned tariff
		//
		if (accountId != null)
		{
			//
			// {Tariff.Name} + ' for ' + {Account.Name}
			//
			String tariffName =
				parentTariff.getTariff().Name + ' for ' + accountName;
			if (fieldLength < tariffName.length())
			{
				tariffName = tariffName.substring(0, fieldLength);
			}
			getTariff().Name = tariffName;
		}
	}

	//--------------------------------------------------------------------------
	// Private members
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Add included, additional and supplementary service tariff options to
	// tariff options
	//--------------------------------------------------------------------------
	public void addAllTariffOptions()
	{
		tariffOptions = new List<ITariffOption>();
		tariffOptions.addAll(this.includedTariffOptions);
		tariffOptions.addAll(this.additionalTariffOptions);
		tariffOptions.addAll(this.supplementaryServiceTariffOptions);
	}

	//--------------------------------------------------------------------------
	// Compare tariffs
	//--------------------------------------------------------------------------
	private Boolean equalsTariffsOnly(ITariff tariff)
	{
		if (getTariff().Name != tariff.getTariff().Name
			|| getTariff().Account__c != tariff.getTariff().Account__c
			|| getTariff().Code__c != tariff.getTariff().Code__c
			|| getTariff().Commercial_Plan__c != tariff.getTariff().Commercial_Plan__c
			|| getTariff().Contract_Term__c != tariff.getTariff().Contract_Term__c
			|| getTariff().Description_DE__c != tariff.getTariff().Description_DE__c
			|| getTariff().Description_ENG__c != tariff.getTariff().Description_ENG__c
			|| getTariff().Global_Local__c != tariff.getTariff().Global_Local__c
			|| getTariff().Group__c != tariff.getTariff().Group__c
			|| getTariff().One_Off_Price__c != tariff.getTariff().One_Off_Price__c
			|| getTariff().Recurring_Price__c != tariff.getTariff().Recurring_Price__c
			|| getTariff().Product_Family__c != tariff.getTariff().Product_Family__c
			|| getTariff().Sales_Channel__c != tariff.getTariff().Sales_Channel__c
			//|| getTariff().Status__c != tariff.getTariff().Status__c
			|| getTariff().Volume_Start__c != tariff.getTariff().Volume_Start__c
			|| getTariff().Volume_End__c != tariff.getTariff().Volume_End__c)
		{
		    system.debug(getTariff());
		    system.debug(tariff.getTariff());
			return false;
		}
		  system.debug('TRUEMJ');
		return true;
	}

	//--------------------------------------------------------------------------
	// Merges tariff options
	// Tariff options that are missing in original tariff are added
	// and unselected
	//--------------------------------------------------------------------------
	private static void mergeTariffOptions(
			List<ITariffOption> originalTariffOptions,
			List<ITariffOption> parentTariffOptions)
	{
		Integer i = 0;
		for (ITariffOption tariffOption : parentTariffOptions)
		{
			if (i >= originalTariffOptions.size())
			{
				ITariffOption clonedTariffOption = tariffOption.deepClone();
				clonedTariffOption.setIsSelected(false);
				originalTariffOptions.add(clonedTariffOption);
			}
			else if(!tariffOption.equalsWithoutConfiguration(originalTariffOptions[i]))
			{
				ITariffOption clonedTariffOption = tariffOption.deepClone();
				clonedTariffOption.setIsSelected(false);
				originalTariffOptions.add(i, clonedTariffOption);
			}
			i++;
		}
	}

	//--------------------------------------------------------------------------
	// Check if this tariff has discounts
	//--------------------------------------------------------------------------
	private Boolean getIsAnyTargetFeeDiscounted()
	{
		for (ITariffOption tariffOption : getTariffOptions())
		{
			if (tariffOption.getIsSelected()
				&& tariffOption.getIsAnyTargetFeeDiscounted())
			{
				return true;
			}
		}
		return false;
	}
}