public with sharing class QuoteTriggerHandler {

	public static void handleBeforeInsert(list<Quote> newQuotes) {
		
		set<Id> opportunityIds = new set<Id>();
		
		for (Quote q : newQuotes) {
  			opportunityIds.add(q.OpportunityId);
  		}
  		
  		// set synchronized product bundle on quote
  		setProductBundle(newQuotes, opportunityIds);
	}
	
	private static void setProductBundle(list<Quote> newQuotes, set<Id> opportunityIds) {
		map<Id, Opportunity> oppsMap = new map<Id, Opportunity>([select Id, Synched_Bundle_Id__c from Opportunity where Id IN : opportunityIds]);
		for(Quote q : newQuotes){
			q.Product_Bundle__c = oppsMap.get(q.OpportunityId).Synched_Bundle_Id__c;
		}
	}
}