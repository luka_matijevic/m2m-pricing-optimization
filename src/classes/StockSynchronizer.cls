public class StockSynchronizer {
	
	public StockSynchronizationResult synchronize(List<Stock__c> unsynchronizedSims) {
		
		StockSynchronizationResult result = new StockSynchronizationResult();
		
		Set<String> iccids = getIccids(unsynchronizedSims);
		Map<String, Stock__c> iccidStockMap = selectExistingIccidStockMap(iccids);
				
		for (Stock__c unsynchronizedSim : unsynchronizedSims) {
			if (iccidStockMap.containsKey(unsynchronizedSim.ICCID__c)) {
				Stock__c existingSim = iccidStockMap.get(unsynchronizedSim.ICCID__c);
				updateStatusFields(existingSim, unsynchronizedSim);
				result.StocksToUpdate.add(existingSim);
			} else {
				result.StocksToInsert.add(unsynchronizedSim); 
			}
		}
		return result;
	}
	
	private Set<String> getIccids(List<Stock__c> sims) {
		Set<String> iccids = new Set<String>();
		for (Stock__c sim : sims) {
			iccids.add(sim.ICCID__c);
		}
		return iccids;
	}
	
	private Map<String, Stock__c> selectExistingIccidStockMap(Set<String> iccids) {
		
		Map<String, Stock__c> iccidStockMap = new Map<String, Stock__c>();
		
		List<Stock__c> existingStocks = [select Id, ICCID__c from Stock__c where ICCID__c in :iccids];
		
		for (Stock__c sim : existingStocks) {
			iccidStockMap.put(sim.ICCID__c, sim);
		}
		return iccidStockMap;
	}

	private Stock__c updateStatusFields(Stock__c oldSim, Stock__c newSim) {
		
		oldSim.IMSI__c 					= newSim.IMSI__c;
		oldSim.MSISDN__c 				= newSim.MSISDN__c;
		oldSim.SM2M_Status__c 			= newSim.SM2M_Status__c;
		oldSim.State_Trans_Manual__c 	= newSim.State_Trans_Manual__c;
		oldSim.State_Change_Date__c 	= newSim.State_Change_Date__c;
		oldSim.State_Change_Reason__c 	= newSim.State_Change_Reason__c;
	
		return oldSim;
	}
}