/*
	25.1.2017.
	P. Matkovic
	For each billing account that has online bill report delivery option, send message to ETL that report should be generated
	Note: Always execute this batch with batch size 1, because of callouts
*/
global class BatchCreateEtlReports implements Database.Batchable<sObject>, Database.AllowsCallouts {
	
	String query;
	
	global BatchCreateEtlReports() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		query = 'SELECT Id FROM billing_account__c WHERE online_bill_report_delivery__c = true';
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<billing_account__c> billingAccountList = (List<billing_account__c>)scope;
		if (billingAccountList != null && billingAccountList.size()>0){
			csam_t1.ObjectGraphCalloutHandler.queueMessageFromId('Create Reports', billingAccountList[0].id);
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
}