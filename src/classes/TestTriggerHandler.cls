@isTest
private class TestTriggerHandler {
	
	static testMethod void testOpportunityTriggerDelegate() {
	
		Account account = new Account(
	    		Name = 'Test',
	        	Type = 'Business'
	    );
	        
	    insert account;
	    	  	
		Double monthlyRevenue = 2.0;  	
	    	
		List<Opportunity> opportunities = new List<Opportunity>();
		
		opportunities.add(new Opportunity(
			Name = 'Test-1',
			AccountId = account.Id,
			CloseDate = Date.newInstance(2013, 1, 1),
			StageName = 'Closed Won',
			Win_Reason__c = 'Price',
			Contract_Term_Per_SIM__c = 12,
			Monthly_Revenue_Per_SIM__c = monthlyRevenue,
			Description = 'Test description 20 characters'
		));
	    	
		opportunities.add(new Opportunity(
			Name = 'Test-2',
			AccountId = account.Id,
			CloseDate = Date.newInstance(2013, 1, 1),
			StageName = 'Closed Won',
			Win_Reason__c = 'Price',
			Contract_Term_Per_Subscription__c = 12,
			Monthly_Revenue_Per_Service__c = monthlyRevenue,
			Description = 'Test description 20 characters'
		));
	    	
		insert opportunities;
				
		delete opportunities[0];
		
		opportunities[1].Monthly_Revenue_Per_Service__c = 1.0;
		update opportunities[1];
	}
	
}