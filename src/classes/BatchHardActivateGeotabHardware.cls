/*
	Petar Matkovic 23.11.2016.
	get geotab order requests that are in standard payment type and order is placed in geotab at least 14 days ago
	create order for those requests and bill hardware one time.
*/
global class BatchHardActivateGeotabHardware implements Database.Batchable<sObject>, Database.Stateful {
	
	String query;
	GEOTAB_Settings__c geotabSettings = GEOTAB_Settings__c.getInstance();
	
	global BatchHardActivateGeotabHardware() {
		
	}
	
	global List<order_request__c> start(Database.BatchableContext BC) {
		List<order_request__c> orderRequestList = [SELECT id, name, Geotab_Payment_Type__c 
													from order_request__c where RecordTypeId = :geotabSettings.Order_Request_Type_Id__c
				 										and Geotab_Payment_Type__c = 'Standard' 
				 										and DAY_ONLY(Order_Posted_In_GEOTAB_Date__c) <= LAST_N_DAYS:14];
		return orderRequestList;
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		List<order_request__c> orderRequestList = (List<order_request__c>)scope;

   		Map<Id, csord__Order__c> orderRequestIdOrderMap = new Map<Id, csord__Order__c>();
   		if (orderRequestList != null && orderRequestList.size() > 0){
			orderRequestIdOrderMap = getOrdersForOrderRequests(orderRequestList);
		}
		
		if (orderRequestIdOrderMap != null){
			for (Id ordReqId : orderRequestIdOrderMap.keySet()){
				if (orderRequestIdOrderMap.get(ordReqId) == null){
					//create order and bill hardware one time
					Factory.createCreateOrder(ordReqId).create();
				}
			}
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}

	private Map<Id, csord__Order__c> getOrdersForOrderRequests(List<order_request__c> orderRequestListIn){
		Map<Id, csord__Order__c> returnMap = new Map<Id, csord__Order__c>();

		List<csord__Order__c> orderList = [SELECT id, name, order_request__c
											FROM csord__Order__c
											WHERE order_request__c in :orderRequestListIn];

		if (orderList != null){
			for (csord__Order__c order : orderList){
				for (order_request__c orderReq : orderRequestListIn){
					if (String.valueOf(order.order_request__c) == String.valueOf(orderReq.Id)){
						returnMap.put(orderReq.Id, order);
						break;
					}
				}
			}
		}
		return returnMap;
	}
	
}