/*
* UserTriggerDelegate extends Trigger handler to execute calls based on events.
*/
public class UserTriggerDelegate extends TriggerHandler.DelegateBase {

    public override void prepareBefore() {
        // do any preparation here – bulk loading of data etc
    }


    list<GroupMember> grpMembersToInsert = new list<GroupMember>();
    set<id> existingSecondLevelUsersInGroup = new set<id>();
    set<id> existingFirstLevelUsersInGroup = new set<id>();
    public override void prepareAfter() {
        
        string secondLevelProfileId;
        string firstLevelProfileId;
        string M2M_Standard_User_ProfileId;
        
        string secondLevelGorupId;
        string firstLevelGroupId;
        
        List<Profile> profilesLst = new list<Profile>([select id, name from profile where name= :Label.M2M_Service_2nd_Level OR Name=:Label.X1st_Level_DSC_READ_permissions OR Name ='M2M Standard User']);
        
        for(profile obj: profilesLst){
            if(obj.name == Label.M2M_Service_2nd_Level){
                secondLevelProfileId = obj.id;
            }else if(obj.name == Label.X1st_Level_DSC_READ_permissions){
                firstLevelProfileId = obj.id;
            }else if(obj.name == 'M2M Standard User'){
                M2M_Standard_User_ProfileId = obj.id;
            }
        }
        
        /* start: Giving Ticket read permission to M2M standard User*/
        for(User u :(list<User>) trigger.new){
            if(trigger.isInsert){
                if(M2M_Standard_User_ProfileId != null && M2M_Standard_User_ProfileId != '' && u.profileId == M2M_Standard_User_ProfileId){
                    Database.executeBatch(new Tickets_Permission_To_Service2ndLevel());
                    break;
                }
            }else if(trigger.isUpdate){
                map<id, User> oldMap = ( map<id, User>) trigger.oldMap;
                if(oldMap.get(u.id).profileId != u.profileId){
                    if(M2M_Standard_User_ProfileId != null && M2M_Standard_User_ProfileId != '' && u.profileId == M2M_Standard_User_ProfileId){
                        Database.executeBatch(new Tickets_Permission_To_Service2ndLevel());
                        break;
                    }
                }
            }
        }
        /* End: Giving Ticket read permission to M2M standard User*/
        
        Boolean assignUserToGorup = false;
        for(User u :(list<User>) trigger.new){
            if(u.profileId == firstLevelProfileId || u.profileId == secondLevelProfileId){
                assignUserToGorup = true;
                break;
            }
        }
        if(assignUserToGorup){
            // If any user created/updated with profile "M2M Service 2nd Level" OR "First Level DSC Read Permission" Profile, then adding the user
            // to public group "M2M Service 2nd Level" OR "First Level DSC Read Permission", to avoid manual configuration
            
            for(Group obj :[select name from group where name= :Label.M2M_Service_2nd_Level
                                                       OR Name=:Label.X1st_Level_DSC_READ_permissions]){
                if(obj.name == Label.M2M_Service_2nd_Level){
                    secondLevelGorupId = obj.id;
                }else if(obj.name == Label.X1st_Level_DSC_READ_permissions){
                    firstLevelGroupId = obj.id;
                }
            }
            
            for(GroupMember obj: [select id, UserOrGroupId,group.name from GroupMember where (group.name = :Label.M2M_Service_2nd_Level OR group.name =:Label.M2M_Service_2nd_Level) and UserOrGroupId =:trigger.newMap.keySet()]){
                if(obj.group.name == Label.M2M_Service_2nd_Level){
                     existingSecondLevelUsersInGroup.add(obj.UserOrGroupId);
                }else if(obj.group.name == Label.X1st_Level_DSC_READ_permissions){
                    existingFirstLevelUsersInGroup.add(obj.UserOrGroupId);
                }
            }
            
            List<GroupMember> firstLevelGroupMems = New list<GroupMember>();
            if(firstLevelProfileId != null && firstLevelGroupId != null){
                firstLevelGroupMems = User_Trigger_Util.assignUsersToGorup(firstLevelProfileId,firstLevelGroupId,existingsecondLevelUsersInGroup);    
            }
            
            
            List<GroupMember> secondLevelGroupMems = New list<GroupMember>();
            if(secondLevelProfileId != null && secondLevelGorupId != null){
                secondLevelGroupMems = User_Trigger_Util.assignUsersToGorup(secondLevelProfileId,secondLevelGorupId,existingSecondLevelUsersInGroup);
            }
            
            
            if(firstLevelGroupMems.size() > 0){
                grpMembersToInsert.addAll(firstLevelGroupMems);
            }
            
            if(secondLevelGroupMems.size() > 0){
                grpMembersToInsert.addAll(secondLevelGroupMems);
            }
            
        }
    }

    public override void beforeInsert(sObject o) {
        // Apply before insert logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method    
    }

    public override void beforeUpdate(sObject old, sObject o) {
        
    }


    public override void afterInsert(sObject o) {
        // Apply after insert logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterUpdate(sObject old, sObject o) {
        // Apply after update logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method        
    }


    public override void finish() {
       if(grpMembersToInsert.size() > 0){
           insert grpMembersToInsert;
       }
    }

}