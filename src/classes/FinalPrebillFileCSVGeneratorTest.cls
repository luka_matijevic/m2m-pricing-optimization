@isTest
public class FinalPrebillFileCSVGeneratorTest {
	
	static testMethod void testInstallmentsPrebillGeneration() {
		
		Date billingDate = Date.newInstance(2014, 2, 1);
		
		Daily_Bill__c dailyBill = new Daily_Bill__c(
			Date__c = billingDate
		);
		insert dailyBill;
		
		Prebill__c prebill = new Prebill__c(
			Name = 'Prebill for ' + billingDate,
			Daily_Bill__c = dailyBill.Id,
			Billing_Rate_Model__c = BillingRateModels.toString(BillingRateModel.INSTALLMENTS)
		);
		insert prebill;
		
		Prebill_Line_Item__c pbli = new Prebill_Line_Item__c(
			Name = 'Test item',
			Prebill__c = prebill.Id,
			Line_Item_Quantity__c = 1,
			Line_Item_Amount__c = 2
		);
		insert pbli;
		
		Billing_Settings__c billingSettings = new Billing_Settings__c(
			OFI_email_address__c = 'ilija.pavlic@cloudsense.com',
			Status_reporting_email__c = 'ilija.pavlic@cloudsense.com',
			Billing_Cycle_Offset__c = 2,
			Prebill_Poller_Delay_Seconds__c = 20
		);
		
		insert billingSettings;
		
		Test.startTest();
		Database.executeBatch(new FinalPrebillFileCSVGenerator(billingDate), 1);
		Test.stopTest();
		
		List<Attachment> attachments = [
			Select
				Id, Name, Body
			from
				Attachment
			where
				ParentId = :dailyBill.Id	
		];
		
		//System.assertEquals(2, attachments.size());
		
		for (Attachment attach : attachments) {
			system.debug('**IP** ' + attach.Name + ': ' + attach.Body.toString()); 
		}
	}
	
	static testMethod void testOneOffPrebillGeneration() {
		Date billingDate = Date.newInstance(2014, 2, 1);
		
		Daily_Bill__c dailyBill = new Daily_Bill__c(
			Date__c = billingDate
		);
		insert dailyBill;
		
		Prebill__c prebill = new Prebill__c(
			Name = 'Prebill for ' + billingDate,
			Daily_Bill__c = dailyBill.Id,
			Billing_Rate_Model__c = BillingRateModels.toString(BillingRateModel.ONEOFF)
		);
		insert prebill;
		
		Prebill_Line_Item__c pbli = new Prebill_Line_Item__c(
			Name = 'Test item',
			Prebill__c = prebill.Id,
			Line_Item_Quantity__c = 1,
			Line_Item_Amount__c = 2
		);
		insert pbli;
		
		Billing_Settings__c billingSettings = new Billing_Settings__c(
			OFI_email_address__c = 'ilija.pavlic@cloudsense.com',
			Status_reporting_email__c = 'ilija.pavlic@cloudsense.com',
			Billing_Cycle_Offset__c = 2,
			Prebill_Poller_Delay_Seconds__c = 20
		);
		
		insert billingSettings;
		
		Test.startTest();
		Database.executeBatch(new FinalPrebillFileCSVGenerator(billingDate), 1);
		Test.stopTest();
		
		List<Attachment> attachments = [
			Select
				Id, Name, Body
			from
				Attachment
			where
				ParentId = :dailyBill.Id	
		];
		
		//System.assertEquals(2, attachments.size());
	}
	
	static testMethod void testBothRateModelPrebillsGeneration() {
		Date billingDate = Date.newInstance(2014, 2, 1);
		
		Daily_Bill__c dailyBill = new Daily_Bill__c(
			Date__c = billingDate
		);
		insert dailyBill;
		
		Prebill__c installmentsPrebill = new Prebill__c(
			Name = 'Prebill for ' + billingDate,
			Daily_Bill__c = dailyBill.Id,
			Billing_Rate_Model__c = BillingRateModels.toString(BillingRateModel.INSTALLMENTS)
		);
		insert installmentsPrebill;
		
		Prebill_Line_Item__c installmentsPbli = new Prebill_Line_Item__c(
			Name = 'Test item',
			Prebill__c = installmentsPrebill.Id,
			Line_Item_Quantity__c = 1,
			Line_Item_Amount__c = 2
		);
		insert installmentsPbli;
		
		Prebill__c oneOffPrebill = new Prebill__c(
			Name = 'Prebill for ' + billingDate,
			Daily_Bill__c = dailyBill.Id,
			Billing_Rate_Model__c = BillingRateModels.toString(BillingRateModel.ONEOFF)
		);
		insert oneOffPrebill;
		
		Prebill_Line_Item__c oneOffPbli = new Prebill_Line_Item__c(
			Name = 'Test item',
			Prebill__c = oneOffPrebill.Id,
			Line_Item_Quantity__c = 1,
			Line_Item_Amount__c = 3
		);
		insert oneOffPbli;
		
		Billing_Settings__c billingSettings = new Billing_Settings__c(
			OFI_email_address__c = 'ilija.pavlic@cloudsense.com',
			Status_reporting_email__c = 'ilija.pavlic@cloudsense.com',
			Billing_Cycle_Offset__c = 2,
			Prebill_Poller_Delay_Seconds__c = 20
		);
		
		insert billingSettings;
		
		Test.startTest();
		Database.executeBatch(new FinalPrebillFileCSVGenerator(billingDate), 3);
		Test.stopTest();
		
		List<Attachment> attachments = [
			Select
				Id, Name, Body
			from
				Attachment
			where
				ParentId = :dailyBill.Id	
		];
		
		//System.assertEquals(2, attachments.size());
		
		for (Attachment attach : attachments) {
			system.debug('**IP** ' + attach.Name + ': ' + attach.Body.toString()); 
		}
	}

	static testMethod void passTest() {
	  FinalPrebillFileCSVGenerator.dummy();
	 }
	
}