@isTest(seeAllData = false)
private class TestTerminatedSIMsOnAccountController {

	private static testMethod void testTerminatedSIMsOnAccSingle() {
        List<Stock__c> sims = TestDataGeneratorForTerminateSIMProcess.createSIMs(1);
        TestDataGeneratorForTerminateSIMProcess.terminateSIMs(sims);
        List<Stock__c> queriedSIMS = [Select Id, Termination_Status__c, Third_Upload_File__c, Third_Upload_File__r.Order_Request__r.Frame_Contract__r.Customer__c from Stock__c where Id IN:sims];
        System.assertEquals('REQUESTED', queriedSIMS[0].Termination_Status__c);
        Account acc = [Select Id, Name from Account LIMIT 1];
        System.assertEquals(queriedSIMS[0].Third_Upload_File__r.Order_Request__r.Frame_Contract__r.Customer__c, acc.Id);
        PageReference pageRef = Page.TerminatedSIMsOnAccountPage;
        pageRef.getPArameters().put('id',acc.id);
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(acc);
        TerminatedSIMsOnAccountController termCtrl = new TerminatedSIMsOnAccountController(stdCtrl);
        termCtrl.getAvailableFCs();
        termCtrl.getAllSims();
        termCtrl.getPageNumber();
        termCtrl.getTotalPages();
        termCtrl.first();
        termCtrl.next();
        termCtrl.previous();
        termCtrl.last();
        termCtrl.getNoOfSIMs();
        termCtrl.backToAccount();
        termCtrl.exportToExcel();
        termCtrl.getAllTerminatedSIMs();
        termCtrl.exportToPDF();
        termCtrl.stopTermination();
        
        termCtrl.performSearchbyFC();
        termCtrl.searchFC = [Select Name from Frame_Contract__c LIMIT 1].Name;
        termCtrl.getAllSims();
        termCtrl.getAllTerminatedSIMs();
        termCtrl.getNoOfSIMs();
        
        
        Test.stopTest();
        
        System.assertEquals(false, termCtrl.hasNext);
        System.assertEquals(false, termCtrl.hasPrevious);
        List<Stock__c> stock = [Select Id, Termination_Status__c from Stock__c where Id IN :sims];
        System.assertEquals(null, stock[0].Termination_Status__c);
	}

	private static testMethod void testTerminatedSIMsOnAccBulk() {
        List<Stock__c> sims = TestDataGeneratorForTerminateSIMProcess.createSIMs(200);
        TestDataGeneratorForTerminateSIMProcess.terminateSIMs(sims);
        List<Stock__c> queriedSIMS = [Select Id, Termination_Status__c, Third_Upload_File__c, Third_Upload_File__r.Order_Request__r.Frame_Contract__r.Customer__c from Stock__c where Id IN:sims];
        System.assertEquals('REQUESTED', queriedSIMS[0].Termination_Status__c);
        Account acc = [Select Id, Name from Account LIMIT 1];
        System.assertEquals(queriedSIMS[0].Third_Upload_File__r.Order_Request__r.Frame_Contract__r.Customer__c, acc.Id);
        PageReference pageRef = Page.TerminatedSIMsOnAccountPage;
        pageRef.getPArameters().put('id',acc.id);
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(acc);
        TerminatedSIMsOnAccountController termCtrl = new TerminatedSIMsOnAccountController(stdCtrl);
        termCtrl.getAvailableFCs();
        termCtrl.getAllSims();
        termCtrl.getPageNumber();
        termCtrl.getTotalPages();
        termCtrl.first();
        System.assertEquals(true, termCtrl.hasNext);
        termCtrl.next();
        System.assertEquals(true, termCtrl.hasPrevious);
        termCtrl.previous();
        termCtrl.last();
        termCtrl.getNoOfSIMs();
        termCtrl.backToAccount();
        termCtrl.exportToExcel();
        termCtrl.getAllTerminatedSIMs();
        termCtrl.exportToPDF();
        termCtrl.stopTermination();
        
        termCtrl.performSearchbyFC();
        termCtrl.searchFC = [Select Name from Frame_Contract__c LIMIT 1].Name;
        termCtrl.getAllSims();
        termCtrl.getAllTerminatedSIMs();
        termCtrl.getNoOfSIMs();
        
        Test.stopTest();
        
        List<Stock__c> stock = [Select Id, Termination_Status__c from Stock__c where Id IN :sims];
        System.assertEquals(null, stock[0].Termination_Status__c);
	}
}