public with sharing class CaseLevelUpDownController{

    private static final String CASE_LEVEL_UP = 'up';
    private static final String CASE_LEVEL_DOWN = 'down';
    
    private Case caseObject;
    public CaseComment ticketComment{get;set;}
    public List<CaseComment> ticketComments{get;set;}
    private ApexPages.StandardController controller;
    public boolean stopTransfer{get;set;}
    private String type;
    public CaseLevelUpDownController (ApexPages.StandardController cont){
        ticketComment = new CaseComment();
        ticketComments = new List<CaseComment>();
        type = ApexPages.currentPage().getParameters().get('type');
        System.debug('Type : ' + type);
        controller = cont;
        this.caseObject = (Case) cont.getRecord();
        System.debug('Case Object : ' + caseObject);
        if(caseObject == null){
            caseObject = new Case();
        }
        ticketComments = [Select Id, CommentBody, ParentId from CaseComment where ParentId = : caseObject.Id Order by CreatedDate];
        /*if(!ticketComments.isEmpty()){
            ticketComment = ticketComments[0];
        }*/
        Id ticketOwnerId = [select ownerId from Case where Id =: caseObject.Id LIMIT 1].ownerId;
        //System.debug('Case Owner : ' + ticketOwnerId);
        //Added additional logic to stop the first level user from sending ticket to higher or lower levels for tickets already in 2nd & 3rd levels
        Set<Id> firstlevelOwners = new Set<Id>();
        Set<Id> secondlevelOwners = new Set<Id>();
        Set<Id> thirdlevelOwners = new Set<Id>();
        for(Group g : [select id, name, developerName, type, (select UserOrGroupId, GroupId from GroupMembers) from Group where Type = 'Queue' and (developerName = 'DSC_Queue' OR developerName = 'CS_NUE' OR developerName = 'Operations_Management')]){
            if(g.developerName.equalsIgnoreCase('DSC_Queue')){
                firstlevelOwners.add(g.Id);
                for(GroupMember gm : g.GroupMembers){
                    firstlevelOwners.add(gm.UserorGroupId);
                }
            }
            else if(g.developerName.equalsIgnoreCase('CS_NUE')){
                secondlevelOwners.add(g.Id);
                for(GroupMember gm : g.GroupMembers){
                    secondlevelOwners.add(gm.UserorGroupId);
                }
            }
            else if(g.developerName.equalsIgnoreCase('Operations_Management')){
                thirdlevelOwners.add(g.Id);
                for(GroupMember gm : g.GroupMembers){
                    thirdlevelOwners.add(gm.UserorGroupId);
                }
            }
        }
        if(firstlevelOwners.contains(UserInfo.getUserId()) && (secondlevelOwners.contains(ticketOwnerId) || thirdlevelOwners.contains(ticketOwnerId))){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'You are not allowed to send ticket to higher/lower level'));
            stopTransfer = true;
        }
        //Done
        
        //Logic to stop user from sending 3rd level ticket to higher level and 1st level ticket to lower level
        if(type != null){
            if(type.equalsIgnoreCase(CASE_LEVEL_UP) && thirdlevelOwners.contains(ticketOwnerId)){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'This ticket is already at the highest level.'));
            	stopTransfer = true;
            }
            else if(type.equalsIgnoreCase(CASE_LEVEL_DOWN) && firstlevelOwners.contains(ticketOwnerId)){
         		ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'This ticket is already at the lowest level.'));
            	stopTransfer = true;
            }    
        }
        //
    }
    
    /*public PageReference levelUpCase(){
        string nextQueue = 'DSC_Queue';
        system.debug(caseObject.ownerid);
        caseObject = [Select id, ownerId, caseNumber, account.Name, Owner.Name from Case where id =: caseObject.Id];
        List<GroupMember> groupNames = [select id,  Group.developerName
                                            from GroupMember where Group.Type = 'Queue' 
                                            and UserOrGroupId = :caseObject.ownerid and (Group.developerName = 'DSC_Queue' OR Group.developerName = 'CS_NUE' OR Group.developerName = 'Operations_Management')];
          
        if (!groupNames.isEmpty()) {
            system.debug(groupNames[0].Group.developerName);
            if (groupNames[0].Group.developerName == 'DSC_Queue') nextQueue='CS_NUE';
            if (groupNames[0].Group.developerName == 'CS_NUE') nextQueue='Operations_Management';
        }
        List<Group> newQueue = [select id, name from Group where Type = 'Queue' and developerName = :nextQueue];
        system.debug(nextQueue);
        if (!newQueue.isEmpty())
        {
            system.debug(newQueue[0].id);
            caseObject.ownerid = newQueue[0].id;
            update caseObject;
        }
        PageReference pref = new ApexPages.StandardController(caseObject).view();
        return pref;
    }*/
    
    public PageReference levelUpCase(){
        if(ticketComment.commentBody == NULL || ticketComment.commentBody.length() <= 0){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Please enter Comments'));
            return null;
        }
        if(ticketComment.commentBody != NULL && ticketComment.commentBody.length() > 0){
            controller.Save();
            if(ticketComment.parentId == NULL) 	ticketComment.parentId = caseObject.Id;
            upsert ticketComment;
        }
        TicketAssignmentHelper.assignTicketToHigherLevel(caseObject.Id);
        PageReference pref = new ApexPages.StandardController(caseObject).view();
        return pref;
    }
    
    public PageReference levelDownCase(){
        if(ticketComment.commentBody == NULL || ticketComment.commentBody.length() <= 0){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Please enter Comments'));
            return null;
        }
        if(ticketComment.commentBody != NULL && ticketComment.commentBody.length() > 0){
            controller.Save();
            if(ticketComment.parentId == NULL) 	ticketComment.parentId = caseObject.Id;
            upsert ticketComment;
        }
        TicketAssignmentHelper.assignTicketToLowerLevel(caseObject.Id);
        PageReference pref = new ApexPages.StandardController(caseObject).view();
        return pref;
    }
      
    public PageReference cancelCase(){   
        // return to case page
        PageReference newocp = new PageReference('/' + caseObject.id);
        newocp.setRedirect(true);
        return newocp;
    }  
}