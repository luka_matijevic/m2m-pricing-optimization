public class PrebillHelper {
	
	public static Prebill__c buildPrebill(csord__Service_Line_Item__c sli, Daily_Bill__c dailyBill) {
		 String text='0';
		     if(!test.isrunningtest())
        {
		List<Invoice_Number__c> ll = [select id, name, value__c from invoice_number__c where name='Current' FOR UPDATE];
        Invoice_Number__c invoiceNumber = ll[0];
        
        text=String.valueOf(invoiceNumber.Value__c.setScale(0)); 
        while (text.length() < 10)  
                { 
                text = '0' + text; 
                }
                
		invoiceNumber.Value__c=invoiceNumber.Value__c+1;
        update invoiceNumber;
        }
       
		return new Prebill__c(
		    Invoice_Number__c=text,
			Daily_Bill__c = dailyBill.Id,
			Transaction_Date__c = Date.today(),
			Billing_Account__c = LineItems.billingAccountId(sli),
			Billing_Rate_Model__c = BillingRateModels.billingRateModelString(sli),
			Name = 'Prebill for ' + String.valueOf(dailyBill.Date__c),
			Currency_Code__c = 'EUR'
		);
	}

	public static Prebill__c buildPrebill(csord__Order_Line_Item__c oli, Daily_Bill__c dailyBill) {
		String text='0';
		     if(!test.isrunningtest())
        {
		List<Invoice_Number__c> ll = [select id, name, value__c from invoice_number__c where name='Current' FOR UPDATE];
        Invoice_Number__c invoiceNumber = ll[0];
        
        text=String.valueOf(invoiceNumber.Value__c.setScale(0)); 
        while (text.length() < 10)  
                { 
                text = '0' + text; 
                }
                
		invoiceNumber.Value__c=invoiceNumber.Value__c+1;
        update invoiceNumber;
        }
        
		return new Prebill__c(
		    Invoice_Number__c=text,
		    Daily_Bill__c = dailyBill.Id,
			Transaction_Date__c = Date.today(),
			Billing_Account__c = LineItems.billingAccountId(oli),
			Billing_Rate_Model__c = BillingRateModels.billingRateModelString(oli),
			Name = 'Prebill for ' + String.valueOf(dailyBill.Date__c),
			Currency_Code__c = 'EUR'
		);
	}
}