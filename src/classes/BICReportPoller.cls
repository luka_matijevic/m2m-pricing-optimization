global class BICReportPoller implements Schedulable {
	
	public static void startPolling() {
		BICReportPoller.scheduleTick(System.now().addSeconds(10));
	}
	
	global void execute(SchedulableContext sc) {
		
		List<BIC_Export_Job_Data__c> reportGeneratedJobs = [
			Select
				Id, Name, Report_Upload_Started__c, CSV_Generation_Start_Time__c
			from
				BIC_Export_Job_Data__c
			where 
				Report_Generated__c = true
			and
				Report_Upload_Started__c = false  
			Limit 9  
		];
		
		for (BIC_Export_Job_Data__c jobData : reportGeneratedJobs) {
			jobData.Report_Upload_Started__c = true;
			update jobData;
			
			uploadBicReport(jobData);
			
			uploadPqtReport(jobData);
			
			uploadVvlReport(jobData);
		}	
		
		if (!BICReportSettingsHelper.isDisableBICReportPollerReschedule()) {
			Datetime nextTick = System.now().addSeconds(BICReportSettingsHelper.getBICReportPollerDelaySeconds());
			BICReportPoller.scheduleTick(nextTick);
		}
		System.abortJob(sc.getTriggerId());
	}
	
	private static void scheduleTick(Datetime dt) {
		String chron_exp = dt.format('ss mm HH dd MM ? yyyy');
		BICReportPoller poller = new BICReportPoller();
		// Schedule the next job, and give it the system time so name is unique
		System.schedule(jobName(dt), chron_exp, poller);
	}
	
	private static String jobName(Datetime dt) {
		return 'SIMReportsPoller_' + dt.getTime();
	}
	
	private Map<Id, Attachment> jobReportsMap(List<BIC_Export_Job_Data__c> exportJobs) {
		
		Set<Id> parentIds = new Set<Id>();
		for (BIC_Export_Job_Data__c exportJob : exportJobs) {
			parentIds.add(exportJob.Id);
		}
		
		List<Attachment> attachments = [
			select Id, ParentId, Name, Description, CreatedDate, ContentType, Body, IsDeleted
			from Attachment
			where isDeleted = false
			and ParentId in :parentIds
		];
		
		Map<Id, Attachment> resultMap = new Map<Id, Attachment>();
		for (Attachment attach : attachments) {
			Attachment currentCandidate = resultMap.get(attach.ParentId);
			if (currentCandidate == null || currentCandidate.CreatedDate < attach.CreatedDate) {
				resultMap.put(attach.ParentId, attach);
			}
		}
		return resultMap;
	}
	
	private void uploadBicReport(BIC_Export_Job_Data__c item) {
		csam_t1.ObjectGraphCalloutHandler.AdditionalObjects addObjects = new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects();
		addObjects.typeName = 'BIC_Export_Job_Data__c';
		addObjects.ids = new Id[]{item.id};

		csam_t1.ObjectGraphCalloutHandler.createAndSendExtended('Upload BIC File', item.Id, new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects[]{addObjects});
	}
	
	private void uploadPqtReport(BIC_Export_Job_Data__c item) {
		csam_t1.ObjectGraphCalloutHandler.AdditionalObjects addObjects = new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects();
		addObjects.typeName = 'BIC_Export_Job_Data__c';
		addObjects.ids = new Id[]{item.id};

		csam_t1.ObjectGraphCalloutHandler.createAndSendExtended('Upload PQT File', item.Id, new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects[]{addObjects});
	}
	
	private void uploadVvlReport(BIC_Export_Job_Data__c item) {
		csam_t1.ObjectGraphCalloutHandler.AdditionalObjects addObjects = new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects();
		addObjects.typeName = 'BIC_Export_Job_Data__c';
		addObjects.ids = new Id[]{item.id};

		csam_t1.ObjectGraphCalloutHandler.createAndSendExtended('Upload VVL File', item.Id, new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects[]{addObjects});
	}
}