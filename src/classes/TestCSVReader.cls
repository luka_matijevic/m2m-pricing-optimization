public class TestCSVReader {

	 /**
     * Test some use cases for reading IETF RFC 4180-compliant CSV values.
     */
    public static testMethod void readIETFRFC4180CSVValueTest() {
        String data = null;  // Placeholder for data to use in testing.

        System.debug(data = Parser.CRLF);
        new CSVReader.CSVValue('', false, Parser.CRLF)
                .assertEquals(CSVReader.readIETFRFC4180CSVValue(data));

        System.debug(data = '""' + Parser.CRLF);
        new CSVReader.CSVValue('', true, Parser.CRLF)
                .assertEquals(CSVReader.readIETFRFC4180CSVValue(data));

        System.debug(data = '"",asdf' + Parser.CRLF);
        new CSVReader.CSVValue('', true, Parser.COMMA)
                .assertEquals(CSVReader.readIETFRFC4180CSVValue(data));

        System.debug(data = ',asdf' + Parser.CRLF);
        new CSVReader.CSVValue('', false, Parser.COMMA)
                .assertEquals(CSVReader.readIETFRFC4180CSVValue(data));

        System.debug(data = '"' + Parser.CRLF + '",blah' + Parser.CRLF);
        new CSVReader.CSVValue(Parser.CRLF, true, Parser.COMMA)
                .assertEquals(CSVReader.readIETFRFC4180CSVValue(data));

        System.debug(data = '"""marty""","""chang"""' + Parser.CRLF);
        new CSVReader.CSVValue('"marty"', true, Parser.COMMA)
                .assertEquals(CSVReader.readIETFRFC4180CSVValue(data));

        System.debug(data = '"com""pli""cate' + Parser.CRLF + 'd"'
                + Parser.CRLF);
        new CSVReader.CSVValue('com"pli"cate' + Parser.CRLF + 'd', true, Parser.CRLF)
                .assertEquals(CSVReader.readIETFRFC4180CSVValue(data));

        System.debug(data = 'asdf' + Parser.CRLF);
        new CSVReader.CSVValue('asdf', false, Parser.CRLF)
                .assertEquals(CSVReader.readIETFRFC4180CSVValue(data));
    }

    /**
     * Tests one case of parsing a CSV file that contains the following data:
     *
     * abc,"def","g""h""i"
     * "j
     * kl","m
     * n""o""",
     */
    public static testMethod void readIETFRFC4180CSVFile() {
      String data = 'abc,"def","g""h""i"' + Parser.CRLF + '"j' + Parser.CRLF
             + 'kl","m' + Parser.CRLF + 'n""o""",';

      List<List<String>> fileValues =
             CSVReader.readIETFRFC4180CSVFile(Blob.valueOf(data));
    }

}