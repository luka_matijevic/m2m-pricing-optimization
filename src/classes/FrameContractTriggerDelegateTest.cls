@isTest
private class FrameContractTriggerDelegateTest {

	static testmethod void checkFrameContract()
	{
		Test.startTest();
		Id opptyRecType = [Select id from RecordType where sObjectType = 'Opportunity' and DeveloperName = 'Trial_Global'].Id;
		Id fcRecType = [Select id from RecordType where sObjectType = 'Frame_Contract__c' and DeveloperName = 'Trial'].Id;
		List<Account> accList = new List<Account>();
		List<Opportunity> oppList = new List<Opportunity>();
		List<cscfga__Product_Bundle__c> productBundles = new List<cscfga__Product_Bundle__c>(); 
		List<Unit_Schedule__c> schedules = new List<Unit_Schedule__c>();
		List<Frame_Contract__c> frameContracts = new List<Frame_Contract__c>();
		for(Integer i = 0; i < 10; i++)
		{
			Account acc = new Account(Name = 'TestAccount' + i, Type = 'Customer');
			accList.add(acc);
		}
		System.assertNotEquals(accList.size(),0);
		insert accList;
		for(Integer i = 0; i < 10; i++)
		{
			Opportunity opp = new Opportunity(recordTypeId = opptyRecType, Name = 'Test Oppty' + i, AccountId = accList[i].Id, Type = 'New Business', Product_Family__c = 'Connectivity', CloseDate = System.today() + 10, StageName = 'Prospecting', Estimated_Success_Probability__c = 45, LeadSource = 'Email', Description = 'Test Test Test Test Test Test Test Test');
			oppList.add(opp);
		}
		System.assertNotEquals(oppList.size(),0);
		insert oppList;
		for(Integer i = 0; i < 10; i++)
		{
			cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(cscfga__Synchronised_with_Opportunity__c = true, cscfga__Opportunity__c = oppList[i].Id);
			productBundles.add(productBundle);
		}
		System.assertNotEquals(productBundles.size(),0);
		insert productBundles;
		for(Opportunity opp : oppList)
		{
			for(Integer i = 0; i < 2; i++)
			{
				Unit_Schedule__c schedule = new Unit_Schedule__c(Opportunity__c = opp.Id, Year__c = '2013', Connectivity_Sims_January__c = 4, Connectivity_Sims_February__c = 6, Connectivity_Sims_March__c = 8, Connectivity_Sims_April__c = 10);
				schedules.add(schedule);
				schedule = new Unit_Schedule__c(Opportunity__c = opp.Id, Year__c = '2014', Connectivity_Sims_January__c = 4, Connectivity_Sims_February__c = 6, Connectivity_Sims_March__c = 8, Connectivity_Sims_April__c = 10);
				schedules.add(schedule);
				schedule = new Unit_Schedule__c(Opportunity__c = opp.Id, Year__c = '2015', Connectivity_Sims_January__c = 4, Connectivity_Sims_February__c = 6, Connectivity_Sims_March__c = 8);
				schedules.add(schedule);
			}
		}
		System.assertNotEquals(schedules.size(),0);
		insert schedules;
		for(Integer i = 0; i < 10 ; i++)
		{
			Frame_Contract__c fc = new Frame_Contract__c(recordTypeId = fcRecType, Customer__c = accList[i].Id, Opportunity__c = oppList[i].Id, Status__c = 'In Progress');
			frameContracts.add(fc);
		}
		System.assertNotEquals(frameContracts.size(),0);
		insert frameContracts;
        update frameContracts;
        delete frameContracts;
		Test.stopTest();
	} 
}