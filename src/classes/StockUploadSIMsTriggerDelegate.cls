public with sharing class StockUploadSIMsTriggerDelegate  extends TriggerHandler.DelegateBase
{
	List<Stock__c> stock;
	Map<String, SRF__c> sRFs;
	Map<Id, Third_Upload_File__c> tUFs;

	public override void prepareBefore()
	{
		if (Trigger.isInsert)
		{
			Set<Id> srfIds = new Set<Id>();
			stock = Trigger.new;
			for (Stock__c asset : stock)
			{
				if (!srfIds.contains(asset.SRF__c))
				{
					srfIds.add(asset.SRF__c);
				}
			}
			sRFs = new Map<String, SRF__c>([
					SELECT RecordTypeId, Order_Request__c
					FROM SRF__c
					WHERE Id IN :srfIds]);
			tUFs = new Map<Id, Third_Upload_File__c >();
			for (SRF__c srf : sRFs.values())
			{
				if (srf.RecordTypeId == Cache.RecordTypeId('SRF__c.Direct SIM Delivery'))
				{
					tUFs.put(srf.Order_Request__c, new Third_Upload_File__c());
				}
			}

			List<Third_Upload_File__c> tUFsList =  [
					SELECT Id, Order_Request__c
					FROM Third_Upload_File__c
					WHERE Order_Request__c in :tUFs.keySet() ];
			for (Third_Upload_File__c tuf : tUFsList)
			{
				tUFs.put(tuf.Order_Request__c, tuf);
			}
		}
	}


	public override void beforeInsert(sObject o)
	{
		Stock__c item = (Stock__c)o;
		item.State_Change_Date__c = DateTime.now();

		if (item.RecordTypeId == Cache.RecordTypeId('Stock__c.SIM'))
		{
			if (sRFs.get(item.SRF__c).RecordTypeId == Cache.RecordTypeId('SRF__c.Direct SIM Delivery'))
			{
				item.Status__c = 'Reserved';
				item.Third_Upload_File__c = tUFs.get(sRFs.get(item.SRF__c).Order_Request__c).Id;
			}
			else if (sRFs.get(item.SRF__c).RecordTypeId == Cache.RecordTypeId('SRF__c.Internal SIM Delivery'))
			{
				item.Status__c = 'On Stock';
			}
			else if (sRFs.get(item.SRF__c).RecordTypeId == Cache.RecordTypeId('SRF__c.Standard SRF'))
			{
				item.addError('Standard SRF can not be used to upload stock!');
			}
			item.Name = item.ICCID__c;
		}
	}
}