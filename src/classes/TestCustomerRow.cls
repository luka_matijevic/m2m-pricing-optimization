/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCustomerRow
{
	//--------------------------------------------------------------------------
	// Create and retrieve customer row with account address
	//--------------------------------------------------------------------------
    static testMethod void createAndRetrieveRow()
    {
    	//
    	// Create test data
    	//
    		Account accPayer = new Account(
			Name = 'Test account', 
			BillingCity = 'Bremen', 
			BillingCountry = 'Germany', 
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße', 
			Type = 'Customer', 

			VAT__c = 1,
            Global__c = true
			
		);
		insert accPayer;
        
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1)
			, recordtypeid = Cache.getRecordTypeId('Opportunity.Commercial_Global'),
            Description = 'Test description 20 characters'
		);
		insert opp;		
		 Contact con = Test_Util.createContact(accPayer.id);
		
        Frame_Contract__c fc = new Frame_Contract__c();
        fc.contact__c = con.id;
        fc.customer__c = accPayer.id;
        fc.opportunity__c = opp.id;
         insert fc;      
        
		Billing_Account__c billingAccount = new Billing_Account__c(
             Account_number__C = 1234,
			Payer__c = accPayer.Id, 
			Trial__c=false, 
			Billable__c=true,
			VAT__c = '1'
			, OFI_ID__c = '123'
		);
		insert billingAccount;

    	//
    	// Create customer row and retrieve cells
    	//
    	Test.startTest();
    	CustomerRow customerRow = new CustomerRow(billingAccount);
    	List<String> cells = customerRow.getCells();
        system.debug('cells>>>>>>>>>'  + cells);
    	Test.stopTest();
    	//
    	// Tests
    	//
    	System.assertEquals(23, cells.size());
    	System.assertEquals('1234', cells[0]);
        /*
    	System.assertEquals(null, cells[1]);
    	System.assertEquals('Unit Test Billing Account', cells[2]);
    	System.assertEquals('Unit Test Street', cells[3]);
    	System.assertEquals('Krizevci', cells[4]);
    	System.assertEquals('48260', cells[5]);
    	System.assertEquals('HR', cells[6]);
    	System.assertEquals('E', cells[7]);
    	System.assertEquals('+38512345678', cells[8]);
    	System.assertEquals('+38587654321', cells[9]);
    	System.assertEquals(null, cells[10]);
    	System.assertEquals('+38522222222', cells[11]);
    	System.assertEquals('unit.test@m2m.com', cells[12]);
    	System.assertEquals('TaxID', cells[13]);
    	System.assertEquals('Bank Identifier', cells[14]);
    	System.assertEquals('87654321', cells[15]);
    	System.assertEquals('Bank Name', cells[16]);
    	System.assertEquals('Bank Address', cells[17]);
    	System.assertEquals('30 days', cells[18]);
    	System.assertEquals('Überweisung', cells[19]);
    	System.assertEquals(null, cells[20]);
    	System.assertEquals('Sales Representative', cells[21]);
    	System.assertEquals(null, cells[22]);
        */
    }

	//--------------------------------------------------------------------------
	// Create and retrieve customer row with billing account address
	//--------------------------------------------------------------------------
    static testMethod void createAndRetriveRowWithBillingAddress()
    {
    	//
    	// Create test data
    	//
    			Account accPayer = new Account(
			                    
			Name = 'Test account', 
			BillingCity = 'Bremen', 
			BillingCountry = 'Germany', 
			BillingPostalCode = '5000',
			BillingStreet = 'Lindenstraße', 
			Type = 'Customer', 

			VAT__c = 1,
            Global__c = true
			
		);
		insert accPayer;
        
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1)
			, recordtypeid = Cache.getRecordTypeId('Opportunity.Commercial_Global'),
            Description = 'Test description 20 characters'
		);
		insert opp;		
		 Contact con = Test_Util.createContact(accPayer.id);
		
        Frame_Contract__c fc = new Frame_Contract__c();
        fc.contact__c = con.id;
        fc.customer__c = accPayer.id;
        fc.opportunity__c = opp.id;
         insert fc;      
        
		Billing_Account__c billingAccount = new Billing_Account__c(
            Account_number__C = 1234,
			Payer__c = accPayer.Id, 
			Trial__c=false, 
			Billable__c=true,
			VAT__c = '1'
			, OFI_ID__c = '123'
		);
		insert billingAccount;
    	//
    	// Change use account address field
    	//
    	

    	//
    	// Create customer row and retrieve cells
    	//
    	Test.startTest();
    	CustomerRow customerRow = new CustomerRow(billingAccount);
    	List<String> cells = customerRow.getCells();
    	Test.stopTest();
    	//
    	// Tests
    	//
    	System.assertEquals(23, cells.size());
    	System.assertEquals('1234', cells[0]);
/*System.assertEquals(null, cells[1]);
    	System.assertEquals('Unit Test Billing Account', cells[2]);
    	System.assertEquals('Billing Street', cells[3]);
    	System.assertEquals('Billing City', cells[4]);
    	System.assertEquals('10000', cells[5]);
    	System.assertEquals('DE', cells[6]);
    	System.assertEquals('E', cells[7]);
    	System.assertEquals('+38512345678', cells[8]);
    	System.assertEquals('+38587654321', cells[9]);
    	System.assertEquals(null, cells[10]);
    	System.assertEquals('+38522222222', cells[11]);
    	System.assertEquals('unit.test@m2m.com', cells[12]);
    	System.assertEquals('TaxID', cells[13]);
    	System.assertEquals('Bank Identifier', cells[14]);
    	System.assertEquals('87654321', cells[15]);
    	System.assertEquals('Bank Name', cells[16]);
    	System.assertEquals('Bank Address', cells[17]);
    	System.assertEquals('30 days', cells[18]);
    	System.assertEquals('Überweisung', cells[19]);
    	System.assertEquals(null, cells[20]);
    	System.assertEquals('Sales Representative', cells[21]);
    	System.assertEquals(null, cells[22]);
*/
    }
}