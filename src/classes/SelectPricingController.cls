public class SelectPricingController {

    private string oppId;
    public string productBundleId { get; set; }
    public List<cscfga__Product_Bundle__c> productBundlesList {get; set;}
    public ProcessInstance currentApprovalProcess;
    public Opportunity opp {get; set;}
    
    
          // ALWAYS CHECK THIS BEFORE PRODUCTION DEPLOY !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    /*********************************************************************************************************/
    private String labelConfigCopyRequestId = Label.Config_Copy_Request_Id;
    private String labelTariffId = Label.Tariff_Id;
    private String labelDummyTariffId = Label.Dummy_Tariff_Id;
    private String labelApprovalProcessId = Label.Approval_Process_Id;
    /*************************************************************************************************************/
    
    public SelectPricingController(ApexPages.StandardController cntrl) {
		initiatePage();
	}
	
	public void initiatePage() {
	        
       string message = ApexPages.currentPage().getParameters().get('msg');
             
       if (message!=null) { 
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,message);
            ApexPages.addMessage(myMsg);
       }
	    
       oppId  = ApexPages.currentPage().getParameters().get('id');
       
       string opquery = 'SELECT ' + SobjectHelper.getFieldListForSOQL('Opportunity', null, null) + ' FROM Opportunity where id = :oppId'; 
       opp = database.query(opquery);
       
       productBundlesList =  new List<cscfga__Product_Bundle__c>();
       
       string pbquery = 'SELECT ' + SobjectHelper.getFieldListForSOQL('cscfga__Product_Bundle__c', null, null) + ' from cscfga__Product_Bundle__c where cscfga__Opportunity__c=\'' + oppId + '\' ORDER BY CreatedDate DESC';
       productBundlesList = Database.query(pbquery);
       
       
       if(productBundlesList.size() == 0) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Product bundle list is empty!');
            ApexPages.addMessage(myMsg);
       }
       
       
	}


    public boolean getShowTable(){
        boolean show = true;
        if(productBundlesList.size() == 0){
            show = false;
        }
        return show;
    }
    
    public PageReference addNewPricing() {
        
        cscfga__Product_Bundle__c theBundle = new cscfga__Product_Bundle__c();
        theBundle.cscfga__Opportunity__c=oppId;
    
        insert theBundle;
    
        cscfga__Product_Basket__c basket= new cscfga__Product_Basket__c();
        basket.cscfga__Opportunity__c=oppId;
        insert basket;
        
        if(opp.Product_Family__c=='Horizontals') {
        List<cscfga.ProductConfigurationHelper.ConfigurationCopyRequest> lista = new List<cscfga.ProductConfigurationHelper.ConfigurationCopyRequest>();

        lista.add(new cscfga.ProductConfigurationHelper.ConfigurationCopyRequest(labelConfigCopyRequestId, 1));

        cscfga.ProductConfigurationHelper pchelper = new cscfga.ProductConfigurationHelper();

        cscfga.ProductConfigurationHelper.copyConfigurations(lista, theBundle.id,basket.id);

        string query_pc = 'SELECT ' +  SObjectHelper.getFieldListForSOQL('cscfga__product_configuration__c',null,null) + ' FROM cscfga__product_configuration__c where cscfga__product_bundle__c=\''+theBundle.id+'\'';    
		List<cscfga__Product_Configuration__c> pclist = database.query(query_pc);   
        PageReference newocp = new PageReference('/apex/pricingmainpage?oppId=' + oppId + '&Id=' + theBundle.id + '&configurationId=' +  pclist[0].id + '&tariffId=' + labelTariffId);
        newocp.setRedirect(true);
        return newocp;
            
        }
        else {

            PageReference newocp = new PageReference('/apex/pricingservicespage?oppId=' + oppId + '&Id=' + theBundle.id);
            newocp.setRedirect(true);
        
            return newocp;
        }
    }
    
    public PageReference editExistingBundle() {
        string productConfigurationId;
        PageReference newocp;
        List<cscfga__Product_Configuration__c> productConfigurations = [SELECT Id, cscfga__Product_Bundle__c, CreatedDate FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Bundle__c = :productBundleId ORDER BY CreatedDate ASC];
        
        if(productConfigurations != null && productConfigurations.size() > 0) {
            productConfigurationId = productConfigurations[0].Id;
        }
        
          List<ProcessInstance> approvalProcess = [SELECT Id, Status, TargetObjectId, CreatedDate FROM ProcessInstance WHERE TargetObjectId = :productBundleId ORDER BY CreatedDate DESC];
	        if(approvalProcess != null && approvalProcess.size() > 0) {
	            currentApprovalProcess = approvalProcess[0];
        if (currentApprovalProcess.Status == 'Pending') {
               if(opp.Product_Family__c=='Horizontals') newocp = new PageReference('/apex/submitpricing?oppId=' + oppId + '&Id=' + productBundleId + '&configurationId=' + productConfigurationId + '&tariffId=' + labelTariffId);
               else newocp = new PageReference('/apex/pricingservicespage?oppId=' + oppId + '&Id=' + productBundleId);
        }
        else {
          if(opp.Product_Family__c=='Horizontals')  newocp = new PageReference('/apex/pricingmainpage?oppId=' + oppId + '&Id=' + productBundleId + '&configurationId=' + productConfigurationId + '&tariffId=' + labelTariffId);
             else newocp = new PageReference('/apex/pricingservicespage?oppId=' + oppId + '&Id=' + productBundleId);
        }
        }
        else {
        if(opp.Product_Family__c=='Horizontals') newocp = new PageReference('/apex/pricingmainpage?oppId=' + oppId + '&Id=' + productBundleId + '&configurationId=' + productConfigurationId + '&tariffId=' + labelTariffId);
           else newocp = new PageReference('/apex/pricingservicespage?oppId=' + oppId + '&Id=' + productBundleId);
        }
        newocp.setRedirect(true);
        
        return newocp;
    }
    
    public PageReference deleteExistingBundle() {
        PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
        
        cscfga__Product_Bundle__c bundle = [SELECT Id FROM cscfga__Product_Bundle__c WHERE Id = :productBundleId];
        
        delete bundle;
        
        pageRef.setRedirect(true);
        return pageRef;
    }
}