@isTest
public class Assign_commericial_FC_Test {

    @testsetup
    static void prepareTestData(){
        List<account> accLst = new List<account>();
        accLst.add(Test_Util.createAccount('Account 1'));
        accLst.add(Test_Util.createAccount('Account 2'));
        insert accLst;
        
        Id trailFCRTID = Schema.SObjectType.Frame_Contract__c.getRecordTypeInfosByName().get('Trial').getRecordTypeId();
        Id commercialFCRTID = Schema.SObjectType.Frame_Contract__c.getRecordTypeInfosByName().get('Commercial').getRecordTypeId();
        
        
        list<Frame_Contract__c> fcLst = new list<Frame_Contract__c>();
        for(Account acc : accLst){
            fcLst.add(new Frame_Contract__c(Customer__c = acc.id,RecordTypeID =trailFCRTID));
            fcLst.add(new Frame_Contract__c(Customer__c = acc.id,RecordTypeID =commercialFCRTID));
        }
        
        insert fcLst;
        fcLst = [select id , Type__c , Customer__c,RecordTypeID,RecordType.Name from Frame_Contract__c];
        list<Order_Request__c> ordLst = new list<Order_Request__c>();
        for(Frame_Contract__c fc : fcLst){
            ordLst.add(new Order_Request__c(Frame_Contract__c = fc.id,Account__c =fc.Customer__c,Status__c = 'Closed',RunTime__c=12, Shipping_City__c='Test City', Shipping_Country__c = 'Test Country', Shipping_Postal_Code__c='10000', Shipping_Street__c='Test shipping street'));
        }
        
        insert ordLst;
        
        
        list<Third_Upload_File__c> tufLst = new list<Third_Upload_File__c>();
        
        ordLst = [select id,Account__c, Frame_Contract__r.Type__c from Order_Request__c];
        for(Order_Request__c ord : ordLst){
             tufLst.add(new Third_Upload_File__c(Account__c = ord.Account__c,Order_Request__c = ord.id));
        }
        
        insert tufLst;
        
        
        List<Stock__c> stkLst = new list<Stock__c>();
        
        for(Third_Upload_File__c tuf : tufLst){
             stkLst.add(new Stock__c(Account__c =tuf.Account__c ,Third_Upload_File__c = tuf.id,Commercial_Date__c = System.today()));
        }
        
        insert stkLst;
    }
    
    static testMethod void testAssignment(){
        
        Test.startTest();
         
        dataBAse.executeBatch(new Assign_commericial_FC());
        
        List<Stock__c> stkLst = [Select id,Third_Upload_File__c,Third_Upload_File__r.Account__C,Activation_Date__c,Commercial_Date__c from Stock__c where Third_Upload_File__r.Order_Request__r.Frame_Contract__r.Type__C= 'Commercial' Order by Third_Upload_File__c];
        system.assertEquals(stkLst.size(), 2);
        
        Test.stopTest();
    }
    
    static testMethod void populatingEnDdate(){
        
        Test.startTest();
         
        dataBAse.executeBatch(new Populate_OrderRequest_On_Stock());
        
        List<Stock__c> stkLst = [Select id,Third_Upload_File__c,Third_Upload_File__r.Account__C,Activation_Date__c,Commercial_Date__c from Stock__c where Third_Upload_File__r.Order_Request__r.Frame_Contract__r.Type__C= 'Commercial' Order by Third_Upload_File__c];
        system.assertEquals(stkLst.size(), 2);
        
        Test.stopTest();
    }

}