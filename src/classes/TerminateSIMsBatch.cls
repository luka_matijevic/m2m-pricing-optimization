global class TerminateSIMsBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    
    private static final String TERMINATION_STATUS_REQUESTED = 'REQUESTED';
    private static final String TERMINATION_STATUS_PENDING = 'PENDING';
    private static final String SM2M_STATUS_SUSPENDED = 'SUSPENDED';
    //Petar Matkovic
    //End Date is calculated as -1 day from Potential Termination Date but termination is day after that end date
    String query = 'Select Id,Name,End_date__c,Termination_Status__c,SM2M_Status__c from stock__C where End_date__c < TODAY AND Termination_Status__c = \'' + TERMINATION_STATUS_REQUESTED + '\' and SM2M_Status__c = \'ACTIVE\'';
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> sims){
        System.debug('1 test');
        List<Stock__c> simsToUpdate = new List<Stock__c>();
        for(Stock__c stock : (List<Stock__c>)sims){
            //stock.SM2M_Status__c = SM2M_STATUS_SUSPENDED;
            stock.Termination_Status__c = TERMINATION_STATUS_PENDING;
            stock.State_Change_Date__c = Date.today();
            simsToUpdate.add(stock);
        }
        if(!simsToUpdate.isEmpty()){
            update simsToUpdate;
        }
        Datetime now = Datetime.now();
        System.debug('2 test');
        //Call Heroku to update the status and on succesful responce set the Termination Status to Terminated
        SIM_Termination_Job__c jobData = new SIM_Termination_Job__c(
			Name = 'SIM Termination JOB @' + now.formatLong(),
			Status__c = Constants.TERMINATION_PENDING
		);
		
		insert jobData;
        System.enqueueJob(new AsyncSIMTerminationSendMessage(jobData));
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
}