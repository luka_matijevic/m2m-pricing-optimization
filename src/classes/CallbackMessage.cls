/**
 * @description Class for structured callbacks. Using it and its JSON representation, integration server sends
 * instructions how to instantiate ObjectGraph, provides a Map of Ids that are used in ferences, sObjects to insert,
 * update and delete (as separate lists) and callback message(s) with corresponding object record ids.
 * @author Hrvoje Tutman
 */
public class CallbackMessage {
	/**
	 * List of objects in ObjectGraph. The list is used for object graph instantiation.
	 */
	public List<String> objectsInGraph {get; }

	/**
	 * Set of object Ids referenced by sObjects to insert.
	 */
	public Set<Id> idsToMap {get; }

	/**
	 * When the reference is no longer under the same callback message (e.g. due to splitting), to reference the record
	 * an SOQL statement can be provided using the map.
	 */
	public Map<Id, String> idsToSoqlMap {get; }

	/**
	 * sObjects to insert
	 */
	public List<sObject> objectsToInsert {get; set;}

	/**
	 * sObjects to delete
	 */
	public List<sObject> objectsToDelete {get; }

	/**
	 * sObjects to update
	 */
	public List<sObject> objectsToUpdate {get; }
}