/*
* BillingAcountTriggerDelegate extends Trigger handler to execute calls based on events.
*/
public with sharing class BillingAccountDiscountTriggerDelegate extends TriggerHandler.DelegateBase {

  
    public override void prepareBefore() {
        // do any preparation here – bulk loading of data etc

    }

    public override void prepareAfter() {
        // do any preparation here – bulk loading of data etc
    }
  public override void beforeInsert(sObject o) {
        // Apply before insert logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method    
         Billing_Account_Discount__c newAcc = (Billing_Account_Discount__c)o;
         Integer wrongCount = 0;
         
         if(newAcc.Runtime__c!='Unlimited') {
             newAcc.Date_End__c=newAcc.Date_Start__c.addmonths(Integer.valueOf(newAcc.Runtime__c));
         }
         else newAcc.Date_End__c=Date.newInstance(2030, 12, 31);
         
         newAcc.Approval_Status__c='Pending';
         
         
                      
             List<Billing_Account_Discount__c> oldAccList = [Select Id, Date_Start__c from Billing_Account_Discount__c where Billing_Account__c=:newAcc.Billing_Account__c 
             and Discount_Type__c=:newAcc.Discount_Type__c and Billing_Account_Subsc_Group__c=:newAcc.Billing_Account_Subsc_Group__c];
             if (!oldAccList.isEmpty()) {
                 for (Billing_Account_Discount__c bcd : oldAccList) {
                     //allow insert of maximum two accounts in running month (start dates are in this or in the next month)
                     if (bcd.Date_Start__c.month()==newAcc.Date_Start__c.month() || bcd.Date_Start__c.addmonths(-1).month()==newAcc.Date_Start__c.month())
                     wrongCount++;
                 }
             }
             
             if (wrongCount>1) {
                 newAcc.addError('It is not possible to create this discount. There are more than two discounts in the running month with the same parameters on the same billing account.');
             }
        
    }
    public override void beforeUpdate(sObject old, sObject o) {
        Billing_Account_Discount__c oldestAcc = (Billing_Account_Discount__c)old;
        Billing_Account_Discount__c newAcc = (Billing_Account_Discount__c)o;
        
        if(newAcc.Approval_Status__c=='Approved') {
             
             List<Billing_Account_Discount__c> oldAccList = [Select Id, Date_Start__c from Billing_Account_Discount__c where Billing_Account__c=:newAcc.Billing_Account__c 
             and Discount_Type__c=:newAcc.Discount_Type__c and Billing_Account_Subsc_Group__c=:oldestAcc.Billing_Account_Subsc_Group__c and Id!=:oldestAcc.Id and CreatedDate<:oldestAcc.CreatedDate];
             if (!oldAccList.isEmpty()) {
                Billing_Account_Discount__c oldAcc = oldAccList[0];    
                 if (oldAcc!=null) {
                 
                 newAcc.Date_Start__c= Date.today().toStartOfMonth().Addmonths(1);
                 oldAcc.Date_End__c=newAcc.Date_Start__c.addDays(-1);
                 
                  if(newAcc.Runtime__c!='Unlimited') {
                        newAcc.Date_End__c=newAcc.Date_Start__c.addmonths(Integer.valueOf(newAcc.Runtime__c));
                    }
                    else newAcc.Date_End__c= Date.newInstance(2030, 12, 31);
                 
                 update oldAcc;
                }
            }    
        }
    }

    public override void beforeDelete(sObject o) {
        // Apply before delete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterInsert(sObject o) {
        Billing_Account_Discount__c newAcc = (Billing_Account_Discount__c)o;
        // Apply after insert logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
         Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setComments('Submitted for approval. Please approve.');
            req.setObjectId(newAcc.Id);
            Approval.ProcessResult result = Approval.process(req);

        
    }

    public override void afterUpdate(sObject old, sObject o) {
        // Apply after update logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterDelete(sObject o) {
        // Apply after delete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterUndelete(sObject o) {
        // Apply after undelete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void finish() {

    }
    
}