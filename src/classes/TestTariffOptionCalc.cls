@isTest
private class TestTariffOptionCalc
{

	//--------------------------------------------------------------------------
	// Test tariff option calculation
	//--------------------------------------------------------------------------
    static testMethod void testTariffOptionCalc()
	{
		TariffOptionCalc tariffOptionCalc =
			new TariffOptionCalc(createTariffOption());
		//
		// Test
		//
		system.assertEquals(3960.9389 * 6, tariffOptionCalc.getDataOverUsage());
		system.assertEquals(756.459 * 6, tariffOptionCalc.getTextOverUsage());
		system.assertEquals(3833.877 * 6, tariffOptionCalc.getVoiceOverUsage());
		system.assertEquals(84.55, tariffOptionCalc.getOneTimeTargetFeePerCustomer());
		system.assertEquals(317.38, tariffOptionCalc.getOneTimeTargetFeePerVolume());
		system.assertEquals(1988.83, tariffOptionCalc.getOneTimeTargetFeeTotal());
		system.assertEquals(290.68, tariffOptionCalc.getRecurringTargetFeePerCustomer());
		system.assertEquals(135.49, tariffOptionCalc.getRecurringTargetFeePerVolume());
		system.assertEquals(1103.62, tariffOptionCalc.getRecurringTargetFeeTotal());
    }

	//--------------------------------------------------------------------------
	// Create tariff option for test
	//--------------------------------------------------------------------------
	static ITariffOption createTariffOption()
	{
		Tariff_Option__c to = new Tariff_Option__c();
		to.Name = 'Test tariff option';
		to.Recurring_Base_Fee__c = 10.32;
		to.Recurring_Target_Fee__c = 43.23;
		ITariffOption returnValue =
			new TariffOption(to, createRateCards(), createServiceFees(), true, 6);
		return returnValue;
	}

	//--------------------------------------------------------------------------
	// Create rate cards for test
	//--------------------------------------------------------------------------
	static List<IRateCard> createRateCards()
	{
		List<IRateCard> returnValue = new List<IRateCard>();

		Rate_Card__c rateCard1 = new Rate_Card__c();
		rateCard1.Name = 'Test Rate Card 1';
		rateCard1.Clock_Rate__c = 'MB';
		rateCard1.Target_Usage_Fee__c = 12.43;
		rateCard1.Expected_Usage__c = 54.54;

		Rate_Card__c rateCard2 = new Rate_Card__c();
		rateCard2.Name = 'Test Rate Card 2';
		rateCard2.Clock_Rate__c = 'kB';
		rateCard2.Target_Usage_Fee__c = 54.29;
		rateCard2.Expected_Usage__c = 59.41;

		Rate_Card__c rateCard3 = new Rate_Card__c();
		rateCard3.Name = 'Test Rate Card 3';
		rateCard3.Clock_Rate__c = '10 kB';
		rateCard3.Target_Usage_Fee__c = 02.46;
		rateCard3.Expected_Usage__c = 23.43;

		Rate_Card__c rateCard4 = new Rate_Card__c();
		rateCard4.Name = 'Test Rate Card 4';
		rateCard4.Clock_Rate__c = 'SMS';
		rateCard4.Target_Usage_Fee__c = 26.73;
		rateCard4.Expected_Usage__c = 28.3;

		Rate_Card__c rateCard5 = new Rate_Card__c();
		rateCard5.Name = 'Test Rate Card 5';
		rateCard5.Clock_Rate__c = 'Min';
		rateCard5.Target_Usage_Fee__c = 38.66;
		rateCard5.Expected_Usage__c = 23.60;

		Rate_Card__c rateCard6 = new Rate_Card__c();
		rateCard6.Name = 'Test Rate Card 6';
		rateCard6.Clock_Rate__c = 'Sec';
		rateCard6.Target_Usage_Fee__c = 19.83;
		rateCard6.Expected_Usage__c = 95.9;

		Rate_Card__c rateCard7 = new Rate_Card__c();
		rateCard7.Name = 'Test Rate Card 7';
		rateCard7.Clock_Rate__c = '60/60';
		rateCard7.Target_Usage_Fee__c = 43.6;
		rateCard7.Expected_Usage__c = 23.39;

		returnValue.add(new RateCard(rateCard1,new List<IRateCardCountryManagement>(),new List<SelectOption>()));
		returnValue.add(new RateCard(rateCard2,new List<IRateCardCountryManagement>(),new List<SelectOption>()));
		returnValue.add(new RateCard(rateCard3,new List<IRateCardCountryManagement>(),new List<SelectOption>()));
		returnValue.add(new RateCard(rateCard4,new List<IRateCardCountryManagement>(),new List<SelectOption>()));
		returnValue.add(new RateCard(rateCard5,new List<IRateCardCountryManagement>(),new List<SelectOption>()));
		returnValue.add(new RateCard(rateCard6,new List<IRateCardCountryManagement>(),new List<SelectOption>()));
		returnValue.add(new RateCard(rateCard7,new List<IRateCardCountryManagement>(),new List<SelectOption>()));

		return returnValue;
	}

	//--------------------------------------------------------------------------
	// Create service fees for test
	//--------------------------------------------------------------------------
	static List<IServiceFee> createServiceFees()
	{
		List<IServiceFee> returnValue = new List<IServiceFee>();

		Service_Fee__c serviceFee1 = new Service_Fee__c();
		serviceFee1.Name = 'TestService Fee 1';
		serviceFee1.Charged_Per__c = 'Customer';
		serviceFee1.One_Time_Target_Fee__c = 42.43;
		serviceFee1.Recurring_Target_Fee__c = 23.34;

		Service_Fee__c serviceFee2 = new Service_Fee__c();
		serviceFee2.Name = 'TestService Fee 2';
		serviceFee2.Charged_Per__c = 'Customer';
		serviceFee2.One_Time_Target_Fee__c = 42.12;
		serviceFee2.Recurring_Target_Fee__c = 267.34;

		Service_Fee__c serviceFee3 = new Service_Fee__c();
		serviceFee3.Name = 'TestService Fee 3';
		serviceFee3.Charged_Per__c = 'Volume';
		serviceFee3.One_Time_Target_Fee__c = 234.54;
		serviceFee3.Recurring_Target_Fee__c = 54.32;

		Service_Fee__c serviceFee4 = new Service_Fee__c();
		serviceFee4.Name = 'TestService Fee 4';
		serviceFee4.Charged_Per__c = 'Volume';
		serviceFee4.One_Time_Target_Fee__c = 82.84;
		serviceFee4.Recurring_Target_Fee__c = 37.94;

		returnValue.add(new ServiceFee(serviceFee1));
		returnValue.add(new ServiceFee(serviceFee2));
		returnValue.add(new ServiceFee(serviceFee3));
		returnValue.add(new ServiceFee(serviceFee4));

		return returnValue;
	}
}