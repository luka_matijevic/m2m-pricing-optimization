@isTest
private class TestStockAccountUpdateJob
{
	static void setupData()
	{
		No_Trigger__c notriggers = new No_Trigger__c();
		notriggers.Flag__c = true;
		insert notriggers;

		//
		// Create test data
		//
		TestHelper.createOrderRequest();
		Account account = [ SELECT Id FROM Account LIMIT 1 ];
		Order_Request__c ord = [ SELECT Id FROM Order_Request__c LIMIT 1 ];
		Article_Info_Set__c ais = [ 
			SELECT Id
			FROM Article_Info_Set__c
			WHERE Article_Type__c = 'SIM'
			LIMIT 1
		];

		Third_Upload_File__c thirdFile = new Third_Upload_File__c(
			Name = 'name', 
			Order_Request__c = ord.Id,
			Upload_to_SM2M_Status__c = 'Uploaded'
		);

		insert thirdFile;

		SRF__c srf = new SRF__c(
			Account__c = account.Id,
			SIM_Type__c = ais.Id
		);
		insert srf;

		Stock__c[] assets = new List<Stock__c>();
		assets.add(new Stock__c(
			Name = 'Stock1',
			SIM_Type__c = 'LOCAL',
			Third_Upload_File__c = thirdFile.Id
		));
		
		insert assets;

		notriggers.Flag__c = false;
		update notriggers;
	}

    static testMethod void testUpdateStockAccount()
	{
		setupData();

		Stock__c[] assets = [ SELECT Account__c FROM Stock__c ];
		System.assertEquals(1, assets.size());
		System.assertEquals(null, assets[0].Account__c);

		Test.startTest();
		Database.executeBatch(new StockAccountUpdateJob());
		Test.stopTest();

		//
		// Assertions
		//
		Account account = [ SELECT Id FROM Account ];
		assets = [SELECT Account__c FROM Stock__c ];
		System.assertEquals(1, assets.size());
		System.assertEquals(account.Id, assets[0].Account__c);
    }
}