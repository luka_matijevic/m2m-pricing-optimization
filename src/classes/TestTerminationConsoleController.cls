@isTest
public class TestTerminationConsoleController {

    @testSetup
    static void prepareData(){
        Account acc = Test_Util.createAccount('Account Test');
        insert acc;
        
        Case cs = Test_Util.createCase('Service','Complaints','Open',acc.id, 'Test Demo111', '24', 'Standard', 'Email','High', 'test@t.com');
        insert cs;
        
        
        list<Stock__C> stkLst = new list<Stock__c>();
        for(integer i=0; i < 10 ; i++){
            string name = '893407110028938747'+i;
            stkLst.add(Test_Util.createStock(name,acc.id));
        }
        insert stkLst;
        
    }
    
    static testMethod void TerminationConsoleController_Test(){
        List<Case> cseLst = new list<Case>([select id from Case limit 1]);
        if(cseLst != null && cseLst.size() > 0){
            PageReference pf = Page.TerminationConsolePage;
            pf.getPArameters().put('id',cseLst[0].id);
            Test.setCurrentPage(pf);
            
            Test.startTest();
            TerminationConsoleController obj = new TerminationConsoleController();
            system.assertEquals(obj.displayUploadArea, false);
            system.assertEquals(obj.displayMainContent, true);
            
            obj.swapRendering();
            
            system.assertEquals(obj.displayUploadArea, true);
            system.assertEquals(obj.displayMainContent, false);
            
            obj.addAllSims();
            obj.backToTerminationWindow();
            
            List<Stock__c> allSims = obj.AllSims;
            obj.performSearchbyFC();
            
            String fileBodyString = '8934071100289387470' + '\n' + '8934071100289387471';
            Blob fileBody = blob.valueOf(fileBodyString);
            obj.fileBody = fileBody;
            obj.fileNAme = 'FileName';
            obj.uploadSims();
            obj.putDataIntoSetCtrl('iccid');
            obj.putDataIntoSetCtrl('selectedSims');
            List<Stock__c> fileData = obj.fileData;
            boolean b = obj.hasNext;
            b = obj.hasPrevious;
            integer n = obj.getPageNumber();
            n = obj.getTotalPages();
            obj.first();
            obj.last();
            obj.previous();
            obj.next();
            
            b = obj.hasPNext;
            b = obj.hasPPrevious;
            n = obj.getPPageNumber();
            n = obj.getPTotalPages();
            obj.pfirst();
            obj.plast();
            obj.pprevious();
            obj.pnext();
            
            obj.exportToExcel();
            obj.exportToPDF();
            obj.terminateSims();
            obj.stopTermination();
            obj.addAllToSimList();
            if(allSims.size() > 0){
                obj.stockId = allSims[0].id;
            }
            
            obj.addSelectedSIM();
            obj.performSearch();
            if(allSims.size() > 0){
                obj.removeSim = allSims[0].id;
            }
            obj.removeSim();
            obj.clearSelectedSims();
            
            Test.stopTest();
        }
        
    }
    
    
    static testMethod void searchKeyTest(){
        List<Case> cseLst = new list<Case>([select id from Case limit 1]);
        if(cseLst != null && cseLst.size() > 0){
            PageReference pf = Page.TerminationConsolePage;
            pf.getPArameters().put('id',cseLst[0].id);
            Test.setCurrentPage(pf);
            
            Test.startTest();
            TerminationConsoleController obj = new TerminationConsoleController();
            system.assertEquals(obj.displayUploadArea, false);
            system.assertEquals(obj.displayMainContent, true);
            
            obj.putDataIntoSetCtrl('allsIms');
            obj.iccIds.add('8934071100289387470');
            obj.iccIds.add('8934071100289387471');
            obj.iccIds.add('8934071100289387472');
            String fileBodyString = '8934071100289387470' + '\n' + '8934071100289387471';
            Blob fileBody = blob.valueOf(fileBodyString);
            obj.fileBody = fileBody;
            obj.fileNAme = 'FileName';
            obj.uploadSims();
            List<Stock__c> fileData = obj.fileData;
            obj.searchKey = '40711002893';
            fileData = obj.fileData;
            
            
            
            Test.stopTest();
        }
        
    }
    
    
    static testMethod void searchFCTest(){
        List<Case> cseLst = new list<Case>([select id from Case limit 1]);
        if(cseLst != null && cseLst.size() > 0){
            PageReference pf = Page.TerminationConsolePage;
            pf.getPArameters().put('id',cseLst[0].id);
            Test.setCurrentPage(pf);
            
            Test.startTest();
            TerminationConsoleController obj = new TerminationConsoleController();
            system.assertEquals(obj.displayUploadArea, false);
            system.assertEquals(obj.displayMainContent, true);
            obj.putDataIntoSetCtrl('selectedSims');
            Test.stopTest();
        }
        
    }

}