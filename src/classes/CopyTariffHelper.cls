/*
	Petar Matkovic - 8.9.2016.
*/
public with sharing class CopyTariffHelper {
	Id accountId;

	public CopyTariffHelper() {
		
	}

	public Id copyTariffWithAllOptions(String idTariff, Id bundleId){
		cscfga__Product_Bundle__c productBundle =
			[ SELECT Id, cscfga__Opportunity__r.Account.Id, Name,
			  cscfga__Opportunity__r.Account.Name
			  FROM cscfga__Product_Bundle__c
			  WHERE Id = :bundleId ];
		this.accountId = productBundle.cscfga__Opportunity__r.Account.Id;

		Tariff__c tariff = getTariffById(idTariff);
		Tariff__c newTariff = null;
		
		if (tariff != null){
			newTariff = createNewTariffFromTariff(tariff, accountId);
			insert newTariff;
		}

		if (newTariff != null){
			cloneTariffOptionsAllData(idTariff, newTariff.Id);
		}

		return newTariff.Id;
	}

	/*****************************************************************
		TARIFF
	*****************************************************************/
	private Tariff__c getTariffById(String idTariff){
		Tariff__c tariff = null;
		String tariffQuery = 'SELECT ' + SObjectHelper.getFieldListForSOQL('Tariff__c', null, null)
							+ ' FROM Tariff__c'
							+ ' WHERE ID = \'' + idTariff +'\'';
		List<Tariff__c> tariffList = Database.query(tariffQuery);
		if (tariffList != null && tariffList.size() > 0){
			tariff = tariffList[0];
		}
		return tariff;
	}

	private Tariff__c createNewTariffFromTariff(Tariff__c tariff, Id accountId){
		Tariff__c newTariff = new Tariff__c();
		newTariff.Account__c = accountId;
		newTariff.Code__c = tariff.Code__c;
		newTariff.Commercial_Plan__c = tariff.Commercial_Plan__c;
		newTariff.Contract_Term__c = tariff.Contract_Term__c;
		newTariff.Description_DE__c = tariff.Description_DE__c;
		newTariff.Description_ENG__c = tariff.Description_ENG__c;
		newTariff.Global_Local__c = tariff.Global_Local__c;
		newTariff.Group__c = tariff.Group__c;
		newTariff.One_Off_Price__c = tariff.One_Off_Price__c;
		newTariff.One_Off_Price_Billing_Description__c = tariff.One_Off_Price_Billing_Description__c;
		newTariff.Product_Family__c = tariff.Product_Family__c;
		newTariff.Recurring_Price__c = tariff.Recurring_Price__c;
		newTariff.Recurring_Price_Billing_Description__c = tariff.Recurring_Price_Billing_Description__c;
		newTariff.Sales_Channel__c = tariff.Sales_Channel__c;
		newTariff.Volume_End__c = tariff.Volume_End__c;
		newTariff.Volume_Start__c = tariff.Volume_Start__c;
		newTariff.Name = tariff.Name;
		return newTariff;
	}

	/*********************************************************************************
		TARIFF OPTIONS
	**********************************************************************************/
	private void cloneTariffOptionsAllData(String idTariffOriginal, String idTariffNew){
		List<Tariff_Option__c> originalTariffOptionAllDataList = getTariffOptionsAllDataById(idTariffOriginal);
		List<Tariff_Option__c> newTariffOptionList = new List<Tariff_Option__c>();
		
		for (Tariff_Option__c originalTariffOption : originalTariffOptionAllDataList){
			newTariffOptionList.add(createNewTariffOptionFromTariffOption(originalTariffOption, idTariffNew));
		}

		insert newTariffOptionList;

		List<Rate_Card__c> newRateCardList = new List<Rate_Card__c>();
		for (Tariff_Option__c newTariffOption : newTariffOptionList){
			//find all rate cards for TariffOption
			Tariff_Option__c rateCardTo = getRateCard(originalTariffOptionAllDataList, newTariffOption.Name);
			if (rateCardTo != null){
				newRateCardList.addAll(createRateCardsFromRateCard(rateCardTo.Rate_Cards__r, newTariffOption.Id));
			}
		}

		insert newRateCardList;
	}

	private Tariff_Option__c createNewTariffOptionFromTariffOption(Tariff_Option__c tariffOption, String newTariffId){
		Tariff_Option__c newtariffOption = new Tariff_Option__c();

		newtariffOption.Name = tariffOption.Name;
		newtariffOption.Can_Discount__c = tariffOption.Can_Discount__c;
		newtariffOption.Clock_Rate__c = tariffOption.Clock_Rate__c;
		newtariffOption.Commercial_Plan__c = tariffOption.Commercial_Plan__c;
		newtariffOption.Default_configuration__c = tariffOption.Default_configuration__c;
		newtariffOption.Description_DE__c = tariffOption.Description_DE__c;
		newtariffOption.Description_ENG__c = tariffOption.Description_ENG__c;
		newtariffOption.Edited_Fee__c = tariffOption.Edited_Fee__c;
		newtariffOption.isConfiguration__c = tariffOption.isConfiguration__c;
		newtariffOption.isCustomOption__c = tariffOption.isCustomOption__c;
		newtariffOption.isEdited__c = tariffOption.isEdited__c;
		newtariffOption.isUsed__c = tariffOption.isUsed__c;
		newtariffOption.One_Time_Base_Fee__c = tariffOption.One_Time_Base_Fee__c;
		newtariffOption.One_Time_Target_Fee__c = tariffOption.One_Time_Target_Fee__c;
		newtariffOption.Per_SIM__c = tariffOption.Per_SIM__c;
		newtariffOption.Quantity__c = tariffOption.Quantity__c;
		newtariffOption.Recurring_Base_Fee__c = tariffOption.Recurring_Base_Fee__c;
		newtariffOption.Recurring_Price_Billing_Description__c = tariffOption.Recurring_Price_Billing_Description__c;
		newtariffOption.Recurring_Target_Fee__c = tariffOption.Recurring_Target_Fee__c;
		newtariffOption.Sequence_No__c = tariffOption.Sequence_No__c;
		newtariffOption.Service_Product_Bundle__c = tariffOption.Service_Product_Bundle__c;
		newtariffOption.Show_on_Quote__c = tariffOption.Show_on_Quote__c;
		newtariffOption.Type__c = tariffOption.Type__c;
		newtariffOption.Usage_Unit__c = tariffOption.Usage_Unit__c;
		newTariffOption.Tariff__c = newTariffId;
		return newtariffOption;
	}

	private List<Tariff_Option__c> getTariffOptionsAllDataById(String idTariff){

		String tariffOptionsSOQL = 'SELECT '
			+ SObjectHelper.getFieldListForSOQL('Tariff_Option__c', null, null)
			+ ', ( SELECT '
			+ SObjectHelper.getFieldListForSOQL('Rate_Card__c', null, null)
			+ ' FROM Rate_Cards__r)'
			+ ', ( SELECT '
			+ SobjectHelper.getFieldListForSOQL('Service_Fee__c', null, null)
			+ ' FROM Service_Fee__r)'
			+ ' FROM Tariff_Option__c WHERE Tariff__c = \'' + idTariff +'\'';

		List<Tariff_Option__c> tariffOptions = Database.query(tariffOptionsSOQL);

		return tariffOptions;
	}

	/*********************************************************************************
		RATE CARDS
	**********************************************************************************/
	private Tariff_Option__c getRateCard(List<Tariff_Option__c> originalTariffOptionList, String tariffOptionName){
		Tariff_Option__c rateCardTariffOption = null;
		for (Tariff_Option__c to : originalTariffOptionList){
			if (to.Name == tariffOptionName){
				rateCardTariffOption = to;
			}
		}
		return rateCardTariffOption;
	}

	private List<Rate_Card__c> createRateCardsFromRateCard(List<Rate_Card__c> originalTariffOptionRateCardList, String idTariffOption){
		
		List<Rate_Card__c> rcList = new List<Rate_Card__c>();

		for (Rate_Card__c rc : originalTariffOptionRateCardList){
			Rate_Card__c newRateCard = new Rate_Card__c();
			newRateCard.Name = rc.Name;
			newRateCard.Base_Usage_Fee__c = rc.Base_Usage_Fee__c;
			newRateCard.Can_Discount__c = rc.Can_Discount__c;
			newRateCard.Clock_Rate__c = rc.Clock_Rate__c;
			newRateCard.Description_DE__c = rc.Description_DE__c;
			newRateCard.Description_ENG__c = rc.Description_ENG__c;
			newRateCard.Direction__c = rc.Direction__c;
			newRateCard.Edited_Fee__c = rc.Edited_Fee__c;
			newRateCard.Expected_Usage__c = rc.Expected_Usage__c;
			newRateCard.isEdited__c = rc.isEdited__c;
			newRateCard.Sequence_No__c = rc.Sequence_No__c;
			newRateCard.Show_on_Quote__c = rc.Show_on_Quote__c;
			newRateCard.Target_Usage_Fee__c = rc.Target_Usage_Fee__c;
			newRateCard.Tariff_Option__c = idTariffOption;
			newRateCard.Usage_Fee_Billing_Description__c = rc.Usage_Fee_Billing_Description__c;
			newRateCard.Worldzone__c = rc.Worldzone__c;
			rcList.add(newRateCard);
		}

		return rcList;
	}
}