@isTest
private class BatchActivateIoTActReadySimCardsTest {
	
	@isTest static void testSingleActivation() {
        
        Frame_Contract__c fc = new Frame_Contract__c();
        insert fc;
        fc = [SELECT Id, Name from Frame_Contract__c limit 1];

        Order_Request__c m2mor = new Order_Request__c(
        	Frame_Contract__c = fc.Id
        );
        insert m2mor;
        m2mor = [SELECT Id, Name from Order_Request__c limit 1];

        Third_Upload_File__c tuf = new Third_Upload_File__c(
        	Order_Request__c = m2mor.Id
        );
        insert tuf;
        tuf = [SELECT Id, Name from Third_Upload_File__c limit 1];

        Account acc = new Account(
            Name = 'Test account',
            TEF_DE_Sales_Channel__c = 'Test channel',
            Type = 'Customer'
        );
        insert acc;

        Date stockChangeDate = System.today();
        stockChangeDate = stockChangeDate.addDays(-65);
        List<Stock__c> simList = new List<Stock__c>();

        Stock__c sim1 = new Stock__c(
            Name = '12345678901234567891',
            ICCID__c = '12345678901234567891',
            Account__c = acc.Id,
            SM2M_Status__c = 'ACTIVATION_READY',
            Third_Upload_File__c = tuf.Id,
            State_Change_Date__c = stockChangeDate
        );
        simList.add(sim1);
        Stock__c sim2 = new Stock__c(
            Name = '12345678901234567892',
            ICCID__c = '12345678901234567892',
            Account__c = acc.Id,
            SM2M_Status__c = 'ACTIVATION_READY',
            Third_Upload_File__c = tuf.Id,
            State_Change_Date__c = stockChangeDate
        );
        simList.add(sim2);
        Stock__c sim3 = new Stock__c(
            Name = '12345678901234567893',
            ICCID__c = '12345678901234567893',
            Account__c = acc.Id,
            SM2M_Status__c = 'ACTIVATION_READY',
            Third_Upload_File__c = tuf.Id,
            State_Change_Date__c = stockChangeDate
        );
        simList.add(sim3);

        insert simList;
        Date dayToCheck = System.today();
        dayToCheck = dayToCheck.addDays(-60);

        Test.startTest();
        Database.executeBatch(new BatchActivateIoTActivationReadySimCards(dayToCheck, fc.Name));
        Test.stopTest();

        List<Stock__c> listAfterStatusChange = [SELECT id, name, status__c 
        										from stock__c 
        										where BIC_Framework_Contract_No__c = :fc.Name
													  AND Status__c = 'Pending activation'];

        System.assertEquals(3, listAfterStatusChange.size());
        System.assert(listAfterStatusChange[0].status__c == 'Pending activation');
    }
	
}