/**
 * Backs the Connectivity Tariff Options page. IConnectivityTariffOptions implementor class
 * is the most important part of the code, serving as a backing object for changes made on the page.
 */
public without sharing class ConnectivityTariffOptionsController
{
    //parameters received
    private Id tariffId {get;set;}
    private Id configurationId {get;set;}
    public Id accountId {get;set;}
    public Boolean editMode {get;set;}
    public Boolean isError {get; set;}
    public Boolean isSubmitted {get; set;}
    public Boolean pricesValid {get; set;}
    public String contractTerm {get; set;}
    private IConnectivityTariffOptions cto;
    public String selectedCountryId { get; set; }
    public String selectedCountryName { get; set; }
    public Decimal ratio { get; set; }
    public Integer rowCount = 1;
    public Integer rowToRemove { get; set; }
    public Id rateCardId { get; set; }
    public Id rateCardCountryManagementId { get; set; }
    private List<SelectOption> countries;
    private List<EU_Countries__c> countriesCustSett = [SELECT Id, Name, Code__c FROM EU_Countries__c ORDER BY Name ASC];

    /**
     * Serves as a backing field for embedding into JavaScript code.
     */
    public String ctoResults {get; set;}

    public List<ITariffOption> getIncludedTariffOptions() {
        return cto.getIncludedTariffOptions();
    }

    public List<ITariffOption> getAdditionalTariffOptions() {
        return cto.getAdditionalTariffOptions();
    }

    public List<ITariffOption> getSupplementaryServiceTariffOptions() {
        return cto.getSupplementaryServiceTariffOptions();
    }

    public ConnectivityTariffOptionsController(ApexPages.StandardController controller) {
        this();
    }

    public ConnectivityTariffOptionsController() {
        ctoResults = 'test';
        isError = false;

        tariffId = checkId(ApexPages.currentPage().getParameters().get('tariffId'));
        accountId = checkId(ApexPages.currentPage().getParameters().get('accountId'));
        configurationId = checkId(ApexPages.currentPage().getParameters().get('configurationId'));

        //used to render edit view page block and read only view page block
        //if coming from an offer account id will be null and in that case show readOnly mode of the page
        if(accountId == null)
        {
            editMode = false;
        }
        else
        {
            editMode = true;
        }

        try
        {
            cto = Factory.createConnectivityTariffOptions(tariffId, accountId, configurationId);
        }
        catch (Exception e)
        {
            isError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }

    /**
     * Upon submitting changes, the IConnectivityTariffOptions implementor clones the tariff
     * and calculates values that are used for Configurator. The values are shared with
     * JavaScript code through a String ctoResults backing field which should be rerendered
     * after completing this action to contain the calculated values.
     */
    public PageReference submitAction()
    {
        PageReference pageRef = validateValues();
        if (!pricesValid)
            return pageRef;

        //System.debug('ConnectivityTariffOptionsController.submitAction');
        //System.debug('Contract term: ' + contractTerm);
        Id clonedTariffId = cto.cloneTariff(contractTerm);
        String uniqueKey = cto.createConfigurations();
        String commercialApprovalRequired = cto.getCommercialApprovalRequired() ? 'Yes' : 'No';
        Decimal totalExpectedDataOverUsage = cto.getTotalExpectedDataOverUsage();
        Decimal totalExpectedTextOverUsage = cto.getTotalExpectedTextOverUsage();
        Decimal totalExpectedVoiceOverUsage = cto.getTotalExpectedVoiceOverUsage();

        Map<String, Object> results = new Map<String, Object>
        {
            'Tariff' => clonedTariffId,
            'Unique Key' => uniqueKey,
            'Requires Commercial Approval VF' => commercialApprovalRequired
        };

        if (totalExpectedDataOverUsage != null)
        {
            results.put('Total Expected Data Over-Usage', totalExpectedDataOverUsage);
        }

        if (totalExpectedTextOverUsage != null)
        {
            results.put('Total Expected Text Over-Usage', totalExpectedTextOverUsage);
        }

        if (totalExpectedVoiceOverUsage != null)
        {
            results.put('Total Expected Voice Over-Usage', totalExpectedVoiceOverUsage);
        }

        ctoResults = JSON.serialize(results);
        editMode = false;
        isSubmitted = true;
        return null;
    }


    /**
     * used for custom validation of the quantities and expected overusage values on the rate cards
    */
    public PageReference validateValues()
    {
        pricesValid = true;

        List<ITariffOption> includedOptions = getIncludedTariffOptions();
        for(ITariffOption includedOption : includedOptions)
        {
            List<IRateCard> rateCards = includedOption.getRateCards();
            for(IRateCard rateCard : rateCards)
            {
                if( rateCard.getBaseUsageFee() != rateCard.getTargetUsageFee() && rateCard.getExpectedUsage() == 0)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Expected Target Usage for Included Tariff Rate Card "' + includedOption.getName() + ' / ' + rateCard.getName() + '" must be greater than 0!'));
                    pricesValid = false;
                }
            }
        }


        List<ITariffOption> additionalOptions = getAdditionalTariffOptions();
        for(ITariffOption additionalOption : additionalOptions)
        {
            List<IRateCard> rateCards = additionalOption.getRateCards();
            for(IRateCard rateCard : rateCards)
            {
                if( rateCard.getBaseUsageFee() != rateCard.getTargetUsageFee() && rateCard.getExpectedUsage() == 0)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Expected Target Usage for Additional Tariff Rate Card "' + additionalOption.getName() + ' / ' + rateCard.getName() + '" must be greater than 0!'));
                    pricesValid = false;
                }
            }

            if(additionalOption.getIsAnyTargetFeeDiscounted() && additionalOption.getQuantity() == 0)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Quantity for Additional Tariff "' + additionalOption.getName() + '" must be greater than 0!'));
                pricesValid = false;
            }
        }

        List<ITariffOption> supplementaryOptions = getSupplementaryServiceTariffOptions();
        for(ITariffOption supplementaryOption : supplementaryOptions)
        {
            if(supplementaryOption.getIsAnyTargetFeeDiscounted() && supplementaryOption.getQuantity() == 0)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Quantity for Supplementary Service Tariff"' + supplementaryOption.getName() + '" must be greater than 0!'));
                pricesValid = false;
            }
        }

        return null;
    }

    public PageReference editAction()
    {
        editMode = true;
        return null;
    }

    /*public PageReference save() {
        
        return null;
    }*/

    /**
     * This method ckecks if id is not blank i blank returns null
     */
    private Id checkId(String idForCheck)
    {
        if (String.isBlank(idForCheck)
                || idForCheck.equalsIgnoreCase('null'))
        {
            return null;
        }
        return idForCheck;
    }

    /**
     * Don't know what is the usage of this method
     */
    public PageReference RateCardTargetPriceChanged() {
        return null;
    }
    
    private String getCountryNameByCode(string code) {
        String cName = '';
        
        if(countries != null && countries.size() > 0) {
            for (SelectOption c : countries) {
                if (c.getValue() == code) {
                    cName = c.getLabel();
                    break;
                }
            }
        }
        
        return cName;
    }
    
    public void addNewRccm() {
        Rate_Card__c selectedRateCard = [SELECT Id, Name, Worldzone__c FROM Rate_Card__c WHERE Id = :rateCardId];
        
        Rate_Card_Country_Management__c rccm = new Rate_Card_Country_Management__c();
        rccm.Rate_Card__c = rateCardId;
        
        if(selectedRateCard != null) {
            if(selectedRateCard.Worldzone__c.contains('Europe')) {
                if(countriesCustSett != null && countriesCustSett.size() > 0) {
                    rccm.Country_Code__c = countriesCustSett[0].Code__c;
                    rccm.Country_Name__c = getCountryNameByCode(countriesCustSett[0].Code__c);
                    rccm.Ratio__c = 0;
                }
            }
            else if(selectedRateCard.Worldzone__c.contains('World')) {
                Schema.DescribeFieldResult fieldResult = User.Countrycode.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
                if(ple != null && ple.size() > 0) {
                    rccm.Country_Code__c = ple[0].getValue();
                    //rccm.Country_Name__c = getCountryNameByCode(countriesCustSett[0].Code__c);
                    rccm.Ratio__c = 0;
                }
            }
        }
        
        saveRow(rccm);
    }
    
    public void deleteRccm() {
        Rate_Card_Country_Management__c selectedRccm = [SELECT Id FROM Rate_Card_Country_Management__c WHERE Id = :rateCardCountryManagementId];
        
        deleteRow(selectedRccm);
    }
    
    public void updateRccm() {
        Decimal totalRatio = 0;
        List<Rate_Card_Country_Management__c> rccmList = [SELECT Id, Rate_Card__c, Ratio__c FROM Rate_Card_Country_Management__c WHERE Rate_Card__c = :rateCardId AND Id <> :rateCardCountryManagementId];
        
        if(rccmList != null && rccmList.size() > 0) {
            for(Rate_Card_Country_Management__c row : rccmList) {
                totalRatio += row.Ratio__c;
            }
        }
        
        Rate_Card_Country_Management__c rccm = null;
        
        rccm = new Rate_Card_Country_Management__c();
        rccm.Id = rateCardCountryManagementId;
        rccm.Country_Code__c = selectedCountryId;
        rccm.Country_name__c = selectedCountryName;
        rccm.Ratio__c = ratio;
        
        totalRatio += rccm.Ratio__c;
        
        if(totalRatio > 100) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Total ratio is higher than 100!');
            ApexPages.addMessage(myMsg);
        } else {
            update rccm;
        }
    }
   
    private void saveRow(Rate_Card_Country_Management__c rccm) {
        insert rccm;
    }
    
    private void deleteRow(Rate_Card_Country_Management__c rccm) {
        delete rccm;
    }
}