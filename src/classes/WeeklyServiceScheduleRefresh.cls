global class WeeklyServiceScheduleRefresh implements Schedulable {
    global void execute(SchedulableContext sc) {
		DailyServiceRefreshReports rr = new DailyServiceRefreshReports('Weekly');
		Database.executeBatch(rr);

    }
}