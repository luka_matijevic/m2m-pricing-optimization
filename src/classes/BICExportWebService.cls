/**
	* BICExportWebService - Used just for testing of BIC export jobs expose webs ervice methods to be invoked from butons
	* @author: Zoran Zunko
	* @version: 1.0
*/

global class BICExportWebService {
	WebService static String sendToHeroku(Id bicFileId) {
    	try {
    	/*	List<Attachment> attach = [SELECT Id, ParentId, Name, Description, CreatedDate, ContentType, Body, IsDeleted
    								FROM Attachment
    								WHERE isDeleted = false
    								AND ParentId = :bicFileId
    								ORDER BY CreatedDate DESC
    								LIMIT 1];
    		if(attach == null || attach.size() == 0){
    			return 'No attachment found related to this BIC File record. Please generate the CSV first!';
    		}
    		
    	
			csam_t1.ObjectGraphCalloutHandler.AdditionalObjects addObjects = new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects();
			addObjects.typeName = 'Attachment';
			addObjects.ids = new Id[]{attach.iterator().next().id};
		*/
		//	String response = csam_t1.ObjectGraphCalloutHandler.createAndSendExtended('Upload BIC File', bicFileId, new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects[]{addObjects});
    		String response = csam_t1.ObjectGraphCalloutHandler.createAndSend('Upload BIC File', bicFileId);
    	
    		
//			String response = csam_t1.ObjectGraphCalloutHandler.createAndSend('Upload BIC File1', bicFileId, attach.get(0).id);
//			BIC_Export_Job_Data__c bicJobData = [SELECT Id, Upload_to_BIC_Status__c FROM BIC_Export_Job_Data__c WHERE Id =:bicFileId];
//			bicJobData.Upload_to_BIC_Status__c = 'Pending';
//			update bicJobData;
    		return response;
    	} catch(Exception e){
    		return e.getMessage();
    	}
    }

}