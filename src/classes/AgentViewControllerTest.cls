@isTest
private class AgentViewControllerTest {

    static testMethod void agentViewTest() {
    	Contact con = TestHelper_CaseLevelUpDown.CreateContact();
    	Case c1 = TestHelper_CaseLevelUpDown.CreateCase(con);
    	c1.priority = 'High';
    	Case c2 = TestHelper_CaseLevelUpDown.CreateCase(con);  
    	c2.status = 'In progress';  	
    	Case c3 = TestHelper_CaseLevelUpDown.CreateCase(con);
    	c3.priority = 'Low';
    	update c1;
    	update c2;
    	update c3;
    	Test.startTest();
        AgentViewController avc = new AgentViewController(new ApexPages.StandardController(c1));
        avc.viewCaseDetails();
        avc.selectedPriority = 'Priority 1';
        avc.updateAllCaseView();
        avc.selectedPriority = 'Priority 2';
        avc.updateAllCaseView();
        avc.selectedPriority = 'Priority 3';
        avc.updateAllCaseView();
        avc.selectedPriority = 'Unassigned';
        avc.updateAllCaseView();
        avc.selectedPriority = '';
        avc.updateAllCaseView();
        avc.caseId = c1.id;
        avc.viewCaseDetails();
        Test.stopTest();
        
    }
}