public class BillingHelper {
	
	// We go at most a month back from today to try and find the billing date 
	public static Date billingDate(Integer billingDay, Date fromDate) {
		Date candidate = fromDate;
		for (Integer i = 0; i <= 31; i++) {
			if (candidate.addDays(-i).day() == billingDay) {
				return candidate.addDays(-i);
			}
		}
		return null;
	}
}