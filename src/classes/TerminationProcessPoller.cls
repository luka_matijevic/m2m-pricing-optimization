global class TerminationProcessPoller implements Schedulable {
	public static void startPolling() {
		TerminationProcessPoller.scheduleTick(System.now().addSeconds(10));
	}
	
	global void execute(SchedulableContext sc) {
		
		Datetime now = Datetime.now();

        //Call Heroku to update the status and on succesful responce set the Termination Status to Terminated
        SIM_Termination_Job__c jobData = new SIM_Termination_Job__c(
			Name = 'SIM Termination Scheduled JOB @' + now.formatLong(),
			Status__c = 'POLLER'
		);
		
		insert jobData;
        System.enqueueJob(new AsyncSIMTerminationSendMessage(jobData));

		Datetime nextTick = System.now().addMinutes(5);
		TerminationProcessPoller.scheduleTick(nextTick);
		System.abortJob(sc.getTriggerId());
	}
	
	private static void scheduleTick(Datetime dt) {
		String chron_exp = dt.format('ss mm HH dd MM ? yyyy');
		TerminationProcessPoller poller = new TerminationProcessPoller();
		// Schedule the next job, and give it the system time so name is unique
		System.schedule(jobName(dt), chron_exp, poller);
	}
	
	private static String jobName(Datetime dt) {
		return 'SimTerminationProcessPoller' + dt.getTime();
	}
}