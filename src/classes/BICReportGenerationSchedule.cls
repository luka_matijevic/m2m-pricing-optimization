global class BICReportGenerationSchedule implements Schedulable {
	
	// Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
	public static final String CRON_EXP = '0 0 11 * * ? *';

	global static String scheduleIt() {
		return System.schedule('Daily BIC Report Generation', CRON_EXP, new BICReportGenerationSchedule());
	}

	global void execute(SchedulableContext sc) {
		
		Datetime now = Datetime.now();

		BIC_Export_Job_Data__c jobData = new BIC_Export_Job_Data__c(
			Name = 'SIM Export Job @' + now.formatLong(),
			CSV_Generation_Start_Time__c = now,
			Status__c = Constants.CSV_GENERATION_STARTED
		);
		
		insert jobData;

		csam_t1.ObjectGraphCalloutHandler.createAndSend('Generate BIC Report', jobData.Id);
	}
}