public class ProvisionAccountAfterMigration {
    static List<Account> acctList = null;
    static List<Billing_Account__c> billAcctList = null;
    
    public static void provisionAccounts() {
        acctList = [SELECT Id, Eplus_Customer__c FROM Account WHERE Eplus_Customer__c = true
                   //and SM2M_provisioning_status__c != 'Completed' and Local__c = true
                    and id = '001b000002bEHFw'
                   ];
        //billAcctList = [SELECT Id, Payer__c FROM Billing_Account__c WHERE Payer__c in :acctList];
        system.debug('acctList.size()' +  acctList.size());
         csam_t1.ObjectGraphCalloutHandler.createAndSend('Provision Account Message', acctList[0].Id);
       /*
        for(Account acct : acctList) {
            system.debug('acct: ' + acct);
            //if(ValidationHelper.validateProvisionAccountFromTrigger(acct.Id).equalsIgnoreCase('VALID')) {
            try {    
            csam_t1.ObjectGraphCalloutHandler.createAndSend('Provision Account Message', acct.Id);
             }
            catch (Exception e)
            {system.debug('Error' + e.getMessage());}

            //}
        }
        */
    }
    
   
    
    public static void provisionBillingAccounts(List<Billing_Account__c> billingAccounts) {
        Integer sm2mId = 0;
        //SM2M_ID__c
        for(Billing_Account__c bacct : billingAccounts) {
            sm2mId = bacct.SM2M_ID__c != null ? Integer.valueOf(bacct.SM2M_ID__c) : 0;
            if(sm2mid == 0) {
                csam_t1.ObjectGraphCalloutHandler.createAndSend('Create Billing Account', String.valueOf(bacct.Id));
            } else {
                csam_t1.ObjectGraphCalloutHandler.createAndSend('Update Billing Account', String.valueOf(bacct.Id));
            }
        }
    }
}