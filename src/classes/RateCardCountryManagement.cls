public without sharing class RateCardCountryManagement implements IRateCardCountryManagement {
    
    Rate_Card_Country_Management__c rcc;

    public RateCardCountryManagement(Rate_Card_Country_Management__c rcc)
    {
        this.rcc = rcc;
    }

    public Rate_Card_Country_Management__c getRateCardCountryManagement()
    {
        return rcc;
    }
    
    public String getCountryCode()
    {
    	return rcc.Country_code__c;
    }

    public void setCountryCode(String countryCode) {
    	rcc.Country_code__c = countryCode;
    }
    
    public String getCountryName()
    {
    	return rcc.Country_name__c;
    }

    public void setCountryName(String countryName) {
    	rcc.Country_name__c = countryName;
    }

    public Decimal getRatio()
    {
    	return rcc.Ratio__c;
    }

    public void setRatio(Decimal ratio)
    {
        rcc.Ratio__c = ratio;
    }
    
    public Id getRateCardCountryManagementId()
    {
        return rcc.Rate_Card_Country_Management_Id__c;
    }
}