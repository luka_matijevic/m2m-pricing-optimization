/*
	Petar Matkovic
	21.9.2016.
*/
public class ScheduleIoTMobilityServiceGeneration implements Schedulable {
	public void execute(SchedulableContext sc) {
		//MyBatchClass b = new MyBatchClass();
		//database.executebatch(b);
		Database.executebatch(new BatchCreateMobilitySubscriptions(), 10);
	}
}