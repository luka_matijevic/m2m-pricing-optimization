global class DailyServiceRefreshReports implements Database.Batchable<sObject> {
	
	private String query;
	private String period;
	
	global DailyServiceRefreshReports(String period) {
		this.period = period;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		List<String> reportIds = new List<String>();

		if(period == 'Daily'){
			List<DailyServiceReportIds__c> reportsDaily = DailyServiceReportIds__c.getall().values();
			for(DailyServiceReportIds__c obj : reportsDaily){

				reportIds.add(obj.Report_ID__c);
			}		
		}

		else if (period == 'Weekly'){
			List<WeeklyServiceReportIds__c> reportsWeekly = WeeklyServiceReportIds__c.getall().values();
			for(WeeklyServiceReportIds__c obj : reportsWeekly){

				reportIds.add(obj.Report_ID__c);
			}
		}

		else if (period == 'Monthly'){
			List<MonthlyServiceReportIds__c> reportsMonthly = MonthlyServiceReportIds__c.getall().values();
			for(MonthlyServiceReportIds__c obj : reportsMonthly){

				reportIds.add(obj.Report_ID__c);
			}
		}
		query = 'SELECT Id FROM Report WHERE  Id = :reportIds';
		return Database.getQueryLocator(query);

	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {


		// Run the report
		for(sObject obj : scope){
			Reports.ReportResults results = Reports.ReportManager.runReport(obj.Id, true);
			System.debug('Synchronous results: ' + results);
		}
	}
	
	global void finish( Database.BatchableContext BC) {
		
		if(!Test.isRunningTest()){
			DailyServiceSendReports ss = new DailyServiceSendReports(period);
			Database.executeBatch(ss);

		}
	}
	
}