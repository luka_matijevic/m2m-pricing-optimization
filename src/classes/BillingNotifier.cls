public class BillingNotifier {
	
	static Billing_Settings__c billingSettings;
	
	static {
		billingSettings = Billing_Settings__c.getOrgDefaults();
	}
	
	/**
	 * To address is not required. If not available, it will be overriden here.
	 */ 
	public static Messaging.SendEmailResult sendEmail(Messaging.SingleEmailMessage email) {
		if (email.getToAddresses() == null || email.getToAddresses().isEmpty()) {
			email.setToAddresses(new String[] {billingSettings.Status_reporting_email__c });
		}
		Messaging.SendEmailResult[] result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
		if (result.size() > 0) {
			return result[0];
		}
		return null;
	}
}