@isTest
private class TestM2MYearlyTotalCalculator {

    static testMethod void testEmptyUnitScheduleValues() {

    	Account account = new Account(
    		Name = 'Test',
        	Type = 'Business'
        );

        insert account;



    	Opportunity opp = new Opportunity(
    		Name = 'Test',
    		AccountId = account.Id,
    		CloseDate = Date.newInstance(2013, 1, 1),
    		StageName = 'Closed Won',
    		Win_Reason__c = 'Price',
    		Contract_Term_Per_SIM__c = 24,
    		Contract_Term_Per_Subscription__c = 24,
            Description = 'Test description 20 characters'
    	);

    	insert opp;

    	cscfga__Product_Bundle__c bundle = new cscfga__Product_Bundle__c(
    		Name = 'Test',
    		cscfga__Synchronised_with_Opportunity__c = true,
    		cscfga__Opportunity__c = opp.Id
    	);

    	insert bundle;

    	Unit_Schedule__c schedule = new Unit_Schedule__c(
    		Opportunity__c = opp.Id,
    		Year__c = '2013'
    	);

    	insert schedule;

        Double connectivityRevenue = M2MYearlyTotalCalculator.calculateConnectivityRevenue(opp.Id, 2013);
        System.assertEquals(0.0, connectivityRevenue);

        Double servicesRevenue = M2MYearlyTotalCalculator.calculateServiceRevenue(opp.Id, 2013);
        System.assertEquals(0.0, servicesRevenue);

    }

    static testMethod void testServiceRevenue() {

    	Account account = new Account(
    		Name = 'Test',
        	Type = 'Business'
        );

        insert account;

    	Double monthlyRevenue = 2.0;

    	List<Opportunity> opportunities = new List<Opportunity>();

    	opportunities.add(new Opportunity(
    		Name = 'Test-1',
    		AccountId = account.Id,
    		CloseDate = Date.newInstance(2013, 1, 1),
    		StageName = 'Closed Won',
    		Win_Reason__c = 'Price',
    		Contract_Term_Per_Subscription__c = 12,
    		Monthly_Revenue_Per_Service__c = monthlyRevenue,
            Description = 'Test Description 20 characters'
    	));

    	opportunities.add(new Opportunity(
    		Name = 'Test-2',
    		AccountId = account.Id,
    		CloseDate = Date.newInstance(2013, 1, 1),
    		StageName = 'Closed Won',
    		Win_Reason__c = 'Price',
    		Contract_Term_Per_Subscription__c = 12,
    		Monthly_Revenue_Per_Service__c = monthlyRevenue,
            Description = 'Test description 20 characters'
    	));

    	insert opportunities;

    	List<cscfga__Product_Bundle__c> bundles = new List<cscfga__Product_Bundle__c>();

    	bundles.add(new cscfga__Product_Bundle__c(
    		Name = 'Test 1',
    		cscfga__Synchronised_with_Opportunity__c = true,
    		cscfga__Opportunity__c = opportunities.get(0).Id
    	));

    	bundles.add(new cscfga__Product_Bundle__c(
    		Name = 'Test 2',
    		cscfga__Synchronised_with_Opportunity__c = true,
    		cscfga__Opportunity__c = opportunities.get(1).Id
    	));

    	insert bundles;

    	List<Unit_Schedule__c> schedules = new List<Unit_Schedule__c>();


    	Integer monthlyAmount = 10;

    	schedules.add(new Unit_Schedule__c(
    		Opportunity__c = opportunities[0].Id,
    		Year__c = '2013',
    		VAS_Units_January__c 	= monthlyAmount,
			VAS_Units_February__c 	= monthlyAmount,
			VAS_Units_March__c 		= monthlyAmount,
			VAS_Units_April__c		= monthlyAmount,
			VAS_Units_May__c 		= monthlyAmount,
			VAS_Units_June__c 		= monthlyAmount,
			VAS_Units_July__c 		= monthlyAmount,
			VAS_Units_August__c 	= monthlyAmount,
			VAS_Units_September__c 	= monthlyAmount,
			VAS_Units_October__c 	= monthlyAmount,
			VAS_Units_November__c 	= monthlyAmount,
			VAS_Units_December__c 	= monthlyAmount
    	));

    	schedules.add(new Unit_Schedule__c(
    		Opportunity__c = opportunities[0].Id,
    		Year__c = '2012',
    		VAS_Units_November__c = monthlyAmount
    	));

    	schedules.add(new Unit_Schedule__c(
    		Opportunity__c = opportunities[1].Id,
    		Year__c = '2013',
    		VAS_Units_January__c 	= monthlyAmount,
			VAS_Units_February__c 	= monthlyAmount,
			VAS_Units_March__c 		= monthlyAmount,
			VAS_Units_April__c		= monthlyAmount,
			VAS_Units_May__c 		= monthlyAmount,
			VAS_Units_June__c 		= monthlyAmount,
			VAS_Units_July__c 		= monthlyAmount,
			VAS_Units_August__c 	= monthlyAmount,
			VAS_Units_September__c 	= monthlyAmount,
			VAS_Units_October__c 	= monthlyAmount,
			VAS_Units_November__c 	= monthlyAmount,
			VAS_Units_December__c 	= monthlyAmount
    	));

    	schedules.add(new Unit_Schedule__c(
    		Opportunity__c = opportunities[1].Id,
    		Year__c = '2012',
    		VAS_Units_November__c = monthlyAmount
    	));

    	insert schedules;

    	double expectedRevenue = 0.0;
    	// adding values for 2013 Unit_Schedule__c
    	for (integer i = 0; i < 12; i++) {
    		expectedRevenue += (12 - i) * monthlyAmount * monthlyRevenue;
    	}

    	// add value for 2012 Unit_Schedule__c (10 months in 2013)
    	expectedRevenue += 10 * monthlyAmount * monthlyRevenue;

    	System.assertEquals(expectedRevenue, M2MYearlyTotalCalculator.calculateServiceRevenue(opportunities[0].Id, 2013));
    }

    static testMethod void testConnectivityRevenue() {

    	Account account = new Account(
    		Name = 'Test',
        	Type = 'Business'
        );

        insert account;

    	Double monthlyRevenue = 1.0;

    	List<Opportunity> opportunities = new List<Opportunity>();

    	opportunities.add(new Opportunity(
    		Name = 'Test-1',
    		AccountId = account.Id,
    		CloseDate = Date.newInstance(2013, 1, 1),
    		StageName = 'Closed Won',
    		Win_Reason__c = 'Price',
    		Contract_Term_Per_SIM__c = 12,
    		Monthly_Revenue_Per_SIM__c = monthlyRevenue,
            Description = 'Test description 20 characters'
    	));

    	opportunities.add(new Opportunity(
    		Name = 'Test-2',
    		AccountId = account.Id,
    		CloseDate = Date.newInstance(2013, 1, 1),
    		StageName = 'Closed Won',
    		Win_Reason__c = 'Price',
    		Contract_Term_Per_SIM__c = 12,
    		Monthly_Revenue_Per_SIM__c = monthlyRevenue,
            Description = 'Test description 20 characters'
    	));

    	insert opportunities;

    	List<cscfga__Product_Bundle__c> bundles = new List<cscfga__Product_Bundle__c>();

    	bundles.add(new cscfga__Product_Bundle__c(
    		Name = 'Test 1',
    		cscfga__Synchronised_with_Opportunity__c = true,
    		cscfga__Opportunity__c = opportunities.get(0).Id
    	));

    	bundles.add(new cscfga__Product_Bundle__c(
    		Name = 'Test 2',
    		cscfga__Synchronised_with_Opportunity__c = true,
    		cscfga__Opportunity__c = opportunities.get(1).Id
    	));

    	insert bundles;

    	List<Unit_Schedule__c> schedules = new List<Unit_Schedule__c>();


    	Integer monthlyAmount = 10;

    	schedules.add(new Unit_Schedule__c(
    		Opportunity__c = opportunities[0].Id,
    		Year__c = '2013',
    		Connectivity_Sims_January__c 	= monthlyAmount,
			Connectivity_Sims_February__c 	= monthlyAmount,
			Connectivity_Sims_March__c 		= monthlyAmount,
			Connectivity_Sims_April__c		= monthlyAmount,
			Connectivity_Sims_May__c 		= monthlyAmount,
			Connectivity_Sims_June__c 		= monthlyAmount,
			Connectivity_Sims_July__c 		= monthlyAmount,
			Connectivity_Sims_August__c 	= monthlyAmount,
			Connectivity_Sims_September__c 	= monthlyAmount,
			Connectivity_Sims_October__c 	= monthlyAmount,
			Connectivity_Sims_November__c 	= monthlyAmount,
			Connectivity_Sims_December__c 	= monthlyAmount
    	));

    	schedules.add(new Unit_Schedule__c(
    		Opportunity__c = opportunities[0].Id,
    		Year__c = '2012',
    		Connectivity_Sims_December__c = monthlyAmount
    	));

    	// Add schedules to other opportunities to test isolation
    	schedules.add(new Unit_Schedule__c(
    		Opportunity__c = opportunities[1].Id,
    		Year__c = '2013',
    		Connectivity_Sims_January__c 	= monthlyAmount,
			Connectivity_Sims_February__c 	= monthlyAmount,
			Connectivity_Sims_March__c 		= monthlyAmount,
			Connectivity_Sims_April__c		= monthlyAmount,
			Connectivity_Sims_May__c 		= monthlyAmount,
			Connectivity_Sims_June__c 		= monthlyAmount,
			Connectivity_Sims_July__c 		= monthlyAmount,
			Connectivity_Sims_August__c 	= monthlyAmount,
			Connectivity_Sims_September__c 	= monthlyAmount,
			Connectivity_Sims_October__c 	= monthlyAmount,
			Connectivity_Sims_November__c 	= monthlyAmount,
			Connectivity_Sims_December__c 	= monthlyAmount
    	));

    	schedules.add(new Unit_Schedule__c(
    		Opportunity__c = opportunities[1].Id,
    		Year__c = '2012',
    		Connectivity_Sims_December__c = monthlyAmount
    	));

    	insert schedules;

    	double expectedRevenue = 0.0;
    	// adding values for 2013 Unit_Schedule__c
    	for (integer i = 0; i < 12; i++) {
    		expectedRevenue += (12 - i) * monthlyAmount * monthlyRevenue;
    	}

    	// add value for 2012 Unit_Schedule__c (11 months in 2013)
    	expectedRevenue += 11 * monthlyAmount * monthlyRevenue;

    	System.assertEquals(expectedRevenue, M2MYearlyTotalCalculator.calculateConnectivityRevenue(opportunities[0].Id, 2013));
    }

    static testMethod void testNoContractTerm() {

    	Account account = new Account(
    		Name = 'Test',
        	Type = 'Business'
        );

        insert account;

    	Double monthlyRevenue = 1.0;

    	Opportunity opp = new Opportunity(
    		Name = 'Test',
    		AccountId = account.Id,
    		CloseDate = Date.newInstance(2013, 1, 1),
    		StageName = 'Closed Won',
    		Win_Reason__c = 'Price',
    		Monthly_Revenue_Per_Service__c = monthlyRevenue,
            Description = 'Test description 20 characters'
    	);

    	insert opp;

    	cscfga__Product_Bundle__c bundle = new cscfga__Product_Bundle__c(
    		Name = 'Test',
    		cscfga__Synchronised_with_Opportunity__c = true,
    		cscfga__Opportunity__c = opp.Id
    	);

    	insert bundle;

    	List<Unit_Schedule__c> schedules = new List<Unit_Schedule__c>();

    	Integer monthlyAmount = 10;

    	schedules.add(new Unit_Schedule__c(
    		Opportunity__c = opp.Id,
    		Year__c = '2013',
    		VAS_Units_January__c 	= monthlyAmount,
			VAS_Units_February__c 	= monthlyAmount,
			VAS_Units_March__c 		= monthlyAmount,
			VAS_Units_April__c		= monthlyAmount,
			VAS_Units_May__c 		= monthlyAmount,
			VAS_Units_June__c 		= monthlyAmount,
			VAS_Units_July__c 		= monthlyAmount,
			VAS_Units_August__c 	= monthlyAmount,
			VAS_Units_September__c 	= monthlyAmount,
			VAS_Units_October__c 	= monthlyAmount,
			VAS_Units_November__c 	= monthlyAmount,
			VAS_Units_December__c 	= monthlyAmount
    	));
    	insert schedules;

    	// No contract term is set - all contracts have zero length
    	double expectedRevenue = 0.0;

    	System.assertEquals(expectedRevenue, M2MYearlyTotalCalculator.calculateServiceRevenue(opp.Id, 2013));
    }
}