global class TaskForSIMsTerminationBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    
    private static final String TERMINATION_STATUS_REQUESTED = 'REQUESTED';
    private Date reqDate = System.today().addDays(3);
    private String query = 'Select Id,Name,End_date__c,Termination_Status__c,SM2M_Status__c,Account__c,Order_Request__c,Order_Request__r.Frame_Contract__c,Order_Request__r.Frame_Contract__r.Opportunity__c,Order_Request__r.Frame_Contract__r.Opportunity__r.ownerId from stock__C where End_date__c = :reqDate AND Termination_Status__c = \'' + TERMINATION_STATUS_REQUESTED + '\'';
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> sims){
        List<Stock__c> simsToUpdate = new List<Stock__c>();
        Set<Id> accountIds = new Set<Id>();
        Set<Id> opptyOwners = new Set<Id>();
        Set<Id> ticketOwners = new Set<Id>();
        List<Task> tasksTobeCreated = new List<Task>();
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        for(Stock__c stock : (List<Stock__c>)sims){
            if(stock.Account__c != null){
                accountIds.add(stock.Account__c);
            }
            if(stock.Order_Request__c != null && stock.Order_Request__r.Frame_Contract__c != null && stock.Order_Request__r.Frame_Contract__r.Opportunity__c != null && stock.Order_Request__r.Frame_Contract__r.Opportunity__r.ownerId != null){
                opptyOwners.add(stock.Order_Request__r.Frame_Contract__r.Opportunity__r.ownerId);
            }
        }
        //Create Task for the Opporunity Owner
        if(!opptyOwners.isEmpty()){
            for(Id oppOwner : opptyOwners){
                Task t = new Task(Description = 'Termination of SIM requested',
                                    Priority = 'Normal',
                                    Status = 'Not Started',
                                    Subject = 'Other',
                                    IsReminderSet = true,
                                    ReminderDateTime = System.now() + 1,
                                    OwnerId = oppOwner);
                tasksTobeCreated.add(t);
            }
        }
        if(!tasksTobeCreated.isEmpty()){
            insert tasksTobeCreated;
        }
        if(!accountIds.isEmpty()){
            for(Case c : [Select id, CaseNumber, ownerId from Case where AccountId IN :accountIds]){
                ticketOwners.add(c.ownerId);
            }
        }
        if(!ticketOwners.isEmpty()){
            for(User u : [select FirstName, LastName, Email from User where Id IN :ticketOwners]){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.toAddresses = new String[] { u.Email };
                mail.setSubject('SIM Termination');
                String body = 'Dear ' + u.FirstName + ' ' + u.LastName + ', ';
                body += '\n Your request for terminating the SIMs will be successfully processed.';
                mail.setHtmlBody(body);
                mails.add(mail);
            }
        }
        if(!mails.isEmpty()){
            Messaging.sendEmail(mails);
        }
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
}