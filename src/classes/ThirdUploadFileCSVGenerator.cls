public without sharing class ThirdUploadFileCSVGenerator {
	/*Luka 30.11.2016 changed thirdUploadFileLineFormat*/
    //private static final string thirdUploadFileLineFormat = '{0},{1},1,{2},{3},{4},{5},{6}\n';//{ICCID},{CustomerOrderNumber-OrderId},SIMType - 1 always global,{SIM model - Stock.sim model}, {APN list with 10 apns ,,,,,,},{Static APN index},{Staic IP adress}, {AccountId}
	private static final string thirdUploadFileLineFormat = '{0},{1},1,{2},{3},{4},{5},{6}\n';//{ICCID},{CustomerOrderNumber-OrderId},SIMType - 1 always global,{SIM model - Stock.sim model}, {APN list with 10 apns ,,,,,,},{Static APN index},{Staic IP adress}, {AccountId},{Subscription Group},{LifeCycle Status}
	private static final string thirdUploadFileLineFormatNew = '{0},{1},1,{2},{3},{4},{5},{6},{7},{8}\n';//{ICCID},{CustomerOrderNumber-OrderId},SIMType - 1 always global,{SIM model - Stock.sim model}, {APN list with 10 apns ,,,,,,},{Static APN index},{Staic IP adress}, {AccountId},{Subscription Group},{LifeCycle Status}
    private static final string thirdUploadFileSimSwapLineFormat = '{0},{1},1,{2},{3},{4},{5},{6},{7},{8}\n';//{ICCID},{CustomerOrderNumber-OrderId},SIMType - 1 always global,{SIM model - Stock.sim model}, {APN list with 10 apns ,,,,,,},{Static APN index},{Staic IP adress}, {AccountId}
    //private static final string thirdUploadFileLineFormat = '{0},{1},1,,{2},{3},{4},{5}\n';//{ICCID},{CustomerOrderNumber-OrderId},SIMType - 1 always global,{SIM model - Stock.sim model}, {APN list with 10 apns ,,,,,,},{Static APN index},{Staic IP adress}, {AccountId}
    private static final string apnStringFormat = '{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}';
    private static List<MSISDN_Stock__c> msisdnStockToInsert = new List<MSISDN_Stock__c>();
    private static List<MSISDN__c> msisdnListForUpdate = new List<MSISDN__c>();
    private static List<Stock__c> stocksForUpdate = new List<Stock__c>();
    
    public static blob createThirdUploadFileCSV(Id thirdUploadFileId)
    {
        List<Stock__c> trialStocks = new List<Stock__c>(); 
        string staticApnAddress = '';
        string staticApnIndex = '';
        string response = '' ;
        //string apnList = String.format(apnStringFormat,  new List<string> {'','','','','','','','','',''} );
        string tufCSV = '';
        Third_Upload_File__c tufObject = [SELECT Id, Name, Account__c, Order_Request__c, Order_Request__r.Frame_Contract__r.Type__c, SIM_swap_Ticket__c FROM Third_Upload_File__c WHERE Id = :thirdUploadFileId];
		List<Activation_Centre__c> activationCentreList = [select id from Activation_Centre__c where M2M_Order_Request__c = : tufObject.Order_Request__c];

        if(tufObject != null && tufObject.SIM_swap_Ticket__c != null) {
            List<Stock__c> stocksInSwapProgress = [SELECT Id, Commercial_group__c, ICCID__c, MSISDN__c, SIM_Swap_Requested_For_Ticket__c, Status__c, End_date__c FROM Stock__c WHERE SIM_Swap_Requested_For_Ticket__c = :tufObject.SIM_swap_Ticket__c AND Status__c = 'Swap in progress'];
            Integer numberOfSwapSimCards = [SELECT Count() FROM Stock__c WHERE SIM_Swap_Requested_For_Ticket__c = :tufObject.SIM_swap_Ticket__c AND Status__c = 'Swap in progress'];
            
            List<Stock__c> fetchedStocks = [SELECT Id, Commercial_group__c, ICCID__c ,SRF__c, End_date__c, Third_Upload_File__r.Account__c, SRF__r.SM2M_SAP_PO__c, SIM_Model__c, Type__c, SIM_Range_Origin__c, SIM_Range__c, SIM_Range__r.Account__c, SIM_Range__r.Account__r.Name, MSISDN__c, SIM_Swap_Requested_For_Ticket__c, Status__c, Third_Upload_File__c FROM Stock__c WHERE Third_Upload_File__c = :thirdUploadFileId];
            Integer numberOfFetchedStocks = [SELECT Count() FROM Stock__c WHERE Third_Upload_File__c = :thirdUploadFileId];
            
            if(numberOfSwapSimCards != numberOfFetchedStocks) {
                throw new ThirdUploadFileException('Number of SIM cards ready for swap is different from number of SIM cards selected in Third Upload File!');
            }
            
            List<MSISDN_Stock__c> msisdnStockRecordsForSwapStocks = [SELECT Id, isCurrentSIM__c, MSISDN__c, MSISDN__r.MSISDN__c, Stock__c FROM MSISDN_Stock__c WHERE Stock__c = :stocksInSwapProgress];
            
            for(MSISDN_Stock__c rec : msisdnStockRecordsForSwapStocks) {
                rec.isCurrentSIM__c = false;
            }
            
            update msisdnStockRecordsForSwapStocks;
            
            for(Integer i = 0; i < numberOfSwapSimCards; i++) {
                if(fetchedStocks[i].SIM_Range_Origin__c != null && (fetchedStocks[i].SIM_Range_Origin__c.equalsIgnoreCase('GREEN') || fetchedStocks[i].SIM_Range_Origin__c.equalsIgnoreCase('BLUE'))) {
                    fetchedStocks[i].MSISDN__c = stocksInSwapProgress[i].MSISDN__c;
                }
                
                fetchedStocks[i].Commercial_group__c = stocksInSwapProgress[i].Commercial_group__c;
                stocksInSwapProgress[i].MSISDN__c = '';
                fetchedStocks[i].End_date__c = stocksInSwapProgress[i].End_date__c;
                
                stocksInSwapProgress[i].Status__c = 'Pending retirement';
                //fetchedStocks[i].Status__c = 'Pending activation';
            }
            
            update stocksInSwapProgress;
            update fetchedStocks;
            
            List<APN_TUF__c> apnTufJuncts = [SELECT APN__c FROM  APN_TUF__c where TUF__c = :thirdUploadFileId];
            
            if(apnTufJuncts.size() == 0)
                throw new ThirdUploadFileException('At least one APN needs to be set in order to generate Third Upload File');
    
            List<Id> apnIds = new List<Id>();
    
            for(APN_TUF__c item : apnTufJuncts)
                apnIds.add(item.APN__c);
    
            List<APN__c> apns = [SELECT Domain__c, Static__c, IP_Mask__c  from APN__c where id IN : apnIds];
    
            List<String> apnDomains = new List<String>();
    
            for(Integer i = 0; i < apns.size(); i++)
            {
                apnDomains.add(apns[i].Domain__c);
                if(apns[i].Static__c)
                {
                    staticApnAddress = apns[i].IP_Mask__c;
                    staticApnIndex = String.valueOf(i+1);
                }
            }
            
            for(Integer i = apnDomains.size()-1; i < 10; i++)
            {
                apnDomains.add('');         
            }
            
            string apnList = String.format(apnStringFormat, apnDomains);
            
            for(Stock__c item : fetchedStocks)
            {
                if(item.Third_Upload_File__r.Account__c == null)
                    throw new ThirdUploadFileException('Account neds to be associated with the Third Upload File Object. Please add the Account!');
        
                if(item.SRF__r.SM2M_SAP_PO__c == null)
                    throw new ThirdUploadFileException('M2M SAP purchase order must be set on the SRF: ' + item.SRF__c);
                    
                if(tufObject.Order_Request__r.Frame_Contract__r.Type__c.equalsIgnoreCase('Trial')) {
                    Stock__c stock = new Stock__c();
                    stock.Id = item.Id;
                    stock.Trial__c = true;
                    trialStocks.add(stock);
                }
                
                response += String.format(thirdUploadFileSimSwapLineFormat, new List<string> {item.ICCID__c,item.SRF__r.SM2M_SAP_PO__c,  String.isEmpty(item.Type__c) ? '': item.Type__c, apnList, staticApnIndex, staticApnAddress, item.Third_Upload_File__r.Account__c, item.Commercial_group__c, 'ACTIVE'});
            }
            
            if(trialStocks.size() > 0) {
                update trialStocks;
            }
        }
        else if //(tufObject.Name.contains('Automatic TUF for AC')){
			(activationCentreList.size()!=0){
            List<MSISDN__c> msisdnList = [SELECT Id, MSISDN_Range__c, MSISDN__c, isReadyForUse__c, Date_quarantine_start__c, Date_quarantine_end__c,  MSISDN_Range__r.Is_Reserved__c, MSISDN_Range__r.Account__c, MSISDN_Range__r.Range_start__c, MSISDN_Range__r.Range_end__c
                        FROM MSISDN__c WHERE isReadyForUse__c = true AND MSISDN_Range__r.Account__c = :tufObject.Account__c AND MSISDN_Range__r.Is_Reserved__c = true];
                        
            List<APN_TUF__c> apnTufJuncts = [SELECT APN__c FROM  APN_TUF__c where TUF__c = :thirdUploadFileId];
            if(apnTufJuncts.size() == 0)
                throw new ThirdUploadFileException('At least one APN needs to be set in order to generate Third Upload File');
            
    
            List<Id> apnIds = new List<Id>();
    
            for(APN_TUF__c item : apnTufJuncts)
                apnIds.add(item.APN__c);
    
            List<APN__c> apns = [SELECT Domain__c, Static__c, IP_Mask__c  from APN__c where id IN : apnIds];
    
            List<String> apnDomains = new List<String>();
    
            for(Integer i = 0; i < apns.size(); i++)
            {
                apnDomains.add(apns[i].Domain__c);
                if(apns[i].Static__c)
                {
                    staticApnAddress = apns[i].IP_Mask__c;
                    staticApnIndex = String.valueOf(i +1);
                }
            }
            
            for(Integer i = apnDomains.size() -1; i < 10; i++)
            {
                apnDomains.add('');         
            }
            
            
    
            string apnList = String.format(apnStringFormat, apnDomains);
        //  not working  List<APN__c> apns = [SELECT Domain__c, Static__c from APN__c where id IN : [SELECT APN__c FROM  APN_TUF__c where TUF__c = :thirdUploadFileId]];
            
            List<Stock__c> fetchedStocks = [SELECT Id, ICCID__c, msisdn__c, SRF__c ,Third_Upload_File__c, Third_Upload_File__r.Account__c,  SRF__r.SM2M_SAP_PO__c, SIM_Model__c, Type__c, SIM_Range_Origin__c, SIM_Range__c, SIM_Range__r.Account__c, SIM_Range__r.Account__r.Name, Subscription_Group__r.SM2M_Subscription_Group_ID__c, SM2M_Status__c, Subscription_Group__r.SM2M_Subscription_Group_Name__c FROM Stock__c WHERE Third_Upload_File__c = :thirdUploadFileId];
            List<MSISDN_Stock__c> msisdnStockRecordsForFetchedStocks = [SELECT Id, isCurrentSIM__c, MSISDN__c, MSISDN__r.MSISDN__c, Stock__c FROM MSISDN_Stock__c WHERE Stock__c = :fetchedStocks];
            List<MSISDN__c> msisdnListFreeOutsideTheAccount = [SELECT Id, MSISDN_Range__c, MSISDN__c, isReadyForUse__c, Date_quarantine_start__c, Date_quarantine_end__c,  MSISDN_Range__r.Is_Reserved__c, MSISDN_Range__r.Account__c, MSISDN_Range__r.Range_start__c, MSISDN_Range__r.Range_end__c
                            FROM MSISDN__c WHERE isReadyForUse__c = true AND Id NOT IN (SELECT MSISDN__c FROM MSISDN_Stock__c WHERE isCurrentSIM__c = true) AND MSISDN_Range__r.Range_Type__c='Local' AND MSISDN_Range__r.Is_Reserved__c = false LIMIT :fetchedStocks.size()];
            
            for(Stock__c item : fetchedStocks)
            {
                if(item.Third_Upload_File__r.Account__c == null)
                    throw new ThirdUploadFileException('Account neds to be associated with the Third Upload File Object. Please add the Account!');
        
                if(item.SRF__r.SM2M_SAP_PO__c == null)
                    throw new ThirdUploadFileException('M2M SAP purchase order must be set on the SRF: ' + item.SRF__c);
                 
                if(item.SIM_Range_Origin__c != null && item.msisdn__c == null && (item.SIM_Range_Origin__c.equalsIgnoreCase('GREEN') || item.SIM_Range_Origin__c.equalsIgnoreCase('BLUE'))) {
                    checkForMSISDNs(tufObject, msisdnList, msisdnStockRecordsForFetchedStocks, item, msisdnListFreeOutsideTheAccount);
                }
                
                if(tufObject.Order_Request__r.Frame_Contract__r.Type__c.equalsIgnoreCase('Trial')) {
                    Stock__c stock = new Stock__c();
                    stock.Id = item.Id;
                    stock.Trial__c = true;
                    trialStocks.add(stock);
                }
                
                response += String.format(thirdUploadFileLineFormatNew, new List<string> {item.ICCID__c,item.SRF__r.SM2M_SAP_PO__c,  String.isEmpty(item.Type__c) ? '': item.Type__c, apnList, staticApnIndex, staticApnAddress, item.Third_Upload_File__r.Account__c, item.Subscription_Group__r.SM2M_Subscription_Group_Name__c, item.SM2M_Status__c == 'INACTIVE_NEW' ? '' : item.SM2M_Status__c});
                //system.debug((new List<string> {item.ICCID__c,item.SRF__r.Order_Request__c, item.SIM_Model__c, apnStringFormat, staticApnIndex, staticApnAddress,item.SRF__r.Account__c}).size());
            }
        
            if(stocksForUpdate.size() > 0) {
                update stocksForUpdate;
            }
            
            if(msisdnListForUpdate.size() > 0) {
                update msisdnListForUpdate;
            }
            
            if(msisdnStockToInsert.size() > 0) {
                insert msisdnStockToInsert;
            }
            
            if(trialStocks.size() > 0) {
                update trialStocks;
            }    
        }
		else {
            List<MSISDN__c> msisdnList = [SELECT Id, MSISDN_Range__c, MSISDN__c, isReadyForUse__c, Date_quarantine_start__c, Date_quarantine_end__c,  MSISDN_Range__r.Is_Reserved__c, MSISDN_Range__r.Account__c, MSISDN_Range__r.Range_start__c, MSISDN_Range__r.Range_end__c
                        FROM MSISDN__c WHERE isReadyForUse__c = true AND MSISDN_Range__r.Account__c = :tufObject.Account__c AND MSISDN_Range__r.Is_Reserved__c = true];
                        
            List<APN_TUF__c> apnTufJuncts = [SELECT APN__c FROM  APN_TUF__c where TUF__c = :thirdUploadFileId];
            if(apnTufJuncts.size() == 0)
                throw new ThirdUploadFileException('At least one APN needs to be set in order to generate Third Upload File');
            
    
            List<Id> apnIds = new List<Id>();
    
            for(APN_TUF__c item : apnTufJuncts)
                apnIds.add(item.APN__c);
    
            List<APN__c> apns = [SELECT Domain__c, Static__c, IP_Mask__c  from APN__c where id IN : apnIds];
    
            List<String> apnDomains = new List<String>();
    
            for(Integer i = 0; i < apns.size(); i++)
            {
                apnDomains.add(apns[i].Domain__c);
                if(apns[i].Static__c)
                {
                    staticApnAddress = apns[i].IP_Mask__c;
                    staticApnIndex = String.valueOf(i +1);
                }
            }
            
            for(Integer i = apnDomains.size() -1; i < 10; i++)
            {
                apnDomains.add('');         
            }
            
            
    
            string apnList = String.format(apnStringFormat, apnDomains);
        //  not working  List<APN__c> apns = [SELECT Domain__c, Static__c from APN__c where id IN : [SELECT APN__c FROM  APN_TUF__c where TUF__c = :thirdUploadFileId]];
            
            List<Stock__c> fetchedStocks = [SELECT Id, ICCID__c, SRF__c ,Third_Upload_File__c, Third_Upload_File__r.Account__c,  SRF__r.SM2M_SAP_PO__c, SIM_Model__c, Type__c, SIM_Range_Origin__c, SIM_Range__c, SIM_Range__r.Account__c, SIM_Range__r.Account__r.Name, Subscription_Group__r.SM2M_Subscription_Group_ID__c, SM2M_Status__c, Subscription_Group__r.SM2M_Subscription_Group_Name__c FROM Stock__c WHERE Third_Upload_File__c = :thirdUploadFileId];
            List<MSISDN_Stock__c> msisdnStockRecordsForFetchedStocks = [SELECT Id, isCurrentSIM__c, MSISDN__c, MSISDN__r.MSISDN__c, Stock__c FROM MSISDN_Stock__c WHERE Stock__c = :fetchedStocks];
            List<MSISDN__c> msisdnListFreeOutsideTheAccount = [SELECT Id, MSISDN_Range__c, MSISDN__c, isReadyForUse__c, Date_quarantine_start__c, Date_quarantine_end__c,  MSISDN_Range__r.Is_Reserved__c, MSISDN_Range__r.Account__c, MSISDN_Range__r.Range_start__c, MSISDN_Range__r.Range_end__c
                            FROM MSISDN__c WHERE isReadyForUse__c = true AND Id NOT IN (SELECT MSISDN__c FROM MSISDN_Stock__c WHERE isCurrentSIM__c = true) AND MSISDN_Range__r.Range_Type__c='Local' AND MSISDN_Range__r.Is_Reserved__c = false LIMIT :fetchedStocks.size()];
            
            for(Stock__c item : fetchedStocks)
            {
                if(item.Third_Upload_File__r.Account__c == null)
                    throw new ThirdUploadFileException('Account neds to be associated with the Third Upload File Object. Please add the Account!');
        
                if(item.SRF__r.SM2M_SAP_PO__c == null)
                    throw new ThirdUploadFileException('M2M SAP purchase order must be set on the SRF: ' + item.SRF__c);
                 
                if(item.SIM_Range_Origin__c != null && (item.SIM_Range_Origin__c.equalsIgnoreCase('GREEN') || item.SIM_Range_Origin__c.equalsIgnoreCase('BLUE'))) {
                    checkForMSISDNs(tufObject, msisdnList, msisdnStockRecordsForFetchedStocks, item, msisdnListFreeOutsideTheAccount);
                }
                
                if(tufObject.Order_Request__r.Frame_Contract__r.Type__c.equalsIgnoreCase('Trial')) {
                    Stock__c stock = new Stock__c();
                    stock.Id = item.Id;
                    stock.Trial__c = true;
                    trialStocks.add(stock);
                }
                
                response += String.format(thirdUploadFileLineFormat, new List<string> {item.ICCID__c,item.SRF__r.SM2M_SAP_PO__c,  String.isEmpty(item.Type__c) ? '': item.Type__c, apnList, staticApnIndex, staticApnAddress, item.Third_Upload_File__r.Account__c, '',''});
                //system.debug((new List<string> {item.ICCID__c,item.SRF__r.Order_Request__c, item.SIM_Model__c, apnStringFormat, staticApnIndex, staticApnAddress,item.SRF__r.Account__c}).size());
            }
        
            if(stocksForUpdate.size() > 0) {
                update stocksForUpdate;
            }
            
            if(msisdnListForUpdate.size() > 0) {
                update msisdnListForUpdate;
            }
            
            if(msisdnStockToInsert.size() > 0) {
                insert msisdnStockToInsert;
            }
            
            if(trialStocks.size() > 0) {
                update trialStocks;
            }    
        }
        
        return blob.valueOf(response);
    }
    
    private static void checkForMSISDNs(Third_Upload_File__c tufObject, List<MSISDN__c> msisdnList, List<MSISDN_Stock__c> msisdnStockRecordsForFetchedStocks, Stock__c stockItem, List<MSISDN__c> msisdnListFreeOutsideTheAccount) {
        Boolean alreadyAssigned = false;
        System.debug(LoggingLevel.INFO, 'Inside checkForMSISDNs');
        for(MSISDN_Stock__c ms : msisdnStockRecordsForFetchedStocks) {
            if(ms.Stock__c == stockItem.Id && ms.isCurrentSIM__c) {
                //throw new ThirdUploadFileException('Stock with ICCID ' + stockItem.ICCID__c + ' is already assigned to MSISDN ' + ms.MSISDN__r.MSISDN__c);
                alreadyAssigned = true;
            }
        }
        
        if(alreadyAssigned)
            return;
        
        if(stockItem.SIM_Range__r.Account__c != null && stockItem.SIM_Range__r.Account__c != tufObject.Account__c) {
            throw new ThirdUploadFileException('Stock with ICCID ' + stockItem.ICCID__c + ' is already reserved for a different Account: ' + stockItem.SIM_Range__r.Account__r.Name);
        }
        
        System.debug(LoggingLevel.INFO, 'msisdnList size ' + msisdnList.size());
        if(msisdnList != null && msisdnList.size() > 0) {
            prepareDataForDML(msisdnList[0], stockItem);
            msisdnList.remove(0);
        } else {
            System.debug(LoggingLevel.INFO, 'msisdnListFreeOutsideTheAccount size ' + msisdnListFreeOutsideTheAccount.size());
            if(msisdnListFreeOutsideTheAccount != null && msisdnListFreeOutsideTheAccount.size() > 0) {
                prepareDataForDML(msisdnListFreeOutsideTheAccount[0], stockItem);
                msisdnListFreeOutsideTheAccount.remove(0);
            }
        }
    }
    
    private static void prepareDataForDML(MSISDN__c msisdn, Stock__c stockItem) {
        System.debug(LoggingLevel.INFO, 'Inside prepareDataForDML');
        MSISDN_Stock__c msItem = new MSISDN_Stock__c();
        msItem.isCurrentSIM__c = true;
        msitem.MSISDN__c = msisdn.Id;
        msItem.Stock__c = stockItem.Id;
        msisdnStockToInsert.add(msItem);
        System.debug(LoggingLevel.INFO, 'msisdn.MSISDN__c ' + msisdn.MSISDN__c);
        stockItem.MSISDN__c = msisdn.MSISDN__c;
        stocksForUpdate.add(stockItem);
        MSISDN__c msisdnForUpdate = new MSISDN__c();
        msisdnForUpdate.Id = msisdn.Id;
        msisdnForUpdate.MSISDN_Range__c = msisdn.MSISDN_Range__c;
        msisdnForUpdate.MSISDN__c = msisdn.MSISDN__c;
        msisdnForUpdate.isReadyForUse__c = false; 
        msisdnForUpdate.Date_quarantine_start__c = null; 
        msisdnForUpdate.Date_quarantine_end__c = null;
        msisdnListForUpdate.add(msisdnForUpdate);
    }
    
    public class ThirdUploadFileException extends Exception{}
    
}