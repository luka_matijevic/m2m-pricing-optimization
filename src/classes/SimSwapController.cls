public class SimSwapController {

    public Case c {get; set;}
    public ApexPages.StandardSetController sController;
    private String query;
    private static final String limitClause = ' LIMIT ' + (Limits.getLimitQueryRows()-Limits.getQueryRows());
    private static final Integer pageSize = 100;
    public List<Stock__c> sims = new List<Stock__c>();
    private List<Stock__c> selForSimSwap = new List<Stock__c>();
    private List<Stock__c> newSimsSelection = new List<Stock__c>();
    public boolean showSrchData {get; set;}
    public boolean uploaded {get; set;}
    public Blob fileBody {get; set;}
    public String fileName {get; set;}
    public String uploadedFileName {get; set;}
    public ApexPages.StandardSetController stdController;
    private String pQuery;
    public String searchKey {get; set;}
    public Set<String> iccIds = new Set<String>();
    public Id stockId {get; set;}
    public Set<Id> addedIds = new Set<Id>();
    public boolean showSIMAddedMsg {get; set;}
    public boolean showAllSIMsAddedMsg {get; set;}
    public boolean displayUploadArea {get; set;}
    public boolean displayMainContent {get; set;}
    private boolean allSimsAdded;
    public Account acc {get; set;}
    public static final Map<Integer, String> germanMonthNames = new Map<Integer, String>{1 => 'Januar', 2 => 'Februar', 3 => 'März', 4 => 'April', 5 => 'Mai', 6 => 'Juni', 7 => 'Juli', 8 => 'August', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Dezember'};
    private String caseId;
    public String errorMsgs {get; set;}
    public String removeSim {get; set;}
    public String removeSimNew {get; set;}
    public String simLst {get; set;}
    public String csvFileData {get; set;}
    public String icicdFilter {get; set;}
    public String ticketNumber {get;set;}
    public String ticketAccount {get;set;}
    public boolean ticketLAPfieldPopulated {get;set;}
    public boolean swapProcessStarted {get;set;}
    private boolean addingNewSimCards {get;set;}
    
    public List<Stock__c> AllSims{
        get {
            try {
                sims = prepareAllSims();
            } catch(Exception e){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
            }
            return sims;
        }
        set;
    }
    
    public List<Stock__c> SelectedForSimSwap {
        get {
            try {
                selForSimSwap = prepareSimsForSwap(true);
            } catch(Exception e){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
            }
            return selForSimSwap;
        }
        set;
    }
    
    public List<Stock__c> SelectedForSimSwapNewCards {
        get {
            try {
                newSimsSelection = prepareSimsForSwap(false);
            } catch(Exception e){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
            }
            return newSimsSelection;
        }
        set;
    }
    
    public List<Stock__c> fileData{
        get{
            try{
                if(fileData == null || fileData.size() == 0) {
                    sims = new List<Stock__c>();
                    if(stdController != null){
                        for(Stock__c stock : (List<Stock__c>)stdController.getRecords()){
                            if(!addedIds.contains(stock.Id)) {
                                if(searchKey != null && searchKey.length() > 3){
                                    if((stock.ICCID__c != null && stock.ICCID__c.contains(searchKey) ) || (stock.MSISDN__c != null && stock.MSISDN__c.contains(searchKey) )){
                                        sims.add(stock);
                                    }
                                }else{
                                    sims.add(stock);
                                }
                            }
                        }
                    }
                } else {
                    sims = prepareAllSims();
                }
                return sims;
            }catch(Exception e){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
                return null;
            }
        }set;
    }
    
    public SimSwapController() {
        try{
            uploaded = false;
            displayUploadArea = false;
            displayMainContent = true;
            swapProcessStarted = false;
            allSimsAdded = false; // It will set true, If user clicks on "Add All Sims" button
            caseId = ApexPages.currentPage().getParameters().get('Id');
            if(!String.isEmpty(caseId)){
                List<Case> tmpCases = new list<Case>([Select Id, CaseNumber, Frame_Contract__c, CreatedDate, AccountId, Account.Name, LAP_Auftragsnummer__c from Case where Id = :caseId]);
                if(tmpCases.size() > 0){
                    c = tmpCases[0];
                    ticketNumber = c.CaseNumber;
                    ticketAccount = c.Account.Name;
                    ticketLAPfieldPopulated = c.LAP_Auftragsnummer__c != null && c.LAP_Auftragsnummer__c.length() > 0 ? true : false;
                }
            }
            
            putDataIntoSetCtrl('allsims');
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
    }
    
    public PageReference backToSimSwapWindow() {
        showSIMAddedMsg = false;
        showAllSIMsAddedMsg = false;
        swapProcessStarted = false;
        uploaded = false;
        displayUploadArea = !displayUploadArea;
        displayMainContent = !displayMainContent;
        putDataIntoSetCtrl('allsims');
        return null;
    }
    
    public PageReference addNewSimCards() {
        displayUploadArea = !displayUploadArea;
        displayMainContent = !displayMainContent;
        addingNewSimCards = true;
        return null;
    }
    
    public PageReference addOldSimCards() {
        displayUploadArea = !displayUploadArea;
        displayMainContent = !displayMainContent;
        addingNewSimCards = false;
        return null;
    }
    
    public List<Stock__c> prepareSimsForSwap(boolean getOldSimCards) {
        List<Stock__c> returnList = new List<Stock__c>();
        
        if(sController != null){
            for(Stock__c stock : (List<Stock__c>)sController.getRecords()){
                errorMsgs = '';
                showSrchData = true;
                if(getOldSimCards) {
                    if((stock.Status__c != null && (stock.Status__c.equalsIgnoreCase('Ready for deactivation') || stock.Status__c.equalsIgnoreCase('Deactivated')) &&  stock.SIM_Swap_Requested_For_Ticket__c == c.Id)) {
                        returnList.add(stock);
                    }
                } else {
                    if((stock.Status__c != null && (stock.Status__c.equalsIgnoreCase('Ready for activation') || stock.Status__c.equalsIgnoreCase('Activated')) &&  stock.SIM_Swap_Requested_For_Ticket__c == c.Id)) {
                        returnList.add(stock);
                    }
                }
            }
        }
        
        return returnList;
    }
    
    public PageReference startSwapProcess() {
        if(selForSimSwap != null && selForSimSwap.size() > 0 && newSimsSelection != null && newSimsSelection.size() > 0) {
            Id tufIdFromStock = selForSimSwap[0].Third_Upload_File__c;
            List<Third_Upload_File__c> tufReference = [SELECT Id, SIM_Article_Type__c, Order_Request__c, Account__c, Type__c FROM Third_Upload_File__c WHERE Id = :tufIdFromStock];
            List<APN_TUF__c> apnList = [SELECT Id, APN__c, TUF__c, Name FROM APN_TUF__c WHERE TUF__c = :tufIdFromStock];
            List<APN_TUF__c> apnListToInsert = new List<APN_TUF__c>();
            List<Order_Request__c> tufOrderRequest = [SELECT Id, Account__c, Frame_Contract__c, Trial_Kunde__c FROM Order_Request__c WHERE Id = :tufReference[0].Order_Request__c];
        
            if(tufOrderRequest != null && tufOrderRequest.size() > 0) {
                Third_Upload_File__c tuf = new Third_Upload_File__c();
                tuf.SIM_Article_Type__c = tufReference[0].SIM_Article_Type__c; //'a157E000000Fa4v';
                tuf.Order_Request__c = tufOrderRequest[0].Id; 
                tuf.Account__c = c.AccountId ;
                tuf.Type__c = tufReference[0].Type__c;
                tuf.SIM_swap_Ticket__c = c.Id;
                insert tuf;
                
                for(APN_TUF__c at : apnList) {
                    apnListToInsert.add(new APN_TUF__c(APN__c = at.APN__c, TUF__c = tuf.Id, Name = at.Name));
                }
                
                insert apnListToInsert;
                
                c.SIM_swap_TUF__c = tuf.Id;
                c.SIM_Swap_progress__c = 'SIM swap process triggered';
                update c;
                
                for(Stock__c stock : newSimsSelection) {
                    stock.Status__c = 'Pending activation';
                }
                
                for(Stock__c stock : selForSimSwap) {
                    stock.Status__c = 'Pending retirement';
                }
                
                update selForSimSwap;
                update newSimsSelection;
            }
        }
        
        swapProcessStarted = true;
        return null;
    }
    
    public List<Stock__c> prepareAllSims(){
        sims = new List<Stock__c>();
        
        if(sController != null){
            for(Stock__c stock : (List<Stock__c>)sController.getRecords()){
                errorMsgs = '';
                showSrchData = true;
                if(stock.Status__c != null && (!stock.Status__c.equalsIgnoreCase('Ready for deactivation') && !stock.Status__c.equalsIgnoreCase('Ready for activation')) && stock.SIM_Swap_Requested_For_Ticket__c == null) {
                    if(icicdFilter != null && icicdFilter != '') {
                        if(stock.ICCID__c.contains(icicdFilter) || stock.MSISDN__c.contains(icicdFilter)){
                            sims.add(stock);  
                        }
                    } else {
                        sims.add(stock);  
                    }
                }
            }   
        }
        
        return sims;
    
    }
    
    public PageReference uploadSims(){
        try{
            fileData = new List<Stock__c>();
            searchKey = null;
            uploaded = true;
            showSIMAddedMsg = false;
            swapProcessStarted = false;
            showAllSIMsAddedMsg = false;
            if(fileName != null && fileName != ''){
                uploadedFileName = fileName;
            }
            iccIds = SIMDispatchFileParser.parseIccidAndMSISDNExportFile(fileBody,true);
            if(!iccIds.isEmpty()){
                putDataIntoSetCtrl('iccid');
            }
        }catch(Exception e){
            if(e.getMessage().contains('Argument cannot be null')){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, 'Please select a file'));
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
            }
        }
        return null;
    }
    
    public void putDataIntoSetCtrl(String type){
        try{
            if(type != null){
                if(type.equalsIgnoreCase('iccid')){
                    pQuery = 'Select Id,Name,ICCID__c,MSISDN__c,IMSI__c, SIM_Swap_Requested_For_Ticket__c,Account__c, Status__c, Third_Upload_File__c from stock__c where Account__c = \'' + c.AccountId + '\' AND (ICCID__c IN :iccIds OR MSISDN__c IN :iccIds) Limit ' + (Limits.getLimitQueryRows()-Limits.getQueryRows());
                    stdController = new ApexPages.StandardSetController(Database.getQueryLocator(pQuery));
                    stdController.setPageSize(pageSize);
                }
                else if(type.equalsIgnoreCase('allsims')){
                    allSimsAdded = true;
                    query = 'Select Id,Name,ICCID__c,MSISDN__c,IMSI__c,SIM_Swap_Requested_For_Ticket__c,Account__c, Status__c, Third_Upload_File__c from stock__c where Account__c = \'' + c.AccountId + '\'';
                    
                    if(addedIds != null && addedIds.size() > 0){
                        query = query + ' OR Id In :addedIds';
                    }
                    
                    sController = new ApexPages.StandardSetController(Database.getQueryLocator(query + limitClause));
                    sController.setPageSize(pageSize);
                }else if(type.equalsIgnoreCase('icicdFilter')){
                    if(allSimsAdded){
                        query = 'Select Id,Name,ICCID__c,MSISDN__c,IMSI__c,SIM_Swap_Requested_For_Ticket__c,Account__c, Status__c, Third_Upload_File__c from stock__c where Account__c = \'' + c.AccountId  + '\'';        
                    }
                    else{
                        query = 'Select Id,Name,ICCID__c,MSISDN__c,IMSI__c,SIM_Swap_Requested_For_Ticket__c,Account__c, Status__c, Third_Upload_File__c from stock__c where Account__c = \'' + c.AccountId + '\' AND SIM_Swap_Requested_For_Ticket__c = \'' + c.Id+ '\'';
                    }
                    
                    if(!addedIds.isEmpty()){
                    	query += ' AND Id IN :addedIds';
                    }
                    if(icicdFilter != null && icicdFilter != ''){
                    	String srchFilter = '%'+ icicdFilter+ '%';
                    	query += ' AND (ICCID__c like :srchFilter OR MSISDN__c like :srchFilter)';
                    }
                    if(query != null){
                        sController = new ApexPages.StandardSetController(Database.getQueryLocator(query + limitClause));
                        sController.setPageSize(pageSize);
                    }
                }
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
    }

    //==================================================================================================================================================
    //Pagination for Main Page
    public Boolean hasNext {
        get {
            return sController.getHasNext();
        }
        set;
    }
    
    public Boolean hasPrevious {
        get {
            return sController.getHasPrevious();
        }
        set;
    }
    
    public Integer getPageNumber(){
        return sController.getPageNumber();
    }
    
    public Integer getTotalPages(){
        Decimal noPages = (sController.getResultSize() / sController.getPageSize());
        noPages = Math.floor(noPages) + ((Math.mod(sController.getResultSize(), sController.getPageSize()) > 0) ? 1 : 0);
        return (Integer.valueOf(noPages));
    }
    
    public void first() {
        sController.first();
    }
    
    public void last() {
        sController.last();
    }
    
    public void previous() {
        sController.previous();
    }
    
    public void next() {
        sController.next();
    }
    //==================================================================================================================================================
    
    //=================================================================================================================================================
    //Pagination for Popup page
    public Boolean hasPNext {
        get {
            return stdController.getHasNext();
        }
        set;
    }
    
    public Boolean hasPPrevious {
        get {
            return stdController.getHasPrevious();
        }
        set;
    }
    
    public Integer getPPageNumber(){
        return stdController.getPageNumber();
    }
    
    public Integer getPTotalPages(){
        Decimal noPages = (stdController.getResultSize() / stdController.getPageSize());
        noPages = Math.floor(noPages) + ((Math.mod(stdController.getResultSize(), stdController.getPageSize()) > 0) ? 1 : 0);
        return (Integer.valueOf(noPages));
    }
    
    public void pfirst() {
        stdController.first();
    }
    
    public void plast() {
        stdController.last();
    }
    
    public void pprevious() {
        stdController.previous();
    }
    
    public void pnext() {
        stdController.next();
    }
    //=================================================================================================================================================
    
    public string checkNullString(string obj){
        if(obj == null){
            obj = '';
        }
        return obj;
    }
    
    public static string formatDateTime(DateTime dt){
         string tmp = '';
         if(dt != null){
             tmp = dt.format('dd.MM.yyyy HH:mm');
         }
         return tmp;
    }
    
    public PageReference addAllToSimList(){
        try{
            List<Stock__c> stocksForUpdate = new List<Stock__c>();
            if(addingNewSimCards) {
                //Quering again without considering fileData because of pagination as filedata contains only records of one page but not all
                for(Stock__c stock : Database.query(pQuery)){
                    stock.Status__c = 'Ready for activation';
                    stock.SIM_Swap_Requested_For_Ticket__c = c.Id;
                    stocksForUpdate.add(stock);
                }
            } else {
                //Quering again without considering fileData because of pagination as filedata contains only records of one page but not all
                for(Stock__c stock : Database.query(pQuery)){
                    stock.Status__c = 'Ready for deactivation';
                    stock.SIM_Swap_Requested_For_Ticket__c = c.Id;
                    stocksForUpdate.add(stock);
                }
            }
            
            
            if(stocksForUpdate.size() > 0)
                update stocksForUpdate;
            
            
            showAllSIMsAddedMsg = true;
            showSIMAddedMsg = false;
            swapProcessStarted = false;
            uploaded = false;
            
            putDataIntoSetCtrl('allsims');
            prepareAllSims();
        }catch(Exception e){
            showAllSIMsAddedMsg = false;
            if(e.getMessage().contains('Argument 1 cannot be null')){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, 'Query is null'));
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
            }
        }
        return null;
    }
    
    public PageReference addSIMCardForSwap() {
        try{
            if(stockId != null){
                if(addingNewSimCards) {
                    for(Stock__c stock : sims){
                        if(String.valueOf(stock.Id).equals(stockId)) {
                            stock.Status__c = 'Ready for activation';
                            stock.SIM_Swap_Requested_For_Ticket__c = c.Id;
                            update stock;
                            break;
                        }
                    }
                } else {
                    for(Stock__c stock : sims){
                        if(String.valueOf(stock.Id).equals(stockId)) {
                            stock.Status__c = 'Ready for deactivation';
                            stock.SIM_Swap_Requested_For_Ticket__c = c.Id;
                            update stock;
                            break;
                        }
                    }
                }
            }
            showSIMAddedMsg = true;
            uploaded = false;
            swapProcessStarted = false;
            putDataIntoSetCtrl('allsims');
            prepareAllSims();
        }catch(Exception e){
            showSIMAddedMsg = false;
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
        return null;
    }
    
    public PageReference performSearch(){
        return null;
    }
    
    public PageReference removeSim(){
        //system.debug('removeSim **** '+removeSim);
        if(removeSim != null && removeSim != ''){
            for(Stock__c stock : selForSimSwap){
                if(String.valueOf(stock.Id).equals(removeSim)) {
                    stock.Status__c = 'Active';
                    stock.SIM_Swap_Requested_For_Ticket__c = null;
                    update stock;
                    break;
                }
            }
        }
        prepareAllSims();
        return null;
    }
    
    public PageReference removeSimNew(){
        //system.debug('removeSim **** '+removeSim);
        if(removeSimNew != null && removeSimNew != ''){
            for(Stock__c stock : newSimsSelection){
                if(String.valueOf(stock.Id).equals(removeSimNew)) {
                    stock.Status__c = 'Active';
                    stock.SIM_Swap_Requested_For_Ticket__c = null;
                    update stock;
                    break;
                }
            }
        }
        prepareAllSims();
        return null;
    }
    
    public pageReference clearSelectedSims(){
        try{
            if(!sims.isEmpty()){
                showSrchData = false;
                addedIds = new set<id>();
                sController = null;
                allSimsAdded = false;
                swapProcessStarted = false;
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, 'Please add the sims before clearing'));
            }
        }catch(Exception e){
            showSIMAddedMsg = false;
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
        return null;
    }
    
    public Integer getNoOfSIMs(){
        Integer i = 0;
        try{
            String queryForCount;
            if(allSimsAdded){
                queryForCount = 'Select Count() from stock__C where (Account__c = \'' + c.AccountId + '\'';
            }
            
            if(icicdFilter != null && icicdFilter != ''){
                String srchFilter = '%'+ icicdFilter+ '%';
                queryForCount += ' AND (ICCID__c like :srchFilter OR MSISDN__c like :srchFilter)';
            }
            queryForCount += ')';
            if(!addedIds.isEmpty()){
                queryForCount += ' OR Id IN :addedIds';
            }
            
            i = Database.countQuery(queryForCount);
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
        return i;
    }
    
    public String convertDateToGermanFormat(Date reqDate){
        Integer day, month, year;
        String germanDate = '';
        try{
            if(reqDate != null){
                day = reqDate.day();
                month = reqDate.month();
                year = reqDate.year();
                if(!germanMonthNames.isEmpty() && germanMonthNames.containsKey(month)){
                    germanDate = day + '. ' + germanMonthNames.get(month) + ' ' + year;
                }
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        }
        return germanDate;
    }
    
    public PageReference filterRecordsByICCIDOrMSISDN(){
        putDataIntoSetCtrl('icicdFilter');
        return null;
    }
}