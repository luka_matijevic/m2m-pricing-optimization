public interface IRateCardCountryManagement
{
	//--------------------------------------------------------------------------
	// Getters / setters
	//--------------------------------------------------------------------------
	Rate_Card_Country_Management__c getRateCardCountryManagement();
	
	String getCountryCode();
	
	void setCountryCode(String countryCode);
	
	String getCountryName();
	
	void setCountryName(String countryName);
	
	//Rate_Card__c getRateCard();
	
	Decimal getRatio();
	
	void setRatio(Decimal ratio);
	
	Id getRateCardCountryManagementId();
}