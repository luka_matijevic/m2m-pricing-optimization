global class RetrieveAccountAPNsBatch implements Database.Batchable<SObject>{ 

     
    //---------------------------------------------------------------------------
    // Start of batch apex
    //---------------------------------------------------------------------------
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        //
        // Filter records
        //

        String query = 'SELECT Id from account where SM2M_ID__c != null OR SM2M_ID_Local__c != null';
        return Database.getQueryLocator(query);
    }

    //--------------------------------------------------------------------------
    // Execute of batch apex
    //--------------------------------------------------------------------------
    global void execute(Database.BatchableContext bc, List<SObject> scope)
    {
        for (Account acct : (List<Account>)scope)
        {
           csam_t1.ObjectGraphCalloutHandler.queueMessageFromId
				( 'Retrieve APNs'
				, acct.Id);
        }
    }

    //--------------------------------------------------------------------------
    // Finish of batch apex
    //--------------------------------------------------------------------------
    global void finish(Database.BatchableContext bc)
    {
	
    }
}