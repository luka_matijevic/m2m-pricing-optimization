@isTest
public class AttachmentTriggerDelegateTest {
	
	@isTest static void testPdfContentTypeChange() {
		
		Account a = new Account(
			Name = 'Test Account',
			Type = 'Customer'
		);
		insert a;
		
		Attachment att = new Attachment(
			Name = 'test.pdf',
			ContentType = 'text/plain',
			Body = Blob.valueOf('content'),
			ParentId = a.Id
		);
		insert att;
		
		Attachment inserted = [Select ContentType from Attachment where Id = :att.Id];
		System.assertEquals('application/pdf', inserted.ContentType);
	}
	
	@isTest static void testBicReportReaction() {
		
		BIC_Export_Job_Data__c jobData = new BIC_Export_Job_Data__c(
			Name = 'BIC job'
		);
		insert jobData;
		
		Attachment att = new Attachment(
			Name = 'm2m_to_telefonica_sim_lifecycle_20140414_001.zip',
			ContentType = 'application/zip',
			Body = Blob.valueOf('content'),
			ParentId = jobData.Id
		);
		insert att;
		
		BIC_Export_Job_Data__c updatedJobData = [Select Id, Report_Generated__c from BIC_Export_Job_Data__c where Id = :jobData.Id limit 1];
		
		system.assertEquals(true, updatedJobData.Report_Generated__c);
	}
}