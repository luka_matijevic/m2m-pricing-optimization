//------------------------------------------------------------------------------
// Helper class for inserting related objects
//------------------------------------------------------------------------------
public without sharing class InsertHelper implements IInsertHelper
{
    public class InsertHelperException extends Exception {}

    //--------------------------------------------------------------------------
    // Inner class for parent child connections
    //--------------------------------------------------------------------------
    private class Connection
    {
        //----------------------------------------------------------------------
        // Constructor
        //----------------------------------------------------------------------
        public Connection(String p_field,
                SObjectWrapper p_parent, Integer p_level)
        {
            this.field = p_field;
            this.parent = p_parent;
            this.level = p_level;
        }

        private String field;
        private SObjectWrapper parent;
        private Integer level;
    }

    //--------------------------------------------------------------------------
    // Inner class for child
    //--------------------------------------------------------------------------
    public class SObjectWrapper
    {
        //----------------------------------------------------------------------
        // Constructor
        //----------------------------------------------------------------------
        public SObjectWrapper(SObject p_sObject)
        {
            this.wrappedObject = p_sObject;
        }

        //----------------------------------------------------------------------
        // equals
        //----------------------------------------------------------------------
        public Boolean equals(Object p_obj)
        {
            SObjectWrapper obj = (SObjectWrapper)p_obj;
            if (obj == null)
            {
                return false;
            }
            return (this.wrappedObject === obj.wrappedObject);
        }

        //----------------------------------------------------------------------
        // hashCode
        //----------------------------------------------------------------------
        public Integer hashCode()
        {
            Integer returnValue = 0;
            String name = (String)wrappedObject.get('Name');
            if (name != null)
            {
                returnValue = name.hashCode();
            }
            return returnValue;
        }

        public SObject wrappedObject;
    }

    private List<List<SObject>> objectLevels;
    private Map<SObjectWrapper, Connection> objectConnections;

    //--------------------------------------------------------------------------
    // Empty constructor
    //--------------------------------------------------------------------------
    public InsertHelper()
    {
        //
        // Initialize private members
        //
        objectLevels = new List<List<SObject>>();
        objectConnections = new Map<SObjectWrapper, Connection>();
    }

    //--------------------------------------------------------------------------
    // Add single object
    //--------------------------------------------------------------------------
    public void add(SObject obj)
    {
        //
        // Insert object to first level
        //
        addToStructure(obj, null, null);
    }

    //--------------------------------------------------------------------------
    // Add related objects
    // child       Child SObject
    // fieldName   Field name on child object for lookup to parent object
    // parent      Parent SObject
    //--------------------------------------------------------------------------
    public void add(SObject child, String fieldName, SObject parent)
    {
        if (child == null)
        {
            return;
        }
        //
        // Check field name
        //
        Schema.DescribeFieldResult fieldDescription =
                SObjectHelper.getFieldDescription(child.getSObjectType(), fieldName);
        if (fieldDescription == null)
        {
            throw new InsertHelperException('Field ' + fieldName + ' doesn\'t exist');
        }
        if (fieldDescription.getType() != Schema.DisplayType.Reference)
        {
            throw new InsertHelperException('Filed ' + fieldName + ' is not lookup');
        }
        //
        // Add it to structure
        //
        addToStructure(child, fieldName, parent);
    }

    //--------------------------------------------------------------------------
    // Save all objects to the database with relationships
    //--------------------------------------------------------------------------
    public void store()
    {
        for (List<SObject> objectLevel : objectLevels)
        {
            for (SObject obj : objectLevel)
            {
                //
                // Connect with parent object
                //
                SObjectWrapper wrappedObject = new SObjectWrapper(obj);
                Connection connection = objectConnections.get(wrappedObject);
                if (connection.parent != null && connection.field != null)
                {
                    obj.put(connection.field, connection.parent.wrappedObject.get('Id'));
                }
            }
            //
            // chunk the objectLevel list if it exceeds SalesForce chunk limit
            // (FATAL_ERROR|System.TypeException: Cannot have more than 10 chunks
            // in a single operation. Please rearrange the data to reduce chunking.)
            //
            List<List<sObject>> chunkSafeLists = saveSObjectList(objectLevel);
            System.debug('--------------------'+chunkSafeLists );
            for(List<sObject> sObjectList : chunkSafeLists){
                    System.debug(sObjectList);
                   insert sObjectList;
            }
        }
        //
        // Clear all data
        //
        objectLevels.clear();
        objectConnections.clear();
    }

    public void storeUpdate() 
    {
        for (List<SObject> objectLevel : objectLevels)
        {
            for (SObject obj : objectLevel)
            {
                //
                // Connect with parent object
                //
                SObjectWrapper wrappedObject = new SObjectWrapper(obj);
                Connection connection = objectConnections.get(wrappedObject);
                if (connection.parent != null && connection.field != null)
                {
                    obj.put(connection.field, connection.parent.wrappedObject.get('Id'));
                }
            }
            //
            // chunk the objectLevel list if it exceeds SalesForce chunk limit
            // (FATAL_ERROR|System.TypeException: Cannot have more than 10 chunks
            // in a single operation. Please rearrange the data to reduce chunking.)
            //
            List<List<sObject>> chunkSafeLists = saveSObjectList(objectLevel);
            System.debug('--------------------'+chunkSafeLists );
            for(List<sObject> sObjectList : chunkSafeLists){
                   update sObjectList;
            }
        }
        //
        // Clear all data
        //
        objectLevels.clear();
        objectConnections.clear();
    }

    //--------------------------------------------------------------------------
    // Used for clonning
    //--------------------------------------------------------------------------
    public IInsertHelper deepClone()
    {
        return new InsertHelper();
    }

    //--------------------------------------------------------------------------
    // Get list of SObjects at level
    //--------------------------------------------------------------------------
    private List<SObject> getLevel(Integer level)
    {
        //
        // Create new values for list
        //
        if (objectLevels.size() == level)
        {
            objectLevels.add(new List<SObject>());
        }
        else if (objectLevels.size() < level)
        {
            system.assert(false);
        }
        return objectLevels[level];
    }

    //--------------------------------------------------------------------------
    // Add child object with connection to parent
    //--------------------------------------------------------------------------
    private void addToStructure(SObject child, String field, SObject parent)
    {
        System.debug(child);
        //
        // Add to first level
        //
        if (parent == null)
        {
            getLevel(0).add(child);
            objectConnections.put(new SObjectWrapper(child), new Connection(null, null, 0));
        }
        else
        {
            //
            // Search for parent
            //
            SObjectWrapper parentWrapper = new SObjectWrapper(parent);
            Connection parentConnection = objectConnections.get(parentWrapper);
            //
            // Check if parent exists, if not add it to level 0
            //
            if (parentConnection == null)
            {
                add(parent);
                parentConnection = objectConnections.get(parentWrapper);
            }
            //
            // Add to level
            //
            getLevel(parentConnection.level + 1).add(child);
            objectConnections.put(
                    new SObjectWrapper(child),
                    new Connection(
                        field,
                        new SObjectWrapper(parent),
                        parentConnection.level + 1));
        }
    }

    //--------------------------------------------------------------------------
    // Organize and split the list into chunks
    //--------------------------------------------------------------------------
    private static List<List<SObject>> saveSObjectList(List<SObject> listToUpdate)
    {
        List<List<SObject>> chunkedList = new List<List<SObject>>();
        //
        // Developed this part due to
        // System.TypeException: Cannot have more than 10 chunks in a single operation
        //
        Map<Schema.SObjectType, List<SObject>> sortedMapPerObjectType =
                new Map<Schema.SObjectType, List<SObject>>();
        for (SObject obj : listToUpdate)
        {
            Schema.SObjectType objType = obj.getSObjectType();
            if (!sortedMapPerObjectType.containsKey(objType))
            {
                sortedMapPerObjectType.put(objType, new List<SObject>());
            }
            sortedMapPerObjectType.get(objType).add(obj);
        }

        while (!sortedMapPerObjectType.isEmpty())
        {
            //
            // Create a new list, which can contain a max of chunking limit,
            // and sorted, so we don't get any errors
            //
            List<SObject> safeListForChunking = new List<SObject>();
            List<Schema.SObjectType> keyListSObjectType = new List<Schema.SObjectType>(sortedMapPerObjectType.keySet());
            for (Integer i = Constants.SFDC_CHUNK_LIMIT; i > 0 && !sortedMapPerObjectType.isEmpty(); i--)
            {
                List<SObject> listSObjectOfOneType;
                //
                // If we have more sobjects of the same type than limits
                //
                if (sortedMapPerObjectType.get(keyListSObjectType.get(0)).size() > Constants.SFDC_CHUNKED_RECORD_COUNT_LIMIT)
                {
                    listSObjectOfOneType = new List<SObject>();
                    for (Integer j = Constants.SFDC_CHUNKED_RECORD_COUNT_LIMIT; j > 0; j--)
                    {
                        SObject record = sortedMapPerObjectType.get(keyListSObjectType.get(0)).remove(0);
                        listSObjectOfOneType.add(record);
                    }
                }
                else
                {
                    listSObjectOfOneType = sortedMapPerObjectType.remove(keyListSObjectType.remove(0));
                }
                safeListForChunking.addAll(listSObjectOfOneType);
            }
            chunkedList.add(safeListForChunking);
        }
        return chunkedList;
    }

}