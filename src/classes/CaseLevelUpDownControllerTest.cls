@isTest
private class CaseLevelUpDownControllerTest{
    static testmethod void caseLevelUpTest(){
        Contact con = TestHelper_CaseLevelUpDown.createContact();
        TestHelper_CaseLevelUpDown.createEntitlements(con.AccountId, 'SLA Priority 1');
        Case c = TestHelper_CaseLevelUpDown.createCase(con);
        CaseComment cCom = TestHelper_CaseLevelUpDown.createCaseComment(c);
        ApexPages.standardController stdCtrl = new Apexpages.standardController(c);
        CaseLevelUpDownController clc = new CaseLevelUpDownController(stdCtrl);
        clc.ticketComment = cCom;
        System.assertEquals(clc.ticketComment.CommentBody, cCom.CommentBody);
        PageReference pgRef = Page.CaseLevelUp;
     	Test.setCurrentPageReference (pgRef);
        Test.startTest();
        clc.levelUpCase();
        clc.cancelCase();
        Test.stopTest();
        //System.assertEquals([Select ownerId from Case LIMIT 1].ownerId, [select id, name, developerName, type from Group where Type = 'Queue' and developerName = 'Operations_Management'].Id);
    }
    
    static testmethod void caseLevelUpTest1(){
        Contact con = TestHelper_CaseLevelUpDown.createContact();
        TestHelper_CaseLevelUpDown.createEntitlements(con.AccountId, 'Recurring');
        Case c = TestHelper_CaseLevelUpDown.createCase(con);
        CaseComment cCom = TestHelper_CaseLevelUpDown.createCaseComment(c);
        ApexPages.standardController stdCtrl = new Apexpages.standardController(c);
        CaseLevelUpDownController clc = new CaseLevelUpDownController(stdCtrl);
        clc.ticketComment = cCom;
        System.assertEquals(clc.ticketComment.CommentBody, cCom.CommentBody);
        PageReference pgRef = Page.CaseLevelUp;
     	Test.setCurrentPageReference (pgRef);
        Test.startTest();
        clc.levelUpCase();
        clc.cancelCase();
        Test.stopTest();
        //System.assertEquals([Select ownerId from Case LIMIT 1].ownerId, [select id, name, developerName, type from Group where Type = 'Queue' and developerName = 'CS_NUE'].Id);
    }
    
    static testmethod void caseLevelDownTest(){
        Contact con = TestHelper_CaseLevelUpDown.createContact();
        TestHelper_CaseLevelUpDown.createEntitlements(con.AccountId, 'SLA Priority 1');
        Case c = TestHelper_CaseLevelUpDown.createCase(con);
        CaseComment cCom = TestHelper_CaseLevelUpDown.createCaseComment(c);
        ApexPages.standardController stdCtrl = new Apexpages.standardController(c);
        CaseLevelUpDownController clc = new CaseLevelUpDownController(stdCtrl);
        clc.ticketComment = cCom;
        PageReference pgRef = Page.CaseLevelUp;
     	Test.setCurrentPageReference (pgRef);
        Test.startTest();
        clc.levelDownCase();
        clc.cancelCase();
        Test.stopTest();
        //System.assertEquals([Select ownerId from Case LIMIT 1].ownerId, [select id, name, developerName, type from Group where Type = 'Queue' and developerName = 'DSC_Queue'].Id);
    }
    
    static testmethod void caseLevelDownTest1(){
        Contact con = TestHelper_CaseLevelUpDown.createContact();
        TestHelper_CaseLevelUpDown.createEntitlements(con.AccountId, 'SLA Priority 1');
        Case c = TestHelper_CaseLevelUpDown.createCase(con);
        CaseComment cCom = TestHelper_CaseLevelUpDown.createCaseComment(c);
        ApexPages.standardController stdCtrl = new Apexpages.standardController(c);
        CaseLevelUpDownController clc = new CaseLevelUpDownController(stdCtrl);
        clc.ticketComment = cCom;
        PageReference pgRef = Page.CaseLevelUp;
     	Test.setCurrentPageReference (pgRef);
        Test.startTest();
        clc.levelUpCase();
        //System.assertEquals([Select ownerId from Case LIMIT 1].ownerId, [select id, name, developerName, type from Group where Type = 'Queue' and developerName = 'Operations_Management'].Id);
        clc.levelDownCase();
        clc.cancelCase();
        Test.stopTest();
        //System.assertEquals([Select ownerId from Case LIMIT 1].ownerId, [select id, name, developerName, type from Group where Type = 'Queue' and developerName = 'CS_NUE'].Id);
    }
}