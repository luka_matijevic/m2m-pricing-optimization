public with sharing class RateCardCountryManagementController {
    public String selectedCountry { get; set; }
    public Integer rowCount = 1;
    public List<Rate_Card_Country_Management__c> rccList { get; set; }
    public Integer rowToRemove { get; set; }
    public Id rateCardId { get; set; }
    private List<SelectOption> countries;
    
    public RateCardCountryManagementController(ApexPages.StandardController cntrl) {
		initiatePage();
        rccList = new List<Rate_Card_Country_Management__c>();
        addNewRowToRccList();
	}
	
	public void initiatePage() {
	    rateCardId = 'a1FZ000000173Tk'; //europe
	    //rateCardId = 'a1FZ000000173Tl'; //world
	}
	
	public PageReference saveRow() {
        save(rccList);
        return null;
    }
	
	public List<SelectOption> getCountries() {
    
        countries = new List<SelectOption>();
        
        Rate_Card__c selectedRateCard = [SELECT Id, Name, Worldzone__c FROM Rate_Card__c WHERE Id = :rateCardId];
        List<EU_Countries__c> countriesCustSett = [SELECT Id, Name, Code__c FROM EU_Countries__c ORDER BY Name ASC];
        
        if(selectedRateCard != null) {
            if(selectedRateCard.Worldzone__c.contains('Europe')) {
                if(countriesCustSett != null && countriesCustSett.size() > 0) {
                    for (EU_Countries__c eu : countriesCustSett) {
                        countries.add(new SelectOption(eu.Code__c, eu.Name));
                    }
                }
            }
            else if(selectedRateCard.Worldzone__c.contains('World')) {
                Schema.DescribeFieldResult fieldResult = User.Countrycode.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
                for(Schema.PicklistEntry f : ple) {
                    Boolean isExisting = false;
                    for(EU_Countries__c ctry : countriesCustSett) {
                        if(f.getValue().equalsIgnoreCase(ctry.Code__c)) {
                            isExisting = true;
                        }
                    }
                    
                    if(!isExisting)
                        countries.add(new SelectOption(f.getValue(), f.getLabel()));
                }
            }
        }
        
        return countries;
    }
    
    private String getCountryNameByCode(string code) {
        String cName = '';
        
        if(countries != null && countries.size() > 0) {
            for (SelectOption c : countries){
                if (c.getValue() == code){
                    cName = c.getLabel();
                    break;
                }
            }
        }
        
        return cName;
    }
    
    public void removeRowFromRccList() {
        rccList = removeRowToRccList(rowToRemove, rccList);
    }
    
    public void addNewRowToRccList() {
        rccList = addNewRowToRccList(rccList);
    }
   
   private List<Rate_Card_Country_Management__c> addNewRowToRccList(List<Rate_Card_Country_Management__c> rccList) {
        Rate_Card_Country_Management__c newRecord = new Rate_Card_Country_Management__c();
        
        rccList.add(newRecord);
        
        return rccList;
    }
    
    
    private List<Rate_Card_Country_Management__c> removeRowToRccList(Integer rowToRemove, List<Rate_Card_Country_Management__c> rccList) {
        rccList.remove(rowToRemove);
        return rccList;
    }
    
    private void save(List<Rate_Card_Country_Management__c> rccList) {
        Decimal totalRatio = 0;
        if(rccList != null && rccList.size() > 0) {
            for(Rate_Card_Country_Management__c rcc : rccList) {
                totalRatio += rcc.Ratio__c;
                rcc.Rate_Card__c = rateCardId;
                rcc.Country_name__c = getCountryNameByCode(rcc.Country_code__c);
            }
            
            if(totalRatio > 100) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Total ratio is higher than 100!');
                ApexPages.addMessage(myMsg);
            }
            else {
                insert rccList;
            }
        }
    }
}