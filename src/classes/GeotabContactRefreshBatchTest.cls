@isTest
private class GeotabContactRefreshBatchTest {
	
	@isTest static void test_method_one() {
		List<GeotabUtils.GeotabUserContact> geotabContactsApi = new List<GeotabUtils.GeotabUserContact>();
		GeotabUtils.GeotabUserContact geotabContact = new GeotabUtils.GeotabUserContact();

		geotabContact.Active = true;
		geotabContact.Address = 'test';
		geotabContact.City = 'test';
		geotabContact.ContactEmail = 'test@email.com';
		geotabContact.ContactName = 'test name';
		geotabContact.Country = 'test country';
		geotabContact.DisplayName = 'test display name';
		geotabContact.Id = 2;
		geotabContact.State = 'test state';
		geotabContact.Street1 = 'test street';
		geotabContact.Street2 = 'test street 2';
		geotabContact.Telephone1 = '0292929202';
		geotabContact.Telephone2 = '3737733';
		geotabContact.ZipCode = '22222';

		geotabContactsApi.add(geotabContact);

		Geotab_Contact__c gcont = new Geotab_Contact__c();
		gcont.Geotab_Id__c = '1';
		gcont.Geotab_Contact_Name__c = 'First Contact';
		insert gcont;

		Test.startTest();
		Database.executeBatch(new GeotabContactRefreshBatch(geotabContactsApi));
		Test.stopTest();
	}	
}