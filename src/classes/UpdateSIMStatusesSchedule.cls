global with sharing class UpdateSIMStatusesSchedule  implements Schedulable {
	// Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
	public static final String CRON_EXP = '0 0 1 * * ? *';

	global static String scheduleIt() {
		return System.schedule('Daily SIM Statuses Update', CRON_EXP, new UpdateSIMStatusesSchedule());
	}
	
	global void execute(SchedulableContext sc)	{
		Database.executeBatch(new UpdateSIMStatusesJob(), 1);
	}
}