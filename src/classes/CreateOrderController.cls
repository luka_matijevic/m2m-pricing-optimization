//------------------------------------------------------------------------------
// Creates order
//------------------------------------------------------------------------------
public with sharing class CreateOrderController
{
	//--------------------------------------------------------------------------
	// Members
	//--------------------------------------------------------------------------
	ApexPages.StandardController controller;
	public String errorMessage {get; set;}
	public boolean hasTrial {get; set;}
	private Order_Request__c orderRequest;
	public double sm2mID {get; set;}
	public String accountsm2mID {get; set;}
	public id billingAccountId {get; set;}

	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	public CreateOrderController(ApexPages.StandardController controller)	
	{	
		if(!test.isRunningTest()) {
			List<String> fieldList = new List<String>();
			fieldList.add('Billing_Account__r.Trial__c');
			fieldList.add('Billing_Account__c');
			fieldList.add('Billing_Account__r.Account_SM2M_ID__c');
			fieldList.add('Billing_Account__r.SM2M_ID__c');
			fieldList.add('Status__c');
			controller.addFields(fieldList);
		}
		this.controller = controller;
		this.orderRequest = (Order_Request__c) controller.getRecord();		
		this.hasTrial = orderRequest.Billing_Account__r.Trial__c;
		this.accountsm2mID = orderRequest.Billing_Account__r.Account_SM2M_ID__c;
		this.sm2mID = orderRequest.Billing_Account__r.SM2M_ID__c;
		this.billingAccountId = orderRequest.Billing_Account__c;
	}
	
	public boolean hasTrial(){
		return hasTrial;
	}
	
	public void commercialize()
	{
		Billing_Account__c ba = [select id, Trial__c, Billable__c from Billing_Account__c where id = :orderRequest.Billing_Account__c limit 1];
		ba.Trial__c = false;
		ba.Billable__c = true;
		try{
			update ba;
			hasTrial = false;
			
		}
		catch (DmlException  e){
			errorMessage = e.getdmlMessage(0);
		}
		
	}
	

	//--------------------------------------------------------------------------
	// Create order with order line items
	//--------------------------------------------------------------------------
	public PageReference create()
	{
		//
		// Set savepoint
		//
		Savepoint sp = Database.setSavepoint();
		try
		{
			//
			// Create order
			//
			Id orderId = Factory.createCreateOrder(controller.getId()).create();
			//
			// Create subscriptions
			//
			Factory.createCreateSubscriptions(orderId).create();
			
			//Close M2M order request && update sims to commercial
			closeOrderRequest();
			updateSims();
			
			return new PageReference('/' + orderId);
		}
		catch (JournalMappingException e)
		{
			//
			// If there is no journal mapping
			// rollback everything and set error message
			//
			Database.rollback(sp);
			errorMessage = 'Cannot find journal mapping(s) for the artice info set(s) ' + e.getMessage();
		}
		catch (CreateOrder.OrderException e)
		{
			//
			// If order already exists
			//
			//errorMessage = 'Order is already created';
			errorMessage = e.getMessage();
		}
		return null;
	}
	
	private void closeOrderRequest(){
		orderRequest.Status__c = 'Closed';
		update orderRequest;
	}
	private void updateSims(){
		
		for (List<Stock__c> sList : [ select id, Trial__c from Stock__c where Trial__c = true and Third_Upload_File__r.Order_Request__c = :controller.getId() ]){
				for (Stock__c s:slist){
					s.Trial__c = false;
				}					
			update sList;	
		}
		
			
	}	
	

	//--------------------------------------------------------------------------
	// Goto order request
	//--------------------------------------------------------------------------
	public PageReference gotoOrderRequest()
	{
		return new PageReference('/' + controller.getId());
	}
}