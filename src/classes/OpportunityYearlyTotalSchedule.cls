global class OpportunityYearlyTotalSchedule implements Schedulable {
	
	public static String CRON_EXP = '0 0 0 1 1 ? *';
	
	global static String scheduleIt() {
		OpportunityYearlyTotalSchedule yt = new OpportunityYearlyTotalSchedule();
		return System.schedule('Yearly Totals Calculation', CRON_EXP, yt);
	}
	
	global void execute(SchedulableContext sc) {
		OpportunityBatchUpdateYearlyTotal batch = new OpportunityBatchUpdateYearlyTotal();
		Database.executeBatch(batch, 4);
	}
}