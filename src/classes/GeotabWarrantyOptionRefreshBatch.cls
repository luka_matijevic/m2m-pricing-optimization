global class GeotabWarrantyOptionRefreshBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
	
	String query;
	List<Geotab_Warranty_Option__c> geotabWarranty;
	List<GeotabUtils.GeotabWarrantyOption> geotabWarrantyApi;
	Map<String, Geotab_Warranty_Option__c> sfdcGeotabWarrantyMap = new Map<String, Geotab_Warranty_Option__c>();
	List<Geotab_Warranty_Option__c> geotabWarrantyForInsert;
	
	global GeotabWarrantyOptionRefreshBatch(List<GeotabUtils.GeotabWarrantyOption> geotabWarrantyApi) {
		this.geotabWarrantyApi = geotabWarrantyApi;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		query = 'SELECT Id, Geotab_Id__c, Geotab_Warranty_Name__c FROM Geotab_Warranty_Option__c';
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		geotabWarranty = (List<Geotab_Warranty_Option__c>) scope;
		//System.debug(LoggingLevel.INFO, 'Geotab Contacts ->' + geotabContacts);

		for(Geotab_Warranty_Option__c gw : geotabWarranty) {
			sfdcGeotabWarrantyMap.put(gw.Geotab_Id__c, gw);
			//System.debug(LoggingLevel.INFO, 'sfdcGeotabContactMap execute ->' + sfdcGeotabContactMap);
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		//System.debug(LoggingLevel.INFO, 'Geotab Contacts API ->' + geotabContactsApi);
   		//System.debug(LoggingLevel.INFO, 'sfdcGeotabContactMap ->' + sfdcGeotabContactMap);

   		geotabWarrantyForInsert = new List<Geotab_Warranty_Option__c>();

		for(GeotabUtils.GeotabWarrantyOption gc : geotabWarrantyApi) {
			//System.debug(LoggingLevel.INFO, 'Geotab Contacts API ->' + gc);
			String gcApiId = String.valueOf(gc.Id);
			Geotab_Warranty_Option__c gcSFDC = sfdcGeotabWarrantyMap.get(gcApiId);

			if(gcSFDC == null) {
				Geotab_Warranty_Option__c newGC = new Geotab_Warranty_Option__c();
				newGC.Name = gc.Name;
				newGC.Geotab_Id__c = gcApiId;
				newGC.Geotab_Warranty_Name__c = gc.Name;
				//System.debug(LoggingLevel.INFO, 'Geotab Contacts New ->' + newGC);
				geotabWarrantyForInsert.add(newGC);
			}
		}

		if(geotabWarrantyForInsert.size() > 0)
			insert geotabWarrantyForInsert;
	}
	
}