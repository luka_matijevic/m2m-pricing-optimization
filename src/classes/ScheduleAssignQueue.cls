global class ScheduleAssignQueue implements Schedulable {

	global void execute(SchedulableContext sc) {
		Datetime now = Datetime.now();
		AssignQueue newAssign = new AssignQueue();
		Database.executeBatch(newAssign);
		Datetime nextSchedule = System.now().addMinutes(2);
		ScheduleAssignQueue.scheduleBatch(nextSchedule);

	}

	private static void scheduleBatch(Datetime dt){

		String cron_exp = dt.format('ss mm HH dd MM ? yyyy');
		ScheduleAssignQueue assign = new ScheduleAssignQueue();
		// Schedule the next job, and give it the system time so name is unique
		System.schedule(jobName(dt), cron_exp, assign);
	}

	private static String jobName(Datetime dt) {
		return 'CaseReopenJob' + dt.getTime();
	}
}