@isTest
public class AsyncCustomerBillUploadTest {
	static testMethod void testAsyncCustomerBillUpload() {
        Account acc = Test_Util.createAccount('Account Test');
        acc.Global__c = true;
        //Address addr = new Address(BillingAddress = 'Test', BillingCity = 'Zagreb', BillingCountry = 'Croatia', BillingCountryCode = 'DE', BillingPostalCode = '10000', BillingState = 'Zagreb', BillingStreet = 'Empty 46');
        acc.Billing_Cycle__c = '2';
        acc.VAT__c = 19;
        acc.BillingCity = 'Troisdorf';
        acc.BillingCountry = 'Germany';
        acc.BillingPostalCode = '53844';
        acc.BillingStreet = 'Junkersring 57';
        insert acc;
        Billing_Account__c bac = new Billing_Account__c(Payer__c = acc.Id, SM2M_ID__c = 0, Name = 'Test Billing Acc', SM2M_Type__c = 'Global', VAT__c = '19', Activation_token__c = 'NotNull', OFI_ID__c = '32232332');
        insert bac;
        Date d = system.today().toStartOfMonth().addMonths(-6);
        Daily_Bill__c db = new Daily_Bill__c(Date__c = d);
        insert db;
        Customer_Monthly_Bill__c cmb = new Customer_Monthly_Bill__c();
        cmb.Invoice_Number__c = '1214334322';
        cmb.Daily_Bill__c = db.Id;
        cmb.Billing_Account__c = bac.Id;
        String tmp = bac.OFI_ID__c;
        String tmpDate = db.Date__c.format().replace('.', '_');
        String sm2mType = bac.SM2M_Type__c;
        String tmpLocalOrGlobal = sm2mType != null && !sm2mType.equalsIgnoreCase('null') ? sm2mType.equalsIgnoreCase('Local') ? 'TEFDEL' : 'TEFDEG' : '';
        String sm2mId = bac.SM2M_ID__c != null ? String.valueOf(bac.SM2M_ID__c.intValue()) : '';
        String tmpBillingAcctAddition = String.format('{0}{1}', new String[] {sm2mId, !String.isEmpty(tmpLocalOrGlobal) ? '_' + tmpLocalOrGlobal : ''});
        String bodyString = 'test';
        Blob body = EncodingUtil.base64Decode(bodyString);
        cmb.PDFBlob__c = EncodingUtil.base64Encode(body);
        cmb.PDF_Bill_Link__c = Label.CustomerBillDownloadLink + tmp + '_' + tmpDate + '_' + tmpBillingAcctAddition + '_' + cmb.Invoice_Number__c + '.pdf';
        insert cmb;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UDOUtilsHTTPFakeCalloutsImpl());
        System.enqueueJob(new AsyncCustomerBillUpload());
        Test.stopTest();
    }
}