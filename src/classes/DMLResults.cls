public class DMLResults {

    public integer resultLimit    = 1000;
    public boolean includeSuccess = false;
    public LIST<DMLResults.Result> results {get; private set;}
    { results = new LIST<DMLResults.Result>(); }


	/**
	* constructor
	* @param lst a LIST of [Saveresult | Upsertresult |Deleteresult | Undeleteresult]
	* @param records the LIST of sobjects that were included in the dml statement.
	*/
    public DMLResults() {}
    public DMLResults(LIST<object> lst) { this(lst,null);}
    public DMLResults(LIST<object> lst,  LIST<sobject> records) {
        integer cnt=0;
        for (Object o : lst) {
            try {
                Result  r;
                if (o instanceof Database.Saveresult )         r= new Result((Database.Saveresult) o, records[cnt]);
                else if (o instanceof Database.Upsertresult )  r= new Result((Database.Upsertresult) o, records[cnt]);
                else if (o instanceof Database.Deleteresult)   r= new Result((Database.Deleteresult) o, records[cnt]);
                else if (o instanceof Database.Undeleteresult) r= new Result((Database.Undeleteresult) o, records[cnt]);
                else
                throw new InvalidResultException('Invalid DML Result.');
                if (includeSuccess || (!r.success) ) {
                if (results.size()<resultLimit) this.add(r);
                }
            }catch(exception ex) { system.debug(ex); }
            cnt++;
        }
    }

       /**
       * add additional exceptions
       * @param lst a LIST of [Database.Saveresult | Database.Upsertresult | Database.Deleteresult | Database.Undeleteresult]
       * @param records the LIST of sobjects that were included in the dml statement.

       */
    public void add(LIST<object> lst) { add(new DMLResults(lst,null)); }
    public void add(LIST<object> lst,  LIST<sobject> records) {
        add(new DMLResults(lst,records));
    }
    public void add(DMLResults.Result r) {
        if (results.size()<resultLimit)
            results.add(r);
    }
    public void add(DMLResults dmlr) {
        if (results.size()<resultLimit) {
        if (results.size()+dmlr.results.size()<resultLimit)
            results.addAll(dmlr.results);
        else {
            for (Result r : dmlr.results) {
            if (results.size()<resultLimit)
                this.add(r);
                else
                break;
            }
        }
        }
    }
    public void add(LIST<DMLResults.Result> lst) {
        if (results.size()<resultLimit)
        results.addAll(lst);
    }


    public class Result {
	    public string record    {get;set;}
	    public Id id            {get;set;}
	    public string errors    {get;set;}
	    public boolean success  {get;set;}
	    //public Database.Error[] errs    {get;set;}
	    //{ errs = new Database.Error[]{}; }
	    public Result(Database.Saveresult r) { this(r,null); }
	    public Result(Database.Saveresult r, sobject sObj) {
	        if (r.getId()!=null)
	            id=r.getId();
	        else if (sObj!=null && sObj.id!=null)
	            id=sObj.Id;
	        errors=string.valueOf(r.getErrors());
	        success=r.isSuccess();
	        record=(sObj!=null) ? string.valueOf(sObj) : null;
	    }
	    public Result(database.Deleteresult r) { this(r,null); }
	    public Result(database.Deleteresult r,sobject sObj) {
	        if (r.getId()!=null)
	            id=r.getId();
	        else if (sObj!=null && sObj.id!=null)
	            id=sObj.Id;
	        errors=string.valueOf(r.getErrors());
	        success=r.isSuccess();
	        record=(sObj!=null) ? string.valueOf(sObj) : null;
	    }
	    public Result(database.Upsertresult r) { this(r,null); }
	    public Result(database.Upsertresult r,sobject sObj) {
	        if (r.getId()!=null)
	            id=r.getId();
	        else if (sObj!=null && sObj.id!=null)
	            id=sObj.Id;
	        errors=string.valueOf(r.getErrors());
	        success=r.isSuccess();
	        record=(sObj!=null) ? string.valueOf(sObj) : null;
	    }
	    public Result(Database.Undeleteresult r) { this(r,null); }
	    public Result(Database.Undeleteresult r,sobject sObj) {
	        if (r.getId()!=null)
	            id=r.getId();
	        else if (sObj!=null && sObj.id!=null)
	            id=sObj.Id;
	        errors=string.valueOf(r.getErrors());
	        success=r.isSuccess();
	        record=(sObj!=null) ? string.valueOf(sObj) : null;
	    }
    }


    public string resultsToString() {
    string rtn;
    rtn='Total DML results: '+String.valueOf(results.size())+'\n';
    if (results.size()>0) {
        for(DMLResults.Result r : results) {
            if(r.record!=null)
                rtn+='Record: '+String.valueOf(r.record)+'\n';
            rtn+='Error: '+String.valueOf(r.errors)+'\n\n';
        }
    }
    return rtn;
    }


	public string resultsToHtml() {
	    string rtn;
	    rtn='Total DML results: '+String.valueOf(results.size())+'<br/>';
	    rtn+='<table border="1px"><tr style="background:gray;"><th>success</th><th>error(s)</th><th>record</th></tr>';
	    for(DMLResults.Result r : results) {
	        //rtn+=String.format('<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>',
	        rtn+=String.format('<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>',
	        	new string[]{/*String.valueOf(r.id),*/String.valueOf(r.success),r.errors,r.record});
	    }
	    rtn+='</table>';
	    return rtn;
	}

	 public void batchOnFinish(Id jobId) { batchOnFinish(jobId,true); }
	 public void batchOnFinish(Id jobId, boolean OnlyNotifyOnError) {
	    boolean html = true;
	    AsyncApexJob a = [  Select ApexClass.Name, Id, Status, NumberOfErrors, JobItemsProcessed,
	    					TotalJobItems, CreatedBy.Email
	            From AsyncApexJob where Id =:jobId
	            ];
	    if (a.NumberOfErrors>0 || results.size()>0 || (!OnlyNotifyOnError)) {

	        // Send an email to the Apex job's submitter notifying of job completion.
	        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	        String[] toAddresses = new String[] {a.CreatedBy.Email};
	        mail.setToAddresses(toAddresses);
	        mail.setSubject(a.ApexClass.Name+': ' + a.Status);


	        string s = 'The ' + a.ApexClass.Name + ' job processed '+a.TotalJobItems+' batches with '+a.NumberOfErrors+' failures.';
	        if(html) {
	            s+='<br/><br/>';
	            s+=resultsToHtml();
	            mail.setHtmlBody(s);
	        }
	        else {
	            s+='\n\n';
	            s+=resultsToString();
	            mail.setPlainTextBody(s);
	        }
	        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	    }
	}

	public class InvalidResultException extends Exception {}
}