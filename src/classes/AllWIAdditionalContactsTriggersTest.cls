@isTest
private class AllWIAdditionalContactsTriggersTest {
    static testMethod void method1(){
         //Test Data creation
        Account acc = new Account();
            acc.Name = 'AP Test 121';
            acc.Company_Email__c = 'company222@email.com';
            acc.Unique_Business_Number__c = 'UBN 12333';
            acc.BillingCountry = 'Ireland';
            acc.BillingStreet = 'Warwick street 115';
            acc.BillingCity = 'Cork';
            acc.BillingPostalCode = '57000';
            acc.Jurisdiction__c = 'Jurisdiction';
            acc.VO_Number__c = '4678398';
            acc.VAT__c = 12.50;
            acc.Phone = '1234567890';
            acc.Type = 'Customer';
            acc.Sales_Employee__c = 'John Voight';
            acc.Frame_Contract__c = false;
            acc.Website = 'www';
        	acc.Global__c = true;
            
        insert acc;
         Contact con = Test_Util.createContact(acc.id);
        
        con.Country__c = 'Germany';
        insert con;
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1)
			, recordtypeid = Cache.getRecordTypeId('Opportunity.Commercial_Global'),
            Description = 'Test description 20 characters'
		);
		insert opp;		
		
		
        Frame_Contract__c fc = new Frame_Contract__c();
        fc.contact__c = con.id;
        fc.customer__c = acc.id;
        fc.opportunity__c = opp.id;
         insert fc;          
        //Billing Account
        Billing_Account__c billingAccount = new Billing_Account__c();
        billingAccount.Account_Number__c = 1234;
        billingAccount.Name = 'Unit Test Billing Account';
        billingAccount.Billing_Language__c = 'English';
        billingAccount.Bank_Identifier__c = 'Bank Identifier';
        billingAccount.BIC__c = 8764321;
        billingAccount.Bank_Name__c = 'Bank Name';
        billingAccount.Bank_Address__c = 'Bank Address';
        billingAccount.Payment_Term__c = '30 Days';
        billingAccount.Payment_Method__c = 'Bank Transfer';
        billingAccount.Trial__c = true;
        billingAccount.Billable__c = false;
        billingAccount.Payer__c = acc.Id;
        billingAccount.VAT__c='0';
        insert billingAccount;
        
        WI_Additional_Contacts__c ac = new WI_Additional_Contacts__c();
        ac.name='Name';
        ac.Email_Address__c = 'test@test.com';
        ac.Billing_Account__c = billingAccount.id;
        insert ac;
    }

}