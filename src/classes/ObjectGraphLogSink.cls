global class ObjectGraphLogSink implements csog_tw1.Log.Sink {
	global void logMessage(LoggingLevel level, String message) {
		System.debug(level, message);
	}
}