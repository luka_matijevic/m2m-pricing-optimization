global class UpdateOutgoingMessagesScheduled implements Schedulable {

	// Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
	public static final String CRON_EXP = '0 0 1 * * ? *';

	/**
	 * @return jobId
	 */ 
	global static String scheduleIt() {
		return System.schedule('Update Outgoing Messages Scheduled', CRON_EXP, new UpdateOutgoingMessagesScheduled());
	}

	/**
	 * Executes the prebill fetching with a job size of 1. The date of the fetching is
	 * today() - offset, which is either determined from the custom setting or 2. 
	 */ 
	global void execute(SchedulableContext sc) {
	    List<csam_t1__Incoming_Message__c> incomingMsgs = [SELECT Id, csam_t1__Status__c, csam_t1__Outgoing_Message__c, csam_t1__Outgoing_Message__r.csam_t1__Status__c, CreatedDate FROM csam_t1__Incoming_Message__c WHERE CreatedDate = TODAY AND csam_t1__Status__c = 'Processed'];
	    Set<Id> outgoingIds = new Set<Id>();
	    for(csam_t1__Incoming_Message__c im : incomingMsgs) {
	        outgoingIds.add(im.csam_t1__Outgoing_Message__c);
	    }
	    
		List<csam_t1__Outgoing_Message__c> outgoingMsgs = [SELECT Id, csam_t1__Status__c, csam_t1__ObjectGraph_Callout_Handler__c, csam_t1__ObjectGraph_Callout_Handler__r.Name FROM csam_t1__Outgoing_Message__c WHERE Id in :outgoingIds AND csam_t1__ObjectGraph_Callout_Handler__r.Name = 'Update SIM Statuses'];
            
        if(outgoingMsgs != null && outgoingMsgs.size() > 0) {
            for(csam_t1__Outgoing_Message__c om : outgoingMsgs) {
                if(om.csam_t1__Status__c != null && om.csam_t1__Status__c.equalsIgnoreCase('Waiting for response')) {
                    om.csam_t1__Status__c = 'Response received';
                    update om;
            	}    
            }
        }
	}
}