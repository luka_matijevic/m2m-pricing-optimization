/**********************************************************
Petar Matkovic 18.11.2016.
Refreshes geotab order requests and fetch geotab order number for each order request that needs it
***********************************************************/
global class GeotabRefreshDataSchedule implements Schedulable {
	/**********************************************************
	VARIABLES AND PROPERTIES
	***********************************************************/
	private GEOTAB_Settings__c geotabSettingstmp;
	public GEOTAB_Settings__c geotabSettings{
		get{
			if (geotabSettingstmp == null){
				geotabSettingstmp = GEOTAB_Settings__c.getInstance();
			}
			return geotabSettingstmp;
		}
	}
	String userInfoEndPoint = geotabSettings.UserInfoEndPoint__c;
	String sessionIdResult;
	String apiKeyResult;
	List<GeotabUtils.GeotabAccount> geotabAccounts;
	List<Geotab_orders_log__c> logList;
	String getOrderStatusEndPoint;
	Integer refreshFrequency;
	/**********************************************************
	IMPLEMENTATION
	***********************************************************/
	public static void startPolling() {
		GeotabRefreshDataSchedule.scheduleTick(System.now().addSeconds(10));
	}

	public static void startPolling(Datetime dateIn) {
		GeotabRefreshDataSchedule.scheduleTick(dateIn.addSeconds(10));
	}

	global void execute(SchedulableContext sc) {
		//MyBatchClass b = new MyBatchClass();
		//database.executebatch(b);
		geotabAccounts = null;
		apiKeyResult = null;
		sessionIdResult = null;
		getOrderStatusEndPoint = geotabSettings.GetOnlineOrderStatusEndPoint__c;
		refreshFrequency = Integer.valueOf(geotabSettings.GeotabOrderNumberFetchingFrequency__c == null ? 1 : geotabSettings.GeotabOrderNumberFetchingFrequency__c);
		List<Order_Request__c> ordReqList = getOrderRequestsForRefresh();
		if (ordReqList != null && ordReqList.size() > 0){
			refreshGeotabApiVariables();
			if (geotabAccounts != null && geotabAccounts.size() > 0 && !String.isEmpty(apiKeyResult) && !String.isEmpty(sessionIdResult)){
				for (Order_Request__c ordReq : ordReqList){
					refreshOrderRequest(ordReq);
				}
				/**********************************************
				UPDATE ORDER REQUESTS
				***********************************************/
				update ordReqList;
				upsert logList;
			}
			else{
				//session, apikey or geotabaccounts fault
				String logMessage = 'Session: ' + sessionIdResult;
				logMessage = logMessage + ' Api Key: ' + apiKeyResult;
				logMessage = logMessage + ' Geotab Accounts: ' + String.valueOf(geotabAccounts);
				insertLog(logMessage);
			}
		}

		//if true, add hours; if false, add minutes
		boolean addhours = true;
		Integer numberToAdd = refreshFrequency;
		if (refreshFrequency > 24){
			numberToAdd = 60 * (24 / refreshFrequency);
		}

		Datetime nextTick = System.now();
		if (addhours){
			nextTick.addHours(numberToAdd);
		}
		else{
			nextTick.addMinutes(numberToAdd);
		}
		GeotabRefreshDataSchedule.scheduleTick(nextTick);

		System.abortJob(sc.getTriggerId());
	}

	private static void scheduleTick(Datetime dt) {
		String chron_exp = dt.format('ss mm HH dd MM ? yyyy');
		GeotabRefreshDataSchedule poller = new GeotabRefreshDataSchedule();
		// Schedule the next job, and give it the system time so name is unique
		System.schedule(jobName(dt), chron_exp, poller);
	}

	private static String jobName(Datetime dt) {
		return 'GeotabRefreshData_' + dt.getTime();
	}

	private List<Order_Request__c> getOrderRequestsForRefresh(){
		List<Order_Request__c> ordReqList = new List<Order_Request__c>();

		ordReqList = [SELECT id, Name 
					FROM Order_request__c 
					WHERE 	Order_posted_in_geotab__c = true and 
							Geotab_order_number__c = null];

		return ordReqList;
	}

	private void refreshOrderRequest(Order_Request__c orderRequestIn){
		String poNumber = orderRequestIn.Geotab_Purchase_Order_Number__c;
		if (!String.isEmpty(poNumber)){
			String tmpString = 'apiKey="' + apiKeyResult + '"&forAccount="' + geotabAccounts[0].AccountId + '"&sessionId="' + 
			sessionIdResult + '"&purchaseOrderNo="' + poNumber +'"';

			tmpString = EncodingUtil.urlEncode(tmpString, 'UTF-8');
			tmpString = tmpString.replace('+', '%20'); // Salesforce bug

			String requestTmp = getOrderStatusEndPoint + tmpString;

			HttpResponse resp = CaseUtils.executeRequest(requestTmp, 'GET', null, 32);
			if(resp != null && resp.getStatusCode() == 200) {
				//get geotab order number and update order request
				GeotabUtils.RootResponse responseJson = (GeotabUtils.RootResponse)JSON.deserialize(resp.getBody(), GeotabUtils.RootResponse.class);

				if (responseJson != null && responseJson.result.size()>0){
					orderRequestIn.Geotab_Order_Number__c = String.valueOf(responseJson.result[0].orderNo);
					if (orderRequestIn.Order_Posted_In_GEOTAB__c == false){
						orderRequestIn.Order_Posted_In_GEOTAB_Date__c = System.now();
					}
					orderRequestIn.Order_Posted_In_GEOTAB__c = true;
				}
			}

			createGeotabOrdersLog(orderRequestIn, resp.getBody(), String.valueOf(resp.getStatusCode()), requestTmp);
		}
	}

	private void refreshGeotabApiVariables(){
		Configurable_Sixt_helper__c csh = null;
		List<Configurable_Sixt_helper__c> customConfig = [SELECT Id, Name, Geotab_username__c, Geotab_password__c, Sixt_frame_contract__c, Sixt_account__c, Sixt_billing_account__c, Sixt_Opportunity_RecordType__c FROM Configurable_Sixt_helper__c WHERE Name = 'Configurable records' ORDER BY CreatedDate ASC];
		if (customConfig != null && customConfig.size() > 0)
		{
			csh = customConfig[0];
		}
		
		if (csh != null)
		{
			userInfoEndPoint = userInfoEndPoint + csh.Geotab_username__c + '&password=' + csh.Geotab_password__c;
		}
		String requestTmp = userInfoEndPoint;
		HttpResponse resp = CaseUtils.executeRequest(userInfoEndPoint, 'GET', null, 32);
		if(resp != null && resp.getStatusCode() == 200) {
			Map<String, GeotabUtils.AuthenticationResponseClass> authorizationResultMap = null;
			authorizationResultMap = (Map<String, GeotabUtils.AuthenticationResponseClass>)JSON.deserialize(resp.getBody(), Map<String, GeotabUtils.AuthenticationResponseClass>.class);
			if (authorizationResultMap != null) {
				String userIdResult;
				Integer shipToUserContactId = 0;
				apiKeyResult = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').userId : null;
				sessionIdResult = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').sessionId : null;
				geotabAccounts = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').accounts : null;
			}
		}
	}

	private void insertLog(String message){
		Geotab_orders_log__c gol = new Geotab_orders_log__c();
		gol.Description__c = message;
		insert gol;
	}

	private void createGeotabOrdersLog(Order_request__c orderRequestIn, String responseMessageIn, String responseStatusCodeIn, String requestIn){
		if (logList == null){
			logList = new List<Geotab_orders_log__c>();
		}

		Geotab_orders_log__c gol = new Geotab_orders_log__c();
		gol.Order_Request__c = orderRequestIn.Id;
		gol.Response_Status_Code__c = responseStatusCodeIn;
		gol.Original_response__c = responseMessageIn;
		gol.Request__c = requestIn;
		gol.Description__c = 'GEOTAB Order Number fetching';

		logList.add(gol);
	}
}