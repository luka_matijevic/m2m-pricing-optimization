/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCopyBundleController {

    static testMethod void copyBundleUnitTest() {
        // create a bundle
        TestHelper.createProductBundle();

        // set up the controller

        //
        // Start test
        //
        Test.startTest();
        PageReference pageRef = Page.copyBundle;
        Test.setCurrentPage(pageRef);

        // get the existing bundle and opportunity
        cscfga__Product_Bundle__c bundle = [SELECT id, Name FROM cscfga__Product_Bundle__c LIMIT 1];
        Map<Id, cscfga__Product_Configuration__c> configsMap = new Map<Id, cscfga__Product_Configuration__c>([SELECT Id, Name FROM cscfga__Product_Configuration__c]);

        ApexPages.StandardController sc = new ApexPages.StandardController(bundle);
        CopyBundleController controller = new CopyBundleController(sc);

        // set the opp id of the target bundle (simulate lookup selection by the page)

        controller.targetBundle.cscfga__Opportunity__c = controller.sourceBundle.cscfga__Opportunity__c;

        // copy it to the same opp

        controller.cloneBundle();
        //
        // Stop test
        //
        Test.stopTest();

        // assert that it really got copied

		Map<Id, cscfga__Product_Configuration__c> copiedConfigsMap = new Map<Id, cscfga__Product_Configuration__c>([SELECT Id, Name FROM cscfga__Product_Configuration__c WHERE Id NOT IN :configsMap.keyset()]);

		system.AssertEquals(configsMap.keyset().size(), copiedConfigsMap.keyset().size());

        // relinking and testing the relinking process will not be done here since that functionality is already covered by its own tests
    }
}