@isTest
private class DailyServiceSendReportsTest {
	
	@isTest(SeeAllData='true')
	public static void test_send() {

		Integer i = 0;
		List<DailyServiceEmails__c> emails = new List<DailyServiceEmails__c>();
		List<Report> reports = [SELECT Id, Name FROM Report LIMIT 2];
		List<DailyServiceReportIds__c> reportIds = new List<DailyServiceReportIds__c>();

		emails.add(new DailyServiceEmails__c(Name = 'TestEmail01', Email__c = 'ttteee@kdkd.com'));
		emails.add(new DailyServiceEmails__c(Name = 'TestEmail02', Email__c = 'beee@mee.com'));

		insert emails;

		for(Report r : reports){
				
				reportIds.add(new DailyServiceReportIds__c(Name ='Test'+ i, Report_ID__c = String.valueOf(r.Id)));
				i++;
		}

		insert reportIds;

		Test.startTest();

			DailyServiceSendReports ds = new DailyServiceSendReports('Daily');
			Database.executeBatch(ds);
					   		
		Test.stopTest();
		
		ds.fillAndSend(reports);

	}	

	@isTest(SeeAllData='true')
	public static void test_send2() {

		Integer i = 0;
		List<DailyServiceEmails__c> emails = new List<DailyServiceEmails__c>();
		List<Report> reports = [SELECT Id, Name FROM Report LIMIT 2];
		List<DailyServiceReportIds__c> reportIds = new List<DailyServiceReportIds__c>();

		emails.add(new DailyServiceEmails__c(Name = 'TestEmail01', Email__c = 'ttteee@kdkd.com'));
		emails.add(new DailyServiceEmails__c(Name = 'TestEmail02', Email__c = 'beee@mee.com'));

		insert emails;

		for(Report r : reports){
				
				reportIds.add(new DailyServiceReportIds__c(Name ='Test'+ i, Report_ID__c = String.valueOf(r.Id)));
				i++;
		}

		insert reportIds;

		Test.startTest();

			DailyServiceSendReports ds = new DailyServiceSendReports('Weekly');
			Database.executeBatch(ds);
					   		
		Test.stopTest();
		
		ds.fillAndSend(reports);

	}	

	@isTest(SeeAllData='true')
	public static void test_send3() {

		Integer i = 0;
		List<DailyServiceEmails__c> emails = new List<DailyServiceEmails__c>();
		List<Report> reports = [SELECT Id, Name FROM Report LIMIT 2];
		List<DailyServiceReportIds__c> reportIds = new List<DailyServiceReportIds__c>();

		emails.add(new DailyServiceEmails__c(Name = 'TestEmail01', Email__c = 'ttteee@kdkd.com'));
		emails.add(new DailyServiceEmails__c(Name = 'TestEmail02', Email__c = 'beee@mee.com'));

		insert emails;

		for(Report r : reports){
				
				reportIds.add(new DailyServiceReportIds__c(Name ='Test'+ i, Report_ID__c = String.valueOf(r.Id)));
				i++;
		}

		insert reportIds;

		Test.startTest();

			DailyServiceSendReports ds = new DailyServiceSendReports('Monthly');
			Database.executeBatch(ds);
					   		
		Test.stopTest();
		
		ds.fillAndSend(reports);

	}	
}