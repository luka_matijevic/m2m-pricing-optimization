public with sharing class ServiceTriggerDelegate extends TriggerHandler.DelegateBase {
	EmailTemplate emailTemplate;
	List<String> emailRecipient;
	Messaging.SingleEmailMessage email;
    Map<Id, Csord__order__c> serviceOrderMap;

	public override void prepareBefore() {}

	public override void prepareAfter() {
		emailTemplate = [SELECT Id, Name, Subject, HtmlValue FROM EmailTemplate where DeveloperName = 'APN_Order_Notification'];
		
		emailRecipient = new List<String>();
		List<APN_Order_Mail_Addresses__c> emailAdressList = APN_Order_Mail_Addresses__c.getAll().values();
		for (APN_Order_Mail_Addresses__c apnEmailAddress : emailAdressList){
			if (apnEmailAddress.Email_Address__c != null){
				emailRecipient.add(apnEmailAddress.Email_Address__c);
			}
		}

        //Get list of services and their orders and put them into map
        //Map is used for getting Order link and Name for email        
        List<csord__Service__c> serviceOrderRequests = [SELECT Id, csord__Order_Request__r.Id from csord__Service__c where id in :Trigger.new];
        List<Id> orderRequestsIds = new List<Id>();
        for (csord__Service__c serv : serviceOrderRequests){
            if (serv.csord__Order_Request__c != null)
                orderRequestsIds.add(serv.csord__Order_Request__c);
        }
        List<Csord__order__c> orderList = [SELECT id, name, csord__Order_Request__r.Id FROM Csord__Order__c where Csord__Order_Request__c in :orderRequestsIds];
	    
        serviceOrderMap = new Map<Id, Csord__order__c>();
        if (orderList != null && orderList.size() > 0){
            for (csord__Service__c serv : serviceOrderRequests){
                for (Csord__order__c order : orderList){
                    if (order.csord__Order_Request__c == serv.csord__Order_Request__c){
                        serviceOrderMap.put(serv.Id, order);
                        break;
                    }
                }
            }
        }
        //End of service-order map creating   
    }

	public override void beforeInsert(sObject o) {
        // Apply before insert logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method    
    }

    public override void beforeUpdate(sObject old, sObject o) {
    	// Apply before update logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void beforeDelete(sObject o) {
    	// Apply before delete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }


    public override void afterInsert(sObject o) {
        // Apply after insert logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
        Csord__service__c ordService = (Csord__service__c)o;
        if (ordService.csord__Identification__c == 'CS-APN / VPN'){
        	//Send mail
        	if (emailTemplate != null){
        		email = new Messaging.SingleEmailMessage();
        		email.setToAddresses(emailRecipient);

                csord__order__c order = serviceOrderMap.get(ordService.Id);

        		String mailBody = emailTemplate.HtmlValue;
        		String linkService = '<a href="'+URL.getSalesforceBaseUrl().toExternalForm() + '/' +ordService.Id + '">' + ordService.Name + '</a>';
        		String linkOrder = '<a href="'+URL.getSalesforceBaseUrl().toExternalForm() + '/' + String.valueOf(order.Id) + '">' + String.valueOf(order.Name) + '</a>';
        		mailBody = mailBody.replace('{!csord__Service__c.CreatedDate}', String.valueOf(ordService.CreatedDate));
        		mailBody = mailBody.replace('{!csord__Service__c.Name}', linkService);
        		mailBody = mailBody.replace('{!csord__Service__c.csord__Order__c}', linkOrder);
        		email.setHtmlBody(mailBody);
        		email.setSubject(emailTemplate.Subject);
        	}
        }
    }

    public override void afterUpdate(sObject old, sObject o) {
        // Apply after update logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterDelete(sObject old) {
        // Apply after delete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method        
    }

    public override void afterUndelete(sObject old) {
        // Apply after undelete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method        
    }

    public override void finish() {
    	if (email != null){
    		try{
    			Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    		}catch(Exception ex){
    			System.debug(ex);
    		}
    	}
    }
}