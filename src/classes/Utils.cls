//------------------------------------------------------------------------------
// Utility methods
//------------------------------------------------------------------------------
public without sharing class Utils
{
    private static List<String> monthNames = new List<String>{
        'January','February','March','April','May','June','July','August','September','October','November','December'
    };

    //--------------------------------------------------------------------------
    // Add two decimal values
    //--------------------------------------------------------------------------
    public static Decimal addValues(Decimal value1, Decimal value2)
    {
        Decimal returnValue = 0;
        if (value1 != null) {
            returnValue += value1;
        }
        if (value2 != null) {
            returnValue += value2;
        }
        return returnValue;
    }

    //--------------------------------------------------------------------------
    // Return null if value is zero
    //--------------------------------------------------------------------------
    public static Decimal nullIfZero(Decimal value)
    {
        if (value != null && value == 0) {
            return null;
        }
        return value;
    }

    //--------------------------------------------------------------------------
    // Count configurations looking at the tariff
    //--------------------------------------------------------------------------
    public static Integer countConfigurationsOnTariff(Id tariffId)
    {
        Integer returnValue = 0;
        for (List<cscfga__Attribute__c> attributes
                : [ SELECT cscfga__Value__c
                    FROM cscfga__Attribute__c
                    WHERE Name = :Constants.ATTRIBUTE_NAME_TARIFF])
        {
            for (cscfga__Attribute__c attribute : attributes)
            {
                if (attribute.cscfga__Value__c == tariffId)
                {
                    returnValue++;
                }
            }
        }
        return returnValue;
    }

    public static Decimal zeroIfNull(Decimal value) {
        return value != null ? value : 0.0;
    }

    public static Integer zeroIfNull(Integer value) {
        return value != null ? value : 0;
    }

    //--------------------------------------------------------------------------
    // Returns month name by month Index (1 - based)
    //--------------------------------------------------------------------------
    public static string getMonthName(Integer monthIndex)
    {
        String monthName;
        if (monthIndex >= 1 && monthIndex <= 12) {
            monthName = monthNames[monthIndex-1];
        }
        return monthName;
    }
    
    /*
    * Method to sort a list based on passed Field Name & sort order
    */
    public static List<sObject> sortList(List<Sobject> items, String sortField, String order){

        List<Sobject> resultList = new List<Sobject>();

        //Create a map that can be used for sorting 
        Map<object, List<Sobject>> objectMap = new Map<object, List<Sobject>>();
           
        for(Sobject ob : items){
                if(objectMap.get(ob.get(sortField)) == null){  // For non Sobject use obj.ProperyName
                    objectMap.put(ob.get(sortField), new List<Sobject>()); 
                }
                objectMap.get(ob.get(sortField)).add(ob);
        }       
        //Sort the keys
        List<object> keys = new List<object>(objectMap.keySet());
        keys.sort();

        for(object key : keys){ 
            resultList.addAll(objectMap.get(key)); 
        }

        //Apply the sorted values to the source list
        items.clear();
        if(order.toLowerCase() == 'asc'){
            for(Sobject ob : resultList){
                items.add(ob); 
            }
        }else if(order.toLowerCase() == 'desc'){
            for(integer i = resultList.size()-1; i >= 0; i--){
                items.add(resultList[i]);  
            }
        }
        return items;
    }

    // Get a comma separated SObject Field list 
  public static String getSobjectFields (String so) {
    String fieldString;
    String packagePrefix = '';
    //packagePrefix = (Utility.packageNamespace == '') ? '' : Utility.packageNamespace + '__';
    SObjectType sot = Schema.getGlobalDescribe().get(packagePrefix + so);
    
    List<Schema.SObjectField> fields = sot.getDescribe().fields.getMap().values();
    
    fieldString = fields[0].getDescribe().LocalName;
    for (Integer i = 1; i < fields.size(); i++) {
      fieldString += ',' + fields[i].getDescribe().LocalName;
    }
    
    return fieldString;
  }
}