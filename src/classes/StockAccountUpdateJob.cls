//------------------------------------------------------------------------------
// Apex job for updating Stock Account__c field
//------------------------------------------------------------------------------
global class StockAccountUpdateJob implements Database.Batchable<SObject> {
	
	static final String initialQuery =
		'SELECT Id, Account__c, Third_Upload_File__r.Upload_to_SM2M_Status__c, Third_Upload_File__r.Account__c, Third_Upload_File__r.Order_Request__r.Account__c ' +
		'FROM Stock__c ' +
		'WHERE Third_Upload_File__c != null';

	//--------------------------------------------------------------------------
	// Start of batch apex
	//--------------------------------------------------------------------------
	global Database.QueryLocator start(Database.BatchableContext bc) {
		return Database.getQueryLocator(initialQuery);
	}

	//--------------------------------------------------------------------------
	// Execute of batch apex
	//--------------------------------------------------------------------------
	global void execute(Database.BatchableContext bc, List<SObject> scope) {
		
		for (Stock__c sim : (List<Stock__c>) scope) {
			if (sim.Third_Upload_File__r.Upload_to_SM2M_Status__c == 'Uploaded') {
				if (!String.isEmpty(sim.Third_Upload_File__r.Account__c)) {
					sim.Account__c = sim.Third_Upload_File__r.Account__c;
				} else if (!String.isEmpty(sim.Third_Upload_File__r.Order_Request__r.Account__c)) {
					sim.Account__c = sim.Third_Upload_File__r.Order_Request__r.Account__c;
				}
			} else {
				sim.Account__c = null;
			}
		}
		update scope;
	}

	//--------------------------------------------------------------------------
	// Finish batch apex
	//--------------------------------------------------------------------------
	global void finish(Database.BatchableContext bc) {
		// empty
	}
}