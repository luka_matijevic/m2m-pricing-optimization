global class DailyBillSchedule implements Schedulable {

	// Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
	public static final String CRON_EXP = '0 0 3 * * ? *';
	public static final Integer DEFAULT_BILLING_OFFSET = 2;

	/**
	 * @return jobId
	 */ 
	global static String scheduleIt() {
		return System.schedule('Daily Bill Generation', CRON_EXP, new DailyBillSchedule());
	}

	/**
	 * Executes the prebill fetching with a job size of 1. The date of the fetching is
	 * today() - offset, which is either determined from the custom setting or 2. 
	 */ 
	global void execute(SchedulableContext sc) {
		Billing_Settings__c billingSettings = Billing_Settings__c.getOrgDefaults();
		Integer offset = DEFAULT_BILLING_OFFSET;
		if (billingSettings != null && billingSettings.Billing_Cycle_Offset__c != null) {
			offset = billingSettings.Billing_Cycle_Offset__c.intValue();
		}
		Date forDate = System.today().addDays(-offset);
		Database.executeBatch(new BatchPrebillFetcher(forDate), 1);
	}
}