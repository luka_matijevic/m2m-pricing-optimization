global with sharing class OFISendEmail implements ISendEmail
{
	//--------------------------------------------------------------------------
	// Private members
	//--------------------------------------------------------------------------
	Messaging.EmailFileAttachment attachment;

	//--------------------------------------------------------------------------
	// Set attachment
	//--------------------------------------------------------------------------
	global void setAttachment(String attachmentName, String attachmentBody)
	{
		attachment = new Messaging.EmailFileAttachment();
		attachment.setFileName(attachmentName);
		attachment.setBody(Blob.valueOf(attachmentBody));
	}

	//--------------------------------------------------------------------------
	// Send email
	//--------------------------------------------------------------------------
	global void send()
	{
		Email_Settings__c emailSettings =
				[ SELECT Email__c, Subject__c, Body__c
				  FROM Email_Settings__c
				  WHERE Name = :Constants.PROVISION_IN_OFI ];
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		email.setSubject(emailSettings.Subject__c);
		email.setToAddresses(new String[] {emailSettings.Email__c});
		email.setPlainTextBody(emailSettings.Body__c);
		if (attachment != null)
		{
			email.setFileAttachments(new Messaging.EmailFileAttachment[] {attachment});
		}

		Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
	}
}