public without sharing class PostCopyConnectivityConfigurations
{
	//--------------------------------------------------------------------------
	// Members
	//--------------------------------------------------------------------------
	Id bundleId;
	Id accountId;
	String accountName;
	List<cscfga__Product_Configuration__c> configurations;

	//--------------------------------------------------------------------------
	// Constructor for fixing configurations
	//--------------------------------------------------------------------------
	public PostCopyConnectivityConfigurations(Id bundleId)
	{
		this.bundleId = bundleId;

		//
		// Retrieve product bundle and account id
		//
		cscfga__Product_Bundle__c productBundle =
			[ SELECT Id, cscfga__Opportunity__r.Account.Id, Name,
			  cscfga__Opportunity__r.Account.Name
			  FROM cscfga__Product_Bundle__c
			  WHERE Id = :bundleId ];
		this.accountId = productBundle.cscfga__Opportunity__r.Account.Id;
		this.accountName = productBundle.cscfga__Opportunity__r.Account.Name;

		//
		// Retrieve connect ivity configurations
		// S.S. added bundle id to filter only PCs in copied bundle
		//
		configurations =
			[ SELECT Id, Name, Original_Product_Name__c,
			  cscfga__Product_Definition__r.Name,
			  ( SELECT Id, Name, cscfga__Value__c
				FROM cscfga__Attributes__r
				WHERE Name
				IN ('Tariff', 'Additional Tariff Option',
					'Supplementary Service', 'Requires Commercial Approval') )
			  FROM cscfga__Product_Configuration__c
			  WHERE cscfga__Product_Bundle__c = :bundleId AND
			  	(
				  	Original_Product_Name__c
				  	IN ('Connectivity', 'Tariff Additinal Service',
					  'Tariff Supplementary Service')
				  	OR cscfga__Product_Definition__r.Name
				  	IN ('Connectivity', 'Tariff Additional Service',
					  'Tariff Supplementary Service')
				)
			];
	}

	//--------------------------------------------------------------------------
	// Create new tariff clones and connect product configurations with it
	//--------------------------------------------------------------------------
	public void doExecute()
	{
		List<cscfga__Attribute__c> tariffAttributes =
			new List<cscfga__Attribute__c>();
		List<cscfga__Attribute__c> tariffOptionAttributes =
			new List<cscfga__Attribute__c>();
		List<cscfga__Attribute__c> approvalAttributes =
			new List<cscfga__Attribute__c>();
		Set<Id> tariffIds = new Set<Id>();
		Map<Id, cscfga__Attribute__c> approvalTariffMap =
			new Map<Id, cscfga__Attribute__c>();

		//
		// Retrieve original tariffs with parents
		//
		readConfigurations(
				tariffAttributes,
				tariffOptionAttributes,
				approvalAttributes,
				approvalTariffMap,
				tariffIds,
				configurations);

		List<ITariff> originalTariffs = Factory.createTariffs(tariffIds);

		system.debug(LoggingLevel.WARN, 'Original Tariffs: ' + originalTariffs);

		Map<Id, ITariff> parentTariffMap = createParentTariffMap(originalTariffs);

		List<ITariff> clonedTariffs =
			cloneAndStoreTariffs(originalTariffs, parentTariffMap);

		system.debug(LoggingLevel.WARN, 'Cloned Tariffs: ' + clonedTariffs);

		Map<Id, Id> idMap = new Map<Id, Id>();
		Map<Id, ITariff> clonedTariffMap = new Map<Id, ITariff>();

		createIdMaps(idMap, clonedTariffMap, originalTariffs, clonedTariffs);

		//
		// Connect connectivity configurations with cloned tariffs
		//
		connectAttributesToTariffs(
				tariffAttributes,
				tariffOptionAttributes,
				approvalAttributes,
				approvalTariffMap,
				idMap,
				clonedTariffMap);
	}

	//--------------------------------------------------------------------------
	// Read attributes from configurations
	//--------------------------------------------------------------------------
	private static void readConfigurations(
				List<cscfga__Attribute__c> tariffAttributes,
				List<cscfga__Attribute__c> tariffOptionAttributes,
				List<cscfga__Attribute__c> approvalAttributes,
				Map<Id, cscfga__Attribute__c> approvalTariffMap,
				Set<Id> tariffIds,
				List<cscfga__Product_Configuration__c> configurations)
	{
		for (cscfga__Product_Configuration__c configuration : configurations)
		{
			cscfga__Attribute__c tariffAttribute = null;
			cscfga__Attribute__c approvalAttribute = null;
			for (cscfga__Attribute__c attribute : configuration.cscfga__Attributes__r)
			{
				if (attribute.Name == 'Tariff')
				{
					tariffAttribute = attribute;
					tariffAttributes.add(attribute);
					tariffIds.add(attribute.cscfga__Value__c);
				}
				else if (attribute.Name == 'Additional Tariff Option'
						 || attribute.Name == 'Supplementary Service')
				{
					tariffOptionAttributes.add(attribute);
				}
				else if (attribute.Name == 'Requires Commercial Approval')
				{
					approvalAttribute = attribute;
					approvalAttributes.add(attribute);
				}
			}
			//
			// Connect approval with tariff
			//
			if (tariffAttribute != null && approvalAttribute != null)
			{
				approvalTariffMap.put(approvalAttribute.Id, tariffAttribute);
			}
		}
	}

	//--------------------------------------------------------------------------
	// Returns parent tariffs
	//--------------------------------------------------------------------------
	private static Map<Id, ITariff> createParentTariffMap(List<ITariff> originalTariffs)
	{
		Set<Id> parentTariffIds = new Set<Id>();
		for (ITariff tariff : originalTariffs)
		{
			if (tariff.getTariff().Parent_Tariff__c != null)
			{
				parentTariffIds.add(tariff.getTariff().Parent_Tariff__c);
			}
		}

		List<ITariff> parentTariffs = Factory.createTariffs(parentTariffIds);
		Map<Id, ITariff> parentTariffMap = new Map<Id, ITariff>();
		for (ITariff tariff : parentTariffs)
		{
			parentTariffMap.put(tariff.getTariff().Id, tariff);
		}

		return parentTariffMap;
	}

	//--------------------------------------------------------------------------
	// Store cloned tariffs
	//--------------------------------------------------------------------------
	private List<ITariff> cloneAndStoreTariffs(
		List<ITariff> originalTariffs,
		Map<Id, ITariff> parentTariffMap)
	{
		List<ITariff> clonedTariffs = new List<ITariff>();
		IInsertHelper insertHelper = new InsertHelper();
		for (ITariff tariff : originalTariffs)
		{
			ITariff parentTariff = null;
			ITariff clonedTariff = null;
			if (tariff.getTariff().Parent_Tariff__c != null)
			{
				parentTariff = parentTariffMap.get(tariff.getTariff().Parent_Tariff__c);
				tariff.deepMerge(parentTariff);
				clonedTariff = tariff.deepClone();
			}
			else
			{
				parentTariff = tariff;
				clonedTariff = tariff.deepClone();
			}
			clonedTariff.prepareChild(parentTariff, accountId, accountName, tariff.getTariff().Contract_Term__c);
			clonedTariff.store(insertHelper);
			clonedTariffs.add(clonedTariff);
		}
		insertHelper.store();
		return clonedTariffs;
	}

	//--------------------------------------------------------------------------
	// Create tariffs map from original and cloned tariffs
	//--------------------------------------------------------------------------
	private static void createIdMaps(
			Map<Id, Id> idMap,
			Map<Id, ITariff> clonedTariffMap,
			List<ITariff> originalTariffs,
			List<ITariff> clonedTariffs)
	{
		//
		// Loop tariffs
		//
		Iterator<ITariff> clonedIterator = clonedTariffs.iterator();
		for (ITariff originalTariff : originalTariffs)
		{
			ITariff clonedTariff = clonedIterator.next();
			//
			// Loop tariff options
			//
			Iterator<ITariffOption> clonedTOIterator =
				clonedTariff.getTariffOptions().iterator();
			for (ITariffOption originalTariffOption : originalTariff.getTariffOptions())
			{
				ITariffOption clonedTariffOption = clonedTOIterator.next();
				//
				// Map tariff option
				//
				idMap.put(
						originalTariffOption.getTariffOption().Id,
						clonedTariffOption.getTariffOption().Id);
			}
			//
			// Map tariff
			//
			idMap.put(
					originalTariff.getTariff().Id,
					clonedTariff.getTariff().Id);
			clonedTariffMap.put(clonedTariff.getTariff().Id, clonedTariff);
		}
	}

	//--------------------------------------------------------------------------
	// Connect attributes to cloned tariffs
	//--------------------------------------------------------------------------
	private static void connectAttributesToTariffs(
			List<cscfga__Attribute__c> tariffAttributes,
			List<cscfga__Attribute__c> tariffOptionAttributes,
			List<cscfga__Attribute__c> approvalAttributes,
			Map<Id, cscfga__Attribute__c> approvalTariffMap,
			Map<Id, Id> idMap,
			Map<Id, ITariff> clonedTariffMap)
	{
		//
		// Connect tariff attributes to cloned tariffs
		//
		for (cscfga__Attribute__c attribute : tariffAttributes)
		{
			system.debug(LoggingLevel.WARN, 'Tariff Attribute old value: ' + attribute.cscfga__Value__c);
			attribute.cscfga__Value__c =
				idMap.get(attribute.cscfga__Value__c);
			system.debug(LoggingLevel.WARN, 'Tariff Attribute new value: ' + attribute.cscfga__Value__c);
		}
		//
		// Connect tariff option attributes to cloned tariff options
		//
		for (cscfga__Attribute__c attribute : tariffOptionAttributes)
		{
			system.debug(LoggingLevel.WARN, 'Tariff Option Attribute old value: ' + attribute.cscfga__Value__c);
			attribute.cscfga__Value__c =
				idMap.get(attribute.cscfga__Value__c);
			system.debug(LoggingLevel.WARN, 'Tariff Option Attribute new value: ' + attribute.cscfga__Value__c);
		}

		//
		// Requires commercial approval
		//
		for (cscfga__Attribute__c attribute : approvalAttributes)
		{
			String value = 'Yes';
			cscfga__Attribute__c tariffAttribute =
					approvalTariffMap.get(attribute.Id);
			if (tariffAttribute != null)
			{
				ITariff tariff = clonedTariffMap.get(tariffAttribute.cscfga__Value__c);
				if (tariff != null
					&& tariff.getTariff().Status__c == Constants.TARIFF_STATUS_APPROVED)
				{
					value = 'No';
				}
			}
			attribute.cscfga__Value__c = value;
		}

		//
		// Update attributes
		//
		List<cscfga__Attribute__c> attributes =
			new List<cscfga__Attribute__c>();
		attributes.addAll(tariffAttributes);
		attributes.addAll(tariffOptionAttributes);
		attributes.addAll(approvalAttributes);

		system.debug(LoggingLevel.WARN, 'Attributes to update: ' + attributes);

		update attributes;
	}
}