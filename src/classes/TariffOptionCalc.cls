//------------------------------------------------------------------------------
// Calculations for tariff option calc
//------------------------------------------------------------------------------
public without sharing class TariffOptionCalc
{
	//--------------------------------------------------------------------------
	// Private members
	//--------------------------------------------------------------------------
	private Decimal dataOverUsage;
	private Decimal textOverUsage;
	private Decimal voiceOverUsage;
	private Decimal oneTimeTargetFeePerCustomer;
	private Decimal oneTimeTargetFeePerVolume;
	private Decimal oneTimeTargetFeeTotal;
	private Decimal recurringTargetFeePerCustomer;
	private Decimal recurringTargetFeePerVolume;
	private Decimal recurringTargetFeeTotal;

	//--------------------------------------------------------------------------
	// Constructor calculates everything
	//--------------------------------------------------------------------------
	public TariffOptionCalc(ITariffOption tariffOption)
	{
		//
		// Look in the rate cards
		//
		dataOverUsage = 0;
		textOverUsage = 0;
		voiceOverUsage = 0;
		for (IRateCard rateCard : tariffOption.getRateCards())
		{
			Rate_Card__c rc = rateCard.getRateCard();
			if (rc.Clock_Rate__c == 'MB'
				|| rc.Clock_Rate__c == 'kB' || rc.Clock_Rate__c == '1 kB' //Adam Mehtic added (1 kB)
				|| rc.Clock_Rate__c == '10 kB')
			{
				dataOverUsage = addOverUsage(dataOverUsage, rc);
			}
			else if (rc.Clock_Rate__c == 'SMS' ||  rc.Clock_Rate__c == '1 SMS')
			{
				textOverUsage = addOverUsage(textOverUsage, rc);
			}
			else if (rc.Clock_Rate__c == 'Min'
					|| rc.Clock_Rate__c == 'Sec'
					|| rc.Clock_Rate__c == '60/60'
					|| rc.Clock_Rate__c == '1 Min')
			{
				voiceOverUsage = addOverUsage(voiceOverUsage, rc);
			}
		}

		//
		// Look in the service fees
		//
		oneTimeTargetFeePerCustomer = 0;
		recurringTargetFeePerCustomer = 0;
		oneTimeTargetFeePerVolume = 0;
		recurringTargetFeePerVolume = 0;
		oneTimeTargetFeeTotal = 0;
		recurringTargetFeeTotal = 0;
		List<IServiceFee> serviceFees = tariffOption.getServiceFees();
		for (IServiceFee serviceFee : serviceFees)
		{
			Service_Fee__c sf = serviceFee.getServiceFee();
			if (sf.Charged_Per__c == 'Customer')
			{
				oneTimeTargetFeePerCustomer =
					Utils.addValues(oneTimeTargetFeePerCustomer, sf.One_Time_Target_Fee__c);
				recurringTargetFeePerCustomer =
					Utils.addValues(recurringTargetFeePerCustomer, sf.Recurring_Target_Fee__c);
			}
			else if (sf.Charged_Per__c == 'Volume')
			{
				oneTimeTargetFeePerVolume =
					Utils.addValues(oneTimeTargetFeePerVolume, sf.One_Time_Target_Fee__c);
				recurringTargetFeePerVolume =
					Utils.addValues(recurringTargetFeePerVolume, sf.Recurring_Target_Fee__c);
			}
		}
		recurringTargetFeePerVolume =
			Utils.addValues(recurringTargetFeePerVolume,
					tariffOption.getTariffOption().Recurring_Target_Fee__c);
		//
		// Totals
		//
		oneTimeTargetFeeTotal = oneTimeTargetFeePerCustomer;
		recurringTargetFeeTotal = recurringTargetFeePerCustomer;
		if (tariffOption.getQuantity() != null)
		{
			oneTimeTargetFeeTotal +=
				oneTimeTargetFeePerVolume * tariffOption.getQuantity();
			recurringTargetFeeTotal +=
				recurringTargetFeePerVolume * tariffOption.getQuantity();
			dataOverUsage = dataOverUsage * tariffOption.getQuantity();
			textOverUsage = textOverUsage * tariffOption.getQuantity();
			voiceOverUsage = voiceOverUsage * tariffOption.getQuantity();
		}

		dataOverUsage = Utils.nullIfZero(dataOverUsage);
		textOverUsage = Utils.nullIfZero(textOverUsage);
		voiceOverUsage = Utils.nullIfZero(voiceOverUsage);
		oneTimeTargetFeePerCustomer = Utils.nullIfZero(oneTimeTargetFeePerCustomer);
		oneTimeTargetFeePerVolume = Utils.nullIfZero(oneTimeTargetFeePerVolume);
		oneTimeTargetFeeTotal = Utils.nullIfZero(oneTimeTargetFeeTotal);
		recurringTargetFeePerCustomer = Utils.nullIfZero(recurringTargetFeePerCustomer);
		recurringTargetFeePerVolume = Utils.nullIfZero(recurringTargetFeePerVolume);
		recurringTargetFeeTotal = Utils.nullIfZero(recurringTargetFeeTotal);
	}

	//--------------------------------------------------------------------------
	// Get expected data over usage
	//--------------------------------------------------------------------------
	public Decimal getDataOverUsage()
	{
		return dataOverUsage;
	}

	//--------------------------------------------------------------------------
	// Get expected text over usage
	//--------------------------------------------------------------------------
	public Decimal getTextOverUsage()
	{
		return textOverUsage;
	}

	//--------------------------------------------------------------------------
	// Get expected voice over usage
	//--------------------------------------------------------------------------
	public Decimal getVoiceOverUsage()
	{
		return voiceOverUsage;
	}

	//--------------------------------------------------------------------------
	// Get one time target fee per customer
	//--------------------------------------------------------------------------
	public Decimal getOneTimeTargetFeePerCustomer()
	{
		return oneTimeTargetFeePerCustomer;
	}

	//--------------------------------------------------------------------------
	// Get one time target fee per volume
	//--------------------------------------------------------------------------
	public Decimal getOneTimeTargetFeePerVolume()
	{
		return oneTimeTargetFeePerVolume;
	}

	//--------------------------------------------------------------------------
	// Get one time target fee total
	//--------------------------------------------------------------------------
	public Decimal getOneTimeTargetFeeTotal()
	{
		return oneTimeTargetFeeTotal;
	}

	//--------------------------------------------------------------------------
	// Recurring target fee per customer
	//--------------------------------------------------------------------------
	public Decimal getRecurringTargetFeePerCustomer()
	{
		return recurringTargetFeePerCustomer;
	}

	//--------------------------------------------------------------------------
	// Recurring target fee per volume
	//--------------------------------------------------------------------------
	public Decimal getRecurringTargetFeePerVolume()
	{
		return recurringTargetFeePerVolume;
	}

	//--------------------------------------------------------------------------
	// Recurring target fee total
	//--------------------------------------------------------------------------
	public Decimal getRecurringTargetFeeTotal()
	{
		return recurringTargetFeeTotal;
	}

	//--------------------------------------------------------------------------
	// Add over usage of the rate card
	//--------------------------------------------------------------------------
	private static Decimal addOverUsage(Decimal overUsage, Rate_Card__c rc)
	{
		if (rc.Expected_Usage__c != null
			&& rc.Target_Usage_Fee__c != null)
		{
			overUsage += rc.Expected_Usage__c * rc.Target_Usage_Fee__c;
		}
		return overUsage;
	}
}