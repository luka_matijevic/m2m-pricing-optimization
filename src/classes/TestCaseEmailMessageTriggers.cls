/*
* Description: Test class for CaseEmailMessageTriggers & related controller
*/
@isTest (SeeAllData=false)
class TestCaseEmailMessageTriggers{

    /*
    * Validate Case email call.
    */
    testMethod static void validateCaseEmail() {
    
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers;
        // Insert account
        Account acc = new Account(Company_Email__c = 'company@email.com',
        Unique_Business_Number__c = 'UBN 123',
        BillingCountry = 'Ireland',
        BillingStreet = 'Warwick street 15',
        BillingCity = 'Cork',
        Jurisdiction__c = 'Jurisdiction',
        VO_Number__c = '4567898',
        VAT__c = 12.50,
        Phone = '12345',
        Type = 'Customer',
        BillingPostalCode = '57000',
        Sales_Employee__c = 'John Voight',
        Frame_Contract__c = false,
        Website = 'www',
        Name = 'CS Service Test Acct'
        );
        insert acc;
        
        // Insert Case records
        List<Case> caseList = new List<Case>();
        caseList.add(new Case(Status='Open', Subject='Test Demo111', SLA_Time__c='24', Type='Standard', Origin='Email', Priority='High'));    
        insert caseList;
        
        notriggers.Flag__c = false;
        update notriggers;
        
        Test.StartTest();
        
        // Create CaseEmail
        EmailMessage newCaseEmail = new EmailMessage(FromAddress = 'someone@test.com', Incoming = True, ToAddress= 'someone@salesforce.com', 
                                    Subject = 'Test email', TextBody = 'Test Data', ParentId = caseList[0].Id); 
        insert newCaseEmail; 
        List<FeedItem> fItem  = [Select Id, ParentId from FeedItem where ParentId=: caseList[0].Id];
        system.assert(fItem.size()>0);
              
        // delete call to execute handler methods
        delete newCaseEmail;
        
        Test.StopTest();
  
    }
}