//------------------------------------------------------------------------------
// Creates order from order request
//------------------------------------------------------------------------------
public with sharing class CreateOrder
{
	//--------------------------------------------------------------------------
	// Inner order exception
	//--------------------------------------------------------------------------
	public class OrderException extends Exception {}

	//--------------------------------------------------------------------------
	// Members
	//--------------------------------------------------------------------------
	Id orderRequestId;
	IInsertHelper insertHelper;

	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	public CreateOrder(Id orderRequestId, IInsertHelper insertHelper)
	{
		this.orderRequestId = orderRequestId;
		this.insertHelper = insertHelper;
	}
	
	//--------------------------------------------------------------------------
	// Create order from order request
	//--------------------------------------------------------------------------
	public Id create()
	{
		//
		// Retrieve order request with locking statement
		//
		Order_Request__c orderReq =
				[ SELECT Name,
					Boni_Check__c,
					Boni_Check__r.Status_of_CC__c,
					Billing_Account__c,
					Billing_Account__r.Trial__c,
					Billing_Account__r.OFI_ID__c,
					Billing_Account__r.Payer__c,
				  	Billing_Account__r.Payer__r.BillingCountry,
				  ( SELECT Id
					FROM Orders__r )
				  FROM Order_Request__c
				  WHERE Id = :orderRequestId
				  FOR UPDATE ];

		//
		// Check if order is created
		//
		if (orderReq.Orders__r.size() != 0)
		{
			throw new OrderException('Order already exists!');
		}

		// Defect #Dev_054 - It shall be not possible to create order without Boni Check and Billing Account
		if (orderReq.Boni_Check__c == null)
		{
			throw new OrderException('Order cannot be created without Boni Check set on the Order Request!');
		}
		if (orderReq.Billing_Account__c == null)
		{
			throw new OrderException('Order cannot be created without Billing Account set on the Order Request!');
		}

		// Defect #Dev_085 - OFI ID shall be mandatory on billing account
		if (orderReq.Billing_Account__r.OFI_ID__c == null || orderReq.Billing_Account__r.OFI_ID__c == '')
		{
			throw new OrderException('Order cannot be created without OFI ID set on the Billing Account!');
		}

		// Defect #Dev_135 - It shall be not possible to create order without Boni Check in status Accepted
		if (orderReq.Boni_Check__r.Status_of_CC__c == null || orderReq.Boni_Check__r.Status_of_CC__c != 'Accepted')
		{
			throw new OrderException('Order cannot be created without Boni Check in status \'Accepted\'!');
		}


		//
		// Retrieve product orders
		//
		Map<Id, Product_Order__c> productOrderMap = new Map<Id, Product_Order__c>(
				[ SELECT Id, Quantity__c
				  FROM Product_Order__c
				  WHERE Order_Request__c = :orderRequestId ]);
		//
		// Retrieve product configuration orders
		//
		Product_Configuration_Order__c[] productConfigurationOrders =
				[ SELECT Id, Product_Configuration__c, Quantity__c
				  FROM Product_Configuration_Order__c
				  WHERE Product_Order__c IN :productOrderMap.keySet() ];
		//
		// Create map of product configuration orders regarding product configuration id
		//
		Map<Id, Product_Configuration_Order__c> productConfigurationOrderMap =
				new Map<Id, Product_Configuration_Order__c>();
		for (Product_Configuration_Order__c pco : productConfigurationOrders)
		{
			productConfigurationOrderMap.put(pco.Product_Configuration__c, pco);
		}

		//
		// Retrieve product configuration attributes
		// Non recurring line items and quantity
		//
		List<Id> keyList = new List<Id>();
		keyList.addAll(productConfigurationOrderMap.keySet());
		
		cscfga__Attribute__c[] attributes =
				[ SELECT Id, Name, cscfga__Is_Line_Item__c, cscfga__Recurring__c,
				  cscfga__Price__c, cscfga__Value__c, cscfga__Product_Configuration__c,
				  cscfga__Line_Item_Description__c,
				  cscfga__Product_Configuration__r.Billing_Rate_Model__c,
				  cscfga__Product_Configuration__r.Original_Product_Name__c
				  FROM cscfga__Attribute__c
				  WHERE cscfga__Product_Configuration__c in :productConfigurationOrderMap.keySet()
				  		AND (( cscfga__Is_Line_Item__c = true
				  		  	   AND cscfga__Recurring__c = false
				  		  	   AND Name != 'Tariff One-Time Target Fee Total')
				  		  	 OR Name = :Constants.ATTRIBUTE_NAME_QUANTITY
				  		  	 OR Name = :Constants.ATTRIBUTE_NAME_ARTICLE_INFO_SET) ];
		//
		// Put quantities to products
		//
		Map<Id, Integer> configurationQuantities  = new Map<Id, Integer>();
		Map<Id, Id> configurationArticleInfoSets = new Map<Id, Id>();
		for (cscfga__Attribute__c att : attributes)
		{
			if (att.Name == Constants.ATTRIBUTE_NAME_QUANTITY)
			{
				configurationQuantities.put(att.cscfga__Product_Configuration__c, Integer.valueOf(att.cscfga__Value__c));
			}
			if (att.Name == Constants.ATTRIBUTE_NAME_ARTICLE_INFO_SET)
			{
				try
				{
					configurationArticleInfoSets.put(att.cscfga__Product_Configuration__c, att.cscfga__Value__c);
				}
				catch(Exception e)
				{
					System.debug('Wrong Id in attribute: ' + att.cscfga__Value__c);
					throw e;
				}
			}
		}
		//
		// Get journal mappings
		//
		Map<Id, Article_Info_Set__c> articleInfoSets = new Map<Id, Article_Info_Set__c>(
				[ SELECT Id, One_Off_Journal_Mapping__c
				  FROM Article_Info_Set__c
				  WHERE Id IN :configurationArticleInfoSets.values() ]);
		Set<String> journalMappingNames = new Set<String>();
		for (Article_Info_Set__c ais : articleInfoSets.values())
		{
			journalMappingNames.add(ais.One_Off_Journal_Mapping__c);
		}

		String country = getCountry(orderReq.Billing_Account__r.Payer__r.BillingCountry);

		Journal_Mapping__c[] journalMappings =
				[ SELECT Id, Name
				  FROM Journal_Mapping__c
				  WHERE Name IN :journalMappingNames
				  AND Country__c = :country ];
		Map<String, Id> journalMappingMap = new Map<String, Id>();
		for (Journal_Mapping__c jm : journalMappings)
		{
			journalMappingMap.put(jm.Name, jm.Id);
		}

		//
		// Create order request
		//
		csord__Order_Request__c orderRequest = new csord__Order_Request__c();
		orderRequest.Name = orderReq.Name;
		orderRequest.csord__Module_Name__c = Constants.ORDER_MODULE_NAME;
		orderRequest.csord__Module_Version__c = Constants.ORDER_MODULE_VERSION;
		insert orderRequest;
		//
		// Create order
		//
		csord__Order__c order = new csord__Order__c();
		order.Name = 'Order for ' + orderReq.Name;
		order.Order_Request__c = orderReq.Id;
		order.csord__Order_Request__c = orderRequest.Id;
		order.csord__Account__c = orderReq.Billing_Account__r.Payer__c;
		order.csord__Identification__c = 'Order';
		order.csord__Status2__c = 'Opened';
		insertHelper.add(order);

		Set<Id> aisWithoutJournalMapping = new Set<Id>();

		//
		// Calculate order line items
		//
		for (cscfga__Attribute__c att : attributes)
		{
			if (att.cscfga__Is_Line_Item__c == true
				&& att.cscfga__Price__c != null
				&& att.cscfga__Price__c != 0)
			{
				//
				// Calculate price
				//
				Decimal price = att.cscfga__Price__c;
				Decimal quantity = productConfigurationOrderMap.get(att.cscfga__Product_Configuration__c).Quantity__c;
				price *= quantity;

				//24.3.207. PM removed dividing price by quantity for ICAP - BUG for the rest?!
				if (!att.cscfga__Product_Configuration__r.Original_Product_Name__c.equalsIgnoreCase('IoT Connect Advanced') &&
					!att.cscfga__Product_Configuration__r.Original_Product_Name__c.equalsIgnoreCase('IoT Connect Advanced Option')){
					price /= configurationQuantities.get(att.cscfga__Product_Configuration__c);
				}
				
				Id articleInfoSet = configurationArticleInfoSets.get(att.cscfga__Product_Configuration__c);
				Id journalMapping = null;
				if (articleInfoSet != null)
				{
					String journalMappingName = articleInfoSets.get(articleInfoSet).One_Off_Journal_Mapping__c;
					if (journalMappingName != null)
					{
						journalMapping = journalMappingMap.get(journalMappingName);
					}
				}
				//
				// Check if journal mapping exists
				//
				if (journalMapping == null)
				{
					if (articleInfoSet == null)
					{
						aisWithoutJournalMapping.add(att.cscfga__Product_Configuration__c);
					}
					else
					{
						aisWithoutJournalMapping.add(articleInfoSet);
					}
				}
				//
				// Order line item
				//
				csord__Order_Line_Item__c orderLineItem = new csord__Order_Line_Item__c();
				orderLineItem.csord__Order_Request__c = orderRequest.Id;
				orderLineItem.Name = att.Name;
				orderLineItem.csord__Identification__c = 'OrderLineItem';
				orderLineItem.csord__Is_Recurring__c = false;
				orderLineItem.csord__Line_Description__c = att.cscfga__Line_Item_Description__c;
				orderLineItem.Quantity__c = quantity;
				orderLineItem.csord__Total_Price__c = price;
				orderLineItem.Journal_Mapping__c = journalMapping;
				orderLineItem.csord__Discount_Type__c = 'Amount';
				String billingRateModel = att.cscfga__Product_Configuration__r.Billing_Rate_Model__c;
				orderLineItem.Billing_Rate_Model__c =
						billingRateModel != null ? billingRateModel : Constants.ONE_OFF;
				orderLineItem.Article_Info_Set__c = articleInfoSet;
				insertHelper.add(orderLineItem, 'csord__Order__c', order);
			}
		}
		//
		// If there is no journal mapping throw exception
		// because line items without journal mapping cannot be billed
		//
		if (!aisWithoutJournalMapping.isEmpty())
		{
			List<Id> ids = new List<Id>(aisWithoutJournalMapping);
			throw new JournalMappingException( String.join(ids, ', ') );
		}

		insertHelper.store();
		return order.Id;
	}

	//--------------------------------------------------------------------------
	// Returns DE or Non-DE depending on country name
	//--------------------------------------------------------------------------
	private static String getCountry(String country)
	{
		System.debug('Country: ' + country);
		if (country == 'Germany'
			|| country == 'Deutschland')
		{
			return 'DE';
		}
		else
		{
			return 'Non-DE';
		}
	}
}