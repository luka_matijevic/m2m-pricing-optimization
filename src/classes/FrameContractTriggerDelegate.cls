/* 
 * Helper class which performs all the actual logic when the trigger on Frame Contract object is fired.
 */
public class FrameContractTriggerDelegate extends TriggerHandler.DelegateBase
{
    Set<Id> opptyIds = new Set<Id>();
    List<Opportunity> opportunities = new List<Opportunity>();
    Map<Id,List<Unit_Schedule__c>> opptyToSchedules = new Map<Id,List<Unit_Schedule__c>>();
    map<id, Frame_Contract__c> frameContracts = new map<id, Frame_Contract__c>(); //Frame Contracts with parent Fields
    /*
     *Method which gets called when before trigger is fired 
     */
    public override void prepareBefore() {
    // do any preparation here – bulk loading of data etc
        if (Trigger.isInsert) {
            for (SObject obj : Trigger.new) {
                Frame_Contract__c contract = (Frame_Contract__c) obj;
                opptyIds.add(contract.Opportunity__c);
            }
            if(opptyIds != null && opptyIds.size() > 0)
            {
                opportunities = [Select Id, Name, (Select Id, Name, Opportunity__c,Year__c,Connectivity_Sims_December__c,Connectivity_Sims_November__c,Connectivity_Sims_October__c,Connectivity_Sims_September__c,Connectivity_Sims_August__c,Connectivity_Sims_July__c,Connectivity_Sims_June__c,Connectivity_Sims_May__c,Connectivity_Sims_April__c,Connectivity_Sims_March__c,Connectivity_Sims_February__c,Connectivity_Sims_January__c from Unit_Schedule__r Order By Year__c DESC NULLS LAST) from Opportunity where Id IN : opptyIds];  
                for(Opportunity opp : opportunities)
                {
                    opptyToSchedules.put(opp.Id, opp.Unit_Schedule__r);
                }
            }
        }
        
        
    }

    /*
     *Method which gets called when after trigger is fired 
     */  
    public override void prepareAfter() {
        if(trigger.isUpdate){
            frameContracts = new map<id, Frame_Contract__c>([select id,Customer__c,Name ,Status__c,Product_Bundle__c,Product_Bundle__r.Re_Price__c from Frame_Contract__c where id In:trigger.newMap.keySet()]);
            sendAnEmailTo2ndLevelUsers(frameContracts, (map<id, Frame_Contract__c>) trigger.oldMap);
        }
    }

    /*
     *Method which gets called when beforeInsert trigger is fired 
     */
    public override void beforeInsert(sObject o) {
    // Apply before insert logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
        Frame_Contract__c fc = (Frame_Contract__c)o;
        System.debug('FC before Date : ' + fc);
        List<Unit_Schedule__c> schedules = opptyToSchedules.containsKey(fc.Opportunity__c) ? opptyToSchedules.get(fc.Opportunity__c) : null;
        System.debug('Schedules List : ' + schedules);
        if(schedules != null && schedules.size() > 0)
        {
            Unit_Schedule__c schedule = schedules[0];
            System.debug('Latest Schedule : ' + schedule);
            fc.Activation_due_date__c = (schedule.Connectivity_Sims_December__c != null ? '01/12/' + schedule.Year__c : (schedule.Connectivity_Sims_November__c != null ? '01/11/' + schedule.Year__c : (schedule.Connectivity_Sims_October__c != null ? '01/10/' + schedule.Year__c : (schedule.Connectivity_Sims_September__c != null ? '01/09/' + schedule.Year__c : (schedule.Connectivity_Sims_August__c != null ? '01/08/' + schedule.Year__c : (schedule.Connectivity_Sims_July__c != null ? '01/07/' + schedule.Year__c :  (schedule.Connectivity_Sims_June__c != null ? '01/06/' + schedule.Year__c : (schedule.Connectivity_Sims_May__c != null ? '01/05/' + schedule.Year__c :  (schedule.Connectivity_Sims_April__c != null ? '01/04/' + schedule.Year__c : (schedule.Connectivity_Sims_March__c != null ? '01/03/' + schedule.Year__c : (schedule.Connectivity_Sims_February__c != null ? '01/02/' + schedule.Year__c : (schedule.Connectivity_Sims_January__c != null ? '01/01/' + schedule.Year__c : ''))))))))))));
            System.debug('FC after Date : ' + fc);
        }
    }

    /*
     *Method which gets called when beforeUpdate trigger is fired 
     */
    public override void beforeUpdate(sObject old, sObject o) {
        // Apply before update logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    /*
     *Method which gets called when beforeDelete trigger is fired 
     */
    public override void beforeDelete(sObject o) {
        // Apply before delete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    /*
     *Method which gets called when afterInsert trigger is fired 
     */
    public override void afterInsert(sObject o) {
        // Apply after insert logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    /*
     *Method which gets called when afterUpdate trigger is fired 
     */
    public override void afterUpdate(sObject old, sObject o) {
        // Apply after update logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    /*
     *Method which gets called when afterDelete trigger is fired 
     */
    public override void afterDelete(sObject o) {
        // Apply after delete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    /*
     *Method which gets called when afterUndelete trigger is fired 
     */
    public override void afterUndelete(sObject o) {
        // Apply after undelete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    /*
     *Method which gets called after respective methods are completed based on trigger event
     */
    public override void finish() {
    }
    
    
    public static void sendAnEmailTo2ndLevelUsers(map<id, Frame_Contract__c> frameContracts, map<id, Frame_Contract__c> oldMap){
        try{
            if(frameContracts != null && frameContracts.size()> 0){
                set<id> accIds = new set<id>();
                map<string, Integer> noOfFCperCustomer = new map<string, Integer>();
                for(Frame_Contract__c fc : frameContracts.values()){
                    if(fc.Status__c == Label.Archived_Contract_And_Sent_To_Customer && oldMap.get(fc.Id) != null && oldMap.get(fc.Id).Status__c != Label.Archived_Contract_And_Sent_To_Customer){
                        if(fc.Customer__c != null && fc.Product_Bundle__r.Re_Price__c == false){
                            accIds.add(fc.Customer__c);
                        }
                    }
                }
                
                if(accIds.size() > 0){
                    for(AggregateResult ar : [select count(id) cnt, Customer__c from Frame_Contract__c where id in:accIds Group by Customer__c]){
                        string accId = string.valueOf(ar.get('Customer__c'));
                        Integer noOfFcs = Integer.valueOf(ar.get('cnt'));
                        if(accId != null && noOfFcs != null){
                            noOfFCperCustomer.put(accId,noOfFcs);
                        } 
                    }
                }
                
                List<Frame_Contract__c> frameContractsToSendEmail = new List<Frame_Contract__c>();
                for(Frame_Contract__c fc : frameContracts.values()){
                    if(fc.Status__c == Label.Archived_Contract_And_Sent_To_Customer && oldMap.get(fc.Id) != null && oldMap.get(fc.Id).Status__c != Label.Archived_Contract_And_Sent_To_Customer){
                        if(fc.Product_Bundle__r.Re_Price__c == false){
                            frameContractsToSendEmail.add(fc);
                        }else if(fc.Customer__c != null && noOfFCperCustomer.get(fc.Customer__c) != null && noOfFCperCustomer.get(fc.Customer__c) > 1){
                            frameContractsToSendEmail.add(fc);
                        }
                    }
                }
                
                
                if(frameContractsToSendEmail.size() > 0){
                    
                    List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
                    //EmailTemplate templateId = [Select id from EmailTemplate where name = 'New Frame contract for existing customer archieved and sent to the customer'];
                    
                    set<string> usersToRecievemail = new set<string>();
                    if(Label.Email_Addresses != null){
                        usersToRecievemail.addAll(Label.Email_Addresses.split(';'));
                    }
                    for(User u : [select id, name,Email from User where profile.Name = 'M2M Service 2nd Level' and IsActive = true]){
                        usersToRecievemail.add(u.Email);
                    }
                    
                    //if(templateId != null && usersToRecievemail.size() > 0){
                    if(usersToRecievemail.size() > 0){
                        string subject = Label.Subject_FC_Archived;
                        string mainBody = Label.Msg_FC_Archived;
                        string baseLocation = URL.getSalesforceBaseUrl().toExternalForm();
                        string loggedInUserName = UserInfo.getName();
                        list<string> userEmails = new list<string>();
                        userEmails.addAll(usersToRecievemail);
                        system.debug('User EMail  ****'+usersToRecievemail);
                        system.debug('FC EMail  ****'+frameContractsToSendEmail);
                        for(Frame_Contract__c fc : frameContractsToSendEmail){
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            mail.setToAddresses(userEmails);
                            //mail.setReplyTo('email@email.com');
                            mail.setSenderDisplayName(loggedInUserName);
                            mail.setSubject(subject);
                            
                            string htmlBody = preapreMailBody(mainBody,Fc,baseLocation);
                            system.debug('htmlBody  ****'+htmlBody);
                            mail.setHtmlBody(htmlBody);
                            allmsg.add(mail);
                        }
                    }
                    system.debug('allmsg  ****'+allmsg);
                    if(allmsg.size() > 0){
                        
                       Messaging.sendEmail(allmsg);
                    }
                    
                }
            }
        }catch(Exception ex){
            system.debug('Exception occurred while sending an email to 2nd Level user , when Existing Customer Fc get Archived and sent to customer'+ex.getMessage());
        }
    }   
    
    
    public static string preapreMailBody(string partialBody, Frame_Contract__c fc, string url){
        url = url + '/'+ fc.id;
        string htmlBody = '<body>';
        htmlBody = htmlBody +  partialBody;
        htmlBody = htmlBody +  '<br/>';
        htmlBody = htmlBody +  '<a href=' + '"' +url + '">'+fc.Name+'</a>' ;
        htmlBody = htmlBody +  '</body>';
        return htmlBody;
    }
    
}