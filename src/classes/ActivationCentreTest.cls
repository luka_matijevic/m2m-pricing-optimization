@isTest
private class ActivationCentreTest {

	@isTest
	private static void method1() {
		
		//
		// Create frame contract with product bundle
		//
    	No_Trigger__c notriggers = new No_Trigger__c();
    	notriggers.Flag__c = true;
    	insert notriggers;
		No_Validation__c noValidation = new No_Validation__c(
			Flag__c = true
		);
		insert noValidation;
		TestHelper.createFrameContract();
		Frame_Contract__c frameContract =
				[ SELECT Id, Product_Bundle__c, Opportunity__c,
				  Contact__c, Customer__c
				  FROM Frame_Contract__c
				  LIMIT 1];
		Order_Request__c orderReq = new Order_Request__c();
		orderReq.Account__c = [ SELECT Id FROM Account LIMIT 1].Id;
		Id billingAccountId = [ SELECT Id FROM Billing_Account__c LIMIT 1].Id;
        Billing_Account_Subscription_Group__c basg = new  Billing_Account_Subscription_Group__c();
        basg.Billing_Account__c = billingAccountId;
        basg.SM2M_Subscription_Group_Name__c = 'test';
        insert basg;
        Article_Info_Set__c ais = new Article_Info_Set__c();
		ais.recordtypeid =Schema.SObjectType.Article_Info_Set__c.getRecordTypeInfosByName().get('SIM').getRecordTypeId();
        ais.Name = 'test';
        insert ais;
        
		orderReq.Billing_Account__c = billingAccountId;
		insert orderReq;
        
        Third_Upload_File__c tuf = new Third_Upload_File__c();
        tuf.Order_Request__c = orderReq.id;
		tuf.SIM_Article_Type__c = ais.id;
        insert tuf;
        Stock__c stock = new Stock__c();
        stock.Third_Upload_File__c = tuf.id;
        stock.SM2M_Status__c = 'INACTIVE_NEW';
        stock.State_Change_Date__c = DateTime.now();
        stock.subscription_group__C = basg.id;
        stock.Article_Info_Set__c = ais.id;
        
        insert stock;
        
    	notriggers.Flag__c = false;
    	update notriggers;

	


		Test.startTest();
		PageReference pageRef = Page.ActivationCentre;
        pageRef.getParameters().put('OrderRequestId', orderReq.Id);      
		Test.setCurrentPage(pageRef);      
		ActivationCentreController controller = new ActivationCentreController(new ApexPages.StandardController(new Activation_Centre__c()));
        controller.initiatePage();
        controller.getSubscriptionGroups();
        controller.getAPNs();
        controller.getArticleInfoSets();
        controller.getLifeCycleStatusesOptions();
        /*
        ActivationCentreController.simData sim = new ActivationCentreController.simData();
        sim.iccid = '1234567890123456789';
        sim.LifeCycleStatus = 'INACTIVE_NEW';
        sim.articleInfoSetId = ais.id;
        sim.exists = false;
        controller.simDataList.add(sim);
		*/
        controller.startProcessing();
		Test.stopTest();

	}
}