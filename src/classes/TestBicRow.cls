/**
	* TestBicRow - Tests for BICRow IRow implementation
*/

@isTest
private class TestBicRow {
	static testMethod void testRowCreation()
	{
		No_Trigger__c notriggers = new No_Trigger__c();
    	notriggers.Flag__c = true;
    	insert notriggers;
		//
    	// Create test data
    	//
    	TestHelper.createOrderRequest();
		notriggers.Flag__c = false;
    	update notriggers;

		Order_Request__c ord = [SELECT Id, Account__c FROM Order_Request__c LIMIT 1];

		Third_Upload_File__c thirdFile = new Third_Upload_File__c(Name = 'name', Order_Request__c = ord.Id);

		insert thirdFile;


		TestSecondUploadFileDataGenerator.insertSimAsset();
		Stock__c stock = [SELECT Id, State_Change_Date__c, SM2M_Status__c, IMSI__c, Third_Upload_File__c FROM Stock__c LIMIT 1];

		stock.Third_Upload_File__c = thirdFile.Id;
		stock.State_Change_Date__c = Datetime.now()+10;
		stock.SM2M_Status__c = Constants.ACTIVE;
		stock.IMSI__c = '123456789012';
		stock.Account__c = ord.Account__c;
		update stock;

		test.startTest();
		stock = [ SELECT Id, State_Change_Date__c, SM2M_Status__c, HistoryTracking__c, Commercial_Date__c, Activation_Date__c,
		          Account__r.Name,
				  Account__c, ICCID__c, IMSI__c, MSISDN__c,
				  Article_Info_Set__c, Article_Info_Set__r.Name,
				  Account__r.BillingPostalCode, Account__r.BillingState,
				  Account__r.BillingStreet, Account__r.BillingCountry,
				  Account__r.Phone, Account__r.Fax, Account__r.Company_Email__c,
				  Account__r.VO_Number__c, Account__r.SPIN_ID__c,
				  Third_Upload_File__r.Order_Request__r.Frame_Contract__r.Name
				  FROM Stock__c
				  WHERE Id = :stock.Id ];

		BICRow row = new BICRow(stock, '24');
		List<String> cells = row.getCells();
		test.stopTest();

		System.assertEquals(24, cells.size());
    	System.assertEquals(Datetime.now().addDays(10).format('dd.MM.yyyy'), cells[0]);
    	System.assertEquals('ACTIVATED', cells[1]);
    	System.assertEquals('Unit Test Account', cells[2]);
    	System.assertEquals(stock.Account__c, cells[3]);
		System.assertEquals(stock.Id, cells[4]);
    	System.assertEquals('1235791085088000000', cells[5]);
    	System.assertEquals('123456789012', cells[6]);
    	System.assertEquals('34698700000', cells[7]);
    	System.assertEquals(stock.Article_Info_Set__c, cells[8]);
    	System.assertEquals('M2M GLOBAL SIM CHIP', cells[9]);
    	System.assertEquals('48260', cells[10]);
    	System.assertEquals(null, cells[11]);
    	System.assertEquals('Unit Test Street 4', cells[12]);
    	System.assertEquals('4', cells[13]);
    	System.assertEquals('191', cells[14]);
    	System.assertEquals('+385166666666', cells[15]);
    	System.assertEquals('+385166666667', cells[16]);
    	System.assertEquals('company@email2.com', cells[17]);
    	System.assertEquals('24', cells[18]);
    	System.assertEquals('1234567', cells[19]);
    	System.assertEquals('SPIN_ID', cells[20]);
    	System.assertEquals(stock.Third_Upload_File__r.Order_Request__r.Frame_Contract__r.Name, cells[21]);
    	System.assertEquals('', cells[22]);
		System.assertEquals('', cells[23]);

	}
}