global class AssignQueue implements Database.Batchable<SObject> {
	
	String query;
	private String status = 'WVL';
	
	global AssignQueue() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {

		query = 'SELECT Id, Reroute_Date__c, Status, OwnerId, Last_Assignee__c FROM Case WHERE Status = :status AND DAY_ONLY(Reroute_Date__c) = TODAY';
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Case> scope) {
   		
		for(Case c : scope){

			if(c.Reroute_Date__c < System.now()){
				system.debug(c.Last_Assignee__c);
				c.Status = 'Open';
				c.OwnerId = c.Last_Assignee__c;
			}
		}
		
		update scope;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}