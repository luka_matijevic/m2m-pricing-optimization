//------------------------------------------------------------------------------
// Controller user for copying bundle to opportunity
//------------------------------------------------------------------------------
public with sharing class CopyBundleController
{
    //--------------------------------------------------------------------------
    // Getters / setters
    //--------------------------------------------------------------------------
    public cscfga__Product_Bundle__c sourceBundle { get; set; }
    public cscfga__Product_Bundle__c targetBundle { get; set; }
    public cscfga__Product_Basket__c targetBasket { get; set; }

    static Integer bundleFieldLength = cscfga__Product_Bundle__c.Name.getDescribe().getLength();

    //--------------------------------------------------------------------------
    // Constructor
    //--------------------------------------------------------------------------
    public CopyBundleController(ApexPages.StandardController stdController)
    {
        //
        // Get original bundle
        //
        sourceBundle =
                [ SELECT Id, Name, cscfga__Opportunity__c
                  FROM cscfga__Product_Bundle__c
                  WHERE Id = :stdController.getId() ];

        //
        // Set target bundle
        //
        targetBundle = new cscfga__Product_Bundle__c();
        String targetName = 'Copy of ' + sourceBundle.Name;
        if (targetName.length() > bundleFieldLength)
        {
            targetName = targetName.substring(0, bundleFieldLength);
        }
        targetBundle.Name = targetName;
        targetBundle.cscfga__Opportunity__c = sourceBundle.cscfga__Opportunity__c;


        //
        // Set target basket (we don't need the original basket)
        //
        targetBasket = new cscfga__Product_Basket__c();
        targetBasket.Name = 'Basket for Opportunity: ' + sourceBundle.cscfga__Opportunity__c;   // use the same opp id
        // targetBasket.cscfga__Opportunity__c is set by the page automatically (on the press of the clone button setter is called by the page)
        targetBasket.cscfga__Basket_Status__c = 'Valid';


    }

    //--------------------------------------------------------------------------
    // Clone bundle with all configurations
    //--------------------------------------------------------------------------
    public PageReference cloneBundle()
    {
        //
        // Create bundle and basket
        //
        insert targetBundle;
        insert targetBasket;
        //
        // Copy configurations to product bundle
        //
        List<cscfga__Product_Configuration__c> configurations =
                [ SELECT Id
                  FROM cscfga__Product_Configuration__c
                  WHERE cscfga__Product_Bundle__c = :sourceBundle.Id
                  ORDER BY CreatedDate ASC ];

        List<Id> childIdList = cscfga.API_1.cloneConfigurations(configurations, targetBundle, targetBasket);
        
        /* START: T-16373 changes */
        cscfga__Attribute__c clonedBundleAtt;
        List<cscfga__Attribute__c> attributesProdConfigList = new List<cscfga__Attribute__c>();
        Set<String> nameSet = new Set<String>();
        nameSet.add('Account Id');
        nameSet.add('Bundle Id');
        nameSet.add('Config Id');
        nameSet.add('Cloned Tariff');
        System.debug(childIdList);
        for(cscfga__Attribute__c att : [SELECT cscfga__Value__c, Name, cscfga__Product_Configuration__c, Id,
                                    cscfga__Product_Configuration__r.cscfga__Product_Bundle__c, 
                                    cscfga__Product_Configuration__r.cscfga__Product_Bundle__r.cscfga__Opportunity__r.AccountId
                                    FROM cscfga__Attribute__c where cscfga__Product_Configuration__c IN: childIdList AND Name IN: nameSet]){
            if(att.Name.equalsIgnoreCase('Account Id')){
                att.cscfga__Value__c = att.cscfga__Product_Configuration__r.cscfga__Product_Bundle__r.cscfga__Opportunity__r.AccountId;
            }
            if(att.Name.equalsIgnoreCase('Bundle Id')){
                att.cscfga__Value__c = att.cscfga__Product_Configuration__r.cscfga__Product_Bundle__c;
            }
            if(att.Name.equalsIgnoreCase('Config Id')){
                att.cscfga__Value__c = att.cscfga__Product_Configuration__c;
            }
            if(att.Name == 'Cloned Tariff' && !String.isEmpty(att.cscfga__Value__c)){
                clonedBundleAtt = att;
                continue;
            }
            attributesProdConfigList.add(att);
        }
        if(!attributesProdConfigList.isEmpty()){
            update attributesProdConfigList;
        }
        /* END: T-16373 changes */

        //PostCopyConnectivityConfigurations connector = new PostCopyConnectivityConfigurations(targetBundle.Id);
        //connector.doExecute();
        System.debug(clonedBundleAtt);
        if (clonedBundleAtt != null && clonedBundleAtt.cscfga__Value__c != null){
            CopyTariffHelper ctHelper = new CopyTariffHelper();
            Id clonedTariffId = ctHelper.copyTariffWithAllOptions(clonedBundleAtt.cscfga__Value__c, targetBundle.Id);
            System.debug(clonedTariffId);
            if (clonedTariffId != null){
                clonedBundleAtt.cscfga__Value__c = String.valueOf(clonedTariffId);
                System.debug(clonedBundleAtt);
                update clonedBundleAtt;
            }
        }

        return null; // return viewNewBundle();
    }

    //--------------------------------------------------------------------------
    // Returns link to new product bundle
    //--------------------------------------------------------------------------
    public PageReference viewNewBundle()
    {
        // Send the user to the detail page for the new bundle.
        PageReference bundlePage = new ApexPages.StandardController(targetBundle).view();
        bundlePage.setRedirect(true);
        return bundlePage;
    }
}