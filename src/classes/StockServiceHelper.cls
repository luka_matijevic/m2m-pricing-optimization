/*
 * @description: Helper class for Stock (SIM) Operations.
 * 
 * @author: Ritesh Saxena
 */
public class StockServiceHelper{
    
	public static List<sObject> filterTrialSObjects(List<sObject> unfilteredSObjects){
        Set<Id> srfIdSet = new Set<Id>();
        Set<Id> tufIdSet = new Set<Id>();
        Set<Id> accIdSet = new Set<Id>();
        Map<Id,SRF__c> srfSObjectList = new Map<Id,SRF__c>();
        Map<Id,Third_Upload_File__c> tufSObjectList = new Map<Id,Third_Upload_File__c>();
        List<Billing_Account__c> billingAccountList = new List<Billing_Account__c>();
        Map<Id,Billing_Account__c> accIdToBillingAccMap = new Map<Id,Billing_Account__c>();
        Map<Id,Account> accountList = new Map<Id,Account>();
        List<sObject> filteredSObjectList = new List<sObject>();
        
        for(Stock__c obj : (List<Stock__c>) unfilteredSObjects){
            //if(obj.SRF__c != null) srfIdSet.add(obj.SRF__c);
            //if(obj.Third_Upload_File__c != null) tufIdSet.add(obj.Third_Upload_File__c);
            /*if(obj.Account__c != null) {
                accIdSet.add(obj.Account__c);
            }*/
            
            if (!obj.Trial__c) {
                filteredSObjectList.add(obj);
            }
        }

        //if(srfIdSet.size()>0) 
         //   srfSObjectList = new Map<Id,SRF__c>([Select Account__c,Id,Order_Request__c, Order_Request__r.Billing_Account__c, Order_Request__r.Billing_Account__r.Trial__c,Order_Request__r.Account__c from SRF__c where Id in :srfIdSet]);
        //if(tufIdSet.size()>0) 
         //   tufSObjectList = new Map<Id,Third_Upload_File__c>([Select Account__c,Id, Order_Request__c,Order_Request__r.Billing_Account__c,Order_Request__r.Billing_Account__r.Trial__c,Order_Request__r.Account__c from Third_Upload_File__c where Id in :tufIdSet]);
        //if(srfSObjectList.size()>0){
         //   for(SRF__c srf : srfSObjectList.values()){
         //       accIdSet.add(srf.Account__c);
        //    }
        //}

       // if(tufSObjectList.size()>0){
        //    for(Third_Upload_File__c tuf : tufSObjectList.values()){
        //        accIdSet.add(tuf.Account__c);
        //    }
        //}
        
        /*if(accIdSet.size()>0) 
            accountList = new Map<Id,Account>([Select id,Name from Account where Id in :accIdSet]);
            
        if(accIdSet.size()>0) 
            billingAccountList = [Select Payer__c,Trial__c,Name from Billing_Account__c where Payer__c in :accIdSet];
            
        if(billingAccountList.size()>0){
            for(Billing_Account__c bacc : billingAccountList){
                if(bacc.Name.contains('default') || bacc.Name.contains('Default') || accIdToBillingAccMap.get(bacc.Payer__c) == null){
                    accIdToBillingAccMap.put(bacc.Payer__c,bacc);
                }
            }
        }*/
        
        /*for(sObject obj : unfilteredSObjects){
            Stock__c sim = (Stock__c) obj;
            //Boolean srfTrial = false;
            //Boolean tufTrial = false;
            /*Boolean defaultBillingAccountTrial = false;*/
            //try{
            //    if(srfSObjectList.size()>0)
            //        if(sim.SRF__c != null && srfSObjectList.get(sim.SRF__c) != null && srfSObjectList.get(sim.SRF__c).Order_Request__c != null && srfSObjectList.get(sim.SRF__c).Order_Request__r.Billing_Account__c != null ) 
            //            srfTrial = srfSObjectList.get(sim.SRF__c).Order_Request__r.Billing_Account__r.Trial__c;
            //}
            //catch(Exception e){
            //    srfTrial = false;
            //}
            //try{
            //    if(tufSObjectList.size()>0)
            //        if(sim.Third_Upload_File__c != null && tufSObjectList.get(sim.Third_Upload_File__c) != null && tufSObjectList.get(sim.Third_Upload_File__c).Order_Request__c != null && tufSObjectList.get(sim.Third_Upload_File__c).Order_Request__r.Billing_Account__c != null) 
            //            tufTrial = tufSObjectList.get(sim.Third_Upload_File__c).Order_Request__r.Billing_Account__r.Trial__c;
            //}
            //catch(Exception e){
            //    tufTrial = false;
            //}           
            //if(!(srfTrial || tufTrial)){
                //if((sim.SRF__c == null && sim.Third_Upload_File__c == null) ||
                //   !((sim.SRF__c != null && srfSObjectList.get(sim.SRF__c) != null && srfSObjectList.get(sim.SRF__c).Order_Request__c != null && srfSObjectList.get(sim.SRF__c).Order_Request__r.Billing_Account__c != null ) ||
                //     (sim.Third_Upload_File__c != null && tufSObjectList.get(sim.Third_Upload_File__c) != null && tufSObjectList.get(sim.Third_Upload_File__c).Order_Request__c != null && tufSObjectList.get(sim.Third_Upload_File__c).Order_Request__r.Billing_Account__c != null)
                //   )
                //)
                //{
                /*
                    Id stockAccountId;
                    if(sim.Account__c != null){
                        stockAccountId = sim.Account__c;
                    }
                    if(stockAccountId != null){
                        if(accountList.get(stockAccountId) != null && accIdToBillingAccMap.size()>0 && accIdToBillingAccMap.get(stockAccountId) != null){
                            defaultBillingAccountTrial = accIdToBillingAccMap.get(stockAccountId).Trial__c;
                        }
                    }else{
                        defaultBillingAccountTrial = true;
                    }*/
                //}
                
                /*
                if(!defaultBillingAccountTrial) 
                    filteredSObjectList.add(obj);
                    */
            
        //}

        return filteredSObjectList;
    }

}