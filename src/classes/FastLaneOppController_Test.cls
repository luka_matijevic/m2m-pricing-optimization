@isTest
public class FastLaneOppController_Test {

    @testsetup
    static void prepareTestData(){
        list<EU_Countries__c> countries = Test_Util.createCountries();
	    insert countries;
	    
        No_Trigger__c notriggers = new No_Trigger__c();
        notriggers.Flag__c = true;
        insert notriggers;
        
        Account account = new Account(
        	Name = 'Test3',
            Type = 'Business'
        	);
        	
        insert account;
        
        Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Negotiation',
			CloseDate = Date.newInstance(2014,1,1),
			AccountId = account.Id
		);
		
		insert opp;
		
		Contact con = Test_Util.createContact(account.id);
		insert con;
		
		OpportunityContactRole oppConRole = Test_Util.createOppConRole(opp.id,con.id,'Decision Maker');
		insert oppConRole;
		
		cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c(
			cscfga__Synchronised_with_Opportunity__c = false,
			cscfga__Opportunity__c = opp.Id
		);
		
		insert productBundle;
        
        list<cscfga__Product_Definition__c> productDefinitionLst = new list<cscfga__Product_Definition__c>();
        
        
        cscfga__Product_Definition__c productDefinition_f = Test_Util.createProdDefination('FLO Connectivity','Test description');
		productDefinitionLst.add(productDefinition_f);
        
		
		insert productDefinitionLst;
		
		list<id> prodDefinations = new list<id>();
		prodDefinations.add(productDefinition_f.id);
		
		list<cscfga__Attribute_Definition__c> attrDefinationsLst = Test_Util.attrDefinations(prodDefinations);
		insert attrDefinationsLst;
		
		cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c(
			cscfga__Product_Bundle__c = productBundle.Id,
			cscfga__Product_Definition__c = productDefinition_f.Id,
			name = 'SIM & Tariff'
		);
		
		
		insert productConfiguration;
		
		
		map<string,cscfga__Attribute_Definition__c> attrDefinations = Test_Util.attrDefinations(productDefinition_f.Id); 

        list<cscfga__Attribute__c> attLst = new list<cscfga__Attribute__c>();
        attLst.add(Test_Util.createAttrDef('Quantity',productConfiguration.id,'12',attrDefinations.get('Quantity').id ));
        
        attLst.add(Test_Util.createAttrDef('Additional Tariff Option Name',productConfiguration.id,'abc',attrDefinations.get('Additional Tariff Option Name').id ));
        attLst.add(Test_Util.createAttrDef('One-Time Base Fee',productConfiguration.id,'100',attrDefinations.get('One-Time Base Fee').id ));
        attLst.add(Test_Util.createAttrDef('Total All Recurring Fees',productConfiguration.id,'100',attrDefinations.get('Total All Recurring Fees').id ));
        attLst.add(Test_Util.createAttrDef('Activation Fee Lookup',productConfiguration.id,'100',attrDefinations.get('Activation Fee Lookup').id ));
        insert attLst;
        
    }
    
    static testMethod void initTest(){
        cscfga__Product_Bundle__c productBundle = [select id,cscfga__Opportunity__c,cscfga__Bundle_Status__c from cscfga__Product_Bundle__c limit 1];
        Opportunity opp= [select id from opportunity limit 1];
        test.startTest();
        PageReference pf = page.FastLaneOpp;
        pf.getParameters().put('oppId',opp.id);
        test.setCurrentPage(pf);
        FastLaneOppController fl= new FastLaneOppController(new ApexPages.standardController(productBundle));
        fl.init();
        fl.quicksaveBundle();
        test.stopTest();
        
        
    }
    
    static testMethod void initTest2(){
        cscfga__Product_Bundle__c productBundle = [select id,cscfga__Opportunity__c,cscfga__Bundle_Status__c from cscfga__Product_Bundle__c limit 1];
        Opportunity opp= [select id from opportunity limit 1];
        test.startTest();
        PageReference pf = page.FastLaneOpp;
        pf.getParameters().put('oppId',opp.id);
        pf.getParameters().put('id',productBundle.id);
        test.setCurrentPage(pf);
        FastLaneOppController fl= new FastLaneOppController(new ApexPages.standardController(productBundle));
        fl.init();
        fl.cancelBundle();
        fl.saveBundleAndCreateContract();
        
        test.stopTest();
        
        
    }
    
    static testMethod void initTest3(){
        cscfga__Product_Bundle__c productBundle = [select id,cscfga__Opportunity__c,cscfga__Bundle_Status__c from cscfga__Product_Bundle__c limit 1];
        Opportunity opp= [select id from opportunity limit 1];
        test.startTest();
        PageReference pf = page.FastLaneOpp;
        pf.getParameters().put('oppId',opp.id);
        pf.getParameters().put('id',productBundle.id);
        test.setCurrentPage(pf);
        FastLaneOppController fl= new FastLaneOppController(new ApexPages.standardController(productBundle));
        fl.init();
        fl.saveBundle();
        
        test.stopTest();
        
        
    }

}