global class RetrieveAccountAPNsSchedule { 

	public static final String CRON_EXP = '0 0 5 * * ? *';

	global static String scheduleIt() {
		return System.schedule('Daily Subscription Groups Update', CRON_EXP, new RetrieveSubscriptionGroupsSchedule());
	}
	
	global void execute(SchedulableContext sc)	{
		Database.executeBatch(new RetrieveSubscriptionGroupsBatch(), 1);
	}

}