/**
 * BICRow - Interpretation of BIC sim report Row for BIC CSV file generation and integration
 * @author: Zoran Zunko
 * @version: 1.0
 */

public with sharing class BICRow implements IRow
{
	//--------------------------------------------------------------------------
	// Private members
	//--------------------------------------------------------------------------
	private List<String> cells;

	//--------------------------------------------------------------------------
	// Constants
	//--------------------------------------------------------------------------
	private static final Map<String, String> countryMapDE = new Map<String, String>
	{
		'Afghanistan' => '004',
		'Ägypten' => '818',
		'Åland' => '248',
		'Albanien' => '008',
		'Algerien' => '012',
		'Amerikanisch-Samoa' => '016',
		'Amerikanische Jungferninseln' => '850',
		'Andorra' => '020',
		'Angola' => '024',
		'Anguilla' => '660',
		'(Sonderstatus durch Antarktis-Vertrag)' => '010',
		'Antigua und Barbuda' => '028',
		'Äquatorialguinea' => '226',
		'Argentinien' => '032',
		'Armenien' => '051',
		'Aruba' => '533',
		'Aserbaidschan' => '031',
		'Äthiopien' => '231',
		'Australien' => '036',
		'Bahamas' => '044',
		'Bahrain' => '048',
		'Bangladesch' => '050',
		'Barbados' => '052',
		'Belarus (Weißrussland)' => '112',
		'Belgien' => '056',
		'Belize' => '084',
		'Benin' => '204',
		'Bermuda' => '060',
		'Bhutan' => '064',
		'Bolivien' => '068',
		'Bonaire, Sint Eustatius und Saba (Niederlande)' => '535',
		'Bosnien und Herzegowina' => '070',
		'Botswana' => '072',
		'Bouvetinsel' => '074',
		'Brasilien' => '076',
		'Britische Jungferninseln' => '092',
		'Britisches Territorium im Indischen Ozean' => '086',
		'Brunei Darussalam' => '096',
		'Bulgarien' => '100',
		'Burkina Faso' => '854',
		'Burma (jetzt Myanmar)' => '104',
		'Burundi' => '108',
		'Chile' => '152',
		'China, Volksrepublik' => '156',
		'Cookinseln' => '184',
		'Costa Rica' => '188',
		'Côte d’Ivoire (Elfenbeinküste)' => '384',
		'Curaçao' => '531',
		'Dänemark' => '208',
		'Deutschland' => '276',
		'Dominica' => '212',
		'Dominikanische Republik' => '214',
		'Dschibuti' => '262',
		'Ecuador' => '218',
		'El Salvador' => '222',
		'Eritrea' => '232',
		'Estland' => '233',
		'Falklandinseln' => '238',
		'Färöer' => '234',
		'Fidschi' => '242',
		'Finnland' => '246',
		'Frankreich' => '250',
		'(europ. Frankreich ohne Übersee-Départements)' => '249',
		'Französisch-Guayana' => '254',
		'Französisch-Polynesien' => '258',
		'Französische Süd- und Antarktisgebiete' => '260',
		'Gabun' => '266',
		'Gambia' => '270',
		'Georgien' => '268',
		'Ghana' => '288',
		'Gibraltar' => '292',
		'Grenada' => '308',
		'Griechenland' => '300',
		'Grönland' => '304',
		'Guadeloupe' => '312',
		'Guam' => '316',
		'Guatemala' => '320',
		'Guernsey (Kanalinsel)' => '831',
		'Guinea' => '324',
		'Guinea-Bissau' => '624',
		'Guyana' => '328',
		'Haiti' => '332',
		'Heard und McDonaldinseln' => '334',
		'Honduras' => '340',
		'Hongkong' => '344',
		'Indien' => '356',
		'Indonesien' => '360',
		'Insel Man' => '833',
		'Irak' => '368',
		'Iran, Islamische Republik' => '364',
		'Irland' => '372',
		'Island' => '352',
		'Israel' => '376',
		'Italien' => '380',
		'Jamaika' => '388',
		'Japan' => '392',
		'Jemen' => '887',
		'Jersey (Kanalinsel)' => '832',
		'Jordanien' => '400',
		'Jugoslawien (ehemalig)' => '891',
		'Kaimaninseln' => '136',
		'Kambodscha' => '116',
		'Kamerun' => '120',
		'Kanada' => '124',
		'Kap Verde' => '132',
		'Kasachstan' => '398',
		'Katar' => '634',
		'Kenia' => '404',
		'Kirgisistan' => '417',
		'Kiribati' => '296',
		'Kokosinseln' => '166',
		'Kolumbien' => '170',
		'Komoren' => '174',
		'Kongo, Demokratische Republik (ehem. Zaire)' => '180',
		'Republik Kongo' => '178',
		'Korea, Demokratische Volksrepublik (Nordkorea)' => '408',
		'Korea, Republik (Südkorea)' => '410',
		'Kroatien' => '191',
		'Kuba' => '192',
		'Kuwait' => '414',
		'Laos, Demokratische Volksrepublik' => '418',
		'Lesotho' => '426',
		'Lettland' => '428',
		'Libanon' => '422',
		'Liberia' => '430',
		'Libyen' => '434',
		'Liechtenstein' => '438',
		'Litauen' => '440',
		'Luxemburg' => '442',
		'Macao' => '446',
		'Madagaskar' => '450',
		'Malawi' => '454',
		'Malaysia' => '458',
		'Malediven' => '462',
		'Mali' => '466',
		'Malta' => '470',
		'Marokko' => '504',
		'Marshallinseln' => '584',
		'Martinique' => '474',
		'Mauretanien' => '478',
		'Mauritius' => '480',
		'Mayotte' => '175',
		'Mazedonien' => '807',
		'Mexiko' => '484',
		'Mikronesien' => '583',
		'Moldawien (Republik Moldau)' => '498',
		'Monaco' => '492',
		'Mongolei' => '496',
		'Montenegro' => '499',
		'Montserrat' => '500',
		'Mosambik' => '508',
		'Myanmar (Burma)' => '104',
		'Namibia' => '516',
		'Nauru' => '520',
		'Nepal' => '524',
		'Neukaledonien' => '540',
		'Neuseeland' => '554',
		'Neutrale Zone (Saudi-Arabien und Irak bis 1993)' => '536',
		'Nicaragua' => '558',
		'Niederlande' => '528',
		'Niederländische Antillen (ehemalig)' => '530',
		'Niger' => '562',
		'Nigeria' => '566',
		'Niue' => '570',
		'Nördliche Marianen' => '580',
		'Norfolkinsel' => '574',
		'Norwegen' => '578',
		'Oman' => '512',
		'Österreich' => '040',
		'Osttimor (Timor-Leste)' => '626',
		'Pakistan' => '586',
		'Staat Palästina[5]' => '275',
		'Palau' => '585',
		'Panama' => '591',
		'Papua-Neuguinea' => '598',
		'Paraguay' => '600',
		'Peru' => '604',
		'Philippinen' => '608',
		'Pitcairninseln' => '612',
		'Polen' => '616',
		'Portugal' => '620',
		'Puerto Rico' => '630',
		'Réunion' => '638',
		'Ruanda' => '646',
		'Rumänien' => '642',
		'Russische Föderation' => '643',
		'Salomonen' => '090',
		'Saint-Barthélemy' => '652',
		'Saint-Martin (franz. Teil)' => '663',
		'Sambia' => '894',
		'Samoa' => '882',
		'San Marino' => '674',
		'São Tomé und Príncipe' => '678',
		'Saudi-Arabien' => '682',
		'Schweden' => '752',
		'Schweiz (Confoederatio Helvetica)' => '756',
		'Senegal' => '686',
		'Serbien' => '688',
		'Serbien und Montenegro (ehemalig)' => '891',
		'Seychellen' => '690',
		'Sierra Leone' => '694',
		'Simbabwe' => '716',
		'Singapur' => '702',
		'Sint Maarten (niederl. Teil)' => '534',
		'Slowakei' => '703',
		'Slowenien' => '705',
		'Somalia' => '706',
		'Spanien' => '724',
		'Sri Lanka' => '144',
		'St. Helena' => '654',
		'St. Kitts und Nevis' => '659',
		'St. Lucia' => '662',
		'Saint-Pierre und Miquelon' => '666',
		'St. Vincent und die Grenadinen' => '670',
		'Südafrika' => '710',
		'Sudan' => '729',
		'Südgeorgien und die Südlichen Sandwichinseln' => '239',
		'Südsudan' => '728',
		'Suriname' => '740',
		'Svalbard und Jan Mayen' => '744',
		'Swasiland' => '748',
		'Syrien, Arabische Republik' => '760',
		'Tadschikistan' => '762',
		'Republik China (Taiwan)' => '158',
		'Tansania, Vereinigte Republik' => '834',
		'Thailand' => '764',
		'Togo' => '768',
		'Tokelau' => '772',
		'Tonga' => '776',
		'Trinidad und Tobago' => '780',
		'Tschad' => '148',
		'Tschechische Republik' => '203',
		'Tschechoslowakei (ehemalig)' => '200',
		'Tunesien' => '788',
		'Türkei' => '792',
		'Turkmenistan' => '795',
		'Turks- und Caicosinseln' => '796',
		'Tuvalu' => '798',
		'UdSSR (jetzt: Russische Föderation)' => '810',
		'Uganda' => '800',
		'Ukraine' => '804',
		'Ungarn' => '348',
		'United States Minor Outlying Islands' => '581',
		'Uruguay' => '858',
		'Usbekistan' => '860',
		'Vanuatu' => '548',
		'Vatikanstadt' => '336',
		'Venezuela' => '862',
		'Vereinigte Arabische Emirate' => '784',
		'Vereinigte Staaten von Amerika' => '840',
		'Vereinigtes Königreich Großbritannien und Nordirland' => '826',
		'Vietnam' => '704',
		'Wallis und Futuna' => '876',
		'Weihnachtsinsel' => '162',
		'Westsahara' => '732',
		'Zaire (jetzt Demokratische Republik Kongo)' => '180',
		'Zentralafrikanische Republik' => '140',
		'Zypern' => '196'
	};

	private static final Map<String, String> countryMapEN = new Map<String, String>
	{
		'Afghanistan' => '004',
		'Albania' => '008',
		'Antarctica' => '010',
		'Algeria' => '012',
		'American Samoa' => '016',
		'Andorra' => '020',
		'Angola' => '024',
		'Antigua and Barbuda' => '028',
		'Azerbaijan' => '031',
		'Argentina' => '032',
		'Australia' => '036',
		'Austria' => '040',
		'Bahamas' => '044',
		'Bahrain' => '048',
		'Bangladesh' => '050',
		'Armenia' => '051',
		'Barbados' => '052',
		'Belgium' => '056',
		'Bermuda' => '060',
		'Bhutan' => '064',
		'Bolivia, Plurinational State of' => '068',
		'Bosnia and Herzegovina' => '070',
		'Botswana' => '072',
		'Bouvet Island' => '074',
		'Brazil' => '076',
		'Belize' => '084',
		'British Indian Ocean Territory' => '086',
		'Solomon Islands' => '090',
		'Virgin Islands, British' => '092',
		'Brunei Darussalam' => '096',
		'Bulgaria' => '100',
		'Myanmar' => '104',
		'Burundi' => '108',
		'Belarus' => '112',
		'Cambodia' => '116',
		'Cameroon' => '120',
		'Canada' => '124',
		'Cape Verde' => '132',
		'Cayman Islands' => '136',
		'Central African Republic' => '140',
		'Sri Lanka' => '144',
		'Chad' => '148',
		'Chile' => '152',
		'China' => '156',
		'Taiwan, Province of China' => '158',
		'Christmas Island' => '162',
		'Cocos (Keeling) Islands' => '166',
		'Colombia' => '170',
		'Comoros' => '174',
		'Mayotte' => '175',
		'Congo' => '178',
		'Congo, the Democratic Republic of the' => '180',
		'Cook Islands' => '184',
		'Costa Rica' => '188',
		'Croatia' => '191',
		'Cuba' => '192',
		'Cyprus' => '196',
		'Czech Republic' => '203',
		'Benin' => '204',
		'Denmark' => '208',
		'Dominica' => '212',
		'Dominican Republic' => '214',
		'Ecuador' => '218',
		'El Salvador' => '222',
		'Equatorial Guinea' => '226',
		'Ethiopia' => '231',
		'Eritrea' => '232',
		'Estonia' => '233',
		'Faroe Islands' => '234',
		'Falkland Islands (Malvinas)' => '238',
		'South Georgia and the South Sandwich Islands' => '239',
		'Fiji' => '242',
		'Finland' => '246',
		'Åland Islands' => '248',
		'France' => '250',
		'French Guiana' => '254',
		'French Polynesia' => '258',
		'French Southern Territories' => '260',
		'Djibouti' => '262',
		'Gabon' => '266',
		'Georgia' => '268',
		'Gambia' => '270',
		'Palestine, State of' => '275',
		'Germany' => '276',
		'Ghana' => '288',
		'Gibraltar' => '292',
		'Kiribati' => '296',
		'Greece' => '300',
		'Greenland' => '304',
		'Grenada' => '308',
		'Guadeloupe' => '312',
		'Guam' => '316',
		'Guatemala' => '320',
		'Guinea' => '324',
		'Guyana' => '328',
		'Haiti' => '332',
		'Heard Island and McDonald Islands' => '334',
		'Holy See (Vatican City State)' => '336',
		'Honduras' => '340',
		'Hong Kong' => '344',
		'Hungary' => '348',
		'Iceland' => '352',
		'India' => '356',
		'Indonesia' => '360',
		'Iran, Islamic Republic of' => '364',
		'Iraq' => '368',
		'Ireland' => '372',
		'Israel' => '376',
		'Italy' => '380',
		'Côte d\'Ivoire' => '384',
		'Jamaica' => '388',
		'Japan' => '392',
		'Kazakhstan' => '398',
		'Jordan' => '400',
		'Kenya' => '404',
		'Korea, Democratic People\'s Republic of' => '408',
		'Korea, Republic of' => '410',
		'Kuwait' => '414',
		'Kyrgyzstan' => '417',
		'Lao People\'s Democratic Republic' => '418',
		'Lebanon' => '422',
		'Lesotho' => '426',
		'Latvia' => '428',
		'Liberia' => '430',
		'Libya' => '434',
		'Liechtenstein' => '438',
		'Lithuania' => '440',
		'Luxembourg' => '442',
		'Macao' => '446',
		'Madagascar' => '450',
		'Malawi' => '454',
		'Malaysia' => '458',
		'Maldives' => '462',
		'Mali' => '466',
		'Malta' => '470',
		'Martinique' => '474',
		'Mauritania' => '478',
		'Mauritius' => '480',
		'Mexico' => '484',
		'Monaco' => '492',
		'Mongolia' => '496',
		'Moldova, Republic of' => '498',
		'Montenegro' => '499',
		'Montserrat' => '500',
		'Morocco' => '504',
		'Mozambique' => '508',
		'Oman' => '512',
		'Namibia' => '516',
		'Nauru' => '520',
		'Nepal' => '524',
		'Netherlands' => '528',
		'Curaçao' => '531',
		'Aruba' => '533',
		'Sint Maarten (Dutch part)' => '534',
		'Bonaire, Sint Eustatius and Saba' => '535',
		'New Caledonia' => '540',
		'Vanuatu' => '548',
		'New Zealand' => '554',
		'Nicaragua' => '558',
		'Niger' => '562',
		'Nigeria' => '566',
		'Niue' => '570',
		'Norfolk Island' => '574',
		'Norway' => '578',
		'Northern Mariana Islands' => '580',
		'United States Minor Outlying Islands' => '581',
		'Micronesia, Federated States of' => '583',
		'Marshall Islands' => '584',
		'Palau' => '585',
		'Pakistan' => '586',
		'Panama' => '591',
		'Papua New Guinea' => '598',
		'Paraguay' => '600',
		'Peru' => '604',
		'Philippines' => '608',
		'Pitcairn' => '612',
		'Poland' => '616',
		'Portugal' => '620',
		'Guinea-Bissau' => '624',
		'Timor-Leste' => '626',
		'Puerto Rico' => '630',
		'Qatar' => '634',
		'Réunion' => '638',
		'Romania' => '642',
		'Russian Federation' => '643',
		'Rwanda' => '646',
		'Saint Barthélemy' => '652',
		'Saint Helena, Ascension and Tristan da Cunha' => '654',
		'Saint Kitts and Nevis' => '659',
		'Anguilla' => '660',
		'Saint Lucia' => '662',
		'Saint Martin (French part)' => '663',
		'Saint Pierre and Miquelon' => '666',
		'Saint Vincent and the Grenadines' => '670',
		'San Marino' => '674',
		'Sao Tome and Principe' => '678',
		'Saudi Arabia' => '682',
		'Senegal' => '686',
		'Serbia' => '688',
		'Seychelles' => '690',
		'Sierra Leone' => '694',
		'Singapore' => '702',
		'Slovakia' => '703',
		'Viet Nam' => '704',
		'Slovenia' => '705',
		'Somalia' => '706',
		'South Africa' => '710',
		'Zimbabwe' => '716',
		'Spain' => '724',
		'South Sudan' => '728',
		'Sudan' => '729',
		'Western Sahara' => '732',
		'Suriname' => '740',
		'Svalbard and Jan Mayen' => '744',
		'Swaziland' => '748',
		'Sweden' => '752',
		'Switzerland' => '756',
		'Syrian Arab Republic' => '760',
		'Tajikistan' => '762',
		'Thailand' => '764',
		'Togo' => '768',
		'Tokelau' => '772',
		'Tonga' => '776',
		'Trinidad and Tobago' => '780',
		'United Arab Emirates' => '784',
		'Tunisia' => '788',
		'Turkey' => '792',
		'Turkmenistan' => '795',
		'Turks and Caicos Islands' => '796',
		'Tuvalu' => '798',
		'Uganda' => '800',
		'Ukraine' => '804',
		'Macedonia, the former Yugoslav Republic of' => '807',
		'Egypt' => '818',
		'United Kingdom' => '826',
		'Guernsey' => '831',
		'Jersey' => '832',
		'Isle of Man' => '833',
		'Tanzania, United Republic of' => '834',
		'United States' => '840',
		'Virgin Islands, U.S.' => '850',
		'Burkina Faso' => '854',
		'Uruguay' => '858',
		'Uzbekistan' => '860',
		'Venezuela, Bolivarian Republic of' => '862',
		'Wallis and Futuna' => '876',
		'Samoa' => '882',
		'Yemen' => '887',
		'Zambia' => '894'
	};

	public static final String dateFormat = 'dd.MM.yyyy';

	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	public BICRow(Stock__c sim, String contractDuration)
	{
		cells = new List<String>();
		// event_dt
		cells.add(getStateChangeDate(sim));
		// event_type
		cells.add(getSM2MStatus(sim));
		// customer_name
		cells.add(sim.Account__r.Name);
		// customer_id
		cells.add(sim.Account__c);
		// si_id
		cells.add(sim.Id);
		// iccid
		cells.add(sim.ICCID__c);
		// imsi
		cells.add(sim.IMSI__c);
		// msisdn
		cells.add(sim.MSISDN__c);
		//product_id
		// TODO This should be standard product id if exists
		cells.add(sim.Article_Info_Set__c);
		//product_name
		// TODO This should be standard product name
		cells.add(sim.Article_Info_Set__r.Name);
		//customer_address_zip
		cells.add(sim.Account__r.BillingPostalCode);
		//customer_address_state
		cells.add(sim.Account__r.BillingState);
		//customer_address_street
		cells.add(sim.Account__r.BillingStreet);
		//customer_address_house_nr
		cells.add(getHouseNumber(sim.Account__r.BillingStreet));
		//customer_address_country
		cells.add(sim.Account__r.BillingCountry == null ? '' : toCountry(sim.Account__r.BillingCountry));
		//customer_telephone
		cells.add(sim.Account__r.Phone);
		//customer_fax_number
		cells.add(sim.Account__r.Fax);
		//customer_email_address
		cells.add(sim.Account__r.Company_Email__c);
		//min_contract_duration
		// Max contract term in product bundle
		cells.add(contractDuration);
		//dis_vo_number
		cells.add(sim.Account__r.VO_Number__c);
		//spin_id
		cells.add(sim.Account__r.SPIN_ID__c);
		//framework_contract_no
		cells.add(sim.Third_Upload_File__r.Order_Request__r.Frame_Contract__r.Name);
		//sim_card_usage_classification_id
		cells.add('');
		//sim_card_usage_classification_name
		cells.add('');
	}

	//--------------------------------------------------------------------------
	// Get cells
	//--------------------------------------------------------------------------
	public List<String> getCells()
	{
		return cells;
	}

	//--------------------------------------------------------------------------
	// Convert decimal to string
	//--------------------------------------------------------------------------
	private static String toString(Decimal num)
	{
		return num != null ?
			String.valueOf(num) : null;
	}

	//--------------------------------------------------------------------------
	// Convert country
	//--------------------------------------------------------------------------
	private static String toCountry(String country)
	{
		String returnValue = '';

		if (country != null)
		{
			if (countryMapEN.get(country) != null)
			{
				returnValue = countryMapEN.get(country);
			}
			else if (countryMapDE.get(country) != null)
			{
				returnValue = countryMapDE.get(country);
			}
			else
			{
				returnValue = '000';
			}
		}

		return returnValue;
	}

	//--------------------------------------------------------------------------
	// Try to get house number from the adress return empty string if failed
	//--------------------------------------------------------------------------
	private static String getHouseNumber(String street)
	{
		try
		{
			String[] streetSplit = street.split(' ');
			return String.valueOf(Integer.valueOf(streetSplit[streetSplit.size() -1]));
		}
		catch(Exception ex)
		{
			System.debug(LoggingLevel.ERROR, ex.getMessage());
			return '';
		}
	}

	//--------------------------------------------------------------------------
	// Get state change date
	//--------------------------------------------------------------------------
	private static String getStateChangeDate(Stock__c sim)
	{
		Boolean wasEverDeactivated = false;

		if (sim.HistoryTracking__c != null)
		{
			wasEverDeactivated = sim.HistoryTracking__c.containsIgnoreCase(Constants.DEACTIVATED) || sim.HistoryTracking__c.containsIgnoreCase(Constants.SUSPENDED) || 
								sim.HistoryTracking__c.containsIgnoreCase(Constants.RETIRED);
		}

		if (!wasEverDeactivated)
		{
			if (sim.Commercial_Date__c != null && sim.Activation_Date__c != null && sim.Commercial_Date__c > sim.Activation_Date__c)
			{
				return sim.Commercial_Date__c.format(dateFormat);
			}
			else
			{
				return sim.State_Change_Date__c == null ? sim.LastModifiedDate.format(dateFormat) : sim.State_Change_Date__c.format(dateFormat);
			}
		}
		else
		{
			return sim.State_Change_Date__c == null ? sim.LastModifiedDate.format(dateFormat) : sim.State_Change_Date__c.format(dateFormat);	
		}
	}

	//--------------------------------------------------------------------------
	// Get SM2M status
	//--------------------------------------------------------------------------
	private static String getSM2MStatus(Stock__c sim)
	{
		return sim.SM2M_Status__c == Constants.ACTIVE
				? 'ACTIVATED'
				: sim.SM2M_Status__c;
	}
}