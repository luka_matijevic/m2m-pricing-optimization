global class CustomerMonthlyBillService {
    WebService static String createBills(Id dailyBillId, Boolean forOneBillingAccount, String billingAccountId, boolean createTicketReports, boolean createETLReports) {
    	try {
    	    if(createTicketReports)
                System.enqueueJob(new AccountTicketsReportSchedule());
                
    	    Database.executeBatch(new CustomerBillBatch(String.valueOf(dailyBillId), false, System.now().date(), System.now().date(), forOneBillingAccount, billingAccountId), 1);
		    
            if (createETLReports)
                Database.executeBatch(new BatchCreateEtlReports(), 1);
    		

            String response = 'Customer bills are getting created.';
    		
    		return response;
    	} catch(Exception e){
    		return e.getMessage();
    	}
    }
}