public with sharing class ProductConfigurationLineItems
{
	public class LineItem
	{
		public String name;
		public Decimal price;
	}

	public Id productConfigurationId;
	public String name;
	public String originalProductName;
	public Integer contractTerm;
	public Id productConfigurationOrderId;
	public Integer orderRequestQuantity;
	public LineItem[] oneTimeLineItems;
	public LineItem[] recurringLineItems;
}