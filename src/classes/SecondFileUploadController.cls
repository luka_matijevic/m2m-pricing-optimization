/**
a class that is used for controling the uplaods of the second upload file. It save the uplaoded file as attachment to the OrderRequest record
and inserts it. After insert is completed class schedules a batch job to generate SIM assets from the data available in the file.
*/
public with sharing class SecondFileUploadController {

	public SRF__c parent {get;set;}

    public Attachment attach {get;set;}

    public Id acc {get; set;}

    public SecondFileUploadController(ApexPages.StandardController stdController)
    {
        attach = new Attachment();
        this.parent = (SRF__c)stdController.getRecord();
        acc =  [select account__c from SRF__c where id = :parent.id].account__c;
    }


    public ApexPages.Pagereference upload()
    {

        try
        {

        	attach.ParentId = parent.Id;
        	insert attach;

        	List<List<String>> simList = CSVReader.readIETFRFC4180CSVFile(attach.Body);

        	//Frame_Contract__c frameContract = [SELECT ID, NAME, Product_Bundle__c FROM Frame_Contract__c where id in (SELECT Frame_Contract__c from csord__Order_request__c where id =: parentId)];


        	/*List<cscfga__Attribute__c> attributeList =
        	[select cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c in
    		(select id from cscfga__Product_Configuration__c where cscfga__Product_Bundle__c = :frameContract.Product_Bundle__c)
     		and name = 'Article Info Set'];*/

			/*List<string> aisIdList = new List<string>();
			for(cscfga__Attribute__c attribute : attributeList)
			{
				aisIdList.add(attribute.cscfga__Value__c);
			}*/

			Id articleId =
			[select name from Article_Info_Set__c where id = :[SELECT Id, SIM_Type__c FROM SRF__c where Id = :parent.Id].SIM_Type__c and Article_Type__c = 'SIM'].Id;



			Database.executeBatch(new SecondUploadFileBatchParser(articleId, simList, parent.Id, acc));

			/*ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Creation of assests is in progress you will be notified once it is completed.'));
        	return null;*/
        	return new ApexPages.StandardController(parent).view();
        }
        catch(Exception ex)
        {
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'There was an error while uploading the file: ' + ex.getMessage()));
			return null;
        }
    }
}