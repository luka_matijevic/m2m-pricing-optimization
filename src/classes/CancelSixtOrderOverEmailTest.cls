@isTest
private class CancelSixtOrderOverEmailTest {
	
	public static testMethod void testCancelling() {
        Account account = new Account(
        	Name = 'Test3',
            Type = 'Business'
        	);
        	
        insert account;
        
        List <RecordType> recTypeSixt = [SELECT Id FROM RecordType where Name = 'SIXT' AND sObjecttype = 'Case']; //need to correct, RecordType can be fetched from cache (Adam wrote for SIXT)
        Id sixtRecordTypeId = ((RecordType)recTypeSixt.get(0)).Id;
        
        Case c = Test_Util.createCase('Service','Sixt','Open', account.id, 'Test Demo111', '24', 'Standard', 'Email','High', 'test@t.com');
        c.Sixt_Lvid__c = '12345678';
        c.Sixt_Additional_details__c = 'test details';
        c.RecordTypeId = sixtRecordTypeId;
        insert c;
        
        c = Test_Util.createCase('Service','Sixt','Open', account.id, 'Test Demo111', '24', 'Standard', 'Email','High', 'test@t.com');
        c.Sixt_Lvid__c = '123456789';
        c.Sixt_Additional_details__c = 'test details';
        c.RecordTypeId = sixtRecordTypeId;
        insert c;
        Test.startTest();
        
		Messaging.InboundEmail email = new Messaging.InboundEmail();
        email.Subject = 'Cancellation LVID: 12345678';
        email.plainTextBody = 'test body';
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        
        CancelSixtOrderOverEmail csoe = new CancelSixtOrderOverEmail();
        Messaging.InboundEmailResult result = csoe.handleInboundEmail(email, envelope);        
        
        CancelSixtOrderOverEmail csoe2 = new CancelSixtOrderOverEmail();
        Messaging.InboundEmail email_term = new Messaging.InboundEmail();
        email_term.Subject = 'nothing: 123456789';
        email_term.plainTextBody = 'Termination date: 21.02.1989';
        Messaging.InboundEmailResult result2 = csoe2.handleInboundEmail(email_term, envelope);     
        
        Test.stopTest();
	}	
}