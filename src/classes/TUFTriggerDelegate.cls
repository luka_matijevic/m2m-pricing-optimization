public class TUFTriggerDelegate extends TriggerHandler.DelegateBase {


    list<Stock__c> stkRecordsToUpdate = new list<Stock__c>();

    public override void prepareBefore() {
    
    }

    public override void prepareAfter() {
    
        if( trigger.isUpdate){
            /* Start : Updating Order Reuqest on Sim, when Order request changes on TUF*/
			map<id,Third_Upload_File__c> oldMap = (map<id,Third_Upload_File__c>) trigger.oldMap;
			set<id> orderRequestChangedTUFs = new set<id>();
			for(Third_Upload_File__c tuf: (list<Third_Upload_File__c>) trigger.new){
			    if(tuf.Order_Request__c != null && oldMap.get(tuf.id) != null && tuf.Order_REquest__c != oldMap.get(tuf.id).Order_REquest__c){
			        orderRequestChangedTUFs.add(tuf.id);
			    }
			}
			
			stkRecordsToUpdate = [select id from Stock__C where Third_Upload_File__c In :orderRequestChangedTUFs];
            /* End : Updating Order Reuqest on Sim, when Order request changes on TUF*/
        }
        
    }

    public override void beforeInsert(sObject o) {
    }
    
    public override void beforeUpdate(sObject old, sObject o) {
        
    }
    
    public override void beforeDelete(sObject o) {
    }

    public override void afterInsert(sObject o) {
    }

    public override void afterUpdate(sObject old, sObject o) {
        
    }

    public override void afterDelete(sObject o) {
    }

    public override void afterUndelete(sObject o) {
    }

    public override void finish() {
       if(stkRecordsToUpdate.size() > 0){
           update stkRecordsToUpdate;
       }
    }
}