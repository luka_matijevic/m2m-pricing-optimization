public with sharing class UnitScheduleTriggerDelegate extends TriggerHandler.DelegateBase {

  Map<Id, Opportunity> parentOpportunities = new Map<Id, Opportunity>();

  public override void prepareBefore() {
    // do any preparation here – bulk loading of data etc
    if (Trigger.isInsert) {
    	for (SObject obj : Trigger.new) {
    		Unit_Schedule__c schedule = (Unit_Schedule__c) obj;
    		parentOpportunities.put(schedule.Opportunity__c, new Opportunity(Id = schedule.Opportunity__c));
    	}
    }
  }

  public override void prepareAfter() {
    // do any preparation here – bulk loading of data etc
    if (Trigger.isInsert) {
    	for (SObject obj : Trigger.new) {
    		Unit_Schedule__c schedule = (Unit_Schedule__c) obj;
    		parentOpportunities.put(schedule.Opportunity__c, new Opportunity(Id = schedule.Opportunity__c));
    	}
    }
    else if (Trigger.isUpdate || Trigger.isDelete) {      	
    	for (SObject obj : Trigger.old) {
    		Unit_Schedule__c schedule = (Unit_Schedule__c) obj;
    		parentOpportunities.put(schedule.Opportunity__c, new Opportunity(Id = schedule.Opportunity__c));
    	}
    }
  }

  public override void beforeInsert(sObject o) {
    // Apply before insert logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
  }

  public override void beforeUpdate(sObject old, sObject o) {
    // Apply before update logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
  }

  public override void beforeDelete(sObject o) {
    // Apply before delete logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
  }

  public override void afterInsert(sObject o) {
    // Apply after insert logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
  }

  public override void afterUpdate(sObject old, sObject o) {
    // Apply after update logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
  }

  public override void afterDelete(sObject o) {
    // Apply after delete logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
  }

  public override void afterUndelete(sObject o) {
    // Apply after undelete logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
  }

  public override void finish() {
    
    // this should prevent inserting unit schedules to non-legacy opportunities without product bundles
    if (trigger.isBefore && trigger.isInsert) {
    	
    	List<cscfga__Product_Bundle__c> synchronizedBundles = [
    		Select 
    			Id, cscfga__Opportunity__c 
    		from 
    			cscfga__Product_Bundle__c 
    		where 
    			cscfga__Opportunity__c in :parentOpportunities.keySet()
    		and
    			cscfga__Synchronised_with_Opportunity__c = true
    	];

    	Set<Id> legacyOpportunityIds = new Map<Id, Opportunity>([
    		Select 
    			Id
    		from 
    			Opportunity 
    		where 
    			Id in :parentOpportunities.keySet() 
    		and 
    			RecordTypeId in (Select Id from RecordType where DeveloperName = 'Legacy')
    	]).keySet(); 
    	    	
    	Map<Id, Boolean> opportunityHasSynchronizedBundle = new Map<Id, Boolean>();
    	for (cscfga__Product_Bundle__c bundle : synchronizedBundles) {
    		opportunityHasSynchronizedBundle.put(bundle.cscfga__Opportunity__c, true);
    	}
    	
    	for (SObject obj : Trigger.new) {
    		Unit_Schedule__c schedule = (Unit_Schedule__c) obj;
    		if (opportunityHasSynchronizedBundle.get(schedule.Opportunity__c) != true && !legacyOpportunityIds.contains(schedule.Opportunity__c)) {
    			schedule.addError('Opportunity has no synchronized product bundle.');
    		}
    	}
    }
    
    if (trigger.isAfter) {
    	for (Opportunity opportunity : parentOpportunities.values()) {
      		opportunity.Total_Connectivity_Rev_in_current_year__c = M2MYearlyTotalCalculator.calculateConnectivityRevenue(opportunity.Id, Date.today().year());
      		opportunity.Total_Service_Revenue_in_current_year__c = M2MYearlyTotalCalculator.calculateServiceRevenue(opportunity.Id, Date.today().year());
    	}
    	update parentOpportunities.values();
    }
  }
}