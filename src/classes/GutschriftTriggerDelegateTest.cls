@isTest
private class GutschriftTriggerDelegateTest {

	static testMethod void TriggerTest() {
        
        Daily_Bill__c db = new Daily_Bill__c();
        db.date__c=date.today();
        insert db;
        
        Gutschrift__c g = new Gutschrift__c();
        g.Art__c='6';
        //storno.name='Test';
        g.reason__c='Test';
        //storno.VO_NR__c='1';
        //storno.Einzel_Nettobetrag__c=10;
        g.Daily_Bill__c=db.id;
        g.Invoice_Number__c='10';
        //storno.vat__c=19;
        insert g;
        
        List<Daily_Bill_Item__c> c = [SELECT name, id from Daily_Bill_Item__c];
        system.assert(!c.isEmpty(),'Trigger not working as expected');
        
	}

}