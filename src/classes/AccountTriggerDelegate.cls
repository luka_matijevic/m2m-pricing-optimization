/*
* BillingAcountTriggerDelegate extends Trigger handler to execute calls based on events.
*/
public with sharing class AccountTriggerDelegate extends TriggerHandler.DelegateBase {
    Id acctID;
    Boolean provisionAccount = false;
  
    public override void prepareBefore() {
        // do any preparation here – bulk loading of data etc
		 

    }

    public override void prepareAfter() {
        // do any preparation here – bulk loading of data etc
    }
  public override void beforeInsert(sObject o) {
        // Apply before insert logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method    
    }
    public override void beforeUpdate(sObject old, sObject o) {
        Account oldAcc = (Account)old;
        Account newAcc = (Account)o;
        
        // Apply before update logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
        if (UserInfo.getUserName()== 'o2spin-salesforce@syzygy.de') {
            if (newAcc.TEF_DE_Sales_Channel__c=='M2M' || newAcc.TEF_DE_Sales_Channel__c=='Intern') {
                if (oldAcc.TEF_DE_Sales_Channel__c!='M2M' && oldAcc.TEF_DE_Sales_Channel__c!='Intern') {
                    newAcc.TEF_DE_Sales_Channel__c=oldAcc.TEF_DE_Sales_Channel__c;
                }

            } 
          
            if (oldAcc.BillingCountry != newAcc.BillingCountry) {
            	newAcc.BillingCountry = oldAcc.BillingCountry;
            }

        }
        
        if(oldAcc.Local__c != newAcc.Local__c || oldAcc.Global__c != newAcc.Global__c) {
            provisionAccount = true;
            acctID = newAcc.Id;
        }
        
        if(oldAcc.SM2M_ID_Local__c != newAcc.SM2M_ID_Local__c) {
            newAcc.isProvisionedLocal__c = true;
        }
        
        if(oldAcc.SM2M_ID__c != newAcc.SM2M_ID__c) {
            newAcc.isProvisionedGlobal__c = true;
        }
    }

    public override void beforeDelete(sObject o) {
        // Apply before delete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterInsert(sObject o) {
        // Apply after insert logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterUpdate(sObject old, sObject o) {
        // Apply after update logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterDelete(sObject o) {
        // Apply after delete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void afterUndelete(sObject o) {
        // Apply after undelete logic to this sObject. DO NOT do any SOQL
        // or DML here – store records to be modified in an instance variable
        // which can be processed by the finish() method
    }

    public override void finish() {
       /*if(provisionAccount) {
           provisionAccountMethod(acctID);
       }*/
    }
    
    @future
    private static void provisionAccountMethod(Id accid) {
        if(ValidationHelper.validateProvisionAccountFromTrigger(accid).equalsIgnoreCase('VALID')) {
            csam_t1.ObjectGraphCalloutHandler.createAndSend('Provision Account Message', String.valueOf(accid));
        }
    }
}