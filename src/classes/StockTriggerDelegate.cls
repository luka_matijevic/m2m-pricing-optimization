public with sharing class StockTriggerDelegate extends TriggerHandler.DelegateBase
{
    StockLookups stockLookups;
    Map<Id, Third_Upload_File__c> srfDirectSimDeliveriesMap;
    //Map<Id, Stock__c> contractDurationSims = new Map<Id, Stock__c>();  BIC_Contract_Duration__c Handling
    List<MSISDN__c> msisdnsToUpsert = new List<MSISDN__c>();
     /** 
     * Contract durations should be calculated and added before updates and inserts of sims
     */ 
     Map<Id, Third_Upload_File__c> accountsTufs = new Map<Id, Third_Upload_File__c>();
     List<Activation_plan__c> actPlan = new List<Activation_plan__c>();

    public override void prepareBefore() {
        if (Trigger.isInsert) {     
            this.stockLookups = new StockLookups((List<Stock__c>) Trigger.new);
            this.srfDirectSimDeliveriesMap = selectSrfDirectDeliveriesMap(stockLookups.SrfMap.values());
        }
        

        

        if (Trigger.isInsert || Trigger.isUpdate) {
            
            /* Pavan : Termination clean up start*/
            Map<Id, Third_Upload_File__c> tufMap = new  Map<Id, Third_Upload_File__c>();
            set<id> tufIds = new set<id>();
            for (Stock__c sim : (List<Stock__c>)Trigger.new) {
                if(sim.Third_Upload_File__c != null){
                    tufIds.add(sim.Third_Upload_File__c);
                }

                if(sim.Account__c != null) {
                    System.debug(LoggingLevel.DEBUG, 'asim.Account__c ' + sim.Account__c);
                    accountsTufs.put(sim.Account__c, null);
                }
            }

            System.debug(LoggingLevel.DEBUG, 'accountsTufs size ' + accountsTufs.size());
            
            if(tufIds.size() > 0){
                tufMap = new  Map<Id, Third_Upload_File__c>([select id, Order_request__c from Third_Upload_File__c where Id in:tufIds and Order_request__c != null]);
            }
                        
            map<id,Stock__c> oldMap = (map<id,Stock__c>) trigger.oldMap;
            set<id> OrdrReqIds = new set<id>();
            for (Stock__c sim : (List<Stock__c>)Trigger.new) {
                
                /*Start : Assiging Order Request to SIM*/
                
                if(sim.Third_Upload_File__c != null && tufMap.get(sim.Third_Upload_File__c) != null && sim.GEOTAB_Stock__c == false){
                    if(trigger.isUpdate ){
                        sim.Order_Request__c = tufMap.get(sim.Third_Upload_File__c).Order_Request__c;
                        OrdrReqIds.add(sim.Order_Request__c);
                    }else{
                        sim.Order_Request__c = tufMap.get(sim.Third_Upload_File__c).Order_Request__c;
                        OrdrReqIds.add(sim.Order_Request__c);
                    }
                }
            }
            
            map<id, Order_Request__c> ordReqMap = new map<id,Order_Request__c>();
            if(OrdrReqIds.size() > 0){
                ordReqMap = new map<id,Order_Request__c>([select id,RunTime__C from Order_Request__c where id In:OrdrReqIds]);
            }
            
            
            set<string> endCustomerSpinId = new set<string>();  
            
            for (Stock__c sim : (List<Stock__c>)Trigger.new) {
                Integer RunTime;
                /* Start : Calculating end date */
                if (sim.SM2M_Status__c == 'ACTIVE'){
                    if(sim.RunTime__C == null && sim.Order_Request__C != null && ordReqMap.get(sim.Order_Request__C) != null && ordReqMap.get(sim.Order_Request__C).RunTime__C != null){
                        RunTime = Integer.valueOf(ordReqMap.get(sim.Order_Request__C).RunTime__C);
                    }else if(sim.RunTime__C != null){
                        RunTime = Integer.valueOf(sim.RunTime__C);
                    }
                    if(sim.FC_Type__c != null && RunTime != null){
                        if(trigger.isUpdate){
                            if(sim.VVL_date__c == null){
                                caclulatePotentialTerminationDate(sim,RunTime);
                            }    
                        }else{
                            caclulatePotentialTerminationDate(sim,RunTime);
                        }
                    }else if(RunTime == null){
                        sim.Potential_Termination_Date__c = null;
                    }
                }
                
                /* End : Calculating end date */
                
                
                /*
                BIC_Contract_Duration__c Handling
                if(sim.id != null){
                    this.contractDurationSims.put(sim.id, sim);
                }
                */
                if(sim.End_Customer_SPIN_ID__c != null){
                    endCustomerSpinId.add(sim.End_Customer_SPIN_ID__c);
                }
                
                if(RunTime != null){
                    sim.BIC_Contract_Duration__c = RunTime;
                }
                
            }
            /* Pavan : Termination clean up End*/
            
        
                        
            map<string,Account> endCustList = new map<string,Account>();
            
            if (endCustomerSpinId.size() > 0) {
                for(Account acc : [select id, name,SPIN_ID__c from Account where SPIN_ID__c in :endCustomerSpinId ]){
                    endCustList.put(acc.SPIN_ID__c,acc);
                }
            }             
            
            /*
            BIC_Contract_Duration__c Handling
                SimContractDurationCalculator durationCalc = new SimContractDurationCalculator();
                Map<Id, Integer> contractDurations = durationCalc.calculateContractDurations(contractDurationSims.values());
            */
           //ICCID range type has priority over IMSI range type - order by clause
            Map<Id, SIM_Range__c> rangeMap = new Map<Id, SIM_Range__c>([select Id, Range_start__c, Range_end__c, Account__c, Range_Type__c from SIM_Range__c order by Range_Type__c]);
            for (Stock__c sim : (List<Stock__c>)Trigger.new) {
                /*
                BIC_Contract_Duration__c Handling
                if (contractDurationSims.containsKey(sim.Id)) {
                    sim.BIC_Contract_Duration__c = contractDurations.get(sim.Id);
                }*/
                if (sim.End_Customer_SPIN_ID__c!=null && !endCustList.isEmpty() && endCustList.get(sim.End_Customer_SPIN_ID__c) != null){
                    sim.End_customer__c = endCustList.get(sim.End_Customer_SPIN_ID__c).id;
                }
                for (SIM_Range__c sr : rangeMap.values()){
                     /*20.5.2016 Luka - sim range type
                    if (AdditionalUtils.compareBigNumbers(sr.Range_start__c, sim.IMSI__c)<=0 && 
                        AdditionalUtils.compareBigNumbers(sr.Range_end__c, sim.IMSI__c)>=0){
                            sim.SIM_Range__c = sr.Id;
                            if( null != sr.Account__c && sr.Account__c != sim.Account__c) sim.addError('Account defined in SIM range is different!');
                            break;
                        }
                    */
                    
                    if (null != sr.Range_Type__c && sr.Range_Type__c.equals('ICCID') && 
                        AdditionalUtils.compareBigNumbers(sr.Range_start__c, sim.ICCID__c)<=0 && 
                        AdditionalUtils.compareBigNumbers(sr.Range_end__c, sim.ICCID__c)>=0){
                            sim.SIM_Range__c = sr.Id;
                            if( null != sr.Account__c && sr.Account__c != sim.Account__c) sim.addError('Account defined in SIM range is different!');
                            break;
                        }
                    else if (null != sr.Range_Type__c && sr.Range_Type__c.equals('IMSI') && 
                        AdditionalUtils.compareBigNumbers(sr.Range_start__c, sim.IMSI__c)<=0 && 
                        AdditionalUtils.compareBigNumbers(sr.Range_end__c, sim.IMSI__c)>=0){
                            sim.SIM_Range__c = sr.Id;
                            if( null != sr.Account__c && sr.Account__c != sim.Account__c) sim.addError('Account defined in SIM range is different!');
                            break;
                        }
                    else sim.SIM_Range__c = null;
                }
            }
            
        }
        
        /*Desc. Method to capture Stock field history track in Long Text field. (ritesh.saxena@cloudsensesolutions.com)*/
        /*Checks if new MSISDN is quarantined - prevents update if true*/
        if(Trigger.isBefore && Trigger.isUpdate){
            trackFieldChanges(Trigger.new, Trigger.oldMap);
            system.debug(trigger.new);
            checkQuarantine(Trigger.new, Trigger.oldMap);
        }

        // ADAM MEHTIC - find last created TUF for specific Stock Account
        /************************************************/
        if(accountsTufs != null && accountsTufs.size() > 0) {
            List<Third_Upload_File__c> tufsForAccounts = [SELECT Id, Account__c, CreatedDate, Order_Request__c, Order_Request__r.Frame_Contract_Type__c FROM Third_Upload_File__c WHERE Account__c in :accountsTufs.keySet() AND Order_Request__r.Frame_Contract_Type__c not in ('Trial') ORDER BY CreatedDate DESC];

            //System.debug(LoggingLevel.DEBUG, 'tufsForAccounts size ' + tufsForAccounts.size());

            for(Third_Upload_File__c tufc : tufsForAccounts) {
                //System.debug(LoggingLevel.DEBUG, 'tufc is ' + tufc);
                if(accountsTufs.containsKey(tufc.Account__c)) {
                    Third_Upload_File__c tmpTuf = accountsTufs.get(tufc.Account__c);

                    //System.debug(LoggingLevel.DEBUG, 'tmpTuf found ' + tmpTuf);
                    if(tmpTuf != null) {
                        if(tufc.CreatedDate > tmpTuf.CreatedDate) {
                            //System.debug(LoggingLevel.DEBUG, 'tufc inserted ' + tufc);
                            accountsTufs.put(tufc.Account__c, tufc);
                        }
                    } else {
                        //System.debug(LoggingLevel.DEBUG, 'tufc added ' + tufc);
                        accountsTufs.put(tufc.Account__c, tufc);
                    }
                } else {
                    accountsTufs.put(tufc.Account__c, tufc);
                }
            }
        }
        /**********************************************/

        //Determine if Stock is Activated for the first Time and set checkbox
        if (Trigger.isUpdate){
            for (Stock__c stockNew : (List<Stock__c>)Trigger.new){
                Stock__c stockOld = (Stock__c)Trigger.oldMap.get(stockNew.Id);
                if (stockNew.SM2M_Status__c == Constants.ACTIVE && stockNew.First_Activation_Date__c == null && 
                    (stockOld.SM2M_Status__c == Constants.ACTIVATION_READY || stockOld.SM2M_Status__c == Constants.INACTIVE_NEW)){
                    stockNew.First_Activation__c = true;
                    stockNew.First_Activation_Date__c = System.now();
                }
                else{
                    stockNew.First_Activation__c = false;
                }
            }
        }
    }

    public override void beforeUpdate(sObject old, sObject o){
        Stock__c oldSim = (Stock__c) old;
        Stock__c newSim = (Stock__c) o;
        
        if (oldSim.SM2M_Status__c == Constants.ACTIVE && newSim.SM2M_Status__c == Constants.DEACTIVATED) {
            newSim.Deactivation_Date__c = newSim.State_Change_Date__c; 
        }
        If (newSim.Activation_Date__c == null && newSim.SM2M_Status__c ==  Constants.ACTIVE) {
            newSim.Activation_Date__c =   newSim.State_Change_Date__c;
        }
        



        if(oldSim.SM2M_Status__c != 'ACTIVE' && newSim.SM2M_Status__c == 'ACTIVE'){
            actPlan = updateActivationPlan(newSim);
        }
        
        
    }

    
    /**
     * Deactivation date should be added when a SIM is deactivated
     * MSISDN should be quarantined when a SIM is retired
     */ 

    public override void afterUpdate(sObject old, sObject o) {
       Stock__c oldSim = (Stock__c) old;
        Stock__c newSim = (Stock__c) o;

       
        
        if (newSim.SIM_Range_Origin__c!='Global' &&  oldSim.SM2M_Status__c != Constants.RETIRED && newSim.SM2M_Status__c == Constants.RETIRED && !String.isBlank(newSim.MSISDN__c)){
            msisdnsToUpsert.add 
            (
                new MSISDN__c
                (
                    MSISDN__c = newSim.MSISDN__c,   
                    isReadyForUse__c = false,
                    Date_quarantine_start__c = Date.today(),
                    Date_quarantine_end__c = Date.today().addDays(90)

                )
            );
            
        }
        else if (!String.isBlank(newSim.MSISDN__c))
        {
            msisdnsToUpsert.add 
            (
                new MSISDN__c
                (
                    MSISDN__c = newSim.MSISDN__c,   
                    isReadyForUse__c = true
                )
            );
        }
        
        
            
       

    }

    public override void beforeInsert(sObject o) {
        
        Stock__c sim = (Stock__c)o;
        sim.Name = sim.ICCID__c;

        Datetime changeDate = DateUtils.datetimeOrNow(sim.State_Change_Date__c);
        sim.State_Change_Date__c = changeDate;
        
        if (sim.RecordTypeId == Cache.RecordTypeId('Stock__c.SIM')) {
            if (srfId(sim) == Cache.RecordTypeId('SRF__c.Direct SIM Delivery')) {
                sim.Status__c = 'Reserved';
                sim.Third_Upload_File__c = srfDirectSimDeliveriesMap.get(sim.SRF__c).Id;
            } else if (srfId(sim) == Cache.RecordTypeId('SRF__c.Internal SIM Delivery')) {
                sim.Status__c = 'On Stock';

            } else if (srfId(sim) == Cache.RecordTypeId('SRF__c.Standard SRF')) {
                sim.addError('Standard SRF can not be used to upload stock!');
            }
        }
        
        if (sim.SM2M_Status__c == Constants.ACTIVE && sim.Activation_Date__c == null) {
            sim.Activation_Date__c = changeDate;
        }

        //System.debug(LoggingLevel.DEBUG, 'accountsTufs size before insert ' + accountsTufs.size());

        /*for(Id ks : accountsTufs.keySet()) {
            System.debug(LoggingLevel.DEBUG, 'Account Id ' + ks);
            System.debug(LoggingLevel.DEBUG, 'Tuf object ' + accountsTufs.get(ks));
        }*/

        if(sim.Account__c != null && sim.Third_Upload_File__c == null && sim.GEOTAB_Stock__c == false) {
            //System.debug(LoggingLevel.DEBUG, 'Account not null and TUF is empty ' + sim.Account__c);
            if(accountsTufs.containsKey(sim.Account__c)) {
                //System.debug(LoggingLevel.DEBUG, 'sim.Account__c found ' + sim.Account__c);
                Third_Upload_File__c tmpTuf = accountsTufs.get(sim.Account__c);

                if(tmpTuf != null) {
                    //System.debug(LoggingLevel.DEBUG, 'tmpTuf found ' + tmpTuf);
                    sim.Third_Upload_File__c = tmpTuf.Id;
                    sim.Order_Request__c = tmpTuf.Order_Request__c;
                }
            } 
            /*else
            {
                System.debug(LoggingLevel.DEBUG, 'Account not found in accountsTufs ' + sim.Account__c);
            }*/
        }


    }

    public override void afterInsert(sObject o) {
        Stock__c sim = (Stock__c)o;
        
        if(sim.SM2M_Status__c == 'ACTIVE'){
            actPlan = updateActivationPlan(sim);
        }

    }



    public override void finish()
    {
    
        try 
        {           
            update actPlan;
            upsert msisdnsToUpsert MSISDN__c;

        }
        catch ( DmlException e)
        {
            System.debug( e.getMessage());
        }

        
    }

    private Id srfId(Stock__c sim) {
        if (this.stockLookups != null && this.stockLookups.SrfMap.get(sim.Srf__c) != null) {
            return this.stockLookups.SrfMap.get(sim.Srf__c).RecordTypeId;
        }
        return null;
    }

    /**
     * Builds a SRF Id -> TUF map for among given SRFs that have a direct delivery TUF
     */ 
    private Map<Id, Third_Upload_File__c> selectSrfDirectDeliveriesMap(List<SRF__c> srfs) {
        
        // SRF Id -> TUF
        Map<Id, Third_Upload_File__c> srfTufMap = new Map<Id, Third_Upload_File__c>();
        
        // reversed Order Request -> SRF map
        Map<Id, Id> orderRequestSrfIdMap = new Map<Id, Id>();
        for (SRF__c srf : srfs) {
            if (srf.RecordTypeId == Cache.RecordTypeId('SRF__c.Direct SIM Delivery')) {
                orderRequestSrfIdMap.put(srf.Order_Request__c, srf.Id);
            }
        }

        List<Third_Upload_File__c> directSimDeliveries = [
            SELECT Id, Order_Request__c
            FROM Third_Upload_File__c
            WHERE Order_Request__c IN :orderRequestSrfIdMap.keySet()
        ];

        // finally, fill SRF Id -> TUF map to return
        for (Third_Upload_File__c tuf : directSimDeliveries) {
            srfTufMap.put(orderRequestSrfIdMap.get(tuf.Order_Request__c), tuf);
        }

        return srfTufMap;
    }

    /**
     * Finds all SRF and TUF objects for given sims and generates 
     * (TUF Id)->TUF and (SRF Id)->SRF maps
     */ 
    private class StockLookups {

        Map<Id, Srf__c> SrfMap {get; set;}
        Map<Id, Third_Upload_File__c> TufMap {get; set;}

        public StockLookups(List<Stock__c> sims) {
            
            Set<Id> srfIds = new Set<Id>();
            Set<Id> tufIds = new Set<Id>();
        
            for (Stock__c sim : sims) {
                srfIds.add(sim.SRF__c);
                tufIds.add(sim.Third_Upload_File__c);
            }
            
            this.SrfMap = new Map<Id, SRF__c>([
                SELECT 
                    RecordTypeId, Account__c,  Order_Request__c
                FROM
                    SRF__c
                WHERE
                    Id IN :srfIds 
            ]);

            this.TufMap = new Map<Id, Third_Upload_File__c>([
                SELECT
                    Account__c,
                    Order_Request__r.Account__c,
                    Upload_to_SM2M_Status__c,
                    Order_Request__c
                FROM
                    Third_Upload_File__c
                WHERE 
                    Id IN :tufIds 
            ]);
        }
    }
    
    /*
    * Method to Track SM2M Status change for Stock. Also sets 'Activated date' (if null) on SM2M Status changed to Active
    */  
    private void trackFieldChanges(List<sObject> newList, Map<Id,sObject> oldMap){
        for (Stock__c sNew : (List<Stock__c>)newList) {
            String key='';
            String oldValue = '';
            String newValue='';
            Stock__c sOld = (Stock__c)oldMap.get(sNew.Id);
                       
            if (sNew.SM2M_Status__c != sOld.SM2M_Status__c && sNew.SM2M_Status__c!=null &&
                (sNew.SM2M_Status__c.equalsIgnoreCase('ACTIVE') || 
                sNew.SM2M_Status__c.equalsIgnoreCase('SUSPENDED') || 
                sNew.SM2M_Status__c.equalsIgnoreCase('DEACTIVATED') ||
                sNew.SM2M_Status__c.equalsIgnoreCase('RETIRED'))
                && sNew.State_Change_Date__c != null && sNew.State_Change_Date__c != sOld.State_Change_Date__c) {
                newValue = String.valueOf(sNew.SM2M_Status__c);                       
                if(newValue!=''){
                    key = '' + newValue + '|' + sNew.State_Change_Date__c;
                }               
                if(sNew.HistoryTracking__c==null){
                    sNew.HistoryTracking__c='';
                }
                if(key!='' && !(sNew.HistoryTracking__c.trim()).contains(key.trim())){
                    sNew.HistoryTracking__c =  '' + key + '\n' + (sNew.HistoryTracking__c==null?'' : sNew.HistoryTracking__c);
                }
            }
        }
    }

    public static void caclulatePotentialTerminationDate(Stock__c sim,Integer RunTime){
        if(sim.FC_Type__c == 'Trial' && sim.Activation_Date__c != null){
            sim.Potential_Termination_Date__c = date.valueOf(sim.Activation_Date__c).addMonths(RunTime);
        }else if(sim.FC_Type__c == 'Commercial' ){
            if(sim.Commercial_Date__c != null){
                sim.Potential_Termination_Date__c = date.valueOf(sim.Commercial_Date__c).addMonths(RunTime);
            }else if(sim.Activation_Date__c != null){
                sim.Potential_Termination_Date__c = date.valueOf(sim.Activation_Date__c).addMonths(RunTime);
            }
        }
        
        if(sim.End_date__c != null){
            sim.Potential_Termination_Date__c = sim.End_date__c;
        }
    }

    private void checkQuarantine (List<sObject> newList, Map<Id,sObject> oldMap){
        
        Set<String> msisdnSetToCheck = new Set<String>();

        for (Stock__c sNew : (List<Stock__c>)newList) {
            Stock__c sOld = (Stock__c)oldMap.get(sNew.Id);

            if (sNew.MSISDN__c != sOld.MSISDN__c && null != sNew.MSISDN__c)
            {
                msisdnSetToCheck.add(sNew.MSISDN__c);
            }

        }
        Map<Id, MSISDN__c> msisdnMap = new Map<Id, MSISDN__c> ([select Id, MSISDN__c, Date_quarantine_end__c from MSISDN__c where isReadyForUse__c = false and MSISDN__c in :msisdnSetToCheck]);
        //MSISDN__c as key

        Map<String, DateTime> quarantinedMsisdnMap = new Map<String, DateTime>();

        for (MSISDN__c m : msisdnMap.values()) 
            quarantinedMsisdnMap.put(m.MSISDN__c, m.Date_quarantine_end__c);
            
        System.debug(LoggingLevel.INFO, 'quarantinedMsisdnMap size '+ quarantinedMsisdnMap.size());
        System.debug(LoggingLevel.INFO, '(List<Stock__c>)newList size '+ newList.size());
        
        for (Stock__c sNew : (List<Stock__c>)newList) {
            System.debug(LoggingLevel.INFO, 'sNew.MSISDN__c '+ sNew.MSISDN__c);
            System.debug(LoggingLevel.INFO, 'quarantinedMsisdnMap.get(sNew.MSISDN__c) '+ quarantinedMsisdnMap.get(sNew.MSISDN__c));
            
            String dateMessage = '';
            
            if(quarantinedMsisdnMap.containsKey(sNew.MSISDN__c) && quarantinedMsisdnMap.get(sNew.MSISDN__c) != null) {
                dateMessage = quarantinedMsisdnMap.get(sNew.MSISDN__c).format();
            } else {
                dateMessage = 'END DATE IS EMPTY';
            }
            
            if(sNew.MSISDN__c != null && quarantinedMsisdnMap.containsKey(sNew.MSISDN__c)) {
                sNew.addError(' MSISDN is quarantined until ' + dateMessage);
            }
        }

    }


    public List<Activation_plan__c> updateActivationPlan(sObject o){
        system.debug(o);
        Stock__c sim = (Stock__c)o;
        system.debug(sim);
        Order_Request__c order = [SELECT Id, Opportunity__c FROM Order_Request__c WHERE Id = :sim.Order_Request__c];
        system.debug(order);
        List<Activation_plan__c> actPlan = [SELECT Id, Year__c, Real_SIM_Activation_January__c, Real_SIM_Activation_February__c, Real_SIM_Activation_March__c,
                                            Real_SIM_Activation_April__c, Real_SIM_Activation_May__c, Real_SIM_Activation_June__c,
                                            Real_SIM_Activation_July__c, Real_SIM_Activation_August__c, Real_SIM_Activation_September__c,
                                            Real_SIM_Activation_October__c, Real_SIM_Activation_November__c, Real_SIM_Activation_December__c
                                            FROM Activation_plan__c WHERE Opportunity__c = :order.Opportunity__c];
        system.debug(actPlan);

        for(Activation_plan__c a : actPlan){
            if(a.Year__c == String.valueOf(sim.Activation_Date__c.year())){

                system.debug(a);
                if(sim.Activation_Date__c.month() == 1) {
                    a.Real_SIM_Activation_January__c++;
                }
                else  if(sim.Activation_Date__c.month() == 2) {
                    a.Real_SIM_Activation_February__c++;
                }
                else  if(sim.Activation_Date__c.month() == 3) {
                    a.Real_SIM_Activation_March__c++;
                }
                else  if(sim.Activation_Date__c.month() == 4) {
                    a.Real_SIM_Activation_April__c++;
                }
                else  if(sim.Activation_Date__c.month() == 5) {
                    a.Real_SIM_Activation_May__c++;
                }
                else  if(sim.Activation_Date__c.month() == 6) {
                    a.Real_SIM_Activation_June__c++;
                }
                else  if(sim.Activation_Date__c.month() == 7) {
                    a.Real_SIM_Activation_July__c++;
                }
                else  if(sim.Activation_Date__c.month() == 8) {
                    a.Real_SIM_Activation_August__c++;
                }
                else  if(sim.Activation_Date__c.month() == 9) {
                    a.Real_SIM_Activation_September__c++;
                }
                else  if(sim.Activation_Date__c.month() == 10) {
                    a.Real_SIM_Activation_October__c++;
                }
                else  if(sim.Activation_Date__c.month() == 11) {
                    a.Real_SIM_Activation_November__c++;
                }
                else {
                    a.Real_SIM_Activation_December__c++;
                }
            }

        }

        return actPlan;

    }




}