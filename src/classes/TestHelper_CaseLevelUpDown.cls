@isTest
public class TestHelper_CaseLevelUpDown{

    public static Account createAccount(){
        Account acc = new Account(
            Company_Email__c = 'company@email.com',
            Unique_Business_Number__c = 'UBN 123',
            BillingCountry = 'Ireland',
            BillingStreet = 'Warwick street 15',
            BillingCity = 'Cork',
            Jurisdiction__c = 'Jurisdiction',
            VO_Number__c = '4567898',
            VAT__c = 12.50,
            Phone = '12345',
            Type = 'Customer',
            //OwnerId = u.Id,
            BillingPostalCode = '57000',
            Sales_Employee__c = 'John Voight',
            Frame_Contract__c = false,
            Website = 'www',
            Name = 'AP Test Acct'
        );
        insert acc;
        return acc;
    }
    
    public static Contact createContact(){
        Account acc = createAccount();
        Contact c = new Contact(firstName = 'Test', lastName = 'Contact', Salutation = 'Mr', AccountId = acc.Id, email = 'abc@test.com');
        insert c;
        return c;
    }

    public static Case createCase(Contact con){
        Id stdRecId = [Select id from RecordType where sObjectType = 'Case' AND developerName = 'Standard_Case' LIMIT 1].Id;
		Case c = new Case(recordTypeId = stdRecId, AccountId = con.AccountId, contactId = con.Id, Status = 'Open', Type = 'Standard', Category__c = 'Technical', SubCategory__c = 'HV', Origin = 'Email', Description = 'This is a Sample Ticket to see if owner assignment works', subject = 'Sample Ticket');
        insert c;
        return c;
    }
    
    public static CaseComment createCaseComment(Case c){
        CaseComment cCom = new CaseComment(parentId = c.Id, commentBody = 'This is a Sample Comment');
        insert cCom;
        return cCom;
    }
    
    public static void createEntitlements(Id accId, String slaProcessName){
        List<slaProcess> entProcess = [Select Name from slaProcess where Name = :slaProcessName LIMIT 1];
        if(!entProcess.isEmpty()){
            Entitlement e = new Entitlement(AccountId = accId, Name = 'Test Entitlement', SlaProcessId = entProcess[0].Id, StartDate = Date.today().addDays(-10), EndDate = Date.today().addDays(10));
            insert e;
        }
    }
}