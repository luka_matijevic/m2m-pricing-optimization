public with sharing class CustomerBillController {

    // Initialise
    public Id dailyBillId { get; set; }
    public String prebillInvoiceNumber { get; set; }
    public List<Daily_Bill_Item__c> dailyBillItemList { get; set; }
    public List<Prebill__c> prebillList { get; set; }
    private Set<String> invoiceNumberSet = new Set<String>();
    public Map<Account, List<Daily_Bill_Item__c>> accountBillMap = new Map<Account, List<Daily_Bill_Item__c>>();
    private Map<String, String> invoiceAccountMap = new Map<String, String>();
    private Map<String, Account> accountMap = new Map<String, Account>();
    private List<Account> accountList = new List<Account>();
    public List<PDFBill> pdfBills = new List<PDFBill>();
    public PDFBill pdfBill { get; set; }
    
    public CustomerBillController() {

        prebillInvoiceNumber = ApexPages.currentPage().getParameters().get('preb');
        //pdfName = opp.Name + '.pdf';
    }
    
    public void init()
    {
        prebillList = [SELECT Id, Invoice_Number__c, Billing_Account__c, Account_ID__c, Total_Gross_Amount__c, Total_Net_Amount__c, Total_Tax_Amount__c, Daily_Bill__c FROM Prebill__c WHERE Invoice_Number__c = :prebillInvoiceNumber];
        
        if(prebillList != null && prebillList.size() > 0) {
            for(Prebill__c pr : prebillList) {
                dailyBillId = pr.Daily_Bill__c;
                
                //System.debug(LoggingLevel.INFO, 'pr.Invoice_Number__c ' + pr.Invoice_Number__c + ' pr.Account_ID__c ' + pr.Account_ID__c);
                
                if(pr.Invoice_Number__c != null && pr.Account_ID__c != null) {
                    invoiceAccountMap.put(pr.Invoice_Number__c, String.valueOf(pr.Account_ID__c).substring(0, 15));
                    accountMap.put(String.valueOf(pr.Account_ID__c).substring(0, 15), null);
                }
            }
        } else {
            dailyBillItemList = [SELECT Id, Daily_Bill__c, Art__c, Beschreibung__c, Rechnungsnummer__c, Anweisung__c, Steuersatz__c, Einzel_Nettobetrag__c, Einzel_Taxbetrag__c, Einzel_Bruttobetrag__c, VO_Nr__c, Menge__c, Account__c FROM Daily_Bill_Item__c WHERE Rechnungsnummer__c = :prebillInvoiceNumber];
            
            for(Daily_Bill_Item__c dbi : dailyBillItemList) {
                dailyBillId = dbi.Daily_Bill__c;
                
                //System.debug(LoggingLevel.INFO, 'dbi.Rechnungsnummer__c ' + dbi.Rechnungsnummer__c + ' dbi.Account__c ' + dbi.Account__c);
                
                if(dbi.Rechnungsnummer__c != null && dbi.Account__c != null) {
                    invoiceAccountMap.put(dbi.Rechnungsnummer__c, String.valueOf(dbi.Account__c).substring(0, 15));
                    accountMap.put(String.valueOf(dbi.Account__c).substring(0, 15), null);
                }
            }
        }
           
        /*for(String key : invoiceAccountMap.keySet()) {
            system.debug('invoiceAccountMap key ' + key + ' invoiceAccountMap value ' + invoiceAccountMap.get(key));
        }*/
        
        accountList = [SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry FROM Account WHERE Id in :accountMap.keySet()];
        
        /*for(String key : accountMap.keySet()) {
            system.debug('accountMap key ' + key + ' accountMap value ' + accountMap.get(key));
        }*/
	
        
        for(Account acc : accountList) {
            //system.debug('acc ' + acc);
            if(accountMap.containsKey(String.valueOf(acc.Id).substring(0, 15)) || accountMap.containsKey(String.valueOf(acc.Id))) {
                //system.debug('Contains');
                accountMap.put(String.valueOf(acc.Id).substring(0, 15), acc);
            }
            //system.debug(String.valueOf(acc.Id).substring(0, 15) + ' ' + accountMap.get(String.valueOf(acc.Id).substring(0, 15)));
        }
        
        dailyBillItemList = [SELECT Id, Daily_Bill__c, Art__c, Beschreibung__c, Account__c, Rechnungsnummer__c, Anweisung__c, Steuersatz__c, Einzel_Nettobetrag__c, Einzel_Taxbetrag__c, Einzel_Bruttobetrag__c, VO_Nr__c, Menge__c FROM Daily_Bill_Item__c WHERE Daily_Bill__c = :dailyBillId ORDER BY Beschreibung__c ASC];
        
        //system.debug('dailyBillItemList size ' + dailyBillItemList.size());
        
        for(String key : invoiceAccountMap.keySet()) {
            List<Daily_Bill_Item__c> dbiList = new List<Daily_Bill_Item__c>();
            //system.debug('invoiceAccountMap key ' + key);
            for(Daily_Bill_Item__c dbi : dailyBillItemList) {
                //system.debug('dbi.Rechnungsnummer__c ' + dbi.Rechnungsnummer__c);
                if(dbi.Rechnungsnummer__c.equals(key)) {
                    //system.debug('number found');
                    dbiList.add(dbi);
                }
            }
            
            List<DailyBillItemWrapper> dbiWrapper = new List<DailyBillItemWrapper>();
            for(Daily_Bill_Item__c dbiItem : dbiList) {
                dbiWrapper.add(new DailyBillItemWrapper(dbiItem));
            }
            dbiWrapper.sort();
            
            String accId = invoiceAccountMap.get(key);
            //system.debug('accId ' + accId);
            //system.debug('accountMap size ' + accountMap.size());
            Account tmpAcc = accountMap.get(accId);
            //system.debug('tmpAcc ' + tmpAcc);
            //accountBillMap.put(tmpAcc, dbiList);
            PDFBill tmp = new PDFBill();
            tmp.AccountName = tmpAcc != null ? tmpAcc.Name : 'ACCOUNT MISSING';
            tmp.AccountPostalCode = tmpAcc != null ? tmpAcc.BillingPostalCode : 'ACCOUNT MISSING';
            tmp.AccountCity = tmpAcc != null ? tmpAcc.BillingCity : 'ACCOUNT MISSING';
            tmp.AccountStreet = tmpAcc != null ? tmpAcc.BillingStreet : 'ACCOUNT MISSING';
            tmp.BillKundenNummer = dbiList.size() > 0 ? dbiList[0].VO_Nr__c : '';
            String tmpKundenInfo = dbiList.size() > 0 && dbiList[0].Anweisung__c != null ? dbiList[0].Anweisung__c.replace('Abrechnungszeitraum', '') : '';
            tmp.BillKundenInformationen = tmpKundenInfo;
            String firstCharacter = key.substring(0, 1);
            tmp.RechnungNummer = firstCharacter.equals('0') ? key.substring(1) : key;
            tmp.DailyBillItems = dbiWrapper;
            pdfBills.add(tmp);
        }
        
        if(pdfBills.size() > 0)
        	pdfBill = pdfBills[0];
    }
    
    public class PDFBill {
        public String AccountName {get;set;}
        public String AccountPostalCode {get;set;}
        public String AccountCity {get;set;}
        public String AccountStreet {get;set;}
        public String BillKundenNummer {get;set;}
        public String BillKundenInformationen {get;set;}
        public String RechnungNummer {get;set;}
        public List<DailyBillItemWrapper> DailyBillItems {get;set;}
        
        public Decimal PrebillNettoBetrag {
            get {
                Decimal finalValue = 0;
                if(DailyBillItems != null) {
                    for(DailyBillItemWrapper dbi : DailyBillItems) {
                        finalValue += dbi.Einzel_Nettobetrag == null ? 0 : dbi.Einzel_Nettobetrag;
                    }
                }
                return finalValue;
            }
        }
        
        public Decimal PrebillTaxBetrag {
            get {
                Decimal finalValue = 0;
                if(DailyBillItems != null) {
                    for(DailyBillItemWrapper dbi : DailyBillItems) {
                        finalValue += dbi.Einzel_Taxbetrag == null ? 0 : dbi.Einzel_Taxbetrag;
                    }
                }
                return finalValue;
            }
        }
        public Decimal PrebillBruttoBetrag {
            get {
                Decimal finalValue = 0;
                if(DailyBillItems != null) {
                    for(DailyBillItemWrapper dbi : DailyBillItems) {
                        finalValue += dbi.Einzel_Bruttobetrag == null ? 0 : dbi.Einzel_Bruttobetrag;
                    }
                }
                return finalValue;
            }
        }
    }
    
    public class DailyBillItemWrapper implements Comparable {
        public Id Id {get;set;}
        public Id DailyBill {get;set;}
        public Decimal Art {get;set;}
        public String Beschreibung {get;set;}
        public Id AccountId {get;set;}
        public String Rechnungsnummer {get;set;}
        public String Anweisung {get;set;}
        public Decimal Steuersatz {get;set;}
        public Decimal Einzel_Nettobetrag {get;set;}
        public Decimal Einzel_Taxbetrag {get;set;}
        public Decimal Einzel_Bruttobetrag {get;set;}
        public String VO_Nr {get;set;}
        public Decimal Menge {get;set;}
        
        public DailyBillItemWrapper() {}

        public DailyBillItemWrapper(Daily_Bill_Item__c dbi) {
            this.Id = dbi.Id;
            this.DailyBill = dbi.Daily_Bill__c;
            this.Art = dbi.Art__c;
            this.Beschreibung = dbi.Beschreibung__c;
            this.AccountId = dbi.Account__c;
            this.Rechnungsnummer = dbi.Rechnungsnummer__c;
            this.Anweisung = dbi.Anweisung__c;
            this.Steuersatz = dbi.Steuersatz__c;
            this.Einzel_Nettobetrag = dbi.Einzel_Nettobetrag__c;
            this.Einzel_Taxbetrag = dbi.Einzel_Taxbetrag__c;
            this.Einzel_Bruttobetrag = dbi.Einzel_Bruttobetrag__c;
            this.VO_Nr = dbi.VO_Nr__c;
            this.Menge = dbi.Menge__c;
        }

        private Integer sortByDescription(DailyBillItemWrapper dbi) {
            if (this.Beschreibung > dbi.Beschreibung) {
                return 1;
            }

            if (this.Beschreibung == dbi.Beschreibung) {
                return 0;
            }

            return -1;
        }

        public Integer compareTo(Object obj) {
            DailyBillItemWrapper dbi = (DailyBillItemWrapper)(obj);
            return sortByDescription(dbi);
        }
    }
}