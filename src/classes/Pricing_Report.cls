public class Pricing_Report {
    
    public cscfga__Product_Bundle__c startDate{get; set;}
    public cscfga__Product_Bundle__c ToDate{get; set;}
    private Time tm = Time.newInstance(23,59,0,0);
    private Time sttm = Time.newInstance(00,01,0,0);
    public boolean queryRecords {get; set;}
    public integer totalNoOfpages {get; set;}
    
    public string recordsPerPage{get; set;}
    public opportunity opp {get; set;}

    private map<id,cscfga__Product_Bundle__c> totalProdBudnles;
    private map<id,ProcessInstance> processInstanceMap; //Will contain Approval process related information for product bundle
    private map<id,Frame_Contract__c> Frame_ContractMap; //Will contain Frame Contact related information for product bundle
    private map<id,list<Business_comment__c>> businessComments;
    private map<id,list<string>> approvalCommnets;
   
    private DateTime st; 
    private DateTime ed; 
    
    public Pricing_Report(){
        startDate = new cscfga__Product_Bundle__c();
        ToDate = new cscfga__Product_Bundle__c();
        ToDate.Synchronization_Date__c = Date.valueOf(system.today());
        startDate.Synchronization_Date__c = Date.valueOf(system.today()-150);
        queryRecords = true;
        totalNoOfpages = 0;
        
        recordsPerPage = '10';
        
        processInstanceMap = new map<id,ProcessInstance>();
        totalProdBudnles = new map<id,cscfga__Product_Bundle__c>();
        Frame_ContractMap = new map<id,Frame_Contract__c>();
        businessComments = new map<id,list<Business_comment__c>>();
        approvalCommnets = new map<id,list<string>>();
        opp = new Opportunity();
    }
    
    
    public void prepareProductBudnlesMap(){
        if(queryString() != null){
            totalProdBudnles = new map<id,cscfga__Product_Bundle__c>();
            for(cscfga__Product_Bundle__c pb : dataBase.query(queryString())){
                totalProdBudnles.put(pb.id, pb);
            }
        }
    }
    
    public void approvalProcessHIstory(){
        if(totalProdBudnles.size() > 0){
            processInstanceMap = new map<id,ProcessInstance>();
            for(ProcessInstance pi: [select id, CompletedDate,Status,TargetObjectId ,createdDate from ProcessInstance where TargetObjectId in:totalProdBudnles.keySet() order by createdDate]){
                processInstanceMap.put(pi.TargetObjectId,pi);
            } 
        }
    }
    
    public void frameContracts(){
        if(totalProdBudnles.size() > 0){
            Frame_ContractMap = new map<id,Frame_Contract__c>();
            for(Frame_Contract__c obj: [select name,Product_Bundle__c from Frame_Contract__c where Product_Bundle__c in:totalProdBudnles.keySet() order by createdDate]){
                Frame_ContractMap.put(obj.Product_Bundle__c,obj);
            } 
        }
    }
    
    public void businessComments(){
        if(totalProdBudnles.size() > 0){
            businessComments = new map<id,list<Business_comment__c>>();
            for(Business_comment__c obj: [select id,Name,Product_Bundle__c,Comment__c,CreatedById from Business_comment__c where Product_Bundle__c in:totalProdBudnles.keySet() order by createdDate]){
                list<Business_comment__c> tmp = new list<Business_comment__c>();
                if(totalProdBudnles.get(obj.Product_Bundle__c) != null && totalProdBudnles.get(obj.Product_Bundle__c).cscfga__Opportunity__c != null && obj.CreatedById != totalProdBudnles.get(obj.Product_Bundle__c).cscfga__Opportunity__r.ownerId){
                    tmp.add(obj);
                }
                if(businessComments.get(obj.Product_Bundle__c) != null){
                    tmp.addAll(businessComments.get(obj.Product_Bundle__c));
                }
                businessComments.put(obj.Product_Bundle__c,tmp);
            } 
        }
    }
    
    
    public void approvalCommnets(){
        if(totalProdBudnles.size() > 0){
            approvalCommnets = new map<id,list<string>>();
            for(FeedItem obj: [select id,body,ParentId,createdById from feedItem where ParentId in:totalProdBudnles.keySet() order by createdDate]){
                list<string> tmp = new list<string>();
                if(totalProdBudnles.get(obj.ParentId) != null && totalProdBudnles.get(obj.ParentId).cscfga__Opportunity__c != null && obj.CreatedById != totalProdBudnles.get(obj.ParentId).cscfga__Opportunity__r.OwnerId){
                    tmp.add(obj.body);
                }
                if(approvalCommnets.get(obj.ParentId) != null){
                    tmp.addAll(approvalCommnets.get(obj.ParentId));
                }
                approvalCommnets.put(obj.ParentId,tmp);
            } 
            for(feedComment obj: [select id, RelatedRecordId,CommentBody,parentId,CreatedById  from feedComment where ParentId in:totalProdBudnles.keySet() order by createdDate]){
                list<string> tmp = new list<string>();
                if(totalProdBudnles.get(obj.ParentId) != null && totalProdBudnles.get(obj.ParentId).cscfga__Opportunity__c != null && obj.CreatedById != totalProdBudnles.get(obj.ParentId).cscfga__Opportunity__r.OwnerId){
                    tmp.add(obj.CommentBody);
                }
                if(approvalCommnets.get(obj.ParentId) != null){
                    tmp.addAll(approvalCommnets.get(obj.ParentId));
                }
                approvalCommnets.put(obj.ParentId,tmp);
            } 
        }
        
    }
    
    public string queryString(){
        string qry = 'Select id,cscfga__Opportunity__r.OwnerId, Total_Quantity_SIM__c ,Name,Pricing_Reference_Number__c,cscfga__Opportunity__c,cscfga__Opportunity__r.stageName,cscfga__Opportunity__r.name,cscfga__Opportunity__r.TCV__c,cscfga__Opportunity__r.accountId,cscfga__Opportunity__r.account.name,createdDate,Re_Price__c from cscfga__Product_Bundle__c ';
        if(startDate.Synchronization_Date__c != null && Todate.Synchronization_Date__c != null){
            st = DateTime.newInstanceGMT(startDate.Synchronization_Date__c, sttm);
            ed = DateTime.newInstanceGMT(Todate.Synchronization_Date__c,tm);
            qry = qry + ' where createdDate >=:st and createdDate <=:ed';
        }      
        if(opp.accountId != null){
            qry = qry + ' and cscfga__Opportunity__r.AccountId =\''+opp.accountId +'\'';
        }
        qry = qry + ' order by createdDate desc';
        return qry;
    }
    
    public pageReference runReport(){
        
        Date startDate = startDate.Synchronization_Date__c;
        Date endDate = Todate.Synchronization_Date__c;
        if(startDate == null ||endDate == null ){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'From date and To Date are mandatory'));
            queryRecords = false;
        }else if(startDate != null && endDate != null && startDate.monthsBetween(endDate) > 6){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'The duration must be less 6 months'));   
            queryRecords = false;
        }
        
        return null;
    } 

    public ApexPages.StandardSetController con {
        get{
            if(queryRecords == true) {
                integer noOfrecordsPerPage = integer.valueOf(recordsPerPage);
                prepareProductBudnlesMap();
                approvalProcessHIstory();
                frameContracts();
                businessComments();
                approvalCommnets();
                queryRecords = false;
                con = new ApexPages.StandardSetController(Database.getQueryLocator(queryString()));
                totalNoOfpages = Math.mod(con.getResultSize(),noOfrecordsPerPage) > 0 ? con.getResultSize()/noOfrecordsPerPage + 1 : con.getResultSize()/noOfrecordsPerPage;
                con.setPageSize(noOfrecordsPerPage);
            }
            return con;
        }set;
    }
    
    
    
    
    
    public list<wrapper> recordsLst{
        get{
            set<id> prodBundleIds = new set<id>();
            for (cscfga__Product_Bundle__c pb : (List<cscfga__Product_Bundle__c>)con.getRecords()){
                prodBundleIds.add(pb.id);
            }
            list<wrapper> tmp = new list<wrapper>();
            
            for (cscfga__Product_Bundle__c pb : (List<cscfga__Product_Bundle__c>)con.getRecords()){
                Frame_Contract__c frameContract = new Frame_Contract__c();
                list<string> approverCommentsLst = new list<string>();
                list<Business_comment__c> salesComments = new list<Business_comment__c>();
                ProcessInstance pInstance;
                dateTime pInstanceCompletedDate;
                
                if(processInstanceMap.get(pb.id) != null ){
                    pInstance = processInstanceMap.get(pb.id);
                    if(pInstance.CompletedDate != null){
                        pInstanceCompletedDate = pInstance.CompletedDate;
                    }
                }
                if(Frame_ContractMap.get(pb.id) != null){
                    frameContract = Frame_ContractMap.get(pb.id);
                }
                if(businessComments.get(pb.id) != null){
                    for(Business_comment__c b : businessComments.get(pb.id)){
                        salesComments.add(b);
                    }
                }
                if(approvalCommnets.get(pb.id) != null){
                    for(string b : approvalCommnets.get(pb.id)){
                        approverCommentsLst.add(b);
                    }
                }
                tmp.add(new wrapper(pb,frameContract ,approverCommentsLst,salesComments,pInstance,pInstanceCompletedDate));
            }
            return tmp;
        }   
        set;
    }
    
    public class wrapper{
        public cscfga__Product_Bundle__c bundle {get; set;}
        public Frame_Contract__c frameContract {get; set;}
        public list<string> approverComments {get; set;}
        public list<Business_comment__c> salesComments {get; set;}
        public ProcessInstance pInstance {get; set;}
        public string pInstanceCompletedDate {get; set;} //Not able to access "pInstance.CompletedDate" on page, hence created this additional varriable
        public wrapper(cscfga__Product_Bundle__c bundle, Frame_Contract__c frameContract,list<string> approverComments,list<Business_comment__c> salesComments,ProcessInstance pInstance,DateTime pInstanceCompletedDate){
            this.bundle = bundle;
            this.frameContract = frameContract;
            this.approverComments = approverComments;
            this.salesComments = salesComments;
            this.pInstance = pInstance;
            this.pInstanceCompletedDate = formatString(pInstanceCompletedDate);
        }
        
    }
    
    public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }

    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }

    public Integer pageNumber {
        get {
            integer pNum = 0;
            if(totalNoOfpages > 0){
                return con.getPageNumber();
            }else{
                return pNum;
            }
            
        }
        set;
    }

     public void first() {
         con.first();
     }

     public void last() {
         con.last();
     }

     public void previous() {
         con.previous();
     }

     public void next() {
         con.next();
     }

     public void cancel() {
         con.cancel();
     }
     
     
     public pageReference export(){
         
         try{
         
             runReport();
             
             string headerRow = 'Product Bundle Name' + ',' +
                             'Pricing Reference Number' + ',' +
                             'Opportunity' + ',' +
                             'Company name' + ',' +
                             'Number of SIM/units' + ',' +
                             'TCV' + ',' +
                             'Status' + ',' +
                             'Approve/Rejection Date' + ',' +
                             'Submitted Date' + ',' +
                             'Creation of PB Date' + ',' +
                             'FC number' + ',' +
                             'Stage' + ',' +
                             'New Pricing/Re Pricing' + ',' +
                             'Sales Comments' + ',' +
                             'Commercial Approval Comments';
             string rows;
             
             for(cscfga__Product_Bundle__c pb: totalProdBudnles.values()){
                 
                String rowItem =  pb.name+',' + 
                                  pb.Pricing_Reference_Number__c+ ','+
                                  pb.cscfga__Opportunity__c + ',' +
                                  pb.cscfga__Opportunity__r.account.name + ',' +
                                  pb.Total_Quantity_SIM__c + ',';
                
                if(pb.cscfga__Opportunity__c != null && pb.cscfga__Opportunity__r.TCV__C != null){
                    rowItem = rowItem + pb.cscfga__Opportunity__r.TCV__C + ',';
                }else{
                    rowItem = rowItem + '' + ',';
                }
                
                if(processInstanceMap.get(pb.id) != null ){
                    rowItem = rowItem + processInstanceMap.get(pb.id).Status + ',';// Status
                    if(processInstanceMap.get(pb.id).CompletedDate != null){
                        rowItem = rowItem + formatString(processInstanceMap.get(pb.id).CompletedDate)+ ',';// Approve/Rejection Date
                        rowItem = rowItem + formatString(processInstanceMap.get(pb.id).CreatedDate)+ ',';// Submitted Date
                    }else{
                        rowItem = rowItem + '' + ','; // Status
                    }
                }else{
                    rowItem = rowItem + '' + ',' + // Status
                              ''+','+ // Approve/Rejection Date
                              ''+','; // Submitted Date
                }
                
                rowItem = rowItem + formatString(pb.createdDate) + ',';
                
                if(Frame_ContractMap.get(pb.id) != null){
                    rowItem = rowItem + Frame_ContractMap.get(pb.id).name + ',';
                }
                
                if(pb.cscfga__Opportunity__c != null){
                    rowItem = rowItem + pb.cscfga__Opportunity__r.stageName + ',';
                }
                
                if(pb.Re_Price__c){
                    rowItem = rowItem + 'Re Price' + ',';
                }else{
                    rowItem = rowItem + 'New Price' + ',';
                }
                integer i = 1;
                string salesComments = '';
                
                if(businessComments.get(pb.id) != null){
                    for(Business_comment__c b : businessComments.get(pb.id)){
                         salesComments = salesComments +'  '+ i+ ' :'+b.Comment__c.replace('\n',' ') + '  ';
                         i++;
                    }
                }
                
                rowItem = rowItem + salesComments + ',';
                i = 1;
                string apprvrComnts = '';
                if(approvalCommnets.get(pb.id) != null){
                    for(string b : approvalCommnets.get(pb.id)){
                        apprvrComnts = apprvrComnts +'  '+ i +' :' +  b.replace('\n',' ') + '';
                        i++;
                    }
                }
                
                rowItem = rowItem + apprvrComnts;
                             
                if(rows != null){
                    rows = rows + '\n' + rowItem;
                }else{
                    rows = rowItem;        
                }
             }
             
             string csvFile = headerRow + '\n' + rows;
             csvFile = csvFile.replace('null','');
             blob csvBlob = Blob.valueOf(csvFile);
             
             
             Integer day = system.today().day();
             Integer month = system.today().month();
             Integer year = system.today().year();
             
             string tdayDate = day+'.' +month+ '.' + year;
             
             Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
             Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
             efa.setFileName('Pricing Report '+tdayDate+'.csv');
             efa.setBody(csvBlob);
    
             email.setSubject('Pricing Report '+tdayDate);
             email.setToAddresses(new list<string>{userInfo.getUserEmail()});
             email.setPlainTextBody('Pleae find the report in attachment. \n\nThanks');
            
             email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
             
             Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email}); 
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,'Report sent to your registered email, Please check your email'));
             return null;
         }catch(exception e){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,e.getMessage()));
             return null;
         }
     }
     
     public static string formatString(DateTime dt){
         string tmp = '';
         if(dt != null){
             tmp = dt.format('dd.MM.yyyy HH:mm');
         }
         return tmp;
     }
     
     public list<selectOption> recordsSize{
         get{
             
             if(recordsSize == null){
                 list<selectOption> tmp = new list<selectOption>();
                 tmp.add(new selectOption('5','5'));
                 tmp.add(new selectOption('10','10'));
                 tmp.add(new selectOption('20','20'));
                 tmp.add(new selectOption('50','50'));
                 tmp.add(new selectOption('100','100'));
                 tmp.add(new selectOption('200','200'));
                 return tmp;
             }
             return recordsSize;
         }set;
     }
    

}