/*
 * Report Builder class to create entries in SIM_REPORT__c object based on Stock records data.
 */
public class SIMReportBuilder {

	private Map<SIMReportHeader, YearValues> reportValues = new Map<SIMReportHeader, YearValues>();  
	private Set<String> salesChannels = new Set<String>();
	private Integer minYear;
	private Integer toYear;
	private boolean isCommercial;
	private boolean newCBCalculation;
	private boolean customerRelated;
	private Map<String,Map<Integer,Map<Integer,Integer>>> customerBasePerMonth;
	
	private static Set<String> names = new Set<String>{
		Constants.SIMREPORT_ACTIVATIONS,
		Constants.SIMREPORT_DEACTIVATIONS,
		Constants.SIMREPORT_NET_ADS,
		Constants.SIMREPORT_CUSTOMER_BASE
	};
	
	private void updateMinYear(Integer candidate) {
		if (minYear == null || candidate < minYear) {
			minYear = candidate;
		}
	}
	
	public SIMReportBuilder(Integer toYear, boolean isCommercial) {
		this.toYear = toYear;
		this.isCommercial = isCommercial;
		this.newCBCalculation = false;
		this.customerRelated = false;
	}
	
	public SIMReportBuilder(Integer toYear, boolean isCommercial, boolean newCBCalculation, boolean customerRelated) {
		this.toYear = toYear;
		this.isCommercial = isCommercial;
		this.newCBCalculation = newCBCalculation;
		this.customerRelated = customerRelated;
	}
	
	public SIMReportBuilder(Integer toYear, boolean isCommercial, boolean newCBCalculation) {
		this.toYear = toYear;
		this.isCommercial = isCommercial;
		this.newCBCalculation = newCBCalculation;
		this.customerRelated = false;
	}

	private void add(String salesChannel, Date eventDate, String name, Integer amount) {
		Integer year = eventDate.year();
		Integer month = eventDate.month();
		
		updateMinYear(year);
		salesChannels.add(salesChannel);		

		SIMReportHeader header = new SIMReportHeader(salesChannel, year, name);

		if (!reportValues.containsKey(header)) {
			reportValues.put(header, new YearValues());
		}

		YearValues val = reportValues.get(header);
		val.setMonth(month, val.getMonth(month) + amount);
	}

	private void add(String salesChannel, Datetime eventDatetime, String name, Integer amount) {
		add(salesChannel, eventDatetime.date(), name, amount);
	}

	/**
	 * Adds information about the sim to member activation/deactivation variables
	 */ 
	public void addSim(Stock__c sim) {
		String salesChannel = sim.Account__r.TEF_DE_Sales_Channel__c;
		if (customerRelated) {
			salesChannel = sim.Account__r.Name;
		}
		
		if (customerBasePerMonth == null) {
			customerBasePerMonth = new Map<String,Map<Integer,Map<Integer,Integer>>>();
		}
		// Changes to accomodate Commercial Billing date. T-16972 changes.
		if (isCommercial) {
			trackCommercialDate(sim);		 
		}
		
		if (sim.SM2M_Status__c == 'ACTIVE' || (sim.State_Change_Date__c>date.newinstance(2015,9,1) && (sim.sm2m_status__c=='DEACTIVATED' || sim.sm2m_status__c=='SUSPENDED') && sim.Activation_Date__c!=null)) 
		{
			addToCustomerBase(sim);
		}

		/* START T-14128 changes*/
		if (sim.HistoryTracking__c != null) {
			for (String str : sim.HistoryTracking__c.split('\n')) {
				List<String> strList = str.split('\\|');
				if (strList[0] != null && strList[0].equalsIgnoreCase('ACTIVE')) {

					add(salesChannel, Datetime.valueOfGMT(strList[1]), Constants.SIMREPORT_ACTIVATIONS, 1);
					// ensures that the mirror member variable has the same salesChannel and year non-destructively
					//add(salesChannel, Datetime.valueOfGMT(strList[1]), Constants.SIMREPORT_DEACTIVATIONS, 0);
				}

				if (strList[0] != null && (strList[0].equalsIgnoreCase('SUSPENDED') || strList[0].equalsIgnoreCase('DEACTIVATED'))) {
					add(salesChannel, Datetime.valueOfGMT(strList[1]), Constants.SIMREPORT_DEACTIVATIONS, 1);
					// ensures that the mirror member variable has the same salesChannel and year non-destructively
					//add(salesChannel, Datetime.valueOfGMT(strList[1]), Constants.SIMREPORT_ACTIVATIONS, 0);
				}
			}
		}


		/*if (sim.Activation_Date__c != null) { 
			add(salesChannel, sim.Activation_Date__c, Constants.SIMREPORT_ACTIVATIONS, 1);
			// ensures that the mirror member variable has the same salesChannel and year non-destructively
			add(salesChannel, sim.Activation_Date__c, Constants.SIMREPORT_DEACTIVATIONS, 0);
		}

		if (sim.Deactivation_Date__c != null) {
			add(salesChannel, sim.Deactivation_Date__c, Constants.SIMREPORT_DEACTIVATIONS, 1);
			// ensures that the mirror member variable has the same salesChannel and year non-destructively
			add(salesChannel, sim.Deactivation_Date__c, Constants.SIMREPORT_ACTIVATIONS, 0);
		}*/
		/*T-14128 END */
	}

	/**
	 * Adds information about the sims
	 */ 
	public void addSims(List<Stock__c> sims) {
		for (Stock__c sim : sims) {
			addSim(sim);
		}
	}
	
	/**
	 * Customer base calculation
	 */
	private void addToCustomerBase(Stock__c sim) {
		Integer year;
		Integer month;
		if (sim.Commercial_Date__c == null){
			if (sim.Activation_Date__c == null) {
				year = 2014;
				month = 1;
			} else {
				if (sim.Activation_Date__c.year() >= 2014) {
					year = sim.Activation_Date__c.year();
					month = sim.Activation_Date__c.month();
				} else {
					year = 2014;
					month = 1;
				}
			}
		} else {
			year = sim.Commercial_Date__c.year();
			month = sim.Commercial_Date__c.month();
		}
		String salesChannel = sim.Account__r.TEF_DE_Sales_Channel__c;
		if (customerRelated) {
			salesChannel = sim.Account__r.Name;
		}
		
		if (customerBasePerMonth.get(salesChannel) != null) {
			if (customerBasePerMonth.get(salesChannel).get(year) != null) {
				if (customerBasePerMonth.get(salesChannel).get(year).get(month) != null) {
					Integer currentCB = customerBasePerMonth.get(salesChannel).get(year).get(month);
					currentCB += 1;
					customerBasePerMonth.get(salesChannel).get(year).put(month,currentCB);
				} else {
					customerBasePerMonth.get(salesChannel).get(year).put(month,1);
				}
			} else {
				customerBasePerMonth.get(salesChannel).put(year,new Map<Integer,Integer>());
				customerBasePerMonth.get(salesChannel).get(year).put(month,1);
			}
		} else {
			customerBasePerMonth.put(salesChannel, new Map<Integer,Map<Integer,Integer>>());
			customerBasePerMonth.get(salesChannel).put(year, new Map<Integer,Integer>());
			customerBasePerMonth.get(salesChannel).get(year).put(month,1);
		}
	}
	
	/**
	 * Fills missing year, fromYear and toYear inclusive
	 */ 
	private void initializeMissingValues() {
		for (Integer year = minYear; year <= toYear; year++) {
			for (String channel : salesChannels) {
				for (String name : names) {
					SIMReportHeader header = new SIMReportHeader(channel, year, name);
					if (!reportValues.containsKey(header)) {
						reportValues.put(header, new YearValues());
					}
				}
			}
		}
	}

	private Map<SIMReportHeader, YearValues> calculateTotals() {
		Map<SIMReportHeader, YearValues> totalsMap = new Map<SIMReportHeader, YearValues>();

		for (String name : names) {
			for (Integer year = minYear; year <= toYear; year++) {

				YearValues total = new YearValues();

				for (String channel : salesChannels) {
					total = total.add(reportValues.get(new SIMReportHeader(channel, year, name)));
				}

				totalsMap.put(new SIMReportHeader(Constants.SIMREPORT_TOTALS, year, name), total);
			}		  
		}
		return totalsMap;
	}

	/**
	 * Generates report objects from the earliest known sim state change, 
	 * up to given year (inclusive).
	 */ 
	public List<SIM_Report__c> buildReports() {
		List<SIM_Report__c> reports = new List<SIM_Report__c>();

		initializeMissingValues();
		
		for (String channel : salesChannels) {
			Integer accumulatedSum = 0;
			for (Integer year = minYear; year <= toYear; year++) {
				buildNetAds(channel, year);
				buildCustomerBase(channel, year, accumulatedSum);
			}
		}

		reports.addAll(simReports(reportValues));
		reports.addAll(simReports(calculateTotals()));

		return reports;
	}
	
	private void buildNetAds(String salesChannel, Integer year) {
		YearValues activations = reportValues.get(new SIMReportHeader(salesChannel, year, Constants.SIMREPORT_ACTIVATIONS));
		YearValues deactivations = reportValues.get(new SIMReportHeader(salesChannel, year, Constants.SIMREPORT_DEACTIVATIONS));

		YearValues netAds = activations.subtract(deactivations);
		reportValues.put(new SIMReportHeader(salesChannel, year, Constants.SIMREPORT_NET_ADS), netAds);
	}
	
	private void buildCustomerBase(String salesChannel, Integer year, Integer accumulatedSum) {
		YearValues netAds = reportValues.get(new SIMReportHeader(salesChannel, year, Constants.SIMREPORT_NET_ADS));
		YearValues customerBase = reportValues.get(new SIMReportHeader(salesChannel, year, Constants.SIMREPORT_CUSTOMER_BASE));

		for (Integer m = 1; m <= 12; m++) {
			if (newCBCalculation || (year<=2015 && m<8)) {
				if (customerBasePerMonth.get(salesChannel) != null) {
					if (customerBasePerMonth.get(salesChannel).get(year) != null) {
						if (customerBasePerMonth.get(salesChannel).get(year).get(m) != null) {
							accumulatedSum += customerBasePerMonth.get(salesChannel).get(year).get(m);
						} 
					}
				}
				customerBase.setMonth(m, accumulatedSum);
			} else {
				accumulatedSum += netAds.getMonth(m);
				customerBase.setMonth(m, accumulatedSum);
			}
		}
	}

	private SIM_Report__c simReport(SIMReportHeader header, YearValues values) {
		return new SIM_Report__c(
			Name = header.name,
			Year__c = String.valueOf(header.year),
			TEF_DE_Sales_Channel__c = header.salesChannel,
			Month_1__c = values.getMonth(1),
			Month_2__c = values.getMonth(2),
			Month_3__c = values.getMonth(3),
			Month_4__c = values.getMonth(4),
			Month_5__c = values.getMonth(5),
			Month_6__c = values.getMonth(6),
			Month_7__c = values.getMonth(7),
			Month_8__c = values.getMonth(8),
			Month_9__c = values.getMonth(9),
			Month_10__c = values.getMonth(10),
			Month_11__c = values.getMonth(11),
			Month_12__c = values.getMonth(12)
		);
	}

	private List<SIM_Report__c> simReports(Map<SIMReportHeader, YearValues> reportValues) {
		List<SIM_Report__c> reports = new List<SIM_Report__c>();

		for (SIMReportHeader header : reportValues.keySet()) {
			reports.add(simReport(header, reportValues.get(header)));
		}

		return reports;
	}

	private class YearValues {
		private Integer[] months;

		public YearValues() {
			this.months = new Integer[]{0,0,0,0,0,0,0,0,0,0,0,0};
		}

		public void setMonth(Integer month, Integer value) {
			if (month < 1 || month > 12) {
				throw new IndexOutOfRangeException('Months are 1-indexed.');
			}
			this.months[month-1] = value;
		}

		public Integer getMonth(Integer month) {
			if (month < 1 || month > 12) {
				throw new IndexOutOfRangeException('Months are 1-indexed.');
			}
			return months[month-1];
		}

		public YearValues add(YearValues other) {
			YearValues val = new YearValues();
			for (Integer m = 1; m <= 12; m++) {
				val.setMonth(m, this.getMonth(m) + other.getMonth(m));
			}
			return val;
		}

		public YearValues subtract(YearValues other) {
			YearValues val = new YearValues();
			for (Integer m = 1; m <= 12; m++) {
				val.setMonth(m, this.getMonth(m) - other.getMonth(m));
			}
			return val;
		}
	}

	/*
	* Updates commercial date in historyTrack field with latest of CommercialDate or Activation date
	* T-16972 changes to include Commercial dates
	*/
	private void trackCommercialDate(Stock__c sim) {
		if (sim.HistoryTracking__c != null) {
			if (sim.Commercial_Date__c != null && sim.Activation_Date__c < sim.Commercial_Date__c) {
				String actStr =  '' + 'ACTIVE' + '|' + sim.Activation_Date__c;
				String newActStr = '' + 'ACTIVE' + '|' + sim.Commercial_Date__c;
				sim.HistoryTracking__c = sim.HistoryTracking__c.replace(actStr.trim(), newActStr.trim());
			}
		}
	}

	private class IndexOutOfRangeException extends Exception {}
}