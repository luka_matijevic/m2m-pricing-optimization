@isTest
private class SM2MErrorMessageParserTest {

	static testMethod void testHasSM2MError() {
		csam_t1__Incoming_Message__c msg = new csam_t1__Incoming_Message__c(
			csam_t1__HTTP_Method__c = 'PUT',
			csam_t1__Mac_Signature__c = 'test'
		);
		insert msg;
		
		Attachment goodAtt = new Attachment(
			Name = 'test',
			ParentId = msg.Id,
			Body = Blob.valueOf('{"currentPayload":"", "errorMessage": "", "stackTrace":"[JSON]test[/JSON]"}')
		);
		insert goodAtt;
		
		SM2MErrorMessageParser errorParser = new SM2MErrorMessageParser(msg);
		
		system.assert(errorParser.hasSM2MError());
		
		delete goodAtt;
		
		Attachment badAtt = new Attachment(
			Name = 'test',
			ParentId = msg.Id,
			Body = Blob.valueOf('{"currentPayload":"", "errorMessage": "", "stackTrace":"[JSON]test"}')
		);
		insert badAtt;
		
		errorParser = new SM2MErrorMessageParser(msg);
		
		system.assert(!errorParser.hasSM2MError());
	}

	static testMethod void testGetSM2MError() {
		csam_t1__Incoming_Message__c msg = new csam_t1__Incoming_Message__c(
			csam_t1__HTTP_Method__c = 'PUT',
			csam_t1__Mac_Signature__c = 'test'
		);
		insert msg;
		
		Attachment att = new Attachment(
			Name = 'test',
			ParentId = msg.Id,
			Body = Blob.valueOf('{"currentPayload":"", "errorMessage": "", "stackTrace":"[JSON]test[/JSON]"}')
		);
		insert att;
		
		SM2MErrorMessageParser errorParser = new SM2MErrorMessageParser(msg);

		system.assertEquals('test', errorParser.getSM2MError());
		
		delete att;
		
		Attachment badAtt = new Attachment(
			Name = 'test',
			ParentId = msg.Id,
			Body = Blob.valueOf('{"currentPayload":"", "errorMessage": "", "stackTrace":"test[/JSON]"}')
		);
		insert badAtt;
		
		errorParser = new SM2MErrorMessageParser(msg);
		
		system.assertEquals(null, errorParser.getSM2MError());
	}
	
	static testMethod void testGetUnrecoverableException() {
		
		csam_t1__Incoming_Message__c msg = new csam_t1__Incoming_Message__c(
			csam_t1__HTTP_Method__c = 'PUT',
			csam_t1__Mac_Signature__c = 'test'
		);
		insert msg;
		
		Attachment att = new Attachment(
			Name = 'test',
			ParentId = msg.Id,
			Body = Blob.valueOf('{"currentPayload":"", "errorMessage": "", "stackTrace":"com.cloudsense.messaging.UnrecoverableException: test\\n"}')
		);
		insert att;
		
		SM2MErrorMessageParser errorParser = new SM2MErrorMessageParser(msg);

		system.assertEquals('test', errorParser.getUnrecoverableException());
		
		delete att;
		
		Attachment badAtt = new Attachment(
			Name = 'test',
			ParentId = msg.Id,
			Body = Blob.valueOf('{"currentPayload":"", "errorMessage": "", "stackTrace":"test"}')
		);
		insert badAtt;
		
		errorParser = new SM2MErrorMessageParser(msg);
		
		system.assertEquals(null, errorParser.getUnrecoverableException());
	}
}