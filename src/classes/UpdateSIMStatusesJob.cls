//-------------------------------------------------------------------------------
// Job for updating SIM statuses
//-------------------------------------------------------------------------------
global class UpdateSIMStatusesJob implements Database.Batchable<SObject>
{
	//---------------------------------------------------------------------------
	// Start of batch apex
	//---------------------------------------------------------------------------
	global Database.QueryLocator start(Database.BatchableContext bc)
	{
		//
		// Filter accounts with SM2M ID
		//
		String query = 'SELECT Id FROM Account WHERE SM2M_ID__c != null OR SM2M_ID_Local__c != null';
		return Database.getQueryLocator(query);
	}

	//--------------------------------------------------------------------------
	// Execute of batch apex
	//--------------------------------------------------------------------------
	global void execute(Database.BatchableContext bc, List<SObject> scope)
	{
		for (Account account : (List<Account>)scope)
		{
			csam_t1.ObjectGraphCalloutHandler.queueMessageFromId
				( 'Update SIM Statuses'
				, account.Id);
		}
	}

	//--------------------------------------------------------------------------
	// Finish of batch apex
	//--------------------------------------------------------------------------
	global void finish(Database.BatchableContext bc)
	{

	    /*T-24057 - TERMINATION - SIM(Stock) validity changes 
	    dataBase.executeBatch(new Prolongation_Job()); //Created new Scheduler to shcedule the Prolongation_Job*/ 
		Database.executeBatch(new UpdateMSISDNIsReadyForUse());
	}
}