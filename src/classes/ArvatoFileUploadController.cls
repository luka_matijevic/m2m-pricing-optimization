/**
a class that is used for controling the uplaods of the second upload file. It save the uplaoded file as attachment to the OrderRequest record
and inserts it. After insert is completed class schedules a batch job to generate SIM assets from the data available in the file.
*/
public with sharing class ArvatoFileUploadController {

	public Third_Upload_File__c parent {get;set;}
    public Attachment attach {get;set;}

    public ArvatoFileUploadController(ApexPages.StandardController stdController)
    {
        attach = new Attachment();
        if(!test.isRunningTest()){
        	stdController.addFields(new String[]{'Type__c'});    
        }        
        this.parent = (Third_Upload_File__c)stdController.getRecord();       
    }

    public ApexPages.PageReference upload()
    {
		//savepoint sp = Database.setSavepoint();
        try
        { 
        	attach.ParentId = parent.Id;
        	insert attach;
			System.debug(LoggingLevel.ERROR, 'Heap Size in Controller Before : ' + Limits.getHeapSize() + '/' + Limits.getLimitHeapSize());
			Set<String> iccIds = new Set<String>();
			if(apexPages.currentPage().getParameters().get('excludeQuotes') != null){
			    iccIds = SIMDispatchFileParser.parseArvatoExportFile(attach.Body,true);
			}else{
			    iccIds = SIMDispatchFileParser.parseArvatoExportFile(attach.Body,false); 
			}
			System.debug('Final List of iccIds Length : '+iccIds.size());
			System.debug(LoggingLevel.ERROR, 'Heap Size in controller After : ' + Limits.getHeapSize() + '/' + Limits.getLimitHeapSize());
			system.debug('iccIds>>>>'+iccIds);
        	AssignStockToThirdFileBatch.assignStockToThirdFile(iccIds, parent.Id, parent.Type__c);
			//ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'SIMs have been successfully associated with the third upload file'));
			return new ApexPages.StandardController(parent).view();
        	
        }
        catch(Exception ex)
        {
        	//Database.rollback(sp);
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'There was an error while uploading the file: ' + ex.getMessage()));
			return null;
        }finally{
        	attach.body = null;
        	attach = new Attachment();
        	System.debug(LoggingLevel.ERROR, 'Heap Size in controller finally block : ' + Limits.getHeapSize() + '/' + Limits.getLimitHeapSize());
        }
    } 
}