public class MonitoringBOController {
    public class simStatusRecord implements Comparable{
        Id accountId;
        String accountName; 
        DateTime jobDate;
        String status;
        boolean renderButton;
        public Id getAccountId() {return this.accountId;}	
        public String getJobDate() {return this.jobDate.format();}
        public String getAccountName() {return this.accountName;}
        public String getStatus() {return this.status;}
        public boolean getRenderButton() {return this.renderButton;}
        
        public Integer compareTo(Object compareTo){
            simStatusRecord compareToSim = (simStatusRecord) compareTo;
            if (accountId == compareToSim.accountId && jobDate >= compareToSim.jobDate) return 0;
            if (accountId == compareToSim.accountId && jobDate < compareToSim.jobDate) return 1;
            if (accountId > compareToSim.accountId) return 1;
            return -1;
        }
        
    }
    public List<sm2m_users> sm2mUserList {get; set;}
    public List<rabbitQueue> rabbitQueueList {get; set;}
   
    
    public List<Geotab_orders_log__c> geotabOrdersLog {
        get {
            List<Geotab_orders_log__c> geotabOrdersLog = [select id, createddate, Response_message__c, Order_request__r.name, Order_request__r.account__r.name from geotab_orders_log__c];
            return geotabOrdersLog;
            }
        }
        
    
    public String exportFileDate {get;set;} 
    
    Map<String, integer> statusOptionsMap = new Map<String, integer>{
        'Waiting for Response' => 0,
            'Security Error' => 0,
            'Unrecoverable Error' => 0,
            'Future Call Restricted' => 0,
            'Processed' => 0,
            'Process Error' => 0,
            'Integration Error' => 0,
            'Pending' => 0
            };
                
    public  Map<String,String> PrefixList = PrefixList();
    public List<simStatusRecord> simList = new List<simStatusRecord>();
    public String message {get;set;}
    public String selectedOption {get ; set ;}
    

    
    public List<SelectOption> getStatuses() {
        Set<String> statuses = statusOptionsMap.keySet();		
        List<SelectOption> statusOptions = new List<SelectOption>();
        statusOptions.add(new SelectOption('','- All -'));
        statusOptions.add(new SelectOption('All Errors','All Errors'));
        for (String s : statuses)
            statusOptions.add(new SelectOption(s, s));        
        return statusOptions;
    }
    
    public List<simStatusRecord> getsimList(){
        return this.simList;
    }
    public Date simDate { get; set; }
    public Date reportDate { get; set; }
    
    public class BarWedgeData {
        public String name { get; set; }
        public Integer data { get; set; }
        public BarWedgeData(String name, Integer data) {
            this.name = name;
            this.data = data;
        }
    }
    List<BarWedgeData> barData = new List<BarWedgeData>();
    public List<BarWedgeData> getBarData(){
        return this.barData;
    }
    
    
    
    
    public MonitoringBOController (apexPages.standardController con){    
        exportFileDate = String.valueOf(Date.today()).replace('/','').replace('.','').replace('-','');
        stockLookupAccount = (Stock__c) con.getRecord();
        system.debug('ApexPages.CurrentPage().getparameters(): ' + ApexPages.CurrentPage().getparameters());
        
        if(ApexPages.CurrentPage().getparameters().get('simDate')!= Null) {
     		simDate = date.valueOf( ApexPages.CurrentPage().getparameters().get('simDate') );
		}
        else simDate = Date.today();
        reportDate = Date.today();

            if(ApexPages.CurrentPage().getparameters().get('selectedOption') != Null && ApexPages.CurrentPage().getparameters().get('selectedOption') != 'null') {
     			selectedOption =  ApexPages.CurrentPage().getparameters().get('selectedOption') ;
				}
          
            system.debug('ApexPages.CurrentPage().getparameters(): '+ ApexPages.CurrentPage().getparameters().get('selectedOption'));
            system.debug('selectedOption: '+ selectedOption);

        refreshsimListLastStatus();
        //refreshSimList();
        refreshSimData();
        refreshReports();
        refreshQuarantine();
        queryJobs();
        orderRequests();
        if(!Test.isRunningTest()){
        sm2mUserList = (List<sm2m_users>) JSON.deserialize(getSm2mUsers(), List<sm2m_users>.class);
        rabbitQueueList = (List<rabbitQueue>) JSON.deserialize(getRabbitQueues(), List<rabbitQueue>.class);
        }
         
/*
List<sm2m_users> sm2mUserList = (List<sm2m_users>) JSON.deserialize(usersJSON, List<sm2m_users>.class);
system.debug(sm2mUserList);
*/
    }
    
    public PageReference exportSIMtoExcel(){
        system.debug('selectedOption: ' + selectedOption);
        PageReference p = new PageReference('/apex/SIM_Managment_SIM_Status_Excel?simDate='+simDate+'&selectedOption=' + selectedOption);
        p.setRedirect(true);
        return p;
    }
    public PageReference exportQuarantinetoExcel(){
        PageReference p = new  PageReference('/apex/SIM_Managment_Quarantine_Excel');
        p.setRedirect(true);
        return p;
    }
    
    /*
public List<simStatusRecord> simList = new List<simStatusRecord>();

public String selectedOption {get;set;} 
public List<selectOption> getStatuses() {
Set<String> statuses = statusOptionsMap.keySet();		
List<SelectOption> statusOptions = new List<SelectOption>();
statusOptions.add(new SelectOption('','- All -'));
for (String s : statuses)
statusOptions.add(new SelectOption(s, s));        
return statusOptions;
}
*/
    public String selectedQuarantine {get;set;} 
    public List<SelectOption> getQuarantineRanges() {
        List<SelectOption> quarantineRanges = new List<SelectOption>();
        quarantineRanges.add(new SelectOption('', '- All -'));
        quarantineRanges.add(new SelectOption('TODAY', 'TODAY'));
        quarantineRanges.add(new SelectOption('TOMORROW', 'TOMORROW'));
        quarantineRanges.add(new SelectOption('THIS_WEEK', 'THIS WEEK'));
        quarantineRanges.add(new SelectOption('NEXT_WEEK', 'NEXT WEEK'));
        quarantineRanges.add(new SelectOption('THIS_MONTH', 'THIS MONTH'));
        quarantineRanges.add(new SelectOption('NEXT_MONTH', 'NEXT MONTH'));
        return quarantineRanges; 
    }
    
    public List<MSISDN__c> quarantineList = new List<MSISDN__c>();
    List<String> msisdnList = new List<String>();
    public List<finalQuarantine> finalQuarantineList = new List<finalQuarantine>();
    public Stock__c stockLookupAccount {get;set;}
    public void refreshQuarantine(){
        quarantineList.clear();
        quarantineList = Database.query( 'select id, name, Date_quarantine_start__c, Date_quarantine_end__c, isReadyForUse__c, MSISDN__c from MSISDN__c where Date_quarantine_end__c > today ' + (String.isBlank(selectedQuarantine) ? '' : ' and Date_quarantine_end__c <= ' +selectedQuarantine));
        
        msisdnList.clear();
        for (MSISDN__c m : quarantineList) msisdnList.add(m.MSISDN__c);
        List<Stock__c> stockList;
        if (null==stockLookupAccount || null==stockLookupAccount.Account__c)
            stockList = [select id, account__c, account__r.name, msisdn__c 
                         from stock__c where msisdn__c in :msisdnList];
        else  
        {
            stockList = [select id, account__c, account__r.name, msisdn__c 
                         from stock__c where msisdn__c in :msisdnList
                         and Account__c = :stockLookupAccount.Account__c];
            
        }
        
        Map<String, Stock__c> stockMap = new Map<String, Stock__c>();
        for (Stock__c s : stockList) stockMap.put(s.MSISDN__c, s);
        finalQuarantineList.clear();
        
        for (MSISDN__c m : quarantineList){
            finalQuarantine fq = new finalQuarantine(); 			
            fq.id = m.id;
            fq.name = m.name;
            fq.Date_quarantine_start = m.Date_quarantine_start__c;
            fq.Date_quarantine_end = m.Date_quarantine_end__c;
            fq.MSISDN = m.MSISDN__c;
            if(stockMap.containsKey(m.MSISDN__c)){
                fq.accountId = stockMap.get(m.MSISDN__c).account__c;
                fq.accountName = stockMap.get(m.MSISDN__c).account__r.name;
                finalQuarantineList.add(fq);    
            }
            
        }
        
    }
    
    public AggregateResult[] getTerminationRequested(){
        return [
            select 
            Account__c, account__r.name name, count(id) nr
            from stock__C where Termination_Status__c = 'REQUESTED'
            group by account__c,  account__r.name
        ]; 
    }
    
    
    public String getRequestedListViewURL(){
        return ListViewURL(PrefixList,'Stock__c','Termination REQUESTED');	
    }
    public String getTerminatedListViewURL(){
        return ListViewURL(PrefixList,'Stock__c','TERMINATED');	
    }
    public String getRetiredListViewURL(){
        return ListViewURL(PrefixList,'Stock__c','TERMINATED-RETIRED');	
    }
    public AggregateResult[] getTerminatedByStatus(){
        
        
        return [select termination_status__c status, count(id) nr from stock__c where termination_status__c in ('REQUESTED','TERMINATED','TERMINATED-RETIRED') group by termination_status__c];
    }
    
    
    public List<finalQuarantine> getFinalQuarantineList(){
        return this.finalQuarantineList;
    }
    
    public class finalQuarantine{
        Id id;
        String name;
        Date Date_quarantine_start;
        Date Date_quarantine_end;
        String MSISDN; 
        Id accountId;
        String accountName;
        public Id getId() {return this.id;}	
        public String getName() {return this.name;}			
        public String getDate_quarantine_start() {return this.Date_quarantine_start.format();}	
        public String getDate_quarantine_end() {return this.Date_quarantine_end.format();}	
        public String getMSISDN() {return this.MSISDN;}	
        public Id getAccountId() {return this.accountId;}	
        public String getAccountName() {return this.accountName;}	
        
    }		   
    
    public void refreshSimData(){
        barData.clear();
        for (AggregateResult[] groupedResults : [select sm2m_status__c Status, count(id) nr from stock__c group by sm2m_status__c ])
        {
            for (AggregateResult ar : groupedResults)
                barData.add(new BarWedgeData(null!=ar.get('Status') ? (String) ar.get('Status') : 'Unknown', (Integer) ar.get('nr')));
        }	
    }
    
    public PageReference retry(){
        Id accountId = apexpages.currentpage().getparameters().get('accountParam'); 
        csam_t1.ObjectGraphCalloutHandler.createAndSend ('Update SIM Statuses', accountId) ;
        return null;
    }
    
    public void retryAll(){
        
        
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,SIMStatusesUtils.updateFailedSIMStatuses());
        
        ApexPages.addMessage(myMsg);
        
    }
    
    public void refreshSimList( ){
        refreshsimListLastStatus();
        
        simList.clear();
        
        Date d2 =simDate.addDays(1);
        
        List<Id> AccountOK = new List<Id>();
        List<csam_t1__Outgoing_Message_Record__c> outgoindObjectRecordIdOK = Database.query('SELECT  csam_t1__Object_Record_Id__c FROM csam_t1__Outgoing_Message_Record__c WHERE csam_t1__Outgoing_Message__r.csam_t1__ObjectGraph_Callout_Handler__r.Name = \'Update SIM Statuses\' and csam_t1__Outgoing_Message__r.csam_t1__Status__c=\'Response Received\' and CreatedDate >= :simDate AND CreatedDate <= :d2 ');
        for (csam_t1__Outgoing_Message_Record__c omr : outgoindObjectRecordIdOK) 
            AccountOK.add(omr.csam_t1__Object_Record_Id__c);
        
        //get outgoing-no response 
        List<csam_t1__Outgoing_Message_Record__c> outgoingMessagesRecordsList = 
            Database.query( 'SELECT  csam_t1__Object_Record_Id__c, csam_t1__Outgoing_Message__c, csam_t1__Outgoing_Message__r.csam_t1__Status__c, CreatedDate  FROM csam_t1__Outgoing_Message_Record__c WHERE csam_t1__Outgoing_Message__r.csam_t1__ObjectGraph_Callout_Handler__r.Name = \'Update SIM Statuses\' and csam_t1__Outgoing_Message__r.csam_t1__Status__c!=\'Response Received\'  and CreatedDate >= :simDate AND CreatedDate <= :d2 ' 
                           + 'and csam_t1__Object_Record_Id__c not in :AccountOK'); //remove accounts with response received
        
        
        //get incoming
        List<csam_t1__Incoming_Message__c> incomingMessagesList = 
            Database.query( 'SELECT csam_t1__Status__c, csam_t1__Outgoing_Message__c, CreatedDate FROM csam_t1__Incoming_Message__c WHERE csam_t1__Outgoing_Message__r.csam_t1__ObjectGraph_Callout_Handler__r.Name = \'Update SIM Statuses\' and csam_t1__Outgoing_Message__r.csam_t1__Status__c=\'Response Received\' and CreatedDate >= :simDate AND CreatedDate <= :d2 ' 
                           /*moved to sortAndFilterSimStatuses
*    + (String.isBlank(selectedOption) ? 
'' : selectedOption.equals('All Errors') ? 
' and csam_t1__Status__c not in (\'Processed\', \'Pending\', \'Waiting for Response\')' : 
' and csam_t1__Status__c = :selectedOption')
*/
                          );
        List<Id> accountList = new List<Id>();
        
        //populate accoung list from outgoing
        if(outgoingMessagesRecordsList.size()>0){			
            for (csam_t1__Outgoing_Message_Record__c omr : outgoingMessagesRecordsList){
                accountList.add(omr.csam_t1__Object_Record_Id__c);	
            }
        }
        
        //populate outgoing message list for incoming messages
        List<Id> outgoingMessages = new List<Id>();
        if(incomingMessagesList.size()>0){			
            for (csam_t1__Incoming_Message__c imr : incomingMessagesList){
                outgoingMessages.add(imr.csam_t1__Outgoing_Message__c);
            }
        }
        
        //get account IDs for incoming messages	
        List<csam_t1__Outgoing_Message_Record__c> incomingMessagesRecordsList = [select csam_t1__Object_Record_Id__c, csam_t1__Outgoing_Message__c from csam_t1__Outgoing_Message_Record__c where csam_t1__Outgoing_Message__c in :outgoingMessages];
        Map<Id, Id> incomingMap = new Map<Id, Id>();
        for (csam_t1__Outgoing_Message_Record__c omr:incomingMessagesRecordsList){
            incomingMap.put(omr.csam_t1__Outgoing_Message__c, omr.csam_t1__Object_Record_Id__c);
            accountList.add(omr.csam_t1__Object_Record_Id__c);
        }
        //account data
        Map<Id,Account> accountMap = new Map<Id,Account> ([select id, name from Account where id in :accountList] );
        //outgoing 							
        for (csam_t1__Outgoing_Message_Record__c omr : outgoingMessagesRecordsList){
            simStatusRecord ssr = new simStatusRecord();
            ssr.accountId = omr.csam_t1__Object_Record_Id__c;
            ssr.accountName = accountMap.get(omr.csam_t1__Object_Record_Id__c).Name;
            ssr.jobDate = omr.CreatedDate;
            ssr.status = omr.csam_t1__Outgoing_Message__r.csam_t1__Status__c;
            ssr.renderButton = true;
            simList.add(ssr);
        }
        //incoming
        for (csam_t1__Incoming_Message__c im : incomingMessagesList){
            simStatusRecord ssr = new simStatusRecord();
            
            ssr.accountId = incomingMap.get(im.csam_t1__Outgoing_Message__c);
            ssr.accountName = accountMap.get(incomingMap.get(im.csam_t1__Outgoing_Message__c)).Name;
            ssr.jobDate = im.CreatedDate;
            ssr.status = im.csam_t1__Status__c;
            ssr.renderButton = im.csam_t1__Status__c == 'Processed' || im.csam_t1__Status__c == 'Pending' ? false : true;
            simList.add(ssr);
        }
        
        simList = sortAndFilterSimStatuses(simList);
        
        refreshPie(simList);
        
    }
    private List<simStatusRecord> sortAndFilterSimStatuses(List<simStatusRecord> listIn ){
        List<simStatusRecord> outList = new List<simStatusRecord>();
        system.debug('simList: '+listIn);
        listIn.sort();
        system.debug('simList after sort: '+listIn);
        system.debug('simList size: '+listIn.size());
        String previousAccount;
        for ( Integer i = 0; i < listIn.size() ; i++ ){
            if (previousAccount != listIn[i].accountId) {
                /*moved to sortAndFilterSimStatuses
*    + (String.isBlank(selectedOption) ? 
'' : selectedOption.equals('All Errors') ? 
' and csam_t1__Status__c not in (\'Processed\', \'Pending\', \'Waiting for Response\')' : 
' and csam_t1__Status__c = :selectedOption')
*/
                if(!String.isBlank(selectedOption)){
                    if (selectedOption.equals('All Errors') && !(listIn[i].status.equals('Processed') || listIn[i].status.equals('Pending') || listIn[i].status.equals('Waiting for Response'))) outList.add(listIn[i]);
                    else if (listIn[i].status.equals(selectedOption)) outList.add(listIn[i]);
                }
                else {outList.add(listIn[i]);}
            }
            previousAccount = listIn[i].accountId;    
            
            
        }
        system.debug('simList after filter: ' + listIn);
        system.debug('simList size after filter: ' + listIn.size());
        return outList;
        
    }
    
    private void refreshPie(List<simStatusRecord> inList){        
        Set<String> statuses = statusOptionsMap.keySet();
        for (String s : statuses) statusOptionsMap.put(s, 0);
        for (simStatusRecord ssr : inList){
            for (String s : statuses){
                //why if... !?
                if(s==ssr.status) {
                    integer c = statusOptionsMap.get(s);
                    c++;
                    statusOptionsMap.put(s, c);
                }
            }
        }
        
        pieData.clear();
        for (String s : statuses) {
            pieData.add(new PieWedgeData(s, statusOptionsMap.get(s)));		
        }
    }
    public class PieWedgeData {
        public String name { get; set; }
        public Integer data { get; set; }
        public PieWedgeData(String name, Integer data) {
            this.name = name;
            this.data = data;
        }
    }
    List<PieWedgeData> pieData = new List<PieWedgeData>();
    public List<PieWedgeData> getPieData(){
        return pieData;
    }
    
    public class jobRecord{
        id jobId;
        boolean reportGenerated ;
        boolean uploadToBICStatus;
        boolean uploadToPQTStatus;
        boolean uploadToVVLStatus;
        
        public id getJobId() {return this.jobId;}
        public boolean getReportGenerated() {return this.reportGenerated;}
        public boolean getuploadToBICStatus() {return this.uploadToBICStatus;}
        public boolean getuploadToPQTStatus() {return this.uploadToPQTStatus;}
        public boolean getuploadToVVLStatus() {return this.uploadToVVLStatus;}
        
        
    }
    
    public List<jobRecord> jobList = new List<jobRecord>();
    public List<jobRecord> getJobList(){
        return this.jobList;
    }
    
    public void refreshReports(){
        jobList.clear();
        Date d = reportDate.addDays(1);
        List<BIC_Export_Job_Data__c> bicList = new List<BIC_Export_Job_Data__c>([select id, Report_Generated__c, Upload_to_BIC_Status__c, Upload_to_PQT_Status__c, Upload_to_VVL_Status__c 
                                                                                 from BIC_Export_Job_Data__c  where CSV_Generation_Start_Time__c >= :reportDate and CSV_Generation_Start_Time__c <=:d]);
        
        for (BIC_Export_Job_Data__c bic : bicList) {	
            jobRecord jr = new jobRecord();
            jr.jobID = bic.id;
            jr.reportGenerated = bic.Report_Generated__c;
            jr.uploadToBICStatus = bic.Upload_to_BIC_Status__c == 'Completed' ? true : false;
            jr.uploadToPQTStatus = bic.Upload_to_PQT_Status__c == 'Completed' ? true : false;
            jr.uploadToVVLStatus = bic.Upload_to_VVL_Status__c == 'Completed' ? true : false;
            jobList.add(jr);
        }
    }
    
    public class scheduleRecord{
        String name;
        String state;
        DateTime nextFireTime;
        public String getName(){return name;}
        public String getState(){return state;}
        public String getNextFireTime(){return null != nextFireTime ?  nextFireTime.format() : null;}
    }
    
    Map<String, String> jobMap = new Map<String, String>{
        'InMessageJob' => 'InMessageJob%',
            'OutMessageJob' => 'OutMessageJob%',
            'SIMReportsPoller' => 'SIMReportsPoller%',
            'PrebillFetchingPoller' => 'PrebillFetchingPoller%',
            'CaseEmailStatusPoller' => 'CaseEmailStatusPoller%'
            ,'GeotabOrdersCheck' => 'GeotabOrdersCheck%'
            };
    List<CronTrigger> cron = new List<CronTrigger>();
    List<scheduleRecord> schedules = new List<scheduleRecord>();
    public PageReference queryJobs(){
        cron.clear();
        cron = [select id, CronJobDetail.Name, state, StartTime, EndTime, NextFireTime FROM CronTrigger where StartTime >= today and CronJobDetail.Name like :jobMap.values() and state in ('EXECUTING','ACQUIRED','WAITING','QUEUED')];
        Map<String, CronTrigger> cronMap = new Map<String, CronTrigger>();		
        for (CronTrigger ct : cron){
            if(ct.CronJobDetail.Name.contains('_'))
                cronMap.put(ct.CronJobDetail.Name.substring(0, ct.CronJobDetail.Name.indexOf('_')), ct);
            else
                cronMap.put(ct.CronJobDetail.Name, ct);
        }
        schedules.clear();
        for (String s : jobMap.keySet()){
            scheduleRecord sr = new scheduleRecord();
            sr.name = s;
            sr.state = cronMap.containsKey(s) ? cronMap.get(s).state : null ;
            sr.nextFireTime = cronMap.containsKey(s) ? cronMap.get(s).nextFireTime : null ;
            schedules.add(sr);
        }
        return null;
        
    }
    
    public List<scheduleRecord> getSchedules(){
        return schedules;
    }
    
    public String jobType {get;set;}
    public PageReference schedule()
    {   
        
        DateTime dt = System.now().addSeconds(10);
        String chron_exp = dt.format('ss mm HH dd MM ? yyyy');
        
        //if(jobType == 'BICReportPoller') {       
        if(jobType == 'SIMReportPoller') {
            BICReportPoller poller = new BICReportPoller();			
            System.schedule(jobType +'_' + dt.getTime(), chron_exp, poller);
        }
        else if(jobType == 'InMessageJob') {
            csam_t1.InboundMessageScheduler poller = new csam_t1.InboundMessageScheduler();			
            System.schedule(jobType +'_' + dt.getTime(), chron_exp, poller);			
        }
        else if(jobType == 'OutMessageJob') {
            csam_t1.OutboundMessageScheduler poller = new csam_t1.OutboundMessageScheduler();			
            System.schedule(jobType +'_' + dt.getTime(), chron_exp, poller);			
        }
        else if(jobType == 'PrebillFetchingPoller') {
            PrebillFetchingPoller poller = new PrebillFetchingPoller();
            System.schedule(jobType +'_' + dt.getTime(), chron_exp, poller);	
        }
        else if(jobType == 'CaseEmailStatusPoller') {
            CaseEmailStatusPoller poller = new CaseEmailStatusPoller();
            System.schedule(jobType +'_' + dt.getTime(), chron_exp, poller);	
        }
        else if(jobType == 'GeotabOrdersCheck') {
            GeotabOrdersCheck poller = new GeotabOrdersCheck();
            System.schedule(jobType +'_' + dt.getTime(), chron_exp, poller);    
        }
        
        
        queryJobs();
        
        
        return null;
    }
    
    
    public list<Order_Request__c> commercialOrderRequest = new list<Order_Request__c>();
    public list<Order_Request__c> trialOrderRequest = new list<Order_Request__c>();
    public integer orders;
    
    public integer getcommercialOrders(){
        return commercialOrderRequest.size();
    }
    public integer gettrialOrders(){
        return trialOrderRequest.size();
    }
    public integer getOrders(){
        return orders;
    }
    
    public PageReference orderRequests(){
        Map<Id, Order_Request__c> orMap = new Map<Id, Order_Request__c>();
        //list<Order_Request__c> orL = [select id, name, frame_contract__C, status__c, frame_contract__r.recordtype.name from Order_Request__c where status__c!='Closed'];
        list<Order_Request__c> orL = [select id, name, frame_contract__C, status__c, frame_contract__r.recordtype.name, Frame_Contract_Type__c from Order_Request__c where status__c not in ('Closed','Rejected/Cancelled')];
        /*
for (Order_Request__c orq : orL) orMap.put(orq.frame_contract__c, orq);
list<frame_contract__c> fcList =  [select id, name, recordtype.name from frame_contract__C where id in :orMap.keySet()];
for (frame_contract__c fc : fcList) {
if (fc.recordtype.name == 'Commercial') {
commercialOrderRequest.add(orMap.get(fc.id));
}
else{
trialOrderRequest.add(orMap.get(fc.id));
}			
}
*/
        for (Order_Request__c orq : orL){
            if (orq.Frame_Contract_Type__c == 'Commercial') {
                commercialOrderRequest.add(orq);
            }
            else if (orq.Frame_Contract_Type__c == 'Trial') {
                trialOrderRequest.add(orq);
            }			
        }
        
        orders = [select count() from csord__Order__c where createddate = this_month];
        
        ClosedSixtCasesListViewURL = ListViewURL(PrefixList,'Case','SIXT Closed Orders');
        OpenSixtCasesListViewURL = ListViewURL(PrefixList,'Case','SIXT Open Orders');
        CommercialOrderRequestsViewURL  = ListViewURL(PrefixList,'Order_Request__c','Commercial M2M Order Requests');
        TrialOrderRequestsViewURL  = ListViewURL(PrefixList,'Order_Request__c','Trial M2M Order Requests');
        OrdersCreatedThisMonthViewURL = ListViewURL(PrefixList, 'csord__Order__c', 'Orders Created this month');
        numberOfClosedSIXTCases = [select count() from case where Sixt_LVID__c != '' and status in ('Closed','Rejected','Spam','Reclosed')];
        numberOfOpenSIXTCases = [select count() from case where Sixt_LVID__c != '' and status in ('Open','Reopened')];
        return null;
    } 
    public String CommercialOrderRequestsViewURL {get;set;}
    public String TrialOrderRequestsViewURL {get;set;}
    public String ClosedSixtCasesListViewURL {get;set;}
    public String OpenSixtCasesListViewURL {get;set;}
    public String OrdersCreatedThisMonthViewURL {get;set;}
    public integer numberOfClosedSIXTCases {get;set;}
    public integer numberOfOpenSIXTCases {get;set;}
    
    public Map<String,String> PrefixList(){
        Map<String,String> PrefixList = new Map<String,String>{};
            Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        for(String sObj : gd.keySet()){
            Schema.DescribeSObjectResult r =  gd.get(sObj).getDescribe();
            PrefixList.put(r.getName(), r.getKeyPrefix());
        }
        return PrefixList;
    } 
    public String ListViewURL(Map<String,String> PrefixList,String ObjectName,String ListViewLabel){
        String ListViewURL;
        String ListViewId;
        String q = 'SELECT id FROM '+ ObjectName +' LIMIT 1';
        System.debug(PrefixList+ '>>>' +ObjectName+ '>>>' +ListViewLabel);
        ApexPages.StandardSetController ACC = new ApexPages.StandardSetController(Database.getQueryLocator(q));
        List<SelectOption> ListViews = ACC.getListViewOptions();
        for(SelectOption w : ListViews ){
            if(w.getLabel()==ListViewLabel){
                ListViewId = w.getValue().left(15);
                ListViewURL='/'+PrefixList.get(ObjectName)+'?fcf='+ListViewId;
            }
        }
        return ListViewURL;
    }
    
    
    public List<simStatusRecord> simListLastStatus = new List<simStatusRecord>();    
    
    public List<simStatusRecord> getsimListLastStatus(){
        return this.simListLastStatus;
    }
    
    public void refreshsimListLastStatus(){
        simListLastStatus.clear();
        Date d2 =simDate.addDays(1);
        
        System.debug('simDate :'+simDate + ' ' + d2 );
        Map<Id, csam_t1__Incoming_Message__c> incomingMap = new Map<Id, csam_t1__Incoming_Message__c>();
        //get incoming        
        List<csam_t1__Incoming_Message__c> incomingMessagesList = 
            Database.query( 'SELECT csam_t1__Status__c, csam_t1__Outgoing_Message__c, CreatedDate FROM csam_t1__Incoming_Message__c WHERE csam_t1__Outgoing_Message__r.csam_t1__ObjectGraph_Callout_Handler__r.Name = \'Update SIM Statuses\' and CreatedDate >= :simDate AND CreatedDate <= :d2 ');
        List<Id> outgoingIDs = new List<Id>();
        for (csam_t1__Incoming_Message__c im : incomingMessagesList){
            outgoingIds.add(im.csam_t1__Outgoing_Message__c);
            incomingMap.put(im.csam_t1__Outgoing_Message__c, im);
        }
        
        system.debug('incomingMessagesList: ' + incomingMessagesList);
        
        //outgoing messages for incoming
        List<csam_t1__Outgoing_Message_Record__c> outgoingMessagesList = 
            Database.query( 'SELECT  csam_t1__Object_Record_Id__c, csam_t1__Outgoing_Message__c, csam_t1__Outgoing_Message__r.csam_t1__Status__c , CreatedDate  FROM csam_t1__Outgoing_Message_Record__c WHERE csam_t1__Outgoing_Message__r.csam_t1__ObjectGraph_Callout_Handler__r.Name = \'Update SIM Statuses\' and CreatedDate >= :simDate AND CreatedDate <= :d2 ' 
                           + 'and csam_t1__Outgoing_Message__c  in :outgoingIds'); 
        
        system.debug('outgoingMessagesList: ' + outgoingMessagesList);
        //get accounts
        Set<Id> accountIDs = new Set<Id>();
        for (csam_t1__Outgoing_Message_Record__c omr : outgoingMessagesList)
            accountIDs.add(omr.csam_t1__Object_Record_Id__c);
        
        system.debug('accountIDs :' + accountIDs);
        
        //Get last status for missing accounts
        /*
List<AggregateResult> outgoingMessagesListMissing = 
Database.query( 'SELECT  max(id) id, csam_t1__Object_Record_Id__c csam_t1__Object_Record_Id__c  FROM csam_t1__Outgoing_Message_Record__c WHERE csam_t1__Outgoing_Message__r.csam_t1__ObjectGraph_Callout_Handler__r.Name = \'Update SIM Statuses\' ' 
+ ' and csam_t1__Object_Record_Id__c not in :accountIDs'
+ ' group by csam_t1__Object_Record_Id__c');
*/
        List<csam_t1__Outgoing_Message_Record__c> outgoingMessagesListMissing = 
            Database.query( ' select id, csam_t1__Object_Record_Id__c, CreatedDate, csam_t1__Outgoing_Message__c  from csam_t1__Outgoing_Message_Record__c WHERE csam_t1__Outgoing_Message__r.csam_t1__ObjectGraph_Callout_Handler__r.Name = \'Update SIM Statuses\' ' 
                           + ' and csam_t1__Object_Record_Id__c not in :accountIDs' 
                           + ' and CreatedDate = LAST_N_DAYS:15 '
                           + ' order by csam_t1__Object_Record_Id__c, CreatedDate desc');
        
        
        system.debug('outgoingMessagesListMissing: ' + outgoingMessagesListMissing);
        
        //Find incoming for missing accounts
        List<Id> missingOutgoing = new List<Id>();
        Map<Id, csam_t1__Outgoing_Message_Record__c> outgoingMissingMap = new Map<Id, csam_t1__Outgoing_Message_Record__c>();
        /* for (AggregateResult omr : outgoingMessagesListMissing){
accountIDs.add(String.valueOf(omr.get('csam_t1__Object_Record_Id__c')));
missingOutgoing.add(String.valueOf(omr.get('id')));
}
*/
        String account = '';
        for (csam_t1__Outgoing_Message_Record__c omr : outgoingMessagesListMissing){
            if (!account.equals(omr.csam_t1__Object_Record_Id__c)){
                accountIDs.add(omr.csam_t1__Object_Record_Id__c);
                missingOutgoing.add(omr.csam_t1__Outgoing_Message__c);
                account = omr.csam_t1__Object_Record_Id__c;
            }
        }
        List<csam_t1__Outgoing_Message_Record__c> 
            outgoingMessagesListMissingDetails = 
            Database.query( 'SELECT csam_t1__Object_Record_Id__c, csam_t1__Outgoing_Message__c, csam_t1__Outgoing_Message__r.csam_t1__Status__c, CreatedDate  FROM csam_t1__Outgoing_Message_Record__c WHERE csam_t1__Outgoing_Message__r.csam_t1__ObjectGraph_Callout_Handler__r.Name = \'Update SIM Statuses\' ' 
                           + ' and csam_t1__Outgoing_Message__c in :missingOutgoing ');
        Map<Id, csam_t1__Incoming_Message__c> incomingMissingMap = new Map<Id, csam_t1__Incoming_Message__c>();
        List<csam_t1__Incoming_Message__c> incomingMessagesListMissing = 
            Database.query( 'SELECT csam_t1__Status__c, csam_t1__Outgoing_Message__c, CreatedDate FROM csam_t1__Incoming_Message__c WHERE csam_t1__Outgoing_Message__r.csam_t1__ObjectGraph_Callout_Handler__r.Name = \'Update SIM Statuses\' and csam_t1__Outgoing_Message__c in :missingOutgoing ');
        for (csam_t1__Incoming_Message__c im : incomingMessagesListMissing){
            incomingMissingMap.put(im.csam_t1__Outgoing_Message__c, im);
        }
        
        system.debug('incomingMessagesListMissing: ' + incomingMessagesListMissing);
        
        //get account names
        Map<Id, Account> accNamesMap = new Map<Id, Account>([select id, name from Account where id in :accountIDs]);
        
        for (csam_t1__Outgoing_Message_Record__c omr : outgoingMessagesList){
            simStatusRecord ssr = new simStatusRecord();
            ssr.accountId = omr.csam_t1__Object_Record_Id__c;
            ssr.accountName = accNamesMap.containsKey(omr.csam_t1__Object_Record_Id__c) ? accNamesMap.get(omr.csam_t1__Object_Record_Id__c).Name : omr.csam_t1__Object_Record_Id__c;
            ssr.jobDate = omr.CreatedDate;
            //ssr.status = omr.csam_t1__Outgoing_Message__r.csam_t1__Status__c;           
            ssr.status = incomingMap.containsKey(omr.csam_t1__Outgoing_Message__c) ? incomingMap.get(omr.csam_t1__Outgoing_Message__c).csam_t1__Status__c :  omr.csam_t1__Outgoing_Message__r.csam_t1__Status__c;
            //ssr.renderButton = omr.csam_t1__Outgoing_Message__r.csam_t1__Status__c == 'Processed' || omr.csam_t1__Outgoing_Message__r.csam_t1__Status__c == 'Pending' ? false : true;
            ssr.renderButton = incomingMap.containsKey(omr.csam_t1__Outgoing_Message__c) &&
                (incomingMap.get(omr.csam_t1__Outgoing_Message__c).csam_t1__Status__c == 'Processed' ||
                 incomingMap.get(omr.csam_t1__Outgoing_Message__c).csam_t1__Status__c == 'Pending') 
                ? false : true;
            simListLastStatus.add(ssr);
        }
        
        for (csam_t1__Outgoing_Message_Record__c omr : outgoingMessagesListMissingDetails){
            simStatusRecord ssr = new simStatusRecord();
            ssr.accountId = omr.csam_t1__Object_Record_Id__c;
            ssr.accountName = accNamesMap.containsKey(omr.csam_t1__Object_Record_Id__c) ? accNamesMap.get(omr.csam_t1__Object_Record_Id__c).Name : omr.csam_t1__Object_Record_Id__c;
            ssr.jobDate = omr.CreatedDate;
            //ssr.status = omr.csam_t1__Outgoing_Message__r.csam_t1__Status__c;           
            ssr.status = incomingMissingMap.containsKey(omr.csam_t1__Outgoing_Message__c) ? incomingMissingMap.get(omr.csam_t1__Outgoing_Message__c).csam_t1__Status__c :  omr.csam_t1__Outgoing_Message__r.csam_t1__Status__c;
            //ssr.renderButton = omr.csam_t1__Outgoing_Message__r.csam_t1__Status__c == 'Processed' || omr.csam_t1__Outgoing_Message__r.csam_t1__Status__c == 'Pending' ? false : true;
            ssr.renderButton = incomingMissingMap.containsKey(omr.csam_t1__Outgoing_Message__c) &&
                (incomingMissingMap.get(omr.csam_t1__Outgoing_Message__c).csam_t1__Status__c == 'Processed' ||
                 incomingMissingMap.get(omr.csam_t1__Outgoing_Message__c).csam_t1__Status__c == 'Pending') 
                ? false : true;
            simListLastStatus.add(ssr);
        }
        system.debug('simListLastStatus.size :' +simListLastStatus.size());
        simListLastStatus = sortAndFilterSimStatuses(simListLastStatus);
        system.debug('simListLastStatus.size :' +simListLastStatus.size());
        refreshPie(simListLastStatus);
        
    }

public class sm2m_users{
    String today;
    String last_7_days;
    String all_time;
    public String gettoday() {return this.today;}  
    public String getlast_7_days() {return this.last_7_days;}  
    public String getall_time() {return this.all_time;}  
    public sm2m_users(String today, String last_7_days, String all_time){
        this.today = today;
        this.last_7_days = last_7_days;
        this.all_time = all_time;
    }
}

  public String getSm2mUsers() {

    // Instantiate a new http object
    Http h = new Http();

     // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
    HttpRequest req = new HttpRequest();
    req.setEndpoint(Label.ETL_endpoint + 'CustomerOnlineReport/getUserStatistics.php');
    req.setMethod('GET');

    // Send the request, and return a response
    HttpResponse res = h.send(req);
    return res.getBody();
  }

 
 public String getRabbitQueues(){
    Http h = new Http();
     // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
    HttpRequest req = new HttpRequest();
    req.setEndpoint('http://52.58.12.199:15672/api/queues');
    String username = 'm2m_user';
    String password = '11EIQbn8nO';
    Blob headerValue = Blob.valueOf(username +':' +password);
    String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
    req.setHeader('Authorization', authorizationHeader);
    req.setMethod('GET');
    // Send the request, and return a response
    HttpResponse res = h.send(req);
    return res.getBody();
 }


public class details{
    public Decimal rate;
}
public class rabbitQueue{
    public Integer memory;
    public Integer messages;
    public details messages_details;
    public Integer messages_ready;
    public details messages_ready_details;
    public Integer messages_unacknowledged;
    public details messages_unacknowledged_details;
    public String idle_since;
    public String consumer_utilisation;
    public String policy;
    public String exclusive_consumer_tag;
    public integer consumers;
    public String recoverable_slaves;
    public String state;
    public integer messages_ram;
    public integer messages_ready_ram;
    public integer messages_unacknowledged_ram;
    public integer messages_persistent;
    public integer message_bytes;
    public integer message_bytes_ready;
    public integer message_bytes_unacknowledged;
    public integer message_bytes_ram;
    public integer message_bytes_persistent;
    public String head_message_timestamp;
    public integer disk_reads;
    public integer disk_writes;
    public status backing_queue_status;
    public String name;
    public String vhost;
    public boolean durable;
    public boolean auto_delete;
    public boolean exclusive;
    public details arguments;
    public String node;

    public String getName(){return this.name;}
    public String getState(){return this.state;}
    public Integer getMessages(){return this.messages;}
    public Integer getmessages_ready(){return this.messages_ready;}
    public Integer getmessages_unacknowledged(){return this.messages_unacknowledged;}



}
public class status{
    public String mode;
    public integer q1;
    public integer q2;
    public List<String> delta;
    public integer q3;
    public integer q4;
    public String len;
    public String target_ram_count;
    public integer next_seq_id;
    public Decimal avg_ingress_rate;
    public Decimal avg_egress_rate;
    public Decimal avg_ack_ingress_rate;
    public Decimal avg_ack_egress_rate;
}


}