global class Populate_OrderRequest_On_Stock implements Database.Batchable<sObject>{

    
    global Database.QueryLocator start(Database.BatchableContext BC){
        string query = 'select id,Runtime__c, FC_Type__c,Activation_Date__c,Commercial_Date__c,End_date__c,Third_Upload_File__c,Third_Upload_File__r.Order_Request__c,Order_Request__c from Stock__c where Runtime__c != null';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Stock__c> allStocks){
        
        for(Stock__c st : allStocks){
        	if(st.Third_Upload_File__r.Order_Request__c != null && st.Order_Request__c != null){
        		st.Order_Request__c = st.Third_Upload_File__r.Order_Request__c;
        	}
        }
        
        if(allStocks.size() > 0){
        	update allStocks;
        }
    }
    
    global void finish(Database.BatchableContext BC){

    }
}