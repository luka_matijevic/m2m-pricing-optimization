public class BillingSettingsHelper {
	
	public static Integer getPrebillPollerDelaySeconds() {
		return (Integer) Billing_Settings__c.getOrgDefaults().Prebill_Poller_Delay_Seconds__c;
	}
	
	public static Boolean isDisablePrebillPollerReschedule() {
		return Billing_Settings__c.getOrgDefaults().Disable_Prebill_Poller_Reschedule__c == true;
	}
}