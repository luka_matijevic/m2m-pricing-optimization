public class BillingRateModels {
	
	public static BillingRateModel billingRateModel(String billingModel) {
		if ('One Off' == billingModel) {
			return BillingRateModel.ONEOFF;
		} else if ('Rate Payment' == billingModel) {
			return BillingRateModel.INSTALLMENTS;
		} else {
			return null;
		}
	}
	
	public static String toString(BillingRateModel brm) {
		if (BillingRateModel.ONEOFF == brm) {
			return 'One Off';
		} else if (BillingRateModel.INSTALLMENTS == brm) {
			return 'Rate Payment';
		}
		return null;
	}

	public static String billingRateModelString(csord__Order_Line_Item__c oli) {
		return oli.Billing_Rate_Model__c;
	}
	
	public static String billingRateModelString(csord__Service_Line_Item__c sli) {
		return sli.csord__Service__r.Billing_Rate_Model__c;
	}
	
	public static BillingRateModel billingRateModel(csord__Service_Line_Item__c sli) {
		return billingRateModel(billingRateModelString(sli));
	}

	public static BillingRateModel billingRateModel(csord__Order_Line_Item__c oli) {
		return billingRateModel(billingRateModelString(oli));
	}
}