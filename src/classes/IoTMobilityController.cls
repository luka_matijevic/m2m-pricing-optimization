/*
	Author: Petar Matkovic 14.9.2016.
*/
public class IoTMobilityController {
	private String qsOppId = ApexPages.currentPage().getParameters().get('oppId');
	private String qsAccId = ApexPages.currentPage().getParameters().get('accId');
	private String qsBundleId = ApexPages.currentPage().getParameters().get('bundleId');
	private ApexPages.StandardController standardController;
	private Opportunity opportunity;
	private Cscfga__Product_Configuration__c productconfigurationSObject;
	private cscfga.Api_1.ApiSession apiSession;
	private GEOTAB_Settings__c geotabSettings;
	private boolean noTriggerUser = false;
	private No_Trigger__c noTrigger = null;

	/***	PROPERTIES	***/
	public Cscfga__Product_Bundle__c theBundle {get;set;}
	public cscfga.ProductConfiguration productConfiguration {get; set;}
	public List<cscfga.Attribute> screenAttributes {get;set;}
	public List<FLOWrapper> summaryItems {get;set;}
	public List<FLOWrapper> lineItems {get; set;}
	public Boolean hasSynchedBundle {get;set;}

	/***	CONSTRUCTOR
			INITIALIZATION	***/
	public IoTMobilityController(ApexPages.StandardController controller) {
		standardController = controller;
	}

	public void init(){
		hasSynchedBundle = false;
		String oppQuery = 'SELECT 	id, name, AccountId, HasSynchedBundle__c, RecordType.Name, Account.Name, ' 
									+ 'Account.BillingCountry, Account.BillingStreet, Account.BillingCity, '
									+'Account.BillingPostalCode '
							+'FROM Opportunity where id=\''+qsOppId+'\'';
		opportunity = Database.query(oppQuery);
		
		if (qsBundleId == null){
			//No bundle; Fetch new product Definition and create configuration from it
			theBundle = (Cscfga__Product_Bundle__c)standardController.getRecord();
			Cscfga__Product_Definition__c productDefinition = [SELECT Id, Name
																FROM Cscfga__Product_Definition__c
																WHERE Name = 'IoT Mobility'];
			if (theBundle.cscfga__Opportunity__c == null)
				theBundle.cscfga__Opportunity__c = qsOppId;
			upsert theBundle;
			apiSession = cscfga.Api_1.getApiSession(productDefinition);
			productConfiguration = apiSession.getConfiguration();
			productconfigurationSObject = productConfiguration.getSObject();
			productconfigurationSObject.cscfga__Product_Bundle__c = theBundle.id;
			productConfiguration.setOppId(qsOppId);
			apiSession.setBundle(theBundle);
			productconfigurationSObject.cscfga__Product_Bundle__c = theBundle.id;

			apiSession.persistConfiguration(true);
            apiSession.setConfigurationToEdit(productconfigurationSObject);
		}
		else{
			//Bundle already created
			String query_pc = 'SELECT ' +  SObjectHelper.getFieldListForSOQL('cscfga__product_configuration__c',null,null) 
							+ ' FROM cscfga__product_configuration__c'
							+ ' WHERE cscfga__product_bundle__c=\''+qsBundleId+'\'';
			List<cscfga__Product_Configuration__c> pclist = Database.query(query_pc);
			if (pclist == null || pclist.size() == 0){
				addMessageAndRedirect('No Product Configuration Found', ApexPages.severity.ERROR);
			}

			productconfigurationSObject = pclist[0];

			String query_bundl = 'SELECT ' +  SObjectHelper.getFieldListForSOQL('cscfga__product_bundle__c',null,null) 
								+ ' FROM cscfga__product_bundle__c'
								+' WHERE Id=\''+qsBundleId+'\'';
			List<cscfga__Product_Bundle__c> pblist = database.query(query_bundl);
			if (pblist == null || pblist.size() == 0){
				addMessageAndRedirect('No Bundle Found for Id ' + qsBundleId, ApexPages.severity.ERROR);
			}

			theBundle = pblist[0];
			apiSession = cscfga.Api_1.getApiSession(productconfigurationSObject);
		}

		productConfiguration = apiSession.getConfiguration();
		productconfigurationSObject =  productConfiguration.getSObject();
		productconfigurationSObject.cscfga__Configuration_Offer__c = null;

		
		List<cscfga.Attribute> prodConfAttList = productConfiguration.getAttributes();
		screenAttributes = new cscfga.Attribute[prodConfAttList.size()];
		lineItems = new FLOWrapper[prodConfAttList.size()];
		summaryItems = new FLOWrapper[prodConfAttList.size()];

		for (cscfga.Attribute anAttribute : prodConfAttList){
			cscfga__Attribute__c att = anAttribute.getSObject();
            cscfga__Attribute_Definition__c def = anAttribute.getDefinition();
            
            if(def.cscfga__Column__c==1) {
                screenAttributes.add(Integer.valueOf(anAttribute.getDefinition().cscfga__Row__c), anAttribute);
            }

            if(def.cscfga__Column__c==0) {
            	FLOWrapper fwrapper = new FLOWrapper();
                fwrapper.attName=anAttribute;
                if (att.cscfga__Recurring__c==TRUE){
                    fwrapper.RecCharge=anAttribute;
                }
                else if (att.cscfga__Recurring__c==FALSE){
                    fwrapper.nonRecCharge=anAttribute;
                }

                for (cscfga.Attribute anAttribute2 : prodConfAttList){
                	cscfga__Attribute__c att2 = anAttribute2.getSObject();
                    if (att.Name + ' TCV'==att2.Name){
                    	fwrapper.TCV=anAttribute2;
                    }
                }
                lineItems.add(Integer.valueOf(anAttribute.getDefinition().cscfga__Row__c),fwrapper);
            }

            if(def.cscfga__Column__c==4){
            	FLOWrapper fwrapper = new FLOWrapper();
            	fwrapper.attName=anAttribute;
                summaryItems.add(Integer.valueOf(anAttribute.getDefinition().cscfga__Row__c), fwrapper);
            }
		}

		if (opportunity.HasSynchedBundle__c){
			hasSynchedBundle = opportunity.HasSynchedBundle__c;
			Id ordReqId = getOrderRequestIdForProductConfiguration();
			if (ordReqId != null)
				addMessage('This Bundle is already Synched and Order is created. <a href="/' + ordReqId + '">See Order here.</a>', ApexPages.severity.INFO);
			else
				addMessage('This Bundle is already Synched and Order is created.', ApexPages.severity.INFO);
		}
		else{
			//Account Active Quantity
			//get acive GEOTAB qunatity for account-used for discount calculation
			Integer activeQuantity = 0;
			AggregateResult[] groupedResults
									= [SELECT SUM(Geotab_Quantity__c)sumall
									FROM Csord__service__c 
									WHERE csord__status__c = 'active' 
											and csord__subscription__r.csord__order__r.order_request__r.account__c = :qsAccId 
											and csord__subscription__r.csord__order__r.order_request__r.opportunity_type__c = 'SIXT'];
			if (groupedResults[0].get('sumall') != null)
				activeQuantity = Integer.valueOf(groupedResults[0].get('sumall'));

			
			for (cscfga.Attribute anAttribute : prodConfAttList){
				cscfga__Attribute__c att = anAttribute.getSObject();
	            cscfga__Attribute_Definition__c def = anAttribute.getDefinition();
	            if (def.Name == 'Account Active Quantity'){
	            	att.cscfga__value__c = String.valueOf(activeQuantity);
	            }
	        }
		}
	}

	public PageReference cancelButtonClick(){
		return getBackToOpportunity();
	}

	//Save bundle and get back to opportunity
	public PageReference saveBundleAndGoToOpportunity(){
		PageReference nextPage;
		if (saveBundle()){
			nextPage = getBackToOpportunity();
		}
		else{
			nextPage = getCurrentPage();
		}
		return nextPage;
	}

	//Save bundle and refresh page
	public PageReference quickSaveBundle(){
		PageReference nextPage;
		
		boolean isSaved = saveBundle();

        nextPage = getCurrentPage();
		if (isSaved){
			addMessage('Pricing succesfully saved.', ApexPages.severity.Info);
		}
		else{
			nextPage = getCurrentPage();
		}
		nextPage.setRedirect(true);
        return nextPage;
	}

	public PageReference saveBundleAndCreateContract(){
		String orderId = null;
		PageReference nextPage;
		List<No_Trigger__c> noTriggerList = [Select Id, SetupOwnerId, Flag__c 
											from No_Trigger__c 
											where SetupOwnerId =: UserInfo.getUserId() limit 1];
        if (noTriggerList.isEmpty()) {
	        noTriggerUser=true;
	        notrigger = new No_Trigger__c();
	        notrigger.Flag__c = true;
	        notrigger.SetupOwnerId = UserInfo.getUserId();
	        upsert notrigger;
        }
        else{
	        noTriggerList[0].Flag__c = true;
	        upsert noTriggerList[0];
	        notrigger = noTriggerList[0];
        }

		//Check if Bundle has created order
		if (opportunity.HasSynchedBundle__c){
			addMessage('The Order for this Opportunity is already created.', ApexPages.severity.ERROR);
			nextPage = getCurrentPage();
		}
		else{
			//Set Product Name and save bundle
			productconfigurationSObject.Original_Product_Name__c = 'IoT Mobility';
			saveBundle();

			Integer quantity;
			Integer discountTmp = 0;
			//Set Runtime per subscription in Opp
			for (cscfga.Attribute anAttribute : productConfiguration.getAttributes()){
                cscfga__Attribute__c att = anAttribute.getSObject();
                if (att.Name=='Runtime')
                	opportunity.Contract_Term_Per_Subscription__c=decimal.valueof(anAttribute.getValue());
                if (att.Name=='Quantity')
                	quantity = Integer.valueOf(anAttribute.getValue());
                if (att.Name=='Discount')
                	discountTmp = Integer.valueOf((1-anAttribute.getPrice()) * 100);
            }
            opportunity.Contract_Term_Per_SIM__c = 0;
            update opportunity;

            // Activate triggers & remove the created NoTrigger record (if any)
            notrigger.Flag__c = false;
            upsert notrigger; 
            if (noTriggerUser){
                delete notrigger;
            }


            if(!test.isRunningTest()){
                //sync bundle
                cscfga.Api_1.syncBundleToOpportunity(theBundle.id);
            }

            //Get Frame Contract
            String fcQuery;
            if(!test.isRunningTest()){
                fcQuery = 'select id,name, Customer__c, opportunity__c '
                			+'from frame_contract__c '
                			+'where Id=\''+ getGeotabCustomSettings().Frame_Contract_Id__c + '\'';
            }else{
                fcQuery = 'select id,name, Customer__c, opportunity__c from frame_contract__c LIMIT 1';
            }
            //connect with open frame contract
            Frame_Contract__c frameContract  = Database.query(fcQuery);
            if (frameContract == null){
            	addMessageAndRedirect('No Frame Contract found. Order not created.', ApexPages.severity.ERROR);
            }
            frameContract.Opportunity__c = qsOppId;
            update frameContract;

            Account acc = [select id, VAT__c, Language__c, Billing_Cycle__c, Currency__c, ORV_Customer__c, BillingCountry 
            				from Account 
            				where id=:opportunity.AccountId];
            AccountHelper.PopulateAccountSM2MInformation(acc);
            AccountHelper.CreateBillingAccount(frameContract);

            //create order request and connect it with opportunity and its attributes
            Order_Request__c orderRequest = new Order_Request__c(Frame_Contract__c = frameContract.id);
            orderRequest.RecordTypeId = getGeotabCustomSettings().Order_Request_Type_Id__c;
            orderRequest.Use_Account_Address__c = false;
            orderRequest.Runtime__c = Integer.valueOf(opportunity.Contract_Term_Per_Subscription__c);
            orderRequest.ORV_Discount__c = discountTmp + '%';
            orderRequest.Account__c=opportunity.AccountId ;
            orderRequest.Opportunity__c = qsOppId;
            orderRequest.Order_Quantity__c = quantity;
            List<Billing_Account__c> bics = [select id,name 
            								from Billing_account__c 
            								where Payer__c=:opportunity.Accountid limit 1];
            Billing_Account__c bic = new Billing_Account__c();
            
            if (bics != null && bics.size() > 0) {
                bic = bics[0];
                orderRequest.Billing_Account__c=bic.id;
            }
            insert orderRequest;
            orderId = orderRequest.Id;

            //create other related products
            Product_Order__c productOrder = new Product_Order__c();
            productOrder.Order_Request__c=orderRequest.Id;       
            productOrder.Quantity__c=quantity;
            upsert productOrder;

            List<Product_Configuration_Order__c> pcoList = new List<Product_Configuration_Order__c>();
            Product_Configuration_Order__c cor = new Product_Configuration_Order__c();
            cor.Product_Configuration__c=productconfigurationSObject.Id;
            cor.Product_Order__c=productOrder.Id;
            pcoList.add(cor);
            upsert pcoList;

			nextPage = new PageReference('/' + orderId);
			nextPage.setRedirect(true);
		}
		return nextPage;
	}

	private boolean saveBundle(){
		boolean isSaved = true;
		if (opportunity.HasSynchedBundle__c == true){
            addMessage('This Opportunity is already saved and has synced with Product Bundle.', ApexPages.severity.ERROR);
            isSaved = false;
        }
        else{
        	if (theBundle.cscfga__Opportunity__c == null){
        		theBundle.cscfga__Synchronised_with_Opportunity__c = true;
        		theBundle.cscfga__Opportunity__c = qsOppId;
        	}
        	upsert theBundle;
        	productconfigurationSObject.cscfga__Product_Bundle__c=theBundle.id;
        	productConfiguration.setOppId(qsOppId);
        	apiSession.setBundle(theBundle);
        	productconfigurationSObject.cscfga__Product_Bundle__c = theBundle.id;
        	apiSession.updateConfig();
            apiSession.persistConfiguration(true);
        }
        return isSaved;
	}

	private GEOTAB_Settings__c getGeotabCustomSettings(){
		if (geotabSettings == null){
			geotabSettings = GEOTAB_Settings__c.getInstance();
		}
		return geotabSettings;
	}

	private PageReference getBackToOpportunity(){
		PageReference newocp = new PageReference('/' + qsOppId);
		newocp.setRedirect(true);
		return newocp;
	}

	private Id getOrderRequestIdForProductConfiguration(){
		Product_Configuration_Order__c prodConf = [select Product_Order__r.Order_Request__c
				from Product_Configuration_Order__c 
				where Product_Configuration__c=:productconfigurationSObject.Id limit 1];

		Id ordReqId = null; 
		if(prodConf != null)
			ordReqId = prodConf.Product_Order__r.Order_Request__c;
		return ordReqId;
	}

	/***	ADD MESSAGE TO THE PAGE 	***/
	private void addMessage(String message, ApexPages.severity severity) {
        ApexPages.addMessage(new ApexPages.Message(severity, message));
    }

    private void addMessageAndRedirect(String message, ApexPages.severity severity){
    	addMessage(message, severity);
    	getCurrentPage();
    }

    private PageReference getCurrentPage(){
    	return new PageReference('/apex/IoTMobilityPricing?oppId='+qsOppId+'&bundleId='+theBundle.id+'&accId='+qsAccId);
    }

    public class FLOWrapper
    {
        //wrapper class used for live view table
        //gets many attributes in one collection for line items presentation
        public cscfga.Attribute attName {get;set;} 
        public cscfga.Attribute recCharge {get;set;} 
        public cscfga.Attribute nonRecCharge {get;set;}
        public cscfga.Attribute TCV {get;set;}
        integer quantity {get; set;}
        public FLOWrapper()
        {

        }
    }
}