@isTest (SeeAllData=false)
private class TestRetireSIMsBatch {
     
    static testmethod void retireSingleSIMTest(){
        List<Stock__c> sims = TestDataGeneratorForTerminateSIMProcess.createSIMsforRetireSIMs(1);
        Test.startTest();
            Database.executeBatch(new RetireSIMsBatch());
       
        Test.stopTest();
          List<Stock__c> fetchedSIMs = [Select Id, Termination_Status__c, SM2M_Status__c, end_date__c from Stock__c];
        system.debug('fetchedSIMs: ' + fetchedSIMs);
         system.debug('fetchedSIMs[0: ' + fetchedSIMs[0]);
        System.assert(fetchedSIMs != null, 'Sims doesn\'t exist');
        System.assert(fetchedSIMs.size() > 0, 'No SIMs created');
        //System.assertEquals('RETIRED', fetchedSIMs[0].SM2M_Status__c);
        
    }
    
    static testmethod void retireBulkSIMstest(){
        List<Stock__c> sims = TestDataGeneratorForTerminateSIMProcess.createSIMsforRetireSIMs(200);
        Test.startTest();
            Database.executeBatch(new RetireSIMsBatch());
        Test.stopTest();
        List<Stock__c> fetchedSIMs = [Select Id, Termination_Status__c, SM2M_Status__c from Stock__c];
        System.assert(fetchedSIMs != null, 'Sims doesn\'t exist');
        System.assert(fetchedSIMs.size() > 0, 'No SIMs created');
        /*for(Stock__c sim : fetchedSIMs){
            System.assertEquals('RETIRED', sim.SM2M_Status__c);
        }*/
    }
}