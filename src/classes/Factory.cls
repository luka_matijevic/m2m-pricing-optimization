public without sharing class Factory {
    //--------------------------------------------------------------------------
    // Create connectivity tariff options
    //--------------------------------------------------------------------------
    public static IConnectivityTariffOptions createConnectivityTariffOptions(Id tariffId, Id accountId, Id configurationId) {
        //system.debug('tariffId: ' + tariffId);
        //system.debug('accountId: ' + accountId);
        //system.debug('configurationId: ' + configurationId);

        //
        // Create original and parent tariff
        //
        ITariff originalTariff = Factory.createTariff(tariffId);
        ITariff parentTariff;
        
        if (originalTariff.getTariff().Parent_Tariff__c != null) {
            system.debug(originalTariff);
            parentTariff = Factory.createTariff(originalTariff.getTariff().Parent_Tariff__c);
            system.debug(parentTariff);
        }
        else {
            system.debug(originalTariff);
            parentTariff = originalTariff;
        }

        //
        // Create connectivity configurations
        //
        IConnectivityConfigurations configurations = new ConnectivityConfigurations(configurationId);

        //
        // Create connectivity tariff options
        //
        IConnectivityTariffOptions returnValue = new ConnectivityTariffOptions(originalTariff, parentTariff, accountId, configurationId, configurations);

        return returnValue;
    }

    public static IConnectivityTariffOptions createConnectivityTariffOptionsWithList(Id tariffId, Id accountId, Id configurationId,List<ITariffOption> includedTariffOptions,List<ITariffOption> additionalTariffOptions,List<ITariffOption> supplementaryServiceTariffOptions) 
    {
        System.debug('creating connectivity tariff options');
        ITariff originalTariff = Factory.createTariffWithList(tariffId,includedTariffOptions, additionalTariffOptions,  supplementaryServiceTariffOptions);
        ITariff parentTariff;
        


        parentTariff = originalTariff;


        //
        // Create connectivity configurations
        //
        IConnectivityConfigurations configurations = new ConnectivityConfigurations(configurationId);
        //
        // Create connectivity tariff options
        //
        IConnectivityTariffOptions returnValue = new ConnectivityTariffOptions(originalTariff, parentTariff, accountId, configurationId, configurations);

        system.debug(additionalTariffOptions);
        System.debug('return value '+ returnValue);
        return returnValue;
    }
    
    

    //--------------------------------------------------------------------------
    // Create tariff
    //--------------------------------------------------------------------------
    public static ITariff createTariff(Id tariffId)
    {
        //
        // Retrieve tariff with SOQL call
        //
        String tariffSOQL = 'SELECT '
            + SObjectHelper.getFieldListForSOQL('Tariff__c', null, null)
            + ' FROM Tariff__c'
            + ' WHERE Id = \'' + tariffId + '\'';
        
        list<Tariff__c> tariffLstTmp = new list<Tariff__c>();
        tariffLstTmp = Database.query(tariffSOQL);
        Tariff__c tariff = new Tariff__c();
        if(tariffLstTmp.size() > 0){
           tariff = tariffLstTmp[0];
        }

        //
        // Retrieve tariff options with SOQL call
        //
        String tariffOptionsSOQL = 'SELECT '
            + SObjectHelper.getFieldListForSOQL('Tariff_Option__c', null, null)
            + ', ( SELECT '
            + SObjectHelper.getFieldListForSOQL('Rate_Card__c', null, null)
            + ' FROM Rate_Cards__r'
            + ' ORDER BY Sequence_No__c, Name )'
            + ', ( SELECT '
            + SobjectHelper.getFieldListForSOQL('Service_Fee__c', null, null)
            + ' FROM Service_Fee__r'
            + ' ORDER BY Sequence_No__c, Name )'
            + ' FROM Tariff_Option__c WHERE Tariff__c = \'' + tariffId + '\''
            + ' ORDER BY Sequence_No__c, Name';
            
            
        List<Tariff_Option__c> toList = new list<Tariff_Option__c>();
        toList = Database.query(tariffOptionsSOQL);

        //
        // Create tariff option lists
        //
        List<ITariffOption> includedTariffOptions = new List<ITariffOption>();
        List<ITariffOption> additionalTariffOptions = new List<ITariffOption>();
        List<ITariffOption> supplementaryServiceTariffOptions = new List<ITariffOption>();
        
        createTariffOptions(toList, includedTariffOptions, additionalTariffOptions, supplementaryServiceTariffOptions);

        return new Tariff(tariff, includedTariffOptions, additionalTariffOptions, supplementaryServiceTariffOptions, new InsertHelper());
    }

    public static ITariff createTariffWithList(Id tariffId,List<ITariffOption> includedTariffOptions,List<ITariffOption> additionalTariffOptions,List<ITariffOption> supplementaryServiceTariffOptions) 
    {
        //
        // Retrieve tariff with SOQL call
        //
        String tariffSOQL = 'SELECT '
            + SObjectHelper.getFieldListForSOQL('Tariff__c', null, null)
            + ' FROM Tariff__c'
            + ' WHERE Id = \'' + tariffId + '\'';
        Tariff__c tariff = Database.query(tariffSOQL);
        
        for (ITariffOption tariffOption : includedTariffOptions) {
                tariffOption.setQuantity(1);
            }
        
        return new Tariff(tariff, includedTariffOptions, additionalTariffOptions, supplementaryServiceTariffOptions, new InsertHelper());
    }

    //--------------------------------------------------------------------------
    // Create tariff list
    //--------------------------------------------------------------------------
    public static List<ITariff> createTariffs(Set<Id> tariffIds)
    {
        //
        // Retrieve tariff with SOQL call
        //
        String tariffSOQL = 'SELECT '
            + SObjectHelper.getFieldListForSOQL('Tariff__c', null, null)
            + ' FROM Tariff__c'
            + ' WHERE Id IN :tariffIds';
        List<Tariff__c> tariffs = Database.query(tariffSOQL);

        //
        // Retrieve tariff options with SOQL call
        //
        String tariffOptionsSOQL = 'SELECT '
            + SObjectHelper.getFieldListForSOQL('Tariff_Option__c', null, null)
            + ', ( SELECT '
            + SObjectHelper.getFieldListForSOQL('Rate_Card__c', null, null)
            + ' FROM Rate_Cards__r'
            + ' ORDER BY Sequence_No__c, Name )'
            + ', ( SELECT '
            + SobjectHelper.getFieldListForSOQL('Service_Fee__c', null, null)
            + ' FROM Service_Fee__r'
            + ' ORDER BY Sequence_No__c, Name )'
            + ' FROM Tariff_Option__c WHERE Tariff__c IN :tariffIds'
            + ' ORDER BY Sequence_No__c, Name';
        List<Tariff_Option__c> toList = Database.query(tariffOptionsSOQL);
        
        List<ITariff> returnValue = new List<ITariff>();
        for (Tariff__c tariff : tariffs)
        {
            //
            // Search tariff options for this tariff
            //
            List<Tariff_Option__c> currentTOList = new List<Tariff_Option__c>();
            for (Tariff_Option__c tariffOption : toList) {
                if (tariffOption.Tariff__c == tariff.Id) {
                    System.debug('Tariff option ' + tariffOption);
                    currentTOList.add(tariffOption);
                }
            }
            //
            // Create tariff option lists
            //
            List<ITariffOption> includedTariffOptions = new List<ITariffOption>();
            List<ITariffOption> additionalTariffOptions = new List<ITariffOption>();
            List<ITariffOption> supplementaryServiceTariffOptions = new List<ITariffOption>();
            
            
            createTariffOptions(currentTOList, includedTariffOptions, additionalTariffOptions, supplementaryServiceTariffOptions);

            returnValue.add(new Tariff(tariff, includedTariffOptions, additionalTariffOptions, supplementaryServiceTariffOptions, new InsertHelper()));
        }
        if(returnValue != null){
            return returnValue;
        }else{
            return null;
        }
        
    }

    
    // Preparing map of Tariff Option id as key and associated list of Rate_Card_Country_Management__c as value
    
    public static map<id, list<Rate_Card_Country_Management__c>> prepareRatecardCountryMangement(list<Tariff_Option__c> tariffOptions){
        map<id, list<Rate_Card_Country_Management__c>> tariffRateCardMangement = new map<id, list<Rate_Card_Country_Management__c>>();
        if(tariffOptions != null && tariffOptions.size() > 0){
            set<id> rateCardIds = new set<id>();
            for (Tariff_Option__c to : tariffOptions){
                for(Rate_Card__c rc : to.Rate_Cards__r){
                    rateCardIds.add(rc.id);
                }    
            }
            if(rateCardIds.size() > 0){
                list<Rate_Card_Country_Management__c> rtMgmnt = new list<Rate_Card_Country_Management__c>();
                rtMgmnt = [SELECT Id, Name, Rate_Card__c, Rate_Card__r.Tariff_Option__c ,Country_code__c, Country_name__c, Ratio__c, Rate_Card_Country_Management_Id__c FROM Rate_Card_Country_Management__c  WHERE Rate_Card__c IN :rateCardIds];
                if(rtMgmnt.size() > 0){
                    for(Rate_Card_Country_Management__c rcm : rtMgmnt){
                        if(rcm.Rate_Card__r.Tariff_Option__c != null){
                            list<Rate_Card_Country_Management__c> tmp = new list<Rate_Card_Country_Management__c>();
                            tmp.add(rcm);
                            if(tariffRateCardMangement.get(rcm.Rate_Card__r.Tariff_Option__c) != null){
                                tmp.addAll(tariffRateCardMangement.get(rcm.Rate_Card__r.Tariff_Option__c));
                            }
                            tariffRateCardMangement.put(rcm.Rate_Card__r.Tariff_Option__c,tmp);
                        }
                    }
                }
            }
            
            for (Tariff_Option__c to : tariffOptions){
                if(tariffRateCardMangement.get(to.id) == null){
                    tariffRateCardMangement.put(to.id,new list<Rate_Card_Country_Management__c>());
                }
            }    
        }
        return tariffRateCardMangement;
    }

    public static list<EU_Countries__c> prepareCountriesList(){
        List<EU_Countries__c> countriesCustSett = new List<EU_Countries__c>();
        countriesCustSett = [SELECT Id, Name, Language__c, Code__c FROM EU_Countries__c WHERE Language__c = 'ENG' ORDER BY Name ASC];
        return countriesCustSett;
    }

    //--------------------------------------------------------------------------
    // Create lists of tariff options
    //--------------------------------------------------------------------------
    private static void createTariffOptions(
            List<Tariff_Option__c> tariffOptions,
            List<ITariffOption> includedTariffOptions,
            List<ITariffOption> additionalTariffOptions,
            List<ITariffOption> supplementaryServiceTariffOptions)
    {
        //
        // Fill the structures
        //
        map<id, list<Rate_Card_Country_Management__c>> tariffRateCardMangement = prepareRatecardCountryMangement(tariffOptions);
        
        List<EU_Countries__c> countriesCustSett = prepareCountriesList();
        
        for (Tariff_Option__c to : tariffOptions)
        {
            //
            // Preset recurring target fee
            //
            if (to.Recurring_Base_Fee__c == null)
            {
                to.Recurring_Base_Fee__c = 0;
            }
            if (to.Recurring_Target_Fee__c == null)
            {
                to.Recurring_Target_Fee__c = to.Recurring_Base_Fee__c;
            }
            //
            // Create tariff option
            //
            
            TariffOption tariffOption = new TariffOption(to, createRateCards(to.Rate_Cards__r, to.Type__c,tariffRateCardMangement.get(to.id),countriesCustSett), createServiceFees(to.Service_Fee__r), true, 0, 0);
            //
            // Included
            //
            if (tariffOption.getTariffOption().Type__c == 'Included')
            {
                //
                // Quantity of included tariff options must be one for
                // calculations
                //
                tariffOption.setQuantity(1);
                includedTariffOptions.add(tariffOption);
                system.debug(tariffOption);
            }
            //
            // Additional
            //
            else if (tariffOption.getTariffOption().Type__c == 'Additional')
            {
                additionalTariffOptions.add(tariffOption);
            }
            //
            // Supplementary service
            //
            else
            {
                supplementaryServiceTariffOptions.add(tariffOption);
            }
        }
    }
    
    //--------------------------------------------------------------------------
    // Create list of rate cards -- Pavan
    //--------------------------------------------------------------------------
    public static List<IRateCard> createRateCards(List<Rate_Card__c> rateCards, String tariffOptionType,list<Rate_Card_Country_Management__c> rateCardManageMent,list<EU_Countries__c> euCountries)
    {
        List<SelectOption> availableCountries = new List<SelectOption>();
        List<IRateCard> returnValue = new List<IRateCard>();
        List<IRateCardCountryManagement> rccmList = new List<IRateCardCountryManagement>();
        List<Rate_Card_Country_Management__c> rccm = null;
        List<EU_Countries__c> countriesCustSett = new List<EU_Countries__c>();
        
        if(tariffOptionType == 'Included' || tariffOptionType == 'Additional' || tariffOptionType == 'Supplementary Service' || tariffOptionType == 'Support&Service') {
            List<Id> listOfIds = new List<Id>();
            for(Rate_Card__c rc : rateCards) {
                    listOfIds.add(rc.Id);
            }
            
            if(listOfIds != null && listOfIds.size() > 0 ){
                if(rateCardManageMent != null && rateCardManageMent.size() > 0){
                    rccm = new List<Rate_Card_Country_Management__c>();
                    rccm.addAll(rateCardManageMent);          
                }
            }else{
                rccm = new List<Rate_Card_Country_Management__c>();
            }
            
            if(euCountries != null && euCountries.size() > 0){
                countriesCustSett = new List<EU_Countries__c>();
                countriesCustSett.addAll(euCountries);
            }
            
        }
        
        for (Rate_Card__c rc : rateCards)
        {
            if(rccm != null && rccm.size() > 0) {
                // fetch Rate Card Countries Management for current Rate Card
                rccmList = new List<IRateCardCountryManagement>();
                for(Rate_Card_Country_Management__c rcc : rccm) {
                    if(rcc != null && rcc.Rate_Card__c == rc.Id) {
                        rccmList.add(new RateCardCountryManagement(rcc));
                    }
                }
            }
            
            // Preparing drop down list for each rate card
            if(rc != null && (tariffOptionType == 'Included' || tariffOptionType == 'Additional' || tariffOptionType == 'Supplementary Service' || tariffOptionType == 'Support&Service')) {
                availableCountries = new List<SelectOption>();
                
                if(rc.Worldzone__c.contains('Europe')) {
                    if(countriesCustSett != null && countriesCustSett.size() > 0) {
                        for (EU_Countries__c eu : countriesCustSett) {
                            availableCountries.add(new SelectOption(eu.Code__c, eu.Name));
                        }
                    }
                }
                else if(rc.Worldzone__c.contains('World')) {
                    Schema.DescribeFieldResult fieldResult = User.Countrycode.getDescribe();
                    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                    
                    for(Schema.PicklistEntry f : ple) {
                        Boolean isExisting = false;
                        for(EU_Countries__c ctry : countriesCustSett) {
                            if(f.getValue().equalsIgnoreCase(ctry.Code__c)) {
                                isExisting = true;
                            }
                        }
                        
                        if(!isExisting)
                            availableCountries.add(new SelectOption(f.getValue(), f.getLabel()));
                    }
                }
            }
            //
            // Preset target usage fee
            //
            if (rc.Base_Usage_Fee__c == null)
            {
                rc.Base_Usage_Fee__c = 0;
            }
            if (rc.Target_Usage_Fee__c == null)
            {
                rc.Target_Usage_Fee__c = rc.Base_Usage_Fee__c;
            }
            //
            // Create rate card and add it to list
            //
            returnValue.add(new RateCard(rc, rccmList, availableCountries));
        }
        return returnValue;
    }
    
    //--------------------------------------------------------------------------
    // Create list of rate cards
    //--------------------------------------------------------------------------
    public static List<IRateCard> createRateCards(List<Rate_Card__c> rateCards, String tariffOptionType)
    {
        List<SelectOption> availableCountries = new List<SelectOption>();
        List<IRateCard> returnValue = new List<IRateCard>();
        List<IRateCardCountryManagement> rccmList = new List<IRateCardCountryManagement>();
        List<Rate_Card_Country_Management__c> rccm = null;
        List<EU_Countries__c> countriesCustSett = new List<EU_Countries__c>();
        
        if(tariffOptionType == 'Included' || tariffOptionType == 'Additional' || tariffOptionType == 'Supplementary Service' || tariffOptionType == 'Support&Service') {
            List<Id> listOfIds = new List<Id>();
            for(Rate_Card__c rc : rateCards) {
                    listOfIds.add(rc.Id);
            }
            
            if(listOfIds != null && listOfIds.size() > 0)
                rccm = [SELECT Id, Name, Rate_Card__c, Country_code__c, Country_name__c, Ratio__c, Rate_Card_Country_Management_Id__c FROM Rate_Card_Country_Management__c  WHERE Rate_Card__c IN :listOfIds];
            else
                rccm = new List<Rate_Card_Country_Management__c>();
                
            countriesCustSett = [SELECT Id, Name, Language__c, Code__c FROM EU_Countries__c WHERE Language__c = 'ENG' ORDER BY Name ASC];
        }
        
        for (Rate_Card__c rc : rateCards)
        {
            if(rccm != null && rccm.size() > 0) {
                // fetch Rate Card Countries Management for current Rate Card
                rccmList = new List<IRateCardCountryManagement>();
                for(Rate_Card_Country_Management__c rcc : rccm) {
                    if(rcc != null && rcc.Rate_Card__c == rc.Id) {
                        rccmList.add(new RateCardCountryManagement(rcc));
                    }
                }
            }
            
            // Preparing drop down list for each rate card
            if(rc != null && (tariffOptionType == 'Included' || tariffOptionType == 'Additional' || tariffOptionType == 'Supplementary Service' || tariffOptionType == 'Support&Service')) {
                availableCountries = new List<SelectOption>();
                
                if(rc.Worldzone__c.contains('Europe')) {
                    if(countriesCustSett != null && countriesCustSett.size() > 0) {
                        for (EU_Countries__c eu : countriesCustSett) {
                            availableCountries.add(new SelectOption(eu.Code__c, eu.Name));
                        }
                    }
                }
                else if(rc.Worldzone__c.contains('World')) {
                    Schema.DescribeFieldResult fieldResult = User.Countrycode.getDescribe();
                    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                    
                    for(Schema.PicklistEntry f : ple) {
                        Boolean isExisting = false;
                        for(EU_Countries__c ctry : countriesCustSett) {
                            if(f.getValue().equalsIgnoreCase(ctry.Code__c)) {
                                isExisting = true;
                            }
                        }
                        
                        if(!isExisting)
                            availableCountries.add(new SelectOption(f.getValue(), f.getLabel()));
                    }
                }
            }
            //
            // Preset target usage fee
            //
            if (rc.Base_Usage_Fee__c == null)
            {
                rc.Base_Usage_Fee__c = 0;
            }
            if (rc.Target_Usage_Fee__c == null)
            {
                rc.Target_Usage_Fee__c = rc.Base_Usage_Fee__c;
            }
            //
            // Create rate card and add it to list
            //
            returnValue.add(new RateCard(rc, rccmList, availableCountries));
        }
        return returnValue;
    }
    

    //--------------------------------------------------------------------------
    // Create list of service fees
    //--------------------------------------------------------------------------
    public static List<IServiceFee> createServiceFees(List<Service_Fee__c> serviceFees)
    {
        List<IServiceFee> returnValue = new List<IServiceFee>();
        for (Service_Fee__c sf : serviceFees)
        {
            //
            // Preset target fees
            //
            if (sf.One_Time_Base_Fee__c == null)
            {
                sf.One_Time_Base_Fee__c = 0;
            }
            if (sf.One_Time_Target_Fee__c == null)
            {
                sf.One_Time_Target_Fee__c = sf.One_Time_Base_Fee__c;
            }
            if (sf.Recurring_Base_Fee__c == null)
            {
                sf.Recurring_Base_Fee__c = 0;
            }
            if (sf.Recurring_Target_Fee__c == null)
            {
                sf.Recurring_Target_Fee__c = sf.Recurring_Base_Fee__c;
            }
            //
            // Create service fee and add it to list
            //
            returnValue.add(new ServiceFee(sf));
        }
        return returnValue;
    }

    //--------------------------------------------------------------------------
    // Create create order request
    //--------------------------------------------------------------------------
    public static CreateOrderRequest createCreateOrderRequest(Id frameContractId)
    {
        return new CreateOrderRequest(frameContractId, new InsertHelper());
    }

    //--------------------------------------------------------------------------
    // Create create order
    //--------------------------------------------------------------------------
    public static CreateOrder createCreateOrder(Id orderRequestId)
    {
        return new CreateOrder(orderRequestId, new InsertHelper());
    }

    //--------------------------------------------------------------------------
    // Create create subscriptions
    //--------------------------------------------------------------------------
    public static CreateSubscriptions createCreateSubscriptions(Id orderId)
    {
        return new CreateSubscriptions(orderId, new InsertHelper());
    }

    //--------------------------------------------------------------------------
    // Create provision in OFI
    //--------------------------------------------------------------------------
    public static ProvisionInOFI createProvisionInOFI(Id billingAccountId)
    {
        return new ProvisionInOFI(billingAccountId, new OFISendEmail());
    }

    //--------------------------------------------------------------------------
    // Create provision in OFI job
    //--------------------------------------------------------------------------
    public static ProvisionInOFIJob createProvisionInOFIJob()
    {
        return new ProvisionInOFIJob(new OFISendEmail());
    }

    //--------------------------------------------------------------------------
    // Create configuration orders
    //--------------------------------------------------------------------------
    public static List<ConfigurationOrder> createConfigurationOrders(Id orderRequest)
    {
        //
        // TODO
        //
        return null;
    }
}