/*
*  Test StockHistoryTrackBatch execution. Test class to provide coverage to one time execution of StockHistoryTrackBatch .
*/
@isTest
private class StockHistoryTrackBatchTest {
    
    @isTest static void testSingleActivation() {
        
        Integer year = Date.today().year();

        Account acc = new Account(
            Name = 'Test account',
            TEF_DE_Sales_Channel__c = 'Test channel',
            Type = 'Customer'
        );
        insert acc;
        List<Stock__c> simList = new List<Stock__c>();

        Stock__c sim1 = new Stock__c(
            Name = '12345678901234567891',
            ICCID__c = '12345678901234567891',
            Account__c = acc.Id,
            Activation_Date__c = Date.newInstance(year, 1, 3)
        );
        simList.add(sim1);
        Stock__c sim2 = new Stock__c(
            Name = '12345678901234567892',
            ICCID__c = '12345678901234567892',
            Account__c = acc.Id,
            Deactivation_Date__c = Date.newInstance(year, 2, 6)
        );
        simList.add(sim2);
        Stock__c sim3 = new Stock__c(
            Name = '12345678901234567893',
            ICCID__c = '12345678901234567893',
            Account__c = acc.Id,
            SM2M_Status__c= 'SUSPENDED',
            State_Change_Date__c = Date.newInstance(year, 5, 3)
        );
        simList.add(sim3);

        insert simList;
        
        Test.startTest();
            Database.executeBatch(new StockHistoryTrackBatch());
        Test.stopTest();

        Stock__c st = [Select Id, HistoryTracking__c from Stock__c where Id=: simList[0].Id limit 1];
        System.assert(st.HistoryTracking__c!=null);
    }
}