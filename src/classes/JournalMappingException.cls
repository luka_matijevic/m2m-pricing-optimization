//------------------------------------------------------------------------------
// This is class for throwing exception when we cannot get journal mapping
//------------------------------------------------------------------------------
public with sharing class JournalMappingException extends Exception
{

}