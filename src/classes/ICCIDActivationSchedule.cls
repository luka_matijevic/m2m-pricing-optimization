global class ICCIDActivationSchedule implements Schedulable {

	global void execute(SchedulableContext sc) {
		checkForSIMActivationOnGeotab();
	}

	@Future(callout=true)
	private static void checkForSIMActivationOnGeotab() {
		List<Case> casesForCheck;
		String httpGetMethod = 'GET';
		String endPoint = 'https://myadminapi.geotab.com/v2/MyAdminApi.ashx/GetInstallLogs?';
		String userInfoEndPoint = 'https://myadminapi.geotab.com/v2/MyAdminApi.ashx/Authenticate?username=';
		Frame_Contract__c fc = null;
		GeotabUtils.AuthenticationResponseClass arc = null;
		Map<String, GeotabUtils.AuthenticationResponseClass> authorizationResultMap = null;
		Map<String, List<GeotabUtils.InstallLogsClass>> installLogsResultMap = null;
		Datetime nowDatetime = datetime.now();
		//Datetime fromDate = Datetime.newInstance(nowDatetime.year(), nowDatetime.month(), nowDatetime.day() - 1, 0, 0, 0);
		//Datetime toDate = Datetime.newInstance(nowDatetime.year(), nowDatetime.month(), nowDatetime.day() + 1, 0, 0, 0);

		Datetime fromDate = Datetime.newInstance(2015, 9, 1, 0, 0, 0);
		Datetime toDate = Datetime.newInstance(2015, 10, 1, 0, 0, 0);

		List<Case> casesReadyForCreatingAnOrder = new List<Case>();
		boolean noTriggerUser = false;
		No_Trigger__c notrigger = null;
		Configurable_Sixt_helper__c csh = null;
		Boolean updateCases = false;

		casesForCheck = [SELECT Id, CaseNumber, Sixt_LVID__c, Sixt_ICCID__c, Sixt_Geotab_device_number__c, Sixt_contract_duration__c, Status, Sixt_Waiting_for_ICCID_activation__c, Sixt_Cable_type__c, Sixt_SIM_activation_date__c
								FROM Case WHERE Sixt_Waiting_for_ICCID_activation__c = true];

		if (casesForCheck != null && casesForCheck.size() > 0)
		{
			List<Configurable_Sixt_helper__c> customConfig = [SELECT Id, Name,  Geotab_username__c, Geotab_password__c, Sixt_frame_contract__c, Sixt_account__c, Sixt_billing_account__c, Sixt_Opportunity_RecordType__c FROM Configurable_Sixt_helper__c WHERE Name = 'Configurable records' ORDER BY CreatedDate ASC];
			if (customConfig != null && customConfig.size() > 0)
			{
				csh = customConfig[0];
			}
			
			if (csh != null)
			{
				userInfoEndPoint = userInfoEndPoint + csh.Geotab_username__c + '&password=' + csh.Geotab_password__c;
			}
			
			HttpResponse resp = CaseUtils.executeRequest(userInfoEndPoint, httpGetMethod, null, 32);
    	
    		if(resp != null && resp.getStatusCode() == 200)
    		{
				authorizationResultMap = (Map<String, GeotabUtils.AuthenticationResponseClass>)JSON.deserialize(resp.getBody(), Map<String, GeotabUtils.AuthenticationResponseClass>.class);

				if (authorizationResultMap != null)
				{
					String userIdResult, sessionIdResult;

					userIdResult = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').userId : null;
					sessionIdResult = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').sessionId : null;

					for (Case cs : casesForCheck)
					{
						endPoint = endPoint + 'apiKey="' + userIdResult + '"&fromDate="' + fromDate.format('YYYY-MM-dd') + '"&sessionId="' + 
								sessionIdResult + '"&toDate="' + toDate.format('YYYY-MM-dd') + '"&assetNumber="' + cs.Sixt_LVID__c + '"';
				
						resp = CaseUtils.executeRequest(endPoint, httpGetMethod, null, 32);
						if (resp != null && resp.getStatusCode() == 200)
						{
							List<No_Trigger__c> noList = [Select Id, SetupOwnerId, Flag__c from No_Trigger__c where SetupOwnerId =: UserInfo.getUserId() limit 1];
							if (noList.isEmpty()) {
        						noTriggerUser=true;
        						notrigger = new No_Trigger__c();
        						notrigger.Flag__c = true;
        						notrigger.SetupOwnerId = UserInfo.getUserId();
        						upsert notrigger;
							} else {
        						noList[0].Flag__c = true;
        						upsert noList[0];
        						notrigger = noList[0];
							}

							System.debug(LoggingLevel.DEBUG, + 'result is' + resp.getBody());

							//result = resp.getBody();
							installLogsResultMap = (Map<String, List<GeotabUtils.InstallLogsClass>>)JSON.deserialize(resp.getBody(), Map<String, List<GeotabUtils.InstallLogsClass>>.class);
							if (installLogsResultMap != null)
							{
								List<GeotabUtils.InstallLogsClass> installLogList = installLogsResultMap.get('result');
								for(GeotabUtils.InstallLogsClass il : installLogList) {
									if (il != null)
									{
										//if (il.simActive)
										//{
										cs.Sixt_ICCID__c = il.simNumber;
										cs.Sixt_Geotab_device_number__c = il.request.device.serialNumber;
										cs.Sixt_Waiting_for_ICCID_activation__c = false;
										cs.Sixt_SIM_activation_date__c = il.resultDateUtc;
										cs.Status = 'Closed and billed';
										updateCases = true;
										casesReadyForCreatingAnOrder.add(cs);
										//}
									}
								}
							}							
						}
					}
				}

				if (updateCases)
					update casesForCheck;

				if (casesReadyForCreatingAnOrder.size() > 0)
				{
					if (csh != null)
						fc = [SELECT Id, Opportunity__c FROM Frame_Contract__c WHERE Id = :csh.Sixt_frame_contract__c];

					if (fc != null)
					{
						for (Case cs : casesReadyForCreatingAnOrder)
						{
							// Opportunity
							/* ************************************************************************************************************* */
							Opportunity newOpp = new Opportunity();
							newOpp.Name = 'Sixt Leasing AG - Order for ' + cs.CaseNumber + ' ticket';
							newOpp.AccountId = csh.Sixt_account__c;
							newOpp.Type = 'Existing Business';
							newOpp.RecordTypeId = csh.Sixt_Opportunity_RecordType__c;
							newOpp.Product_Family__c = 'Solutions';
							newOpp.CloseDate = Date.today();
							newOpp.StageName = 'Prospecting';
							newOpp.Probability = 50;
							newOpp.Estimated_Success_Probability__c = 100;
							newOpp.LeadSource = 'Other';
							insert newOpp;
							/* ************************************************************************************************************* */

							List<Contact> contactsRelatedToAccount = [SELECT Id, AccountId FROM Contact WHERE AccountId = :csh.Sixt_account__c];

							if (contactsRelatedToAccount != null && contactsRelatedToAccount.size() > 0)
							{
								OpportunityContactRole ocr = new OpportunityContactRole(ContactId = contactsRelatedToAccount[0].Id, OpportunityId = newOpp.Id);
								insert ocr;
							}

							fc.Opportunity__c = newOpp.Id;
							update fc;
						
							String BusinessModelDescription = cs.Sixt_LVID__c;

							// Order request
							Order_Request__c orderRequest = new Order_Request__c(Frame_Contract__c = csh.Sixt_frame_contract__c);
							orderRequest.Account__c = csh.Sixt_account__c;
							orderRequest.Opportunity__c = newOpp.Id;
							orderRequest.Billing_Account__c = csh.Sixt_billing_account__c;
							orderRequest.Business_Model_Description__c = BusinessModelDescription;
							orderRequest.Status__c = 'Sent to Boni';
							orderRequest.Ticket__c = cs.id; // Assigining Case (Ticket) to the order Request - Pavan
							List<Boni_Check__c> accountBoniChecks = [SELECT Id, CreatedDate FROM Boni_Check__c WHERE Account__c = :csh.Sixt_account__c ORDER BY CreatedDate DESC];
							if (accountBoniChecks != null && accountBoniChecks.size() > 0)
							{
								orderRequest.Boni_Check__c = accountBoniChecks[0].Id;
							}

							insert orderRequest;
							/* ************************************************************************************************************* */

							// Product Order
							Product_Order__c productOrder = new Product_Order__c();
							productOrder.Comment__c = BusinessModelDescription;
							productOrder.Order_Request__c = orderRequest.Id;
							productOrder.Quantity__c = 1;
				
							upsert productOrder;
							/* ************************************************************************************************************* */

							String queryOffer = 'SELECT Id, Name FROM cscfga__Configuration_Offer__c WHERE Hardware_code__c = \'' + cs.Sixt_Cable_type__c + '\' AND Name like \'%' + cs.Sixt_Contract_duration__c + '\'';

							List<cscfga__Configuration_Offer__c> standardProduct = Database.query(queryOffer);
							if (standardProduct != null && standardProduct.size() > 0)
							{
								List<cscfga__Product_Configuration__c> productConfigurations = [SELECT Id, Name FROM cscfga__Product_Configuration__c WHERE cscfga__Configuration_Offer__c = :standardProduct[0].Id];

								List<cscfga.ProductConfigurationHelper.ConfigurationCopyRequest> lista = new List<cscfga.ProductConfigurationHelper.ConfigurationCopyRequest>();
								for (cscfga__Product_Configuration__c pc : productConfigurations)
								{
									lista.add(new cscfga.ProductConfigurationHelper.ConfigurationCopyRequest(pc.Id, 1));
								}
    
								cscfga__Product_Bundle__c productBundle = new cscfga__Product_Bundle__c();
								productBundle.cscfga__Opportunity__c = newOpp.Id;
								productBundle.cscfga__Bundle_Status__c = 'Valid';
								insert productBundle;

								cscfga__Product_Basket__c productBasket = new cscfga__Product_Basket__c();
								productBasket.cscfga__Opportunity__c = newOpp.Id;
								insert productBasket;

								List<Id> configs = cscfga.ProductConfigurationHelper.copyConfigurations(lista, productBundle.Id, productBasket.Id);
							
								List<cscfga__Product_Configuration__c> productConfigurationsCopy = [SELECT Id, Name, cscfga__Contract_Term__c 
																			FROM cscfga__Product_Configuration__c WHERE Id IN :configs];

								if (productConfigurationsCopy != null)
								{
									for (cscfga__Product_Configuration__c pcCopy : productConfigurationsCopy)
									{
										pcCopy.cscfga__Contract_Term__c = Integer.valueOf(cs.Sixt_Contract_duration__c);
									}

									update productConfigurationsCopy;

									for (cscfga__Product_Configuration__c pcCopy : productConfigurationsCopy)
									{
										Product_Configuration_Order__c productConfigurationOrder = new Product_Configuration_Order__c();				
										productConfigurationOrder.Product_Configuration__c = pcCopy.Id;
										productConfigurationOrder.Product_Order__c = productOrder.Id;
										upsert productConfigurationOrder;
									}
								}

								productBundle.cscfga__Synchronised_with_Opportunity__c = true;
								update productBundle;
							}

							// Create order
							Id orderId = Factory.createCreateOrder(orderRequest.Id).create();
							// Create subscriptions
							Factory.createCreateSubscriptions(orderId).create();
							/* ************************************************************************************************************* */
						}
					}
				}
			}
		}

		if (noTriggerUser) {
    		delete notrigger;
    	}
	}
}