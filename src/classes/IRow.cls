public interface IRow
{
	List<String> getCells();
}