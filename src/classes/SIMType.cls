public class SIMType {
    public String productConfigurationId;
    public String productBundleId;
    public String articleInfo;
    public integer Quantity;
    public boolean isSelected {get; set;}
    public List<SelectOption> SimItems {get; set;}
    
    public SIMType(){
        SimItems = new list<selectOption>();
    }
    
    public String getProductConfigurationId() {
        return this.productConfigurationId;
    }
    
    public void setProductConfigurationId(String productConfigurationId) {
        this.productConfigurationId=productConfigurationId;
    }
    
    public String getProductBundleId() {
        return this.productBundleId;
    }
    
    public void setProductBundleId(String productBundleId) {
        this.productBundleId=productBundleId;
    }
    
    public String getArticleInfo() {
        return this.articleInfo;
    }
    
    public void setArticleInfo(String articleInfo) {
        this.articleInfo=articleInfo;
    }
    
    public Integer getQuantity() {
        return quantity;
    }
    
     public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}