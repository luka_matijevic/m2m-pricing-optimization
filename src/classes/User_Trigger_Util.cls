public class User_Trigger_Util {
    
    public static list<GroupMember> assignUsersToGorup(string profileId,string groupId,set<id> existingUsersInGroup){
        List<GroupMember> groupMems = New list<GroupMember>();
        set<id> usersNeedsAddToGroup = new set<id>(); 
        for(user usr : (list<User>) trigger.new){
            if(trigger.isInsert &&  usr.profileId == profileId){
                usersNeedsAddToGroup.add(usr.id);
            }
            if(trigger.isUpdate){
                user u = (User)trigger.oldMap.get(usr.id);
                if(usr.profileId == profileId && !existingUsersInGroup.contains(usr.id) && u.isActive){
                    usersNeedsAddToGroup.add(usr.id);
                }
            }
        }
        for(id i : usersNeedsAddToGroup){
            groupMems.add(new GroupMember(GroupId = groupId,UserOrGroupId =i));   
        }        
        return groupMems;
    }

}