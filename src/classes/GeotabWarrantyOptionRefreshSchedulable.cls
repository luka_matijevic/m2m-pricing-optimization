global class GeotabWarrantyOptionRefreshSchedulable implements Schedulable {
	global void execute(SchedulableContext sc) {
		GEOTAB_Settings__c geotabSettings = GEOTAB_Settings__c.getInstance();
		String getWarrantyOptionsEndPoint = geotabSettings.GetWarrantyOptionsEndPoint__c;
		String userInfoEndPoint = geotabSettings.UserInfoEndPoint__c;
		Map<String, GeotabUtils.AuthenticationResponseClass> authorizationResultMap = null;
		Map<String, List<GeotabUtils.GeotabWarrantyOption>> geotabWarrantyOptionResultMap = null;
		List<Configurable_Sixt_helper__c> customConfig = [SELECT Id, Name,  Geotab_username__c, Geotab_password__c, Sixt_frame_contract__c, Sixt_account__c, Sixt_billing_account__c, Sixt_Opportunity_RecordType__c FROM Configurable_Sixt_helper__c WHERE Name = 'Configurable records' ORDER BY CreatedDate ASC];
		List<GeotabUtils.GeotabAccount> geotabAccounts = null;
		Configurable_Sixt_helper__c csh = null;

		if (customConfig != null && customConfig.size() > 0)
		{
			csh = customConfig[0];
		}
		
		if (csh != null)
		{
			userInfoEndPoint = userInfoEndPoint + csh.Geotab_username__c + '&password=' + csh.Geotab_password__c;
		}

		HttpResponse resp = CaseUtils.executeRequest(userInfoEndPoint, 'GET', null, 32);

		if(resp != null && resp.getStatusCode() == 200) {
			authorizationResultMap = (Map<String, GeotabUtils.AuthenticationResponseClass>)JSON.deserialize(resp.getBody(), Map<String, GeotabUtils.AuthenticationResponseClass>.class);
			
			if (authorizationResultMap != null) {
				String userIdResult, sessionIdResult;

				userIdResult = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').userId : null;
				sessionIdResult = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').sessionId : null;
				geotabAccounts = authorizationResultMap.get('result') != null ? authorizationResultMap.get('result').accounts : null;
				//System.debug(LoggingLevel.INFO, 'Geotab Accounts API ->' + geotabAccounts);
				if(geotabAccounts != null && geotabAccounts.size() > 0) {
					getWarrantyOptionsEndPoint = getWarrantyOptionsEndPoint + 'apiKey="' + userIdResult + '"&sessionId="' + 
						sessionIdResult + '"';

					resp = CaseUtils.executeRequest(getWarrantyOptionsEndPoint, 'GET', null, 32);

					if(resp != null && resp.getStatusCode() == 200) {
						geotabWarrantyOptionResultMap = (Map<String, List<GeotabUtils.GeotabWarrantyOption>>)JSON.deserialize(resp.getBody(), Map<String, List<GeotabUtils.GeotabWarrantyOption>>.class);
						List<GeotabUtils.GeotabWarrantyOption> geotabWarrantyApi = geotabWarrantyOptionResultMap.get('result');
						Database.executeBatch(new GeotabWarrantyOptionRefreshBatch(geotabWarrantyApi));
					}
				}
			}
		}
	}
}