global with sharing class RetrieveSubscriptionGroupsSchedule  implements Schedulable {
	// Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
	public static final String CRON_EXP = '0 0 16 * * ? *';

	global static String scheduleIt() {
		return System.schedule('Daily Subscription Groups Update', CRON_EXP, new RetrieveSubscriptionGroupsSchedule());
	}
	
	global void execute(SchedulableContext sc)	{
		Database.executeBatch(new RetrieveSubscriptionGroupsBatch(), 1);
	}
}