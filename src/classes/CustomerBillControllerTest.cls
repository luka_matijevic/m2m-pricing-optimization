@isTest
public class CustomerBillControllerTest {
    public static testMethod void controllerTestMethod() {
        Daily_Bill__c dailyB = new Daily_Bill__c (Date__c = Date.newInstance(2016, 10, 3),
                                                 All_Prebills_Requested__c = true,
                                                 Real_Daily_Bill__c = true,
                                                 Prebill_Retrieval_Complete__c = true);
        
        insert dailyB;
        
        Account account = new Account(
        	Name = 'Test3',
            Type = 'Business'
        	);
        
        account.Billing_Cycle__c = '2';
        account.VAT__c = 19;
        account.BillingCity = 'Troisdorf';
        account.BillingCountry = 'Germany';
        account.BillingPostalCode = '53844';
        account.BillingStreet = 'Junkersring 57';
        account.Global__c = true;
        insert account;
        
        Billing_Account__c bac = new Billing_Account__c(Payer__c = account.Id, Name = 'Test Billing Acc', SM2M_Type__c = 'Global', VAT__c = '19', Activation_token__c = 'NotNull');
        insert bac;
        
        Prebill__c preb = new Prebill__c (Name = 'Prebill for ' + Date.newInstance(2016, 10, 3),
                                          Billing_Rate_Model__c = 'One Off',
                                          Invoice_number__c = '0123456789',
                                          Transaction_Date__c = Date.newInstance(2016, 10, 3),
                                         Currency_Code__c = 'EUR',
                                         Daily_Bill__c = dailyB.Id,
                                         Billing_Account__c = bac.Id);        
        
        insert preb;
        
        Prebill_Line_Item__c pbli = new Prebill_Line_Item__c(
			Name = 'Test item',
			Prebill__c = preb.Id,
			Line_Item_Quantity__c = 1,
			Line_Item_Amount__c = 2
		);
        
		insert pbli;
        
        Daily_Bill_Item__c c = new Daily_Bill_Item__c();
        c.Art__c=32;
        c.Steuersatz__c=19;
        c.Daily_Bill__c = dailyB.id;
        c.menge__c=1;    
        c.Einzel_Nettobetrag__c = 100;
        c.Rechnungsnummer__c = '0123456789';
        c.VO_Nr__c = '19';
        c.Anweisung__c = 'test';
        insert c;
        
        test.startTest();
        PageReference pageRefr = Page.CustomerBill;
        pageRefr.getParameters().put('preb', preb.Invoice_number__c);
        Test.setCurrentPageReference(pageRefr); 
        CustomerBillController ctrl = new CustomerBillController ();
        CustomerBillController.PDFBill pdfBill = new CustomerBillController.PDFBill();
        Decimal dcm = pdfBill.PrebillNettoBetrag;
        dcm = pdfBill.PrebillTaxBetrag;
        dcm = pdfBill.PrebillBruttoBetrag;
        ctrl.init();
        test.stopTest();
    }
}