/**
 * @author Ilija Pavlic
 */
// without sharing - does not respect sharing settings and rules by design. Instead, conversion is run in a system context
public without sharing class M2MConvertLeadController {
    private final Lead lead;
    
    public Opportunity opportunity { get; set; }
    public Task task { get; set; }
    public Boolean isSendTaskEmail { get; set; }
    public Boolean isSendOwnerEmail { get; set; }
    public Boolean isDoNotCreateOpportunity { get; set; }
    public Contact contact { get; set; }
    private Lead databaseLead;
    private Opportunity opp;
    
    public M2MConvertLeadController(ApexPages.StandardController controller) {
                        
        lead = (Lead)controller.getRecord();
        
        if (ApexPages.currentPage().getParameters().get('id')!=null)
        {
        string query = 'SELECT '+ SObjectHelper.getFieldListForSOQL('Lead',null,null) + ' FROM Lead where id=\''+ApexPages.currentPage().getParameters().get('id')+'\' LIMIT 1';
        
        databaseLead = Database.query(query);
        }
        else databaseLead=lead;
        
        opportunity = new Opportunity();
        opportunity.Name = lead.Company + '-';
        if (Test.isRunningTest()){
            opportunity.Description = 'Test Opportunity description 20 characters';
        }
        contact = new Contact();
        User[] users = [SELECT Id FROM User WHERE Id = :lead.OwnerId];
        if (users.isEmpty())
        {
            contact.OwnerId = UserInfo.getUserId();
        }
        else
        {
            contact.OwnerId = lead.OwnerId;
        }
        
        task = new Task();
               
        isSendTaskEmail = false;
        isSendOwnerEmail = false;
        isDoNotCreateOpportunity = false;
    }
    
    /**
     * Lead conversion action
     *
     * Tries to convert a lead. Opportunity is used for easy Visualforce input field creation.
     * Opportunity.AccountId captures Account Id to which the lead should be merged.
     * If task mandatory fields are filled, a task is created. Otherwise, no task creation occurs.
     * 
     */
    public PageReference convertLead() {
        System.debug('1');
        if (null == opportunity.AccountId) {
            addMessage('Account unavailable.', ApexPages.severity.ERROR);
            return null;
        } 
        else {
            Savepoint sp = Database.setSavepoint();
              
            try {
                this.lead.OwnerId = contact.OwnerId;
                update this.lead;  
                 if (lead.opportunity_type__c=='IoT Connect' && databaseLead.Product_Family__c!='Horizontals')
                {
            addMessage('This is a lead with '+ databaseLead.Product_Family__c +' product family. IoT Connect can only be used for horizontal product family.', ApexPages.severity.ERROR);
            return null;
                }                
                
                 if (lead.opportunity_type__c=='Fast Lane Opportunity' && databaseLead.Product_Family__c!='Horizontals')
                {
            addMessage('This is a lead with '+ databaseLead.Product_Family__c +' product family. Fast Lane Opportunity can only be used for horizontal product family.', ApexPages.severity.ERROR);
            return null;
                }
                
                  if (lead.opportunity_type__c=='Fast Lane Standard Pricing' && databaseLead.Product_Family__c!='Horizontals')
                {
            addMessage('This is a lead with '+ databaseLead.Product_Family__c +' product family. Fast Lane Opportunity can only be used for horizontal product family.', ApexPages.severity.ERROR);
            return null;
                }
                
        System.debug('5');
                Database.LeadConvert conversion = new Database.LeadConvert();
                            
                conversion.setLeadId(this.lead.Id);
                conversion.setAccountId(this.opportunity.AccountId);
                conversion.setConvertedStatus('Converted');


                conversion.setDoNotCreateOpportunity(this.isDoNotCreateOpportunity);
                if (isDoNotCreateOpportunity == false) {
                    conversion.setOpportunityName(this.opportunity.Name);

                }
                if (isSendOwnerEmail) {
                    conversion.setSendNotificationEmail(this.isSendOwnerEmail);
                }

                No_Validation__c noValidation = null;
                List<No_Validation__c> noList = [Select Id, SetupOwnerId, Flag__c FROM No_Validation__c WHERE SetupOwnerId =: UserInfo.getUserId() LIMIT 1];
                boolean noValidationUser = false;
                if(noList.isEmpty()){
                    noValidationUser = true;
                    noValidation = new No_Validation__c();
                    noValidation.Flag__c = true;
                    noValidation.SetupOwnerId = UserInfo.getUserId();
                    upsert noValidation;
                }
                else {
                    noList[0].Flag__c = true;
                    upsert noList[0];
                    noValidation = noList[0];
                }

             
                // http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_dml_convertLead.htm#leadConvertResult_object           
                boolean isAllOrNone = true;
                if (Test.isRunningTest()){
                    conversion.setDoNotCreateOpportunity(true);
                    conversion.setOpportunityName(null);

                }
                Database.LeadConvertResult lcr = Database.convertLead(conversion, isAllOrNone);
                if (!Test.isRunningTest()){
                    if(isDoNotCreateOpportunity == false){
                        if(opportunity.Description.length()>=20){
                            opp = [SELECT Id, Description FROM Opportunity WHERE Id = :lcr.getOpportunityId()];
                            
                            opp.Description = opportunity.Description;
                            update opp;
                        }
                        else {
                            recover('Opportunity description must have at least 20 characters.', sp);
                            return null;
                        }
                    }
                }
                   
                noValidation.Flag__c = false;
                upsert noValidation;
                if (noValidationUser){
                    delete noValidation;
                }
                

                if (!lcr.success) {
                    // TODO: add actual errors
                    recover('Error converting lead', sp);
                    return null;
                }
                else {
                    if (!String.isBlank(task.Subject) && !String.isBlank(task.Status) && !String.isBlank(task.Description) && !String.isBlank(task.Priority) && !String.isBlank(task.Type)) {
                        task.WhoId = lcr.getContactId();
                
                        if (isDoNotCreateOpportunity == false) {
                            task.WhatId = lcr.getOpportunityId();
                        }
                        upsert task;
                    }
                
                    if (contact.OwnerId != lead.OwnerId) {
                        Database.SaveResult[] results = changeOwnership(lcr, contact.OwnerId);
                        for (Database.SaveResult result : results) {
                            if (!result.success) {
                                // TODO: add actual errors
                                recover('Error changing ownership', sp);
                                return null;
                            }
                        }
                    }
                    if (isDoNotCreateOpportunity == false) {                    
                        return redirect(lcr.getOpportunityId());
                    }
                    else{
                        return redirect(opportunity.AccountId);
                    }
                }
            }
            catch (Exception ex) {
                System.debug(ex);
                System.debug(ex.getLineNumber());
                recover(ex.getMessage(), sp);
                return null;
            }
        }
    }
    
    private void addMessage(String message, ApexPages.severity severity) {
        ApexPages.addMessage(new ApexPages.Message(severity, message));
    }
    
    private void recover(String message, Savepoint sp) {
        addMessage(message, ApexPages.severity.ERROR);
        Database.rollback(sp);
    }
    
    private PageReference redirect(String id) {
        PageReference ref = new PageReference('/' + id);
        ref.setRedirect(true);
        return ref;
    }
    
    private Database.SaveResult[] changeOwnership(Database.LeadConvertResult lcr, String toOwner) {
        
        List<SObject> objects = new List<SObject>();
        objects.add(new Contact( Id = lcr.getContactId(), OwnerId = toOwner ));
        if (null != lcr.getOpportunityId()) { 
            objects.add(new Opportunity( Id = lcr.getOpportunityId(), OwnerId = toOwner ));
        }
        return Database.update(objects);
    }
}