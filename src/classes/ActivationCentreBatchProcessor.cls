global class ActivationCentreBatchProcessor implements Database.Batchable<Integer>, Database.Stateful, Database.AllowsCallouts {
	/********************************

	**************************************/
	String query;
	List<simData> simDataList;
	Map<Id, Third_Upload_File__c> tufMap;
	Activation_Centre__c activationCentre;
	Set<String> allICCIDs;
	Set<Id> tufWithStock = new Set<Id>();

	global ActivationCentreBatchProcessor(List<simData> simDataListFullIn, Map<Id, Third_Upload_File__c> tufMapIn, 
		Activation_Centre__c activationCentreIn, Set<String> allICCIDsIn) {
		simDataList = simDataListFullIn;
		tufMap = tufMapIn;
		activationCentre = activationCentreIn;
		allICCIDs = allICCIDsIn;
	}
	
	global List<Integer> start(Database.BatchableContext BC) {
		List<Integer> dummyList = new List<Integer>();
		dummyList.add(1);
		return dummyList;
		//return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Integer> scope) {
   		boolean hasError = false;
   		if (simDataList != null){
   			Map<String, Id> newStockMap = createMissingStocks();
   			setStockFields(newStockMap);
   			//removeTUFConnections();
   			//addStocksToTUF();

   			if (!hasError){
				for (Id key : tufMap.keySet()){
					try{
						String createTUFResult = createTUF(tufMap.get(key).Id);
						if (!createTUFResult.equals('Third Upload File CSV was successfully generated')){
							
							break;
						}
					}
					catch (Exception e){
						throw e;
						break;
					}
				}	
			}

			if (!hasError){
				activationCentre.Status__c = 'TUF Created';
				update activationCentre;
			}			
			if (activationCentre.Status__c.equals('TUF Created')){
				for (Id key : tufMap.keySet()){	
					if (tufWithStock.contains(key)){			
						uploadTUF(tufMap.get(key).Id);
					}
				}
					activationCentre.Status__c = 'TUF Upload Started';
					update activationCentre;				
				}
			}//END NEW OR UPLOAD ERROR

			if (activationCentre.Status__c.equalsIgnoreCase('PUBLISH ERROR')){
				activationCentre.Status__c = 'TUF Upload Success';
				update activationCentre;
			}//END PUBLISH ERROR

	}
	
	global void finish(Database.BatchableContext BC) {
		
	}

	private Map<String, Id> createMissingStocks(){
		List<Stock__c> newStockList = new List<Stock__c>();
		Map<String, SRF__C> srfMap = new Map<String, SRF__c>();


		for (simData sim : simDataList){
			if (sim.exists == false && !srfMap.containsKey(sim.articleInfoSetId)){
				SRF__c srf = new SRF__c();
				srf.Account__c = activationCentre.M2M_Order_Request__r.Billing_Account__r.Payer__c;
				srf.SM2M_SAP_PO__c = '0'; //dummy
				srf.Billing_Account__c = activationCentre.M2M_Order_Request__r.Billing_Account__c;
				srf.Order_Request__c = activationCentre.M2M_Order_Request__c;
				srf.SIM_Type__c = sim.articleInfoSetId;
				srf.Name = 'Automatic SRF for: ' +activationCentre.Name;
				srf.RecordTypeId = Cache.RecordTypeId('SRF__c.Activation Centre SRF');
				insert srf;
				srfMap.put(sim.articleInfoSetId, srf);
			}
		}
		
		for (simData sim : simDataList){
			if (sim.exists == false){
				System.debug(sim);
				Stock__c s = new Stock__c();
				s.ICCID__c = sim.iccid;
				s.Status__c = 'Reserved';
				s.Article_Info_Set__c = sim.articleInfoSetId;
				s.SRF__c = srfMap.get(sim.articleInfoSetId).id;
				s.SM2M_Status__c = sim.LifeCycleStatus;
				s.State_Change_Date__c = DateTime.now();
				s.Third_Upload_File__c = tufMap.get(sim.articleInfoSetId).id;
				s.subscription_group__C = sim.SubscriptionGroup;
				tufWithStock.add(sim.articleInfoSetId);
				//TODO add other data
				if (tufMap.get(sim.articleInfoSetId).Type__c == 'Local'){
					s.msisdn__c = sim.msisdn;
				}
				newStockList.add(s);
			}
		}
		System.debug(newStockList);
		insert newStockList;

		Map<String, Id> newStockMap = new Map<String, Id>();
		for (Stock__c stock : newStockList){
			newStockMap.put(stock.ICCID__C, stock.id);
		}
		
		for (simData sim : simDataList){
			if (sim.exists!=null && !sim.exists){
				sim.stockId = newStockMap.get(sim.ICCID);
				sim.exists = true;
			}			
		}
		return newStockMap;
	}

	private void setStockFields(Map<String, Id> newStockMap ){
		List<Stock__C> stockList = [select id, ICCID__C, sm2m_status__c, State_Change_Date__c , subscription_group__c, HistoryTracking__c from stock__c where iccid__c in :allICCIDs and iccid__c not in :newStockMap.keySet()];
		system.debug('setStockFields 1: ' + stockList);
		system.debug('simDataList : ' +simDataList);
		Map<String, Stock__c> stockMap = new Map<String, Stock__C>();		
		Set<Id> errorTufIds = new Set<Id>();
		Set<String> iccidSet;
		Set<String> APNSet;
				
		for (Stock__c stock : stockList){
			stockMap.put(stock.ICCID__c, stock);
		}
		for (simData sim : simDataList){
		system.debug(sim);
			if (stockMap.containsKey(sim.iccid)){
			stockMap.get(sim.iccid).subscription_group__C = sim.SubscriptionGroup;
			stockMap.get(sim.iccid).Article_Info_Set__c = sim.articleInfoSetId;
			tufWithStock.add(sim.articleInfoSetId);
			if(stockMap.get(sim.iccid).SM2M_Status__c==null || !stockMap.get(sim.iccid).SM2M_Status__c.equals(sim.LifeCycleStatus)){
				stockMap.get(sim.iccid).SM2M_Status__c = sim.LifeCycleStatus;
				stockMap.get(sim.iccid).State_Change_Date__c = DateTime.now();
				//History tracking
					if (null!=sim.LifeCycleStatus && (sim.LifeCycleStatus.equalsIgnoreCase('ACTIVE') || 
						sim.LifeCycleStatus.equalsIgnoreCase('SUSPENDED') || 
						sim.LifeCycleStatus.equalsIgnoreCase('DEACTIVATED') ||
						sim.LifeCycleStatus.equalsIgnoreCase('RETIRED'))){
							if (stockMap.get(sim.iccid).HistoryTracking__c == null) stockMap.get(sim.iccid).HistoryTracking__c ='';
							stockMap.get(sim.iccid).HistoryTracking__c = sim.LifeCycleStatus + '|' + stockMap.get(sim.iccid).State_Change_Date__c;
						}
						
			}
			//LM 16,02,2017
			
			errorTufIds.add(stockMap.get(sim.iccid).Third_Upload_File__c);
			
			stockMap.get(sim.iccid).Third_Upload_File__c = tufMap.get(sim.articleInfoSetId).Id;
	        stockMap.get(sim.iccid).Status__c = 'Reserved';
		}
		}

				
					
					


		stockList.clear();
		stockList = stockMap.values();
		system.debug('setStockFields 2: ' +stockList);
		//turn off triggers
		No_Trigger__c notrigger = null;
    	boolean noTriggerUser = false;
		// Set trigger to inactive
		List<No_Trigger__c> noList = [Select Id, SetupOwnerId, Flag__c from No_Trigger__c where SetupOwnerId =: UserInfo.getUserId() limit 1];
		if (noList.isEmpty()) {
			noTriggerUser=true;
			notrigger = new No_Trigger__c();
			notrigger.Flag__c = true;
			notrigger.SetupOwnerId = UserInfo.getUserId();
			upsert notrigger;
		} else {
			noList[0].Flag__c = true;
			upsert noList[0];
			notrigger = noList[0];
		}
		update stockList;
		List<Attachment> attach = [select id from Attachment where parentId in :errorTufIds and Name = 'ThirdUploadFile.csv'];
		delete attach;
				
		//turn on triggers
		notrigger.Flag__c = false;
		upsert notrigger; 
		if (noTriggerUser) {
			delete notrigger;
		}
	}

	private void removeTUFConnections(){
		//remove tuf-stock
		//remove tuf-apn
		//delete tuf.csv attachment
		List<Id> tufIds = new List<Id>();
		Set<Id> errorTufIds = new Set<Id>();
		for (Third_Upload_File__c tuf : tufMap.values()){
			tufIds.add(tuf.id);
		}
		system.debug('tufIds : ' + tufIds);
		List<Stock__c> stockList = [select id, Third_Upload_File__c from Stock__c where Third_Upload_File__c in :tufIds and Third_Upload_File__r.Upload_to_SM2M_Status__c != 'Uploaded'];
		system.debug('stockList :'+stockList);
		for (Stock__c stock : stockList){
			errorTufIds.add(stock.Third_Upload_File__c);
			stock.Third_Upload_File__c = null;
			stock.status__C = 'On stock';
			
		}

		system.debug('errorTufIds : ' +  errorTufIds);
		/*
		List<APN_TUF__c> apnTufList = [select id from APN_TUF__c where TUF__c in :tufIds and TUF__r.Upload_to_SM2M_Status__c != 'Uploaded'];
		delete apnTufList;
		*/
		update stockList;
		List<Attachment> attach = [select id from Attachment where parentId in :errorTufIds and Name = 'ThirdUploadFile.csv'];
		delete attach;
	}

	public boolean addStocksToTUF(){
		try { 
				
				Set<String> iccidSet;
				Set<String> APNSet;
				for (Id key : tufMap.keySet()){
					APNSet = new Set<String>();
					iccidSet = new Set<String>();
					for (simData sim : simDataList){
						if (sim.articleInfoSetId == key){
						iccidSet.add(sim.iccid);					
						APNSet.add(sim.apn);
						}
					}
					
					AssignStockToThirdFileBatch.assignStockToThirdFile(iccidSet, tufMap.get(key).Id, tufMap.get(key).Type__c);
				}
				/*TODO set activation centre status
				activationCentre.Status__c = 'TUF created';
				update activationCentre;
				*/
				clearMessages();
				return false;

	        }
		    catch(Exception ex){
        		//Database.rollback(sp);
				clearMessages();
				String errorMessage = ex.getMessage();
				for (simData sim : simDataList){
					if (errorMessage.contains(sim.iccid)) 
						sim.message = errorMessage.contains('check stock status') ? 
							'Check stock status (should be \'On stock\') and account defined in the sim range (has to be the same as TUF account)!' :
							'Check TUF and Stock types. Global TUF can have only Global Stocks. Local TUF can have Blue or Green Stocks.';
				throw ex;
				return true;
				}
			}finally{
				//TODO do I need this part?
				/*
				attach.body = null;
        		attach = new Attachment();
        		System.debug(LoggingLevel.ERROR, 'Heap Size in controller finally block : ' + Limits.getHeapSize() + '/' + Limits.getLimitHeapSize());
				*/
			}
			return null;
	}

	private void uploadTUF(Id thirdUploadFileId){
		try{
			List<Attachment> attach = [
				SELECT 
					Id, ParentId, Name, Description, CreatedDate, ContentType, Body, IsDeleted
				FROM 
					Attachment
				WHERE 
					isDeleted = false
				AND 
					ParentId = :thirdUploadFileId
				ORDER BY CreatedDate DESC
				LIMIT 1 
			];
			
			
			csam_t1.ObjectGraphCalloutHandler.AdditionalObjects addObjects = new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects();
    		addObjects.typeName = 'Third_Upload_File__c';
    		addObjects.ids = new Id[]{thirdUploadFileId};
    		
    		List<Third_Upload_File__c> currentTuf = [SELECT Id, SIM_swap_Ticket__c FROM Third_Upload_File__c WHERE Id = :thirdUploadFileId];
    		
    		if(currentTuf != null && currentTuf.size() > 0) {
    		    if(currentTuf[0].SIM_swap_Ticket__c != null) {
    		        List<Stock__c> stocks = [SELECT Id, Status__c, Third_Upload_File__c FROM Stock__c WHERE Third_Upload_File__c =:thirdUploadFileId];
    		
            		if(stocks != null && stocks.size() > 0)
            		{
            		    for(Stock__c stock : stocks) {
            		        stock.Status__c = 'Pending activation';
            		    }
            		    
            		    update stocks;
            		}
    		    }
    		}
    		if (attach.size()>0) 
			String response = csam_t1.ObjectGraphCalloutHandler.queueMessageFromIdExtended('Upload TUF Activation Centre', attach.get(0).id, new csam_t1.ObjectGraphCalloutHandler.AdditionalObjects[]{addObjects});//'Third Upload File CSV was successfully sent';
			
			Third_Upload_File__c tuf = [SELECT Id, Upload_to_SM2M_Status__c FROM Third_Upload_File__c WHERE Id =:thirdUploadFileId];
			tuf.Upload_to_SM2M_Status__c = 'Pending';
			update tuf;
		
		} catch(Exception e){
			throw e;
		}
	}

	private String createTUF(Id thirdUploadFileId){
		try	{
			// generate TUF
			Attachment attach = new Attachment();
			attach.ParentId = thirdUploadFileId;
			attach.Name = 'ThirdUploadFile.csv';
			attach.Body = ThirdUploadFileCSVGenerator.createThirdUploadFileCSV(thirdUploadFileId);
			insert attach;
			return 'Third Upload File CSV was successfully generated';
		}
		catch(exception ex)	{
			return ex.getMessage();
		}
	}

	private void clearMessages(){
		for (simData sim : simDataList){					
			sim.message = '';
		}
	}
}