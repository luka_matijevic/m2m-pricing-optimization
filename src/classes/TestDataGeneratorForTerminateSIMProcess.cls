@isTest
public class TestDataGeneratorForTerminateSIMProcess {

    public static List<Stock__c> createSIMs(Integer count){
        List<Stock__c> simsTobeInserted = new List<Stock__c>();
        Account acc = Test_Util.createAccount('Account Test');
        insert acc;
        
        Opportunity opp = Test_Util.createOpportynity('Opp', acc.id, system.today(), 'Dratf','PRice',1, 1);
        opp.Description = 'Test Description 20 characters';
        insert opp;
        
        Frame_Contract__c fc = new Frame_Contract__c(recordTypeId = Schema.SObjectType.Frame_Contract__c.getRecordTypeInfosByName().get('Trial').getRecordTypeId());
        fc.Customer__c = acc.id;
        fc.Opportunity__c = opp.id;
	    insert fc;
	    
        Order_Request__c ordReq = new Order_Request__c(Frame_Contract__c = fc.id, Runtime__c = 24, Shipping_City__c='Test City', Shipping_Country__c = 'Test Country', Shipping_Postal_Code__c='10000', Shipping_Street__c='Test shipping street');
        insert ordReq;
        
        Third_Upload_File__c tuf = new Third_Upload_File__c(Order_Request__c = ordReq.id, Name='Test TUF');
	    insert tuf;
	    
        for(Integer i = 0; i < count; i++){
            Stock__c stock = new Stock__c(Name = 'Test Stock' + i, Order_Request__c = ordReq.Id, SM2M_Status__c = 'ACTIVE', Third_Upload_File__c = tuf.id, Activation_Date__c = System.today().addMonths(-24), Account__c = acc.Id);
            simsTobeInserted.add(stock);
        }
        if(!simsTobeInserted.isEmpty()){
            insert simsTobeInserted;
        }
        return simsTobeInserted;
    }
    
    public static void terminateSIMs(List<Stock__c> sims){
        List<Stock__c> simsTobeUpdated = new List<Stock__c>();
        for(Stock__c stock : sims){
            stock.Termination_Status__c = 'REQUESTED';
            stock.End_date__c = stock.Potential_Termination_Date__c;
            simsTobeUpdated.add(stock);
        }
        if(!simsTobeUpdated.isEmpty()){
            update simsTobeUpdated;
        }
    }
    
    public static List<Stock__c> createSIMsforTaskCreation(Integer count){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'tuser', Email='testuser@m2m.com', 
            EmailEncodingKey='UTF-8', LastName='user', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testuser@m2m.com.test');
        insert u;
		List<Account> accountsTobeInserted = new List<Account>();
		List<Case> casesTobeInserted = new List<Case>();
		List<Stock__c> simsTobeInserted = new List<Stock__c>();
		System.runAs(u){
		    for(Integer i = 0; i < count; i++){
	            Account acc = new Account(Name = 'Test Account', Type = 'Customer', Value_Added_Tax_ID__c = 'test', VAT__c = 14, Billing_Cycle__c = '1', Currency__c = 'Euro', Company_Email__c = 'abc@test.com', Language__c = 'English', VO_Number__c = '123', SPIN_ID__c = '111');
	            accountsTobeInserted.add(acc);
		    }
		    if(!accountsTobeInserted.isEmpty()){
		        insert accountsTobeInserted;
		    }
		    for(Integer i = 0; i < count; i++){
	            Case c = new Case(recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Standard Case').getRecordTypeId(), Status = 'open', Type = 'Standard', Category__c = 'Service', Origin = 'Web', AccountId = accountsTobeInserted[i].Id, Subject = 'Test Case', Description = 'This is a test case to run test classes');
	            casesTobeInserted.add(c);
		    }
		    if(!casesTobeInserted.isEmpty()){
		        insert casesTobeInserted;
		    }
		}
		Opportunity opp = Test_Util.createOpportynity('Opp', accountsTobeInserted[0].id, system.today(), 'Dratf','PRice',1, 1);
        opp.Description = 'Test Description 20 characters';
        insert opp;
        
		Frame_Contract__c fc = new Frame_Contract__c(recordTypeId = Schema.SObjectType.Frame_Contract__c.getRecordTypeInfosByName().get('Trial').getRecordTypeId());
		fc.Customer__c = accountsTobeInserted[0].id;
        fc.Opportunity__c = opp.id;
	    insert fc;
	    
        Order_Request__c ordReq = new Order_Request__c(Frame_Contract__c = fc.id, Runtime__c = 24, Shipping_City__c='Test City', Shipping_Country__c = 'Test Country', Shipping_Postal_Code__c='10000', Shipping_Street__c='Test shipping street');
        insert ordReq;
        
        Third_Upload_File__c tuf = new Third_Upload_File__c(Order_Request__c = ordReq.id, Name='Test TUF');
	    insert tuf;
	    
		for(Integer i = 0; i < count; i++){
            Stock__c stock = new Stock__c(Name = 'Test Stock' + i, Activation_Date__c = System.today().addDays(3).addMonths(-24), Account__c = accountsTobeInserted[i].Id, Termination_Status__c = 'REQUESTED');
            simsTobeInserted.add(stock);
        }
        if(!simsTobeInserted.isEmpty()){
            insert simsTobeInserted;
        }
        return simsTobeInserted;
    }
    
    public static List<Stock__c> createSIMsforRetireSIMs(Integer count){
        List<Stock__c> simsTobeInserted = new List<Stock__c>();
        Account acc = Test_Util.createAccount('Account Test');
        insert acc;
        
        Opportunity opp = Test_Util.createOpportynity('Opp', acc.id, system.today(), 'Dratf','PRice',1, 1);
        opp.Description = 'Test Description 20 characters';
        insert opp;
        
        Frame_Contract__c fc = new Frame_Contract__c(recordTypeId = Schema.SObjectType.Frame_Contract__c.getRecordTypeInfosByName().get('Trial').getRecordTypeId());
        fc.Customer__c = acc.id;
        fc.Opportunity__c = opp.id;
	    insert fc;
	    
        Order_Request__c ordReq = new Order_Request__c(Frame_Contract__c = fc.id, Runtime__c = 24, Shipping_City__c='Test City', Shipping_Country__c = 'Test Country', Shipping_Postal_Code__c='10000', Shipping_Street__c='Test shipping street');
        insert ordReq;
        
        Third_Upload_File__c tuf = new Third_Upload_File__c(Order_Request__c = ordReq.id, Name='Test TUF');
	    insert tuf;
	    
        for(Integer i = 0; i < count; i++){
            Stock__c stock = new Stock__c(Name = 'Test Stock' + i, Order_Request__c = ordReq.Id, Third_Upload_File__c = tuf.id, Activation_Date__c = System.today().addMonths(-24).addDays(-42), Termination_Status__c = 'TERMINATED');
            simsTobeInserted.add(stock);
        }
        if(!simsTobeInserted.isEmpty()){
            insert simsTobeInserted;
        }
        return simsTobeInserted;
    }
    
    public static List<Stock__c> updateSIMsforTaskCreation(List<Stock__c> sims){
        List<Stock__c> simsTobeUpdated = new List<Stock__c>();
        for(Stock__c stock : sims){
            stock.Activation_Date__c = System.today().addDays(3).addMonths(-24);
            simsTobeUpdated.add(stock);
        }
        if(!simsTobeUpdated.isEmpty()){
            update simsTobeUpdated;
        }
        return simsTobeUpdated;
    }
}