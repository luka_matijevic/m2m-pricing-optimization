public without sharing class CreateOrderRequest
{
	//--------------------------------------------------------------------------
	// Members
	//--------------------------------------------------------------------------
	Id frameContractId;
	IInsertHelper insertHelper;
	List<ProductOrderContainer> productOrders;

	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	public CreateOrderRequest(Id frameContractId, IInsertHelper insertHelper)
	{
		this.frameContractId = frameContractId;
		this.insertHelper = insertHelper;
		productOrders = new List<ProductOrderContainer>();
		Map<String, List<Id>> standardProducts =
				new Map<String, List<Id>>();
		List<cscfga__Product_Configuration__c> nonStandardProducts =
				new List<cscfga__Product_Configuration__c>();
		//
		// Get all product configurations from frame contract product bundle
		//
		Frame_Contract__c frameContract =
				[ SELECT Name, Product_Bundle__c
				  FROM Frame_Contract__c
				  WHERE Id = :frameContractId ];
		cscfga__Product_Configuration__c[] configs =
				[ SELECT Id, Name, Offer_Name__c, parent_product_configuration__c, cscfga__Product_Definition__r.Name, cscfga__Description__c, SMS_Discount__c, Discount__c
				  FROM cscfga__Product_Configuration__c
				  WHERE cscfga__Product_Bundle__c = :frameContract.Product_Bundle__c 
				  	AND Name != 'IoT Connect Advanced Root'];
		for (cscfga__Product_Configuration__c config : configs)
		{
			//
			// Standard product name
			//
			if (config.Offer_Name__c != null && config.parent_product_configuration__c==null)
			{
			    
				Id[] configurations =
						standardProducts.get(config.Offer_Name__c);

				if (configurations == null)
				{
					//
					// Create new configuration list
					//
					configurations = new List<Id>();
					configurations.add(config.Id);
				}
				else
				{
					//
					// Add current configuration
					//
					configurations.add(config.Id);
				}
				string productName=config.Offer_Name__c + ' - '+ config.Name;
				standardProducts.put(productName.left(75) , configurations);
			}
			//
			// Product configuration name
			//
			else
			{
				nonStandardProducts.add(config);
			}
		}
		//
		// Create standard product order list
		//
		System.debug('standardProducts ' + standardProducts);
		for (String productName : standardProducts.keySet())
		{
			ProductOrderContainer productOrder = new ProductOrderContainer();
			productOrder.name = productName;
			productOrder.child = false;
			productOrder.productConfigurations = standardProducts.get(productName);
			productOrders.add(productOrder);
		}


		//
		// Split parent and child configurations
		//
		cscfga__Product_Configuration__c[] parents = (cscfga__Product_Configuration__c[])
			Filter.field('parent_product_configuration__c').equals(null).apply(nonStandardProducts);
		cscfga__Product_Configuration__c[] children = (cscfga__Product_Configuration__c[])
			Filter.field('parent_product_configuration__c').notEquals(null).apply(nonStandardProducts);
			
		System.debug('children ' + children);
		System.debug('parents ' + parents);
		//
		// Create non standard product order list
		//
		for (cscfga__Product_Configuration__c config : parents)
		{
			ProductOrderContainer productOrder = new ProductOrderContainer();
			if(config.cscfga__Product_Definition__r.Name == 'IoT Connect Advanced Option'){
				productOrder.name = config.Name + '('+ config.Discount__c +'%) + SMS Discount('+ config.SMS_Discount__c +'%)';
			}
			else{
				productOrder.name = config.Name;
			}
			productOrder.child = false;
			Id[] configurations = new List<Id>();
			configurations.add(config.Id);
			productOrder.productConfigurations = configurations;
			productOrders.add(productOrder);
		}

		//
		// Add children to product orders
		//
		productOrders = reorderProductOrders(productOrders, children);

	}

	//--------------------------------------------------------------------------
	// Get products for order request
	//--------------------------------------------------------------------------
	public List<ProductOrderContainer> getProductOrders()
	{
		return productOrders;
	}

	//--------------------------------------------------------------------------
	// Create order request with all needed data
	//--------------------------------------------------------------------------
	public Id create(Order_Request__c orderRequest)
	{
		//
		// Create order request attached to frame contract
		//
		Frame_Contract__c frameContract =
				[ SELECT Id, Customer__c, Contact__c, Opportunity__c
				  FROM Frame_Contract__c
				  WHERE Id = :frameContractId ];

		orderRequest.Frame_Contract__c = frameContract.Id;
		orderRequest.Status__c = 'Open';
		insertHelper.add(orderRequest);
		//
		// Create product orders and product configuration orders
		//
		Set<Id> productConfigurationIds = new Set<Id>();
		Set<Id> articleInfoSetIds = new Set<Id>();
		for (ProductOrderContainer productOrder : productOrders)
		{
			if (productOrder.quantity > 0)
			{
				Product_Order__c prodOrder = new Product_Order__c(
						Name = productOrder.name,
						Quantity__c = productOrder.quantity,
						Comment__c = productOrder.comment);
				insertHelper.add(prodOrder, 'Order_Request__c', orderRequest);
				for (Id productConfiguration : productOrder.productConfigurations)
				{
					productConfigurationIds.add(productConfiguration);
					Product_Configuration_Order__c prodConfOrder =
							new Product_Configuration_Order__c(
									Product_Configuration__c = productConfiguration);
					insertHelper.add(prodConfOrder, 'Product_Order__c', prodOrder);
				}
			}
		}
		
		/* start : Assigning Runtime On Order Reuquest*/
		for(cscfga__Product_Configuration__c prodConfig : [select id,cscfga__Contract_Term__c from cscfga__Product_Configuration__c where Id in:productConfigurationIds and cscfga__Contract_Term__c != null limit 1]){
		    orderRequest.RunTime__c = prodConfig.cscfga__Contract_Term__c;
		    break;
		}
		/*  End: Assigning Runtime On Order Reuquest*/
		
		//
		// Create SRFs connected to the order request
		//
		cscfga__Attribute__c[] attributes =
				[ SELECT cscfga__Value__c
				  FROM cscfga__Attribute__c
				  WHERE cscfga__Product_Configuration__c IN :productConfigurationIds
				  AND Name = :Constants.ATTRIBUTE_NAME_ARTICLE_INFO_SET];
		for (cscfga__Attribute__c attribute : attributes)
		{
			articleInfoSetIds.add(attribute.cscfga__Value__c);
		}
		Article_Info_Set__c[] articleInfoSets =
				[ SELECT Id, Name
				  FROM Article_Info_Set__c
				  WHERE Id IN :articleInfoSetIds
				  AND RecordType.Name = 'SIM'
				  AND Article_Type__c = 'SIM' ];
		Id recordTypeId = Cache.getRecordTypeId('SRF__c.Standard_SRF');
		for (Article_Info_Set__c ais : articleInfoSets)
		{
			SRF__c srf = new SRF__c();
			srf.Account__c = frameContract.Customer__c;
			srf.Billing_Account__c = orderRequest.Billing_Account__c;
			srf.Contact__c = frameContract.Contact__c;
			srf.Opportunity__c = frameContract.Opportunity__c;
			srf.SIM_Type__c = ais.Id;
			srf.RecordTypeId = recordTypeId;
			insertHelper.add(srf, 'Order_Request__c', orderRequest);
		}
		//
		// Store everything in the database
		//
		insertHelper.store();

		return orderRequest.Id;
	}

	//--------------------------------------------------------------------------
	// Reorder product orders
	//--------------------------------------------------------------------------
	private static ProductOrderContainer[] reorderProductOrders(
			ProductOrderContainer[] parentProductOrders,
			cscfga__Product_Configuration__c[] children)
	{
		ProductOrderContainer[] returnValue = new List<ProductOrderContainer>();
		for (ProductOrderContainer parent : parentProductOrders)
		{
			returnValue.add(new ProductOrderContainer(parent));
			//
			// Add child product orders
			//
			returnValue.addAll(addChildProducts(parent, children));
		}
		return returnValue;
	}

	//--------------------------------------------------------------------------
	// Add child products to parent
	//--------------------------------------------------------------------------
	private static ProductOrderContainer[] addChildProducts(
			ProductOrderContainer parent,
			cscfga__Product_Configuration__c[] children)
	{
		Set<Id> configIds = new Set<Id>(parent.productConfigurations);
		ProductOrderContainer[] returnValue = new List<ProductOrderContainer>();
		for (cscfga__Product_Configuration__c child : children)
		{
			if (configIds.contains(child.parent_product_configuration__c))
			{
				ProductOrderContainer poc = new ProductOrderContainer();
				
				if (child.cscfga__Product_Definition__r.Name!='Connectivity')
				poc.name = child.Name;
				else {
				    string productName=child.Name + ' - ' + child.cscfga__Description__c;
				    poc.name=productName.left(75);
				}
				poc.child = true;
				Id[] configurations = new List<Id>();
				configurations.add(child.Id);
				poc.productConfigurations = configurations;
				returnValue.add(poc);
			}
		}
		return returnValue;
	}
}