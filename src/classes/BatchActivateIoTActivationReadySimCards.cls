/**
 * @author Petar Matkovic 16.8.2016.
 * Get IoT connect SIM cards which are in status Activation ready for 60 days and activate them
 * Activation is done be setting status to "Pending Activation", then Modify SIM job (SimSwapModifierPoller) collects SIMs and activate them in SM2M
 * This should be done before SIM Update job which sync SIM cards with SM2M System
 */ 
 global class BatchActivateIoTActivationReadySimCards implements Database.Batchable<sObject> {
	global Date dateForSims;
	private String fcNumber = 'FC - 4991';

	global BatchActivateIoTActivationReadySimCards(Date dateForQuery) {
		dateForSims = dateForQuery;
	}

	global BatchActivateIoTActivationReadySimCards(Date dateForQuery, String frameContractName){
		dateForSims = dateForQuery;
		fcNumber = frameContractName;
	}

	global BatchActivateIoTActivationReadySimCards(){
		dateForSims = System.today();
		dateForSims = dateForSims.addDays(-60);
	}
	
	//Get SIM Cards
	global List<Stock__c> start(Database.BatchableContext BC) {
		Date minimumDate = Date.newInstance(2016, 4, 18);
		List<Stock__c> stocksForActivation = [select id, name, sm2m_status__c, State_Change_Date__c, Status__c
												from stock__c 
												where DAY_ONLY(State_Change_Date__c) <= :dateForSims
													  AND DAY_ONLY(State_Change_Date__c) >= :minimumDate
													  AND BIC_Framework_Contract_No__c = :fcNumber
													  AND SM2M_STATUS__c = 'ACTIVATION_READY'];
		return stocksForActivation;
	}

	//Update SIM cards SM2M Statuses and state change date
	//Send message for update in SM2M System
   	global void execute(Database.BatchableContext BC, List<Stock__c> scope) {
		for (Stock__c stock : scope){
			stock.Status__c = 'Pending activation';
		}

		update scope;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}