@isTest
public with sharing class TestTerminationConsoleNewController {
	@testSetup
    static void prepareData(){
        Account acc = Test_Util.createAccount('Account Test');
        insert acc;
        
        Case cs = Test_Util.createCase('Service','Complaints','Open',acc.id, 'Test Demo111', '24', 'Standard', 'Email','High', 'test@t.com');
        insert cs;
        
        
        list<Stock__C> stkLst = new list<Stock__c>();
        for(integer i=0; i < 10 ; i++){
            string name = '893407110028938747'+i;
            stkLst.add(Test_Util.createStock(name,acc.id));
        }
        insert stkLst;
    }

    static testMethod void TerminationConsoleController_Test(){
        List<Case> cseLst = new list<Case>([select id from Case limit 1]);
        if (cseLst != null && cseLst.size() > 0){
        	PageReference pf = Page.TerminationConsoleNewPage;
        	pf.getPArameters().put('id',cseLst[0].id);
            Test.setCurrentPage(pf);

            Test.startTest();
            TerminationConsoleNewController obj = new TerminationConsoleNewController();
            obj.addAllAccountSims();

            obj.iccIds = new Set<String>{'8934071100289387471', '8934071100289387472'};
			obj.icicdFilter = '8934071100289387471,8934071100289387472';
            obj.filterSims();

            obj.searchFC = 'FC - 91';
            obj.filterByFrameContract();


    		obj.simsToRemove='8934071100289387475,8934071100289387476';
            obj.removeSelectedSims();

    		obj.selectedSims='8934071100289387471,8934071100289387472';
            obj.terminateSelectedSims();

    		obj.selectedSims='8934071100289387471,8934071100289387472';
            obj.stopTerminationOfSelectedSims();

    		obj.selectedSims='8934071100289387471,8934071100289387472';
            obj.specialTerminationDate = Date.newInstance(2017, 1, 1);
            obj.specialTermination();

            System.assertEquals(false, obj.allSelected);
            obj.selectDeselectAllSims();
            System.assertEquals(true, obj.allSelected);
            obj.selectDeselectAllSims();

            System.assertEquals(false, obj.hasNext);
            System.assertEquals(false, obj.hasPrevious);
            System.assertEquals('0', obj.pTotalNumberPages);
            System.assertEquals(false, obj.AllSims.size()>0);
            System.assertEquals('0', obj.numOfConsoleSims);
            System.assertEquals(true, obj.AccountFrameContracts.size()>0);

            obj.sortByEndDate();
            obj.sortByRunTime();
            obj.sortByFc();
            obj.sortByTerminationStatus();
            obj.sortByPotentialTerminationDate();
            obj.sortBySM2MStatus();
            obj.exportToExcel();
            obj.prepareCSVFileData();
            obj.downloadTerminationConfirmLetter();
            obj.getTerminatedSIMs();

            obj.pLast();
            obj.pPrevious();
            obj.pFirst();
            obj.pNext();

            String fileBodyString = '8934071100289387470' + '\n' + '8934071100289387471';
            Blob fileBody = blob.valueOf(fileBodyString);
            obj.fileBody = fileBody;
            obj.fileNAme = 'FileName';
            obj.uploadSims();

            Id tmpId = Id.valueOf('001xa000003DIlo');
            obj.isUserAllowedForSpecialTermination(tmpId);

            Test.stopTest();
        }
    }
}